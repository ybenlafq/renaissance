#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHC20P.ksh                       --- VERSION DU 08/10/2016 22:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGHC20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 97/12/11 AT 12.05.28 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GHC20P                                              
# --------------------------------------------------------------------         
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BHC200                                                                
#  ------------                                                                
#  PURGE DE LA TABLE RTHC00 ET RTHC01                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GHC20PA
       ;;
(GHC20PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  97/12/11 AT 12.05.28 BY BURTEC6                  
# *    JOBSET INFORMATION:    NAME...: GHC20P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'PURGE GHC'                             
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GHC20PAA
       ;;
(GHC20PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  POUR EVITER LES PB DE DEAD-LOCK                                             
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN ENTREE (ACCES SOUS-TABLE DELAI)                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSHC00   : NAME=RSHC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC00 /dev/null
#    RSHC01   : NAME=RSHC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC01 /dev/null
# ------  FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHC200 
       JUMP_LABEL=GHC20PAB
       ;;
(GHC20PAB)
       m_CondExec 04,GE,GHC20PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHC210                                                                
#  ------------                                                                
#  PURGE DE LA TABLE RTHC02 ET RTHC03                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHC20PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=GHC20PAD
       ;;
(GHC20PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN ENTREE                                                     
#    RSHC00   : NAME=RSHC00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC00 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSHC02   : NAME=RSHC02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC02 /dev/null
#    RSHC03   : NAME=RSHC03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC03 /dev/null
# ------  FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHC210 
       JUMP_LABEL=GHC20PAE
       ;;
(GHC20PAE)
       m_CondExec 04,GE,GHC20PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
