#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BHE61Y.ksh                       --- VERSION DU 13/10/2016 16:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYBHE61 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.11.21 BY BURTEC6                      
#    STANDARDS: P  JOBSET: BHE61Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BHE61YA
       ;;
(BHE61YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BHE61YAA
       ;;
(BHE61YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YAA.sysin
       m_UtilityExec
# ********************************************************************         
#           VERIFY DES FICHIERS VSAM                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAG
       ;;
(BHE61YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER CICS (ENTREE)                                                 
       m_FileAssign -d SHR FJHE60E ${DATA}/PEX0.F45.JHE601Y
#                                                                              
# ****** FICHIER DE L ETAT JHE601 (SORTIE)                                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 FJHE60S ${DATA}/PTEM/F07.FJHE011Y
       m_ProgramExec BHE601 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER ISSU DU FJHE011Y                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAJ
       ;;
(BHE61YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/F07.FJHE011Y
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/BHE61YAJ.FJHE012Y
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_32_7 32 CH 07
 /KEYS
   FLD_CH_26_5 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_17_8 DESCENDING
 /* Record Type = F  Record Length = 243 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BHE61YAK
       ;;
(BHE61YAK)
       m_CondExec 00,EQ,BHE61YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROGRAME  PGM BHE602                                                        
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAM PGM=BHE602     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAM
       ;;
(BHE61YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FICHIER  (ENTREE)                                                     
       m_FileAssign -d SHR -g ${G_A2} FJHE60E ${DATA}/PTEM/BHE61YAJ.FJHE012Y
#                                                                              
# ****** FICHIER DE L ETAT JHE601 (SORTIE)                                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 FJHE60S ${DATA}/PXX0/F45.FJHE013Y.ENVOI.CSV
       m_ProgramExec BHE602 
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER VERS MAIL VIA XFB-GATEWAY                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FJHE013Y                                                            
#          DATAEND                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAQ PGM=PGMSVC34   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAQ
       ;;
(BHE61YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YAQ.sysin
       m_UtilityExec
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTBHE61Y                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAT
       ;;
(BHE61YAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BHE61YAT.FTBHE61Y
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBHE61Y                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61YAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YAX
       ;;
(BHE61YAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.BHE61YAT.FTBHE61Y(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BHE61YBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YBA
       ;;
(BHE61YBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BHE61YZA
       ;;
(BHE61YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
