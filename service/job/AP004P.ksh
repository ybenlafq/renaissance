#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP004P.ksh                       --- VERSION DU 08/10/2016 13:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP004 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/12 AT 13.24.40 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AP004P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ***************************************                                      
#  ATTESTATIONS FACTURES A21                                                   
#  REPRISE: OUI                                                                
# ***************************************                                      
# *  DELETE DU FICHIER ANNUEL                                                  
# ***************************************                                      
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP004PA
       ;;
(AP004PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       PROCSTEP=${PROCSTEP:-AP004PA}
       RUN=${RUN}
       JUMP_LABEL=AP004PAA
       ;;
(AP004PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AP004PAA.sysin
       m_UtilityExec
# **************************************                                       
#  BAP010  : ATTESTATIONS FACTURES A21                                         
#  REPRISE : OUI                                                               
# **************************************                                       
#                                                                              
# ***********************************                                          
# *   STEP AP004PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=AP004PAD
       ;;
(AP004PAD)
       m_CondExec ${EXAAF},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP17 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#  FICHIER DES ATTESTATIONS A2I POUR IMPRIMEUR (ZIP ET ENVOI GATEWAY)          
       m_FileAssign -d NEW,CATLG,DELETE -r 700 -t LSEQ FAP010 ${DATA}/PXX0.F07.FAP010P
#                                                                              
# ***********************************                                          
# AAK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":FAP010AP"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":FAP010P"",FAP010AP.TXT)                                       
#  ":FAP010P""                                                                 
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP010 
       JUMP_LABEL=AP004PAE
       ;;
(AP004PAE)
       m_CondExec 04,GE,AP004PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP004PAG PROC=JVZIP     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=AP004PAG
       ;;
(AP004PAG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAK},NE,YES "
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileOverride -d NEW,CATLG,DELETE -r 700 -t LSEQ -s JVZIPU FICZIP ${DATA}/PXX0.F07.FAP010AP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AP004PAG.sysin
       JUMP_LABEL=AP004PAJ
       ;;
(AP004PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/AP004PAJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
