#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC10P.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGQC10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 18.16.23 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC10P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BQC140 : EXTRACTION DES DONNEES VENTES TOPEES LIVREES                      
#   REPRISE: LA REPRISE SERA FAITE DEMAIN MATIN                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC10PA
       ;;
(GQC10PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC10PAA
       ;;
(GQC10PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAM ASSOCIES AUX FAMILLES                                    
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ADRESSES                                                       
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSPO01   : NAME=RSPO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPO01 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/GQC10P1
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GQC10P2
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010 ${DATA}/PXX0/F07.BQC010AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC011 ${DATA}/PXX0/F07.BQC011AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012 ${DATA}/PXX0/F07.BQC012AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FQC013 ${DATA}/PXX0/F07.BQC013AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC140 
       JUMP_LABEL=GQC10PAB
       ;;
(GQC10PAB)
       m_CondExec 04,GE,GQC10PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC141 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAD
       ;;
(GQC10PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER ALIMENT� EN CAS DE REPRISE                                   
       m_FileAssign -d SHR -g +0 FCREA ${DATA}/PXX0/F07.FCREAAP
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
#                                                                              
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC014 ${DATA}/PXX0/F07.BQC014AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC141 
       JUMP_LABEL=GQC10PAE
       ;;
(GQC10PAE)
       m_CondExec 04,GE,GQC10PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAG
       ;;
(GQC10PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BQC014AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BQC016AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_PD_88_7 88 PD 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_PD_88_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10PAH
       ;;
(GQC10PAH)
       m_CondExec 00,EQ,GQC10PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC171 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAP      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# RSGQ01   FILE  DYNAM=YES,NAME=RSGQ01,MODE=I                                  
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01,MODE=I                                  
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ** FICHIER EN ENTREE                                                         
# FQC014   FILE  NAME=BQC016AP,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC020   FILE  NAME=BQC020AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC171) PLAN(BQC171)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
# * TRI DU FIC                                                                 
# * REPRISE: OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC020AP,MODE=I                                          
# SORTOUT  FILE  NAME=BQC021AP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(5,19,A),FORMAT=CH                                              
#  RECORD TYPE=F,LENGTH=50                                                     
#          DATAEND                                                             
# ********************************************************************         
#   BQC142 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA00   FILE  DYNAM=YES,NAME=RSGA00,MODE=I                                  
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# RSGA03   FILE  DYNAM=YES,NAME=RSGA03,MODE=I                                  
# RSGS65   FILE  DYNAM=YES,NAME=RSGS65,MODE=I                                  
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02,MODE=I                                  
# RSGV10   FILE  DYNAM=YES,NAME=RSGV10,MODE=I                                  
# ******* FDATE JJMMSSAA                                                       
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ******** FICHIER ALIMENT� EN CAS DE REPRISE                                  
# FCREA    FILE  NAME=FCREAAP,MODE=I                                           
# ** FICHIER EN ENTREE                                                         
# FQC020   FILE  NAME=BQC021AP,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC015   FILE  NAME=BQC015AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC142) PLAN(BQC142)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABE      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC015AP,MODE=I                                          
# SORTOUT  FILE  NAME=BQC022AP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,7,A),FORMAT=CH                                               
#  RECORD TYPE=F,LENGTH=100                                                    
#          DATAEND                                                             
# ********************************************************************         
#   BQC161 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# ABJ      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01,MODE=I                                  
# RSQC06   FILE  DYNAM=YES,NAME=RSQC06,MODE=I                                  
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# * FICHIER EN ENTREE                                                          
# FQC015   FILE  NAME=BQC022AP,MODE=I                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC161) PLAN(BQC161)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   BQC145 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAJ PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAJ
       ;;
(GQC10PAJ)
       m_CondExec ${EXABO},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A2} FQC010E ${DATA}/PXX0/F07.BQC010AP
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010S ${DATA}/PXX0/F07.BQC145AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC145 
       JUMP_LABEL=GQC10PAK
       ;;
(GQC10PAK)
       m_CondExec 04,GE,GQC10PAJ ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAM PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAM
       ;;
(GQC10PAM)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.BQC012AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC10PAM.BQC012CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_1 7 CH 1
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_1 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10PAN
       ;;
(GQC10PAN)
       m_CondExec 00,EQ,GQC10PAM ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC146 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAQ PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAQ
       ;;
(GQC10PAQ)
       m_CondExec ${EXABY},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A4} FQC012E ${DATA}/PTEM/GQC10PAM.BQC012CP
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012S ${DATA}/PXX0/F07.BQC146AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC146 
       JUMP_LABEL=GQC10PAR
       ;;
(GQC10PAR)
       m_CondExec 04,GE,GQC10PAQ ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * REMISE _A BLANCE DU FICHIER CONTENANT LES PARAM DE REPRISE                  
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10PAT PGM=IDCAMS     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PAT
       ;;
(GQC10PAT)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 ${DATA}/CORTEX4.P.MTXTFIX1/GQC10P01
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FCREAAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GQC10PAU
       ;;
(GQC10PAU)
       m_CondExec 16,NE,GQC10PAT ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC10PZA
       ;;
(GQC10PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
