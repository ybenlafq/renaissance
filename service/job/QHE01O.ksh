#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QHE01O.ksh                       --- VERSION DU 08/10/2016 14:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POQHE01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/02/25 AT 10.37.04 BY BURTECN                      
#    STANDARDS: P  JOBSET: QHE01O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   REQUETE QHE01                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=QHE01OA
       ;;
(QHE01OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2003/02/25 AT 10.37.04 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: QHE01O                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'LISTE QQF'                             
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=QHE01OAA
       ;;
(QHE01OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE18 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE18 
       JUMP_LABEL=QHE01OAB
       ;;
(QHE01OAB)
       m_CondExec 04,GE,QHE01OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE01                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE01OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QHE01OAD
       ;;
(QHE01OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE20 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE20 
       JUMP_LABEL=QHE01OAE
       ;;
(QHE01OAE)
       m_CondExec 04,GE,QHE01OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
