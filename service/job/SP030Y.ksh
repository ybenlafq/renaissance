#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SP030Y.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYSP030 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 06/11/02 AT 16.41.14 BY OPERAT1                      
#    STANDARDS: P  JOBSET: SP030Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FSP015 ISSU DU PGM BSP010 DE LA CHAINE SP010R                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SP030YA
       ;;
(SP030YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SP030YAA
       ;;
(SP030YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FSP015AY
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/SP030YAA.FSP030AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_145_3 145 CH 3
 /FIELDS FLD_CH_106_5 106 CH 5
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_133_9 133 CH 9
 /FIELDS FLD_PD_133_9 133 PD 9
 /FIELDS FLD_PD_124_9 124 PD 9
 /FIELDS FLD_PD_142_3 142 PD 3
 /FIELDS FLD_CH_1_6 01 CH 06
 /FIELDS FLD_CH_124_9 124 CH 9
 /FIELDS FLD_CH_142_3 142 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_5 ASCENDING,
   FLD_PD_142_3 ASCENDING,
   FLD_CH_106_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_124_9,
    TOTAL FLD_PD_133_9
 /* Record Type = F  Record Length = 040 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_6,FLD_CH_14_5,FLD_CH_142_3,FLD_CH_106_5,FLD_CH_133_9,FLD_CH_124_9,FLD_CH_145_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SP030YAB
       ;;
(SP030YAB)
       m_CondExec 00,EQ,SP030YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSP030                                                                
#  ------------                                                                
#  GENERATIONS DES PROVISIONS                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP030YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SP030YAD
       ;;
(SP030YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN MAJ                                                         
#    RTSP25   : NAME=RSSP25Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP25 /dev/null
# ------  TABLE EN LECTURE                                                     
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FSP030 ${DATA}/PTEM/SP030YAA.FSP030AY
# ------  FICHIER FFMICRO ENTRANT DANS LE NETTING                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FFMICRO ${DATA}/PXX0/F45.FFG30AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP030 
       JUMP_LABEL=SP030YAE
       ;;
(SP030YAE)
       m_CondExec 04,GE,SP030YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSP031                                                                
#  ------------                                                                
#  JUSTIFICATION DE PROVISIONS ET D AVOIRS  (ETAT DE DETAIL)                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP030YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SP030YAG
       ;;
(SP030YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA22   : NAME=RSGA22Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA22 /dev/null
#    RTSP25   : NAME=RSSP25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP25 /dev/null
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  SORTIE EDITION                                                       
       m_OutputAssign -c 9 -w ISP031 ISP031
# ------  FICHIER FSP031 ENTRANT DANS LE PGM BSP032                            
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FSP031 ${DATA}/PTEM/SP030YAG.FSP031AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP031 
       JUMP_LABEL=SP030YAH
       ;;
(SP030YAH)
       m_CondExec 04,GE,SP030YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FSP031 ISSU DU PGM BSP031 LE PGM BSP032                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP030YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SP030YAJ
       ;;
(SP030YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/SP030YAG.FSP031AY
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 SORTOUT ${DATA}/PTEM/SP030YAJ.FSP031BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 01 CH 12
 /FIELDS FLD_PD_26_9 26 PD 09
 /FIELDS FLD_PD_44_9 44 PD 9
 /FIELDS FLD_CH_13_5 13 CH 5
 /FIELDS FLD_PD_35_9 35 PD 9
 /FIELDS FLD_PD_18_3 18 PD 3
 /KEYS
   FLD_CH_1_12 ASCENDING,
   FLD_CH_13_5 DESCENDING,
   FLD_PD_18_3 DESCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_26_9,
    TOTAL FLD_PD_35_9,
    TOTAL FLD_PD_44_9
 /* Record Type = F  Record Length = 060 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP030YAK
       ;;
(SP030YAK)
       m_CondExec 00,EQ,SP030YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSP032                                                                
#  ------------                                                                
#  JUSTIFICATION DE PROVISIONS ET D AVOIRS (ETAT DE SYNTHESE)                  
# ********************************************************************         
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP030YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SP030YAM
       ;;
(SP030YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A3} FSP031 ${DATA}/PTEM/SP030YAJ.FSP031BY
# ------  SORTIE EDITION                                                       
       m_OutputAssign -c 9 -w ISP030 ISP030
       m_OutputAssign -c 9 -w ISP032 ISP032
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP032 
       JUMP_LABEL=SP030YAN
       ;;
(SP030YAN)
       m_CondExec 04,GE,SP030YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SP030YZA
       ;;
(SP030YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SP030YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
