#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC600P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC600 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 17.46.20 BY BURTEC6                      
#    STANDARDS: P  JOBSET: BC600P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BBC600 : EXTRACTIONS DES ENREGISTREMLENTS ET CALCULS STATISTIQUES           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BC600PA
       ;;
(BC600PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BC600PAA
       ;;
(BC600PAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
# *********************************                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****    ENTREE: FICHIER DATE                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******  SORTIE : FICHIER VERS BBC601                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FBC600 ${DATA}/PTEM/F07.FBC600
#                                                                              
# ******  ENTREE / SORTIE : TABLES EN LECTURE                                  
#    RTBC04   : NAME=RSBC04P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBC04 /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC600 
       JUMP_LABEL=BC600PAB
       ;;
(BC600PAB)
       m_CondExec 04,GE,BC600PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FIC                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC600PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BC600PAD
       ;;
(BC600PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/F07.FBC600
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/F07.FBC600TR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_39 1 CH 39
 /KEYS
   FLD_CH_1_39 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BC600PAE
       ;;
(BC600PAE)
       m_CondExec 00,EQ,BC600PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBC601 : MISE EN FORME CSV                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC600PAG PGM=BBC601     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BC600PAG
       ;;
(BC600PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ENTRE  : FICHIER BBC600TR                                            
       m_FileAssign -d SHR -g ${G_A2} FBC600 ${DATA}/PTEM/F07.FBC600TR
#                                                                              
# ******  SORTIE : FICHIER VERS BBC601                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-104 -t LSEQ -g +1 FBC601 ${DATA}/PXX0/F07.FBC601AP
       m_ProgramExec BBC601 
#                                                                              
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER STAT VERS XFB                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=BC600P                                        
#                                                                              
#                                                                              
#     LINE                                                                     
#      FLD 1,SOURCE='SEND PART=XFBPRO,'                                        
#     LINE                                                                     
#      FLD 6,SOURCE='IDF=FBC601AP,'                                            
#     LINE                                                                     
#      FLD  6,SOURCE='NFNAME=SATISTIQUES_CARTES_CLIENTS_'                      
#      FLD 40,SOURCE=M                                                         
#      FLD 42,SOURCE='.CSV'                                                    
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTBC600P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC600PAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BC600PAJ
       ;;
(BC600PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC600PAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BC600PAJ.FTBC600P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBC600P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC600PAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BC600PAM
       ;;
(BC600PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BC600PAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.BC600PAJ.FTBC600P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BC600PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BC600PAQ
       ;;
(BC600PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC600PAQ.sysin
       m_ProgramExec EZACFSM1
# *********************************                                            
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BC600PZA
       ;;
(BC600PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC600PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
