#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PGQC1Y.ksh                       --- VERSION DU 09/10/2016 00:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPGQC1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/12/29 AT 16.40.28 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PGQC1Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PROG : BQC904          SS TABLE DELAI                                       
#  EPURATION RTQC01 LIGNES HISTORIQUES                                         
#  EPURATION RTQC02                                                            
#  EPURATION RTQC03                                                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PGQC1YA
       ;;
(PGQC1YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PGQC1YAA
       ;;
(PGQC1YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE             -- VUE RVGA01CG                        
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* TABLE CARTES T VENTES LIVREES -- VUE RVQC0100                        
#    RSQC01Y  : NAME=RSQC01Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC01Y /dev/null
# ******* TABLE CARTES T VENTES EMPORTEES -- VUE RVQC0200                      
#    RSQC02Y  : NAME=RSQC02Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC02Y /dev/null
# ******* TABLE CARTES T SAV            -- VUE RVQC0300                        
#    RSQC03Y  : NAME=RSQC03Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC03Y /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER SOCIETE                                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC904 
       JUMP_LABEL=PGQC1YAB
       ;;
(PGQC1YAB)
       m_CondExec 04,GE,PGQC1YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTQC01                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1YAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1YAD
       ;;
(PGQC1YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 171 -g +1 SYSREC01 ${DATA}/PXX0/F45.QC01UNYA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1YAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC01Y                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1YR0                                     
#                  VERIFIER LE BACKOUT RPGQC1Y ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1YAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1YAM
       ;;
(PGQC1YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 115 -g +1 SYSREC01 ${DATA}/PXX0/F45.QC02UNYA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1YAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC02Y                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1YR1                                     
#                  VERIFIER LE BACKOUT RPGQC1Y ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1YAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1YAX
       ;;
(PGQC1YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 139 -g +1 SYSREC01 ${DATA}/PXX0/F45.QC03UNYA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1YAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC03Y                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1YR2                                     
#                  VERIFIER LE BACKOUT RPGQC1Y ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1YBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1YZA
       ;;
(PGQC1YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
