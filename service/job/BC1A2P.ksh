#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC1A2P.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC1A2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/12/03 AT 23.02.02 BY BURTECC                      
#    STANDARDS: P  JOBSET: BC1A2P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CREATION LE 28.12.2004 CCA                                                  
# ********************************************************************         
#  CREATION D'UNE GENERATION A VIDE POUR DEBLOCAGE DE CLIC0P ET CLIREP         
#  EN CAS DE PLANTAGE DE BC102P                                                
#  CREATION D'UNE GENERATION A VIDE POUR EVITER PLANTAGE DANS BC103P           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BC1A2PA
       ;;
(BC1A2PA)
#
#BC1A2PAG
#BC1A2PAG Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#BC1A2PAG
#
       EXAAA=${EXAAA:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2011/12/03 AT 23.02.02 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: BC1A2P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-BC102P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BC1A2PAA
       ;;
(BC1A2PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC ISSU NORMALEMENT DE LA CHAINE BC102P                             
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
# *****   FICHIER REMIS A ZERO A DESTINATION DES CHAINES BASE CLIENTS          
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -g +1 OUT1 ${DATA}/PXX0/F07.FBC102AP
       m_FileAssign -d NEW,CATLG,DELETE -r 750 -g +1 OUT2 ${DATA}/PXX0/F07.FBC102BP
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -g +1 OUT3 ${DATA}/PXX0/F07.FBC102CP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC1A2PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BC1A2PAB
       ;;
(BC1A2PAB)
       m_CondExec 16,NE,BC1A2PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC1A2PAD PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
