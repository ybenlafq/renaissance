#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB068P.ksh                       --- VERSION DU 13/10/2016 16:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB068 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/29 AT 15.27.33 BY PREPA2                       
#    STANDARDS: P  JOBSET: RB068P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#           VERIFY DES FICHIERS VSAM                                           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB068PA
       ;;
(RB068PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RB068PAD
       ;;
(RB068PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR INPUT ${DATA}/PAB0.F44.ABBAS
       m_FileAssign -d NEW,CATLG,DELETE -r 1500 -g +1 OUTPUT ${DATA}/PTEM/RB068PAD.BRB068AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB068PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB068PAE
       ;;
(RB068PAE)
       m_CondExec 16,NE,RB068PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB068PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB068PAG
       ;;
(RB068PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/RB068PAD.BRB068AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1500 -g +1 SORTOUT ${DATA}/PTEM/RB068PAG.BRB068BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_12 "218318"
 /DERIVEDFIELD CST_9_20 "A"
 /DERIVEDFIELD CST_1_4 "218316"
 /DERIVEDFIELD CST_7_16 "E")
 /DERIVEDFIELD CST_3_8 "218317"
 /FIELDS FLD_CH_90_1 90 CH 1
 /FIELDS FLD_CH_30_30 30 CH 30
 /FIELDS FLD_CH_98_6 98 CH 6
 /CONDITION CND_1 FLD_CH_98_6 EQ CST_1_4 OR FLD_CH_98_6 EQ CST_3_8 OR FLD_CH_98_6 EQ CST_5_12 OR FLD_CH_90_1 EQ CST_7_16 AND FLD_CH_90_1 NE CST_9_20 
 /KEYS
   FLD_CH_30_30 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB068PAH
       ;;
(RB068PAH)
       m_CondExec 00,EQ,RB068PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRB068                                                                      
#  EXTRATION DES PRODUITS DARTYBOX RECEPTIONNES LE MOIS PRECEDENT              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB068PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB068PAJ
       ;;
(RB068PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/RB068P01
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10 /dev/null
#    RSRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB00 /dev/null
#    RSRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB01 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
#    RSRB35   : NAME=RSRB35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB35 /dev/null
#                                                                              
# *****   FICHIER                                                              
       m_FileAssign -d SHR -g ${G_A2} FRB068 ${DATA}/PTEM/RB068PAG.BRB068BP
#                                                                              
       m_OutputAssign -c 9 -w IRB068A IRB068A
#                                                                              
       m_OutputAssign -c 9 -w IRB068B IRB068B
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB068 
       JUMP_LABEL=RB068PAK
       ;;
(RB068PAK)
       m_CondExec 04,GE,RB068PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB068PZA
       ;;
(RB068PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB068PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
