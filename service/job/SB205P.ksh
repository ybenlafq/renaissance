#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SB205P.ksh                       --- VERSION DU 09/10/2016 05:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSB205 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/03 AT 11.54.58 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SB205P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER VENANT DE LA GATEWAY                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SB205PA
       ;;
(SB205PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2014/11/03 AT 11.54.58 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: SB205P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'RECUP INFO GV'                         
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SB205PAA
       ;;
(SB205PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# PRED     LINK  NAME=$EX010P,MODE=I                                           
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.BSB205AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SB205PAA.BSB205BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_5_1 5 CH 1
 /KEYS
   FLD_CH_11_8 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_5_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SB205PAB
       ;;
(SB205PAB)
       m_CondExec 00,EQ,SB205PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BSB205 : 1.                                                                
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB205PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SB205PAD
       ;;
(SB205PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES                                                               
#    RSBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS03 /dev/null
# ******* TABLES                                                               
#    RSBS01   : NAME=RSBS01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS03 /dev/null
#                                                                              
       m_FileAssign -d SHR -g +0 FDATE ${DATA}/PEX0/F07.FDATE
# ******  FICHIER VENANT DE LA GATEWAY                                         
       m_FileAssign -d SHR -g ${G_A1} FMGDENT ${DATA}/PTEM/SB205PAA.BSB205BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB205 
       JUMP_LABEL=SB205PAE
       ;;
(SB205PAE)
       m_CondExec 04,GE,SB205PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SB205PZA
       ;;
(SB205PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SB205PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
