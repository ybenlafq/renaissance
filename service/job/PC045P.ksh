#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC045P.ksh                       --- VERSION DU 08/10/2016 21:59
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC045 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.27.44 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PC045P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *************************************                                        
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PC045PA
       ;;
(PC045PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PC045PAA
       ;;
(PC045PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
# *************************************                                        
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F07.FPC045AP
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PC045PAA.FPC045XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_143 1 CH 143
 /KEYS
   FLD_CH_1_143 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PC045PAB
       ;;
(PC045PAB)
       m_CondExec 00,EQ,PC045PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : FPC045  ALIM BASE ACC A PARTIR DE COMMANDES 3P                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC045PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PC045PAD
       ;;
(PC045PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN M.A.J.                                                      
#    RSPC05   : NAME=RSPC05,MODE=U - DYNAM=YES                                 
# -X-RSPC05   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSPC05 /dev/null
#    RSPC06   : NAME=RSPC06,MODE=U - DYNAM=YES                                 
# -X-RSPC06   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSPC06 /dev/null
# ******  FIC EN ENTREE ( SORTIE DU TRI )                                      
       m_FileAssign -d SHR -g ${G_A1} FPC045 ${DATA}/PTEM/PC045PAA.FPC045XP
# ******  FIC DES ANOMALIES                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F07.FPC045BP
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC045 
       JUMP_LABEL=PC045PAE
       ;;
(PC045PAE)
       m_CondExec 04,GE,PC045PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  DELETE DES FICHIERS REMONT�S VIA GATEWAY                                  
# * REPRISE : OUI                                                              
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP PC045PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PC045PAG
       ;;
(PC045PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC045PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PC045PAH
       ;;
(PC045PAH)
       m_CondExec 16,NE,PC045PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CR�ATION _A VIDE D'UNE G�N�RATION SI PAS DE FICHIER                          
#  R�CUP�R� LA VEILLE.                                                         
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP PC045PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PC045PAJ
       ;;
(PC045PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC LOG DE COMPLETEL                                                 
       m_FileAssign -d SHR IN1 /dev/null
# ******  CR�ATION _A VIDE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.FPC045AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC045PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PC045PAK
       ;;
(PC045PAK)
       m_CondExec 16,NE,PC045PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PC045PZA
       ;;
(PC045PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC045PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
