#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SP200O.ksh                       --- VERSION DU 09/10/2016 05:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POSP200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/06 AT 17.48.10 BY BURTECN                      
#    STANDARDS: P  JOBSET: SP200O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   BSP200 : EXTRACT CODIC EPD EPF AVEC STOCK MAG OU STOCK DEPOT > 0           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SP200OA
       ;;
(SP200OA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2003/01/06 AT 17.48.10 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: SP200O                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'CONTROL STATUT APPRO'                  
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SP200OAA
       ;;
(SP200OAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLE                                                              
#    RTGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00O /dev/null
# ******* LIEUX                                                                
#    RTGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10O /dev/null
# ******* FAMILLE                                                              
#    RTGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14O /dev/null
# ******* STOCK DEPOT                                                          
#    RTGS10O  : NAME=RSGS10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS10O /dev/null
# ******* STOCK MAG                                                            
#    RTGS30O  : NAME=RSGS30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGS30O /dev/null
#                                                                              
# ******* FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FSP200 ${DATA}/PXX0/F16.BSP200AO
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP200 
       JUMP_LABEL=SP200OAB
       ;;
(SP200OAB)
       m_CondExec 04,GE,SP200OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
