#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SB010O.ksh                       --- VERSION DU 08/10/2016 15:20
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POSB010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/07 AT 11.15.33 BY BURTECC                      
#    STANDARDS: P  JOBSET: SB010O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSB010 : RECUPERATION DU NUMERO DE VENTE                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SB010OA
       ;;
(SB010OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=SB010OAA
       ;;
(SB010OAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN LECTURE                                                        
#    RTBS01   : NAME=RSBS01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTGV08   : NAME=RSGV08O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV08 /dev/null
#    RTGV10   : NAME=RSGV10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# *** TABLE EN MAJ                                                             
#    RTBS01   : NAME=RSBS01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTBS02   : NAME=RSBS02,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS02 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB010 
       JUMP_LABEL=SB010OAB
       ;;
(SB010OAB)
       m_CondExec 04,GE,SB010OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSB020 : RECUPERATION DES DATES DE TOPAGE                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB010OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SB010OAD
       ;;
(SB010OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***                                                                          
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# *** TABLES EN LECTURE                                                        
#    RTBS01   : NAME=RSBS01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTGV10   : NAME=RSGV10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
# *** TABLE EN MAJ                                                             
#    RTBS01   : NAME=RSBS01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTBS02   : NAME=RSBS02,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS02 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB020 
       JUMP_LABEL=SB010OAE
       ;;
(SB010OAE)
       m_CondExec 04,GE,SB010OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
