#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM030P.ksh                       --- VERSION DU 08/10/2016 12:56
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNM030 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/07/11 AT 16.39.49 BY BURTECB                      
#    STANDARDS: P  JOBSET: NM030P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM030PA
       ;;
(NM030PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM030PAA
       ;;
(NM030PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM160AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNM003BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNMCUMCP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PBN0/NM030PAA.BNM003CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_8 14 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM030PAB
       ;;
(NM030PAB)
       m_CondExec 00,EQ,NM030PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM003 :  G�N�RATION DU FICHIER COMPTABLE I.C.S DES VENTES TOP�ES           
#           J(+1) SANS BON DE COMMANDE.                                        
#                                                                              
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM030PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM030PAD
       ;;
(NM030PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC DES VENTES TOP�ES SANS BDC                                       
       m_FileAssign -d SHR -g ${G_A1} FTS60 ${DATA}/PBN0/NM030PAA.BNM003CP
# ************  FICHIERS EN ENTREE ***************                             
# ******  FICHIER COMPTABLE A DESTINATION DE L'ICS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 FFTI00 ${DATA}/PNCGP/F07.BNM003AP
# ******  FICHIER DES VENTES TOP�ES EN REPRISE POUR J(+1)                      
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FREPRIS ${DATA}/PNCGP/F07.BNM003BP
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w IMPRIM IMPRIM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM003 
       JUMP_LABEL=NM030PAE
       ;;
(NM030PAE)
       m_CondExec 04,GE,NM030PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM030PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM030PAG
       ;;
(NM030PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DE LA VEILLE(VOIE ALTERN�E)                             
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 OUT1 ${DATA}/PNCGP/F07.BNMCUMCP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM030PAG.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM030PAH
       ;;
(NM030PAH)
       m_CondExec 16,NE,NM030PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM030PZA
       ;;
(NM030PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM030PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
