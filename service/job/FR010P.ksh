#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FR010P.ksh                       --- VERSION DU 08/10/2016 22:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFR010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/19 AT 14.33.51 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FR010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BFR100  :                                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FR010PA
       ;;
(FR010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FR010PAA
       ;;
(FR010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER EN ENTR�                                                     
       m_FileAssign -d SHR FEXTR ${DATA}/CORTEX4.P.MTXTFIX1/FR010PAA
# ******  DATE DEBUT JJMMSSAA                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE DEBUT JJMMSSAA                                                  
       m_FileAssign -i FDATDEB
$DMOISJ
_end
#                                                                              
# ******  DATE DEBUT JJMMSSAA                                                  
       m_FileAssign -i FDATFIN
$FMOISJ
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSFI61   : NAME=RSFI61,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFI61 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB55   : NAME=RSGB55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB55 /dev/null
#                                                                              
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FICFI61 ${DATA}/PXX0/F07.FFR010AP
#                                                                              
# ***************************************************************              
#  TRI DES FICHIERS VENTE                                                      
#  REPRISE : OUI                                                               
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP FR010PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          

       m_ProgramExec -b BFR100 
       JUMP_LABEL=FR010PAD
       ;;
(FR010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.FFR010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FFR010BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_84_10 84 PD 10
 /FIELDS FLD_PD_94_10 94 PD 10
 /FIELDS FLD_PD_67_7 67 PD 07
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_PD_74_10 74 PD 10
 /KEYS
   FLD_CH_1_16 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_67_7,
    TOTAL FLD_PD_74_10,
    TOTAL FLD_PD_84_10,
    TOTAL FLD_PD_94_10
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FR010PAE
       ;;
(FR010PAE)
       m_CondExec 00,EQ,FR010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFR101  :                                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR010PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FR010PAG
       ;;
(FR010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE DEBUT JJMMSSAA                                                  
       m_FileAssign -i FDATDEB
$DMOISJ
_end
#                                                                              
# ******  DATE DEBUT JJMMSSAA                                                  
       m_FileAssign -i FDATFIN
$FMOISJ
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A2} FICFI61 ${DATA}/PXX0/F07.FFR010BP
#                                                                              
# ******  FICHIERS EN SORTIE                                                   
       m_OutputAssign -c 9 -w IFR100 FICEDIT
# FICEDIT  REPORT SYSOUT=*,RECFM=FBA,LRECL=133,BLKSIZE=133                     
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFR101 
       JUMP_LABEL=FR010PAH
       ;;
(FR010PAH)
       m_CondExec 04,GE,FR010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
