#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FX053L.ksh                       --- VERSION DU 08/10/2016 21:58
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLFX053 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/04 AT 10.01.38 BY BURTECA                      
#    STANDARDS: P  JOBSET: FX053L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  CUMUL DU FICHIER DES MVTS DES CONTRATS + MVTS REJETES                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FX053LA
       ;;
(FX053LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=FX053LAA
       ;;
(FX053LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# ** MVTS DES CONTRATS DU JOUR ISSU DU BFS052                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BFS052CL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.BBS052CL
# ** FICHIER CUMUL DES MVTS REJETES DE LA VEILLE                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BPS200AL
# ** FICHIER CUMUL DES MVTS REJETES LRECL=80                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PXX0/F61.BPS200AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 1 CH 27
 /FIELDS FLD_CH_55_8 55 CH 8
 /KEYS
   FLD_CH_55_8 ASCENDING,
   FLD_CH_1_27 ASCENDING
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FX053LAB
       ;;
(FX053LAB)
       m_CondExec 00,EQ,FX053LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS200                                                                
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FX053LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FX053LAD
       ;;
(FX053LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE PARAMETRES ASSOCIES AU CONTRAT                                 
#    RSGA41   : NAME=RSGA41L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******  TABLE ECHEANCIER SAV 1                                               
#    RSPS01   : NAME=RSPS01L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01 /dev/null
# ******  TABLE ECHEANCIER SAV 2                                               
#    RSPS02   : NAME=RSPS02L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS02 /dev/null
# ******  FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ISSU DE L'ETAPE PRECEDENTE                                   
       m_FileAssign -d SHR -g ${G_A1} FPS200 ${DATA}/PXX0/F61.BPS200AL
# ******  FICHIER DES MVTS REJETES LRECL 80                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g ${G_A2} FPS210 ${DATA}/PXX0/F61.BPS200AL
# ******  EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IPS200 IPS200
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS200 
       JUMP_LABEL=FX053LAE
       ;;
(FX053LAE)
       m_CondExec 04,GE,FX053LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER BFS052CL PROVENANT DE LA CHAINE FS052L             
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FX053LAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FX053LAG
       ;;
(FX053LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT1 ${DATA}/PXX0/F61.BFS052CL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT2 ${DATA}/PNCGL/F61.BBS052CL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FX053LAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FX053LAH
       ;;
(FX053LAH)
       m_CondExec 16,NE,FX053LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCES POUR PLAN :                                                     
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
