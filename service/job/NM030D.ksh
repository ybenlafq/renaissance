#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM030D.ksh                       --- VERSION DU 09/10/2016 05:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDNM030 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/07/11 AT 16.33.04 BY BURTECB                      
#    STANDARDS: P  JOBSET: NM030D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM030DA
       ;;
(NM030DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM030DAA
       ;;
(NM030DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM160AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM003BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNMCUMCD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PBN0/NM030DAA.BNM003CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_14_8 14 CH 8
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM030DAB
       ;;
(NM030DAB)
       m_CondExec 00,EQ,NM030DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM003 :  G�N�RATION DU FICHIER COMPTABLE I.C.S DES VENTES TOP�ES           
#           J(+1) SANS BON DE COMMANDE.                                        
#                                                                              
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM030DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM030DAD
       ;;
(NM030DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC DES VENTES TOP�ES SANS BDC                                       
       m_FileAssign -d SHR -g ${G_A1} FTS60 ${DATA}/PBN0/NM030DAA.BNM003CD
# ************  FICHIERS EN ENTREE ***************                             
# ******  FICHIER COMPTABLE A DESTINATION DE L'ICS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 FFTI00 ${DATA}/PNCGD/F91.BNM003AD
# ******  FICHIER DES VENTES TOP�ES EN REPRISE POUR J(+1)                      
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FREPRIS ${DATA}/PNCGD/F91.BNM003BD
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w IMPRIM IMPRIM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM003 
       JUMP_LABEL=NM030DAE
       ;;
(NM030DAE)
       m_CondExec 04,GE,NM030DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM030DAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM030DAG
       ;;
(NM030DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DE LA VEILLE(VOIE ALTERN�E)                             
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 OUT1 ${DATA}/PNCGD/F91.BNMCUMCD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM030DAG.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM030DAH
       ;;
(NM030DAH)
       m_CondExec 16,NE,NM030DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM030DZA
       ;;
(NM030DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM030DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
