#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB015P.ksh                       --- VERSION DU 17/10/2016 18:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 16.32.48 BY BURTECW                      
#    STANDARDS: P  JOBSET: RB015P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  TRI OMIT POUR RETIRE LIGNES ENT�TES SI R�CUP PLUSIEURS FICHIERS             
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RB015PA
       ;;
(RB015PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 16.32.47 BY BURTECW                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: RB015P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'RETOUR LOG COMPLETEL'                  
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RB015PAA
       ;;
(RB015PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# *****   FICHIER ENTREE R�CUP CHEZ COMPLETEL                                  
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F07.CPTLLOG1
# *****   FICHIER TRI�                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB015PAA.CPTLLOG2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB015PAB
       ;;
(RB015PAB)
       m_CondExec 00,EQ,RB015PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI OMIT POUR RETIRE LIGNES ENT�TES SI R�CUP PLUSIEURS FICHIERS             
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAD
       ;;
(RB015PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE R�CUP CHEZ COMPLETEL                                  
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F07.NC00LOG1
# *****   FICHIER TRI�                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB015PAD.NC00LOG2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB015PAE
       ;;
(RB015PAE)
       m_CondExec 00,EQ,RB015PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRB015 : TRADUCTION DU FICHIER COMPLETEL et NC AU FORMAT               
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAG PGM=BRB015     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAG
       ;;
(RB015PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FIC D ENTREE (RECUP FTP)                                              
       m_FileAssign -d SHR -g ${G_A1} FRB014 ${DATA}/PTEM/RB015PAA.CPTLLOG2
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/RB015PAD.NC00LOG2
#                                                                              
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 FRB015 ${DATA}/PXX0/F07.BRB015AP
       m_ProgramExec BRB015 
#                                                                              
# *******************************************************************          
#  TRI DU FICHIER SORTANT DE BRB016                                            
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAJ
       ;;
(RB015PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/F07.BRB015AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRB015EP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/B0015PAD.BRB015CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_234_8 234 CH 08
 /FIELDS FLD_CH_1_25 01 CH 25
 /KEYS
   FLD_CH_234_8 ASCENDING,
   FLD_CH_1_25 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB015PAK
       ;;
(RB015PAK)
       m_CondExec 00,EQ,RB015PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB016 : INTEGRATION DU FICHIER DANS LES BASES                          
#               ET FORMATTAGE D UN FICHIER ANOMALIES A RECYCLER                
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAM
       ;;
(RB015PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE : FICHIER TRIE DU BRB000                                      
       m_FileAssign -d SHR -g ${G_A3} FRB015 ${DATA}/PTEM/B0015PAD.BRB015CP
#                                                                              
# ******  SORTIE : FICHIER ANOMALIES (LRECL=320)                               
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 FRBANO ${DATA}/PXX0/F07.BRB015DP.ANOMALIE
#                                                                              
# ******  SORTIE : NOUVEAU FICHIER POUR LE LENDEMAIN (LRECL=320)               
       m_FileAssign -d NEW,CATLG,DELETE -r 320 -t LSEQ -g +1 FRB016 ${DATA}/PXX0/F07.BRB015EP
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB00 /dev/null
#    RTRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB016 
       JUMP_LABEL=RB015PAN
       ;;
(RB015PAN)
       m_CondExec 04,GE,RB015PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  DELETE DES FICHIERS REMONT�S VIA GATEWAY                                  
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAQ
       ;;
(RB015PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB015PAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB015PAR
       ;;
(RB015PAR)
       m_CondExec 16,NE,RB015PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CR�ATION _A VIDE D'UNE G�N�RATION SI PAS DE FICHIER                          
#  R�CUP�R� LA VEILLE.                                                         
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB015PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RB015PAT
       ;;
(RB015PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC LOG DE COMPLETEL                                                 
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
# ******  CR�ATION _A VIDE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.CPTLLOG1
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/FTP.F07.NC00LOG1
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB015PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB015PAU
       ;;
(RB015PAU)
       m_CondExec 16,NE,RB015PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB015PZA
       ;;
(RB015PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB015PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
