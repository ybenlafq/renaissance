#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC070P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/14 AT 11.42.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: PC070P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EXTRACTION CARTES CADEAUX TABLES GV(1 PASSAGE POUR PARIS ET FILIALE         
#  LA CHAINE PARISIENNE TRAITERA EN FIN LENSEMBLE DES FICHIERS(FILIALE         
#  POUR CREER UN FICHIER UNIQUE A DESTINATION DE LA GATEWAY                    
#  PGM BPC070                                                                  
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PC070PA
       ;;
(PC070PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PC070PAA
       ;;
(PC070PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSPC01   : NAME=RSPC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
#    RSPC02   : NAME=RSPC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC02 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PC070PAA
#                                                                              
# ******* FICHIER FCCUTIS DU JOUR (LRECL 500)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FCCUTIS ${DATA}/PTEM/PC070PAA.FPC070AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC070 
       JUMP_LABEL=PC070PAB
       ;;
(PC070PAB)
       m_CondExec 04,GE,PC070PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION CARTES CADEAUX TABLES VE (1 PASSAGE POUR PARIS & FILIALE         
#  PGM BPC071                                                                  
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAD
       ;;
(PC070PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSPC01   : NAME=RSPC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
#    RSPC02   : NAME=RSPC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC02 /dev/null
#    RSVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSVE14   : NAME=RSVE14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE14 /dev/null
#    RSVE02   : NAME=RSVE02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE02 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA56   : NAME=RSGA56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA56 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PC070PAD
#                                                                              
# ******* FICHIER FCCUTIS DU JOUR (LRECL 500)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FCCUTIS ${DATA}/PTEM/PC070PAD.FPC071AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC071 
       JUMP_LABEL=PC070PAE
       ;;
(PC070PAE)
       m_CondExec 04,GE,PC070PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT MERGE COPY DES DEUX FICHIERS OBTENUS                                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAG
       ;;
(PC070PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PC070PAA.FPC070AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PC070PAD.FPC071AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PC070PAG.FPC071BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PC070PAH
       ;;
(PC070PAH)
       m_CondExec 00,EQ,PC070PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT MERGE COPY DES DEUX FICHIERS OBTENUS                                   
#  SPECIFICITE DE LA CHAINE PARISIENNE AVEC REGROUPEMENT TOTAL                 
#  DE TOUS LES FICHIERS FILIALES POUR CREER UN FIC POUR LA GATEWAY             
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAJ
       ;;
(PC070PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PC070PAG.FPC071BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FPC071BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FPC071BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FPC071BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FPC071BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FPC071BD
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPC071CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PC070PAK
       ;;
(PC070PAK)
       m_CondExec 00,EQ,PC070PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DU FICHIER TOUTES FILIALES                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=FPC071CP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=PC70ZIPP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -ZIPPED_DSN(PXX0.F07.FPC071CP.*,EXTRACTION_USAGE_CC.CSV)                    
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAM
       ;;
(PC070PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/PC070PAM.FPC070BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAQ PGM=JVMLDM76   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAQ
       ;;
(PC070PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A4} DD1 ${DATA}/PXX0/F07.FPC071CP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.PC70ZIPP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PAQ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FICHIER VERS                                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=PC70ZIPP                                                            
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTPC070P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAT
       ;;
(PC070PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/PC070PAT.FTPC070P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPC070P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PC070PAX
       ;;
(PC070PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.PC070PAT.FTPC070P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PC070PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PC070PBA
       ;;
(PC070PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PC070PZA
       ;;
(PC070PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
