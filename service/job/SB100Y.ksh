#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SB100Y.ksh                       --- VERSION DU 09/10/2016 00:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYSB100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 16.38.07 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SB100Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSB100 :      MISE � JOUR ADRESSE MAIL RTGV03                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SB100YA
       ;;
(SB100YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SB100YAA
       ;;
(SB100YAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN LECTURE                                                        
#    RTPT01   : NAME=RSPT01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPT01 /dev/null
#    RTGV08   : NAME=RSGV02Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV08 /dev/null
#    RTGV10   : NAME=RSGV03Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ********* FICHIER EN ENTREE                                                  
       m_FileAssign -d SHR -g +0 FDERTRTE ${DATA}/PXX0/F45.FSB100AY
# ********* FICHIER WORK                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDERTRTS ${DATA}/PXX0/F45.FSB100AY
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FSB100S ${DATA}/PXX0/F45.FSB100BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB100 
       JUMP_LABEL=SB100YAB
       ;;
(SB100YAB)
       m_CondExec 04,GE,SB100YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BSB110 : RECHERCHE ADRESEE MAIL NON BLACK LIST�ES                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB100YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SB100YAD
       ;;
(SB100YAD)
       m_CondExec ${EXAAF},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A1} FSB110E ${DATA}/PXX0/F45.FSB100BY
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FSB110S ${DATA}/PXX0/F45.FSB110AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB110 
       JUMP_LABEL=SB100YAE
       ;;
(SB100YAE)
       m_CondExec 04,GE,SB100YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BSB120 : MISE _A JOUR RTGV03                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB100YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SB100YAG
       ;;
(SB100YAG)
       m_CondExec ${EXAAK},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN M_AJ                                                            
#    RTGV03   : NAME=RSGV03Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV03 /dev/null
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d SHR -g ${G_A2} FSB120E ${DATA}/PXX0/F45.FSB110AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB120 
       JUMP_LABEL=SB100YAH
       ;;
(SB100YAH)
       m_CondExec 04,GE,SB100YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
