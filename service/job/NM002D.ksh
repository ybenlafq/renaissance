#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM002D.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDNM002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/08 AT 10.18.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM002D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM002DA
       ;;
(NM002DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM002DAA
       ;;
(NM002DAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
#    RECUP FICHIER CAISSES MQ                                                  
# ***************************************                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QNM002D
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/NM002DAA
       m_ProgramExec IEFBR14 "RDAR,NM002D.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NM002DAD
       ;;
(NM002DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QNM002D
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=NM002DAE
       ;;
(NM002DAE)
       m_CondExec 00,EQ,NM002DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER ISSU DU BNM100 REMONTEE MQ ( CODE S55 )              
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAG
       ;;
(NM002DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM155AD
# ******* FIC DE REPRISE DES CAISSES EN ANO (J-1)                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM001BD
# ******* FIC DE CUMUL EN CAS DE PLANTAGE DU TRAITEMENT LA VEILLE              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNMCUMAD
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DAG.BNM155BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DAH
       ;;
(NM002DAH)
       m_CondExec 00,EQ,NM002DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI2 *  R�CUP�RATION DU TYPE='0' (PAIEMENTS)                                
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAJ
       ;;
(NM002DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NM002DAG.BNM155BD
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DAJ.BNM155CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "0"
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_35_8 35 CH 8
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_47_1 47 CH 1
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 DESCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DAK
       ;;
(NM002DAK)
       m_CondExec 00,EQ,NM002DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI3 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAM
       ;;
(NM002DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NM002DAG.BNM155BD
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DAM.BNM155DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 "BQ"
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_44_2 44 CH 2
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_35_8 35 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 AND FLD_CH_44_2 NE CST_3_14 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DAN
       ;;
(NM002DAN)
       m_CondExec 00,EQ,NM002DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI4 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******            ET REMISE EN BANQUE, OUTREC DE BQ* EN BQUE                
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAQ
       ;;
(NM002DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NM002DAG.BNM155BD
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DAQ.BNM155HD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "BQ"
 /DERIVEDFIELD CST_1_11  "BQE"
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_43 1 CH 43
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_47_154 47 CH 154
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_44_2 44 CH 2
 /CONDITION CND_1 FLD_CH_44_2 EQ CST_1_9 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_44_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_43,CST_1_11,FLD_CH_47_154
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=NM002DAR
       ;;
(NM002DAR)
       m_CondExec 00,EQ,NM002DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DES FICHIERS TYPE '0' ET TYPE '1                                
#  TRI5 *  PAR N�DE PI�CES.                                                    
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAT
       ;;
(NM002DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC TYPE 0 ET  1                                                     
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/NM002DAQ.BNM155HD
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/NM002DAJ.BNM155CD
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/NM002DAM.BNM155DD
# ******* FIC LOG DE CAISSES TRI� PAR N�DE PI�CE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DAT.BNM155ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_5 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DAU
       ;;
(NM002DAU)
       m_CondExec 00,EQ,NM002DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM000 : CE PGM SE CHARGE D'ECRIRE LE FICHIER INTERFACE STANDARD            
#           FFTI00 AU FUR ET A MESURE DES DONN�ES DE GESTION CONTENUES         
#           DANS LE FICHIER LOG DE CAISSES FNM001.                             
#           *                                                                  
#           **** VERIFICATION DU TRAITEMENT DEJA EFFECTUE DANS RTHV15          
#           *                                                                  
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NM002DAX
       ;;
(NM002DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE (SOUS TABLES NM000/NM001/NM002/NM003               
# *****                      NM004/NM005/NM006/NM007/NM008/BQMAG)              
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# *****   TABLE DES ECS                                                        
#    RSFX00   : NAME=RSFX00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ************** TABLES EN MISE A JOUR ***********                             
# *****   TABLE CONTROLE DES REMONTEES DE CAISSES                              
#    RSHV15   : NAME=RSHV15D,MODE=U - DYNAM=YES                                
# -X-NM002DR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV15 /dev/null
# *****                                                                        
#    RSGV11   : NAME=RSGV11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A8} FNM001 ${DATA}/PTEM/NM002DAT.BNM155ED
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -t LSEQ -g +1 FFTI00 ${DATA}/PTEM/NM002DAX.BNM000AD
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -t LSEQ -g +1 FFTI02 ${DATA}/PTEM/NM002DAX.BNM000GD
# ******  FICHIER DES CAISSES EN ERREUR                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/NM002DAX.BNM000BD
# ******  FICHIER POUR GENERATEUR D'ETAT (LISTE DE L'ETAT DES CAISSES)         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 INM001 ${DATA}/PTEM/NM002DAX.BNM000CD
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w IMPRIM IMPRIM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******  PARAMETRE : SIMU ==> AUCUNE M.A.J                                    
# ******  PARAMETRE : REEL ==> M.A.J                                           
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/NM002DAX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM000 
       JUMP_LABEL=NM002DAY
       ;;
(NM002DAY)
       m_CondExec 04,GE,NM002DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM001 : EXTRACTION DES CAISSES A COMPTABILISER                             
#           CREATION DU FICHIER POUR L'I.C.S  (INTERFACE COMPTABLE STD         
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBA PGM=BNM001     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBA
       ;;
(NM002DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **************  FICHIERS EN ENTREE ***************                           
#                                                                              
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A9} FNM001 ${DATA}/PTEM/NM002DAT.BNM155ED
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d SHR -g ${G_A10} FFTI00 ${DATA}/PTEM/NM002DAX.BNM000AD
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d SHR -g ${G_A11} FFTI02 ${DATA}/PTEM/NM002DAX.BNM000GD
# ******* FICHIER CAISSES + CAISSES A REPRENDRE DE J(-1)                       
       m_FileAssign -d SHR -g ${G_A12} FREPRIS ${DATA}/PTEM/NM002DAX.BNM000BD
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
#                                                                              
# ******  FICHIER POUR EDITION VERS CHAINE EDITION NM005*                      
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FNM002 ${DATA}/PNCGD/F91.BNM001AD
# ******  FICHIER DES CAISSES A REPRENDRE POUR J(+1)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FNM003 ${DATA}/PNCGD/F91.BNM001BD
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PNCGD/F91.BNM001CD
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT DACEM SEULEMENT                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI03 ${DATA}/PNCGD/F91.BNM001DD
       m_ProgramExec BNM001 
#                                                                              
# ********************************************************************         
#         TRI PREPARATOIRE POUR ETAT INM001                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBD
       ;;
(NM002DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER POUR LISTE DES CAISSES RECYCLEES                             
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/NM002DAX.BNM000CD
# ******  FICHIER DESTINE AU GENERATEUR D'ETAT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DBD.BNM000DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_3 18 CH 03
 /FIELDS FLD_BI_21_3 21 CH 03
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_24_2 24 CH 02
 /FIELDS FLD_BI_15_3 15 CH 03
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DBE
       ;;
(NM002DBE)
       m_CondExec 00,EQ,NM002DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050    CREATION D'UN FICHIER FCUMULS                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBG
       ;;
(NM002DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG00 /dev/null
#    RSEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG05 /dev/null
#    RSEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG10 /dev/null
#    RSEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG15 /dev/null
#    RSEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG25 /dev/null
#    RSEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/NM002DBD.BNM000DD
# ******  FICHIER FCUMULS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/NM002DBG.BNM000ED
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM002DBH
       ;;
(NM002DBH)
       m_CondExec 04,GE,NM002DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBJ
       ;;
(NM002DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/NM002DBG.BNM000ED
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002DBJ.BNM000FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002DBK
       ;;
(NM002DBK)
       m_CondExec 00,EQ,NM002DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   REPRISE : OUI         ETAT DES CAISSES EN RECYCLAGE                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBM
       ;;
(NM002DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/NM002DBD.BNM000DD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/NM002DBJ.BNM000FD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE TRAITEE : 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  EDITION                                                              
       m_OutputAssign -c 9 -w IEE200 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM002DBN
       ;;
(NM002DBN)
       m_CondExec 04,GE,NM002DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002DBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NM002DBQ
       ;;
(NM002DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE FV001)               
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGD/F91.BNMCUMAD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002DBQ.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM002DBR
       ;;
(NM002DBR)
       m_CondExec 16,NE,NM002DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM002DZA
       ;;
(NM002DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
