#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FP12FP.ksh                       --- VERSION DU 08/10/2016 12:54
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFP12F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 02/04/08 AT 14.34.27 BY BURTECC                      
#    STANDARDS: P  JOBSET: FP12FP                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#  PGM : BFP120  TRT REFACTURATIONS NETTING                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FP12FPA
       ;;
(FP12FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FP12FPAA
       ;;
(FP12FPAA)
       m_CondExec ${EXAAA},NE,YES 
# ******************************************                                   
# ******************************************                                   
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFP02P  : NAME=RSFP02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFP02P /dev/null
# ******* TABLE EN MAJ                                                         
#    RSFP01P  : NAME=RSFP01P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFP01P /dev/null
# ******* FICHIER DES FACTURES NETTING                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -g +1 FFMICRO ${DATA}/PMACP/F07.BFP120AP
# ******* FICHIER POUR EDITION D'UN ETAT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFP120 ${DATA}/PXX0/FP12FPAA.BFP120BP
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER PARAM 907000                                                 
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/FP12FPAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP120 
       JUMP_LABEL=FP12FPAB
       ;;
(FP12FPAB)
       m_CondExec 04,GE,FP12FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR EDITION D'UN ETAT                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP12FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FP12FPAD
       ;;
(FP12FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/FP12FPAA.BFP120BP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/FP12FPAD.BFP120CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_13_15 13 CH 15
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP12FPAE
       ;;
(FP12FPAE)
       m_CondExec 00,EQ,FP12FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFP130  CREATION ETAT EOS                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP12FPAG PGM=BFP130     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FP12FPAG
       ;;
(FP12FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FFP120 ${DATA}/PXX0/FP12FPAD.BFP120CP
# ******* ETAT EOS                                                             
       m_OutputAssign -c 9 -w JFP130 JFP130
       m_ProgramExec BFP130 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FP12FPZA
       ;;
(FP12FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP12FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
