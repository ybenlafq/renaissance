#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC102P.ksh                       --- VERSION DU 09/10/2016 05:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC102 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/07/02 AT 13.17.59 BY BURTEC9                      
#    STANDARDS: P  JOBSET: BC102P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  CREAT  A VIDE  FICHIER                                                      
#  EVITE TILT SI AUCUN AS400 CONNECT�                                          
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BC102PA
       ;;
(BC102PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BC102PAA
       ;;
(BC102PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
       m_FileAssign -d SHR FBC100 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -t LSEQ -g +1 FBC100P ${DATA}/PXX0/F07.FBC100P
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC102PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BC102PAB
       ;;
(BC102PAB)
       m_CondExec 16,NE,BC102PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COPY DE TOUS GDG  VENANT NASC CENTRALIS� M5I351  VIA GATEWAY                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC102PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BC102PAD
       ;;
(BC102PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
       m_FileAssign -d SHR FBC100P ${DATA}/PXX0/F07.FBC100P
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -t LSEQ -g +1 FBC100BP ${DATA}/PXX0/F07.FBC100BP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC102PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BC102PAE
       ;;
(BC102PAE)
       m_CondExec 16,NE,BC102PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FIC                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC102PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BC102PAG
       ;;
(BC102PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.FBC100BP
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/BC102PAG.FBC100AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_337_17 337 CH 17
 /KEYS
   FLD_CH_337_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BC102PAH
       ;;
(BC102PAH)
       m_CondExec 00,EQ,BC102PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBC102S : MISE EN FORME DES DONNEES NASC POUR MAJ BASE CLIENT               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC102PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BC102PAJ
       ;;
(BC102PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ENTREE : FIC PROVENANT DE NASC                                       
       m_FileAssign -d SHR -g ${G_A2} FBC100 ${DATA}/PTEM/BC102PAG.FBC100AP
#                                                                              
# ****    ENTREE: FICHIER DATE                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****    ENTREE: FICHIER SOCIETE                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******  SORTIE : FICHIER VERS CLIREP                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FBC101 ${DATA}/PXX0/F07.FBC102AP
# ******  SORTIE : FICHIER VERS CLIC0P                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 750 -t LSEQ -g +1 FBC105 ${DATA}/PXX0/F07.FBC102BP
# ******  SORTIE : FICHIER DES CLIENTS EN ERREURS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -t LSEQ -g +1 FBC100E ${DATA}/PXX0/F07.FBC102CP
#                                                                              
# ******  ENTREE / SORTIE : TABLES EN LECTURE ET EN MISES A JOUR               
#    RTBC31   : NAME=RSBC31P,MODE=(U,I) - DYNAM=YES                            
# -X-RSBC31P  - IS UPDATED IN PLACE WITH MODE=(U,I)                            
       m_FileAssign -d SHR RTBC31 /dev/null
#    RTBC35   : NAME=RSBC35P,MODE=(U,I) - DYNAM=YES                            
# -X-RSBC35P  - IS UPDATED IN PLACE WITH MODE=(U,I)                            
       m_FileAssign -d SHR RTBC35 /dev/null
#                                                                              
#                                                                              
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BC102PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          

       m_ProgramExec -b BBC102S 
       JUMP_LABEL=BC102PAM
       ;;
(BC102PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# ******  SUPPRESSION DES FICHIERS                                             
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC102PAM.sysin
       m_UtilityExec
# *********************************                                            
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BC102PZA
       ;;
(BC102PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC102PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BC102PAN
       ;;
(BC102PAN)
       m_CondExec 16,NE,BC102PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
