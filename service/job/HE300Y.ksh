#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HE300Y.ksh                       --- VERSION DU 08/10/2016 22:17
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYHE300 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/06/11 AT 11.36.19 BY BURTEC7                      
#    STANDARDS: P  JOBSET: HE300Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BHE300  EXTRACTION STOCKS GHE - RACHAT DE PRODUIT éLECTRONIQU         
# ********************************************************************         
#   STEP DB2                                                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HE300YA
       ;;
(HE300YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=HE300YAA
       ;;
(HE300YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSPT01   : NAME=RSPT01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSHE10   : NAME=RSHE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSHE20   : NAME=RSHE20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE20 /dev/null
#    RSHE22   : NAME=RSHE22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE22 /dev/null
#    RSHE23   : NAME=RSHE23Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE23 /dev/null
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 434 -t LSEQ -g +1 FHE30S ${DATA}/PXX0/F45.FHE300AY
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE300 
       JUMP_LABEL=HE300YAB
       ;;
(HE300YAB)
       m_CondExec 04,GE,HE300YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
