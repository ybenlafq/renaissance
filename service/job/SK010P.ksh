#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SK010P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSK010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/12/10 AT 08.11.08 BY BURTECA                      
#    STANDARDS: P  JOBSET: SK010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER RECU DE IPO7 FNCKH EN INTERMEDIAIRE LRECL 40                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SK010PA
       ;;
(SK010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2013/12/10 AT 08.11.08 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: SK010P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'KAIDARA'                               
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SK010PAA
       ;;
(SK010PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEXP/F505.FNCKH
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SK010PAA.SK01CCPP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 07
 /FIELDS FLD_CH_8_3 08 CH 03
 /FIELDS FLD_CH_1_40 01 CH 40
 /FIELDS FLD_CH_21_5 21 CH 05
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_21_5 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_40
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SK010PAB
       ;;
(SK010PAB)
       m_CondExec 00,EQ,SK010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSK010                                                                      
#  EXTRACTION D UN FICHIER EN  55 DE LONG                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAD
       ;;
(SK010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
# ****** FICHIER DE IPO7                                                       
       m_FileAssign -d SHR -g ${G_A1} FNCKH ${DATA}/PTEM/SK010PAA.SK01CCPP
# ****** FICHIER FKAIH                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 55 -t LSEQ -g +1 FKAIH ${DATA}/PTEM/SK010PAD.SK01DDPP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSK010 
       JUMP_LABEL=SK010PAE
       ;;
(SK010PAE)
       m_CondExec 04,GE,SK010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RECU DE IPO7 SK00PK   NAME=FNCKP VERS FNCKPT (181)           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAG
       ;;
(SK010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEXP/F505.FNCPK
       m_FileAssign -d NEW,CATLG,DELETE -r 181 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SK010PAG.SK00BBPP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_7 57 CH 07
 /FIELDS FLD_CH_67_7 67 CH 07
 /FIELDS FLD_CH_54_3 54 CH 03
 /FIELDS FLD_CH_1_7 01 CH 07
 /FIELDS FLD_CH_1_181 01 CH 181
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_54_3 ASCENDING,
   FLD_CH_57_7 ASCENDING,
   FLD_CH_67_7 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 181 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_181
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SK010PAH
       ;;
(SK010PAH)
       m_CondExec 00,EQ,SK010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RECU DE IPO7 SK00BPK   NAME=FNCKP VERS FNCKF                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAJ
       ;;
(SK010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEXP/F505.FNCPK
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.KAIDARA.FNCKF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_77_2 77 CH 02
 /FIELDS FLD_CH_104_5 104 CH 05
 /FIELDS FLD_CH_109_20 109 CH 20
 /FIELDS FLD_CH_54_3 54 CH 03
 /KEYS
   FLD_CH_54_3 ASCENDING,
   FLD_CH_104_5 ASCENDING,
   FLD_CH_77_2 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 030 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_54_3,FLD_CH_104_5,FLD_CH_77_2,FLD_CH_109_20
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SK010PAK
       ;;
(SK010PAK)
       m_CondExec 00,EQ,SK010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RECU DE IPO7 SK00BPK POUR FNCKM                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAM
       ;;
(SK010PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEXP/F505.FNCPK
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.KAIDARA.FNCKM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_84_20 84 CH 20
 /FIELDS FLD_CH_77_2 77 CH 02
 /FIELDS FLD_CH_79_5 79 CH 05
 /FIELDS FLD_CH_54_3 54 CH 03
 /KEYS
   FLD_CH_54_3 ASCENDING,
   FLD_CH_79_5 ASCENDING,
   FLD_CH_77_2 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_54_3,FLD_CH_79_5,FLD_CH_77_2,FLD_CH_84_20
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SK010PAN
       ;;
(SK010PAN)
       m_CondExec 00,EQ,SK010PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTREAVE POUR METTRE EN FORME LE FICHER POUR KAIDARA * FKAIP *            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAQ PGM=EZTPA00    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAQ
       ;;
(SK010PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c X SYSPRINT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A2} FILEA ${DATA}/PTEM/SK010PAG.SK00BBPP
       m_FileAssign -d SHR -g ${G_A3} FILEB ${DATA}/PTEM/SK010PAD.SK01DDPP
# *****   FICHIER A DESTINATION DE KESA                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 FILEC ${DATA}/PXX0/F07.KAIDARA.FKAIP
       m_OutputAssign -c T IMPRIM
       m_OutputAssign -c T SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/SK010PAQ
       m_ProgramExec SK010PAQ
# ********************************************************************         
#  DELETE DU FICHIER NON GDG                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAT
       ;;
(SK010PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SK010PAU
       ;;
(SK010PAU)
       m_CondExec 16,NE,SK010PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AJOUT  DU STEP D UNLOAD LE 30 01 2012                                       
#  REPRISE : OUI MAIS AU STEP PRECEDENT C'EST _A DIRE AU DELETE                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SK010PAX
       ;;
(SK010PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
#    RSGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.BSK010FP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ************************************************                             
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BSK010FP                                                            
# SEND PART=XFBPRO,                                                            
#      IDF=SK00DDPP                                                            
# SEND PART=XFBPRO,                                                            
#      IDF=SK00EEPP                                                            
# SEND PART=XFBPRO,                                                            
#      IDF=SK00FFPP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSK010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBA
       ;;
(SK010PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SK010PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBD
       ;;
(SK010PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSK010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBG
       ;;
(SK010PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SK010PBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBJ
       ;;
(SK010PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSK010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBM
       ;;
(SK010PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SK010PBQ PGM=EZACFSM1   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBQ
       ;;
(SK010PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSK010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SK010PBT PGM=FTP        ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBT
       ;;
(SK010PBT)
       m_CondExec ${EXACS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBT.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SK010PBX PGM=EZACFSM1   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=SK010PBX
       ;;
(SK010PBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PBX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SK010PZA
       ;;
(SK010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SK010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
