#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB070P.ksh                       --- VERSION DU 08/10/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.47.41 BY BURTEC6                      
#    STANDARDS: P  JOBSET: RB070P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRB070 :TRAITEMENT DES DEMANDES DE FERRAILLAGE � SAGEM                      
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB070PA
       ;;
(RB070PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=RB070PAA
       ;;
(RB070PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#    RSRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB01 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE A TRAITER/SOCIETE SAGEM                                      
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RB070PAA
#                                                                              
# ********* FICHIER � ENVOYER VIA GATEWAY � EDI.DINAN@SAGEM.COM                
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FRB070 ${DATA}/PXX0/F07.FRB070
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB070 
       JUMP_LABEL=RB070PAB
       ;;
(RB070PAB)
       m_CondExec 04,GE,RB070PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DES FICHIERS VERS SAGEM                                               
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FRB070AP,                                                           
#      FNAME=":FRB070AP""(0)                                                   
#          DATAEND                                                             
# *******************************************************************          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB070P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB070PAD PGM=FTP        ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB070PAD
       ;;
(RB070PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB070PAD.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB070PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB070PAG
       ;;
(RB070PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB070PAG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
