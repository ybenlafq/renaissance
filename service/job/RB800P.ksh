#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB800P.ksh                       --- VERSION DU 08/10/2016 13:59
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB800 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/03/22 AT 11.07.21 BY BURTEC7                      
#    STANDARDS: P  JOBSET: RB800P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRB800 :SUIVI QUALITE  DES VENTES DE PRESTATION D ENERGIE                   
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RB800PA
       ;;
(RB800PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2011/03/22 AT 11.07.21 BY BURTEC7                
# *    JOBSET INFORMATION:    NAME...: RB800P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'SUIVI QUALITE'                         
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RB800PAA
       ;;
(RB800PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE M.A.J                                                           
#    RSGV08   : NAME=RSGV08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV08 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV31 /dev/null
#                                                                              
# ****** FIC DE SORTIE  (LRECL 250)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FRB800 ${DATA}/PXX0/F07.BRB800AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB800 
       JUMP_LABEL=RB800PAB
       ;;
(RB800PAB)
       m_CondExec 04,GE,RB800PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
