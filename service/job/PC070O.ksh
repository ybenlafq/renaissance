#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC070O.ksh                       --- VERSION DU 08/10/2016 21:58
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPC070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/10/11 AT 14.57.08 BY BURTEC7                      
#    STANDARDS: P  JOBSET: PC070O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EXTRACTION CARTES CADEAUX TABLES GV(1 PASSAGE POUR PARIS ET FILIALE         
#  LA CHAINE PARISIENNE TRAITERA EN FIN L ENSEMBLE DES FICHIERS(FILIAL         
#  POUR CREER UN FICHIER UNIQUE A DESTINATION DE LA GATEWAY                    
#  PGM BPC070                                                                  
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PC070OA
       ;;
(PC070OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PC070OAA
       ;;
(PC070OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA22   : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSPR00   : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PC070OAA
#                                                                              
# ******* FICHIER FCCUTIS DU JOUR (LRECL 500)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 FCCUTIS ${DATA}/PTEM/PC070OAA.FPC070AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC070 
       JUMP_LABEL=PC070OAB
       ;;
(PC070OAB)
       m_CondExec 04,GE,PC070OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION CARTES CADEAUX TABLES VE (1 PASSAGE POUR PARIS ET FILIAL         
#  PGM BPC071                                                                  
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PC070OAD
       ;;
(PC070OAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSVE10   : NAME=RSVE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE11   : NAME=RSVE11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSVE14   : NAME=RSVE14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14 /dev/null
#    RSVE02   : NAME=RSVE02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE02 /dev/null
#    RSGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA56   : NAME=RSGA56O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA56 /dev/null
#    RSPR00   : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PC070OAD
#                                                                              
# ******* FICHIER FCCUTIS DU JOUR (LRECL 500)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 FCCUTIS ${DATA}/PTEM/PC070OAD.FPC071AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC071 
       JUMP_LABEL=PC070OAE
       ;;
(PC070OAE)
       m_CondExec 04,GE,PC070OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT MERGE COPY DES DEUX FICHIERS OBTENUS                                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC070OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PC070OAG
       ;;
(PC070OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PC070OAA.FPC070AO
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PC070OAD.FPC071AO
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 SORTOUT ${DATA}/PXX0/F16.FPC071BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PC070OAH
       ;;
(PC070OAH)
       m_CondExec 00,EQ,PC070OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PC070OZA
       ;;
(PC070OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC070OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
