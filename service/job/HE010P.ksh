#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HE010P.ksh                       --- VERSION DU 17/10/2016 18:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPHE010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/16 AT 11.19.09 BY BURTEC2                      
#    STANDARDS: P  JOBSET: HE010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BHE802 : ENCOURS GHE (EQUIVALENT DE L'ETAT TP => JHE801 )                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=HE010PA
       ;;
(HE010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/06/16 AT 11.19.09 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: HE010P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ETATS STOCK'                           
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=HE010PAA
       ;;
(HE010PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN MAJ                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSHE10   : NAME=RSHE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHE10 /dev/null
# *****   FICHIER EN ENTR�E                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FHE800 ${DATA}/PXX0/F07.FHE802AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE802 
       JUMP_LABEL=HE010PAB
       ;;
(HE010PAB)
       m_CondExec 04,GE,HE010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE920 : TABLEAU DE SUIVI (EQUIVALENT DE L'ETAT TP => JHE921 )              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAD
       ;;
(HE010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN MAJ                                                        
#    RSHE10   : NAME=RSHE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSHE23   : NAME=RSHE23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHE23 /dev/null
# *****   FICHIER EN ENTR�E                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FHE920 ${DATA}/PXX0/F07.FHE920AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE920 
       JUMP_LABEL=HE010PAE
       ;;
(HE010PAE)
       m_CondExec 04,GE,HE010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHC300 : ENCOURS PRODUITS 020 (EQUIVALENT DE L'ETAT TP => JHC031 )          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAG
       ;;
(HE010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN MAJ                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSHC00   : NAME=RSHC00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC00 /dev/null
#    RSHC01   : NAME=RSHC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC01 /dev/null
# *****   FICHIER EN ECRITURE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FHC300 ${DATA}/PXX0/F07.FHC300AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHC300 
       JUMP_LABEL=HE010PAH
       ;;
(HE010PAH)
       m_CondExec 04,GE,HE010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHC500 : APPAREILS TERMIN�S NON SORTIS POUR 020                             
#         :          => (EQUIVALENT DE L'ETAT TP => JHC051 )                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAJ
       ;;
(HE010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN MAJ                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSHC00   : NAME=RSHC00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC00 /dev/null
# *****   FICHIER EN ECRITURE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FHC500 ${DATA}/PXX0/F07.FHC500AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHC500 
       JUMP_LABEL=HE010PAK
       ;;
(HE010PAK)
       m_CondExec 04,GE,HE010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHC600 : ENCOURS PHYSIQUE AAF (EQUIVALENT DE L'ETAT TP => JHC061 )          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAM
       ;;
(HE010PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN MAJ                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSHC00   : NAME=RSHC00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHC00 /dev/null
# *****   FICHIER EN ECRITURE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FHC600 ${DATA}/PXX0/F07.FHC600AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHC600 
       JUMP_LABEL=HE010PAN
       ;;
(HE010PAN)
       m_CondExec 04,GE,HE010PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  GENERATION SYSIN ZIP                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAQ
       ;;
(HE010PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/CORTEX4.P.MTXTFIX1/HE010PAQ
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FHE010BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /DERIVEDFIELD FLD_CH_1_19 '//DD:DD1 -n JHE801_' 
 /DERIVEDFIELD FLD_CH_1_5  '.CSV\n' 
 /DERIVEDFIELD FLD_CH_2_19 '//DD:DD2 -n JHE921_' 
 /DERIVEDFIELD FLD_CH_2_5  '.CSV\n' 
 /DERIVEDFIELD FLD_CH_3_19 '//DD:DD3 -n JHC031_' 
 /DERIVEDFIELD FLD_CH_3_5  '.CSV\n' 
 /DERIVEDFIELD FLD_CH_4_19 '//DD:DD4 -n JHC051_' 
 /DERIVEDFIELD FLD_CH_4_5  '.CSV\n' 
 /DERIVEDFIELD FLD_CH_5_19 '//DD:DD5 -n JHC061_'
 /DERIVEDFIELD FLD_CH_5_5  '.CSV' 
 /FIELDS FLD_CH_1_8 1 8 
 /FIELDS FLD_CH_2_8 1 8 
 /FIELDS FLD_CH_3_8 1 8 
 /FIELDS FLD_CH_4_8 1 8 
 /FIELDS FLD_CH_5_8 1 8 
 /COPY 
 /REFORMAT FLD_CH_1_19,FLD_CH_1_8,FLD_CH_1_5, 
           FLD_CH_2_19,FLD_CH_2_8,FLD_CH_2_5, 
           FLD_CH_3_19,FLD_CH_3_8,FLD_CH_3_5, 
           FLD_CH_4_19,FLD_CH_4_8,FLD_CH_4_5, 
           FLD_CH_5_19,FLD_CH_5_8,FLD_CH_5_5 
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HE010PAR
       ;;
(HE010PAR)
       m_CondExec 00,EQ,HE010PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAT PGM=JVMLDM76   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAT
       ;;
(HE010PAT)
       m_CondExec ${EXABE},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F07.FHE802AP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.HE10ZIPP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HE010PAT.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTHE010P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=HE010PAX
       ;;
(HE010PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HE010PAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/HE010PAX.FTHE010P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTHE010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HE010PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=HE010PBA
       ;;
(HE010PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/HE010PBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.HE010PAX.FTHE010P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=HE010PZA
       ;;
(HE010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HE010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
