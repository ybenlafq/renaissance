#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB060P.ksh                       --- VERSION DU 09/10/2016 05:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB060 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.12.22 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB060P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RB060PA
       ;;
(RB060PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 15.12.21 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: RB060P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CLIENTS DARTYBOX'                      
# *                           APPL...: REPPARIS                                
# *                           WAIT...: RB060P                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RB060PAA
       ;;
(RB060PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***********************************                                          
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g -1 SORTIN ${DATA}/PXX0/FTP.F07.BRB060AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.BRB060AP
# ******* MERGE DES FICHIERS                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 1-112 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BRB060BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_5_1 1 CH 1
 /FIELDS FLD_CH_5_16 1 CH 16
 /CONDITION CND_1 FLD_CH_5_1 EQ CST_1_4 
 /KEYS
   FLD_CH_5_16 ASCENDING
 /OMIT CND_1
 /* Record Type = V  Record Length = 108 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB060PAB
       ;;
(RB060PAB)
       m_CondExec 00,EQ,RB060PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    BRB060                                                                    
#  INTEGRATION DONNEES DARTYBOX DANS LA VENTE                                  
#  REPRISE : OUI                                                               
#  RSGV10 EN MAJ MAIS MAIS CODE EN MODE I POUR DEMARR� EN TP OUVERT            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB060PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB060PAD
       ;;
(RB060PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** FIC DARTYBOX MERG�S                                                   
       m_FileAssign -d SHR -g ${G_A1} FRB060 ${DATA}/PXX0/F07.BRB060BP
#                                                                              
#  RSGV10 EN MAJ MAIS MAIS CODE EN MODE I POUR DEMARR� EN TP OUVERT            
# ******  TABLE EN LECTURE                                                     
#    RSGV10   : NAME=RSGV10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV10 /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB060 
       JUMP_LABEL=RB060PAE
       ;;
(RB060PAE)
       m_CondExec 04,GE,RB060PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REMISE A ZERO DES DEUX G�N�RATIONS                                         
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB060PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB060PAG
       ;;
(RB060PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   FIC INTERFACE GCT                                                   
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-112 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.BRB060AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-112 -t LSEQ -g ${G_A2} OUT2 ${DATA}/PXX0/FTP.F07.BRB060AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB060PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB060PAH
       ;;
(RB060PAH)
       m_CondExec 16,NE,RB060PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DE TOPRBP ( TOPRBP = MISE TERMIN�E SOUS PLAN DE RB061         
#   ET RB060P SI PAS DE FICHIERS DARTBOX RE�US. )                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB060PAJ PGM=CZX2PTRT   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
