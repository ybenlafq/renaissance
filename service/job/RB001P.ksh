#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB001P.ksh                       --- VERSION DU 08/10/2016 12:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 14.56.25 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BRB000 : TRADUCTION DU FICHIER SAGEM CSV AU FORMAT                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RB001PA
       ;;
(RB001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 14.56.25 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: RB001P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXTRAC DONNES SAGEM'                   
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RB001PAA
       ;;
(RB001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#  DEPENDANCE HORAIRE 22 H 00 ET 5 H 00                                        
# **************************************                                       
# ****** FIC D ENTREE (RECUP CFT) EN PROVENANCE DE SAGEM VIA XFB               
       m_FileAssign -d SHR FRB001 ${DATA}/PXX0/FTP.F07.SAGEM01P
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB000 ${DATA}/PXX0/F07.BRB001AP
       m_ProgramExec BRB000 
#                                                                              
# ********************************************************************         
#   PGM BRB000 : TRADUCTION DU FICHIER BEWAN CSV AU FORMAT                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB001PAD PGM=BRB000     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAD
       ;;
(RB001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FIC D ENTREE (RECUP CFT) EN PROVENANCE DE BEWAN VIA XFB               
       m_FileAssign -d SHR FRB001 ${DATA}/PXX0/FTP.F07.BEWAN01P
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB000 ${DATA}/PXX0/F07.BRB001EP
       m_ProgramExec BRB000 
#                                                                              
# ********************************************************************         
#   PGM BRB000 : TRADUCTION DU FICHIER NC                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB001PAG PGM=BRB000     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAG
       ;;
(RB001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FIC D ENTREE (RECUP CFT) EN PROVENANCE DE NC VIA XFB                  
       m_FileAssign -d SHR FRB001 ${DATA}/PXX0/FTP.F07.NC00001P
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB000 ${DATA}/PXX0/F07.BRB001FP
       m_ProgramExec BRB000 
#                                                                              
# ********************************************************************         
#   PGM BRB000 : TRADUCTION DU FICHIER NETGEAR                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB001PAJ PGM=BRB000     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAJ
       ;;
(RB001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FIC D ENTREE (RECUP CFT) EN PROVENANCE DE NC VIA XFB                  
       m_FileAssign -d SHR FRB001 ${DATA}/PXX0/FTP.F07.NETGEA1P
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB000 ${DATA}/PXX0/F07.BRB001GP
       m_ProgramExec BRB000 
#                                                                              
# *******************************************************************          
#  TRI DU FICHIER SORTANT DE BRB000                                            
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB001PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAM
       ;;
(RB001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE SAGEM                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BRB001AP
# *****   FICHIER ENTREE BEWAN                                                 
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F07.BRB001EP
# *****   FICHIER ENTREE NC                                                    
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PXX0/F07.BRB001FP
# *****   FICHIER ENTREE NETGEAR                                               
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PXX0/F07.BRB001GP
# *****   FICHIER REPRISE  J(-1)                                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRB001DP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/B0001PAD.BRB001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /FIELDS FLD_CH_179_13 179 CH 13
 /FIELDS FLD_CH_71_15 71 CH 15
 /FIELDS FLD_CH_136_15 136 CH 15
 /FIELDS FLD_CH_249_10 249 CH 10
 /KEYS
   FLD_CH_249_10 ASCENDING,
   FLD_CH_1_25 ASCENDING,
   FLD_CH_136_15 ASCENDING,
   FLD_CH_179_13 ASCENDING,
   FLD_CH_71_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB001PAN
       ;;
(RB001PAN)
       m_CondExec 00,EQ,RB001PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB001 : INTEGRATION DU FICHIER DANS LES BASES                          
#               ET FORMATTAGE D UN FICHIER ANOMALIES A RECYCLER                
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB001PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAQ
       ;;
(RB001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE : FICHIER TRIE DU BRB000                                      
       m_FileAssign -d SHR -g ${G_A5} FRB000 ${DATA}/PTEM/B0001PAD.BRB001BP
#                                                                              
# ******  SORTIE : FICHIER ANOMALIES                                           
# *FRBANO   FILE  NAME=BRB001CP,MODE=O                                         
       m_OutputAssign -c 9 -w FRBANO01 FRBANO
#                                                                              
# ******  SORTIE : NOUVEAU FICHIER POUR LE LENDEMAIN                           
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB001 ${DATA}/PXX0/F07.BRB001DP
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB00 /dev/null
#    RTRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB001 
       JUMP_LABEL=RB001PAR
       ;;
(RB001PAR)
       m_CondExec 04,GE,RB001PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FICHIERS                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB001PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAT
       ;;
(RB001PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB001PAT.sysin
       m_UtilityExec
# ******************************************************************           
#  CREATION _A VIDE DES FICHER SAGEM BEWAN NUMERICABLE NETGEAR                  
#  REPRISE : OUI                                                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB001PAU
       ;;
(RB001PAU)
       m_CondExec 16,NE,RB001PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RB001PAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RB001PAX
       ;;
(RB001PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   FIC VENTES                                                          
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.SAGEM01P
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/FTP.F07.BEWAN01P
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/FTP.F07.NC00001P
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/FTP.F07.NETGEA1P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB001PAX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB001PAY
       ;;
(RB001PAY)
       m_CondExec 16,NE,RB001PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB001PZA
       ;;
(RB001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
