#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM061M.ksh                       --- VERSION DU 08/10/2016 12:56
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMNM061 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/08 AT 14.41.44 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM061M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FOCNTION DE LA CARTE FCFONC FIC STOCK ET STAT          
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM061MA
       ;;
(NM061MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM061MAA
       ;;
(NM061MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/NM061MAA
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/NM061MAA
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 19                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 FTS061 ${DATA}/PXX0/F89.BNM161AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=NM061MAB
       ;;
(NM061MAB)
       m_CondExec 04,GE,NM061MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM104 : MISE A JOUR DE LA RTGV10 EN METTANT A BLANC LES ZONES NSOC         
#        ET NLIEUP POUR INDIQUER QUE LA VENTE EST _A NOUVEAU DISPONIBLE         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM061MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM061MAD
       ;;
(NM061MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE ARTICLES                                                       
#    RSGV10   : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******  FICHIER DES TOPES LIVRES DU JOUR LG 19                               
       m_FileAssign -d SHR -g ${G_A1} FTS061 ${DATA}/PXX0/F89.BNM161AM
#                                                                              
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM104 
       JUMP_LABEL=NM061MAE
       ;;
(NM061MAE)
       m_CondExec 04,GE,NM061MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
