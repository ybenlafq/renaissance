#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC049P.ksh                       --- VERSION DU 08/10/2016 22:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC049 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/08/20 AT 15.54.46 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PC049P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPC049 ACTIVATION DES CARTES DANS SAP                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PC049PA
       ;;
(PC049PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PC049PAA
       ;;
(PC049PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPC01   : NAME=RSPC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
#    RSPC06   : NAME=RSPC06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC06 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ******  TABLE EN M.A.J.                                                      
#    RSPC01   : NAME=RSPC01,MODE=U - DYNAM=YES                                 
# -X-RSPC01   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSPC01 /dev/null
# ******  FIC EN SORTIE FFTI00                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PXX0/F07.FPC049AP
# ******  FIC EN SORTIE FFTI01                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PXX0/F07.FPC049BP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FFMICRO ${DATA}/PXX0/F07.FPC049CP
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC049 
       JUMP_LABEL=PC049PAB
       ;;
(PC049PAB)
       m_CondExec 04,GE,PC049PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# AAF      STEP  PGM=IKJEFT01                                                  
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *******  TABLE EN LECTURE                                                    
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# RSPT01   FILE  DYNAM=YES,NAME=RSPT01,MODE=I                                  
# ******  TABLE EN M.A.J.                                                      
# RSPC01   FILE  DYNAM=YES,NAME=RSPC01,MODE=U                                  
# ******  FIC EN SORTIE                                                        
# FFTI00   FILE  NAME=FPC050AP,MODE=O                                          
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
# FDATE    DATA  CLASS=VAR,PARMS=FDATE                                         
#                                                                              
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BPC050) PLAN(BPC050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
