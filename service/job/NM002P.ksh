#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM002P.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNM002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/03/02 AT 11.19.32 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM002PA
       ;;
(NM002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM002PAA
       ;;
(NM002PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
#    RECUP FICHIER CAISSES MQ                                                  
# ***************************************                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QNM002P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/NM002PAA
       m_ProgramExec IEFBR14 "RDAR,NM002P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NM002PAD
       ;;
(NM002PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QNM002P
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=NM002PAE
       ;;
(NM002PAE)
       m_CondExec 00,EQ,NM002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER ISSU DU BNM100 REMONTEE MQ ( CODE S55 )              
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAG
       ;;
(NM002PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM155AP
# ******* FIC DE REPRISE DES CAISSES EN ANO (J-1)                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNM001BP
# ******* FIC DE CUMUL EN CAS DE PLANTAGE DU TRAITEMENT LA VEILLE              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNMCUMAP
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM002PAG.BNM155BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PAH
       ;;
(NM002PAH)
       m_CondExec 00,EQ,NM002PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI2 *  R�CUP�RATION DU TYPE='0' (PAIEMENTS)                                
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAJ
       ;;
(NM002PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NM002PAG.BNM155BP
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM002PAJ.BNM155CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "0"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_35_8 35 CH 8
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 DESCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PAK
       ;;
(NM002PAK)
       m_CondExec 00,EQ,NM002PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI3 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAM
       ;;
(NM002PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NM002PAG.BNM155BP
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM002PAM.BNM155DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "1"
 /DERIVEDFIELD CST_3_14 "BQ"
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_35_8 35 CH 8
 /FIELDS FLD_CH_44_2 44 CH 2
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 AND FLD_CH_44_2 NE CST_3_14 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PAN
       ;;
(NM002PAN)
       m_CondExec 00,EQ,NM002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI4 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******            ET REMISE EN BANQUE, OUTREC DE BQ* EN BQUE                
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAQ
       ;;
(NM002PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NM002PAG.BNM155BP
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM002PAQ.BNM155HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11  "BQE"
 /DERIVEDFIELD CST_1_9 "BQ"
 /FIELDS FLD_CH_44_2 44 CH 2
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_43 1 CH 43
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_47_154 47 CH 154
 /CONDITION CND_1 FLD_CH_44_2 EQ CST_1_9 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_44_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_43,CST_1_11,FLD_CH_47_154
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=NM002PAR
       ;;
(NM002PAR)
       m_CondExec 00,EQ,NM002PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DES FICHIERS TYPE '0' ET TYPE '1                                
#  TRI5 *  PAR N�DE PI�CES.                                                    
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAT
       ;;
(NM002PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC TYPE 0 ET  1                                                     
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/NM002PAQ.BNM155HP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/NM002PAJ.BNM155CP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/NM002PAM.BNM155DP
# ******* FIC LOG DE CAISSES TRI� PAR N�DE PI�CE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM002PAT.BNM155EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_5 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PAU
       ;;
(NM002PAU)
       m_CondExec 00,EQ,NM002PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI PAR SOCIETE                                                     
#  TRI5 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
# ABJ      SORT  RSTRT=SAME                                                    
# ******* FIC LOG DE CAISSES TRI� PAR N�DE PI�CE                               
# SORTIN   FILE  NAME=BNM155EP,MODE=I                                          
# ******* FIC LOG DE CAISSES TRI� PAR SOCIETE                                  
# SORTOUT  FILE  NAME=BNM155FP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(20,3,A),FORMAT=CH                                              
#  RECORD TYPE=F,LENGTH=200                                                    
#          DATAEND                                                             
# ********************************************************************         
#  BNM000 : CE PGM SE CHARGE D'ECRIRE LE FICHIER INTERFACE STANDARD            
#           FFTI00 AU FUR ET A MESURE DES DONN�ES DE GESTION CONTENUES         
#           DANS LE FICHIER LOG DE CAISSES FNM001.                             
#           *                                                                  
#           **** VERIFICATION DU TRAITEMENT DEJA EFFECTUE DANS RTHV15          
#           *                                                                  
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NM002PAX
       ;;
(NM002PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE (SOUS TABLES NM000/NM001/NM002/NM003               
# *****                      NM004/NM005/NM006/NM007/NM008/BQMAG)              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# *****   TABLE DES ECS                                                        
#    RSFX00   : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00 /dev/null
# ************** TABLES EN MISE A JOUR ***********                             
# *****   TABLE CONTROLE DES REMONTEES DE CAISSES                              
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
# -X-NM002PR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV15 /dev/null
# *****                                                                        
#    RSGV14   : NAME=RSGV14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A8} FNM001 ${DATA}/PTEM/NM002PAT.BNM155EP
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -g +1 FFTI00 ${DATA}/PTEM/NM002PAX.BNM000AP
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -g +1 FFTI02 ${DATA}/PTEM/NM002PAX.BNM000GP
# ******  FICHIER DES CAISSES EN ERREUR                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -g +1 FREPRIS ${DATA}/PTEM/NM002PAX.BNM000BP
# ******  FICHIER POUR GENERATEUR D'ETAT (LISTE DE L'ETAT DES CAISSES)         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM001 ${DATA}/PTEM/NM002PAX.BNM000CP
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w INM000 IMPRIM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******  PARAMETRE : SIMU ==> AUCUNE M.A.J                                    
# ******  PARAMETRE : REEL ==> M.A.J                                           
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/NM002PAX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM000 
       JUMP_LABEL=NM002PAY
       ;;
(NM002PAY)
       m_CondExec 04,GE,NM002PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM001 : EXTRACTION DES CAISSES A COMPTABILISER                             
#           CREATION DU FICHIER POUR L'I.C.S  (INTERFACE COMPTABLE STD         
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBA PGM=BNM001     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBA
       ;;
(NM002PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **************  FICHIERS EN ENTREE ***************                           
#                                                                              
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A9} FNM001 ${DATA}/PTEM/NM002PAT.BNM155EP
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d SHR -g ${G_A10} FFTI00 ${DATA}/PTEM/NM002PAX.BNM000AP
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d SHR -g ${G_A11} FFTI02 ${DATA}/PTEM/NM002PAX.BNM000GP
# ******* FICHIER CAISSES + CAISSES A REPRENDRE DE J(-1)                       
       m_FileAssign -d SHR -g ${G_A12} FREPRIS ${DATA}/PTEM/NM002PAX.BNM000BP
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
#                                                                              
# ******  FICHIER POUR EDITION VERS CHAINE EDITION NM005*                      
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FNM002 ${DATA}/PNCGP/F07.BNM001AP
# ******  FICHIER DES CAISSES A REPRENDRE POUR J(+1)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FNM003 ${DATA}/PNCGP/F07.BNM001BP
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 FFTI01 ${DATA}/PNCGP/F07.BNM001CP
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT DACEM SEULEMENT                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 FFTI03 ${DATA}/PNCGP/F07.BNM001DP
       m_ProgramExec BNM001 
#                                                                              
# ********************************************************************         
#         TRI PREPARATOIRE POUR ETAT INM001                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBD
       ;;
(NM002PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER POUR LISTE DES CAISSES RECYCLEES                             
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/NM002PAX.BNM000CP
# ******  FICHIER DESTINE AU GENERATEUR D'ETAT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM002PBD.BNM000DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_21_3 21 CH 03
 /FIELDS FLD_BI_15_3 15 CH 03
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_24_2 24 CH 02
 /FIELDS FLD_BI_18_3 18 CH 03
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PBE
       ;;
(NM002PBE)
       m_CondExec 00,EQ,NM002PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050    CREATION D'UN FICHIER FCUMULS                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBG
       ;;
(NM002PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG00 /dev/null
#    RSEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG05 /dev/null
#    RSEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG10 /dev/null
#    RSEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG15 /dev/null
#    RSEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG25 /dev/null
#    RSEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/NM002PBD.BNM000DP
# ******  FICHIER FCUMULS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM002PBG.BNM000EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM002PBH
       ;;
(NM002PBH)
       m_CondExec 04,GE,NM002PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBJ
       ;;
(NM002PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/NM002PBG.BNM000EP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM002PBJ.BNM000FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002PBK
       ;;
(NM002PBK)
       m_CondExec 00,EQ,NM002PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   REPRISE : OUI         ETAT DES CAISSES EN RECYCLAGE                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBM
       ;;
(NM002PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/NM002PBD.BNM000DP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/NM002PBJ.BNM000FP
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE TRAITEE : 907                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  EDITION                                                              
       m_OutputAssign -c 9 -w IEE200 FEDITION
# /FEDITION  DD SYSOUT=*                                                       
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM002PBN
       ;;
(NM002PBN)
       m_CondExec 04,GE,NM002PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002PBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NM002PBQ
       ;;
(NM002PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE FV001)               
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT1 ${DATA}/PNCGP/F07.BNMCUMAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002PBQ.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM002PBR
       ;;
(NM002PBR)
       m_CondExec 16,NE,NM002PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM002PZA
       ;;
(NM002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
