#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SB100P.ksh                       --- VERSION DU 08/10/2016 12:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSB100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 16.38.00 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SB100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSB100 :      MISE � JOUR ADRESSE MAIL RTGV03                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SB100PA
       ;;
(SB100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SB100PAA
       ;;
(SB100PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN LECTURE                                                        
#    RTPT01   : NAME=RSPT01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPT01 /dev/null
#    RTGV08   : NAME=RSGV02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV08 /dev/null
#    RTGV10   : NAME=RSGV03,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ********* FICHIER EN ENTREE                                                  
       m_FileAssign -d SHR -g +0 FDERTRTE ${DATA}/PXX0/F07.FSB100AP
# ********* FICHIER WORK                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDERTRTS ${DATA}/PXX0/F07.FSB100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FSB100S ${DATA}/PXX0/F07.FSB100BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB100 
       JUMP_LABEL=SB100PAB
       ;;
(SB100PAB)
       m_CondExec 04,GE,SB100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BSB110 : RECHERCHE ADRESEE MAIL NON BLACK LIST�ES                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB100PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SB100PAD
       ;;
(SB100PAD)
       m_CondExec ${EXAAF},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A1} FSB110E ${DATA}/PXX0/F07.FSB100BP
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FSB110S ${DATA}/PXX0/F07.FSB110AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB110 
       JUMP_LABEL=SB100PAE
       ;;
(SB100PAE)
       m_CondExec 04,GE,SB100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BSB120 : MISE _A JOUR RTGV03                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB100PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SB100PAG
       ;;
(SB100PAG)
       m_CondExec ${EXAAK},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN M_AJ                                                            
#    RTGV03   : NAME=RSGV03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV03 /dev/null
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d SHR -g ${G_A2} FSB120E ${DATA}/PXX0/F07.FSB110AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB120 
       JUMP_LABEL=SB100PAH
       ;;
(SB100PAH)
       m_CondExec 04,GE,SB100PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
