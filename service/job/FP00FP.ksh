#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FP00FP.ksh                       --- VERSION DU 08/10/2016 12:54
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFP00F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/06/18 AT 17.24.32 BY BURTECA                      
#    STANDARDS: P  JOBSET: FP00FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  PGM : BFP000  CREATION DU FICHIER DES FACTURES A REGLER                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FP00FPA
       ;;
(FP00FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FP00FPAA
       ;;
(FP00FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFP02P  : NAME=RSFP02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFP02P /dev/null
#    RSFP03   : NAME=RSFP03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFP03 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE EN MAJ                                                         
#    RSFP01P  : NAME=RSFP01P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFP01P /dev/null
# ******* FICHIER FLOT EN ENTREE                                               
       m_FileAssign -d SHR -g +0 FLOTI ${DATA}/PMACP/F07.BFP000AP
# ******* FICHIER FLOT EN SORTIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FLOTO ${DATA}/PMACP/F07.BFP000AP
# ******* FICHIER DES FACTURES DU MOIS - VERS PACIFICA                         
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 FFP000 ${DATA}/PMACP/F07.BFP000BP
# ******* FICHIER POUR EDITION ETAT FACTURES ENVOYEES A ASSUREUR               
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFP050 ${DATA}/PTEM/FP00FPAA.BFP050AP
# ******* FIC COMPTE RENDU EXECUTION AVEC LISTE FACTURES NON ENVOYES           
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FFPCR1 ${DATA}/PMACP/F07.BFP000CP
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER PARAM PACIF                                                  
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/PACIF
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP000 
       JUMP_LABEL=FP00FPAB
       ;;
(FP00FPAB)
       m_CondExec 04,GE,FP00FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER A EDITER                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP00FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FP00FPAD
       ;;
(FP00FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FP00FPAA.BFP050AP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FP00FPAD.BFP050BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_13_15 13 CH 15
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP00FPAE
       ;;
(FP00FPAE)
       m_CondExec 00,EQ,FP00FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFP050  CREATION ETAT EOS                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP00FPAG PGM=BFP050     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FP00FPAG
       ;;
(FP00FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FFP050 ${DATA}/PTEM/FP00FPAD.BFP050BP
# ******* ETAT EOS - ETAT DE CTL DES FACTURES SELECT POUR RGLT PACIFIC         
       m_OutputAssign -c 9 -w JFP050 JFP050
       m_ProgramExec BFP050 
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER ARTICLE VIA CFT                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFP000BP,                                                           
#      FNAME=":BFP000BP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  PGM WAITSTOP POUR PLAN POUR AVOIR LA DUREE EXACTE DU TRANSFERT              
#  LIBER� PAR LA PROC DE FIN DE TRANSFERT CFT (=> TRANSFERT OK)                
# ********************************************************************         
#  SUPPRIMER SUITE PASSAGE EN FTP                                              
# ********************************************************************         
# AAU      STEP  PGM=WAITSTOP,TIME=1440                                        
# //SYSPRINT DD SYSOUT=*                                                       
# //SYSOUT   DD SYSOUT=*                                                       
#                                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFP00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP00FPAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FP00FPAJ
       ;;
(FP00FPAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FP00FPAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP FP00FPAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FP00FPAM
       ;;
(FP00FPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP00FPAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FP00FPZA
       ;;
(FP00FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP00FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
