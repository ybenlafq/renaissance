#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FP160P.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFP160 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/19 AT 15.44.28 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FP160P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  PGM : BFP160 CREATION DU FICHIER DES FACTURES A REGLER                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FP160PA
       ;;
(FP160PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FP160PAA
       ;;
(FP160PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSFP01   : NAME=RSFP01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFP01 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV08   : NAME=RSGV08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV08 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR53   : NAME=RSPR53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR53 /dev/null
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FFP01 ${DATA}/PTEM/FP160PAA.NFP160AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FFP02 ${DATA}/PTEM/FP160PAA.NFP160BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FFP03 ${DATA}/PTEM/FP160PAA.NFP160CP
# ******* FICHIER                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP160 
       JUMP_LABEL=FP160PAB
       ;;
(FP160PAB)
       m_CondExec 04,GE,FP160PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FFP01                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP160PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FP160PAD
       ;;
(FP160PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FP160PAA.NFP160AP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FP160PAD.NFP160DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_28_20 28 CH 20
 /FIELDS FLD_CH_10_10 10 CH 10
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_28_20 ASCENDING,
   FLD_CH_10_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP160PAE
       ;;
(FP160PAE)
       m_CondExec 00,EQ,FP160PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FFP02                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP160PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FP160PAG
       ;;
(FP160PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FP160PAA.NFP160BP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FP160PAG.NFP160EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_98_20 98 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_9_10 09 CH 10
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_98_20 ASCENDING,
   FLD_CH_9_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP160PAH
       ;;
(FP160PAH)
       m_CondExec 00,EQ,FP160PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFP170 GENERATION FICHIER SINISTRE PACIFICA                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP160PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FP160PAJ
       ;;
(FP160PAJ)
       m_CondExec ${EXAAP},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ******* TABLES EN ECRITURE                                                   
#    RSFP01   : NAME=RSFP01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFP01 /dev/null
#    RSFP02   : NAME=RSFP02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFP02 /dev/null
#    RSFP03   : NAME=RSFP03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFP03 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A3} FFP01 ${DATA}/PTEM/FP160PAD.NFP160DP
       m_FileAssign -d SHR -g ${G_A4} FFP02 ${DATA}/PTEM/FP160PAG.NFP160EP
       m_FileAssign -d SHR -g ${G_A5} FFP03 ${DATA}/PTEM/FP160PAA.NFP160CP
# ******* FICHIER EN ECRITURE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 191 -t LSEQ -g +1 FRENDU ${DATA}/PXX0/F07.NFP160FP
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP170 
       JUMP_LABEL=FP160PAK
       ;;
(FP160PAK)
       m_CondExec 04,GE,FP160PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTFP160P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP160PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FP160PAM
       ;;
(FP160PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP160PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FP160PAM.FTFP160P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFP160P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP160PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FP160PAQ
       ;;
(FP160PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FP160PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FP160PAM.FTFP160P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FP160PZA
       ;;
(FP160PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP160PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
