#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MC027O.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POMC027 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/23 AT 17.23.11 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MC027O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BQC260                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MC027OA
       ;;
(MC027OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=MC027OAA
       ;;
(MC027OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
# *******                                                                      
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV03   : NAME=RSGV03O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03 /dev/null
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSPM06   : NAME=RSPM06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM06 /dev/null
# *******                                                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FMC027 ${DATA}/PXX0/F16.FMC027AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC027 
       JUMP_LABEL=MC027OAB
       ;;
(MC027OAB)
       m_CondExec 04,GE,MC027OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
