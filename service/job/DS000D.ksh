#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  DS000D.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDDS000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/01 AT 18.00.01 BY PREPA2                       
#    STANDARDS: P  JOBSET: DS000D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  LOAD DES DONNEES DE LA TABLE RTDS00 DE PARIS                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=DS000DA
       ;;
(DS000DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABX=${EXABX:-0}
       EXACC=${EXACC:-0}
       EXACH=${EXACH:-0}
       EXACM=${EXACM:-0}
       EXACR=${EXACR:-0}
       EXACW=${EXACW:-0}
       EXADB=${EXADB:-0}
       EXADG=${EXADG:-0}
       EXADL=${EXADL:-0}
       EXADQ=${EXADQ:-0}
       EXADV=${EXADV:-0}
       EXAEA=${EXAEA:-0}
       EXAEF=${EXAEF:-0}
       EXAEK=${EXAEK:-0}
       EXAEP=${EXAEP:-0}
       EXAEU=${EXAEU:-0}
       EXAEZ=${EXAEZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=DS000DAA
       ;;
(DS000DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSDS00D  : NAME=RSDS00D,MODE=(U,N) - DYNAM=YES                            
# -X-RSDS00D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSDS00D /dev/null
# ******* FIC DE LOAD DE RTDS00                                                
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.DS00UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DS000DAA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/DS000D_DS000DAA_RTDS00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=DS000DAB
       ;;
(DS000DAB)
       m_CondExec 04,GE,DS000DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DES DONNEES DE LA TABLE RTDS05 DE PARIS                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAD
       ;;
(DS000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSDS05D  : NAME=RSDS05D,MODE=(U,N) - DYNAM=YES                            
# -X-RSDS05D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSDS05D /dev/null
# ******* FIC DE LOAD DE RTDS05                                                
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.DS05UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DS000DAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/DS000D_DS000DAD_RTDS05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=DS000DAE
       ;;
(DS000DAE)
       m_CondExec 04,GE,DS000DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DES DONNEES DE LA TABLE RTDS10 DE PARIS                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAG
       ;;
(DS000DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE                                                                
#    RSDS10D  : NAME=RSDS10D,MODE=(U,N) - DYNAM=YES                            
# -X-RSDS10D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSDS10D /dev/null
# ******* FIC DE LOAD DE RTDS10                                                
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.DS10UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DS000DAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/DS000D_DS000DAG_RTDS10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=DS000DAH
       ;;
(DS000DAH)
       m_CondExec 04,GE,DS000DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDS000                                                                
# ********************************************************************         
#  EXTRACTION DES ARTICLES DEPRECIES                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAJ
       ;;
(DS000DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTVA05   : NAME=RSVA05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   TABLES EN MAJ                                                        
#    RTDS05   : NAME=RSDS05D,MODE=(U,U) - DYNAM=YES                            
# -X-RSDS05D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RTDS05 /dev/null
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDS000 ${DATA}/PTEM/DS000DAJ.FDS000AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDS000 
       JUMP_LABEL=DS000DAK
       ;;
(DS000DAK)
       m_CondExec 04,GE,DS000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FDS000AD ENTRANT DANS PGM BDS005                             
#  SORT FIELDS (4,5,A,9,7,A)                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAM
       ;;
(DS000DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/DS000DAJ.FDS000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/DS000DAM.FDS000BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_5 4 CH 5
 /FIELDS FLD_CH_9_7 9 CH 7
 /KEYS
   FLD_CH_4_5 ASCENDING,
   FLD_CH_9_7 ASCENDING
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DAN
       ;;
(DS000DAN)
       m_CondExec 00,EQ,DS000DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDS005                                                                
# ********************************************************************         
#  CREATION DES FICHIERS ENTRANT DANS LE GENERATEUR D'ETAT                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAQ
       ;;
(DS000DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA05   : NAME=RSVA05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTDS00   : NAME=RSDS00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTDS00 /dev/null
#    RTDS10   : NAME=RSDS10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTDS10 /dev/null
# *****   TABLES EN MAJ                                                        
#    RTDS05   : NAME=RSDS05D,MODE=(U,U) - DYNAM=YES                            
# -X-RSDS05D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RTDS05 /dev/null
# *****   FICHIER D'EXTARCTION TRIE                                            
       m_FileAssign -d SHR -g ${G_A2} FDS005 ${DATA}/PTEM/DS000DAM.FDS000BD
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER POUR ETAT IDS000                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC0 ${DATA}/PTEM/DS000DAQ.IDS000AD
# *****   FICHIER POUR ETAT IDS005                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC5 ${DATA}/PTEM/DS000DAQ.IDS005AD
# *****   FICHIER POUR ETAT IDS000                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRA20 ${DATA}/PTEM/DS000DAQ.IDS020AD
# *****   FICHIER POUR ETAT IDS005                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRA25 ${DATA}/PTEM/DS000DAQ.IDS025AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDS005 
       JUMP_LABEL=DS000DAR
       ;;
(DS000DAR)
       m_CondExec 04,GE,DS000DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IDS025AR POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAT
       ;;
(DS000DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/DS000DAQ.IDS025AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DAT.IDS025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_13_3 13 PD 3
 /FIELDS FLD_BI_16_17 16 CH 17
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_17 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DAU
       ;;
(DS000DAU)
       m_CondExec 00,EQ,DS000DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=DS000DAX
       ;;
(DS000DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/DS000DAT.IDS025BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/DS000DAX.IDS025CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=DS000DAY
       ;;
(DS000DAY)
       m_CondExec 04,GE,DS000DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBA
       ;;
(DS000DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/DS000DAX.IDS025CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DBA.IDS025DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DBB
       ;;
(DS000DBB)
       m_CondExec 00,EQ,DS000DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IDS025 (PREMIER ETAT DE DEPRECIATION STOCK)              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBD
       ;;
(DS000DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/DS000DAT.IDS025BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FCUMULS ${DATA}/PTEM/DS000DBA.IDS025DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IDS025                                                          
       m_OutputAssign -c 9 -w IDS025 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=DS000DBE
       ;;
(DS000DBE)
       m_CondExec 04,GE,DS000DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IDS000AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBG PGM=SORT       ** ID=ABX                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBG
       ;;
(DS000DBG)
       m_CondExec ${EXABX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/DS000DAQ.IDS000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DBG.IDS000BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_16_17 16 CH 17
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_13_3 13 PD 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_17 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DBH
       ;;
(DS000DBH)
       m_CondExec 00,EQ,DS000DBG ${EXABX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBJ PGM=IKJEFT01   ** ID=ACC                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBJ
       ;;
(DS000DBJ)
       m_CondExec ${EXACC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/DS000DBG.IDS000BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/DS000DBJ.IDS000CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=DS000DBK
       ;;
(DS000DBK)
       m_CondExec 04,GE,DS000DBJ ${EXACC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBM PGM=SORT       ** ID=ACH                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBM
       ;;
(DS000DBM)
       m_CondExec ${EXACH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/DS000DBJ.IDS000CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DBM.IDS000DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DBN
       ;;
(DS000DBN)
       m_CondExec 00,EQ,DS000DBM ${EXACH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IDS000 (PREMIER ETAT DE DEPRECIATION STOCK)              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBQ PGM=IKJEFT01   ** ID=ACM                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBQ
       ;;
(DS000DBQ)
       m_CondExec ${EXACM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/DS000DBG.IDS000BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/DS000DBM.IDS000DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IDS000                                                          
       m_OutputAssign -c 9 -w IDS000 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=DS000DBR
       ;;
(DS000DBR)
       m_CondExec 04,GE,DS000DBQ ${EXACM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IDS005AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBT PGM=SORT       ** ID=ACR                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBT
       ;;
(DS000DBT)
       m_CondExec ${EXACR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/DS000DAQ.IDS005AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DBT.IDS005BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_PD_13_3 13 PD 3
 /FIELDS FLD_BI_16_17 16 CH 17
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_17 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DBU
       ;;
(DS000DBU)
       m_CondExec 00,EQ,DS000DBT ${EXACR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DBX PGM=IKJEFT01   ** ID=ACW                                   
# ***********************************                                          
       JUMP_LABEL=DS000DBX
       ;;
(DS000DBX)
       m_CondExec ${EXACW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/DS000DBT.IDS005BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/DS000DBX.IDS005CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=DS000DBY
       ;;
(DS000DBY)
       m_CondExec 04,GE,DS000DBX ${EXACW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCA PGM=SORT       ** ID=ADB                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCA
       ;;
(DS000DCA)
       m_CondExec ${EXADB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/DS000DBX.IDS005CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DCA.IDS005DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DCB
       ;;
(DS000DCB)
       m_CondExec 00,EQ,DS000DCA ${EXADB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IDS005 (PREMIER ETAT DE DEPRECIATION STOCK)              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCD PGM=IKJEFT01   ** ID=ADG                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCD
       ;;
(DS000DCD)
       m_CondExec ${EXADG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/DS000DBT.IDS005BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/DS000DCA.IDS005DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IDS005                                                          
       m_OutputAssign -c 9 -w IDS005 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=DS000DCE
       ;;
(DS000DCE)
       m_CondExec 04,GE,DS000DCD ${EXADG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IDS000AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCG PGM=SORT       ** ID=ADL                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCG
       ;;
(DS000DCG)
       m_CondExec ${EXADL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/DS000DAQ.IDS020AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DCG.IDS020BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_13_3 13 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_16_17 16 CH 17
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_17 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DCH
       ;;
(DS000DCH)
       m_CondExec 00,EQ,DS000DCG ${EXADL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCJ PGM=IKJEFT01   ** ID=ADQ                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCJ
       ;;
(DS000DCJ)
       m_CondExec ${EXADQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/DS000DCG.IDS020BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/DS000DCJ.IDS020CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=DS000DCK
       ;;
(DS000DCK)
       m_CondExec 04,GE,DS000DCJ ${EXADQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCM PGM=SORT       ** ID=ADV                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCM
       ;;
(DS000DCM)
       m_CondExec ${EXADV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/DS000DCJ.IDS020CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DCM.IDS020DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DCN
       ;;
(DS000DCN)
       m_CondExec 00,EQ,DS000DCM ${EXADV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IDS020 (PREMIER ETAT DE DEPRECIATION STOCK)              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCQ PGM=IKJEFT01   ** ID=AEA                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCQ
       ;;
(DS000DCQ)
       m_CondExec ${EXAEA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/DS000DCG.IDS020BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A22} FCUMULS ${DATA}/PTEM/DS000DCM.IDS020DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IDS020                                                          
       m_OutputAssign -c 9 -w IDS020 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=DS000DCR
       ;;
(DS000DCR)
       m_CondExec 04,GE,DS000DCQ ${EXAEA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDS010                                                                
# ********************************************************************         
#  CREATION D'UN  FICHIER ENTRANT DANS LE GENERATEUR D'ETAT                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCT PGM=IKJEFT01   ** ID=AEF                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCT
       ;;
(DS000DCT)
       m_CondExec ${EXAEF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA05   : NAME=RSVA05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTDS00   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTDS00 /dev/null
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER POUR ETAT IDS010                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IDS010 ${DATA}/PTEM/DS000DCT.IDS010AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDS010 
       JUMP_LABEL=DS000DCU
       ;;
(DS000DCU)
       m_CondExec 04,GE,DS000DCT ${EXAEF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IDS010AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DCX PGM=SORT       ** ID=AEK                                   
# ***********************************                                          
       JUMP_LABEL=DS000DCX
       ;;
(DS000DCX)
       m_CondExec ${EXAEK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/DS000DCT.IDS010AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DCX.IDS010BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_102_3 102 PD 3
 /FIELDS FLD_PD_105_3 105 PD 3
 /FIELDS FLD_PD_94_8 94 PD 8
 /FIELDS FLD_PD_86_8 86 PD 8
 /FIELDS FLD_BI_7_21 7 CH 21
 /KEYS
   FLD_BI_7_21 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_86_8,
    TOTAL FLD_PD_94_8,
    TOTAL FLD_PD_102_3,
    TOTAL FLD_PD_105_3
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DCY
       ;;
(DS000DCY)
       m_CondExec 00,EQ,DS000DCX ${EXAEK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DDA PGM=IKJEFT01   ** ID=AEP                                   
# ***********************************                                          
       JUMP_LABEL=DS000DDA
       ;;
(DS000DDA)
       m_CondExec ${EXAEP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PTEM/DS000DCX.IDS010BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/DS000DDA.IDS010CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=DS000DDB
       ;;
(DS000DDB)
       m_CondExec 04,GE,DS000DDA ${EXAEP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DDD PGM=SORT       ** ID=AEU                                   
# ***********************************                                          
       JUMP_LABEL=DS000DDD
       ;;
(DS000DDD)
       m_CondExec ${EXAEU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/DS000DDA.IDS010CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/DS000DDD.IDS010DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DS000DDE
       ;;
(DS000DDE)
       m_CondExec 00,EQ,DS000DDD ${EXAEU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IDS000 (PREMIER ETAT DE DEPRECIATION STOCK)              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DS000DDG PGM=IKJEFT01   ** ID=AEZ                                   
# ***********************************                                          
       JUMP_LABEL=DS000DDG
       ;;
(DS000DDG)
       m_CondExec ${EXAEZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A26} FEXTRAC ${DATA}/PTEM/DS000DCX.IDS010BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A27} FCUMULS ${DATA}/PTEM/DS000DDD.IDS010DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IDS010                                                          
       m_OutputAssign -c 9 -w IDS010 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=DS000DDH
       ;;
(DS000DDH)
       m_CondExec 04,GE,DS000DDG ${EXAEZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=DS000DZA
       ;;
(DS000DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DS000DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
