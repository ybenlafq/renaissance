#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QHE03O.ksh                       --- VERSION DU 08/10/2016 12:56
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POQHE03 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/03 AT 15.14.24 BY BURTEC6                      
#    STANDARDS: P  JOBSET: QHE03O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  REQUETE QCHSVAL                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QHE03OA
       ;;
(QHE03OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=QHE03OAA
       ;;
(QHE03OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QCHSVAL DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN ADMFIL.QCHSVAL (&&SOC='$MGIODEP_1_3' FORM=ADMFIL.FCHSVAL
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE03O1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE03OAB
       ;;
(QHE03OAB)
       m_CondExec 04,GE,QHE03OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REQUETE QCHSVALD                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE03OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QHE03OAD
       ;;
(QHE03OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QCHSVALD DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QCHSVALD (&&SOC='$MGIODEP_1_3' FORM=ADMFIL.FCHSVALD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE03O2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE03OAE
       ;;
(QHE03OAE)
       m_CondExec 04,GE,QHE03OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
