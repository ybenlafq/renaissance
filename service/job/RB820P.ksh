#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB820P.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB820 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 10.32.25 BY BURTEC6                      
#    STANDARDS: P  JOBSET: RB820P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRBS20 :GENERATION SYSIN POUR UNLOAD                                        
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB820PA
       ;;
(RB820PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RB820PAA
       ;;
(RB820PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV10   : NAME=RSGV10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV10D  : NAME=RSGV10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV10L  : NAME=RSGV10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10L /dev/null
#    RSGV10M  : NAME=RSGV10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSGV10O  : NAME=RSGV10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV10Y  : NAME=RSGV10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10Y /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#   TABLES EN LECTURE                                                          
# ****** SYSIN EN ENTREE                                                       
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB820PAA.sysin
#                                                                              
# ****** FIC DE SORTIE  (LRECL 250)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPCSYS ${DATA}/PXX0/F07.BRBS20AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRBS20 
       JUMP_LABEL=RB820PAB
       ;;
(RB820PAB)
       m_CondExec 04,GE,RB820PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FASTUNLOAD : UNLOAD DES DONN�ES                                             
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB820PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB820PAD
       ;;
(RB820PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
# ****** TABLE EN LECTURE                                                      
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
#    RSGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02L /dev/null
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
#    RSGV05P  : NAME=RSGV05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05P /dev/null
#    RSGV05D  : NAME=RSGV05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05D /dev/null
#    RSGV05L  : NAME=RSGV05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05L /dev/null
#    RSGV05M  : NAME=RSGV05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05M /dev/null
#    RSGV05O  : NAME=RSGV05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05O /dev/null
#    RSGV05Y  : NAME=RSGV05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV05Y /dev/null
#    RSGV08   : NAME=RSGV08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV08 /dev/null
#    RSGV08D  : NAME=RSGV08D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08D /dev/null
#    RSGV08L  : NAME=RSGV08L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08L /dev/null
#    RSGV08M  : NAME=RSGV08M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08M /dev/null
#    RSGV08O  : NAME=RSGV08O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08O /dev/null
#    RSGV08Y  : NAME=RSGV08Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08Y /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV10L  : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10L /dev/null
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV31 /dev/null
#    RSGV31D  : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31D /dev/null
#    RSGV31L  : NAME=RSGV31L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31L /dev/null
#    RSGV31M  : NAME=RSGV31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31M /dev/null
#    RSGV31O  : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31O /dev/null
#    RSGV31Y  : NAME=RSGV31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31Y /dev/null
#                                                                              
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.BRB820AP
#                                                                              
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSTSPRT
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PXX0/F07.BRBS20AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

#                                                                              
# ********************************************************************         
#  BRB820  : FORMATTAGE DES DONN�ES                                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB820PAG PGM=BRB820     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB820PAG
       ;;
(RB820PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** FIC EN ENTREE                                                         
       m_FileAssign -d SHR -g ${G_A2} FEXTRACE ${DATA}/PXX0/F07.BRB820AP
#                                                                              
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FEXTRACS ${DATA}/PXX0/F07.BRB820BP
       m_ProgramExec BRB820 
#                                                                              
# YSTSIN  DATA  *,CLASS=FIX2                                                   
# DSN SYSTEM(RDAR)                                                             
# RUN PROGRAM(BRB800) PLAN(BRB800)                                             
# END                                                                          
#         DATAEND                                                              
# ********************************************************************         
#  ENVOI VERS  "\\S4I600\50-SUIVIPRODUCTION\VENTES\ENERGIE"                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BRB820BP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTRB820P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB820PAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB820PAJ
       ;;
(RB820PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB820PAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/RB820PAJ.FTRB820P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB820P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB820PAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB820PAM
       ;;
(RB820PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB820PAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.RB820PAJ.FTRB820P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB820PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB820PAQ
       ;;
(RB820PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB820PAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB820PZA
       ;;
(RB820PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB820PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
