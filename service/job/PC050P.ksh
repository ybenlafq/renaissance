#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC050P.ksh                       --- VERSION DU 09/10/2016 00:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/11/14 AT 14.38.06 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PC050P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPC050 ACTIVATION DES CARTES DANS SAP                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PC050PA
       ;;
(PC050PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PC050PAA
       ;;
(PC050PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPC01   : NAME=RSPC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
#    RSPC06   : NAME=RSPC06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC06 /dev/null
#    RSPC11   : NAME=RSPC11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC11 /dev/null
#    RSPC12   : NAME=RSPC12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC12 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ******  TABLE EN M.A.J.                                                      
#    RSPC02   : NAME=RSPC02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC02 /dev/null
# ******  FIC EN SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PXX0/F07.FPC050AP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PXX0/F07.FPC050BP
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FFMICRO ${DATA}/PXX0/F07.FPC050CP
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC050 
       JUMP_LABEL=PC050PAB
       ;;
(PC050PAB)
       m_CondExec 04,GE,PC050PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
