#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB067P.ksh                       --- VERSION DU 08/10/2016 15:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB067 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.47.33 BY BURTEC6                      
#    STANDARDS: P  JOBSET: RB067P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRB030  : CLIENTS DARTYBOX A FACTURER                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB067PA
       ;;
(RB067PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=RB067PAA
       ;;
(RB067PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RB067PAA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******                                                                       
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV10D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV10L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10L /dev/null
#    RSGV10M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSGV10O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV10Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
# ******                                                                       
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******                                                                       
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******                                                                       
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
# ******                                                                       
#    RSRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB01 /dev/null
# *******  MAJ TABLE ARTICLE INFOCENTRE                                        
#    RSEE98   : NAME=RSEE98,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSEE98 /dev/null
# ******  MAJ TABLE DES VENTES POUR TOUTES FILIALES                            
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#                                                                              
# *****   FICHIER FACTURES CLIENTS A ENVOYER VIA LA GATEWAY                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1-304 -t LSEQ -g +1 FRB067 ${DATA}/PXX0/F99.BRB067AP
# **  FICHIER VENTE DARTYBOX EN RM NON TOPE NON FACTURES PAR HIGHDEAL          
       m_FileAssign -d NEW,CATLG,DELETE -r 1-150 -t LSEQ -g +1 FRB065 ${DATA}/PXX0/F99.BRB067BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB067 
       JUMP_LABEL=RB067PAB
       ;;
(RB067PAB)
       m_CondExec 04,GE,RB067PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY DBOXGWQ (PROD)                                   
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BRB067AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY DBOXGWQ (PRE-PROD)                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPPRO,                                                           
#      IDF=BRB067AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB067P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB067PAD PGM=FTP        ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB067PAD
       ;;
(RB067PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB067PAD.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB067PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB067PAG
       ;;
(RB067PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB067PAG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
