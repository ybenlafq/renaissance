#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SB020P.ksh                       --- VERSION DU 08/10/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSB020 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/01/05 AT 10.58.01 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SB020P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BBS030 : PGM DE PURGE                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SB020PA
       ;;
(SB020PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       RUN=${RUN}
       JUMP_LABEL=SB020PAA
       ;;
(SB020PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN LECTURE                                                        
#    RTBS01   : NAME=RSBS01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTPT99   : NAME=RSPT01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPT99 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# *** TABLE EN MAJ                                                             
#    RTBS01   : NAME=RSBS01,MODE=(U,REST=NO) - DYNAM=YES                       
# -X-RSBS01   - IS UPDATED IN PLACE WITH MODE=(U,REST=NO)                      
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTBS02   : NAME=RSBS02,MODE=(U,REST=NO) - DYNAM=YES                       
# -X-RSBS02   - IS UPDATED IN PLACE WITH MODE=(U,REST=NO)                      
       m_FileAssign -d SHR RTBS02 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB030 
       JUMP_LABEL=SB020PAB
       ;;
(SB020PAB)
       m_CondExec 04,GE,SB020PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BBS040 : ECRITURE D UN �TAT QUI AFFICHE LES LIGNE GV EN ERREUR         
#   PAR RAPPORT � LA COMMANDE INITIALE                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB020PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SB020PAD
       ;;
(SB020PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN LECTURE                                                        
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTBS01   : NAME=RSBS01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTBS02   : NAME=RSBS02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTBS02 /dev/null
#    RTGV31   : NAME=RSGV31,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTPR00   : NAME=RSPR00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPT99   : NAME=RSPT01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPT99 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.SB0002BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F07.SB0002AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSB040 
       JUMP_LABEL=SB020PAE
       ;;
(SB020PAE)
       m_CondExec 04,GE,SB020PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTXXXXXX                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB020PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SB020PAG
       ;;
(SB020PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SB020PAG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SB020PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SB020PAJ
       ;;
(SB020PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 ${DATA}/CORTEX4.P.MTXTFIX2/SB020PAJ
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.SB0002BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SB020PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SB020PAK
       ;;
(SB020PAK)
       m_CondExec 16,NE,SB020PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
