#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP000P.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/05/06 AT 15.52.34 BY BURTECO                      
#    STANDARDS: P  JOBSET: AP000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  FICHIER CUMUL DES FNM001 ISSUE DE TOUTES LES FILIALES.                      
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP000PA
       ;;
(AP000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=AP000PAA
       ;;
(AP000PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM155AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNM155AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BNM155AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.BNM155AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.BNM155AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM155AD
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PNCGP/F07.AP000PAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_13_30 "VER"
 /DERIVEDFIELD CST_21_46 "0"
 /DERIVEDFIELD CST_9_22 "VEG"
 /DERIVEDFIELD CST_5_14 "VEB"
 /DERIVEDFIELD CST_17_38 "SA"
 /DERIVEDFIELD CST_15_34 "2"
 /DERIVEDFIELD CST_1_6 "6"
 /DERIVEDFIELD CST_11_26 "3"
 /DERIVEDFIELD CST_7_18 "3"
 /DERIVEDFIELD CST_19_42 "O"
 /DERIVEDFIELD CST_3_10 "3"
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_20_9 20 CH 9
 /FIELDS FLD_CH_107_1 107 CH 1
 /FIELDS FLD_CH_44_2 44 CH 2
 /CONDITION CND_1 FLD_CH_47_1 EQ CST_1_6 OR FLD_CH_47_1 EQ CST_3_10 AND FLD_CH_44_3 EQ CST_5_14 OR FLD_CH_47_1 EQ CST_7_18 AND FLD_CH_44_3 EQ CST_9_22 OR FLD_CH_47_1 EQ CST_11_26 AND FLD_CH_44_3 EQ CST_13_30 OR FLD_CH_47_1 EQ CST_15_34 AND FLD_CH_44_2 EQ CST_17_38 AND FLD_CH_107_1 EQ CST_19_42 AND FLD_CH_43_1 EQ CST_21_46 
 /KEYS
   FLD_CH_20_9 ASCENDING,
   FLD_CH_1_16 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=AP000PAB
       ;;
(AP000PAB)
       m_CondExec 00,EQ,AP000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP000  : RECUPERATION DE NOUVELLE RECEPTION, ATTRIBUTION DES NUMER         
#          : D'IMMO AUX EQUIPEMENTS CONCERNéES.                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP000PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=AP000PAD
       ;;
(AP000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER CUMUL DES FNM001 ISSUE DE TOUTES LES FILIALES.               
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PNCGP/F07.AP000PAA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP10   : NAME=RSAP10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP10 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP11   : NAME=RSAP11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP11 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP12   : NAME=RSAP12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP12 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP13   : NAME=RSAP13,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP13 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP000 
       JUMP_LABEL=AP000PAE
       ;;
(AP000PAE)
       m_CondExec 04,GE,AP000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
