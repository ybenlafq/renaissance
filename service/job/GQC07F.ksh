#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC07F.ksh                       --- VERSION DU 20/10/2016 15:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGQC07 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/09 AT 14.15.33 BY BURTECA                      
#    STANDARDS: P  JOBSET: GQC07F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GQC07FA
       ;;
(GQC07FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+4'}
       G_A11=${G_A11:-'+5'}
       G_A12=${G_A12:-'+5'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+3'}
       G_A8=${G_A8:-'+3'}
       G_A9=${G_A9:-'+4'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/06/09 AT 14.15.33 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GQC07F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'FUSION FIC LIBEL'                      
# *                           APPL...: REPEXPL                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GQC07FAA
       ;;
(GQC07FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FBQC250D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FBQC250L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FBQC250M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FBQC250O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FBQC250P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FBQC250Y
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC07FAA.FBQC250T
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_70 11 CH 70
 /FIELDS FLD_CH_1_9 1 CH 9
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_9 ASCENDING
 /* Record Type = F  Record Length = 80 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_8,FLD_CH_11_70
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC07FAB
       ;;
(GQC07FAB)
       m_CondExec 00,EQ,GQC07FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU MERGE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAD
       ;;
(GQC07FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GQC07FAA.FBQC250T
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FBQC250F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 80 */
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC07FAE
       ;;
(GQC07FAE)
       m_CondExec 00,EQ,GQC07FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAG
       ;;
(GQC07FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GQC07FAG.FTGQC07F
       m_ProgramExec EZACFSM1
# ******************************************************                       
# ** ZIP DU FICHIER POUR ENVOIS VERS TESSI                                     
# ******************************************************                       
# AAP      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ****** FICHIER EN ENTREE DE ZIP                                              
# DD1      FILE  NAME=FQC250TP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=GQC07ZIP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#         DATAEND                                                              
#          FILE  NAME=FTGQC07F,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAJ
       ;;
(GQC07FAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_FileAssign -d SHR -g +0 DD1 ${DATA}/PXX0/F07.FQC250TP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F99.GQC07ZIP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FAJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATTAGE SYSIN ENVOI FTP                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAM
       ;;
(GQC07FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GQC07FAG.FTGQC07F
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A4} SORTOUT ${DATA}/PTEM/GQC07FAG.FTGQC07F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_2_6  "PXX0.F99.GQC07ZIP(0)"
 /DERIVEDFIELD CST_4_8  " "
 /DERIVEDFIELD CST_BLANK_1_5  X"27"
 /DERIVEDFIELD CST_0_4  "PUT "
 /DERIVEDFIELD CST_BLANK_3_7  X"27"
 /DERIVEDFIELD CST_1_3 "CARTET"
 /FIELDS FLD_CH_2_6 2 CH 6
 /CONDITION CND_1 FLD_CH_2_6 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_4,CST_BLANK_1_5,CST_2_6,CST_BLANK_3_7,CST_4_8
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC07FAN
       ;;
(GQC07FAN)
       m_CondExec 00,EQ,GQC07FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTGQC07F POUR TESSI-GED                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAQ
       ;;
(GQC07FAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC07FAG.FTGQC07F(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGQC07F POUR TESSI-NCG                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAT
       ;;
(GQC07FAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC07FAG.FTGQC07F(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGQC07F                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FAX
       ;;
(GQC07FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A7} SYSOUT ${DATA}/PTEM/GQC07FAG.FTGQC07F
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGQC07F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FBA
       ;;
(GQC07FBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC07FAG.FTGQC07F(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGQC07F                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FBD
       ;;
(GQC07FBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A9} SYSOUT ${DATA}/PTEM/GQC07FAG.FTGQC07F
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGQC07F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FBG
       ;;
(GQC07FBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC07FAG.FTGQC07F(+4),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGQC07F                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FBJ
       ;;
(GQC07FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A11} SYSOUT ${DATA}/PTEM/GQC07FAG.FTGQC07F
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGQC07F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC07FBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FBM
       ;;
(GQC07FBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC07FAG.FTGQC07F(+5),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC07FZA
       ;;
(GQC07FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC07FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
