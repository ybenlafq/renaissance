#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC20Y.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGQC20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/21 AT 12.28.50 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC20Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FQC010 ISSU DU TRAITEMENT FS052Y                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC20YA
       ;;
(GQC20YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC20YAA
       ;;
(GQC20YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
# SORTIN   FILE  NAME=BQC010AY,MODE=I                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BQC145AY
#  FICHIER TRI�                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC20YAA.BQC160AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_80_1 80 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_80_1 DESCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC20YAB
       ;;
(GQC20YAB)
       m_CondExec 00,EQ,GQC20YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC160  EXTRACTION DES CARTES T VENTES LIVREES                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC20YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YAD
       ;;
(GQC20YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***** FIC TRIE                                                               
       m_FileAssign -d SHR -g ${G_A1} FQC010 ${DATA}/PTEM/GQC20YAA.BQC160AY
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES QUOTAS RESTANTS                                            
#    RSQC06   : NAME=RSQC06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSQC06 /dev/null
# ******* TABLE TOURNEES DE LIVRAISONS / GENERALITES TOURNEES                  
#    RSTL01   : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01 /dev/null
# ******* TABLE TOURNEES DE LIVRAISONS / TABLE DES LIVREURS                    
#    RSTL03   : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL03 /dev/null
# ******* TABLE DES QUOTAS DE LIVRAISONS                                       
#    RSGQ01   : NAME=RSGQ01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* TABLE DES CARTES T VENTES LIVREES                                    
#    RSQC01   : NAME=RSQC01Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC01 /dev/null
# ******* TABLE DES QUOTAS RESTANTS                                            
#    RSQC06   : NAME=RSQC06Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC06 /dev/null
# ******* TABLE AJOUTE LE 21 AOUT 2013                                         
#    RSGV02   : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSQC11   : NAME=RSQC11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC160 
       JUMP_LABEL=GQC20YAE
       ;;
(GQC20YAE)
       m_CondExec 04,GE,GQC20YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FQC011 ISSU DU TRAITEMENT FS052P                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC20YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YAG
       ;;
(GQC20YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BQC011AY
#  FICHIER TRI�                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC20YAG.BQC011BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_18_1 18 CH 1
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_18_1 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC20YAH
       ;;
(GQC20YAH)
       m_CondExec 00,EQ,GQC20YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FQC012 ISSU DU TRAITEMENT FS052Y                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC20YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YAJ
       ;;
(GQC20YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=BQC012AY,MODE=I                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BQC146AY
#  FICHIER TRI�                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC20YAJ.BQC012BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_89_1 89 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_18_1 18 CH 1
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_18_1 ASCENDING,
   FLD_CH_89_1 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC20YAK
       ;;
(GQC20YAK)
       m_CondExec 00,EQ,GQC20YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC170  EXTRACTION DES CARTES T VENTES LIVREES                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC20YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YAM
       ;;
(GQC20YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***** FIC TRIE                                                               
       m_FileAssign -d SHR -g ${G_A2} FQC011 ${DATA}/PTEM/GQC20YAG.BQC011BY
       m_FileAssign -d SHR -g ${G_A3} FQC012 ${DATA}/PTEM/GQC20YAJ.BQC012BY
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE TOURNEES DE LIVRAISONS / GENERALITES TOURNEES                  
#    RSTL01   : NAME=RSTL01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL01 /dev/null
# ******* TABLE TOURNEES DE LIVRAISONS / TABLE DES LIVREURS                    
#    RSTL03   : NAME=RSTL03Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL03 /dev/null
# ******* TABLE DES QUOTAS DE LIVRAISONS                                       
#    RSGQ01   : NAME=RSGQ01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* TABLE DES CARTES T VENTES LIVREES                                    
#    RSQC01   : NAME=RSQC01Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC01 /dev/null
# ******* TABLE AJOUTE LE 21 AOUT 2013                                         
#    RSGV02   : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSQC11   : NAME=RSQC11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC11 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC170 
       JUMP_LABEL=GQC20YAN
       ;;
(GQC20YAN)
       m_CondExec 04,GE,GQC20YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RAZ  FICHIER VENTES LIVREES CARTE.T                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC20YAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YAQ
       ;;
(GQC20YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN /dev/null
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT ${DATA}/PXX0/F45.BQC010AY
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F45.BQC011AY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F45.BQC012AY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC20YAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GQC20YAR
       ;;
(GQC20YAR)
       m_CondExec 16,NE,GQC20YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC20YZA
       ;;
(GQC20YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC20YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
