#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP001P.ksh                       --- VERSION DU 08/10/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/21 AT 10.02.09 BY BURTECA                      
#    STANDARDS: P  JOBSET: AP001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  FICHIER DES CRI TOP�S DANS NASC.                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP001PA
       ;;
(AP001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=AP001PAA
       ;;
(AP001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
# *****   FICHIER DE REMONT�E DES CRI DE NASC  ORIGINE AS400                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.FSLA2IP
       m_FileAssign -d NEW,CATLG,DELETE -r 277 -g +1 SORTOUT ${DATA}/PNCGP/F07.AP001PAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_147_14 147 CH 14
 /FIELDS FLD_BI_161_8 161 CH 8
 /FIELDS FLD_BI_1_17 1 CH 17
 /KEYS
   FLD_BI_1_17 ASCENDING,
   FLD_BI_161_8 ASCENDING,
   FLD_BI_147_14 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=AP001PAB
       ;;
(AP001PAB)
       m_CondExec 00,EQ,AP001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP001  : ALIMENTATION DES FACTURES A2I _A PARTIR DES                        
#          : R�ALISATIONS DE PRESTATIONS A2I                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP001PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=AP001PAD
       ;;
(AP001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TRI DU FICHIER DES CRI TOP�S PAR NASC                                
       m_FileAssign -d SHR -g ${G_A1} FAP001 ${DATA}/PNCGP/F07.AP001PAA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP12   : NAME=RSAP12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP12 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP17 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP001 
       JUMP_LABEL=AP001PAE
       ;;
(AP001PAE)
       m_CondExec 04,GE,AP001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
