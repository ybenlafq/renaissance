#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB110P.ksh                       --- VERSION DU 08/10/2016 13:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB110 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/09 AT 08.24.23 BY OPERATB                      
#    STANDARDS: P  JOBSET: RB110P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BRB110 : LECTURE DANS TABLE DES ENVOIS GEF(RTEF00)                      
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB110PA
       ;;
(RB110PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       RUN=${RUN}
       JUMP_LABEL=RB110PAA
       ;;
(RB110PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FPARAM DATE DU DERNIER TRAIT (JJMMSSAA)                               
       m_FileAssign -d OLD,KEEP,DELETE -g +0 FPARAM ${DATA}/PXX0/F07.DATB110P
# ****** CARTE FPARAM2                                                         
       m_FileAssign -d SHR FPARAM2 ${DATA}/CORTEX4.P.MTXTFIX1/RB110PAA
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
#                                                                              
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# ******  FICHIER A ENVOYER � SAGEM                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 484 -t LSEQ -g +1 FRB110 ${DATA}/PXX0/F07.BRB110AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB110 
       JUMP_LABEL=RB110PAB
       ;;
(RB110PAB)
       m_CondExec 04,GE,RB110PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB110 : LECTURE DANS TABLE DES ENVOIS GEF(RTEF00)                      
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB110PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAD
       ;;
(RB110PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FPARAM DATE DU DERNIER TRAIT (JJMMSSAA)                               
       m_FileAssign -d OLD,KEEP,DELETE -g +0 FPARAM ${DATA}/PXX0/F07.DATB111P
# ****** CARTE FPARAM2                                                         
       m_FileAssign -d SHR FPARAM2 ${DATA}/CORTEX4.P.MTXTFIX1/RB110PAD
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
#                                                                              
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# ******  FICHIER A ENVOYER � ANOVO                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 484 -t LSEQ -g +1 FRB110 ${DATA}/PXX0/F07.BRB110GP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB110 
       JUMP_LABEL=RB110PAE
       ;;
(RB110PAE)
       m_CondExec 04,GE,RB110PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB110 : LECTURE DANS TABLE DES ENVOIS GEF( RTEF00)                     
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB110PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAG
       ;;
(RB110PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FPARAM DATE DU DERNIER TRAIT (JJMMSSAA)                               
       m_FileAssign -d OLD,KEEP,DELETE -g +0 FPARAM ${DATA}/PXX0/F07.DATB111C
# ****** CARTE FPARAM2                                                         
       m_FileAssign -d SHR FPARAM2 ${DATA}/CORTEX4.P.MTXTFIX1/RB110PAG
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
#                                                                              
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# ******  FICHIER A ENVOYER � NAGRA                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 484 -t LSEQ -g +1 FRB110 ${DATA}/PXX0/F07.BRB110HP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB110 
       JUMP_LABEL=RB110PAH
       ;;
(RB110PAH)
       m_CondExec 04,GE,RB110PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB110 : LECTURE DANS TABLE DES ENVOIS                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB110PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAJ
       ;;
(RB110PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FPARAM DATE DU DERNIER TRAIT (JJMMSSAA)                               
       m_FileAssign -d OLD,KEEP,DELETE -g +0 FPARAM ${DATA}/PXX0/F07.DATB111D
# ****** CARTE FPARAM2                                                         
       m_FileAssign -d SHR FPARAM2 ${DATA}/CORTEX4.P.MTXTFIX1/RB110PAJ
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
#                                                                              
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# ******  FICHIER A ENVOYER � NC                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 484 -t LSEQ -g +1 FRB110 ${DATA}/PXX0/F07.BRB110IP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB110 
       JUMP_LABEL=RB110PAK
       ;;
(RB110PAK)
       m_CondExec 04,GE,RB110PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB110 : LECTURE DANS TABLE DES ENVOIS                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB110PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAM
       ;;
(RB110PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FPARAM DATE DU DERNIER TRAIT (JJMMSSAA)                               
       m_FileAssign -d OLD,KEEP,DELETE -g +0 FPARAM ${DATA}/PXX0/F07.DATB111E
# ****** CARTE FPARAM2                                                         
       m_FileAssign -d SHR FPARAM2 ${DATA}/CORTEX4.P.MTXTFIX1/RB110PAM
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
#                                                                              
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# ******  FICHIER A ENVOYER � NETGEAR                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 484 -t LSEQ -g +1 FRB110 ${DATA}/PXX0/F07.BRB110JP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB110 
       JUMP_LABEL=RB110PAN
       ;;
(RB110PAN)
       m_CondExec 04,GE,RB110PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTRB110P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#  AAZ      STEP  PGM=FTP,PARM='(EXIT'                                         
#                                                                              
#  SYSPRINT REPORT SYSOUT=*                                                    
#  OUTPUT   REPORT SYSOUT=*                                                    
#  //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                           
#  INPUT    DATA  *                                                            
#  10.135.2.114                                                                
#  IPO1GTW                                                                     
#  IPO1GTW                                                                     
#  LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                          
#  PUT 'PXX0.F07.BRB110AP(0)' BRB110AP                                         
#  QUIT                                                                        
#           DATAEND                                                            
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#  ABE      STEP  PGM=EZACFSM1                                                 
#  SYSOUT   FILE  NAME=FLOGFTP,MODE=N                                          
#  SYSIN    DATA  *                                                            
#  &LYYMMDD &LHR:&LMIN:&LSEC &JOBNAME PXX0.F07.BRB110AP(0)                     
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB110P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB110PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAQ
       ;;
(RB110PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PAQ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB110PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAT
       ;;
(RB110PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PAT.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB110P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB110PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RB110PAX
       ;;
(RB110PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PAX.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB110PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RB110PBA
       ;;
(RB110PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB110P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB110PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RB110PBD
       ;;
(RB110PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PBD.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# ********************************************************                     
#                                                                              
# ***********************************                                          
# *   STEP RB110PBG PGM=EZACFSM1   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RB110PBG
       ;;
(RB110PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB110PBG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
