#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC32P.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGQC32 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/09 AT 09.29.58 BY BURTECA                      
#    STANDARDS: P  JOBSET: GQC32P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNZIP DES FICHIERS RE�US DE TESSI                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAA      STEP  PGM=PKUNZIP                                                   
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# PRED     LINK  NAME=$EX010P,MODE=I                                           
# PRED     LINK  NAME=$EOS4CF,MODE=I                                           
# ***************************************                                      
# //STEPLIB  DD  DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                              
# //***      DD  DSN=SYS2.ZIP390.D21ZTAB,DISP=SHR                              
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=TESSIAP,MODE=I                                           
# ******* FICHIER EN SORTIE DE ZIP                                             
# TARGET   FILE  NAME=FQC032XP,MODE=O                                          
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ARCHIVE_INFILE(DD1)                                                         
# -EXTRACT                                                                     
# -OUTFILE(TARGET)                                                             
# -TRAN(ASCII850)                                                              
#          DATAEND                                                             
# *******************************************************************          
#  JAVAUNZIP  FICHIER HRV7                                                     
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC32PA
       ;;
(GQC32PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC32PAA
       ;;
(GQC32PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 DD1 ${DATA}/PXX0/FTP.F99.TESSIAP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC32PAA.sysin
       m_ProgramExec JVMLDM76_UNZIP.ksh
# ********************************************************************         
#  ECLATEMENT DES FICHIERS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC32PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC32PAD
       ;;
(GQC32PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GQC32PAA.FQC032XP
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F91.FQC032AD
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F61.FQC032AL
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F89.FQC032AM
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF4 ${DATA}/PXX0/F16.FQC032AO
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF5 ${DATA}/PXX0/F07.FQC032AP
       m_FileAssign -d NEW,CATLG,DELETE -r 916 -t LSEQ -g +1 SORTOF6 ${DATA}/PXX0/F45.FQC032AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "991"
 /DERIVEDFIELD CST_1_18 "945"
 /DERIVEDFIELD CST_1_9 "989"
 /DERIVEDFIELD CST_1_15 "907"
 /DERIVEDFIELD CST_1_12 "916"
 /DERIVEDFIELD CST_1_6 "961"
 /FIELDS FLD_CH_29_3 29 CH 3
 /CONDITION CND_4 FLD_CH_29_3 EQ CST_1_12 
 /CONDITION CND_5 FLD_CH_29_3 EQ CST_1_15 
 /CONDITION CND_3 FLD_CH_29_3 EQ CST_1_9 
 /CONDITION CND_6 FLD_CH_29_3 EQ CST_1_18 
 /CONDITION CND_1 FLD_CH_29_3 EQ CST_1_3 
 /CONDITION CND_2 FLD_CH_29_3 EQ CST_1_6 
 /COPY
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
 /MT_OUTFILE_SUF 4
 /INCLUDE CND_4
 /MT_OUTFILE_SUF 5
 /INCLUDE CND_5
 /MT_OUTFILE_SUF 6
 /INCLUDE CND_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC32PAE
       ;;
(GQC32PAE)
       m_CondExec 00,EQ,GQC32PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC320                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC32PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC32PAG
       ;;
(GQC32PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES ADRESSES                                                   
#    RSQC12   : NAME=RSQC12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSQC12 /dev/null
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g ${G_A2} FQC320 ${DATA}/PXX0/F07.FQC032AP
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC320 
       JUMP_LABEL=GQC32PAH
       ;;
(GQC32PAH)
       m_CondExec 04,GE,GQC32PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNZIP DU FICHIER REPONSE RECU DE TESSI                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=PKUNZIP                                                   
# //STEPLIB  DD  DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                              
# //***      DD  DSN=SYS2.ZIP390.D21ZTAB,DISP=SHR                              
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=TESSIBP,MODE=I                                           
# ******* FICHIER EN SORTIE DE ZIP                                             
# TARGET   FILE  NAME=FQC032BP,MODE=O                                          
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ARCHIVE_INFILE(DD1)                                                         
# -EXTRACT                                                                     
# -OUTFILE(TARGET)                                                             
# -TRAN(ASCII850)                                                              
#          DATAEND                                                             
# *******************************************************************          
#  JAVAUNZIP  FICHIER                                                          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GQC32PAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC32PAJ
       ;;
(GQC32PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR -g +0 DD1 ${DATA}/PXX0/FTP.F99.TESSIBP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC32PAJ.sysin
       m_ProgramExec JVMLDM76_UNZIP.ksh
# ********************************************************************         
#   PROG : BQC330                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC32PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQC32PAM
       ;;
(GQC32PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES ADRESSES                                                   
#    RSQC13   : NAME=RSQC13,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSQC13 /dev/null
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g ${G_A3} FQC330 ${DATA}/PXX0/F07.FQC032BP
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC330 
       JUMP_LABEL=GQC32PAN
       ;;
(GQC32PAN)
       m_CondExec 04,GE,GQC32PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC32PZA
       ;;
(GQC32PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC32PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
