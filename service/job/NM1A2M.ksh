#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM1A2M.ksh                       --- VERSION DU 08/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMNM1A2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/09/13 AT 10.22.00 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM1A2M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#       *  TRI CUMUL EN CAS DE PLANTAGE DE LA CHAINE NM002M                    
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NM1A2MA
       ;;
(NM1A2MA)
#
#NM1A2MAM
#NM1A2MAM Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#NM1A2MAM
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2000/09/13 AT 10.22.00 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: NM1A2M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-NM002M'                            
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NM1A2MAA
       ;;
(NM1A2MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGM/F89.BNM155AM
# ******* FIC DE CUMUL EN CAS DE PLANTAGE DU TRAITEMENT LA VEILLE              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNMCUMAM
# ******* FIC DE CUMUL POUR (J+1)                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PNCGM/F89.BNMCUMAM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM1A2MAB
       ;;
(NM1A2MAB)
       m_CondExec 00,EQ,NM1A2MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
#   - FIC1 :                                                                   
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION POUR I.C.S                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM1A2MAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM1A2MAD
       ;;
(NM1A2MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGM/F89.BNM001CM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT2 ${DATA}/PNCGM/F89.BNM001DM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM1A2MAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM1A2MAE
       ;;
(NM1A2MAE)
       m_CondExec 16,NE,NM1A2MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM1A2MAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
