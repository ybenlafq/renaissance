#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB120P.ksh                       --- VERSION DU 08/10/2016 12:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB120 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 14.54.32 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB120P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BRB120 : LECTURE FICHIER ENVOYE PAR SAGEM                               
#             : MAJ DES TABLES DES ENVOIS RTRB02 & RTRB03                      
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB120PA
       ;;
(RB120PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       RUN=${RUN}
       JUMP_LABEL=RB120PAA
       ;;
(RB120PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER EN PROVENANCE DE SAGEM ET BEWAN                              
       m_FileAssign -d SHR FRB120 ${DATA}/PXX0/F07.BRB120AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F07.BEWAN0AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F07.NC0000AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/F07.NETGE0AP
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB02   : NAME=RSRB02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB02 /dev/null
# *                                                                            
#    RSRB03   : NAME=RSRB03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB120 
       JUMP_LABEL=RB120PAB
       ;;
(RB120PAB)
       m_CondExec 04,GE,RB120PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BRB036 : MAJ DE LA TABLE RTRB31 POUR SIGNALER QUE LE PRODUIT               
#            EST � FERRAILLER CHEZ SAGEM                                       
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB120PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB120PAD
       ;;
(RB120PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *******                                                                      
#    RSRB03   : NAME=RSRB03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
# *******                                                                      
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
# *******                                                                      
#    RSRB31   : NAME=RSRB31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# *******                                                                      
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# * FIC PROD A FERAILLER VENANT DE GATEWAY ENVOYE PAR SAGEM ET BEWAN           
       m_FileAssign -d SHR FRB036 ${DATA}/PXX0/F07.BRB120AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F07.BEWAN0AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F07.NC0000AP
       m_FileAssign -d SHR -C ${DATA}/PXX0/F07.NETGE0AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB036 
       JUMP_LABEL=RB120PAE
       ;;
(RB120PAE)
       m_CondExec 04,GE,RB120PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB125 : SOLDE DE LA GEF                                                
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB120PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB120PAG
       ;;
(RB120PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ------  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#    RSRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB00 /dev/null
#    RSRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB01 /dev/null
#    RSRB03   : NAME=RSRB03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB03 /dev/null
#                                                                              
# ******  TABLES DES ENVOIS EN MAJ                                             
#    RSRB00   : NAME=RSRB00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB00 /dev/null
# *                                                                            
#    RSRB01   : NAME=RSRB01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB125 
       JUMP_LABEL=RB120PAH
       ;;
(RB120PAH)
       m_CondExec 04,GE,RB120PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  DELETE DES FICHIERS REMONT�S VIA GATEWAY EN PROVENANCE DE SAGEM           
# *  APR�S TRAITEMENT.                                                         
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB120PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB120PAJ
       ;;
(RB120PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB120PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB120PAK
       ;;
(RB120PAK)
       m_CondExec 16,NE,RB120PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CR�ATION � VIDE D'UNE G�N�RATION APR�S TRAITEMENT                           
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB120PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB120PAM
       ;;
(RB120PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC LOG DE COMPLETEL                                                 
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
# ******  CR�ATION � VIDE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 495 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.BRB120AP
       m_FileAssign -d NEW,CATLG,DELETE -r 495 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/FTP.F07.BEWAN0AP
       m_FileAssign -d NEW,CATLG,DELETE -r 495 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/FTP.F07.NC0000AP
       m_FileAssign -d NEW,CATLG,DELETE -r 495 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F07.NETGE0AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB120PAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB120PAN
       ;;
(RB120PAN)
       m_CondExec 16,NE,RB120PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
