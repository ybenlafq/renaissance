#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SP100D.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDSP100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/06/11 AT 16.11.02 BY BURTEC6                      
#    STANDARDS: P  JOBSET: SP100D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSP150 :PRIX DE VENTES INFERIEUR AU SRP                                
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SP100DA
       ;;
(SP100DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SP100DAA
       ;;
(SP100DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP20D  : NAME=RSSP20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP20D /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE                                                                       
#    RTGA04D  : NAME=RSGA04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA04D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE                                                                       
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE                                                                       
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN ENTREE : FNSOC                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#  FICHIER EN SORTIE : ISP150AP DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISP150 ${DATA}/PTEM/SP100DAA.ISP150AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP150 
       JUMP_LABEL=SP100DAB
       ;;
(SP100DAB)
       m_CondExec 04,GE,SP100DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP150AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAD
       ;;
(SP100DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SP100DAA.ISP150AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DAD.ISP150BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_20_20 20 CH 20
 /FIELDS FLD_PD_12_3 12 PD 3
 /KEYS
   FLD_BI_7_5 DESCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_20_20 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DAE
       ;;
(SP100DAE)
       m_CondExec 00,EQ,SP100DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP150CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAG
       ;;
(SP100DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/SP100DAD.ISP150BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/SP100DAG.ISP150CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP100DAH
       ;;
(SP100DAH)
       m_CondExec 04,GE,SP100DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAJ
       ;;
(SP100DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SP100DAG.ISP150CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DAJ.ISP150DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DAK
       ;;
(SP100DAK)
       m_CondExec 00,EQ,SP100DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP150                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAM
       ;;
(SP100DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/SP100DAD.ISP150BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/SP100DAJ.ISP150DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP150 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP100DAN
       ;;
(SP100DAN)
       m_CondExec 04,GE,SP100DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSP151 :PRIX DE VENTES INFERIEUR AU SRP  (EPF)                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAQ
       ;;
(SP100DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP20D  : NAME=RSSP20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP20D /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE                                                                       
#    RTGA04D  : NAME=RSGA04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA04D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE                                                                       
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE                                                                       
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN ENTREE : FNSOC                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#  FICHIER EN SORTIE : ISP151AY DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISP151 ${DATA}/PTEM/SP100DAQ.ISP151AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP151 
       JUMP_LABEL=SP100DAR
       ;;
(SP100DAR)
       m_CondExec 04,GE,SP100DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP151AY)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAT
       ;;
(SP100DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/SP100DAQ.ISP151AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DAT.ISP151BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_20_20 20 CH 20
 /KEYS
   FLD_BI_7_5 DESCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_20_20 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DAU
       ;;
(SP100DAU)
       m_CondExec 00,EQ,SP100DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP151CY                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SP100DAX
       ;;
(SP100DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/SP100DAT.ISP151BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/SP100DAX.ISP151CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP100DAY
       ;;
(SP100DAY)
       m_CondExec 04,GE,SP100DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBA
       ;;
(SP100DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/SP100DAX.ISP151CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DBA.ISP151DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DBB
       ;;
(SP100DBB)
       m_CondExec 00,EQ,SP100DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP151                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBD
       ;;
(SP100DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/SP100DAT.ISP151BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/SP100DBA.ISP151DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP151 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP100DBE
       ;;
(SP100DBE)
       m_CondExec 04,GE,SP100DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSP180 :PRIX DE VENTES INFERIEUR AU SRP  (PREVISIONNEL)                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBG
       ;;
(SP100DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTGF10D  : NAME=RSGF10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGF10D /dev/null
#  TABLE                                                                       
#    RTGF20D  : NAME=RSGF20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGF20D /dev/null
#  TABLE                                                                       
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#    RTGA06D  : NAME=RSGA06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA06D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE                                                                       
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE                                                                       
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#  TABLE                                                                       
#    RTGA59D  : NAME=RSGA59D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59D /dev/null
# ****** TABLE DES COMMANDES FOURNISSEUR                                       
#    RTGA69D  : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA69D /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN ENTREE : FNSOC                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#  FICHIER EN SORTIE : ISP180AP DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISP180 ${DATA}/PTEM/SP100DBG.ISP180AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP180 
       JUMP_LABEL=SP100DBH
       ;;
(SP100DBH)
       m_CondExec 04,GE,SP100DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP180AY)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBJ
       ;;
(SP100DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/SP100DBG.ISP180AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DBJ.ISP180BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_61 7 CH 61
 /KEYS
   FLD_BI_7_61 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DBK
       ;;
(SP100DBK)
       m_CondExec 00,EQ,SP100DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP180CY                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBM
       ;;
(SP100DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/SP100DBJ.ISP180BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/SP100DBM.ISP180CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP100DBN
       ;;
(SP100DBN)
       m_CondExec 04,GE,SP100DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBQ
       ;;
(SP100DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/SP100DBM.ISP180CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DBQ.ISP180DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DBR
       ;;
(SP100DBR)
       m_CondExec 00,EQ,SP100DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP180                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBT
       ;;
(SP100DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/SP100DBJ.ISP180BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/SP100DBQ.ISP180DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP180 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP100DBU
       ;;
(SP100DBU)
       m_CondExec 04,GE,SP100DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSP190 :PRIX ETIQUETTE INFERIEUR AU SRP                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=SP100DBX
       ;;
(SP100DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP20D  : NAME=RSSP20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP20D /dev/null
#  TABLE                                                                       
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE                                                                       
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE                                                                       
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN ENTREE : FNSOC                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#  FICHIER EN SORTIE : ISP190A DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISP190 ${DATA}/PTEM/SP100DBX.ISP190AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP190 
       JUMP_LABEL=SP100DBY
       ;;
(SP100DBY)
       m_CondExec 04,GE,SP100DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP190A)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCA
       ;;
(SP100DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/SP100DBX.ISP190AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DCA.ISP190BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_16_3 16 PD 3
 /FIELDS FLD_BI_12_4 12 CH 4
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_19_5 19 CH 5
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_4 ASCENDING,
   FLD_PD_16_3 ASCENDING,
   FLD_BI_19_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DCB
       ;;
(SP100DCB)
       m_CondExec 00,EQ,SP100DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP190CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCD
       ;;
(SP100DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/SP100DCA.ISP190BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/SP100DCD.ISP190CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP100DCE
       ;;
(SP100DCE)
       m_CondExec 04,GE,SP100DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCG
       ;;
(SP100DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/SP100DCD.ISP190CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DCG.ISP190DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DCH
       ;;
(SP100DCH)
       m_CondExec 00,EQ,SP100DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP190                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCJ
       ;;
(SP100DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/SP100DCA.ISP190BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A20} FCUMULS ${DATA}/PTEM/SP100DCG.ISP190DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP190 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP100DCK
       ;;
(SP100DCK)
       m_CondExec 04,GE,SP100DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSP071 :  SRP  RECYCLES                                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCM
       ;;
(SP100DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP10D  : NAME=RSSP10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP10D /dev/null
#  TABLE                                                                       
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#    RTGA06D  : NAME=RSGA06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA06D /dev/null
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE                                                                       
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE                                                                       
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#  TABLE                                                                       
#    RTGF20D  : NAME=RSGF20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGF20D /dev/null
#  TABLE                                                                       
#    RTGG70D  : NAME=RSGG70D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70D /dev/null
# ***** TABLE DES COMMANDES FOURNISSEUR                                        
#    RTGF10D  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGF10D /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN SORTIE : ISP071AP DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISP071 ${DATA}/PTEM/SP100DCM.ISP071AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP071 
       JUMP_LABEL=SP100DCN
       ;;
(SP100DCN)
       m_CondExec 04,GE,SP100DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP071AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCQ
       ;;
(SP100DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/SP100DCM.ISP071AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DCQ.ISP071BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_62 7 CH 62
 /KEYS
   FLD_BI_7_62 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DCR
       ;;
(SP100DCR)
       m_CondExec 00,EQ,SP100DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP071CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCT
       ;;
(SP100DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A22} FEXTRAC ${DATA}/PTEM/SP100DCQ.ISP071BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/SP100DCT.ISP071CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP100DCU
       ;;
(SP100DCU)
       m_CondExec 04,GE,SP100DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=SP100DCX
       ;;
(SP100DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/SP100DCT.ISP071CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/SP100DCX.ISP071DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP100DCY
       ;;
(SP100DCY)
       m_CondExec 00,EQ,SP100DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP071                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP100DDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=SP100DDA
       ;;
(SP100DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PTEM/SP100DCQ.ISP071BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A25} FCUMULS ${DATA}/PTEM/SP100DCX.ISP071DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP071 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP100DDB
       ;;
(SP100DDB)
       m_CondExec 04,GE,SP100DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SP100DZA
       ;;
(SP100DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SP100DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
