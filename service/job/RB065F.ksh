#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB065F.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFRB065 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.12.36 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB065F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BRB065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB065FA
       ;;
(RB065FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RB065FAA
       ;;
(RB065FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***********************************                                          
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FAA.BRB065BP
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAD
       ;;
(RB065FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/RB065FAA.BRB065BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRB066KP.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FAD.BRB065CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_49_3 49 CH 3
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FAE
       ;;
(RB065FAE)
       m_CondExec 00,EQ,RB065FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAG
       ;;
(RB065FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FRB066 ${DATA}/PTEM/RB065FAD.BRB065CP
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F07.BRB066KP.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F07.BRB066AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FAH
       ;;
(RB065FAH)
       m_CondExec 04,GE,RB065FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAJ PGM=BRB065     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAJ
       ;;
(RB065FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FAJ.BRB065BD
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAM
       ;;
(RB065FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RB065FAJ.BRB065BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BRB066KD.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FAM.BRB065CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_49_3 49 CH 3
 /FIELDS FLD_CH_52_7 52 CH 7
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FAN
       ;;
(RB065FAN)
       m_CondExec 00,EQ,RB065FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAQ
       ;;
(RB065FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FRB066 ${DATA}/PTEM/RB065FAM.BRB065CD
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F91.BRB066KD.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F91.BRB066AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FAR
       ;;
(RB065FAR)
       m_CondExec 04,GE,RB065FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAT PGM=BRB065     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAT
       ;;
(RB065FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FAT.BRB065BL
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RB065FAX
       ;;
(RB065FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/RB065FAT.BRB065BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BRB066KL.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FAX.BRB065CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_49_3 49 CH 3
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FAY
       ;;
(RB065FAY)
       m_CondExec 00,EQ,RB065FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBA
       ;;
(RB065FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FRB066 ${DATA}/PTEM/RB065FAX.BRB065CL
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F61.BRB066KL.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F61.BRB066AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FBB
       ;;
(RB065FBB)
       m_CondExec 04,GE,RB065FBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBD PGM=BRB065     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBD
       ;;
(RB065FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FBD.BRB065BM
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBG
       ;;
(RB065FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RB065FBD.BRB065BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BRB066KM.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FBG.BRB065CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_49_3 49 CH 3
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FBH
       ;;
(RB065FBH)
       m_CondExec 00,EQ,RB065FBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBJ
       ;;
(RB065FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRB066 ${DATA}/PTEM/RB065FBG.BRB065CM
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F89.BRB066KM.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F89.BRB066AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FBK
       ;;
(RB065FBK)
       m_CondExec 04,GE,RB065FBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBM PGM=BRB065     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBM
       ;;
(RB065FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FBM.BRB065BO
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBQ
       ;;
(RB065FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/RB065FBM.BRB065BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BRB066KO.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FBQ.BRB065CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_49_3 49 CH 3
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FBR
       ;;
(RB065FBR)
       m_CondExec 00,EQ,RB065FBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBT
       ;;
(RB065FBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A10} FRB066 ${DATA}/PTEM/RB065FBQ.BRB065CO
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F16.BRB066KO.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F16.BRB066AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FBU
       ;;
(RB065FBU)
       m_CondExec 04,GE,RB065FBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FBX PGM=BRB065     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=RB065FBX
       ;;
(RB065FBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FIC VENANT DARTYBOX VIA XFB-GATEWAY ET CFT                           
       m_FileAssign -d SHR -g +0 FRB065 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066 ${DATA}/PTEM/RB065FBX.BRB065BY
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_ProgramExec BRB065 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=RB065FCA
       ;;
(RB065FCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC VENANT                                                           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/RB065FBX.BRB065BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BRB066KY.RECYCLE
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/RB065FCA.BRB065CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_49_3 49 CH 3
 /KEYS
   FLD_CH_49_3 ASCENDING,
   FLD_CH_52_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB065FCB
       ;;
(RB065FCB)
       m_CondExec 00,EQ,RB065FCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRB066                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB065FCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=RB065FCD
       ;;
(RB065FCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A12} FRB066 ${DATA}/PTEM/RB065FCA.BRB065CY
# ******  TABLE EN M.A.J.                                                      
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSRB10   : NAME=RSRB10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB10 /dev/null
#    RSRB31   : NAME=RSRB31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB31 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSRB30   : NAME=RSRB30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#  FICHIER DE RECYCLAGE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FRB066O ${DATA}/PXX0/F45.BRB066KY.RECYCLE
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FANO01 ${DATA}/PXX0/F45.BRB066AY
#                                                                              
# ******************************************************************           
#  REMISE A ZERO DU FICHIER BRB065AF/BRB067BP ISSU DE LA GATEWAY               
#  REPRISE : OUI                                                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB066 
       JUMP_LABEL=RB065FCE
       ;;
(RB065FCE)
       m_CondExec 04,GE,RB065FCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RB065FCG PGM=IDCAMS     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=RB065FCG
       ;;
(RB065FCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-150 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.BRB065AF
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-150 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F99.BRB067BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB065FCG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB065FCH
       ;;
(RB065FCH)
       m_CondExec 16,NE,RB065FCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB065FZA
       ;;
(RB065FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB065FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
