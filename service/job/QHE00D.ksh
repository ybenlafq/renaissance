#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QHE00D.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDQHE00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/23 AT 13.50.26 BY BURTEC2                      
#    STANDARDS: P  JOBSET: QHE00D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   REQUETE QHE00                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QHE00DA
       ;;
(QHE00DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=QHE00DAD
       ;;
(QHE00DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE01 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE01 
       JUMP_LABEL=QHE00DAE
       ;;
(QHE00DAE)
       m_CondExec 04,GE,QHE00DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE03                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAG
       ;;
(QHE00DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE03 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -i HOSTIN
DEBMOIS=$DEBMOIS_ANNMMDD
_end
       m_FileAssign -i -C
FINMOIS=$FINMOIS_ANNMMDD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE03 
       JUMP_LABEL=QHE00DAH
       ;;
(QHE00DAH)
       m_CondExec 04,GE,QHE00DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE04                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAJ
       ;;
(QHE00DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE04 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE04 
       JUMP_LABEL=QHE00DAK
       ;;
(QHE00DAK)
       m_CondExec 04,GE,QHE00DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE06                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAM
       ;;
(QHE00DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHE06 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QHE06 (&&SOC='$DPMDEP_1_3' FORM=ADMFIL.FHE06
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE00D3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE00DAN
       ;;
(QHE00DAN)
       m_CondExec 04,GE,QHE00DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE07                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAQ
       ;;
(QHE00DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE07 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE07 
       JUMP_LABEL=QHE00DAR
       ;;
(QHE00DAR)
       m_CondExec 04,GE,QHE00DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE08                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAT
       ;;
(QHE00DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE08 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE08 
       JUMP_LABEL=QHE00DAU
       ;;
(QHE00DAU)
       m_CondExec 04,GE,QHE00DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE10                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DAX
       ;;
(QHE00DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHE10 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QHE00DN1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE00DN1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE00DAY
       ;;
(QHE00DAY)
       m_CondExec 04,GE,QHE00DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE11                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBA
       ;;
(QHE00DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHE11 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QHE11 (&&DEB='$DEBMOIS_ANNMMDD' &&FIN='$FINMOIS_ANNMMDD' FORM=ADMFIL.FHE11
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE00DN2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE00DBB
       ;;
(QHE00DBB)
       m_CondExec 04,GE,QHE00DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE12                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBD
       ;;
(QHE00DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHE12 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QHE12 (&&D='$DEBMOIS_ANNMMDD' &&F='$FINMOIS_ANNMMDD' &&SOC='$DPMDEP' FORM=ADMFIL.FHE12
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE00DN3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE00DBE
       ;;
(QHE00DBE)
       m_CondExec 04,GE,QHE00DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REQUETE QHE14                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBM
       ;;
(QHE00DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHE17 DSQPRINT
       m_FileAssign -i QMFPARM
RUN QHE17 (&&DEB='$DEBMOIS_ANNMMDD' &&FIN='$FINMOIS_ANNMMDD' &&SOC='$DPMDEP_1_3' FORM=FHE17
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QHE00D17 -a QMFPARM DSQPRINT
       JUMP_LABEL=QHE00DBN
       ;;
(QHE00DBN)
       m_CondExec 04,GE,QHE00DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          GHS - REMONTEES AU CENTRE DU MOIS VALORISEES     RA FLUX            
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBQ
       ;;
(QHE00DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QCSDRA4 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -i HOSTIN
DEBMOIS=$DEBMOIS_ANNMMDD
_end
       m_FileAssign -i -C
FINMOIS=$FINMOIS_ANNMMDD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QCSDRA4 
       JUMP_LABEL=QHE00DBR
       ;;
(QHE00DBR)
       m_CondExec 04,GE,QHE00DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#          LISTE DES ECHANGES SAV                  BOITE AL DSAV               
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBT
       ;;
(QHE00DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QCSDAL3 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -i HOSTIN
DEBMOIS=$DEBMOIS_ANNMMDD
_end
       m_FileAssign -i -C
FINMOIS=$FINMOIS_ANNMMDD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QCSDAL3 
       JUMP_LABEL=QHE00DBU
       ;;
(QHE00DBU)
       m_CondExec 04,GE,QHE00DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QQF QHE10B                                                           
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DBX
       ;;
(QHE00DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QHE10B QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QHE10B 
       JUMP_LABEL=QHE00DBY
       ;;
(QHE00DBY)
       m_CondExec 04,GE,QHE00DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE200 : EXTRACTION ETATS-GRF-STOCK DU MOIS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCA
       ;;
(QHE00DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA69   : NAME=RSGA69D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE10   : NAME=RSHE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX2/QHE00DA1
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F91.FHE200AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE200 
       JUMP_LABEL=QHE00DCB
       ;;
(QHE00DCB)
       m_CondExec 04,GE,QHE00DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE210 : EXTRACTION ETATS-GRF-SORTIES DU MOIS                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCD
       ;;
(QHE00DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA69   : NAME=RSGA69D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE10   : NAME=RSHE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX2/QHE00DA2
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F91.FHE210AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE210 
       JUMP_LABEL=QHE00DCE
       ;;
(QHE00DCE)
       m_CondExec 04,GE,QHE00DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE220 : EXTRACTION ETATS-GRF-STOCK   DU MOIS                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCG
       ;;
(QHE00DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA69   : NAME=RSGA69D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE10   : NAME=RSHE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F91.FHE220AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE220 
       JUMP_LABEL=QHE00DCH
       ;;
(QHE00DCH)
       m_CondExec 04,GE,QHE00DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE230 : EXTRACTION ETATS-GRF-SUIVI-CREATION-ENVOI DU MOIS                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCJ
       ;;
(QHE00DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA69   : NAME=RSGA69D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE10   : NAME=RSHE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX2/QHE00D3
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F91.FHE230AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE230 
       JUMP_LABEL=QHE00DCK
       ;;
(QHE00DCK)
       m_CondExec 04,GE,QHE00DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCM PGM=EZACFSM1   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCM
       ;;
(QHE00DCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE00DCM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/QHE00DCM.FTQHE00D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCQ PGM=JVMLDM76   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCQ
       ;;
(QHE00DCQ)
       m_CondExec ${EXAEB},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F91.FHE200AD
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F91.QHE0DZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE00DCQ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTQHE00D                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCT PGM=EZACFSM1   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCT
       ;;
(QHE00DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE00DCT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PTEM/QHE00DCM.FTQHE00D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE00D                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE00DCX PGM=FTP        ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DCX
       ;;
(QHE00DCX)
       m_CondExec ${EXAEL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE00DCX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.QHE00DCM.FTQHE00D(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QHE00DZA
       ;;
(QHE00DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE00DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
