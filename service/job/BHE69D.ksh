#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BHE69D.ksh                       --- VERSION DU 14/10/2016 09:49
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDBHE69 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/02/08 AT 15.05.42 BY PREPA2                       
#    STANDARDS: P  JOBSET: BHE69D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BHE69DA
       ;;
(BHE69DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BHE69DAA
       ;;
(BHE69DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE69DAA.sysin
       m_UtilityExec
# ********************************************************************         
#           VERIFY DES FICHIERS VSAM                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE69DAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BHE69DAG
       ;;
(BHE69DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# ******  FIC VSAM                                                             
# -M-FJHE601D - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR IN1 ${DATA}/PEX0.F91.JHE601D
#                                                                              
# ******  FIC SAM DE SAUVEGARDE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT1 ${DATA}/PXX0/F91.FJHE60AD.SAUVE.SAM
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE69DAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BHE69DAH
       ;;
(BHE69DAH)
       m_CondExec 16,NE,BHE69DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROGRAME  PGM BJHE69  EXTRACTION A PARTIR DU FIC CICS (ATTENTION)           
#  DES MOUVEMENTS A RECONDUIRE                                                 
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE69DAJ PGM=BJHE69     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BHE69DAJ
       ;;
(BHE69DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER CICS (ENTREE)                                                 
# -M-FJHE601D - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR FJHE69E ${DATA}/PEX0.F91.JHE601D
#                                                                              
# ****** FICHIER SAM DES MVTS A CONSERVER                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FJHE69S ${DATA}/PXX0/F91.FH601AD
       m_ProgramExec BJHE69 
#                                                                              
# ********************************************************************         
#  DEL DEF DU FICHIER VSAM                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE69DAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BHE69DAM
       ;;
(BHE69DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE69DAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BHE69DAN
       ;;
(BHE69DAN)
       m_CondExec 16,NE,BHE69DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO  DU SAM MVTS RECONDUITS SUR LE VSAM VIDE                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE69DAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BHE69DAQ
       ;;
(BHE69DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# ******  FIC VSAM                                                             
       m_FileAssign -d SHR OUT1 ${DATA}/PEX0.F91.JHE601D
#                                                                              
# ******  FIC SAM DE SAUVEGARDE                                                
       m_FileAssign -d SHR -g ${G_A1} IN1 ${DATA}/PXX0/F91.FH601AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE69DAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BHE69DAR
       ;;
(BHE69DAR)
       m_CondExec 16,NE,BHE69DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP BHE69DAT PGM=PGMSVC34   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BHE69DAT
       ;;
(BHE69DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE69DAT.sysin
       m_UtilityExec
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
