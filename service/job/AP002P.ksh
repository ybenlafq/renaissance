#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP002P.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/09/16 AT 09.52.37 BY BURTECA                      
#    STANDARDS: P  JOBSET: AP002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BAP002  : ALIMENTATION DES ATTESTATIONS � PARTIR DES VENTES DE              
#          : PRESTATIONS A2I                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP002PA
       ;;
(AP002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       RUN=${RUN}
       JUMP_LABEL=AP002PAA
       ;;
(AP002PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE DES VENTS                                                      
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11X  : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11X /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP10   : NAME=RSAP10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP10 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP17 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP002 
       JUMP_LABEL=AP002PAB
       ;;
(AP002PAB)
       m_CondExec 04,GE,AP002PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP003  : D�TERMINATION DU MONTANT CESU � PRENDRE EN COMPTE PAR             
#          : ENCAISSEMENT DES VENTES A2I                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAD
       ;;
(AP002PAD)
       m_CondExec ${EXAAF},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP11   : NAME=RSAP11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP11 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP14   : NAME=RSAP14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP14 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP003 
       JUMP_LABEL=AP002PAE
       ;;
(AP002PAE)
       m_CondExec 04,GE,AP002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP004  : RAPPROCHEMENT DES DONN�ES DE R�ALISATION DE LA PRESTATION         
#          : DES DONN�ES DE VENTE A2I                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAG
       ;;
(AP002PAG)
       m_CondExec ${EXAAK},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP17 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP004 
       JUMP_LABEL=AP002PAH
       ;;
(AP002PAH)
       m_CondExec 04,GE,AP002PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP005  : IMPUTATION DES MOYENS DE PAIEMENT SUR LES PRESTATIONS             
#          : VENDUES ET R�ALIS�ES                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAJ
       ;;
(AP002PAJ)
       m_CondExec ${EXAAP},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP10   : NAME=RSAP10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP10 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP11   : NAME=RSAP11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP11 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP14   : NAME=RSAP14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP14 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP005 
       JUMP_LABEL=AP002PAK
       ;;
(AP002PAK)
       m_CondExec 04,GE,AP002PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP006  : ALIMENTATION DES ATTESTATIONS � PARTIR DES CRI ENCAISS�S          
#          : DES DONN�ES DE R�ALISATION                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAM
       ;;
(AP002PAM)
       m_CondExec ${EXAAU},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP12   : NAME=RSAP12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP12 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP13   : NAME=RSAP13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP13 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP12   : NAME=RSAP12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP12 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP17 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP006 
       JUMP_LABEL=AP002PAN
       ;;
(AP002PAN)
       m_CondExec 04,GE,AP002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP007  : D�TERMINATION DU MONTANT CESU � PRENDRE EN COMPTE PAR             
#          : ENCAISSE-MENT DES CRI                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAQ
       ;;
(AP002PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP13   : NAME=RSAP13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP13 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP14   : NAME=RSAP14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP14 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP007 
       JUMP_LABEL=AP002PAR
       ;;
(AP002PAR)
       m_CondExec 04,GE,AP002PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BAP008  : IMPUTATION DES MOYENS DE PAIEMENT SUR LES PRESTATIONS             
#          : ENCAISS�ES VIA CRI                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP002PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=AP002PAT
       ;;
(AP002PAT)
       m_CondExec ${EXABE},NE,YES 
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP13   : NAME=RSAP13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP13 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP14   : NAME=RSAP14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP14 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP008 
       JUMP_LABEL=AP002PAU
       ;;
(AP002PAU)
       m_CondExec 04,GE,AP002PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
