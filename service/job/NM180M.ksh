#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM180M.ksh                       --- VERSION DU 08/10/2016 22:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMNM180 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/07/19 AT 11.20.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM180M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BNM180 : CONSTITUTION DU FICHIER TRIE FNM180                           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM180MA
       ;;
(NM180MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM180MAA
       ;;
(NM180MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *************************************** TABLES EN LECTURE                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTEM51   : NAME=RSEM51,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEM51 /dev/null
#    RTEM52   : NAME=RSEM52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEM52 /dev/null
#    RTEM53   : NAME=RSEM53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEM53 /dev/null
# *****   ENTREE: FICHIER SOCIETE                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   PARAMETRE  MOIS                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER EN SORTIE : BNM180A                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FNM180 ${DATA}/PTEM/NM180MAA.BNM180AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM180 
       JUMP_LABEL=NM180MAB
       ;;
(NM180MAB)
       m_CondExec 04,GE,NM180MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FNM180A D EXTRACTION DES MOUVEMENTS POUR FICHIER             
#                 D EDITION FNM181A                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM180MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM180MAD
       ;;
(NM180MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIERS ENTREE : BNM180A                                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NM180MAA.BNM180AM
# *****   FICHIER SORTIE TRIE : BNM181A                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/NM180MAD.BNM181AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_14 16 CH 14
 /KEYS
   FLD_CH_16_14 ASCENDING
 /* Record Type = F  Record Length = 83 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM180MAE
       ;;
(NM180MAE)
       m_CondExec 00,EQ,NM180MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BNM181                                                                
#  SORTIE DE L ETAT INM181 CONCERNANT TOUS LES MOUVEMENTS DE CAISSES           
#         AVEC REMBOURSEMENT ESPECES (RENDU MONNAIE OU REMBOURSEMENT)          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM180MAG PGM=BNM181     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM180MAG
       ;;
(NM180MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   ENTREE: FICHIER TRIE BNM181A ISSU DU TRI PRECEDENT                   
       m_FileAssign -d SHR -g ${G_A2} FNM181 ${DATA}/PTEM/NM180MAD.BNM181AM
# *****   SORTIE: ETAT INM181                                                  
       m_OutputAssign -c 9 -w INM181 INM181
       m_ProgramExec BNM181 
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM180MZA
       ;;
(NM180MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM180MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
