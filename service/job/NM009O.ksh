#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM009O.ksh                       --- VERSION DU 09/10/2016 00:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PONM009 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/27 AT 11.53.27 BY BURTECL                      
#    STANDARDS: P  JOBSET: NM009O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DU  TABLESPACE : RSMQ15O                                            
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM009OA
       ;;
(NM009OA)
#
#NM009OAA
#NM009OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#NM009OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=NM009OAD
       ;;
(NM009OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/NM009OAD
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/NM009OAD
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15O  : NAME=RSMQ15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  FICHIER LG 107                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -g +1 FTS057 ${DATA}/PXX0/F16.BNM157AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=NM009OAE
       ;;
(NM009OAE)
       m_CondExec 04,GE,NM009OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
