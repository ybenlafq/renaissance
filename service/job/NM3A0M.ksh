#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM3A0M.ksh                       --- VERSION DU 08/10/2016 13:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMNM3A0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/02/26 AT 11.11.34 BY BURTECL                      
#    STANDARDS: P  JOBSET: NM3A0M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#        * REPRO CUMUL POUR (J+1)  EN CAS DE PLANTAGE DE NM030M                
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NM3A0MA
       ;;
(NM3A0MA)
#
#NM3A0MAJ
#NM3A0MAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#NM3A0MAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2003/02/26 AT 11.11.34 BY BURTECL                
# *    JOBSET INFORMATION:    NAME...: NM3A0M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-NM030M'                            
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NM3A0MAA
       ;;
(NM3A0MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC EN DE MQ VENTES TOP�ES SANS BON DE COMMANDE                      
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGM/F89.BNM160AM
# ******* FIC DE REPRISE L POUR (J+1)                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 OUT1 ${DATA}/PNCGM/F89.BNMCUMCM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM3A0MAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM3A0MAB
       ;;
(NM3A0MAB)
       m_CondExec 16,NE,NM3A0MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        * CREATION D'UN FICHIER VIDE POUR LIBERATION DE L'ICS                 
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM3A0MAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM3A0MAD
       ;;
(NM3A0MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******* FIC POUR L'INTERFACE COMPTABLE STD (I.C.S)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGM/F89.BNM003AM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM3A0MAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM3A0MAE
       ;;
(NM3A0MAE)
       m_CondExec 16,NE,NM3A0MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM3A0MAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
