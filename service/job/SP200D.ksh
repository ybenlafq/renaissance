#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SP200D.ksh                       --- VERSION DU 08/10/2016 13:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDSP200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/19 AT 12.34.15 BY BURTEC5                      
#    STANDARDS: P  JOBSET: SP200D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   BSP200 : EXTRACT CODIC EPD EPF AVEC STOCK MAG OU STOCK DEPOT > 0           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SP200DA
       ;;
(SP200DA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2001/02/19 AT 12.34.15 BY BURTEC5                
# *    JOBSET INFORMATION:    NAME...: SP200D                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'CONTROL STATUT APPRO'                  
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SP200DAA
       ;;
(SP200DAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLE                                                              
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
# ******* LIEUX                                                                
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
# ******* FAMILLE                                                              
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
# ******* STOCK DEPOT                                                          
#    RTGS10D  : NAME=RSGS10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS10D /dev/null
# ******* STOCK MAG                                                            
#    RTGS30D  : NAME=RSGS30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGS30D /dev/null
#                                                                              
# ******* FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FSP200 ${DATA}/PXX0/F91.BSP200AD
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP200 
       JUMP_LABEL=SP200DAB
       ;;
(SP200DAB)
       m_CondExec 04,GE,SP200DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
