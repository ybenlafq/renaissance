#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM002M.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMNM002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/08 AT 10.19.54 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM002M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM002MA
       ;;
(NM002MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM002MAA
       ;;
(NM002MAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
#    RECUP FICHIER CAISSES MQ                                                  
# ***************************************                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PXX0/RBA.QNM002M
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/NM002MAA
       m_ProgramExec IEFBR14 "RDAR,NM002M.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NM002MAD
       ;;
(NM002MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QNM002M
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=NM002MAE
       ;;
(NM002MAE)
       m_CondExec 00,EQ,NM002MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER ISSU DU BNM100 REMONTEE MQ ( CODE S55 )              
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAG
       ;;
(NM002MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGM/F89.BNM155AM
# ******* FIC DE REPRISE DES CAISSES EN ANO (J-1)                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNM001BM
# ******* FIC DE CUMUL EN CAS DE PLANTAGE DU TRAITEMENT LA VEILLE              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNMCUMAM
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MAG.BNM155BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MAH
       ;;
(NM002MAH)
       m_CondExec 00,EQ,NM002MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI2 *  R�CUP�RATION DU TYPE='0' (PAIEMENTS)                                
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAJ
       ;;
(NM002MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NM002MAG.BNM155BM
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MAJ.BNM155CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "0"
 /FIELDS FLD_CH_35_8 35 CH 8
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_43_1 43 CH 1
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 DESCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MAK
       ;;
(NM002MAK)
       m_CondExec 00,EQ,NM002MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI3 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAM
       ;;
(NM002MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NM002MAG.BNM155BM
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MAM.BNM155DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 "BQ"
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_35_8 35 CH 8
 /FIELDS FLD_CH_44_2 44 CH 2
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_10 AND FLD_CH_44_2 NE CST_3_14 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_35_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MAN
       ;;
(NM002MAN)
       m_CondExec 00,EQ,NM002MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DU FICHIER LOG DE CAISE TRIE                                    
#  TRI4 *  R�CUP�RATION DU TYPE='1' (OPERATIONS ADMINISTRATIVES)               
# *******            ET REMISE EN BANQUE, OUTREC DE BQ* EN BQUE                
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAQ
       ;;
(NM002MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NM002MAG.BNM155BM
# ******* FIC LOG DE CAISSES (PAIEMENTS)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MAQ.BNM155HM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "BQ"
 /DERIVEDFIELD CST_1_11  "BQE"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_47_154 47 CH 154
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_43 1 CH 43
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_44_2 44 CH 2
 /CONDITION CND_1 FLD_CH_44_2 EQ CST_1_9 
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_44_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 200 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_43,CST_1_11,FLD_CH_47_154
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=NM002MAR
       ;;
(NM002MAR)
       m_CondExec 00,EQ,NM002MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#       *  TRI DES FICHIERS TYPE '0' ET TYPE '1                                
#  TRI5 *  PAR N�DE PI�CES.                                                    
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAT
       ;;
(NM002MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC TYPE 0 ET  1                                                     
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/NM002MAQ.BNM155HM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/NM002MAJ.BNM155CM
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/NM002MAM.BNM155DM
# ******* FIC LOG DE CAISSES TRI� PAR N�DE PI�CE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MAT.BNM155EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_5 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MAU
       ;;
(NM002MAU)
       m_CondExec 00,EQ,NM002MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM000 : CE PGM SE CHARGE D'ECRIRE LE FICHIER INTERFACE STANDARD            
#           FFTI00 AU FUR ET A MESURE DES DONN�ES DE GESTION CONTENUES         
#           DANS LE FICHIER LOG DE CAISSES FNM001.                             
#           *                                                                  
#           **** VERIFICATION DU TRAITEMENT DEJA EFFECTUE DANS RTHV15          
#           *                                                                  
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NM002MAX
       ;;
(NM002MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE (SOUS TABLES NM000/NM001/NM002/NM003               
# *****                      NM004/NM005/NM006/NM007/NM008/BQMAG)              
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# *****   TABLE DES ECS                                                        
#    RSFX00   : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ************** TABLES EN MISE A JOUR ***********                             
# *****   TABLE CONTROLE DES REMONTEES DE CAISSES                              
#    RSHV15   : NAME=RSHV15M,MODE=U - DYNAM=YES                                
# -X-NM002MR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV15 /dev/null
# *****                                                                        
#    RSGV11   : NAME=RSGV11M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A8} FNM001 ${DATA}/PTEM/NM002MAT.BNM155EM
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -t LSEQ -g +1 FFTI00 ${DATA}/PTEM/NM002MAX.BNM000AM
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d NEW,CATLG,DELETE -r 417 -t LSEQ -g +1 FFTI02 ${DATA}/PTEM/NM002MAX.BNM000GM
# ******  FICHIER DES CAISSES EN ERREUR                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/NM002MAX.BNM000BM
# ******  FICHIER POUR GENERATEUR D'ETAT (LISTE DE L'ETAT DES CAISSES)         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 INM001 ${DATA}/PTEM/NM002MAX.BNM000CM
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w IMPRIM IMPRIM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ******  PARAMETRE : SIMU ==> AUCUNE M.A.J                                    
# ******  PARAMETRE : REEL ==> M.A.J                                           
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/NM002MAX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM000 
       JUMP_LABEL=NM002MAY
       ;;
(NM002MAY)
       m_CondExec 04,GE,NM002MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNM001 : EXTRACTION DES CAISSES A COMPTABILISER                             
#           CREATION DU FICHIER POUR L'I.C.S  (INTERFACE COMPTABLE STD         
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBA PGM=BNM001     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBA
       ;;
(NM002MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **************  FICHIERS EN ENTREE ***************                           
#                                                                              
# ******* FIC LOG DE CAISSES + REPRISE(J-1) TRIE                               
       m_FileAssign -d SHR -g ${G_A9} FNM001 ${DATA}/PTEM/NM002MAT.BNM155EM
# ******  FICHIER DES CAISSES (CORRECTES ET EN ERREURS)                        
       m_FileAssign -d SHR -g ${G_A10} FFTI00 ${DATA}/PTEM/NM002MAX.BNM000AM
# ******  FICHIER DES CAISSES DACEM (CORRECTES ET EN ERREURS)                  
       m_FileAssign -d SHR -g ${G_A11} FFTI02 ${DATA}/PTEM/NM002MAX.BNM000GM
# ******* FICHIER CAISSES + CAISSES A REPRENDRE DE J(-1)                       
       m_FileAssign -d SHR -g ${G_A12} FREPRIS ${DATA}/PTEM/NM002MAX.BNM000BM
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
#                                                                              
# ******  FICHIER POUR EDITION VERS CHAINE EDITION NM005*                      
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FNM002 ${DATA}/PNCGM/F89.BNM001AM
# ******  FICHIER DES CAISSES A REPRENDRE POUR J(+1)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FNM003 ${DATA}/PNCGM/F89.BNM001BM
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PNCGM/F89.BNM001CM
# ******  FICHIER POUR (I.C.S) POUR COMPTA GCT DACEM SEULEMENT                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI03 ${DATA}/PNCGM/F89.BNM001DM
       m_ProgramExec BNM001 
#                                                                              
# ********************************************************************         
#         TRI PREPARATOIRE POUR ETAT INM001                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBD
       ;;
(NM002MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER POUR LISTE DES CAISSES RECYCLEES                             
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/NM002MAX.BNM000CM
# ******  FICHIER DESTINE AU GENERATEUR D'ETAT                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MBD.BNM000DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_21_3 21 CH 03
 /FIELDS FLD_BI_15_3 15 CH 03
 /FIELDS FLD_BI_18_3 18 CH 03
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_24_2 24 CH 02
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MBE
       ;;
(NM002MBE)
       m_CondExec 00,EQ,NM002MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050    CREATION D'UN FICHIER FCUMULS                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBG
       ;;
(NM002MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG00 /dev/null
#    RSEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG05 /dev/null
#    RSEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG10 /dev/null
#    RSEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG15 /dev/null
#    RSEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG25 /dev/null
#    RSEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/NM002MBD.BNM000DM
# ******  FICHIER FCUMULS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/NM002MBG.BNM000EM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM002MBH
       ;;
(NM002MBH)
       m_CondExec 04,GE,NM002MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBJ
       ;;
(NM002MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/NM002MBG.BNM000EM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NM002MBJ.BNM000FM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM002MBK
       ;;
(NM002MBK)
       m_CondExec 00,EQ,NM002MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   REPRISE : OUI         ETAT DES CAISSES EN RECYCLAGE                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBM
       ;;
(NM002MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/NM002MBD.BNM000DM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/NM002MBJ.BNM000FM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE TRAITEE : 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ******  EDITION                                                              
       m_OutputAssign -c 9 -w IEE200 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM002MBN
       ;;
(NM002MBN)
       m_CondExec 04,GE,NM002MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM002MBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NM002MBQ
       ;;
(NM002MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE FV001)               
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGM/F89.BNMCUMAM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002MBQ.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM002MBR
       ;;
(NM002MBR)
       m_CondExec 16,NE,NM002MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM002MZA
       ;;
(NM002MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM002MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
