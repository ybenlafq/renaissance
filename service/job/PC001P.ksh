#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC001P.ksh                       --- VERSION DU 08/10/2016 21:57
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/06 AT 08.05.50 BY BURTEC6                      
#    STANDARDS: P  JOBSET: PC001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE LA GATEWAY                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PC001PA
       ;;
(PC001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2013/07/06 AT 08.05.50 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: PC001P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'COMP. CARTES CADEAUX'                  
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PC001PAA
       ;;
(PC001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F07.PC001P
       m_FileAssign -d NEW,CATLG,DELETE -r 723 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGP/F07.BPC001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "H"
 /FIELDS FLD_BI_7_4 7 CH 4
 /FIELDS FLD_BI_4_2 4 CH 2
 /FIELDS FLD_BI_35_19 35 CH 19
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_BI_349_10 349 CH 10
 /FIELDS FLD_BI_1_2 1 CH 2
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_8 
 /KEYS
   FLD_BI_7_4 ASCENDING,
   FLD_BI_4_2 ASCENDING,
   FLD_BI_1_2 ASCENDING,
   FLD_BI_35_19 ASCENDING,
   FLD_BI_349_10 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 723 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PC001PAB
       ;;
(PC001PAB)
       m_CondExec 00,EQ,PC001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPC001                                                                
# --------------------------------------------------------------------         
#            MISE A JOUR DES TABLES RTPC00,RTPC01 ET RTPC02                    
#               ---- CE PGM APPELLE LE MODULE MPC000 -----                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PC001PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PC001PAD
       ;;
(PC001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSPC00   : NAME=RSPC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC00 /dev/null
#    RSPC01   : NAME=RSPC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC01 /dev/null
#    RSPC02   : NAME=RSPC02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC02 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A1} FPC001 ${DATA}/PNCGP/F07.BPC001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC001 
       JUMP_LABEL=PC001PAE
       ;;
(PC001PAE)
       m_CondExec 04,GE,PC001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  DELETE DES FICHIERS REMONT�S VIA GATEWAY                                  
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP PC001PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PC001PAG
       ;;
(PC001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC001PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PC001PAH
       ;;
(PC001PAH)
       m_CondExec 16,NE,PC001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CR�ATION _A VIDE D'UNE G�N�RATION SI PAS DE FICHIER                          
#  R�CUP�R� LA VEILLE.                                                         
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP PC001PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PC001PAJ
       ;;
(PC001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC CARTES CADEAUX PROSODIE                                          
       m_FileAssign -d SHR IN1 /dev/null
# ******  CR�ATION _A VIDE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 723 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.PC001P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PC001PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PC001PAK
       ;;
(PC001PAK)
       m_CondExec 16,NE,PC001PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
