#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FX100P.ksh                       --- VERSION DU 08/10/2016 23:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFX100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 97/02/14 AT 09.16.35 BY BURTEC6                      
#    STANDARDS: P  JOBSET: FX100P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FX100PA
       ;;
(FX100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    97/02/14 AT 09.16.35 BY BURTEC6                  
# *    JOBSET INFORMATION:    NAME...: FX100P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'RUBRIK BUDG  GA90'                     
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FX100PAA
       ;;
(FX100PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# ***********************************************                              
#   ALIMENTATION RUBRIQUE BUDGETAIRE DANS RTGA90                               
#  REPRISE: OUI                                                                
# ***********************************************                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSGA90   : NAME=RSGA90,MODE=(U,N) - DYNAM=YES                             
# -X-RSGA90   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA90 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFX100 
       JUMP_LABEL=FX100PAB
       ;;
(FX100PAB)
       m_CondExec 04,GE,FX100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FX100PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=FX100PAD
       ;;
(FX100PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c 9 -w FX100P SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FX100PAD.sysin
       m_ExecSQL -f SYSIN
#                                                                              
#   DEPENDANCE POUR PLAN                                                       
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=FX100PAE
       ;;
(FX100PAE)
       m_CondExec 04,GE,FX100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
