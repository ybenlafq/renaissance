#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SP110L.ksh                       --- VERSION DU 17/10/2016 18:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLSP110 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/06 AT 09.11.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: SP110L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSP140 :REGLES CREEES OU MODIFIEES CE JOUR  :                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SP110LA
       ;;
(SP110LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SP110LAA
       ;;
(SP110LAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP15L  : NAME=RSSP15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP15L /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00L  : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00L /dev/null
#  TABLE                                                                       
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE                                                                       
#    RTGA14L  : NAME=RSGA14L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14L /dev/null
#  TABLE                                                                       
#    RTGA30L  : NAME=RSGA30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30L /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
#  FICHIER EN SORTIE : ISP140AP DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 ISP140 ${DATA}/PTEM/SP110LAA.ISP140AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP140 
       JUMP_LABEL=SP110LAB
       ;;
(SP110LAB)
       m_CondExec 04,GE,SP110LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP140AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAD
       ;;
(SP110LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SP110LAA.ISP140AL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SP110LAD.ISP140BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_20_20 20 CH 20
 /KEYS
   FLD_BI_7_5 DESCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_20 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP110LAE
       ;;
(SP110LAE)
       m_CondExec 00,EQ,SP110LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP140CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAG
       ;;
(SP110LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00L  : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00L /dev/null
#    RTEG05L  : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05L /dev/null
#    RTEG10L  : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10L /dev/null
#    RTEG15L  : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15L /dev/null
#    RTEG25L  : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25L /dev/null
#    RTEG30L  : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30L /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/SP110LAD.ISP140BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/SP110LAG.ISP140CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP110LAH
       ;;
(SP110LAH)
       m_CondExec 04,GE,SP110LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAJ
       ;;
(SP110LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SP110LAG.ISP140CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SP110LAJ.ISP140DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP110LAK
       ;;
(SP110LAK)
       m_CondExec 00,EQ,SP110LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP140                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAM
       ;;
(SP110LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71L  : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71L /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00L  : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00L /dev/null
#    RTEG05L  : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05L /dev/null
#    RTEG10L  : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10L /dev/null
#    RTEG11L  : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11L /dev/null
#    RTEG15L  : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15L /dev/null
#    RTEG20L  : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20L /dev/null
#    RTEG25L  : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25L /dev/null
#    RTEG30L  : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30L /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/SP110LAD.ISP140BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/SP110LAJ.ISP140DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISP140 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP110LAN
       ;;
(SP110LAN)
       m_CondExec 04,GE,SP110LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSP141 : CODICS EN DEROGATION POUR TOUTES SOCIETES                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAQ
       ;;
(SP110LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RTSP15L  : NAME=RSSP15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTSP15L /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00L  : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00L /dev/null
#  TABLE                                                                       
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE                                                                       
#    RTGA14L  : NAME=RSGA14L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14L /dev/null
#  TABLE                                                                       
#    RTGA30L  : NAME=RSGA30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30L /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN SORTIE : ISP141AP DE 512 DE LONG                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 ISP141 ${DATA}/PTEM/SP110LAQ.ISP141AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP141 
       JUMP_LABEL=SP110LAR
       ;;
(SP110LAR)
       m_CondExec 04,GE,SP110LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (ISP141AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAT
       ;;
(SP110LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/SP110LAQ.ISP141AL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SP110LAT.ISP141BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_20_20 20 CH 20
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_PD_12_3 12 PD 3
 /KEYS
   FLD_BI_7_5 DESCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_20 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP110LAU
       ;;
(SP110LAU)
       m_CondExec 00,EQ,SP110LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS ISP141CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SP110LAX
       ;;
(SP110LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00L  : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00L /dev/null
#    RTEG05L  : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05L /dev/null
#    RTEG10L  : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10L /dev/null
#    RTEG15L  : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15L /dev/null
#    RTEG25L  : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25L /dev/null
#    RTEG30L  : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30L /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/SP110LAT.ISP141BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/SP110LAX.ISP141CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=SP110LAY
       ;;
(SP110LAY)
       m_CondExec 04,GE,SP110LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SP110LBA
       ;;
(SP110LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/SP110LAX.ISP141CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SP110LBA.ISP141DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SP110LBB
       ;;
(SP110LBB)
       m_CondExec 00,EQ,SP110LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : ISP141                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SP110LBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SP110LBD
       ;;
(SP110LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71L  : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71L /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00L  : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00L /dev/null
#    RTEG05L  : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05L /dev/null
#    RTEG10L  : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10L /dev/null
#    RTEG11L  : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11L /dev/null
#    RTEG15L  : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15L /dev/null
#    RTEG20L  : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20L /dev/null
#    RTEG25L  : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25L /dev/null
#    RTEG30L  : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30L /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/SP110LAT.ISP141BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/SP110LBA.ISP141DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
# *********FEDITION REPORT SYSOUT=(9,ISP141),RECFM=VA                          
       m_OutputAssign -c Z FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=SP110LBE
       ;;
(SP110LBE)
       m_CondExec 04,GE,SP110LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SP110LZA
       ;;
(SP110LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SP110LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
