#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP003P.ksh                       --- VERSION DU 08/10/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP003 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/12 AT 13.24.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AP003P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *************************************                                        
# *  DELETE DU FICHIER CUMUL DU MOIS                                           
# *************************************                                        
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP003PA
       ;;
(AP003PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       PROCSTEP=${PROCSTEP:-AP003PA}
       RUN=${RUN}
       JUMP_LABEL=AP003PAA
       ;;
(AP003PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AP003PAA.sysin
       m_UtilityExec
# ***************************************                                      
#                                                                              
# ***********************************                                          
# *   STEP AP003PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=AP003PAD
       ;;
(AP003PAD)
       m_CondExec ${EXAAF},NE,YES 
# ***************************************                                      
# ***************************************                                      
#  PRODUCTION DES FACTURES A21                                                 
#  REPRISE: OUI                                                                
# ******************************************                                   
# ***************************************                                      
#  BAP009  : PRODUCTION DES FACTURES A21   *                                   
#  REPRISE : OUI                           *                                   
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP16   : NAME=RSAP16,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSAP16 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSAP17   : NAME=RSAP17,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSAP17 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSBC12   : NAME=RSBC12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBC12 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSBC02   : NAME=RSBC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBC02 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSPM06   : NAME=RSPM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM06 /dev/null
# ******  TABLE ASSISTANCE A LA PERSONE                                        
#    RSPR10   : NAME=RSPR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR10 /dev/null
#  FICHIER DES FACTURES A2I POUR IMPRIMEUR  ( ZIP ET ENVOI GATEWAY  )          
       m_FileAssign -d NEW,CATLG,DELETE -r 700 -t LSEQ FAP009 ${DATA}/PXX0.F07.FAP009P
# ***********************************                                          
# AAK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":FAP009AP"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":FAP009P"",FAP009AP.TXT)                                       
#  ":FAP009P""                                                                 
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP009 
       JUMP_LABEL=AP003PAE
       ;;
(AP003PAE)
       m_CondExec 04,GE,AP003PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AP003PAG PROC=JVZIP     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=AP003PAG
       ;;
(AP003PAG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAK},NE,YES "
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileOverride -d NEW,CATLG,DELETE -r 700 -t LSEQ -s JVZIPU FICZIP ${DATA}/PXX0.F07.FAP009AP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AP003PAG.sysin
       JUMP_LABEL=AP003PAJ
       ;;
(AP003PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/AP003PAJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ***********************************                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
