#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MC001P.ksh                       --- VERSION DU 17/10/2016 18:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMC001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 14.09.57 BY BURTECA                      
#    STANDARDS: P  JOBSET: MC001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMCS10  GENERATION DE LA SYSIN D'UNLOAD                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MC001PA
       ;;
(MC001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'-1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MC001PAA
       ;;
(MC001PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC001PAA.sysin
# ****** FDATE                                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER                                                              
       m_FileAssign -d SHR -g +0 FDERTRTE ${DATA}/PXX0/F07.FMC001AP
#                                                                              
# ******  FICHIER SEQUENTIEL POUR SYSIN FASTUNLOAD                             
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPCSYS ${DATA}/PXX0/F07.FBMCS10S
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMCS10 
       JUMP_LABEL=MC001PAB
       ;;
(MC001PAB)
       m_CondExec 04,GE,MC001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : UNLOAD AVEC SYSIN G�N�R�E PAR BMCS10                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAD
       ;;
(MC001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_OutputAssign -c "*" SYSTSPRT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSUDUMP
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_OutputAssign -c "*" SYSABEND
# ******  TABLE LECTURE                                                        
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV10L  : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10L /dev/null
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.FC001UNP
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PXX0/F07.FBMCS10S
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  PGM : BMC010  --   EXTRACTION DE VENTES                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAG
       ;;
(MC001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
# ******  TABLE MAJ                                                            
#    RSMC00   : NAME=RSMC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC00 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FDERTRTE ${DATA}/PXX0/F07.FMC001AP
       m_FileAssign -d SHR -g ${G_A2} FDESC ${DATA}/PXX0/F07.FC001UNP
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDERTRTS ${DATA}/PXX0/F07.FMC001AP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC010 
       JUMP_LABEL=MC001PAH
       ;;
(MC001PAH)
       m_CondExec 04,GE,MC001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BMC000  --   MAJ DE LA TABLE RSMC01 A PARTIR DU FICHIER               
#                     PROVENANT D'INN0VENTE                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAJ
       ;;
(MC001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE MAJ                                                            
#    RSMC01   : NAME=RSMC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC01 /dev/null
# *****   FICHIER PROVENANT DE LA GATEWAY                                      
       m_FileAssign -d SHR -g +0 FVENTE ${DATA}/PXX0/FTP.F07.BMC000AP
# *****   FICHIER EN SORTIE                                                    
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC000 
       JUMP_LABEL=MC001PAK
       ;;
(MC001PAK)
       m_CondExec 04,GE,MC001PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMCS10  GENERATION DE LA SYSIN D'UNLOAD                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAM
       ;;
(MC001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC001PAM.sysin
# ****** FDATE                                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER                                                              
       m_FileAssign -d SHR -g ${G_A3} FDERTRTE ${DATA}/PXX0/F07.FMC001AP
#                                                                              
# ******  FICHIER SEQUENTIEL POUR SYSIN FASTUNLOAD                             
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPCSYS ${DATA}/PXX0/F07.FBMCS10P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMCS10 
       JUMP_LABEL=MC001PAN
       ;;
(MC001PAN)
       m_CondExec 04,GE,MC001PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : UNLOAD                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAQ
       ;;
(MC001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_OutputAssign -c "*" SYSTSPRT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSUDUMP
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_OutputAssign -c "*" SYSABEND
# ******  TABLE LECTURE                                                        
#    RSMC01   : NAME=RSMC01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC01 /dev/null
#    RSVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE10D  : NAME=RSVE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10D /dev/null
#    RSVE10L  : NAME=RSVE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10L /dev/null
#    RSVE10M  : NAME=RSVE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10M /dev/null
#    RSVE10O  : NAME=RSVE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10O /dev/null
#    RSVE10Y  : NAME=RSVE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10Y /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSVE11D  : NAME=RSVE11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11D /dev/null
#    RSVE11L  : NAME=RSVE11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11L /dev/null
#    RSVE11M  : NAME=RSVE11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11M /dev/null
#    RSVE11O  : NAME=RSVE11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11O /dev/null
#    RSVE11Y  : NAME=RSVE11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11Y /dev/null
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.FC010UNP
       m_FileAssign -d SHR -g ${G_A4} SYSIN ${DATA}/PXX0/F07.FBMCS10P
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  PGM : BMC011  --   EXTRACTION DE VENTES                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC001PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MC001PAT
       ;;
(MC001PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSVE02   : NAME=RSVE02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE02 /dev/null
#    RSVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
# ******  TABLE MAJ                                                            
#    RSMC00   : NAME=RSMC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC00 /dev/null
#    RSMC01   : NAME=RSMC01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC01 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIERS EN ENTREE VENANT DU F. UNLOAD PR�C�DENT                     
       m_FileAssign -d SHR -g ${G_A5} FDESC ${DATA}/PXX0/F07.FC010UNP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC011 
       JUMP_LABEL=MC001PAU
       ;;
(MC001PAU)
       m_CondExec 04,GE,MC001PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MC001PZA
       ;;
(MC001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
