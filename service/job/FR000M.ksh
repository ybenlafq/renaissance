#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FR000M.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFR000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/02 AT 15.47.16 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FR000M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BFR000  :                                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FR000MA
       ;;
(FR000MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+2'}
       G_A13=${G_A13:-'+3'}
       G_A14=${G_A14:-'+3'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FR000MAA
       ;;
(FR000MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER EN ENTR�E                                                    
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F89.FFR000AM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFR50 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV03   : NAME=RSGV03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV03 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPT99   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT99 /dev/null
#    RSTH10   : NAME=RSTH10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH10 /dev/null
#    RSTH14   : NAME=RSTH14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH14 /dev/null
#    RSTH15   : NAME=RSTH15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH15 /dev/null
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 531 -t LSEQ -g +1 FVENTE ${DATA}/PTEM/FR000MAA.FFRVTEAM
       m_FileAssign -d NEW,CATLG,DELETE -r 570 -t LSEQ -g +1 FCLIENT ${DATA}/PTEM/FR000MAA.FFRCLTAM
       m_FileAssign -d NEW,CATLG,DELETE -r 367 -t LSEQ -g +1 FCAISSE ${DATA}/PTEM/FR000MAA.FFRCAIAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFR000 
       JUMP_LABEL=FR000MAB
       ;;
(FR000MAB)
       m_CondExec 04,GE,FR000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFR005  :                                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FR000MAD
       ;;
(FR000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER EN ENTR�E                                                    
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F89.FFR000BM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFR50 /dev/null
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#    RTGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA41 /dev/null
#    RTPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPT99   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT99 /dev/null
#    RTTH10   : NAME=RSTH10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH10 /dev/null
#    RTTH14   : NAME=RSTH14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH14 /dev/null
#    RTTH15   : NAME=RSTH15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH15 /dev/null
#    RTVE02   : NAME=RSVE02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE02 /dev/null
#    RTVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE10 /dev/null
#    RTVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE11 /dev/null
#    RTVE14   : NAME=RSVE14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE14 /dev/null
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 531 -t LSEQ -g +1 FVENTE ${DATA}/PTEM/FR000MAD.FFRVTEBM
       m_FileAssign -d NEW,CATLG,DELETE -r 570 -t LSEQ -g +1 FCLIENT ${DATA}/PTEM/FR000MAD.FFRCLTBM
       m_FileAssign -d NEW,CATLG,DELETE -r 367 -t LSEQ -g +1 FCAISSE ${DATA}/PTEM/FR000MAD.FFRCAIBM
#                                                                              
# ***************************************************************              
#  TRI DES FICHIERS VENTE                                                      
#  REPRISE : OUI                                                               
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP FR000MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BFR005 
       JUMP_LABEL=FR000MAG
       ;;
(FR000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FR000MAA.FFRVTEAM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/FR000MAD.FFRVTEBM
       m_FileAssign -d NEW,CATLG,DELETE -r 531 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FR000MAG.FFRVTETM
# ***************************************************************              
#  TRI DES FICHIERS CLIENT                                                     
#  REPRISE : OUI                                                               
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP FR000MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_6 9 CH 6
 /FIELDS FLD_CH_16_6 16 CH 6
 /FIELDS FLD_CH_32_26 32 CH 26
 /KEYS
   FLD_CH_16_6 ASCENDING,
   FLD_CH_9_6 ASCENDING,
   FLD_CH_32_26 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FR000MAJ
       ;;
(FR000MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/FR000MAA.FFRCLTAM
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/FR000MAD.FFRCLTBM
       m_FileAssign -d NEW,CATLG,DELETE -r 570 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FR000MAJ.FFRCLTTM
# ***************************************************************              
#  TRI DES FICHIERS VENTE                                                      
#  REPRISE : OUI                                                               
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP FR000MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_6 9 CH 6
 /FIELDS FLD_CH_57_15 57 CH 15
 /FIELDS FLD_CH_16_6 16 CH 6
 /FIELDS FLD_CH_53_3 53 CH 3
 /KEYS
   FLD_CH_16_6 ASCENDING,
   FLD_CH_9_6 ASCENDING,
   FLD_CH_57_15 ASCENDING,
   FLD_CH_53_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FR000MAM
       ;;
(FR000MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FR000MAA.FFRCAIAM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/FR000MAD.FFRCAIBM
       m_FileAssign -d NEW,CATLG,DELETE -r 367 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FR000MAM.FFRCAITM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_6 9 CH 6
 /FIELDS FLD_CH_16_6 16 CH 6
 /FIELDS FLD_CH_32_26 32 CH 26
 /KEYS
   FLD_CH_16_6 ASCENDING,
   FLD_CH_9_6 ASCENDING,
   FLD_CH_32_26 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FR000MAN
       ;;
(FR000MAN)
       m_CondExec 00,EQ,FR000MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFR011  : CONSTITUTION DES FICHIERS POUR ENVOIS SUR PORTAIL FRANCHI         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FR000MAQ
       ;;
(FR000MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER PARAM                                                        
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F89.FFR000CM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTBS02   : NAME=RSBS02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS02 /dev/null
#    RTFM95   : NAME=RSFM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFM95 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPT99   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT99 /dev/null
# ******  FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g ${G_A7} FVENTE ${DATA}/PTEM/FR000MAG.FFRVTETM
       m_FileAssign -d SHR -g ${G_A8} FCLIENT ${DATA}/PTEM/FR000MAJ.FFRCLTTM
       m_FileAssign -d SHR -g ${G_A9} FCAISSE ${DATA}/PTEM/FR000MAM.FFRCAITM
#                                                                              
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 531 -t LSEQ -g +1 FVENTES ${DATA}/PXX0/F89.FFRVTEPM
       m_FileAssign -d NEW,CATLG,DELETE -r 570 -t LSEQ -g +1 FCLIENTS ${DATA}/PXX0/F89.FFRCLTPM
       m_FileAssign -d NEW,CATLG,DELETE -r 367 -t LSEQ -g +1 FCAISSES ${DATA}/PXX0/F89.FFRCAIPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFR011 
       JUMP_LABEL=FR000MAR
       ;;
(FR000MAR)
       m_CondExec 04,GE,FR000MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTFR000M                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FR000MAT
       ;;
(FR000MAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FR000MAT.FTFR000M
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFR000M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FR000MAX
       ;;
(FR000MAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FR000MAT.FTFR000M(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTFR000M                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FR000MBA
       ;;
(FR000MBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MBA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A11} SYSOUT ${DATA}/PTEM/FR000MAT.FTFR000M
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFR000M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FR000MBD
       ;;
(FR000MBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MBD.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FR000MAT.FTFR000M(+2),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTFR000M                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MBG PGM=EZACFSM1   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FR000MBG
       ;;
(FR000MBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MBG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A13} SYSOUT ${DATA}/PTEM/FR000MAT.FTFR000M
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFR000M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FR000MBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FR000MBJ
       ;;
(FR000MBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MBJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FR000MAT.FTFR000M(+3),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FR000MZA
       ;;
(FR000MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FR000MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
