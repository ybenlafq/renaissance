#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM1A2L.ksh                       --- VERSION DU 08/10/2016 13:49
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLNM1A2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/09/13 AT 10.32.09 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM1A2L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#       *  TRI CUMUL EN CAS DE PLANTAGE DE LA CHAINE NM002L                    
#  TRI1 *                                                                      
# *******                                                                      
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NM1A2LA
       ;;
(NM1A2LA)
#
#NM1A2LAM
#NM1A2LAM Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#NM1A2LAM
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2000/09/13 AT 10.32.08 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: NM1A2L                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-NM002L'                            
# *                           APPL...: IMPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NM1A2LAA
       ;;
(NM1A2LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC LOG DE CAISSES                                                   
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM155AL
# ******* FIC DE CUMUL EN CAS DE PLANTAGE DU TRAITEMENT LA VEILLE              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.BNMCUMAL
# ******* FIC DE CUMUL POUR (J+1)                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PNCGL/F61.BNMCUMAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM1A2LAB
       ;;
(NM1A2LAB)
       m_CondExec 00,EQ,NM1A2LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
#   - FIC1 :                                                                   
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION POUR I.C.S                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM1A2LAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM1A2LAD
       ;;
(NM1A2LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
# ******** RECREATION DU NOUVEAU FIC (FTICSL I.C.S LILLE)                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGL/F61.BNM001CL
# ******** RECREATION DU NOUVEAU FIC (FTICSB I.C.S DACEM)                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT2 ${DATA}/PNCGL/F61.BNM001DL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM1A2LAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM1A2LAE
       ;;
(NM1A2LAE)
       m_CondExec 16,NE,NM1A2LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM1A2LAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
