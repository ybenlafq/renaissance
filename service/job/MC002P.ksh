#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MC002P.ksh                       --- VERSION DU 17/10/2016 18:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMC002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 10.27.56 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MC002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BMC015  --   RECUP DES CLIENTS SANS ADRESSE MAIL DANS LA              
#                --   TABLE RTMC00                                             
# *------------------------------------------------------------------*         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MC002PA
       ;;
(MC002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MC002PAA
       ;;
(MC002PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSMC00   : NAME=RSMC00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC00 /dev/null
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FEXTRACT ${DATA}/PXX0/F07.FMC002AP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC015 
       JUMP_LABEL=MC002PAB
       ;;
(MC002PAB)
       m_CondExec 04,GE,MC002PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * ATTENTION CE PROGRAMME SE SERT DE DDF POUR ALLER CHERCHER  *******         
# *           DES INFOS SUR SIEBEL                             *******         
# ********************************************************************         
#  PGM : BMC016  --   RECHERCHE DES ADRESSES MAIL DES CLIENTS REMONT�E         
#                --   PAR LE BMC015                                            
# *------------------------------------------------------------------*         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC002PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MC002PAD
       ;;
(MC002PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} FEXTRACE ${DATA}/PXX0/F07.FMC002AP
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FEXTRACS ${DATA}/PXX0/F07.FMC002BP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC016 
       JUMP_LABEL=MC002PAE
       ;;
(MC002PAE)
       m_CondExec 04,GE,MC002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BMC017  --   MISE A JOUR DES MAIL DANS LA TABLE RTMC00                
# *------------------------------------------------------------------*         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC002PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MC002PAG
       ;;
(MC002PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ******  TABLE EN ECRITURE                                                    
#    RSMC00   : NAME=RSMC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC00 /dev/null
# *****   FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FEXTRACT ${DATA}/PXX0/F07.FMC002BP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC017 
       JUMP_LABEL=MC002PAH
       ;;
(MC002PAH)
       m_CondExec 04,GE,MC002PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES POUR BMC018.                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC002PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MC002PAJ
       ;;
(MC002PAJ)
       m_CondExec ${EXAAP},NE,YES 
# ******  TABLE LECTURE                                                        
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
#    RSGV11   : NAME=RSGV11P,MODE=I - DYNAM=YES                                
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
#    RSMC01   : NAME=RSMC01,MODE=I - DYNAM=YES                                 
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/MC002PAJ.UNMC02AP
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/MC002PAJ.UNMC02BP
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/MC002PAJ.UNMC02CP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC002PAJ.sysin
       
       m_DBHpuUnload -f SYSIN
# *******************************************************************          
#  PGM : BMC018  --   M A J DATE TOPAGE ET DATE ANNULATION DE RTMC00           
# *------------------------------------------------------------------*         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC002PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MC002PAM
       ;;
(MC002PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE LECTURE                                                        
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  TABLE EN ECRITURE                                                    
#    RSMC00   : NAME=RSMC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMC00 /dev/null
# ****** FICHIER EN ENTR�E                                                     
       m_FileAssign -d SHR -g ${G_A3} FDESCA ${DATA}/PTEM/MC002PAJ.UNMC02AP
       m_FileAssign -d SHR -g ${G_A4} FDESCT ${DATA}/PTEM/MC002PAJ.UNMC02BP
       m_FileAssign -d SHR -g ${G_A5} FDESCS ${DATA}/PTEM/MC002PAJ.UNMC02CP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMC018 
       JUMP_LABEL=MC002PAN
       ;;
(MC002PAN)
       m_CondExec 04,GE,MC002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MC002PZA
       ;;
(MC002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
