#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BHE61D.ksh                       --- VERSION DU 13/10/2016 18:48
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDBHE61 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.03.35 BY BURTEC6                      
#    STANDARDS: P  JOBSET: BHE61D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BHE61DA
       ;;
(BHE61DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BHE61DAA
       ;;
(BHE61DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DAA.sysin
       m_UtilityExec
# ********************************************************************         
#           VERIFY DES FICHIERS VSAM                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAG
       ;;
(BHE61DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER CICS (ENTREE)                                                 
       m_FileAssign -d SHR FJHE60E ${DATA}/PEX0.F91.JHE601D
#                                                                              
# ****** FICHIER DE L ETAT JHE601 (SORTIE)                                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 FJHE60S ${DATA}/PTEM/BHE61DAG.FJHE011D
       m_ProgramExec BHE601 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER ISSU DU FJHE011P                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAJ
       ;;
(BHE61DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/BHE61DAG.FJHE011D
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/BHE61DAJ.FJHE012D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_32_7 32 CH 07
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_17_8 17 CH 8
 /KEYS
   FLD_CH_26_5 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_17_8 DESCENDING
 /* Record Type = F  Record Length = 243 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BHE61DAK
       ;;
(BHE61DAK)
       m_CondExec 00,EQ,BHE61DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROGRAME  PGM BHE602                                                        
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAM PGM=BHE602     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAM
       ;;
(BHE61DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ****** FICHIER  (ENTREE)                                                     
       m_FileAssign -d SHR -g ${G_A2} FJHE60E ${DATA}/PTEM/BHE61DAJ.FJHE012D
#                                                                              
# ****** FICHIER DE L ETAT JHE601 (SORTIE)                                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 243 -t LSEQ -g +1 FJHE60S ${DATA}/PXX0/F91.FJHE013D.ENVOI.CSV
       m_ProgramExec BHE602 
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER VERS MAIL VIA XFB-GATEWAY                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FJHE013D                                                            
#          DATAEND                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAQ PGM=PGMSVC34   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAQ
       ;;
(BHE61DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DAQ.sysin
       m_UtilityExec
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTBHE61D                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAT
       ;;
(BHE61DAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BHE61DAT.FTBHE61D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBHE61D                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BHE61DAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DAX
       ;;
(BHE61DAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.BHE61DAT.FTBHE61D(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BHE61DBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DBA
       ;;
(BHE61DBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BHE61DZA
       ;;
(BHE61DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BHE61DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
