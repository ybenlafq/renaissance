#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB400F.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFRB400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.14.59 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB400F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRB400                                                                      
#  TRAITEMENT DU FICHIER RE�U DE L'APPLICATION FLUIID                          
#  ENVOIS DES MESSAGES VIA MQ VERS SIEBEL                                      
#  REPRISE :                                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB400FA
       ;;
(RB400FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=RB400FAA
       ;;
(RB400FAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** EN PROVENANCE DE LA GATEWAY                                           
       m_FileAssign -d SHR FRB400 ${DATA}/PXX0/FTP.F99.BRB400AF
#                                                                              
# *****                                                                        
#                                                                              
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB400FAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          

       m_ProgramExec -b BRB400 
       JUMP_LABEL=RB400FAD
       ;;
(RB400FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# ******  SUPPRESSION DES FICHIERS                                             
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB400FAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB400FAE
       ;;
(RB400FAE)
       m_CondExec 16,NE,RB400FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
#   - FIC1 : BRB400AF                                                          
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB400FAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB400FAG
       ;;
(RB400FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 5909 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.BRB400AF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB400FAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB400FAH
       ;;
(RB400FAH)
       m_CondExec 16,NE,RB400FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
