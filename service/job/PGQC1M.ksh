#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PGQC1M.ksh                       --- VERSION DU 08/10/2016 13:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPGQC1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/12/29 AT 16.59.51 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PGQC1M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PROG : BQC904          SS TABLE DELAI                                       
#  EPURATION RTQC01 LIGNES HISTORIQUES                                         
#  EPURATION RTQC02                                                            
#  EPURATION RTQC03                                                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PGQC1MA
       ;;
(PGQC1MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PGQC1MAA
       ;;
(PGQC1MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE             -- VUE RVGA01CG                        
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******* TABLE CARTES T VENTES LIVREES -- VUE RVQC0100                        
#    RSQC01M  : NAME=RSQC01M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC01M /dev/null
# ******* TABLE CARTES T VENTES EMPORTEES -- VUE RVQC0200                      
#    RSQC02M  : NAME=RSQC02M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC02M /dev/null
# ******* TABLE CARTES T SAV            -- VUE RVQC0300                        
#    RSQC03M  : NAME=RSQC03M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSQC03M /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER SOCIETE                                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC904 
       JUMP_LABEL=PGQC1MAB
       ;;
(PGQC1MAB)
       m_CondExec 04,GE,PGQC1MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTQC01                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1MAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1MAD
       ;;
(PGQC1MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 171 -g +1 SYSREC01 ${DATA}/PXX0/F89.QC01UNMA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1MAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC01M                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1MR0                                     
#                  VERIFIER LE BACKOUT RPGQC1M ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1MAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1MAM
       ;;
(PGQC1MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 115 -g +1 SYSREC01 ${DATA}/PXX0/F89.QC02UNMA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1MAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC02M                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1MR1                                     
#                  VERIFIER LE BACKOUT RPGQC1M ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1MAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1MAX
       ;;
(PGQC1MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 139 -g +1 SYSREC01 ${DATA}/PXX0/F89.QC03UNMA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1MAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSQC03M                                       
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PGQC1MR2                                     
#                  VERIFIER LE BACKOUT RPGQC1M ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGQC1MBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PGQC1MZA
       ;;
(PGQC1MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGQC1MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
