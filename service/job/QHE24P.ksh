#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QHE24P.ksh                       --- VERSION DU 17/10/2016 18:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQHE24 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/10 AT 15.41.58 BY BURTECA                      
#    STANDARDS: P  JOBSET: QHE24P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   CONCAT FICHIER PGM BHE200                                                  
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QHE24PA
       ;;
(QHE24PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+2'}
       G_A15=${G_A15:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=QHE24PAA
       ;;
(QHE24PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FHE200AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FHE200AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FHE200AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FHE200AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FHE200AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FHE200AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FHE200TP
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QHE24PAB
       ;;
(QHE24PAB)
       m_CondExec 00,EQ,QHE24PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CONCAT FICHIER PGM BHE210                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAD
       ;;
(QHE24PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FHE210AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FHE210AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FHE210AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FHE210AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FHE210AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FHE210AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FHE210TP
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QHE24PAE
       ;;
(QHE24PAE)
       m_CondExec 00,EQ,QHE24PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CONCAT FICHIER PGM BHE220                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAG
       ;;
(QHE24PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FHE220AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FHE220AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FHE220AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FHE220AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FHE220AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FHE220AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FHE220TP
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QHE24PAH
       ;;
(QHE24PAH)
       m_CondExec 00,EQ,QHE24PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CONCAT FICHIER PGM BHE230                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAJ
       ;;
(QHE24PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FHE230AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FHE230AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FHE230AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FHE230TP
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QHE24PAK
       ;;
(QHE24PAK)
       m_CondExec 00,EQ,QHE24PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHE240 : EXTRACTION                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAM
       ;;
(QHE24PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FHE200I ${DATA}/PXX0/F07.FHE200TP
       m_FileAssign -d SHR -g ${G_A2} FHE210I ${DATA}/PXX0/F07.FHE210TP
       m_FileAssign -d SHR -g ${G_A3} FHE220I ${DATA}/PXX0/F07.FHE220TP
       m_FileAssign -d SHR -g ${G_A4} FHE230I ${DATA}/PXX0/F07.FHE230TP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A5} FHE200O ${DATA}/PXX0/F07.FHE200TP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A6} FHE210O ${DATA}/PXX0/F07.FHE210TP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A7} FHE220O ${DATA}/PXX0/F07.FHE220TP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A8} FHE230O ${DATA}/PXX0/F07.FHE230TP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FHE240O ${DATA}/PXX0/F07.FHE240TP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHE240 
       JUMP_LABEL=QHE24PAN
       ;;
(QHE24PAN)
       m_CondExec 04,GE,QHE24PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTQHE00                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.FHE200TP(0)' +                                                 
# entrees_&LMON.&YR4..csv                                                      
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTQHE24P,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE00                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=FTP,PARM='(EXIT'                                          
#                                                                              
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FTQHE24P,MODE=I                                          
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTQHE00                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABJ      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.FHE210TP(0)' +                                                 
# sorties_&LMON.&YR4..csv                                                      
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTQHE24P,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE00                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABO      STEP  PGM=FTP,PARM='(EXIT'                                          
#                                                                              
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FTQHE24P,MODE=I                                          
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTQHE00                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABT      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.FHE220TP(0)' +                                                 
# stocks_&LMON.&YR4..csv                                                       
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTQHE24P,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE00                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABY      STEP  PGM=FTP,PARM='(EXIT'                                          
#                                                                              
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FTQHE24P,MODE=I                                          
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTQHE00                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# ACD      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.FHE240TP(0)' +                                                 
# ferraillage_&LMON.&YR4..csv                                                  
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTQHE24P,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE00                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ACI      STEP  PGM=FTP,PARM='(EXIT'                                          
#                                                                              
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FTQHE24P,MODE=I                                          
# ***************************************************************              
#  REPRISE : OUI                                                               
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAQ
       ;;
(QHE24PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE24PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/QHE24PAQ.FTQHE24P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAT PGM=JVMLDM76   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAT
       ;;
(QHE24PAT)
       m_CondExec ${EXABE},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A9} DD1 ${DATA}/PXX0/F07.FHE200TP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.FHE24ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE24PAT.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTQHE24P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PAX
       ;;
(QHE24PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE24PAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A14} SYSOUT ${DATA}/PTEM/QHE24PAQ.FTQHE24P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTQHE24P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QHE24PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PBA
       ;;
(QHE24PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE24PBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.QHE24PAQ.FTQHE24P(+2),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QHE24PZA
       ;;
(QHE24PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QHE24PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
