#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC31T.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PTGQC31 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/09 AT 08.50.50 BY BURTECA                      
#    STANDARDS: P  JOBSET: GQC31T                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GQC31TA
       ;;
(GQC31TA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/06/09 AT 08.50.50 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GQC31T                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'XXXXXXXXXXXXXXX'                       
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GQC31TAA
       ;;
(GQC31TAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FBC260AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FBC260AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FBC260AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FBC260AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FBC260AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FBC260AY
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FBC260CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC31TAB
       ;;
(GQC31TAB)
       m_CondExec 00,EQ,GQC31TAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAD
       ;;
(GQC31TAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC31TAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GQC31TAD.FTGQC31T
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP DU FICHIER MERGE                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=FBC260CP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=GQC31ZIP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#          DATAEND                                                             
#          FILE  NAME=FTGQC31T,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAG PGM=JVMLDM76   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAG
       ;;
(GQC31TAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F99.FBC260CP
# ******* FICHIER EN SORTIE DE ZIP                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F99.GQC31ZIP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC31TAG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATTAGE SYSIN ENVOI FTP                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAJ
       ;;
(GQC31TAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GQC31TAD.FTGQC31T
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC31TAJ.FTGQC32T
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_4_8  " "
 /DERIVEDFIELD CST_1_3 "CARTET"
 /DERIVEDFIELD CST_2_6  "PXX0.F99.GQC31ZIP(0)"
 /DERIVEDFIELD CST_0_4  "PUT "
 /DERIVEDFIELD CST_BLANK_3_7  X"27"
 /DERIVEDFIELD CST_BLANK_1_5  X"27"
 /FIELDS FLD_CH_2_6 2 CH 6
 /CONDITION CND_1 FLD_CH_2_6 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_4,CST_BLANK_1_5,CST_2_6,CST_BLANK_3_7,CST_4_8
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC31TAK
       ;;
(GQC31TAK)
       m_CondExec 00,EQ,GQC31TAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTGQC31T                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAM
       ;;
(GQC31TAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC31TAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC31TAJ.FTGQC32T(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATTAGE SYSIN ENVOI FTP BIS POUR DEPOSE SUR VFD DANS GATEWAY             
#  POUR Z1I351. FICHIER G�N�R� AVEC UN 'Z' DEVANT POUR LE TEST COT�            
#  GATEWAY ET R�CUP�RER LE BON FICHIER POUR LE VFD                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAQ
       ;;
(GQC31TAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GQC31TAD.FTGQC31T
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC31TAQ.FTGQC34T
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_BLANK_3_7  X"27"
 /DERIVEDFIELD CST_2_6  "PXX0.F99.GQC31ZIP(0)"
 /DERIVEDFIELD CST_0_4  "PUT "
 /DERIVEDFIELD CST_4_8  " Z"
 /DERIVEDFIELD CST_1_3 "CARTET"
 /DERIVEDFIELD CST_BLANK_1_5  X"27"
 /FIELDS FLD_CH_2_6 2 CH 6
 /CONDITION CND_1 FLD_CH_2_6 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
 /* MT_ERROR (unknown field) ',X'7D',C' Z' */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_4,CST_BLANK_1_5,CST_2_6,CST_BLANK_3_7,CST_4_8
 /* MT_ERROR (unknown field) ',X'7D',C' Z' */
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC31TAR
       ;;
(GQC31TAR)
       m_CondExec 00,EQ,GQC31TAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTGQC31T                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC31TAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TAT
       ;;
(GQC31TAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC31TAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQC31TAQ.FTGQC34T(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC31TZA
       ;;
(GQC31TZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC31TZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
