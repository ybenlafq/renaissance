#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB100P.ksh                       --- VERSION DU 08/10/2016 22:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/09/25 AT 09.20.49 BY BURTECA                      
#    STANDARDS: P  JOBSET: RB100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BRB100 : EPURATION DE LA GEF                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#  PGM BRB100 : INTEGRATION DU FICHIER DANS LES BASES                          
#               ET FORMATTAGE D UN FICHIER ANOMALIES A RECYCLER                
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB100PA
       ;;
(RB100PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=RB100PAA
       ;;
(RB100PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#  DEPENDANCE HORAIRE 22 H 00 ET 5 H 00                                        
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* VALEURS FSIMU : S = SIMU ==> PAS DE MISE A JOUR                      
# *******               : R = REEL ==> MISE A JOUR                             
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/RB100P
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ANOMALIES                                                    
       m_OutputAssign -c 9 -w FRBANO02 FEFREJ
# ******  TABLES EN LECTURE                                                    
#    RTPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT01 /dev/null
#    RTRB10   : NAME=RSRB10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB10 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTEF10   : NAME=RSEF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEF10 /dev/null
# ******  TABLES EN M.A.J.                                                     
#    RTEF00   : NAME=RSEF00,MODE=U - DYNAM=YES                                 
# -X-RSEF00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTEF00 /dev/null
#    RTEF11   : NAME=RSEF11,MODE=U - DYNAM=YES                                 
# -X-RSEF11   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTEF11 /dev/null
#    RTRB01   : NAME=RSRB01,MODE=U - DYNAM=YES                                 
# -X-RSRB01   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTRB01 /dev/null
#    RTHE10   : NAME=RSHE10,MODE=U - DYNAM=YES                                 
# -X-RSHE10   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTHE10 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
# -X-RSGS10   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
# -X-RSGS40   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTGS40 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB100 
       JUMP_LABEL=RB100PAB
       ;;
(RB100PAB)
       m_CondExec 04,GE,RB100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
