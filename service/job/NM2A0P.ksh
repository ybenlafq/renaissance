#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM2A0P.ksh                       --- VERSION DU 08/10/2016 17:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNM2A0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/06/15 AT 17.58.15 BY BURTEC3                      
#    STANDARDS: P  JOBSET: NM2A0P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#        * REPRO CUMUL POUR (J+1)  EN CAS DE PLANTAGE DE NM020P                
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NM2A0PA
       ;;
(NM2A0PA)
#
#NM2A0PAJ
#NM2A0PAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#NM2A0PAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2000/06/15 AT 17.58.15 BY BURTEC3                
# *    JOBSET INFORMATION:    NAME...: NM2A0P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-NM020P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NM2A0PAA
       ;;
(NM2A0PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC EN PROVENANCE DE L'INTERFACE CAISSES NEM                         
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGP/F07.BNM001CP
# ******* FIC DE REPRISE L POUR (J+1)                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGP/F07.BNMCUMBP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM2A0PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM2A0PAB
       ;;
(NM2A0PAB)
       m_CondExec 16,NE,NM2A0PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        * CREATION D'UN FICHIER VIDE POUR LIBERATION NETTING FG00FP           
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM2A0PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM2A0PAD
       ;;
(NM2A0PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC EN PROVENANCE DE L'INTERFACE CAISSES NEM                         
       m_FileAssign -d SHR IN1 /dev/null
# ******* FIC DE REPRISE L POUR (J+1)                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -g +1 OUT1 ${DATA}/PNCGP/F07.BNM002AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM2A0PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM2A0PAE
       ;;
(NM2A0PAE)
       m_CondExec 16,NE,NM2A0PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM2A0PAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
