#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PC040P.ksh                       --- VERSION DU 08/10/2016 13:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPC040 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/06/02 AT 11.35.56 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PC040P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPC040  ETAT JUSTIFICATIF DE COMPTE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PC040PA
       ;;
(PC040PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2008/06/02 AT 11.35.56 BY BURTEC2                
# *    JOBSET INFORMATION:    NAME...: PC040P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ETAT JUSTIF COMPTE'                    
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PC040PAA
       ;;
(PC040PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN M.A.J.                                                      
#    RSPC01   : NAME=RSPC01,MODE=U - DYNAM=YES                                 
# -X-RSPC01   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSPC01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RSPC02   : NAME=RSPC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPC02 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPC040 
       JUMP_LABEL=PC040PAB
       ;;
(PC040PAB)
       m_CondExec 04,GE,PC040PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
