#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC10D.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGQC10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 18.12.36 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC10D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BQC140 : EXTRACTION DES DONNEES VENTES TOPEES LIVREES                      
#   REPRISE: LA REPRISE SERA FAITE DEMAIN MATIN                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC10DA
       ;;
(GQC10DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC10DAA
       ;;
(GQC10DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAM ASSOCIES AUX FAMILLES                                    
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ADRESSES                                                       
#    RSGV02   : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSPO01   : NAME=RSPO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPO01 /dev/null
#    RSGA41   : NAME=RSGA41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/GQC10D1
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GQC10D2
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010 ${DATA}/PXX0/F91.BQC010AD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC011 ${DATA}/PXX0/F91.BQC011AD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012 ${DATA}/PXX0/F91.BQC012AD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FQC013 ${DATA}/PXX0/F91.BQC013AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC140 
       JUMP_LABEL=GQC10DAB
       ;;
(GQC10DAB)
       m_CondExec 04,GE,GQC10DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC141 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAD
       ;;
(GQC10DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA02   : NAME=RSGA02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV02   : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER ALIMENT� EN CAS DE REPRISE                                   
       m_FileAssign -d SHR -g +0 FCREA ${DATA}/PXX0/F91.FCREAAPD
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
#                                                                              
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC014 ${DATA}/PXX0/F91.BQC014AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC141 
       JUMP_LABEL=GQC10DAE
       ;;
(GQC10DAE)
       m_CondExec 04,GE,GQC10DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAG
       ;;
(GQC10DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.BQC014AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.BQC016AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_88_7 88 PD 7
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_PD_88_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10DAH
       ;;
(GQC10DAH)
       m_CondExec 00,EQ,GQC10DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC171 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAP      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******* TABLES EN LECTURE                                                    
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01D,MODE=I                                 
# RSGQ01   FILE  DYNAM=YES,NAME=RSGQ01D,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01D,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******* FDATE JJMMSSAA                                                       
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******* CODE SOCIETE                                                         
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDPM                                         
# * FICHIER EN ENTREE                                                          
# FQC014   FILE  NAME=BQC016AD,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC020   FILE  NAME=BQC020AD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC171) PLAN(BQC171D)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAU      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC020AD,MODE=I                                          
# SORTOUT  FILE  NAME=BQC021AD,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(5,19,A),FORMAT=CH                                              
#  RECORD TYPE=F,LENGTH=50                                                     
#          DATAEND                                                             
# ********************************************************************         
#   BQC142 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA00   FILE  DYNAM=YES,NAME=RSGA00D,MODE=I                                 
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01D,MODE=I                                 
# RSGA03   FILE  DYNAM=YES,NAME=RSGA03D,MODE=I                                 
# RSGS65   FILE  DYNAM=YES,NAME=RSGS65D,MODE=I                                 
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02D,MODE=I                                 
# RSGV10   FILE  DYNAM=YES,NAME=RSGV10D,MODE=I                                 
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDPM                                         
# ******** FICHIER ALIMENT� EN CAS DE REPRISE                                  
# FCREA    FILE  NAME=FCREAAPD,MODE=I                                          
# ** FICHIER EN ENTREE                                                         
# FQC020   FILE  NAME=BQC021AD,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC015   FILE  NAME=BQC015AD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC142) PLAN(BQC142D)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABE      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC015AD,MODE=I                                          
# SORTOUT  FILE  NAME=BQC022AD,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,7,A),FORMAT=CH                                               
#  RECORD TYPE=F,LENGTH=100                                                    
#          DATAEND                                                             
# ********************************************************************         
#   BQC161 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# ABJ      STEP  PGM=IKJEFT01                                                  
# *                                                                            
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******* TABLES EN LECTURE                                                    
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01D,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01D,MODE=I                                 
# RSQC06   FILE  DYNAM=YES,NAME=RSQC06D,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDPM                                         
# * FICHIER EN ENTREE                                                          
# FQC015   FILE  NAME=BQC022AD,MODE=I                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC161) PLAN(BQC161D)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   BQC145 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAJ PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAJ
       ;;
(GQC10DAJ)
       m_CondExec ${EXABO},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A2} FQC010E ${DATA}/PXX0/F91.BQC010AD
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010S ${DATA}/PXX0/F91.BQC145AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC145 
       JUMP_LABEL=GQC10DAK
       ;;
(GQC10DAK)
       m_CondExec 04,GE,GQC10DAJ ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAM PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAM
       ;;
(GQC10DAM)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F91.BQC012AD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC10DAM.BQC012CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_1 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10DAN
       ;;
(GQC10DAN)
       m_CondExec 00,EQ,GQC10DAM ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC146 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAQ PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAQ
       ;;
(GQC10DAQ)
       m_CondExec ${EXABY},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A4} FQC012E ${DATA}/PTEM/GQC10DAM.BQC012CD
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012S ${DATA}/PXX0/F91.BQC146AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC146 
       JUMP_LABEL=GQC10DAR
       ;;
(GQC10DAR)
       m_CondExec 04,GE,GQC10DAQ ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * REMISE _A BLANCE DU FICHIER CONTENANT LES PARAM DE REPRISE                  
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10DAT PGM=IDCAMS     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DAT
       ;;
(GQC10DAT)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 ${DATA}/CORTEX4.P.MTXTFIX1/GQC10D01
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FCREAAPD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10DAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GQC10DAU
       ;;
(GQC10DAU)
       m_CondExec 16,NE,GQC10DAT ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC10DZA
       ;;
(GQC10DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
