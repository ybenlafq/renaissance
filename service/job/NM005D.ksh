#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM005D.ksh                       --- VERSION DU 09/10/2016 05:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDNM005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/12/29 AT 09.38.39 BY BURTEC6                      
#    STANDARDS: P  JOBSET: NM005D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BNM010 : EDITION DE VERIF DE CAISSE                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER EN PROVENANCE DE NM002                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM005DA
       ;;
(NM005DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM005DAA
       ;;
(NM005DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
# *** FICHIER ISSU DE LA CHAINE NM002    (N.E.M)                               
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM010                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DAA.BNM010AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_8 1 CH 8
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_9_8 9 CH 8
 /FIELDS FLD_BI_26_3 26 CH 3
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING,
   FLD_BI_9_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DAB
       ;;
(NM005DAB)
       m_CondExec 00,EQ,NM005DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM010 .ETAT DE VERIFICATION DE CAISSE                                 
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAD
       ;;
(NM005DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PTEM/NM005DAA.BNM010AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM010 ${DATA}/PTEM/NM005DAD.BNM010BD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM010 
       JUMP_LABEL=NM005DAE
       ;;
(NM005DAE)
       m_CondExec 04,GE,NM005DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER INM010AD                                                     
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAG
       ;;
(NM005DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER EN ENTREE                          
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NM005DAD.BNM010BD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DAG.INM010BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_24_3 24 CH 3
 /FIELDS FLD_BI_21_3 21 CH 3
 /FIELDS FLD_BI_7_8 07 CH 8
 /FIELDS FLD_BI_18_3 18 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DAH
       ;;
(NM005DAH)
       m_CondExec 00,EQ,NM005DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM010CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAJ
       ;;
(NM005DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/NM005DAG.INM010BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DAJ.INM010CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DAK
       ;;
(NM005DAK)
       m_CondExec 04,GE,NM005DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAM
       ;;
(NM005DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NM005DAJ.INM010CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DAM.INM010DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DAN
       ;;
(NM005DAN)
       m_CondExec 00,EQ,NM005DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM010                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAQ
       ;;
(NM005DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/NM005DAG.INM010BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/NM005DAM.INM010DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM010 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DAR
       ;;
(NM005DAR)
       m_CondExec 04,GE,NM005DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM030 : ETAT RECAP DES REMISES D'ESPECES, CHEQUES ET C.B.                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAT
       ;;
(NM005DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM030                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DAT.BNM030AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_23_3 23 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_44_3 ASCENDING,
   FLD_CH_23_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DAU
       ;;
(NM005DAU)
       m_CondExec 00,EQ,NM005DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM030 .POUR EDITION ETAT INM030                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DAX
       ;;
(NM005DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSFM10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A7} FNM001 ${DATA}/PTEM/NM005DAT.BNM030AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM030 ${DATA}/PTEM/NM005DAX.INM030AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM030 
       JUMP_LABEL=NM005DAY
       ;;
(NM005DAY)
       m_CondExec 04,GE,NM005DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM030A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBA
       ;;
(NM005DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM030                             
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/NM005DAX.INM030AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DBA.INM030BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_7_8 07 CH 8
 /FIELDS FLD_BI_20_3 20 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DBB
       ;;
(NM005DBB)
       m_CondExec 00,EQ,NM005DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM030CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBD
       ;;
(NM005DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/NM005DBA.INM030BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DBD.INM030CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DBE
       ;;
(NM005DBE)
       m_CondExec 04,GE,NM005DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBG
       ;;
(NM005DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/NM005DBD.INM030CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DBG.INM030DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DBH
       ;;
(NM005DBH)
       m_CondExec 00,EQ,NM005DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM030                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBJ
       ;;
(NM005DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/NM005DBA.INM030BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/NM005DBG.INM030DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM030 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DBK
       ;;
(NM005DBK)
       m_CondExec 04,GE,NM005DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM050 : ETAT DETAIL DE CAISSE PAR MODE DE PAIEMENT                        
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBM
       ;;
(NM005DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM050                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DBM.BNM050AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_24_3 24 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_24_3 ASCENDING,
   FLD_CH_27_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DBN
       ;;
(NM005DBN)
       m_CondExec 00,EQ,NM005DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM050 .POUR EDITION ETAT INM050                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBQ
       ;;
(NM005DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A13} FNM001 ${DATA}/PTEM/NM005DBM.BNM050AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM050 ${DATA}/PTEM/NM005DBQ.INM050AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM050 
       JUMP_LABEL=NM005DBR
       ;;
(NM005DBR)
       m_CondExec 04,GE,NM005DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM050A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBT
       ;;
(NM005DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM050                             
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/NM005DBQ.INM050AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DBT.INM050BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_12_8 12 CH 8
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DBU
       ;;
(NM005DBU)
       m_CondExec 00,EQ,NM005DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM050CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=NM005DBX
       ;;
(NM005DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/NM005DBT.INM050BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DBX.INM050CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DBY
       ;;
(NM005DBY)
       m_CondExec 04,GE,NM005DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCA
       ;;
(NM005DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/NM005DBX.INM050CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DCA.INM050DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_21_5 21 CH 5
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_29_2 29 CH 2
 /FIELDS FLD_BI_18_3 18 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_5 ASCENDING,
   FLD_BI_26_3 ASCENDING,
   FLD_BI_29_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DCB
       ;;
(NM005DCB)
       m_CondExec 00,EQ,NM005DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM050                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCD
       ;;
(NM005DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/NM005DBT.INM050BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FCUMULS ${DATA}/PTEM/NM005DCA.INM050DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM050 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DCE
       ;;
(NM005DCE)
       m_CondExec 04,GE,NM005DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM080 : ETAT DES OPERATIONS DE CAISSE                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCG
       ;;
(NM005DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM080                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DCG.BNM080AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_12 "LIB")
 /DERIVEDFIELD CST_1_8 "REO"
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_9_8 9 CH 8
 /FIELDS FLD_BI_1_8 1 CH 8
 /CONDITION CND_1 FLD_CH_44_3 EQ CST_1_8 OR FLD_CH_44_3 EQ CST_3_12 OR 
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING,
   FLD_BI_9_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DCH
       ;;
(NM005DCH)
       m_CondExec 00,EQ,NM005DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM080 .POUR EDITION ETAT INM080                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCJ
       ;;
(NM005DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A19} FNM001 ${DATA}/PTEM/NM005DCG.BNM080AD
# *** FICHIER EN SORTIE  (LRECL 512) POUR GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM080 ${DATA}/PXX0/F91.INM080AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM080 
       JUMP_LABEL=NM005DCK
       ;;
(NM005DCK)
       m_CondExec 04,GE,NM005DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM080A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCM
       ;;
(NM005DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM080                             
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PXX0/F91.INM080AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DCM.INM080FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DCN
       ;;
(NM005DCN)
       m_CondExec 00,EQ,NM005DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM080CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCQ
       ;;
(NM005DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/NM005DCM.INM080FD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DCQ.INM080CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DCR
       ;;
(NM005DCR)
       m_CondExec 04,GE,NM005DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCT
       ;;
(NM005DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/NM005DCQ.INM080CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DCT.INM080DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DCU
       ;;
(NM005DCU)
       m_CondExec 00,EQ,NM005DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM080                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=NM005DCX
       ;;
(NM005DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE(512)                
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/NM005DCM.INM080FD
# *********************************** FICHIER FCUMULS TRIE(512)                
       m_FileAssign -d SHR -g ${G_A24} FCUMULS ${DATA}/PTEM/NM005DCT.INM080DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -g +1 FEDITION ${DATA}/PXX0/F91.INM080ED
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DCY
       ;;
(NM005DCY)
       m_CondExec 04,GE,NM005DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP NM005DDA PGM=IEBGENER   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDA
       ;;
(NM005DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A25} SYSUT1 ${DATA}/PXX0/F91.INM080ED
       m_OutputAssign -c 9 -w INM080 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=NM005DDB
       ;;
(NM005DDB)
       m_CondExec 00,EQ,NM005DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         FUSION DU FICHIER CAISSES POUR EFFECTUER MICRO-FICHES                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDD
       ;;
(NM005DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION CAISSES DU JOUR                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.INM080BD
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PXX0/F91.INM080ED
# ******  EDITION DESTINE AUX MICRO-FICHES SUR 7 JOURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -g +1 SORTOUT ${DATA}/PXX0/F91.INM080BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DDE
       ;;
(NM005DDE)
       m_CondExec 00,EQ,NM005DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM120 : LISTE DES MODES DE PAIEMENT MODIFI�S                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDG
       ;;
(NM005DDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM120                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DDG.BNM120AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_43_1 43 CH 1
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_47_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DDH
       ;;
(NM005DDH)
       m_CondExec 00,EQ,NM005DDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM120 .POUR EDITION ETAT INM120                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDJ
       ;;
(NM005DDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A27} FNM001 ${DATA}/PTEM/NM005DDG.BNM120AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM120 ${DATA}/PTEM/NM005DDJ.INM120AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM120 
       JUMP_LABEL=NM005DDK
       ;;
(NM005DDK)
       m_CondExec 04,GE,NM005DDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM120A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDM
       ;;
(NM005DDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM120                             
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/NM005DDJ.INM120AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DDM.INM120BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_13_3 13 CH 3
 /FIELDS FLD_BI_23_8 23 CH 8
 /FIELDS FLD_BI_16_7 16 CH 7
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_10_3 10 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_7 ASCENDING,
   FLD_BI_23_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DDN
       ;;
(NM005DDN)
       m_CondExec 00,EQ,NM005DDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM120CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDQ
       ;;
(NM005DDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A29} FEXTRAC ${DATA}/PTEM/NM005DDM.INM120BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DDQ.INM120CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DDR
       ;;
(NM005DDR)
       m_CondExec 04,GE,NM005DDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDT
       ;;
(NM005DDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/NM005DDQ.INM120CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DDT.INM120DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DDU
       ;;
(NM005DDU)
       m_CondExec 00,EQ,NM005DDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM120                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DDX
       ;;
(NM005DDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A31} FEXTRAC ${DATA}/PTEM/NM005DDM.INM120BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A32} FCUMULS ${DATA}/PTEM/NM005DDT.INM120DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM120 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DDY
       ;;
(NM005DDY)
       m_CondExec 04,GE,NM005DDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM140 : LISTE DES AVOIRS ACOMPTES ET _A R�CEPTION FACTURE                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEA
       ;;
(NM005DEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM140                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DEA.BNM140AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_44_3 44 CH 3
 /FIELDS FLD_BI_1_8 1 CH 8
 /FIELDS FLD_BI_9_8 9 CH 8
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_9_8 ASCENDING,
   FLD_BI_44_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DEB
       ;;
(NM005DEB)
       m_CondExec 00,EQ,NM005DEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM140 .POUR EDITION ETAT INM140                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DED
       ;;
(NM005DED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A33} FNM001 ${DATA}/PTEM/NM005DEA.BNM140AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM140 ${DATA}/PTEM/NM005DED.INM140AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM140 
       JUMP_LABEL=NM005DEE
       ;;
(NM005DEE)
       m_CondExec 04,GE,NM005DED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM140A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEG
       ;;
(NM005DEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM140                             
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/NM005DED.INM140AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DEG.INM140BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_24_3 24 CH 3
 /FIELDS FLD_BI_21_3 21 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_18_3 18 CH 3
 /FIELDS FLD_BI_15_3 15 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DEH
       ;;
(NM005DEH)
       m_CondExec 00,EQ,NM005DEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM140CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEJ
       ;;
(NM005DEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A35} FEXTRAC ${DATA}/PTEM/NM005DEG.INM140BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DEJ.INM140CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DEK
       ;;
(NM005DEK)
       m_CondExec 04,GE,NM005DEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEM
       ;;
(NM005DEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/NM005DEJ.INM140CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DEM.INM140DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DEN
       ;;
(NM005DEN)
       m_CondExec 00,EQ,NM005DEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM140                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEQ
       ;;
(NM005DEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A37} FEXTRAC ${DATA}/PTEM/NM005DEG.INM140BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A38} FCUMULS ${DATA}/PTEM/NM005DEM.INM140DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM140 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DER
       ;;
(NM005DER)
       m_CondExec 04,GE,NM005DEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM170 : LISTE DES APPORTS ET RETRAITS D'ESP�CE                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=NM005DET
       ;;
(NM005DET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM170                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DET.BNM170AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_43_1 43 CH 1
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_1_8 1 CH 8
 /FIELDS FLD_BI_26_3 26 CH 3
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_43_1 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DEU
       ;;
(NM005DEU)
       m_CondExec 00,EQ,NM005DET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM170 .POUR EDITION ETAT INM170                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=NM005DEX
       ;;
(NM005DEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A39} FNM001 ${DATA}/PTEM/NM005DET.BNM170AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM170 ${DATA}/PTEM/NM005DEX.INM170AD
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM170 
       JUMP_LABEL=NM005DEY
       ;;
(NM005DEY)
       m_CondExec 04,GE,NM005DEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM170A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFA
       ;;
(NM005DFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM170                             
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/NM005DEX.INM170AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DFA.INM170BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_15_1 15 CH 1
 /FIELDS FLD_BI_22_3 22 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_1 ASCENDING,
   FLD_BI_16_3 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DFB
       ;;
(NM005DFB)
       m_CondExec 00,EQ,NM005DFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM170CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFD
       ;;
(NM005DFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A41} FEXTRAC ${DATA}/PTEM/NM005DFA.INM170BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DFD.INM170CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DFE
       ;;
(NM005DFE)
       m_CondExec 04,GE,NM005DFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFG
       ;;
(NM005DFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PTEM/NM005DFD.INM170CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DFG.INM170DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DFH
       ;;
(NM005DFH)
       m_CondExec 00,EQ,NM005DFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM170                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFJ
       ;;
(NM005DFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A43} FEXTRAC ${DATA}/PTEM/NM005DFA.INM170BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FCUMULS ${DATA}/PTEM/NM005DFG.INM170DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM170 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DFK
       ;;
(NM005DFK)
       m_CondExec 04,GE,NM005DFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM055 : ETAT CONTROLE VENTILATION NETTING DES MODES DE PAIEMENT           
#            (AVOIRS, BON D'ACHAT)                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#   TRI DU FICHIER                                                             
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFM
       ;;
(NM005DFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001AD
# *** SORTIE POUR PGM BNM055                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005DFM.BNM055AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_24_3 24 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_24_3 ASCENDING,
   FLD_CH_27_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DFN
       ;;
(NM005DFN)
       m_CondExec 00,EQ,NM005DFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM055 POUR EDITION ETAT INM055                                        
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFQ
       ;;
(NM005DFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A45} FNM001 ${DATA}/PTEM/NM005DFM.BNM055AD
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM055 ${DATA}/PTEM/NM005DFQ.INM055AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM055 
       JUMP_LABEL=NM005DFR
       ;;
(NM005DFR)
       m_CondExec 04,GE,NM005DFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM055A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFT PGM=SORT       ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFT
       ;;
(NM005DFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM055                             
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/NM005DFQ.INM055AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DFT.INM055BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DFU
       ;;
(NM005DFU)
       m_CondExec 00,EQ,NM005DFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM055C                                       
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=NM005DFX
       ;;
(NM005DFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/NM005DFT.INM055BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DFX.INM055CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DFY
       ;;
(NM005DFY)
       m_CondExec 04,GE,NM005DFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGA PGM=SORT       ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGA
       ;;
(NM005DGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/NM005DFX.INM055CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DGA.INM055DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DGB
       ;;
(NM005DGB)
       m_CondExec 00,EQ,NM005DGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM055                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGD
       ;;
(NM005DGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/NM005DFT.INM055BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/NM005DGA.INM055DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM055 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DGE
       ;;
(NM005DGE)
       m_CondExec 04,GE,NM005DGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM070 : VENTES POUR LE SAV                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGG
       ;;
(NM005DGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPA70   : NAME=RSPA70D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPA70 /dev/null
# *** FICHIER LOG DE CAISSE                                                    
       m_FileAssign -d SHR -g +0 FNM001 ${DATA}/PNCGD/F91.BNM001AD
# *** FICHIER EN SORTIE  (LRECL 512) POUR LE GENERATEUR D'ETAT                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM070 ${DATA}/PTEM/NM005DGG.INM070AD
# *** FICHIER CUMUL POUR ETAT HEBDO                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FNM070 ${DATA}/PTEM/NM005DGG.BNM070AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM070 
       JUMP_LABEL=NM005DGH
       ;;
(NM005DGH)
       m_CondExec 04,GE,NM005DGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DU FICHIER ISSU DU BNM070 POUR TRAITEMENT HEBDO                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGJ PGM=SORT       ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGJ
       ;;
(NM005DGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION CAISSES DU JOUR                                              
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PTEM/NM005DGG.BNM070AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FNM070AD
# ******  EDITION DESTINE AUX MICRO-FICHES SUR 7 JOURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PXX0/F91.FNM070AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DGK
       ;;
(NM005DGK)
       m_CondExec 00,EQ,NM005DGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM070AD)                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGM PGM=SORT       ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGM
       ;;
(NM005DGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM070                             
       m_FileAssign -d SHR -g ${G_A52} SORTIN ${DATA}/PTEM/NM005DGG.INM070AD
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DGM.INM070BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_2 18 CH 2
 /FIELDS FLD_BI_34_2 34 CH 2
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_26_8 26 CH 8
 /FIELDS FLD_BI_23_3 23 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_2 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_8 ASCENDING,
   FLD_BI_34_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DGN
       ;;
(NM005DGN)
       m_CondExec 00,EQ,NM005DGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM070CD                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGQ
       ;;
(NM005DGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A53} FEXTRAC ${DATA}/PTEM/NM005DGM.INM070BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005DGQ.INM070CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005DGR
       ;;
(NM005DGR)
       m_CondExec 04,GE,NM005DGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGT PGM=SORT       ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGT
       ;;
(NM005DGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A54} SORTIN ${DATA}/PTEM/NM005DGQ.INM070CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005DGT.INM070DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005DGU
       ;;
(NM005DGU)
       m_CondExec 00,EQ,NM005DGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM070                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005DGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=NM005DGX
       ;;
(NM005DGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A55} FEXTRAC ${DATA}/PTEM/NM005DGM.INM070BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A56} FCUMULS ${DATA}/PTEM/NM005DGT.INM070DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM070 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005DGY
       ;;
(NM005DGY)
       m_CondExec 04,GE,NM005DGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM005DZA
       ;;
(NM005DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM005DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
