#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC01L.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGQC01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/20 AT 16.43.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC01L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC01LA
       ;;
(GQC01LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC01LAA
       ;;
(GQC01LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.AS400.NASLKTAL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.NASKT0L
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC01LAA.SAV001AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 231 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC01LAB
       ;;
(GQC01LAB)
       m_CondExec 00,EQ,GQC01LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FQC013 ISSU DE GQC10D                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC01LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC01LAD
       ;;
(GQC01LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GQC01LAA.SAV001AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BQC013AL
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC01LAD.SAV001BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC01LAE
       ;;
(GQC01LAE)
       m_CondExec 00,EQ,GQC01LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC147                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC01LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC01LAG
       ;;
(GQC01LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g ${G_A2} FQCSAVE ${DATA}/PTEM/GQC01LAD.SAV001BL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FQCSAVS ${DATA}/PTEM/GQC01LAG.SAV001CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC147 
       JUMP_LABEL=GQC01LAH
       ;;
(GQC01LAH)
       m_CondExec 04,GE,GQC01LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BQC147                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC01LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC01LAJ
       ;;
(GQC01LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GQC01LAG.SAV001CL
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC01LAJ.SAV001DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_232_1 232 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_232_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC01LAK
       ;;
(GQC01LAK)
       m_CondExec 00,EQ,GQC01LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC150                                                              
#          D'EXTRACTION DES CARTES T REMONT� DES SITES 36 SAV TLM              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC01LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQC01LAM
       ;;
(GQC01LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE -- VUE RVGA01QA -- S/TABLE QCGAR                   
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
#                                                                              
# ******* FILE REMONT� DES AS400 NASL                                          
       m_FileAssign -d SHR -g ${G_A4} FQCSAV ${DATA}/PTEM/GQC01LAJ.SAV001DL
#                                                                              
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* TABLE DES CARTES T SAV                                               
#    RSQC03L  : NAME=RSQC03L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC03L /dev/null
# ******* TABLE DES ADRESSES                                                   
#    RSQC05L  : NAME=RSQC05L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSQC05L /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC150 
       JUMP_LABEL=GQC01LAN
       ;;
(GQC01LAN)
       m_CondExec 04,GE,GQC01LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC01LZA
       ;;
(GQC01LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC01LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
