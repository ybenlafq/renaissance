#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC107P.ksh                       --- VERSION DU 08/10/2016 13:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC107 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/11/30 AT 09.57.39 BY BURTECM                      
#    STANDARDS: P  JOBSET: BC107P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI FIC EXTRACTION N�CLIENT N�VENTES BASE CLIENT: FICHIER MARKETING         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BC107PA
       ;;
(BC107PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BC107PAA
       ;;
(BC107PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# PRED     LINK  NAME=$BC106P,MODE=I                                           
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BBC04HCP
#         FILE  NAME=BBC106BP,MODE=I                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/BC107PAA.BBC107BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_10 "9"
 /DERIVEDFIELD CST_1_6 "0"
 /FIELDS FLD_CH_4_3 04 CH 03
 /FIELDS FLD_CH_15_1 15 CH 1
 /FIELDS FLD_CH_1_3 01 CH 03
 /FIELDS FLD_CH_7_7 07 CH 07
 /CONDITION CND_1 FLD_CH_15_1 GE CST_1_6 AND FLD_CH_15_1 LE CST_3_10 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BC107PAB
       ;;
(BC107PAB)
       m_CondExec 00,EQ,BC107PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBC107 : MAJ IDCLIENT DE RTVT01-02                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC107PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BC107PAD
       ;;
(BC107PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  READ  RTVT01-02                                                      
#    RSEE98   : NAME=RSEE98,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEE98 /dev/null
# ******  MAJ   RTVT01-02                                                      
#    RSVT01   : NAME=RSVT01,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT01 /dev/null
#    RSVT02   : NAME=RSVT02,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT02 /dev/null
#                                                                              
# ******  FIC EXTRACTION N�CLIENT N�VENTES BASE CLIENT: MARKETING              
       m_FileAssign -d SHR -g ${G_A1} FBC409 ${DATA}/PTEM/BC107PAA.BBC107BP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE:H (HISTO)                                                  
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BC107PAD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC107 
       JUMP_LABEL=BC107PAE
       ;;
(BC107PAE)
       m_CondExec 04,GE,BC107PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RAZ FICHIER  BBC106B DE BC106P                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=IDCAMS,LANG=UTIL                                          
# IN       FILE  NAME=BBC106BP,MODE=N                                          
# OUT      FILE  NAME=BBC106BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  REPRO IFILE(IN) OFILE(OUT)                                                  
#         DATAEND                                                              
# *********************************                                            
#  DEPENDANCE POUR PLAN                                                        
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BC107PZA
       ;;
(BC107PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC107PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
