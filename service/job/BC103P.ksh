#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC103P.ksh                       --- VERSION DU 09/10/2016 05:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC103 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/08/05 AT 16.15.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: BC103P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  TRI DU FICHIER PROVENANT DE CLIC0P, DE CLI05P OU DE BC106P                  
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BC103PA
       ;;
(BC103PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2009/08/05 AT 16.15.45 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: BC103P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXT BASE CLI.'                         
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BC103PAA
       ;;
(BC103PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# ******  ENTREE : FICHIERS D EXTRACTIONS                                      
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BBC015CP
# ******  SORTIE : FICHIER TRIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/BC103PAA.FBC103AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "O"
 /FIELDS FLD_CH_32_8 32 CH 8
 /FIELDS FLD_CH_41_1 41 CH 01
 /FIELDS FLD_CH_1_14 1 CH 14
 /CONDITION CND_1 FLD_CH_41_1 EQ CST_1_5 OR 
 /KEYS
   FLD_CH_32_8 ASCENDING,
   FLD_CH_1_14 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BC103PAB
       ;;
(BC103PAB)
       m_CondExec 00,EQ,BC103PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBC103S : EXTRACTION N�CLIENT N�VENTES BASE CLIENT: FICHIER SAV             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC103PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BC103PAD
       ;;
(BC103PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ENTREE : TABLES EN LECTURE                                           
#    RTBC02   : NAME=RSBC02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBC02 /dev/null
#    RTBC12   : NAME=RSBC12P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBC12 /dev/null
#    RTBC13   : NAME=RSBC13P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBC13 /dev/null
#    RTBC31   : NAME=RSBC31P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTBC31 /dev/null
#    RTGQ96   : NAME=RSGQ96,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ96 /dev/null
#                                                                              
# ******  ENTREE : FIC TRI                                                     
       m_FileAssign -d SHR -g ${G_A1} FBC109 ${DATA}/PTEM/BC103PAA.FBC103AP
#                                                                              
# ******  SORTIE : FIC CLIENTS VERS NASC                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 377 -g +1 FBC100 ${DATA}/PXX0/F07.FBC103CP
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC103S 
       JUMP_LABEL=BC103PAE
       ;;
(BC103PAE)
       m_CondExec 04,GE,BC103PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COMPRESSION  FICHIER                                                        
#  (LE FICHIER FBC103CP ET REMIS A ZERO PAR CLIST 400)                         
#  (LE FICHIER FBC102CP ET REMIS A ZERO PAR CLIST 400)                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BC103PAG PGM=SYSTCOMP   ** ID=AAK                                   
# ***********************************                                          
# *********************************                                            
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BC103PZA
       ;;
(BC103PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BC103PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
