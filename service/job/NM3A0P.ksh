#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM3A0P.ksh                       --- VERSION DU 08/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNM3A0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/03/09 AT 10.21.07 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM3A0P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#        * REPRO CUMUL POUR (J+1)  EN CAS DE PLANTAGE DE NM030P                
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NM3A0PA
       ;;
(NM3A0PA)
#
#NM3A0PAJ
#NM3A0PAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#NM3A0PAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2000/03/09 AT 10.21.07 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: NM3A0P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-NM030P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NM3A0PAA
       ;;
(NM3A0PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC EN DE MQ VENTES TOP�ES SANS BON DE COMMANDE                      
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGP/F07.BNM160AP
# ******* FIC DE REPRISE L POUR (J+1)                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 OUT1 ${DATA}/PNCGP/F07.BNMCUMCP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM3A0PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM3A0PAB
       ;;
(NM3A0PAB)
       m_CondExec 16,NE,NM3A0PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        * CREATION D'UN FICHIER VIDE POUR LIBERATION DE L'ICS                 
#  IDCAMS*                                                                     
# ********                                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM3A0PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM3A0PAD
       ;;
(NM3A0PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******* FIC POUR L'INTERFACE COMPTABLE STD (I.C.S)                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGP/F07.BNM003AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM3A0PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM3A0PAE
       ;;
(NM3A0PAE)
       m_CondExec 16,NE,NM3A0PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM3A0PAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
