#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BC900P.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBC900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/05/18 AT 08.15.24 BY BURTECR                      
#    STANDARDS: P  JOBSET: BC900P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BBC900                                                                
# ********************************************************************         
#  PURGE PAR PROGRAMME DES TABLES CIBLE DE LA REPLICATION POUR SIEBEL          
#  PURGE DE TOUTES LES TABLES FILIALES VIA DES SYNONYMES                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BC900PA
       ;;
(BC900PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=BC900PAA
       ;;
(BC900PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ENTREE : TABLES EN LECTURE                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ENTREE : FICHIER DATE                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ENTREE: FICHIER SOCIETE                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******  SORTIE : TABLES EN MAJ                                               
# ******  DIF                                                                  
#    RSBC99   : NAME=RSBC99P,MODE=(U,N) - DYNAM=YES                            
# -X-RSBC99P  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSBC99 /dev/null
#    TSGV02   : NAME=TSGV02,MODE=(U,N) - DYNAM=YES                             
# -X-TSGV02   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02 /dev/null
#    TSGV11   : NAME=TSGV11,MODE=(U,N) - DYNAM=YES                             
# -X-TSGV11   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11 /dev/null
# ******  DO                                                                   
#    TSGV02O  : NAME=TSGV02O,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV02O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02O /dev/null
#    TSGV11O  : NAME=TSGV11O,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV11O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11O /dev/null
# ******  DRA                                                                  
#    TSGV02Y  : NAME=TSGV02Y,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV02Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02Y /dev/null
#    TSGV11Y  : NAME=TSGV11Y,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV11Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11Y /dev/null
# ******  DNN                                                                  
#    TSGV02L  : NAME=TSGV02L,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV02L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02L /dev/null
#    TSGV11L  : NAME=TSGV11L,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV11L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11L /dev/null
# ******  DAL                                                                  
#    TSGV02M  : NAME=TSGV02M,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV02M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02M /dev/null
#    TSGV11M  : NAME=TSGV11M,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV11M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11M /dev/null
# ******  DPM                                                                  
#    TSGV02D  : NAME=TSGV02D,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV02D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV02D /dev/null
#    TSGV11D  : NAME=TSGV11D,MODE=(U,N) - DYNAM=YES                            
# -X-TSGV11D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR TSGV11D /dev/null
# ******  APPEL DU PGM                                                         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC900 
       JUMP_LABEL=BC900PAB
       ;;
(BC900PAB)
       m_CondExec 04,GE,BC900PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
