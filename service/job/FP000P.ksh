#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FP000P.ksh                       --- VERSION DU 08/10/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFP000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/20 AT 17.18.40 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FP000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  PGM : BFP000  CREATION DU FICHIER DES FACTURES A REGLER                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FP000PA
       ;;
(FP000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FP000PAA
       ;;
(FP000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFP02   : NAME=RSFP02N,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFP02 /dev/null
#    RSFP03   : NAME=RSFP03N,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFP03 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE EN MAJ                                                         
#    RSFP01   : NAME=RSFP01N,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFP01 /dev/null
# ******* FICHIER FLOT EN ENTREE                                               
       m_FileAssign -d SHR -g +0 FLOTI ${DATA}/PXX0/F07.NFP001AP
# ******* FICHIER FLOT EN SORTIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FLOTO ${DATA}/PXX0/F07.NFP001AP
# ******* FICHIER DES FACTURES DU MOIS - VERS PACIFICA                         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFP000 ${DATA}/PXX0/F07.NFP001BP
# ******* FICHIER POUR EDITION ETAT FACTURES ENVOYEES A ASSUREUR               
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFP050 ${DATA}/PTEM/FP000PAA.NFP050AP
# ******* FIC COMPTE RENDU EXECUTION AVEC LISTE FACTURES NON ENVOYES           
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FFPCR1 ${DATA}/PXX0/F07.NFP001CP
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER PARAM PACIF                                                  
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/PACIF
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP001 
       JUMP_LABEL=FP000PAB
       ;;
(FP000PAB)
       m_CondExec 04,GE,FP000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER A EDITER                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FP000PAD
       ;;
(FP000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FP000PAA.NFP050AP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FP000PAD.NFP050BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_24 1 CH 24
 /KEYS
   FLD_CH_1_24 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP000PAE
       ;;
(FP000PAE)
       m_CondExec 00,EQ,FP000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFP050  CREATION ETAT EOS                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP000PAG PGM=BFP050     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FP000PAG
       ;;
(FP000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FFP050 ${DATA}/PTEM/FP000PAD.NFP050BP
# ******* ETAT EOS - ETAT DE CTL DES FACTURES SELECT POUR RGLT PACIFIC         
       m_OutputAssign -c J -w JFP050 JFP050
       m_ProgramExec BFP050 
#                                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFP00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP000PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FP000PAJ
       ;;
(FP000PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FP000PAJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FP000PZA
       ;;
(FP000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
