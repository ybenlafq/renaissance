#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AP012P.ksh                       --- VERSION DU 08/10/2016 17:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAP012 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/04/23 AT 18.47.09 BY BURTECO                      
#    STANDARDS: P  JOBSET: AP012P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM BAP012 **  COBOL2/DB2                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AP012PA
       ;;
(AP012PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=AP012PAA
       ;;
(AP012PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# *************************************                                        
# ******* TABLES EN LECTURE                                                    
#    RSAP15   : NAME=RSAP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP15 /dev/null
#    RSAP16   : NAME=RSAP16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAP16 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR53   : NAME=RSPR53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR53 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  LECTURE SOUS TABLE VUE RVCPA2I                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ******* �TAT DE SUIVI DES VENTES DE PRESTATIONS                              
       m_OutputAssign -c 9 -w IAP012 IMPRIM1
# ******* �TAT DE SUIVI DES R�ALISATIONS SAV (C.A R�ALIS� )                    
       m_OutputAssign -c 9 -w IAP013 IMPRIM2
# ******* �TAT DE SUIVI DES R�ALISATIONS SAV (TEMPS PASS� EN SAV)              
       m_OutputAssign -c 9 -w IAP014 IMPRIM3
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAP012 
       JUMP_LABEL=AP012PAB
       ;;
(AP012PAB)
       m_CondExec 04,GE,AP012PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
