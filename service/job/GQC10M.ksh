#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC10M.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGQC10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 18.13.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC10M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BQC140 : EXTRACTION DES DONNEES VENTES TOPEES LIVREES                      
#   REPRISE: LA REPRISE SERA FAITE DEMAIN MATIN                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC10MA
       ;;
(GQC10MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC10MAA
       ;;
(GQC10MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAM ASSOCIES AUX FAMILLES                                    
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ADRESSES                                                       
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSPO01   : NAME=RSPO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPO01 /dev/null
#    RSGA41   : NAME=RSGA41M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/GQC10M1
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GQC10M2
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010 ${DATA}/PXX0/F89.BQC010AM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC011 ${DATA}/PXX0/F89.BQC011AM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012 ${DATA}/PXX0/F89.BQC012AM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FQC013 ${DATA}/PXX0/F89.BQC013AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC140 
       JUMP_LABEL=GQC10MAB
       ;;
(GQC10MAB)
       m_CondExec 04,GE,GQC10MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC141 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAD
       ;;
(GQC10MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* FICHIER ALIMENT� EN CAS DE REPRISE                                   
       m_FileAssign -d SHR -g +0 FCREA ${DATA}/PXX0/F89.FCREAAPM
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
#                                                                              
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC014 ${DATA}/PXX0/F89.BQC014AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC141 
       JUMP_LABEL=GQC10MAE
       ;;
(GQC10MAE)
       m_CondExec 04,GE,GQC10MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAG
       ;;
(GQC10MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.BQC014AM
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.BQC016AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_PD_88_7 88 PD 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_PD_88_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10MAH
       ;;
(GQC10MAH)
       m_CondExec 00,EQ,GQC10MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC171 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAP      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01M,MODE=I                                 
# RSGQ01   FILE  DYNAM=YES,NAME=RSGQ01M,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01M,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDAL                                         
# * FICHIER EN ENTREE                                                          
# FQC014   FILE  NAME=BQC016AM,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC020   FILE  NAME=BQC020AM,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC171) PLAN(BQC171M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAU      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC020AM,MODE=I                                          
# SORTOUT  FILE  NAME=BQC021AM,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(5,19,A),FORMAT=CH                                              
#  RECORD TYPE=F,LENGTH=50                                                     
#          DATAEND                                                             
# ********************************************************************         
#   BQC142 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA00   FILE  DYNAM=YES,NAME=RSGA00M,MODE=I                                 
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01M,MODE=I                                 
# RSGA03   FILE  DYNAM=YES,NAME=RSGA03M,MODE=I                                 
# RSGS65   FILE  DYNAM=YES,NAME=RSGS65M,MODE=I                                 
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02M,MODE=I                                 
# RSGV10   FILE  DYNAM=YES,NAME=RSGV10M,MODE=I                                 
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******* CODE SOCIETE                                                         
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDAL                                         
# ******** FICHIER ALIMENT� EN CAS DE REPRISE                                  
# FCREA    FILE  NAME=FCREAAPM,MODE=I                                          
# ** FICHIER EN ENTREE                                                         
# FQC020   FILE  NAME=BQC021AM,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC015   FILE  NAME=BQC015AM,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC142) PLAN(BQC142M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABE      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC015AM,MODE=I                                          
# SORTOUT  FILE  NAME=BQC022AM,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,7,A),FORMAT=CH                                               
#  RECORD TYPE=F,LENGTH=100                                                    
#          DATAEND                                                             
# ********************************************************************         
#   BQC161 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# ABJ      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01M,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01M,MODE=I                                 
# RSQC06   FILE  DYNAM=YES,NAME=RSQC06M,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******** CODE SOCIETE                                                        
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDAL                                         
# * FICHIER EN ENTREE                                                          
# FQC015   FILE  NAME=BQC022AM,MODE=I                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC161) PLAN(BQC161M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   BQC145 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAJ PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAJ
       ;;
(GQC10MAJ)
       m_CondExec ${EXABO},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A2} FQC010E ${DATA}/PXX0/F89.BQC010AM
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010S ${DATA}/PXX0/F89.BQC145AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC145 
       JUMP_LABEL=GQC10MAK
       ;;
(GQC10MAK)
       m_CondExec 04,GE,GQC10MAJ ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAM PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAM
       ;;
(GQC10MAM)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F89.BQC012AM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC10MAM.BQC012CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_1 7 CH 1
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_1 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10MAN
       ;;
(GQC10MAN)
       m_CondExec 00,EQ,GQC10MAM ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC146 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAQ PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAQ
       ;;
(GQC10MAQ)
       m_CondExec ${EXABY},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A4} FQC012E ${DATA}/PTEM/GQC10MAM.BQC012CM
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012S ${DATA}/PXX0/F89.BQC146AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC146 
       JUMP_LABEL=GQC10MAR
       ;;
(GQC10MAR)
       m_CondExec 04,GE,GQC10MAQ ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * REMISE _A BLANCE DU FICHIER CONTENANT LES PARAM DE REPRISE                  
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10MAT PGM=IDCAMS     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MAT
       ;;
(GQC10MAT)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 ${DATA}/CORTEX4.P.MTXTFIX1/GQC10M01
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F89.FCREAAPM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10MAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GQC10MAU
       ;;
(GQC10MAU)
       m_CondExec 16,NE,GQC10MAT ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC10MZA
       ;;
(GQC10MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
