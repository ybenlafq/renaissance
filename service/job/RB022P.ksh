#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB022P.ksh                       --- VERSION DU 08/10/2016 14:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB022 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/10 AT 19.36.24 BY BURTECC                      
#    STANDARDS: P  JOBSET: RB022P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BRB022 : EXTRACTION DU FICHIER CODES POSTAUX                            
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB022PA
       ;;
(RB022PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       RUN=${RUN}
       JUMP_LABEL=RB022PAA
       ;;
(RB022PAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  SORTIE : FICHIER (LRECL=300)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FRB022 ${DATA}/PXX0/F07.BRB022AP
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTRB06   : NAME=RSRB06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB06 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGQ01   : NAME=RSGQ01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB022 
       JUMP_LABEL=RB022PAB
       ;;
(RB022PAB)
       m_CondExec 04,GE,RB022PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DES FICHIERS VERS XFB REDBOX (FLUX CODES POSTAUX)                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=RB022P                                        
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB022P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB022PAD PGM=FTP        ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB022PAD
       ;;
(RB022PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB022PAD.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB022PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB022PAG
       ;;
(RB022PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB022PAG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  PGM BRB130 : EXTRACTION DES PARAMETRES THD                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB022PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB022PAJ
       ;;
(RB022PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******  SORTIE : FICHIER (LRECL=300)                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-104 -t LSEQ -g +1 FRB130 ${DATA}/PXX0/F07.BRB130AP
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB00 /dev/null
#    RTRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB130 
       JUMP_LABEL=RB022PAK
       ;;
(RB022PAK)
       m_CondExec 04,GE,RB022PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER PARAMETRES THD VERS GATEWAY DARTYBOX                          
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BRB130AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRB022P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB022PAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB022PAM
       ;;
(RB022PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RB022PAM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RB022PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RB022PAQ
       ;;
(RB022PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB022PAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
