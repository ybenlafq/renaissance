#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC34P.ksh                       --- VERSION DU 08/10/2016 12:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGQC34 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/20 AT 16.38.22 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC34P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BQC340                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC34PA
       ;;
(GQC34PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC34PAA
       ;;
(GQC34PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g +0 FQC341 ${DATA}/PXX0/FTP.F99.FQC034AP
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 FQC342 ${DATA}/PXX0/F07.FQC034BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC340 
       JUMP_LABEL=GQC34PAB
       ;;
(GQC34PAB)
       m_CondExec 04,GE,GQC34PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ECLATEMENT DES FICHIERS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC34PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC34PAD
       ;;
(GQC34PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.FQC034BP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F91.FQC034CD
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F61.FQC034CL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F89.FQC034CM
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF4 ${DATA}/PXX0/F16.FQC034CO
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF5 ${DATA}/PXX0/F07.FQC034CP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOF6 ${DATA}/PXX0/F45.FQC034CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_18 "945"
 /DERIVEDFIELD CST_1_3 "991"
 /DERIVEDFIELD CST_1_15 "907"
 /DERIVEDFIELD CST_1_9 "989"
 /DERIVEDFIELD CST_1_12 "916"
 /DERIVEDFIELD CST_1_6 "961"
 /FIELDS FLD_CH_21_3 21 CH 3
 /CONDITION CND_5 FLD_CH_21_3 EQ CST_1_15 
 /CONDITION CND_6 FLD_CH_21_3 EQ CST_1_18 
 /CONDITION CND_1 FLD_CH_21_3 EQ CST_1_3 
 /CONDITION CND_4 FLD_CH_21_3 EQ CST_1_12 
 /CONDITION CND_2 FLD_CH_21_3 EQ CST_1_6 
 /CONDITION CND_3 FLD_CH_21_3 EQ CST_1_9 
 /COPY
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
 /MT_OUTFILE_SUF 4
 /INCLUDE CND_4
 /MT_OUTFILE_SUF 5
 /INCLUDE CND_5
 /MT_OUTFILE_SUF 6
 /INCLUDE CND_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GQC34PAE
       ;;
(GQC34PAE)
       m_CondExec 00,EQ,GQC34PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BQC350                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC34PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC34PAG
       ;;
(GQC34PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES ADRESSES                                                   
#    RSQC11   : NAME=RSQC11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSQC11 /dev/null
# ******* FICHIERS EN ENTR�E                                                   
       m_FileAssign -d SHR -g ${G_A2} FQC351 ${DATA}/PXX0/F07.FQC034CP
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC350 
       JUMP_LABEL=GQC34PAH
       ;;
(GQC34PAH)
       m_CondExec 04,GE,GQC34PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
