#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB600P.ksh                       --- VERSION DU 08/10/2016 22:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB600 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/06/05 AT 10.37.16 BY BURTECA                      
#    STANDARDS: P  JOBSET: RB600P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRB600 :TRAITEMENT DES ECHANGES INTER-FILIALES                              
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RB600PA
       ;;
(RB600PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=RB600PAA
       ;;
(RB600PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE M.A.J                                                           
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB600 
       JUMP_LABEL=RB600PAB
       ;;
(RB600PAB)
       m_CondExec 04,GE,RB600PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVE600 :TRAITEMENT DES ECHANGES INTER-FILIALES                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB600PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB600PAD
       ;;
(RB600PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE EN LECTURE                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGB55   : NAME=RSGB55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB55 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ****** TABLE M.A.J                                                           
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSVE11   : NAME=RSVE11,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSVE11Y  : NAME=RSVE11Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11Y /dev/null
#    RSVE11M  : NAME=RSVE11M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11M /dev/null
#    RSVE11D  : NAME=RSVE11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11D /dev/null
#    RSVE11L  : NAME=RSVE11L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11L /dev/null
#    RSVE11O  : NAME=RSVE11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11O /dev/null
#    RSVE11X  : NAME=RSVE11X,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11X /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE600 
       JUMP_LABEL=RB600PAE
       ;;
(RB600PAE)
       m_CondExec 04,GE,RB600PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
