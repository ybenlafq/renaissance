#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQC10O.ksh                       --- VERSION DU 08/10/2016 22:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGQC10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/18 AT 18.13.50 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GQC10O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGV117 : MISE A JOUR DE LA DCOMPTA DANS LA RTGV11 POUR R�CUP�RATIO         
#   REPRISE: DES CARTES "T".                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQC10OA
       ;;
(GQC10OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQC10OAA
       ;;
(GQC10OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE EN SORTIE                                                      
#    RTGV11   : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV117 
       JUMP_LABEL=GQC10OAB
       ;;
(GQC10OAB)
       m_CondExec 04,GE,GQC10OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC140 : EXTRACTION DES DONNEES VENTES TOPEES LIVREES                      
#   REPRISE: LA REPRISE SERA FAITE DEMAIN MATIN                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAD
       ;;
(GQC10OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* TABLE PARAM ASSOCIES AUX FAMILLES                                    
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30O /dev/null
# ******* TABLE ADRESSES                                                       
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65O  : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65O /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSPO01   : NAME=RSPO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPO01 /dev/null
#    RSGA41   : NAME=RSGA41O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/GQC10O1
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GQC10O2
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010 ${DATA}/PXX0/F16.BQC010AO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC011 ${DATA}/PXX0/F16.BQC011AO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012 ${DATA}/PXX0/F16.BQC012AO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FQC013 ${DATA}/PXX0/F16.BQC013AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC140 
       JUMP_LABEL=GQC10OAE
       ;;
(GQC10OAE)
       m_CondExec 04,GE,GQC10OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC141 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAG
       ;;
(GQC10OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETES VENTES                                                 
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE VENTES                                                         
#    RSGV11   : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER ALIMENT� EN CAS DE REPRISE                                   
       m_FileAssign -d SHR -g +0 FCREA ${DATA}/PXX0/F16.FCREAAPO
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
#                                                                              
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FQC014 ${DATA}/PXX0/F16.BQC014AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC141 
       JUMP_LABEL=GQC10OAH
       ;;
(GQC10OAH)
       m_CondExec 04,GE,GQC10OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAJ
       ;;
(GQC10OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F16.BQC014AO
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.BQC016AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_PD_88_7 88 PD 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_PD_88_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10OAK
       ;;
(GQC10OAK)
       m_CondExec 00,EQ,GQC10OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC171 : EXTRACTION DES DONNEES VENTES LIVREES DU JOUR                     
#  REPRISE : ??????                                                            
# ********************************************************************         
# AAU      STEP  PGM=IKJEFT01                                                  
# *                                                                            
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******* TABLES EN LECTURE                                                    
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01O,MODE=I                                 
# RSGQ01   FILE  DYNAM=YES,NAME=RSGQ01O,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01O,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******* FDATE JJMMSSAA                                                       
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******* CODE SOCIETE                                                         
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# * FICHIER EN ENTREE                                                          
# FQC014   FILE  NAME=BQC016AO,MODE=I                                          
# ** FICHIER EN SORTIE                                                         
# FQC020   FILE  NAME=BQC020AO,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC171) PLAN(BTDARO)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAZ      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC020AO,MODE=I                                          
# SORTOUT  FILE  NAME=BQC021AO,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(5,19,A),FORMAT=CH                                              
#  RECORD TYPE=F,LENGTH=50                                                     
#          DATAEND                                                             
# ********************************************************************         
#   BQC142 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# ABE      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA00   FILE  DYNAM=YES,NAME=RSGA00O,MODE=I                                 
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01O,MODE=I                                 
# RSGA03   FILE  DYNAM=YES,NAME=RSGA03O,MODE=I                                 
# RSGS65   FILE  DYNAM=YES,NAME=RSGS65,MODE=I                                  
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02O,MODE=I                                 
# RSGV10   FILE  DYNAM=YES,NAME=RSGV10O,MODE=I                                 
# ******* FDATE JJMMSSAA                                                       
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******* CODE SOCIETE                                                         
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ******* FICHIER ALIMENT� EN CAS DE REPRISE                                   
# FCREA    FILE  NAME=FCREAAPO,MODE=I                                          
# * FICHIER EN ENTREE                                                          
# FQC020   FILE  NAME=BQC021AO,MODE=I                                          
# * FICHIER EN SORTIE                                                          
# FQC015   FILE  NAME=BQC015AO,MODE=O                                          
#                                                                              
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC142) PLAN(BTDARO)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABJ      STEP  PGM=SORT                                                      
# SYSOUT   REPORT SYSOUT=*                                                     
# SORTIN   FILE  NAME=BQC015AO,MODE=I                                          
# SORTOUT  FILE  NAME=BQC022AO,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,7,A),FORMAT=CH                                               
#  RECORD TYPE=F,LENGTH=100                                                    
#          DATAEND                                                             
# ********************************************************************         
#   BQC161 :                                                                   
#  REPRISE : ??????                                                            
# ********************************************************************         
# ABO      STEP  PGM=IKJEFT01                                                  
# **                                                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******** TABLES EN LECTURE                                                   
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01O,MODE=I                                 
# RSQC01   FILE  DYNAM=YES,NAME=RSQC01O,MODE=I                                 
# RSQC06   FILE  DYNAM=YES,NAME=RSQC06O,MODE=I                                 
# RSTL01   FILE  DYNAM=YES,NAME=RSTL01,MODE=I                                  
# RSTL03   FILE  DYNAM=YES,NAME=RSTL03,MODE=I                                  
# ******** FDATE JJMMSSAA                                                      
# FDATE    DATA  PARMS=FDATE,CLASS=VAR,MBR=FDATE                               
# ******* CODE SOCIETE                                                         
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ** FICHIER EN ENTREE                                                         
# FQC015   FILE  NAME=BQC022AO,MODE=I                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BQC161) PLAN(BTDARO)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   BQC145 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAM PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAM
       ;;
(GQC10OAM)
       m_CondExec ${EXABT},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A2} FQC010E ${DATA}/PXX0/F16.BQC010AO
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC010S ${DATA}/PXX0/F16.BQC145AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC145 
       JUMP_LABEL=GQC10OAN
       ;;
(GQC10OAN)
       m_CondExec 04,GE,GQC10OAM ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC                                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAQ PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAQ
       ;;
(GQC10OAQ)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F16.BQC012AO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQC10OAQ.BQC012CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_1 7 CH 1
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_1 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQC10OAR
       ;;
(GQC10OAR)
       m_CondExec 00,EQ,GQC10OAQ ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BQC146 : RECUPERATION DES MAILS DANS SIEBEL FIC ISSU DU STEP AAA           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAT PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAT
       ;;
(GQC10OAT)
       m_CondExec ${EXACD},NE,YES 
# *                                                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# * FICHIER EN ENTREE                                                          
       m_FileAssign -d SHR -g ${G_A4} FQC012E ${DATA}/PTEM/GQC10OAQ.BQC012CO
# * FICHIER EN SORTIE                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FQC012S ${DATA}/PXX0/F16.BQC146AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQC146 
       JUMP_LABEL=GQC10OAU
       ;;
(GQC10OAU)
       m_CondExec 04,GE,GQC10OAT ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * REMISE _A BLANCE DU FICHIER CONTENANT LES PARAM DE REPRISE                  
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQC10OAX PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OAX
       ;;
(GQC10OAX)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 ${DATA}/CORTEX4.P.MTXTFIX1/GQC10O01
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F16.FCREAAPO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10OAX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GQC10OAY
       ;;
(GQC10OAY)
       m_CondExec 16,NE,GQC10OAX ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQC10OZA
       ;;
(GQC10OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQC10OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
