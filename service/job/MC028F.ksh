#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MC028F.ksh                       --- VERSION DU 20/10/2016 12:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFMC028 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/10/02 AT 15.32.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MC028F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MC028FA
       ;;
(MC028FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2015/10/02 AT 15.32.13 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: MC028F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'MERGE TRIM MGD'                        
# *                           APPL...: REPALL                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MC028FAA
       ;;
(MC028FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.FMC028AF
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MC028FAA.FMC028BF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY
 
/FIELDS
         FLD_01 1:  -  1:,
         FLD_02 2:  -  2:
 /DERIVEDFIELD build_FLD_01   FLD_01  80
 /DERIVEDFIELD build_FLD_02   FLD_02  20
 /PADBYTE x"20"
 
/MT_INFILE_SORT SORTIN  fieldseparator ";"
 /REFORMAT
       build_FLD_01,build_FLD_02
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MC028FAB
       ;;
(MC028FAB)
       m_CondExec 00,EQ,MC028FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC028FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MC028FAD
       ;;
(MC028FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/MC028FAA.FMC028BF
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MC028FAD.FMC028CF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MC028FAE
       ;;
(MC028FAE)
       m_CondExec 00,EQ,MC028FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC028FAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MC028FAG
       ;;
(MC028FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.FMC027CP
       m_FileAssign -d SHR -g -1 -C ${DATA}/PXX0/F99.FMC027CP
       m_FileAssign -d SHR -g -2 -C ${DATA}/PXX0/F99.FMC027CP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FMC028DF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_327_100 327 CH 100
 /KEYS
   FLD_CH_327_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MC028FAH
       ;;
(MC028FAH)
       m_CondExec 00,EQ,MC028FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMC028   REFORMATAGE DU FICHIER                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC028FAJ PGM=BMC028     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MC028FAJ
       ;;
(MC028FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FADRESSE ${DATA}/PTEM/MC028FAD.FMC028CF
       m_FileAssign -d SHR -g ${G_A3} FMC027I ${DATA}/PXX0/F99.FMC028DF
# ******  FICHIER EN 500                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FMC028O ${DATA}/PTEM/MC028FAJ.FMC028EF
       m_ProgramExec BMC028 
#                                                                              
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MC028FAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MC028FAM
       ;;
(MC028FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.FMC027EF
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/MC028FAJ.FMC028EF
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FMC028FF
# *****************************************                                    
#  REPRO DU FICHIER                                                            
# *****************************************                                    
#                                                                              
# ***********************************                                          
# *   STEP MC028FAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MC028FAQ
       ;;
(MC028FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 INPUT ${DATA}/PXX0/F99.FMC027EF
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 OUTPUT ${DATA}/PXX0/F99.FMC027EF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC028FAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MC028FAR
       ;;
(MC028FAR)
       m_CondExec 16,NE,MC028FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MC028FZA
       ;;
(MC028FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MC028FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
