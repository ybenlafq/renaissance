#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM005L.ksh                       --- VERSION DU 08/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLNM005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/08 AT 11.35.50 BY BURTECA                      
#    STANDARDS: P  JOBSET: NM005L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BNM010 : EDITION DE VERIF DE CAISSE                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER EN PROVENANCE DE NM002                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM005LA
       ;;
(NM005LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NM005LAA
       ;;
(NM005LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
# *** FICHIER ISSU DE LA CHAINE NM002    (N.E.M)                               
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM010                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LAA.BNM010AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_9_8 9 CH 8
 /FIELDS FLD_BI_1_8 1 CH 8
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING,
   FLD_BI_9_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LAB
       ;;
(NM005LAB)
       m_CondExec 00,EQ,NM005LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM010 .ETAT DE VERIFICATION DE CAISSE                                 
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAD
       ;;
(NM005LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PTEM/NM005LAA.BNM010AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM010 ${DATA}/PTEM/NM005LAD.BNM010BL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM010 
       JUMP_LABEL=NM005LAE
       ;;
(NM005LAE)
       m_CondExec 04,GE,NM005LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER INM010AL                                                     
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAG
       ;;
(NM005LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER EN ENTREE                          
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NM005LAD.BNM010BL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LAG.INM010BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 07 CH 8
 /FIELDS FLD_BI_21_3 21 CH 3
 /FIELDS FLD_BI_18_3 18 CH 3
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_24_3 24 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LAH
       ;;
(NM005LAH)
       m_CondExec 00,EQ,NM005LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM010CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAJ
       ;;
(NM005LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/NM005LAG.INM010BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LAJ.INM010CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LAK
       ;;
(NM005LAK)
       m_CondExec 04,GE,NM005LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAM
       ;;
(NM005LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NM005LAJ.INM010CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LAM.INM010DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LAN
       ;;
(NM005LAN)
       m_CondExec 00,EQ,NM005LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM010                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAQ
       ;;
(NM005LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/NM005LAG.INM010BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/NM005LAM.INM010DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM010 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LAR
       ;;
(NM005LAR)
       m_CondExec 04,GE,NM005LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM030 : ETAT RECAP DES REMISES D'ESPECES, CHEQUES ET C.B.                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAT
       ;;
(NM005LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM030                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LAT.BNM030AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_44_3 ASCENDING,
   FLD_CH_23_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LAU
       ;;
(NM005LAU)
       m_CondExec 00,EQ,NM005LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM030 .POUR EDITION ETAT INM030                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LAX
       ;;
(NM005LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSFM10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A7} FNM001 ${DATA}/PTEM/NM005LAT.BNM030AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM030 ${DATA}/PTEM/NM005LAX.INM030AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM030 
       JUMP_LABEL=NM005LAY
       ;;
(NM005LAY)
       m_CondExec 04,GE,NM005LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM030A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBA
       ;;
(NM005LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM030                             
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/NM005LAX.INM030AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LBA.INM030BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 07 CH 8
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_20_3 20 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LBB
       ;;
(NM005LBB)
       m_CondExec 00,EQ,NM005LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM030CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBD
       ;;
(NM005LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/NM005LBA.INM030BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LBD.INM030CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LBE
       ;;
(NM005LBE)
       m_CondExec 04,GE,NM005LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBG
       ;;
(NM005LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/NM005LBD.INM030CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LBG.INM030DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LBH
       ;;
(NM005LBH)
       m_CondExec 00,EQ,NM005LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM030                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBJ
       ;;
(NM005LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/NM005LBA.INM030BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/NM005LBG.INM030DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM030 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LBK
       ;;
(NM005LBK)
       m_CondExec 04,GE,NM005LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM050 : ETAT DETAIL DE CAISSE PAR MODE DE PAIEMENT                        
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBM
       ;;
(NM005LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM050                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LBM.BNM050AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_3 24 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_20_3 20 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_24_3 ASCENDING,
   FLD_CH_27_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_47_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LBN
       ;;
(NM005LBN)
       m_CondExec 00,EQ,NM005LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM050 .POUR EDITION ETAT INM050                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBQ
       ;;
(NM005LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A13} FNM001 ${DATA}/PTEM/NM005LBM.BNM050AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM050 ${DATA}/PTEM/NM005LBQ.INM050AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM050 
       JUMP_LABEL=NM005LBR
       ;;
(NM005LBR)
       m_CondExec 04,GE,NM005LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM050A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBT
       ;;
(NM005LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM050                             
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/NM005LBQ.INM050AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LBT.INM050BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_12_8 12 CH 8
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LBU
       ;;
(NM005LBU)
       m_CondExec 00,EQ,NM005LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM050CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=NM005LBX
       ;;
(NM005LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/NM005LBT.INM050BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LBX.INM050CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LBY
       ;;
(NM005LBY)
       m_CondExec 04,GE,NM005LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCA
       ;;
(NM005LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/NM005LBX.INM050CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LCA.INM050DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LCB
       ;;
(NM005LCB)
       m_CondExec 00,EQ,NM005LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM050                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCD
       ;;
(NM005LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/NM005LBT.INM050BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FCUMULS ${DATA}/PTEM/NM005LCA.INM050DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM050 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LCE
       ;;
(NM005LCE)
       m_CondExec 04,GE,NM005LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM080 : ETAT DES OPERATIONS DE CAISSE                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCG
       ;;
(NM005LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM080                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LCG.BNM080AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_12 "LIB")
 /DERIVEDFIELD CST_1_8 "REO"
 /FIELDS FLD_BI_1_8 1 CH 8
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_CH_44_3 44 CH 3
 /FIELDS FLD_BI_9_8 9 CH 8
 /FIELDS FLD_BI_20_3 20 CH 3
 /CONDITION CND_1 FLD_CH_44_3 EQ CST_1_8 OR FLD_CH_44_3 EQ CST_3_12 OR 
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING,
   FLD_BI_9_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LCH
       ;;
(NM005LCH)
       m_CondExec 00,EQ,NM005LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM080 .POUR EDITION ETAT INM080                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCJ
       ;;
(NM005LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A19} FNM001 ${DATA}/PTEM/NM005LCG.BNM080AL
# *** FICHIER EN SORTIE  (LRECL 512) POUR GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM080 ${DATA}/PXX0/F61.INM080AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM080 
       JUMP_LABEL=NM005LCK
       ;;
(NM005LCK)
       m_CondExec 04,GE,NM005LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM080A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCM
       ;;
(NM005LCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM080                             
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PXX0/F61.INM080AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LCM.INM080FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LCN
       ;;
(NM005LCN)
       m_CondExec 00,EQ,NM005LCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM080CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCQ
       ;;
(NM005LCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/NM005LCM.INM080FL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LCQ.INM080CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LCR
       ;;
(NM005LCR)
       m_CondExec 04,GE,NM005LCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCT
       ;;
(NM005LCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/NM005LCQ.INM080CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LCT.INM080DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LCU
       ;;
(NM005LCU)
       m_CondExec 00,EQ,NM005LCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM080                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=NM005LCX
       ;;
(NM005LCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE(512)                
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/NM005LCM.INM080FL
# *********************************** FICHIER FCUMULS TRIE(512)                
       m_FileAssign -d SHR -g ${G_A24} FCUMULS ${DATA}/PTEM/NM005LCT.INM080DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -g +1 FEDITION ${DATA}/PXX0/F61.INM080EL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LCY
       ;;
(NM005LCY)
       m_CondExec 04,GE,NM005LCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP NM005LDA PGM=IEBGENER   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDA
       ;;
(NM005LDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A25} SYSUT1 ${DATA}/PXX0/F61.INM080EL
       m_OutputAssign -c 9 -w INM080 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=NM005LDB
       ;;
(NM005LDB)
       m_CondExec 00,EQ,NM005LDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#         FUSION DU FICHIER CAISSES POUR EFFECTUER MICRO-FICHES                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDD
       ;;
(NM005LDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION CAISSES DU JOUR                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.INM080BL
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PXX0/F61.INM080EL
# ******  EDITION DESTINE AUX MICRO-FICHES SUR 7 JOURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -g +1 SORTOUT ${DATA}/PXX0/F61.INM080BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LDE
       ;;
(NM005LDE)
       m_CondExec 00,EQ,NM005LDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#   BNM120 : LISTE DES MODES DE PAIEMENT MODIFI�S                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDG
       ;;
(NM005LDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM120                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LDG.BNM120AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_47_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LDH
       ;;
(NM005LDH)
       m_CondExec 00,EQ,NM005LDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM120 .POUR EDITION ETAT INM120                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDJ
       ;;
(NM005LDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A27} FNM001 ${DATA}/PTEM/NM005LDG.BNM120AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM120 ${DATA}/PTEM/NM005LDJ.INM120AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM120 
       JUMP_LABEL=NM005LDK
       ;;
(NM005LDK)
       m_CondExec 04,GE,NM005LDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM120A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDM
       ;;
(NM005LDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM120                             
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/NM005LDJ.INM120AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LDM.INM120BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_23_8 23 CH 8
 /FIELDS FLD_BI_16_7 16 CH 7
 /FIELDS FLD_BI_10_3 10 CH 3
 /FIELDS FLD_BI_13_3 13 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_7 ASCENDING,
   FLD_BI_23_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LDN
       ;;
(NM005LDN)
       m_CondExec 00,EQ,NM005LDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM120CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDQ
       ;;
(NM005LDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A29} FEXTRAC ${DATA}/PTEM/NM005LDM.INM120BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LDQ.INM120CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LDR
       ;;
(NM005LDR)
       m_CondExec 04,GE,NM005LDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDT
       ;;
(NM005LDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/NM005LDQ.INM120CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LDT.INM120DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LDU
       ;;
(NM005LDU)
       m_CondExec 00,EQ,NM005LDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM120                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LDX
       ;;
(NM005LDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A31} FEXTRAC ${DATA}/PTEM/NM005LDM.INM120BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A32} FCUMULS ${DATA}/PTEM/NM005LDT.INM120DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM120 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LDY
       ;;
(NM005LDY)
       m_CondExec 04,GE,NM005LDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM140 : LISTE DES AVOIRS ACOMPTES ET _A R�CEPTION FACTURE                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEA
       ;;
(NM005LEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM140                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LEA.BNM140AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_1_8 1 CH 8
 /FIELDS FLD_BI_9_8 9 CH 8
 /FIELDS FLD_BI_44_3 44 CH 3
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_9_8 ASCENDING,
   FLD_BI_44_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LEB
       ;;
(NM005LEB)
       m_CondExec 00,EQ,NM005LEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM140 .POUR EDITION ETAT INM140                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LED
       ;;
(NM005LED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A33} FNM001 ${DATA}/PTEM/NM005LEA.BNM140AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM140 ${DATA}/PTEM/NM005LED.INM140AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM140 
       JUMP_LABEL=NM005LEE
       ;;
(NM005LEE)
       m_CondExec 04,GE,NM005LED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM140A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEG
       ;;
(NM005LEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM140                             
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/NM005LED.INM140AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LEG.INM140BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_24_3 24 CH 3
 /FIELDS FLD_BI_18_3 18 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_21_3 21 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_3 ASCENDING,
   FLD_BI_24_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LEH
       ;;
(NM005LEH)
       m_CondExec 00,EQ,NM005LEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM140CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEJ
       ;;
(NM005LEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A35} FEXTRAC ${DATA}/PTEM/NM005LEG.INM140BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LEJ.INM140CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LEK
       ;;
(NM005LEK)
       m_CondExec 04,GE,NM005LEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEM
       ;;
(NM005LEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/NM005LEJ.INM140CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LEM.INM140DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LEN
       ;;
(NM005LEN)
       m_CondExec 00,EQ,NM005LEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM140                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEQ
       ;;
(NM005LEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A37} FEXTRAC ${DATA}/PTEM/NM005LEG.INM140BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A38} FCUMULS ${DATA}/PTEM/NM005LEM.INM140DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM140 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LER
       ;;
(NM005LER)
       m_CondExec 04,GE,NM005LEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM170 : LISTE DES APPORTS ET RETRAITS D'ESP�CE                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#   SORT DU FICHIER                                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=NM005LET
       ;;
(NM005LET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM170                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LET.BNM170AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_43_1 43 CH 1
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_26_3 26 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_1_8 1 CH 8
 /KEYS
   FLD_BI_1_8 ASCENDING,
   FLD_BI_43_1 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LEU
       ;;
(NM005LEU)
       m_CondExec 00,EQ,NM005LET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM170 .POUR EDITION ETAT INM170                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=NM005LEX
       ;;
(NM005LEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPM06   : NAME=RSPM06L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPM06 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A39} FNM001 ${DATA}/PTEM/NM005LET.BNM170AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM170 ${DATA}/PTEM/NM005LEX.INM170AL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM170 
       JUMP_LABEL=NM005LEY
       ;;
(NM005LEY)
       m_CondExec 04,GE,NM005LEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM170A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFA
       ;;
(NM005LFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM170                             
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/NM005LEX.INM170AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LFA.INM170BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_22_3 22 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_15_1 15 CH 1
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_19_3 19 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_1 ASCENDING,
   FLD_BI_16_3 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LFB
       ;;
(NM005LFB)
       m_CondExec 00,EQ,NM005LFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM170CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFD
       ;;
(NM005LFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A41} FEXTRAC ${DATA}/PTEM/NM005LFA.INM170BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LFD.INM170CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LFE
       ;;
(NM005LFE)
       m_CondExec 04,GE,NM005LFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFG
       ;;
(NM005LFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PTEM/NM005LFD.INM170CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LFG.INM170DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LFH
       ;;
(NM005LFH)
       m_CondExec 00,EQ,NM005LFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM170                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFJ
       ;;
(NM005LFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A43} FEXTRAC ${DATA}/PTEM/NM005LFA.INM170BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FCUMULS ${DATA}/PTEM/NM005LFG.INM170DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM170 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LFK
       ;;
(NM005LFK)
       m_CondExec 04,GE,NM005LFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BNM055 : ETAT CONTROLE VENTILATION NETTING DES MODES DE PAIEMENT           
#            (AVOIRS, BON D'ACHAT)                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#   TRI DU FICHIER                                                             
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFM
       ;;
(NM005LFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER ISSU DU TRAITEMENT NM002      (N.E.M)                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BNM001AL
# *** SORTIE POUR PGM BNM055                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/NM005LFM.BNM055AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_3 24 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_24_3 ASCENDING,
   FLD_CH_27_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LFN
       ;;
(NM005LFN)
       m_CondExec 00,EQ,NM005LFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM055 POUR EDITION ETAT INM055                                        
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFQ
       ;;
(NM005LFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
# *** FICHIER EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A45} FNM001 ${DATA}/PTEM/NM005LFM.BNM055AL
# *** FICHIER EN SORTIE  (LRECL 512)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM055 ${DATA}/PTEM/NM005LFQ.INM055AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM055 
       JUMP_LABEL=NM005LFR
       ;;
(NM005LFR)
       m_CondExec 04,GE,NM005LFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM055A*)                                                  
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFT PGM=SORT       ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFT
       ;;
(NM005LFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM055                             
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/NM005LFQ.INM055AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LFT.INM055BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_3 23 CH 3
 /KEYS
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LFU
       ;;
(NM005LFU)
       m_CondExec 00,EQ,NM005LFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM055C                                       
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=NM005LFX
       ;;
(NM005LFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/NM005LFT.INM055BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LFX.INM055CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LFY
       ;;
(NM005LFY)
       m_CondExec 04,GE,NM005LFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGA PGM=SORT       ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGA
       ;;
(NM005LGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/NM005LFX.INM055CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LGA.INM055DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LGB
       ;;
(NM005LGB)
       m_CondExec 00,EQ,NM005LGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM055                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGD
       ;;
(NM005LGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/NM005LFT.INM055BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/NM005LGA.INM055DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM055 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LGE
       ;;
(NM005LGE)
       m_CondExec 04,GE,NM005LGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BNM070 : VENTES POUR LE SAV                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGG
       ;;
(NM005LGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLES EN ENTREE SANS MAJ                                                
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTFM04   : NAME=RSFM04L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM04 /dev/null
#    RTPA70   : NAME=RSPA70L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPA70 /dev/null
# *** FICHIER LOG DE CAISSE                                                    
       m_FileAssign -d SHR -g +0 FNM001 ${DATA}/PNCGL/F61.BNM001AL
# *** FICHIER EN SORTIE  (LRECL 512) POUR LE GENERATEUR D'ETAT                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 INM070 ${DATA}/PTEM/NM005LGG.INM070AL
# *** FICHIER CUMUL POUR ETAT HEBDO                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FNM070 ${DATA}/PTEM/NM005LGG.BNM070AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM070 
       JUMP_LABEL=NM005LGH
       ;;
(NM005LGH)
       m_CondExec 04,GE,NM005LGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DU FICHIER ISSU DU BNM070 POUR TRAITEMENT HEBDO                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGJ PGM=SORT       ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGJ
       ;;
(NM005LGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  EDITION CAISSES DU JOUR                                              
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PTEM/NM005LGG.BNM070AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FNM070AL
# ******  EDITION DESTINE AUX MICRO-FICHES SUR 7 JOURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PXX0/F61.FNM070AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LGK
       ;;
(NM005LGK)
       m_CondExec 00,EQ,NM005LGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER  (INM070AL)                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGM PGM=SORT       ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGM
       ;;
(NM005LGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER INM070                             
       m_FileAssign -d SHR -g ${G_A52} SORTIN ${DATA}/PTEM/NM005LGG.INM070AL
# ********************************* FICHIER TRIE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LGM.INM070BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_26_8 26 CH 8
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_18_2 18 CH 2
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_23_3 23 CH 3
 /FIELDS FLD_BI_34_2 34 CH 2
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_2 ASCENDING,
   FLD_BI_23_3 ASCENDING,
   FLD_BI_26_8 ASCENDING,
   FLD_BI_34_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LGN
       ;;
(NM005LGN)
       m_CondExec 00,EQ,NM005LGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS INM070CL                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGQ
       ;;
(NM005LGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER TRIE                             
       m_FileAssign -d SHR -g ${G_A53} FEXTRAC ${DATA}/PTEM/NM005LGM.INM070BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/NM005LGQ.INM070CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=NM005LGR
       ;;
(NM005LGR)
       m_CondExec 04,GE,NM005LGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGT PGM=SORT       ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGT
       ;;
(NM005LGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A54} SORTIN ${DATA}/PTEM/NM005LGQ.INM070CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/NM005LGT.INM070DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NM005LGU
       ;;
(NM005LGU)
       m_CondExec 00,EQ,NM005LGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : INM070                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM005LGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=NM005LGX
       ;;
(NM005LGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A55} FEXTRAC ${DATA}/PTEM/NM005LGM.INM070BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A56} FCUMULS ${DATA}/PTEM/NM005LGT.INM070DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w INM070 FEDITION
#                                                                              
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=NM005LGY
       ;;
(NM005LGY)
       m_CondExec 04,GE,NM005LGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NM005LZA
       ;;
(NM005LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM005LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
