#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FP120P.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFP120 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/26 AT 16.07.49 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FP120P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  PGM : BFP120  TRT REFACTURATIONS NETTING                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FP120PA
       ;;
(FP120PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FP120PAA
       ;;
(FP120PAA)
       m_CondExec ${EXAAA},NE,YES 
# ******************************************                                   
# ******************************************                                   
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSFP02N  : NAME=RSFP02N,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFP02N /dev/null
# ******* TABLE EN MAJ                                                         
#    RSFP01N  : NAME=RSFP01N,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFP01N /dev/null
# ******* FICHIER DES FACTURES NETTING                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FFMICRO ${DATA}/PMACP/F07.BFP120AP
# ******* FICHIER POUR EDITION D'UN ETAT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FFP120 ${DATA}/PTEM/FP120PAA.NFP121BP
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER PARAM                                                        
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/FP012P1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFP121 
       JUMP_LABEL=FP120PAB
       ;;
(FP120PAB)
       m_CondExec 04,GE,FP120PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR EDITION D'UN ETAT                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FP120PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FP120PAD
       ;;
(FP120PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FP120PAA.NFP121BP
# ******* FICHIER A IMPRIMER                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FP120PAD.NFP121CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_15 13 CH 15
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FP120PAE
       ;;
(FP120PAE)
       m_CondExec 00,EQ,FP120PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFP130  CREATION ETAT EOS                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP FP120PAG PGM=BFP130     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FP120PAG
       ;;
(FP120PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FFP120 ${DATA}/PTEM/FP120PAD.NFP121CP
# ******* ETAT EOS                                                             
       m_OutputAssign -c J -w JFP130 JFP130
       m_ProgramExec BFP130 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FP120PZA
       ;;
(FP120PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FP120PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
