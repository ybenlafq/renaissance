#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RB002P.ksh                       --- VERSION DU 08/10/2016 12:46
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRB002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.16.15 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RB002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BRB002 : TRADUCTION DU FICHIER NAGRA AU FORMAT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RB002PA
       ;;
(RB002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 15.16.14 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: RB002P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXTRAC DONNES NAGRA'                   
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RB002PAA
       ;;
(RB002PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#  DEPENDANCE HORAIRE 22 H 00 ET 05 H 00                                       
# **************************************                                       
# **************************************                                       
# ****** FIC D ENTREE EN PROVENANCE DE CFT                                     
       m_FileAssign -d SHR FNA000 ${DATA}/PXX0/FTP.F07.NAGRA01P
#                                                                              
# ****** FIC DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FRB002 ${DATA}/PXX0/F07.BRB002AP
       m_ProgramExec BRB002 
# ********************************************************************         
#  DELETE DES FICHIERS                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RB002PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RB002PAD
       ;;
(RB002PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB002PAD.sysin
       m_UtilityExec
# ******************************************************************           
#  CREATION D'UN FICHER NAGRA01P VIDE                                          
#  REPRISE : OUI                                                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB002PAE
       ;;
(RB002PAE)
       m_CondExec 16,NE,RB002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RB002PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RB002PAG
       ;;
(RB002PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   FIC VENTES                                                          
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.NAGRA01P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB002PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RB002PAH
       ;;
(RB002PAH)
       m_CondExec 16,NE,RB002PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SORTANT DE BRB002                                            
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB002PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RB002PAJ
       ;;
(RB002PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BRB002AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRB002DP.ANOMALIE
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BRB002BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 01 CH 25
 /FIELDS FLD_CH_35_8 35 CH 08
 /KEYS
   FLD_CH_35_8 ASCENDING,
   FLD_CH_1_25 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RB002PAK
       ;;
(RB002PAK)
       m_CondExec 00,EQ,RB002PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRB003 : INTEGRATION DU FICHIER DANS LES BASES                          
#               ET FORMATTAGE D UN FICHIER ANOMALIES A RECYCLER                
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RB002PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RB002PAM
       ;;
(RB002PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE : FICHIER TRIE DU BRB000                                      
       m_FileAssign -d SHR -g ${G_A2} FRB002 ${DATA}/PXX0/F07.BRB002BP
#                                                                              
# ******  SORTIE : FICHIER ANOMALIES                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FRBANO ${DATA}/PTEM/B0002PAD.BRB002CP
#                                                                              
# ******  SORTIE : NOUVEAU FICHIER POUR LE LENDEMAIN                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FRB003 ${DATA}/PXX0/F07.BRB002DP.ANOMALIE
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTRB00   : NAME=RSRB00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB00 /dev/null
#    RTRB01   : NAME=RSRB01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRB003 
       JUMP_LABEL=RB002PAN
       ;;
(RB002PAN)
       m_CondExec 04,GE,RB002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RB002PZA
       ;;
(RB002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RB002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
