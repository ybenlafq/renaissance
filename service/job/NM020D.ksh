#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NM020D.ksh                       --- VERSION DU 08/10/2016 12:49
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDNM020 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/06/15 AT 17.55.25 BY BURTEC3                      
#    STANDARDS: P  JOBSET: NM020D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  BNM002 : CE PGM DETECTE DANS LE FICHIER FFTI01 L'ENSEMBLE DES               
#           ECRITURES NECESSITANT UNE RETROCESSION NETTING.                    
#                                                                              
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NM020DA
       ;;
(NM020DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=NM020DAA
       ;;
(NM020DAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
#  DEPENDANCES POUR PLAN :              *                                      
#    OBLIGATOIRE POUR LOGIQUE APPL      *                                      
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ************** TABLES EN LECTURE ***************                             
# *****   TABLE GENERALISEE (SOUS TABLES TAUX DE TVA)                          
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ************  FICHIERS EN ENTREE ***************                             
# ******* FIC EN PROVENANCE DE L'INTERFACE CAISSE NEM                          
# ******* PEUT ETRE VIDE OU PLEIN EN FONTION DE LA BONNE EXECUTION             
# ******* DE LA CHAINE N.E.M + FICHIER ANO + FICHIER CUMUL.                    
       m_FileAssign -d SHR -g +0 FFTI01 ${DATA}/PNCGD/F91.BNM001CD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM002BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNMCUMBD
#                                                                              
# **************  FICHIERS EN SORTIE ***************                           
# ******  FICHIER A DESTINATION DU NETTING                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -g +1 FFMICRO ${DATA}/PNCGD/F91.BNM002AD
# ******  FICHIER DES ANO POUR (J+1)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 FREPRIS ${DATA}/PNCGD/F91.BNM002BD
# ******  LISTE D'ERREUR DE PARAMETRAGE                                        
       m_OutputAssign -c 9 -w IMPRIM IMPRIM
#                                                                              
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNM002 
       JUMP_LABEL=NM020DAB
       ;;
(NM020DAB)
       m_CondExec 04,GE,NM020DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER DE CUMUL SI INTERFACE OK                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NM020DAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NM020DAD
       ;;
(NM020DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DE LA VEILLE                                            
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DE CUMUL REMIS A ZERO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PNCGD/F91.BNMCUMBD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NM020DAD.sysin
       m_UtilityExec
# ******                                                                       
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NM020DAE
       ;;
(NM020DAE)
       m_CondExec 16,NE,NM020DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
