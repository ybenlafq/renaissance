      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGT00   EGT00                                              00000020
      ***************************************************************** 00000030
       01   EGT00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABLE1L      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCTABLE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTABLE1F      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTABLE1I      PIC X(6).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTABLEG2L     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTABLEG2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTABLEG2F     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTABLEG2I     PIC X(15).                                 00000330
           02 MLIGNESI OCCURS   14 TIMES .                              00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDOMAPPLL   COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCDOMAPPLL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCDOMAPPLF   PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCDOMAPPLI   PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTABLEL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCTABLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTABLEF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCTABLEI     PIC X(6).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTABLEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLTABLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLTABLEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLTABLEI     PIC X(30).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEAUTOL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MTYPEAUTOL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTYPEAUTOF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MTYPEAUTOI   PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEDITL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCEDITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEDITF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCEDITI      PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: EGT00   EGT00                                              00000760
      ***************************************************************** 00000770
       01   EGT00O REDEFINES EGT00I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNUMPAGEA      PIC X.                                     00000950
           02 MNUMPAGEC PIC X.                                          00000960
           02 MNUMPAGEP PIC X.                                          00000970
           02 MNUMPAGEH PIC X.                                          00000980
           02 MNUMPAGEV PIC X.                                          00000990
           02 MNUMPAGEO      PIC X(2).                                  00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MPAGEMAXA      PIC X.                                     00001020
           02 MPAGEMAXC PIC X.                                          00001030
           02 MPAGEMAXP PIC X.                                          00001040
           02 MPAGEMAXH PIC X.                                          00001050
           02 MPAGEMAXV PIC X.                                          00001060
           02 MPAGEMAXO      PIC X(2).                                  00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCFONCA   PIC X.                                          00001090
           02 MCFONCC   PIC X.                                          00001100
           02 MCFONCP   PIC X.                                          00001110
           02 MCFONCH   PIC X.                                          00001120
           02 MCFONCV   PIC X.                                          00001130
           02 MCFONCO   PIC X(3).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MCTABLE1A      PIC X.                                     00001160
           02 MCTABLE1C PIC X.                                          00001170
           02 MCTABLE1P PIC X.                                          00001180
           02 MCTABLE1H PIC X.                                          00001190
           02 MCTABLE1V PIC X.                                          00001200
           02 MCTABLE1O      PIC X(6).                                  00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCTABLEG2A     PIC X.                                     00001230
           02 MCTABLEG2C     PIC X.                                     00001240
           02 MCTABLEG2P     PIC X.                                     00001250
           02 MCTABLEG2H     PIC X.                                     00001260
           02 MCTABLEG2V     PIC X.                                     00001270
           02 MCTABLEG2O     PIC X(15).                                 00001280
           02 MLIGNESO OCCURS   14 TIMES .                              00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MCDOMAPPLA   PIC X.                                     00001310
             03 MCDOMAPPLC   PIC X.                                     00001320
             03 MCDOMAPPLP   PIC X.                                     00001330
             03 MCDOMAPPLH   PIC X.                                     00001340
             03 MCDOMAPPLV   PIC X.                                     00001350
             03 MCDOMAPPLO   PIC X(7).                                  00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MCTABLEA     PIC X.                                     00001380
             03 MCTABLEC     PIC X.                                     00001390
             03 MCTABLEP     PIC X.                                     00001400
             03 MCTABLEH     PIC X.                                     00001410
             03 MCTABLEV     PIC X.                                     00001420
             03 MCTABLEO     PIC X(6).                                  00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MLTABLEA     PIC X.                                     00001450
             03 MLTABLEC     PIC X.                                     00001460
             03 MLTABLEP     PIC X.                                     00001470
             03 MLTABLEH     PIC X.                                     00001480
             03 MLTABLEV     PIC X.                                     00001490
             03 MLTABLEO     PIC X(30).                                 00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MTYPEAUTOA   PIC X.                                     00001520
             03 MTYPEAUTOC   PIC X.                                     00001530
             03 MTYPEAUTOP   PIC X.                                     00001540
             03 MTYPEAUTOH   PIC X.                                     00001550
             03 MTYPEAUTOV   PIC X.                                     00001560
             03 MTYPEAUTOO   PIC X(3).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MCEDITA      PIC X.                                     00001590
             03 MCEDITC PIC X.                                          00001600
             03 MCEDITP PIC X.                                          00001610
             03 MCEDITH PIC X.                                          00001620
             03 MCEDITV PIC X.                                          00001630
             03 MCEDITO      PIC X.                                     00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(78).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
