      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IDS001 AU 06/05/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,06,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,03,PD,A,                          *        
      *                           24,05,BI,A,                          *        
      *                           29,07,BI,A,                          *        
      *                           36,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IDS001.                                                        
            05 NOMETAT-IDS001           PIC X(6) VALUE 'IDS001'.                
            05 RUPTURES-IDS001.                                                 
           10 IDS001-FILIALE            PIC X(03).                      007  003
           10 IDS001-MAGASIN            PIC X(06).                      010  006
           10 IDS001-RAYON              PIC X(05).                      016  005
           10 IDS001-WSEQFAM            PIC S9(05)      COMP-3.         021  003
           10 IDS001-CMARQ              PIC X(05).                      024  005
           10 IDS001-NCODIC             PIC X(07).                      029  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IDS001-SEQUENCE           PIC S9(04) COMP.                036  002
      *--                                                                       
           10 IDS001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IDS001.                                                   
           10 IDS001-CAPPRO             PIC X(05).                      038  005
           10 IDS001-CFAM               PIC X(05).                      043  005
           10 IDS001-DATE               PIC X(10).                      048  010
           10 IDS001-LFILIALE           PIC X(20).                      058  020
           10 IDS001-LREFFOURN          PIC X(20).                      078  020
           10 IDS001-TLLIEU             PIC X(20).                      098  020
           10 IDS001-PRMP               PIC S9(07)V9(2) COMP-3.         118  005
           10 IDS001-PVHT               PIC S9(07)V9(2) COMP-3.         123  005
           10 IDS001-QSTOCK             PIC S9(06)      COMP-3.         128  004
            05 FILLER                      PIC X(381).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IDS001-LONG           PIC S9(4)   COMP  VALUE +131.           
      *                                                                         
      *--                                                                       
        01  DSECT-IDS001-LONG           PIC S9(4) COMP-5  VALUE +131.           
                                                                                
      *}                                                                        
