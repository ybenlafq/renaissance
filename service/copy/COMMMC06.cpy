      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MMC01                                                 
      * APPELLE PAR LINK DANS TMC00                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MMC01.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
              10 COMM-MMC01-AAMMJJ       PIC X(06).                     01560   
              10 COMM-MMC01-JJMMSSAA     PIC X(08).                     01560   
              10 COMM-MMC01-INDMAX       PIC S9(5) COMP-3.              01560   
              10 COMM-MMC01-HEURE        PIC 9(08).                     01560   
         05  COMM-MMC01-DEMAND-EDIT     PIC X(01).                              
              88 COMM-MMC01-EDIT-TOUT     VALUE '1'.                            
      *      MESSAGE ERREUR                                                     
         05 COMM-MMC01-RETOUR.                                                  
             10 COMM-MMC01-CODRET        PIC X(01).                             
                  88  COMM-MMC01-OK     VALUE ' '.                              
                  88  COMM-MMC01-ERREUR VALUE '1'.                              
             10 COMM-MMC01-LIBERR        PIC X(60).                             
         05 FILLER                       PIC X(30).                             
                                                                                
