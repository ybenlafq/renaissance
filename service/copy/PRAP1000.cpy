      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE AP1000                       
      ******************************************************************        
      *                                                                         
       CLEF-AP1000             SECTION.                                         
      *                                                                         
           MOVE 'RVAP1000          '       TO   TABLE-NAME.                     
           MOVE 'AP1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-AP1000. EXIT.                                                   
                EJECT                                                           
                                                                                
