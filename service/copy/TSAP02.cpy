      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP02-RECORD.                                                      
           05 TS-AP02-TABLE            OCCURS 5.                                
              10 TS-AP02-NCLIENT        PIC X(08).                              
              10 TS-AP02-NMPRNM         PIC X(44).                              
              10 TS-AP02-ADR1           PIC X(25).                              
              10 TS-AP02-ADR2           PIC X(32).                              
              10 TS-AP02-ADR3           PIC X(30).                              
                                                                                
