      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: histo maj batch:listing trt                                00000020
      ***************************************************************** 00000030
       01   EHT02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPLIL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCAPPLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPLIF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCAPPLII  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAPPLIL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLAPPLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAPPLIF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLAPPLII  PIC X(30).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTRANSI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRANSF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTRANSI  PIC X(30).                                      00000370
           02 MTABLGRPI OCCURS   10 TIMES .                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MDATEI  PIC X(10).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHEUREL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MHEUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MHEUREF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MHEUREI      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSTATUTI     PIC X(2).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTRTL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLTRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLTRTF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLTRTI  PIC X(30).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NBENREGL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 NBENREGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 NBENREGF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 NBENREGI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(78).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: histo maj batch:listing trt                                00000800
      ***************************************************************** 00000810
       01   EHT02O REDEFINES EHT02I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 NPAGEA    PIC X.                                          00000990
           02 NPAGEC    PIC X.                                          00001000
           02 NPAGEP    PIC X.                                          00001010
           02 NPAGEH    PIC X.                                          00001020
           02 NPAGEV    PIC X.                                          00001030
           02 NPAGEO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 NBPAGEA   PIC X.                                          00001060
           02 NBPAGEC   PIC X.                                          00001070
           02 NBPAGEP   PIC X.                                          00001080
           02 NBPAGEH   PIC X.                                          00001090
           02 NBPAGEV   PIC X.                                          00001100
           02 NBPAGEO   PIC X(3).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MCAPPLIA  PIC X.                                          00001130
           02 MCAPPLIC  PIC X.                                          00001140
           02 MCAPPLIP  PIC X.                                          00001150
           02 MCAPPLIH  PIC X.                                          00001160
           02 MCAPPLIV  PIC X.                                          00001170
           02 MCAPPLIO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLAPPLIA  PIC X.                                          00001200
           02 MLAPPLIC  PIC X.                                          00001210
           02 MLAPPLIP  PIC X.                                          00001220
           02 MLAPPLIH  PIC X.                                          00001230
           02 MLAPPLIV  PIC X.                                          00001240
           02 MLAPPLIO  PIC X(30).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCTRANSA  PIC X.                                          00001270
           02 MCTRANSC  PIC X.                                          00001280
           02 MCTRANSP  PIC X.                                          00001290
           02 MCTRANSH  PIC X.                                          00001300
           02 MCTRANSV  PIC X.                                          00001310
           02 MCTRANSO  PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLTRANSA  PIC X.                                          00001340
           02 MLTRANSC  PIC X.                                          00001350
           02 MLTRANSP  PIC X.                                          00001360
           02 MLTRANSH  PIC X.                                          00001370
           02 MLTRANSV  PIC X.                                          00001380
           02 MLTRANSO  PIC X(30).                                      00001390
           02 MTABLGRPO OCCURS   10 TIMES .                             00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MDATEA  PIC X.                                          00001420
             03 MDATEC  PIC X.                                          00001430
             03 MDATEP  PIC X.                                          00001440
             03 MDATEH  PIC X.                                          00001450
             03 MDATEV  PIC X.                                          00001460
             03 MDATEO  PIC X(10).                                      00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MHEUREA      PIC X.                                     00001490
             03 MHEUREC PIC X.                                          00001500
             03 MHEUREP PIC X.                                          00001510
             03 MHEUREH PIC X.                                          00001520
             03 MHEUREV PIC X.                                          00001530
             03 MHEUREO      PIC X(5).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MSTATUTA     PIC X.                                     00001560
             03 MSTATUTC     PIC X.                                     00001570
             03 MSTATUTP     PIC X.                                     00001580
             03 MSTATUTH     PIC X.                                     00001590
             03 MSTATUTV     PIC X.                                     00001600
             03 MSTATUTO     PIC X(2).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MLTRTA  PIC X.                                          00001630
             03 MLTRTC  PIC X.                                          00001640
             03 MLTRTP  PIC X.                                          00001650
             03 MLTRTH  PIC X.                                          00001660
             03 MLTRTV  PIC X.                                          00001670
             03 MLTRTO  PIC X(30).                                      00001680
             03 FILLER       PIC X(2).                                  00001690
             03 NBENREGA     PIC X.                                     00001700
             03 NBENREGC     PIC X.                                     00001710
             03 NBENREGP     PIC X.                                     00001720
             03 NBENREGH     PIC X.                                     00001730
             03 NBENREGV     PIC X.                                     00001740
             03 NBENREGO     PIC X(7).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(78).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
