      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTSB11                             *        
      ******************************************************************        
       01  RVSB1100.                                                            
           10 SB11-NSOCIETE             PIC X(3).                               
           10 SB11-NLIEU                PIC X(3).                               
           10 SB11-NVENTE               PIC X(7).                               
           10 SB11-CTYPENREG            PIC X(1).                               
           10 SB11-NCODICGRP            PIC X(7).                               
           10 SB11-NCODIC               PIC X(7).                               
           10 SB11-NSEQ                 PIC X(2).                               
           10 SB11-CMODDEL              PIC X(3).                               
           10 SB11-DDELIV               PIC X(8).                               
           10 SB11-NORDRE               PIC X(5).                               
           10 SB11-CENREG               PIC X(5).                               
           10 SB11-QVENDUE              PIC S9(5)V USAGE COMP-3.                
           10 SB11-PVUNIT               PIC S9(7)V9(2) USAGE COMP-3.            
           10 SB11-PVUNITF              PIC S9(7)V9(2) USAGE COMP-3.            
           10 SB11-PVTOTAL              PIC S9(7)V9(2) USAGE COMP-3.            
           10 SB11-CEQUIPE              PIC X(5).                               
           10 SB11-NLIGNE               PIC X(2).                               
           10 SB11-TAUXTVA              PIC S9(3)V9(2) USAGE COMP-3.            
           10 SB11-QCONDT               PIC S9(5)V USAGE COMP-3.                
           10 SB11-PRMP                 PIC S9(7)V9(2) USAGE COMP-3.            
           10 SB11-WEMPORTE             PIC X(1).                               
           10 SB11-CPLAGE               PIC X(2).                               
           10 SB11-CPROTOUR             PIC X(5).                               
           10 SB11-CADRTOUR             PIC X(1).                               
           10 SB11-CPOSTAL              PIC X(5).                               
           10 SB11-LCOMMENT             PIC X(35).                              
           10 SB11-CVENDEUR             PIC X(6).                               
           10 SB11-NSOCLIVR             PIC X(3).                               
           10 SB11-NDEPOT               PIC X(3).                               
           10 SB11-NAUTORM              PIC X(5).                               
           10 SB11-WARTINEX             PIC X(1).                               
           10 SB11-WEDITBL              PIC X(1).                               
           10 SB11-WACOMMUTER           PIC X(1).                               
           10 SB11-WCQERESF             PIC X(1).                               
           10 SB11-NMUTATION            PIC X(7).                               
           10 SB11-CTOURNEE             PIC X(8).                               
           10 SB11-WTOPELIVRE           PIC X(1).                               
           10 SB11-DCOMPTA              PIC X(8).                               
           10 SB11-DCREATION            PIC X(8).                               
           10 SB11-HCREATION            PIC X(4).                               
           10 SB11-DANNULATION          PIC X(8).                               
           10 SB11-NLIEUORIG            PIC X(3).                               
           10 SB11-WINTMAJ              PIC X(1).                               
           10 SB11-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 SB11-DSTAT                PIC X(4).                               
           10 SB11-CPRESTGRP            PIC X(5).                               
           10 SB11-NSOCMODIF            PIC X(3).                               
           10 SB11-NLIEUMODIF           PIC X(3).                               
           10 SB11-DATENC               PIC X(8).                               
           10 SB11-CDEV                 PIC X(3).                               
           10 SB11-WUNITR               PIC X(20).                              
           10 SB11-LRCMMT               PIC X(10).                              
           10 SB11-NSOCP                PIC X(3).                               
           10 SB11-NLIEUP               PIC X(3).                               
           10 SB11-NTRANS               PIC S9(8)V USAGE COMP-3.                
           10 SB11-NSEQFV               PIC X(2).                               
           10 SB11-NSEQNQ               PIC S9(5)V USAGE COMP-3.                
           10 SB11-NSEQREF              PIC S9(5)V USAGE COMP-3.                
           10 SB11-NMODIF               PIC S9(5)V USAGE COMP-3.                
           10 SB11-NSOCORIG             PIC X(3).                               
           10 SB11-DTOPE                PIC X(8).                               
           10 SB11-NSOCLIVR1            PIC X(3).                               
           10 SB11-NDEPOT1              PIC X(3).                               
           10 SB11-NSOCGEST             PIC X(3).                               
           10 SB11-NLIEUGEST            PIC X(3).                               
           10 SB11-NSOCDEPLIV           PIC X(3).                               
           10 SB11-NLIEUDEPLIV          PIC X(3).                               
           10 SB11-TYPVTE               PIC X(1).                               
           10 SB11-CTYPENT              PIC X(2).                               
           10 SB11-NLIEN                PIC S9(5)V USAGE COMP-3.                
           10 SB11-NACTVTE              PIC S9(5)V USAGE COMP-3.                
           10 SB11-PVCODIG              PIC S9(7)V9(2) USAGE COMP-3.            
           10 SB11-QTCODIG              PIC S9(5)V USAGE COMP-3.                
           10 SB11-DPRLG                PIC X(8).                               
           10 SB11-CALERTE              PIC X(5).                               
           10 SB11-CREPRISE             PIC X(5).                               
           10 SB11-NSEQENS              PIC S9(5)V USAGE COMP-3.                
           10 SB11-MPRIMECLI            PIC S9(7)V9(2) USAGE COMP-3.            
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTSB11.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 78 TIMES.          
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 78 TIMES.              
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 78      *        
      ******************************************************************        
                                                                                
