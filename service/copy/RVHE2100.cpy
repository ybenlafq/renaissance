      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHE2100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE2100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE2100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE2100.                                                            
      *}                                                                        
           02  HE21-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE21-CLIEUHET                                                    
               PIC X(0005).                                                     
           02  HE21-CLIEUHEI                                                    
               PIC X(0005).                                                     
           02  HE21-NLOTDIAG                                                    
               PIC X(0007).                                                     
           02  HE21-DLOTDIAG                                                    
               PIC X(0008).                                                     
           02  HE21-QLOTDIAG                                                    
               PIC S9(7) COMP-3.                                                
           02  HE21-WFLAG                                                       
               PIC X(0001).                                                     
           02  HE21-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE2100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE2100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE2100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-CLIEUHET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-CLIEUHET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-CLIEUHEI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-CLIEUHEI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-NLOTDIAG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-NLOTDIAG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-DLOTDIAG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-DLOTDIAG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-QLOTDIAG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-QLOTDIAG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE21-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE21-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
