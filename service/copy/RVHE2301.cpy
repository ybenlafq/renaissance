      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHE2301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE2301                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHE2301.                                                            
           02  HE23-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE23-CLIEUHET                                                    
               PIC X(0005).                                                     
           02  HE23-NDEMANDE                                                    
               PIC X(0007).                                                     
           02  HE23-DDEMANDE                                                    
               PIC X(0008).                                                     
           02  HE23-QDEMANDE                                                    
               PIC S9(7) COMP-3.                                                
           02  HE23-WFLAG                                                       
               PIC X(0001).                                                     
           02  HE23-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HE23-DRELANCE1                                                   
               PIC X(0008).                                                     
           02  HE23-DRELANCE2                                                   
               PIC X(0008).                                                     
           02  HE23-DRELANCE3                                                   
               PIC X(0008).                                                     
           02  HE23-ADRESSE                                                     
               PIC X(0007).                                                     
           02  HE23-DVISITE                                                     
               PIC X(0008).                                                     
           02  HE23-DACCORD                                                     
               PIC X(0008).                                                     
           02  HE23-NACCORD                                                     
               PIC X(0012).                                                     
           02  HE23-DDACCORD                                                    
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE2301                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHE2301-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-CLIEUHET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-CLIEUHET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-NDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-NDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-QDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-QDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DRELANCE1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DRELANCE1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DRELANCE2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DRELANCE2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DRELANCE3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DRELANCE3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DVISITE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DVISITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE23-DDACCORD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE23-DDACCORD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
