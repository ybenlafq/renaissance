      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC102  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST A SOUMETTRE AU    *         
      *                   TRAITEMENT                                  *         
      *                                                               *         
      *    FBC102       : FICHIER DES EN-TETES DE VENTE               *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  108 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC102-ENREG.                                                        
           03  FBC102-CLE.                                                      
               05  FBC102-NSOCIETE            PIC  X(03).                       
               05  FBC102-NLIEU               PIC  X(03).                       
               05  FBC102-NVENTE              PIC  X(08).                       
           03  FBC102-DVENTE                  PIC  X(08).                       
           03  FBC102-PTTVENTE                PIC  S9(07)V9(02).                
           03  FBC102-CREMVTE                 PIC  X(05).                       
           03  FBC102-PREMVTE                 PIC  S9(07)V9(02).                
           03  FBC102-DMODIFVTE               PIC  X(08).                       
           03  FBC102-WEXPORT                 PIC  X(01).                       
           03  FBC102-PDIFFERE                PIC  S9(07)V9(02).                
           03  FBC102-CORGORED                PIC  X(05).                       
           03  FBC102-CFCRED                  PIC  X(05).                       
           03  FBC102-NCREDI                  PIC  X(14).                       
           03  FBC102-DVERSION                PIC  X(08).                       
           03  FBC102-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
