      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-PC21-RECORD.                                                      
           05 TS-PC21-TABLE            OCCURS 10.                               
              10 TS-PC21-ACT                   PIC X(01).                       
              10 TS-PC21-STATUT                PIC X(10).                       
              10 TS-PC21-NCDE                  PIC X(12).                       
              10 TS-PC21-DCDE                  PIC X(10).                       
              10 TS-PC21-NCOMPTE               PIC X(08).                       
              10 TS-PC21-RS                    PIC X(25).                       
              10 TS-PC21-FLAG-REG              PIC X(01).                       
                                                                                
