      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM085 AU 26/02/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,03,BI,A,                          *        
      *                           27,08,BI,A,                          *        
      *                           35,08,BI,A,                          *        
      *                           43,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM085.                                                        
            05 NOMETAT-INM085           PIC X(6) VALUE 'INM085'.                
            05 RUPTURES-INM085.                                                 
           10 INM085-DATETR             PIC X(08).                      007  008
           10 INM085-NSOC               PIC X(03).                      015  003
           10 INM085-NLIEU              PIC X(03).                      018  003
           10 INM085-NSOCV              PIC X(03).                      021  003
           10 INM085-NLIEUV             PIC X(03).                      024  003
           10 INM085-NTRANS             PIC X(08).                      027  008
           10 INM085-NVENT              PIC X(08).                      035  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM085-SEQUENCE           PIC S9(04) COMP.                043  002
      *--                                                                       
           10 INM085-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM085.                                                   
           10 INM085-DEVREF             PIC X(15).                      045  015
           10 INM085-PCREDIT            PIC S9(07)V9(2) COMP-3.         060  005
           10 INM085-PMONTANT           PIC S9(07)V9(2) COMP-3.         065  005
            05 FILLER                      PIC X(443).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM085-LONG           PIC S9(4)   COMP  VALUE +069.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM085-LONG           PIC S9(4) COMP-5  VALUE +069.           
                                                                                
      *}                                                                        
