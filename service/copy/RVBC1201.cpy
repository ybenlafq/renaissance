      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1201                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1201.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCLIENTADR                                        
           10 BC12-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       NFOYERADR                                         
           10 BC12-NFOYERADR       PIC X(8).                                    
      *    *************************************************************        
      *                       CTYPCLIENT                                        
           10 BC12-CTYPCLIENT      PIC X(1).                                    
      *    *************************************************************        
      *                       CTITRENOM                                         
           10 BC12-CTITRENOM       PIC X(5).                                    
      *    *************************************************************        
      *                       LNOM                                              
           10 BC12-LNOM            PIC X(25).                                   
      *    *************************************************************        
      *                       LPRENOM                                           
           10 BC12-LPRENOM         PIC X(15).                                   
      *    *************************************************************        
      *                       NADRESSE                                          
           10 BC12-NADRESSE        PIC X(8).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC12-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       NUMERO DE CARTE                                   
           10 BC12-NCARTE          PIC X(09).                                   
      *    *************************************************************        
      *                       DATE DE NAISSANCE                                 
           10 BC12-DNAISS          PIC X(8).                                    
      *    *************************************************************        
      *                       NUMERO D'ORGANISATION TABLE BC16                  
           10 BC12-NORG            PIC X(06).                                   
      *    *************************************************************        
      *                       EXISTENCE D'UN LIEN AVEC UN AUTRE CLIENT          
           10 BC12-LIEN            PIC X(01).                                   
      *    *************************************************************        
      *                       ORIGINE DE CE CLIENT B/T                          
           10 BC12-ORIGINE         PIC X(01).                                   
      *    *************************************************************        
      *                       DERNIERE MODIF B/T                                
           10 BC12-MODIF           PIC X(01).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1201-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-NFOYERADR-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-NFOYERADR-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-CTYPCLIENT-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-CTYPCLIENT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-CTITRENOM-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-CTITRENOM-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-LNOM-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-LNOM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-LPRENOM-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-LPRENOM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-NADRESSE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-NADRESSE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-NCARTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-NCARTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-DNAISS-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-DNAISS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-NORG-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-NORG-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-LIEN-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-LIEN-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-ORIGINE-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-ORIGINE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC12-MODIF-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC12-MODIF-F         PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *        
      ******************************************************************        
                                                                                
