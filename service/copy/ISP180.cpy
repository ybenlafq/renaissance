      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP180 AU 10/05/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,07,BI,A,                          *        
      *                           19,03,PD,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,20,BI,A,                          *        
      *                           47,07,BI,A,                          *        
      *                           54,02,BI,A,                          *        
      *                           56,03,BI,A,                          *        
      *                           59,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP180.                                                        
            05 NOMETAT-ISP180           PIC X(6) VALUE 'ISP180'.                
            05 RUPTURES-ISP180.                                                 
           10 ISP180-COULEUR            PIC X(05).                      007  005
           10 ISP180-NCDE               PIC X(07).                      012  007
           10 ISP180-WSEQFAM            PIC S9(05)      COMP-3.         019  003
           10 ISP180-CMARQ              PIC X(05).                      022  005
           10 ISP180-LREFFOURN          PIC X(20).                      027  020
           10 ISP180-NCODIC             PIC X(07).                      047  007
           10 ISP180-NZONPRIX           PIC X(02).                      054  002
           10 ISP180-CDEVISE            PIC X(03).                      056  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP180-SEQUENCE           PIC S9(04) COMP.                059  002
      *--                                                                       
           10 ISP180-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP180.                                                   
           10 ISP180-CFAM               PIC X(05).                      061  005
           10 ISP180-CSTATUT            PIC X(03).                      066  003
           10 ISP180-DEROGATION         PIC X(01).                      069  001
           10 ISP180-DTRAIT             PIC X(10).                      070  010
           10 ISP180-NSOC               PIC X(03).                      080  003
           10 ISP180-ECART              PIC S9(04)V9(1) COMP-3.         083  003
           10 ISP180-PVTTC              PIC S9(08)V9(2) COMP-3.         086  006
           10 ISP180-SRPTTC             PIC S9(08)V9(2) COMP-3.         092  006
           10 ISP180-DEFFETPV           PIC X(08).                      098  008
           10 ISP180-DLIVRAISON         PIC X(08).                      106  008
           10 ISP180-DVALIDITE          PIC X(08).                      114  008
            05 FILLER                      PIC X(391).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP180-LONG           PIC S9(4)   COMP  VALUE +121.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP180-LONG           PIC S9(4) COMP-5  VALUE +121.           
                                                                                
      *}                                                                        
