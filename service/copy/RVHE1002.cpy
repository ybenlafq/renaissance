      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVHE1002                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE1002                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE1002.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE1002.                                                            
      *}                                                                        
           02  HE10-NLIEUHED                                                    
               PIC X(0003).                                                     
           02  HE10-NGHE                                                        
               PIC X(0007).                                                     
           02  HE10-NCODIC                                                      
               PIC X(0007).                                                     
           02  HE10-DVENTE                                                      
               PIC X(0008).                                                     
           02  HE10-NLIEUVTE                                                    
               PIC X(0003).                                                     
           02  HE10-NVENTE                                                      
               PIC X(0007).                                                     
           02  HE10-DDEPOT                                                      
               PIC X(0008).                                                     
           02  HE10-CTRAIT                                                      
               PIC X(0005).                                                     
           02  HE10-CTYPHS                                                      
               PIC X(0005).                                                     
           02  HE10-LPANNE                                                      
               PIC X(0020).                                                     
           02  HE10-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  HE10-LNOM                                                        
               PIC X(0025).                                                     
           02  HE10-LPRENOM                                                     
               PIC X(0015).                                                     
           02  HE10-LADR1                                                       
               PIC X(0032).                                                     
           02  HE10-LADR2                                                       
               PIC X(0032).                                                     
           02  HE10-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  HE10-LCOMMUNE                                                    
               PIC X(0026).                                                     
           02  HE10-NTEL                                                        
               PIC X(0008).                                                     
           02  HE10-CLIEUHET                                                    
               PIC X(0005).                                                     
           02  HE10-NRETOUR                                                     
               PIC X(0007).                                                     
           02  HE10-CREFUS                                                      
               PIC X(0005).                                                     
           02  HE10-CPANNE                                                      
               PIC X(0005).                                                     
           02  HE10-CLIEUHEI                                                    
               PIC X(0005).                                                     
           02  HE10-NLOTDIAG                                                    
               PIC X(0007).                                                     
           02  HE10-NENVOI                                                      
               PIC X(0007).                                                     
           02  HE10-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE10-NTYOPE                                                      
               PIC X(0003).                                                     
           02  HE10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HE10-NLIEU                                                       
               PIC X(0003).                                                     
           02  HE10-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  HE10-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  HE10-WSOLDE                                                      
               PIC X(0001).                                                     
           02  HE10-NSERIE                                                      
               PIC X(0016).                                                     
           02  HE10-NACCORD                                                     
               PIC X(0012).                                                     
           02  HE10-LNOMACCORD                                                  
               PIC X(0010).                                                     
           02  HE10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HE10-ADRESSE                                                     
               PIC X(0007).                                                     
           02  HE10-NFA                                                         
               PIC X(0007).                                                     
           02  HE10-NLIEURET                                                    
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE1001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE1002-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE1002-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NLIEUHED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NLIEUHED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NGHE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NGHE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NLIEUVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NLIEUVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-DDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-DDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CTYPHS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CTYPHS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LPANNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LPANNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LADR1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LADR1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LADR2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LADR2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NTEL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NTEL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CLIEUHET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CLIEUHET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NRETOUR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NRETOUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CREFUS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CREFUS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CPANNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CPANNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CLIEUHEI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CLIEUHEI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NLOTDIAG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NLOTDIAG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NTYOPE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NTYOPE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-WSOLDE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-WSOLDE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NSERIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NSERIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-LNOMACCORD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-LNOMACCORD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-FNA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-FNA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE10-NLIEURET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE10-NLIEURET-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
