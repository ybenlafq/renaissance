      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP01-RECORD.                                                      
           05 TS-AP01-TABLE            OCCURS 10.                               
              10 TS-AP01-NUMCRI         PIC X(17).                              
              10 TS-AP01-NUMVTE         PIC X(14).                              
              10 TS-AP01-NUMCLT         PIC X(08).                              
              10 TS-AP01-DATEANO        PIC X(10).                              
              10 TS-AP01-CSTATUT        PIC X(03).                              
              10 TS-AP01-CODEANO        PIC X(02).                              
              10 TS-AP01-COMMENT        PIC X(15).                              
              10 TS-AP01-NSEQID         PIC S9(18) COMP-3.                      
                                                                                
