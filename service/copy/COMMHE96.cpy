      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *    APPEL A MHE90 , TABLES STOCKS (GS10, 30, 40, 43, 60, 70)   *         
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-HE96-LONG-COMMAREA PIC S9(4) COMP VALUE +200.                   
      *--                                                                       
       01  COMM-HE96-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
       01    Z-COMMAREA-HE96.                                                   
                                                                        00000830
      *      ZONES PASSEES A MHE96                                              
           02      COMM-HE96-CPROG         PIC X(05).                           
           02      COMM-HE96-DOPER         PIC X(08).                           
           02      COMM-HE96-NORIGINE      PIC X(07).                           
           02      COMM-HE96-NHS           PIC X(07).                           
           02      COMM-HE96-NCODIC        PIC X(07).                           
           02      COMM-HE96-CMOTIF        PIC X(10).                           
           02      COMM-HE96-NLIEUEMET     PIC X(3).                            
           02      COMM-HE96-NLIEUHC       PIC X(3).                            
           02      COMM-HE96-CPROVORIG     PIC X(1).                            
           02      COMM-HE96-CTYPLIEU      PIC X(1).                            
      * ZONES RETOURNEES PAR MHE96                                              
      *                                                                         
      *ZONES DE HEGSB                                                           
           02      COMM-HE96-ENVDEST.                                           
              05     COMM-HE96-NSOCDEST     PIC X(3).                           
              05     COMM-HE96-NLIEUDEST    PIC X(3).                           
              05     COMM-HE96-NSSLIEUDEST  PIC X(3).                           
              05     COMM-HE96-CLIEUTRTDEST PIC X(5).                           
      * REPONSE  OK = 0   SINON = 1 : ERREUR NON BLOQUANTE                      
      *                           2 : ERREUR     BLOQUANTE                      
           02      COMM-HE96-CRETOUR       PIC 9.                               
           02      COMM-HE96-MESS-ERR      PIC X(78).                           
                                                                                
