      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC100  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : INTER  DONT L'ADRESSE EST A SOUMETTRE AU    *         
      *                   TRAITEMENT                                  *         
      *                                                               *         
      *    FBC100       : FICHIER DES ADRESSES                        *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  377 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC100-ENREG.                                                        
           03  FBC100-CLE.                                                      
               05  FBC100-NSOCIETE            PIC  X(03).                       
               05  FBC100-NLIEU               PIC  X(03).                       
               05  FBC100-NVENTE              PIC  X(08).                       
           03  FBC100-CTITRENOM               PIC  X(05).                       
           03  FBC100-LNOM                    PIC  X(25).                       
           03  FBC100-LPRENOM                 PIC  X(15).                       
           03  FBC100-LIGNE-2.                                                  
               05  FBC100-FILLER-L2-1         PIC  X(04).                       
               05  FBC100-LESCALIER           PIC  X(03).                       
               05  FBC100-FILLER-L2-2         PIC  X(05).                       
               05  FBC100-LETAGE              PIC  X(03).                       
               05  FBC100-FILLER-L2-3         PIC  X(05).                       
               05  FBC100-LPORTE              PIC  X(03).                       
               05  FBC100-FILLER-L2-4         PIC  X(15).                       
           03  FBC100-LIGNE-3.                                                  
               05  FBC100-FILLER-L3-1         PIC  X(04).                       
               05  FBC100-LBATIMENT           PIC  X(03).                       
               05  FBC100-FILLER-L3-2         PIC  X(01).                       
               05  FBC100-LCMPAD1             PIC  X(30).                       
           03  FBC100-LIGNE-4.                                                  
               05  FBC100-CVOIE               PIC  X(06).                       
               05  FBC100-CTVOIE              PIC  X(05).                       
               05  FBC100-LNOMVOIE            PIC  X(21).                       
               05  FBC100-FILLER-L4           PIC  X(06).                       
           03  FBC100-LIGNE-5.                                                  
               05  FBC100-LCOMMUNE            PIC  X(32).                       
               05  FBC100-FILLER-L5           PIC  X(06).                       
           03  FBC100-LIGNE-6.                                                  
               05  FBC100-CPOSTAL             PIC  X(05).                       
               05  FBC100-CPOSTALF            PIC  X(01).                       
               05  FBC100-LBUREAU             PIC  X(32).                       
           03  FBC100-WTYPEADR                PIC  X(01).                       
           03  FBC100-DVENTE                  PIC  X(08).                       
           03  FBC100-TELDOM                  PIC  X(15).                       
           03  FBC100-TELBUR                  PIC  X(15).                       
           03  FBC100-LPOSTEBUR               PIC  X(05).                       
           03  FBC100-DVERSION                PIC  X(08).                       
           03  FBC100-IDCLIENT                PIC  X(08).                       
           03  FBC100-CPAYS                   PIC  X(03).                       
           03  FBC100-NGSM                    PIC  X(15).                       
           03  FBC100-TILOT                   PIC  X(01).                       
           03  FBC100-CILOT                   PIC  X(08).                       
           03  FBC100-NSOCSAV                 PIC  X(03).                       
           03  FBC100-NLIEUSAV                PIC  X(03).                       
           03  FBC100-PRFDOS                  PIC  X(03).                       
           03  FBC100-NUDOS                   PIC  X(09).                       
           03  FBC100-NCARTE                  PIC  X(09).                       
           03  FBC100-WFLAG                   PIC  X(01).                       
           03  FBC100-CINSEE                  PIC  X(05).                       
           03  FBC100-ANCLIENT                PIC  X(08).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
