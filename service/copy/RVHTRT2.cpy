      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HTRT2 HISTO MAJ BATCH                  *        
      *----------------------------------------------------------------*        
       01  RVHTRT2.                                                             
           05  HTRT2-CTABLEG2    PIC X(15).                                     
           05  HTRT2-CTABLEG2-REDEF REDEFINES HTRT2-CTABLEG2.                   
               10  HTRT2-CAPPLI          PIC X(05).                             
               10  HTRT2-CTRANSF         PIC X(05).                             
           05  HTRT2-WTABLEG     PIC X(80).                                     
           05  HTRT2-WTABLEG-REDEF  REDEFINES HTRT2-WTABLEG.                    
               10  HTRT2-LTRANSF         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVHTRT2-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HTRT2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HTRT2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HTRT2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HTRT2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
