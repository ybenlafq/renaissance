      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVRB1100                      *        
      ******************************************************************        
       01  RVRB1100.                                                            
           10 RB11-NIDENT               PIC X(10).                              
           10 RB11-NSOCIETE             PIC X(3).                               
           10 RB11-NLIEU                PIC X(3).                               
           10 RB11-CORRELID             PIC X(24).                              
           10 RB11-CFONCTION            PIC X(03).                              
           10 RB11-DSYST                PIC S9(13)V USAGE COMP-3.               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVRB1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-NIDENT-F      PIC S9(4) COMP.                                
      *--                                                                       
           10 RB11-NIDENT-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-NSOCIETE-F    PIC S9(4) COMP.                                
      *--                                                                       
           10 RB11-NSOCIETE-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-NLIEU-F       PIC S9(4) COMP.                                
      *--                                                                       
           10 RB11-NLIEU-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-CORRELID-F    PIC S9(4) COMP.                                
      *--                                                                       
           10 RB11-CORRELID-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-CFONCTION-F   PIC S9(4) COMP.                                
      *--                                                                       
           10 RB11-CFONCTION-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB11-DSYST-F       PIC S9(4) COMP.                                
      *                                                                         
      *--                                                                       
           10 RB11-DSYST-F       PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
