      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFX0500                                             
      *********************************************************                 
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX0500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX0500.                                                            
           02  FX05-NJRN                                                        
               PIC X(0010).                                                     
           02  FX05-NEXERCICE                                                   
               PIC X(0004).                                                     
           02  FX05-NPERIODE                                                    
               PIC X(0002).                                                     
           02  FX05-NPIECE                                                      
               PIC X(0010).                                                     
           02  FX05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFX0500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX05-NJRN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX05-NJRN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX05-NEXERCICE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX05-NEXERCICE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX05-NPERIODE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX05-NPERIODE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX05-NPIECE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX05-NPIECE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
