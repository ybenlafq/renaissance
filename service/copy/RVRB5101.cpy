      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   TABLE RTRB51                                                          
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVRB5100                           
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRB5101.                                                            
           10  RB51-NUMCONT                                                     
               PIC X(0015).                                                     
           10  RB51-NTYPEFAS                                                    
               PIC X(0004).                                                     
           10  RB51-NUMIMMO                                                     
               PIC X(0012).                                                     
           10  RB51-MONTANT                                                     
               PIC S9(7)V9(2) USAGE COMP-3.                                     
           10  RB51-DATEFF                                                      
               PIC X(8).                                                        
           10  RB51-DRESIL                                                      
               PIC X(8).                                                        
           10  RB51-WCEDE                                                       
               PIC X(1).                                                        
           10  RB51-DSYST                                                       
               PIC S9(11)V9(2) USAGE COMP-3.                                    
       01  RVRB5101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-NUMCONT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-NUMCONT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-NTYPEFAS-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-NTYPEFAS-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-NUMIMMO-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-NUMIMMO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-MONTANT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-MONTANT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-DATEFF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-DATEFF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-DRESIL-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-DRESIL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-WCEDE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB51-WCEDE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB51-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 RB51-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
