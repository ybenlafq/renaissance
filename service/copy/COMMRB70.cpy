      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-RB70-LONG-COMMAREA             PIC S9(4) COMP           00000020
      *                                                  VALUE +4096.   00000020
      *--                                                                       
       01  COMM-RB70-LONG-COMMAREA             PIC S9(4) COMP-5                 
                                                         VALUE +4096.           
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
      *    ZONES RESERVEES A AIDA -------------------------------- 100  00000060
           05  FILLER-COM-AIDA                 PIC X(100).              00000070
                                                                        00000080
      *    ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020  00000090
           05  COMM-CICS-APPLID                PIC X(008).              00000100
           05  COMM-CICS-NETNAM                PIC X(008).              00000110
           05  COMM-CICS-TRANSA                PIC X(004).              00000120
                                                                        00000130
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00000140
           05  COMM-DATE-SIECLE                PIC X(002).              00000150
           05  COMM-DATE-ANNEE                 PIC X(002).              00000160
           05  COMM-DATE-MOIS                  PIC X(002).              00000170
           05  COMM-DATE-JOUR                  PIC 9(002).              00000180
                                                                        00000130
      *    QUANTIEMES CALENDAIRE ET STANDARD                            00000190
           05  COMM-DATE-QNTA                  PIC 9(03).               00000200
           05  COMM-DATE-QNT0                  PIC 9(05).               00000210
                                                                        00000130
      *    ANNEE BISSEXTILE 1=OUI 0=NON                                 00000220
           05  COMM-DATE-BISX                  PIC 9(01).               00000230
                                                                        00000130
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           05  COMM-DATE-JSM                   PIC 9(01).               00000250
                                                                        00000130
      *    LIBELLES DU JOUR COURT - LONG                                00000260
           05  COMM-DATE-JSM-LC                PIC X(03).               00000270
           05  COMM-DATE-JSM-LL                PIC X(08).               00000280
                                                                        00000130
      *    LIBELLES DU MOIS COURT - LONG                                00000290
           05  COMM-DATE-MOIS-LC               PIC X(03).               00000300
           05  COMM-DATE-MOIS-LL               PIC X(08).               00000310
                                                                        00000130
      *    DIFFERENTES FORMES DE DATE                                   00000320
           05  COMM-DATE-SSAAMMJJ              PIC X(08).               00000330
           05  COMM-DATE-AAMMJJ                PIC X(06).               00000340
           05  COMM-DATE-JJMMSSAA              PIC X(08).               00000350
           05  COMM-DATE-JJMMAA                PIC X(06).               00000360
           05  COMM-DATE-JJ-MM-AA              PIC X(08).               00000370
           05  COMM-DATE-JJ-MM-SSAA            PIC X(10).               00000380
                                                                        00000130
      *    TRAITEMENT DU NUMERO DE SEMAINE                              00000390
           05  COMM-DATE-WEEK.                                          00000400
               10  COMM-DATE-SEMSS             PIC 9(02).               00000410
               10  COMM-DATE-SEMAA             PIC 9(02).               00000420
               10  COMM-DATE-SEMNU             PIC 9(02).               00000430
           05  COMM-DATE-FILLER                PIC X(08).               00000440
                                                                        00000130
      *    ZONES RESERVEES TRAITEMENT DU SWAP                           00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-SWAP-CURS                  PIC S9(4) COMP VALUE -1. 00000460
      *                                                                         
      *--                                                                       
           05  COMM-SWAP-CURS                  PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
           05  COMM-RB70-ENTETE.                                        00000500
               10  COMM-ENTETE.                                         00000510
                   15  COMM-CODLANG            PIC X(02).               00000520
                   15  COMM-CODPIC             PIC X(02).               00000530
                   15  COMM-LEVEL-SUP          PIC X(05).               00000540
                   15  COMM-LEVEL-MAX          PIC X(05).               00000540
                   15  COMM-ACID               PIC X(08).                       
               10  COMM-MES-ERREUR.                                             
                   15  COMM-COD-ERREUR         PIC X(01).                       
                   15  COMM-LIB-MESSAG         PIC X(59).                       
               10  COMM-DAT-DEBUT              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-DEBUT.              
                   15  COMM-DAT-DEBJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBSA          PIC X(04).                       
               10  COMM-DAT-FINAL              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-FINAL.              
                   15  COMM-DAT-FINJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINSA          PIC X(04).                       
                                                                        00000130
      *--- MENU CONTROLE ENCODAGE FOURNISSEUR - TRB70                   00000390
           05  COMM-RB70-APPLI.                                         00000500
               10  COMM-RB70-NSOCORIG          PIC X(03).               00000520
               10  COMM-RB70-DATDEB            PIC X(10).               00000520
               10  FILLER                REDEFINES COMM-RB70-DATDEB.            
                   15  COMM-RB70-DEBJOUR       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-RB70-DEBMOIS       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-RB70-DEBSSAA       PIC X(04).                       
               10  COMM-RB70-DATFIN            PIC X(10).               00000520
               10  FILLER                REDEFINES COMM-RB70-DATFIN.            
                   15  COMM-RB70-FINJOUR       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-RB70-FINMOIS       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-RB70-FINSSAA       PIC X(04).                       
               10  COMM-RB70-CTIERS            PIC X(05).               00000520
               10  COMM-RB70-NENVOI            PIC X(07).               00000520
               10  COMM-RB70-CTRAIT            PIC X(05).               00000620
               10  COMM-RB70-WOK               PIC X(01).               00000620
               10  COMM-RB70-WECART            PIC X(01).               00000620
               10  COMM-RB70-WNUMSER           PIC X(01).               00000620
               10  COMM-RB70-WCORREC           PIC X(01).               00000620
               10  COMM-RB70-WPRODPL           PIC X(01).               00000620
               10  COMM-RB70-NUMSER            PIC X(16).               00000620
               10  COMM-RB70-NCODIC            PIC X(07).               00000620
      *--- LISTE DES RECEPTIONS - TRB71                                 00000390
           05  COMM-RB71-APPLI.                                         00000040
               10  COMM-RB71.                                           00000050
                   15  COMM-RB71-CPTEURS.                                     00
                       20  COMM-RB71-INDTS     PIC 9(03).                     00
                       20  COMM-RB71-INDMAX    PIC 9(03).                     00
                       20  COMM-RB71-NLIGNE    PIC 9(03).                     00
                   15  COMM-RB71-LIGNE.                                    00000
                       20  COMM-RB71-NENVOI    PIC X(07).                     00
                       20  COMM-RB71-NSOCORIG  PIC X(03).                     00
                       20  COMM-RB71-CTRAIT    PIC X(05).                     00
                       20  COMM-RB71-DATENV    PIC X(10).                     00
                       20  FILLER        REDEFINES COMM-RB71-DATENV.            
                           25 COMM-RB71-ENVJOUR PIC X(02).                      
                           25 FILLER            PIC X(01).                      
                           25 COMM-RB71-ENVMOIS PIC X(02).                      
                           25 FILLER            PIC X(01).                      
                           25 COMM-RB71-ENVSSAA PIC X(04).                      
                       20  COMM-RB71-DATREC    PIC X(10).                     00
                       20  FILLER        REDEFINES COMM-RB71-DATREC.            
                           25 COMM-RB71-RECJOUR PIC X(02).                      
                           25 FILLER            PIC X(01).                      
                           25 COMM-RB71-RECMOIS PIC X(02).                      
                           25 FILLER            PIC X(01).                      
                           25 COMM-RB71-RECSSAA PIC X(04).                      
                       20  COMM-RB71-QTENV     PIC S9(5).                     00
                       20  COMM-RB71-QTREC     PIC S9(5).                     00
                       20  COMM-RB71-STATUT    PIC X(03).                     00
      *--- DETAIL D'UNE RECEPTION - TRB72                               00000390
           05  COMM-RB72-APPLI.                                         00000040
               10  COMM-RB72.                                           00000050
                   15  COMM-RB72-CPTEURS.                                     00
                       20  COMM-RB72-INDTS     PIC 9(03).                     00
                       20  COMM-RB72-INDMAX    PIC 9(03).                     00
                       20  COMM-RB72-NLIGNE    PIC 9(03).                     00
                   15  COMM-RB72-LIGNE.                                    00000
                       20  COMM-RB72-NSEREXP   PIC X(16).                     00
                       20  COMM-RB72-NSERREC   PIC X(16).                     00
                       20  COMM-RB72-NUMHS     PIC X(17).                     00
                       20  COMM-RB72-WLREGUL   PIC X(30).                     00
                       20  COMM-RB72-WSTATUT   PIC X(01).                     00
                   15  COMM-RB72-LNSER         PIC X(28).                  00000
           05  COMM-RB70-FILLER                PIC X(3530).             00000620
      *---                                                              00000390
                                                                                
