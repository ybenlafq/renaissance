      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM170 AU 26/08/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,01,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,28,BI,A,                          *        
      *                           53,08,BI,A,                          *        
      *                           61,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM170.                                                        
            05 NOMETAT-INM170           PIC X(6) VALUE 'INM170'.                
            05 RUPTURES-INM170.                                                 
           10 INM170-DATETR             PIC X(08).                      007  008
           10 INM170-FLAG-DEV           PIC X(01).                      015  001
           10 INM170-NSOC               PIC X(03).                      016  003
           10 INM170-NLIEU              PIC X(03).                      019  003
           10 INM170-NCAISS             PIC X(03).                      022  003
           10 INM170-LIBMOD-DEV         PIC X(28).                      025  028
           10 INM170-NTRANS             PIC X(08).                      053  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM170-SEQUENCE           PIC S9(04) COMP.                061  002
      *--                                                                       
           10 INM170-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM170.                                                   
           10 INM170-CDEVAUT            PIC X(05).                      063  005
           10 INM170-CDEVREF            PIC X(05).                      068  005
           10 INM170-CODOPE             PIC X(07).                      073  007
           10 INM170-LDEVREF            PIC X(25).                      080  025
           10 INM170-MONT-AUT           PIC S9(07)V9(2) COMP-3.         105  005
           10 INM170-MONT-AUT-CACH      PIC S9(07)V9(2) COMP-3.         110  005
           10 INM170-MONT-RF            PIC S9(07)V9(2) COMP-3.         115  005
           10 INM170-MONT-RF-CACH       PIC S9(07)V9(2) COMP-3.         120  005
            05 FILLER                      PIC X(388).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM170-LONG           PIC S9(4)   COMP  VALUE +124.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM170-LONG           PIC S9(4) COMP-5  VALUE +124.           
                                                                                
      *}                                                                        
