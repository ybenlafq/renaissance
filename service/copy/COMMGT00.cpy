      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES TABLES GENERALISEES D'UN UTILISATEUR *        
      *  TRANSACTION: GT00                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION GT00                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                        -----   *        
      * COM-GT00-LONG-COMMAREA A UNE VALUE DE                   4096 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GT00-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-GT00-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(08).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS           PIC 9(02).                           
              05 COMM-DATE-SEMAA           PIC 9(02).                           
              05 COMM-DATE-SEMNU           PIC 9(02).                           
           02 COMM-DATE-FILLER          PIC X(08).                              
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      ******************************************************************        
      *--- ZONES RESERVEES APPLICATIVES DE LA TRANSACTION GT00 - 3724 C*        
      ******************************************************************        
      *                                                                         
           02 COMM-GA00-APPLI.                                                  
      *------ ZONES RESERVEES A LA COMMAREA DE TGA01 ------------ 1000          
              05 COMM-GT00-TGA01        PIC X(1000).                            
      *------ ZONES APPLICATIVES TGT00 -------------------------- 2724          
              05 COMM-GT00-APPLI.                                               
                 10 COMM-GT00-CACID        PIC X(08).                           
      *------------ PAGINATION ---------------------------------------          
                 10 COMM-GT00-PAGINATION.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             15 COMM-GT00-MAX-PAGE     PIC 9(04) COMP.                   
      *--                                                                       
                    15 COMM-GT00-MAX-PAGE     PIC 9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             15 COMM-GT00-NO-PAGE      PIC 9(04) COMP.                   
      *--                                                                       
                    15 COMM-GT00-NO-PAGE      PIC 9(04) COMP-5.                 
      *}                                                                        
                 10 COMM-GT00-PF6          PIC X(01).                           
                 10 COMM-GT00-NSOCIETE     PIC X(03).                           
                 10 COMM-GT00-NSOCGROUPE   PIC X(03).                           
                 10 COMM-GT00-FILLER       PIC X(2705).                         
      *                                                                         
                                                                                
