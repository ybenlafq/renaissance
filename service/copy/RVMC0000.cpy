      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVMC0000                      *        
      ******************************************************************        
       01  RVMC0000.                                                            
      *                       NSOCIETE                                          
           10 MC00-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 MC00-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 MC00-NVENTE          PIC X(7).                                    
      *                       NSEQNQ                                            
           10 MC00-NSEQNQ          PIC X(5).                                    
      *                       DATENC                                            
           10 MC00-DATENC          PIC X(8).                                    
      *                       IDCLIF                                            
           10 MC00-IDCLIF          PIC X(8).                                    
      *                       IDCLIL                                            
           10 MC00-IDCLIL          PIC X(8).                                    
      *                       CTITRENOM                                         
           10 MC00-CTITRENOM       PIC X(5).                                    
      *                       NOM                                               
           10 MC00-NOM             PIC X(25).                                   
      *                       PRENOM                                            
           10 MC00-PRENOM          PIC X(15).                                   
      *                       ADRESSE1                                          
           10 MC00-ADRESSE1        PIC X(50).                                   
      *                       ADRESSE2                                          
           10 MC00-ADRESSE2        PIC X(32).                                   
      *                       LBATIMENT                                         
           10 MC00-LBATIMENT       PIC X(3).                                    
      *                       LESCALIER                                         
           10 MC00-LESCALIER       PIC X(3).                                    
      *                       LETAGE                                            
           10 MC00-LETAGE          PIC X(3).                                    
      *                       LPORTE                                            
           10 MC00-LPORTE          PIC X(3).                                    
      *                       CPOSTAL                                           
           10 MC00-CPOSTAL         PIC X(5).                                    
      *                       CINSEE                                            
           10 MC00-CINSEE          PIC X(5).                                    
      *                       LCOMMUNE                                          
           10 MC00-LCOMMUNE        PIC X(32).                                   
      *                       TELDOM                                            
           10 MC00-TELDOM          PIC X(15).                                   
      *                       TELBUR                                            
           10 MC00-TELBUR          PIC X(15).                                   
      *                       NGSM                                              
           10 MC00-NGSM            PIC X(15).                                   
      *                       ADR_MAIL                                          
           10 MC00-ADR-MAIL        PIC X(50).                                   
      *                       RECH_SIEBEL                                       
           10 MC00-RECH-SIEBEL     PIC X(1).                                    
      *                       NCODIC                                            
           10 MC00-NCODIC          PIC X(7).                                    
      *                       CFAM                                              
           10 MC00-CFAM            PIC X(5).                                    
      *                       LFAM                                              
           10 MC00-LFAM            PIC X(20).                                   
      *                       CMARQ                                             
           10 MC00-CMARQ           PIC X(5).                                    
      *                       LMARQ                                             
           10 MC00-LMARQ           PIC X(20).                                   
      *                       LREF                                              
           10 MC00-LREF            PIC X(20).                                   
      *                       QVENDUE                                           
           10 MC00-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
      *                       CMODDEL                                           
           10 MC00-CMODDEL         PIC X(3).                                    
      *                       DDELIV                                            
           10 MC00-DDELIV          PIC X(8).                                    
      *                       DUREE                                             
           10 MC00-DUREE           PIC X(8).                                    
      *                       CPSE                                              
           10 MC00-CPSE            PIC X(5).                                    
      *                       PPSE                                              
           10 MC00-PPSE            PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVNET                                             
           10 MC00-PVNET           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DEXTRACT                                          
           10 MC00-DEXTRACT        PIC X(8).                                    
      *                       NVENTEA                                           
           10 MC00-NVENTEA         PIC X(18).                                   
      *                       DSYST                                             
           10 MC00-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 39      *        
      ******************************************************************        
                                                                                
