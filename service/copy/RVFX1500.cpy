      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVFX1500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX1500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVFX1500.                                                    00000090
           02  FX15-WSEQPRO                                             00000100
               PIC S9(7) COMP-3.                                        00000110
           02  FX15-CFAMGL                                              00000120
               PIC X(0003).                                             00000130
           02  FX15-NDEPTLMELA                                          00000140
               PIC X(0001).                                             00000150
      *                                                                 00000160
      *---------------------------------------------------------        00000170
      *   LISTE DES FLAGS DE LA TABLE RVFX1500                          00000180
      *---------------------------------------------------------        00000190
      *                                                                 00000200
       01  RVFX1500-FLAGS.                                              00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX15-WSEQPRO-F                                           00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  FX15-WSEQPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX15-CFAMGL-F                                            00000240
      *        PIC S9(4) COMP.                                          00000250
      *--                                                                       
           02  FX15-CFAMGL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX15-NDEPTLMELA-F                                        00000260
      *        PIC S9(4) COMP.                                          00000270
      *--                                                                       
           02  FX15-NDEPTLMELA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000280
                                                                                
