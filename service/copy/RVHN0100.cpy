      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RTHN01)                                    *        
      *        LIBRARY(DSA043.DEVL.SOURCE(RVHN0100))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(HN01-)                                            *        
      *        STRUCTURE(RVHN0100)                                     *        
      *        APOST                                                   *        
      *        LABEL(YES)                                              *        
      *        DBCSDELIM(NO)                                           *        
      *        COLSUFFIX(YES)                                          *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RTHN01 TABLE                                 
           ( CTIERS                         CHAR(5) NOT NULL,                   
             CMARQ                          CHAR(5) NOT NULL,                   
             CFAM                           CHAR(5) NOT NULL,                   
             WTRI                           CHAR(1) NOT NULL,                   
             DSYST                          DECIMAL(13, 0) NOT NULL,            
             CRENDU                         CHAR(5) NOT NULL,                   
             NCODIC                         CHAR(7) NOT NULL,                   
             WNACCORD                       CHAR(1) NOT NULL,                   
             WNSERIE                        CHAR(1) NOT NULL                    
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTHN01                      *        
      ******************************************************************        
       01  RVHN0100.                                                            
      *    *************************************************************        
      *                       CTIERS                                            
           10 HN01-CTIERS          PIC X(5).                                    
      *    *************************************************************        
      *                       CMARQ                                             
           10 HN01-CMARQ           PIC X(5).                                    
      *    *************************************************************        
      *                       CFAM                                              
           10 HN01-CFAM            PIC X(5).                                    
      *    *************************************************************        
      *                       WTRI                                              
           10 HN01-WTRI            PIC X(1).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 HN01-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       CRENDU                                            
           10 HN01-CRENDU          PIC X(5).                                    
      *    *************************************************************        
      *                       NCODIC                                            
           10 HN01-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       WNACCORD                                          
           10 HN01-WNACCORD        PIC X(1).                                    
      *    *************************************************************        
      *                       WNSERIE                                           
           10 HN01-WNSERIE         PIC X(1).                                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTHN01.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 9 TIMES.           
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 9 TIMES.               
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
