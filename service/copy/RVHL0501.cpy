      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHL0501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHL0501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL0501.                                                            
           02  HL05-CTABLE                                                      
               PIC X(0018).                                                     
           02  HL05-CCHAMPR                                                     
               PIC X(0018).                                                     
           02  HL05-CCHAMPL                                                     
               PIC X(0018).                                                     
           02  HL05-NREQUETE                                                    
               PIC S9(5) COMP-3.                                                
           02  HL05-CAPPLI                                                      
               PIC X(0002).                                                     
           02  HL05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHL0501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL0501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-CTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-CTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-CCHAMPR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-CCHAMPR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-CCHAMPL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-CCHAMPL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-NREQUETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-NREQUETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-CAPPLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-CAPPLI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
