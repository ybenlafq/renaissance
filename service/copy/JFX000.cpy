      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JFX000 AU 03/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JFX000.                                                        
            05 NOMETAT-JFX000           PIC X(6) VALUE 'JFX000'.                
            05 RUPTURES-JFX000.                                                 
           10 JFX000-NOECS              PIC X(05).                      007  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JFX000-SEQUENCE           PIC S9(04) COMP.                012  002
      *--                                                                       
           10 JFX000-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JFX000.                                                   
           10 JFX000-COMPTE             PIC X(06).                      014  006
           10 JFX000-CRIT1              PIC X(05).                      020  005
           10 JFX000-CRIT2              PIC X(05).                      025  005
           10 JFX000-CRIT3              PIC X(05).                      030  005
           10 JFX000-LCRIT1             PIC X(20).                      035  020
           10 JFX000-LCRIT2             PIC X(20).                      055  020
           10 JFX000-LCRIT3             PIC X(20).                      075  020
           10 JFX000-LECS               PIC X(20).                      095  020
           10 JFX000-NCRIT1             PIC X(05).                      115  005
           10 JFX000-NCRIT2             PIC X(05).                      120  005
           10 JFX000-NCRIT3             PIC X(05).                      125  005
           10 JFX000-RUBRIQUE           PIC X(06).                      130  006
           10 JFX000-SECTION            PIC X(06).                      136  006
           10 JFX000-SENS               PIC X(01).                      142  001
           10 JFX000-WFX00              PIC X(06).                      143  006
           10 JFX000-DATEFF             PIC X(08).                      149  008
           10 JFX000-DEFFET             PIC X(08).                      157  008
            05 FILLER                      PIC X(348).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JFX000-LONG           PIC S9(4)   COMP  VALUE +164.           
      *                                                                         
      *--                                                                       
        01  DSECT-JFX000-LONG           PIC S9(4) COMP-5  VALUE +164.           
                                                                                
      *}                                                                        
