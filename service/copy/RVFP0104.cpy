      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFP0104                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0104.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0104.                                                            
      *}                                                                        
      *                       NSOCCOMPT                                         
           10 FP01-NSOCCOMPT       PIC X(3).                                    
      *                       NSOC                                              
           10 FP01-NSOC            PIC X(3).                                    
      *                       NLIEU                                             
           10 FP01-NLIEU           PIC X(3).                                    
      *                       CASSUR                                            
           10 FP01-CASSUR          PIC X(5).                                    
      *                       NFACTURE                                          
           10 FP01-NFACTURE        PIC X(10).                                   
      *                       DFACTURE                                          
           10 FP01-DFACTURE        PIC X(8).                                    
      *                       NSINISTRE                                         
           10 FP01-NSINISTRE       PIC X(20).                                   
      *                       DSINISTRE                                         
           10 FP01-DSINISTRE       PIC X(8).                                    
      *                       NOM                                               
           10 FP01-NOM             PIC X(25).                                   
      *                       PRENOM                                            
           10 FP01-PRENOM          PIC X(25).                                   
      *                       CPOSTAL                                           
           10 FP01-CPOSTAL         PIC X(5).                                    
      *                       REFDOC                                            
           10 FP01-REFDOC          PIC X(10).                                   
      *                       CTYPINTER                                         
           10 FP01-CTYPINTER       PIC X(3).                                    
      *                       CLIEUINTER                                        
           10 FP01-CLIEUINTER      PIC X(3).                                    
      *                       MTFRANCHISE                                       
           10 FP01-MTFRANCHISE     PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       WFRANCHISE                                        
           10 FP01-WFRANCHISE      PIC X(1).                                    
      *                       TXREMISE                                          
           10 FP01-TXREMISE        PIC S9(3)V9(2) USAGE COMP-3.                 
      *                       MTLIVR                                            
           10 FP01-MTLIVR          PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       MTFORFREP                                         
           10 FP01-MTFORFREP       PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       NDOSSAV                                           
           10 FP01-NDOSSAV         PIC X(10).                                   
      *                       DANNUL                                            
           10 FP01-DANNUL          PIC X(8).                                    
      *                       DSELECT                                           
           10 FP01-DSELECT         PIC X(8).                                    
      *                       NPRESENT                                          
           10 FP01-NPRESENT        PIC X(2).                                    
      *                       NLOT                                              
           10 FP01-NLOT            PIC X(6).                                    
      *                       DVALID                                            
           10 FP01-DVALID          PIC X(8).                                    
      *                       WREJET                                            
           10 FP01-WREJET          PIC X(1).                                    
      *                       DREJET                                            
           10 FP01-DREJET          PIC X(8).                                    
      *                       CREJET                                            
           10 FP01-CREJET          PIC X(3).                                    
      *                       LREJET                                            
           10 FP01-LREJET          PIC X(25).                                   
      *                       DCREAT                                            
           10 FP01-DCREAT          PIC X(8).                                    
      *                       DNETTING                                          
           10 FP01-DNETTING        PIC X(8).                                    
      *                       ORIGINE                                           
           10 FP01-ORIGINE         PIC X(8).                                    
      *                       CDEVISE                                           
           10 FP01-CDEVISE         PIC X(3).                                    
      *                       DMAJ                                              
           10 FP01-DMAJ            PIC X(8).                                    
      *                       DSYST                                             
           10 FP01-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       MTFORFEXP                                         
           10 FP01-MTFORFEXP       PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       WDERFACT                                          
           10 FP01-WDERFACT        PIC X(1).                                    
      *                       WCARTE                                            
           10 FP01-WCARTE          PIC X(1).                                    
      *                       NFACT_PAC                                         
           10 FP01-NFACT-PAC       PIC X(12).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 39      *        
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES FLAGS                                                       
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0104-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0104-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CASSUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CASSUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSINISTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSINISTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSINISTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSINISTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NOM-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NOM-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-PRENOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-PRENOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-REFDOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-REFDOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CTYPINTER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CTYPINTER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CLIEUINTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CLIEUINTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFRANCHISE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFRANCHISE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WFRANCHISE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WFRANCHISE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-TXREMISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-TXREMISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTLIVR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTLIVR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFORFREP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFORFREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NDOSSAV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NDOSSAV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSELECT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSELECT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NPRESENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NPRESENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NLOT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NLOT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DVALID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-LREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-LREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DNETTING-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DNETTING-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFORFEXP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFORFEXP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WDERFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WDERFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NFACT-PAC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NFACT-PAC-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
