      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC9900                           *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVBC9900.                                                            
      *    *************************************************************        
      *                        CLIENT CIBLE                                     
           10 BC99-NCLIENTADR      PIC X(8).                                    
      *                        CLIENT LIE OU REGROUPE SI FUSION                 
           10 BC99-NCLIENTLIE      PIC X(8).                                    
      *                       DSYST                                             
           10 BC99-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *                       TRAITE OUI NON 0/1                                
           10 BC99-WTRAITE         PIC X.                                       
      *                       FONCTION ' ' NORMALE 'F' FUSION                   
           10 BC99-WFONCTION       PIC X.                                       
      *                       DSYST DU TRAITEMENT                               
           10 BC99-DSYSTTRT        PIC S9(13)  USAGE COMP-3.                    
      *                       NOM/PRENOM                                        
           10 BC99-LNOM            PIC X(25).                                   
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC9900-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-NCLIENTLIE-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-NCLIENTLIE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-WTRAITE-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-WTRAITE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-WFONCTION-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-WFONCTION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-DSYSTTRT-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-DSYSTTRT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC99-LNOM-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC99-LNOM-F          PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
