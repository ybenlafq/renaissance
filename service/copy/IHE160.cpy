      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE160 AU 16/03/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,20,BI,A,                          *        
      *                           37,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE160.                                                        
            05 NOMETAT-IHE160           PIC X(6) VALUE 'IHE160'.                
            05 RUPTURES-IHE160.                                                 
           10 IHE160-CTRAIT             PIC X(05).                      007  005
           10 IHE160-CRENDU             PIC X(05).                      012  005
           10 IHE160-NRENDU             PIC X(20).                      017  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE160-SEQUENCE           PIC S9(04) COMP.                037  002
      *--                                                                       
           10 IHE160-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE160.                                                   
           10 IHE160-ANNEE              PIC X(04).                      039  004
           10 IHE160-CDEVEQU            PIC X(03).                      043  003
           10 IHE160-CDEVREF            PIC X(03).                      046  003
           10 IHE160-MOIS               PIC X(02).                      049  002
           10 IHE160-NLIEUORIG          PIC X(03).                      051  003
           10 IHE160-NORIGINE           PIC X(07).                      054  007
           10 IHE160-NSOCORIG           PIC X(03).                      061  003
           10 IHE160-MTAVO              PIC 9(08)V9(2).                 064  010
           10 IHE160-QTRENDU            PIC 9(03)     .                 074  003
           10 IHE160-TAUX-DEV           PIC S9(01)V9(6) COMP-3.         077  004
            05 FILLER                      PIC X(432).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE160-LONG           PIC S9(4)   COMP  VALUE +080.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE160-LONG           PIC S9(4) COMP-5  VALUE +080.           
                                                                                
      *}                                                                        
