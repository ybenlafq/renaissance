      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC0300                           *        
      ******************************************************************        
       01  RVBC0300.                                                            
      *    *************************************************************        
      *                       NCLIENTADR                                        
           10 BC03-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       CTYPMAIL                                          
           10 BC03-CTYPMAIL        PIC X(1).                                    
      *    *************************************************************        
      *                       EMAIL                                             
           10 BC03-EMAIL           PIC X(63).                                   
      *    *************************************************************        
      *                       DSYST                                             
           10 BC03-DSYST           PIC S9(13)  USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC0300-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC03-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC03-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC03-CTYPMAIL-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC03-CTYPMAIL-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC03-EMAIL-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC03-EMAIL-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC03-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC03-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
