      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1300.                                                            
           02  AP13-NSEQID         PIC S9(18) COMP-3.                           
           02  AP13-NENCAISSE      PIC X(0014).                                 
           02  AP13-NCRI           PIC X(0017).                                 
           02  AP13-CMOPAI         PIC X(0005).                                 
           02  AP13-PMONTANT       PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP13-WISCESU        PIC X(0001).                                 
           02  AP13-DATETRAIT      PIC X(0008).                                 
           02  AP13-NIDCRI         PIC S9(18) COMP-3.                           
           02  AP13-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-NCRI-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-NCRI-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-CMOPAI-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-CMOPAI-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-PMONTANT-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-PMONTANT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-WISCESU-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-WISCESU-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-DATETRAIT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-DATETRAIT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-NIDCRI-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP13-NIDCRI-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP13-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP13-DSYST-F        PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
