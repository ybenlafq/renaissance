      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISPIN4 AU 21/03/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISPIN4.                                                        
            05 NOMETAT-ISPIN4           PIC X(6) VALUE 'ISPIN4'.                
            05 RUPTURES-ISPIN4.                                                 
           10 ISPIN4-NSOCIETE           PIC X(03).                      007  003
           10 ISPIN4-TEBRB              PIC X(02).                      010  002
           10 ISPIN4-CHEFPROD           PIC X(05).                      012  005
           10 ISPIN4-WSEQFAM            PIC S9(05)      COMP-3.         017  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISPIN4-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 ISPIN4-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISPIN4.                                                   
           10 ISPIN4-CAPPRO             PIC X(05).                      022  005
           10 ISPIN4-CASSORT            PIC X(02).                      027  002
           10 ISPIN4-CFAM               PIC X(05).                      029  005
           10 ISPIN4-CMARQ              PIC X(05).                      034  005
           10 ISPIN4-DEFFETEPD          PIC X(08).                      039  008
           10 ISPIN4-DEFFETPV           PIC X(08).                      047  008
           10 ISPIN4-DERO               PIC X(01).                      055  001
           10 ISPIN4-LREFFOURN          PIC X(20).                      056  020
           10 ISPIN4-NCODIC             PIC X(07).                      076  007
           10 ISPIN4-NSSLIEU            PIC X(01).                      083  001
           10 ISPIN4-NZONPRIX           PIC X(02).                      084  002
           10 ISPIN4-PSTDTTC            PIC S9(07)V9(2) COMP-3.         086  005
           10 ISPIN4-QSTOCKDEP          PIC S9(05)      COMP-3.         091  003
           10 ISPIN4-QSTOCKMAG          PIC S9(05)      COMP-3.         094  003
           10 ISPIN4-SRP                PIC S9(07)V9(2) COMP-3.         097  005
            05 FILLER                      PIC X(411).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISPIN4-LONG           PIC S9(4)   COMP  VALUE +101.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISPIN4-LONG           PIC S9(4) COMP-5  VALUE +101.           
                                                                                
      *}                                                                        
