      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPC0700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPC0700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0700.                                                            
           02  PC07-CPRESTA        PIC X(07).                                   
           02  PC07-LPRESTA        PIC X(20).                                   
           02  PC07-NCOMMANDE      PIC X(12).                                   
           02  PC07-PRIX-UNIT      PIC S9(06)V9(02) COMP-3.                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-QTE            PIC S9(05) COMP.                             
      *--                                                                       
           02  PC07-QTE            PIC S9(05) COMP-5.                           
      *}                                                                        
           02  PC07-MT-HT          PIC S9(06)V9(02) COMP-3.                     
           02  PC07-MT-TVA         PIC S9(06)V9(02) COMP-3.                     
           02  PC07-MT-TTC         PIC S9(06)V9(02) COMP-3.                     
           02  PC07-CTAUX-TVA      PIC X(01).                                   
           02  PC07-DSYST          PIC S9(13) COMP-3.                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA RVPC0700                                        
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-CPRESTA-F       PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-CPRESTA-F       PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-LPRESTA-F       PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-LPRESTA-F       PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-NCOMMANDE-F     PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-NCOMMANDE-F     PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-PRIX-UNIT-F     PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-PRIX-UNIT-F     PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-QTE-F           PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-QTE-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-MT-HT-F         PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-MT-HT-F         PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-MT-TVA-F        PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-MT-TVA-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-MT-TTC-F        PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-MT-TTC-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-CTAUX-TVA-F     PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-CTAUX-TVA-F     PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC07-DSYST-F         PIC S9(4) COMP.                             
      *--                                                                       
           02  PC07-DSYST-F         PIC S9(4) COMP-5.                           
      *}                                                                        
       EJECT                                                                    
                                                                                
