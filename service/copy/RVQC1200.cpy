      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVQC1200                      *        
      ******************************************************************        
       01  RVQC1200.                                                            
      *                       CCREATION                                         
           10 QC12-CCREATION       PIC X(1).                                    
      *                       CTQUEST                                           
           10 QC12-CTQUEST         PIC S9(2)V USAGE COMP-3.                     
      *                       NCARTE                                            
           10 QC12-NCARTE          PIC X(15).                                   
      *                       NSOCIETE                                          
           10 QC12-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 QC12-NLIEU           PIC X(3).                                    
      *                       EMAILENT                                          
           10 QC12-EMAILENT        PIC X(50).                                   
      *                       DENVOI                                            
           10 QC12-DENVOI          PIC X(8).                                    
      *                       DSAISIE                                           
           10 QC12-DSAISIE         PIC X(8).                                    
      *                       CVERBATIM_ATT                                     
           10 QC12-CVERBATIM-ATT   PIC X(1).                                    
      *                       SATGLOB                                           
           10 QC12-SATGLOB         PIC S9(3)V USAGE COMP-3.                     
      *                       CVERBATIM                                         
           10 QC12-CVERBATIM       PIC X(2).                                    
      *                       CVERBATIM2                                        
           10 QC12-CVERBATIM2      PIC X(2).                                    
      *                       CVERBATIM3                                        
           10 QC12-CVERBATIM3      PIC X(2).                                    
      *                       CVERBATIM4                                        
           10 QC12-CVERBATIM4      PIC X(2).                                    
      *                       CVERBATIM5                                        
           10 QC12-CVERBATIM5      PIC X(2).                                    
      *                       COMMENTAIRE1                                      
           10 QC12-COMMENTAIRE1    PIC X(250).                                  
      *                       COMMENTAIRE2                                      
           10 QC12-COMMENTAIRE2    PIC X(250).                                  
      *                       DENVOI_QV                                         
           10 QC12-DENVOI-QV       PIC X(8).                                    
      *                       DCREATION                                         
           10 QC12-DCREATION       PIC X(8).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
                                                                                
