      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * B2B : D�tail d'une vente                                        00000020
      ***************************************************************** 00000030
       01   EBB02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMANDEL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCOMMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMMANDEF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCOMMANDEI     PIC X(12).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOMPTAL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MDCOMPTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCOMPTAF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDCOMPTAI      PIC X(10).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOSSIERL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDOSSIERF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDOSSIERI     PIC X(12).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MRFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MRFOURNI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCADEAUL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLCADEAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCADEAUF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCADEAUI      PIC X(30).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTECL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MQTECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQTECF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MQTECI    PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR11L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MADR11L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR11F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MADR11I   PIC X(38).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFIXE1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MFIXE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFIXE1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFIXE1I   PIC X(14).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR21L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MADR21L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR21F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MADR21I   PIC X(38).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGSM1L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MGSM1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MGSM1F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MGSM1I    PIC X(14).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOST1L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCPOST1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOST1F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCPOST1I  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR31L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MADR31L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR31F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MADR31I   PIC X(32).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBUREAU1L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MBUREAU1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MBUREAU1F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MBUREAU1I      PIC X(14).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCLIEUL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSOCLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCLIEUF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSOCLIEUI      PIC X(6).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNVENTEI  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDVENTEI  PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOSSL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNDOSSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNDOSSF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNDOSSI   PIC X(12).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODICI   PIC X(7).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAML     COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MFAML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAMF     PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MFAMI     PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MMARQUEI  PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFFOURNL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MREFFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MREFFOURNF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MREFFOURNI     PIC X(19).                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEL     COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MQTEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQTEF     PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQTEI     PIC X(2).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDELIVL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MDDELIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDELIVF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MDDELIVI  PIC X(10).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOLISL   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCOLISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOLISF   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCOLISI   PIC X(13).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR12L   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MADR12L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR12F   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MADR12I   PIC X(38).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFIXE2L   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MFIXE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFIXE2F   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MFIXE2I   PIC X(14).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR22L   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MADR22L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR22F   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MADR22I   PIC X(38).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGSM2L    COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MGSM2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MGSM2F    PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MGSM2I    PIC X(14).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOST2L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCPOST2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOST2F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCPOST2I  PIC X(5).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADR32L   COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MADR32L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADR32F   PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MADR32I   PIC X(32).                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBUREAU2L      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MBUREAU2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MBUREAU2F      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MBUREAU2I      PIC X(14).                                 00001370
           02 MLIGNEI OCCURS   4 TIMES .                                00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTATUTL    COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MCSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSTATUTF    PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MCSTATUTI    PIC X(5).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATUTL    COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MLSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLSTATUTF    PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MLSTATUTI    PIC X(15).                                 00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREAL      COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MDCREAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCREAF      PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MDCREAI      PIC X(10).                                 00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMMENTL     COMP PIC S9(4).                            00001510
      *--                                                                       
             03 COMMENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 COMMENTF     PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 COMMENTI     PIC X(46).                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MLIBERRI  PIC X(73).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MCODTRAI  PIC X(4).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCICSI    PIC X(5).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MNETNAMI  PIC X(8).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MSCREENI  PIC X(4).                                       00001740
      ***************************************************************** 00001750
      * B2B : D�tail d'une vente                                        00001760
      ***************************************************************** 00001770
       01   EBB02O REDEFINES EBB02I.                                    00001780
           02 FILLER    PIC X(12).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDATJOUA  PIC X.                                          00001810
           02 MDATJOUC  PIC X.                                          00001820
           02 MDATJOUP  PIC X.                                          00001830
           02 MDATJOUH  PIC X.                                          00001840
           02 MDATJOUV  PIC X.                                          00001850
           02 MDATJOUO  PIC X(10).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MTIMJOUA  PIC X.                                          00001880
           02 MTIMJOUC  PIC X.                                          00001890
           02 MTIMJOUP  PIC X.                                          00001900
           02 MTIMJOUH  PIC X.                                          00001910
           02 MTIMJOUV  PIC X.                                          00001920
           02 MTIMJOUO  PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCOMMANDEA     PIC X.                                     00001950
           02 MCOMMANDEC     PIC X.                                     00001960
           02 MCOMMANDEP     PIC X.                                     00001970
           02 MCOMMANDEH     PIC X.                                     00001980
           02 MCOMMANDEV     PIC X.                                     00001990
           02 MCOMMANDEO     PIC X(12).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MDCOMPTAA      PIC X.                                     00002020
           02 MDCOMPTAC PIC X.                                          00002030
           02 MDCOMPTAP PIC X.                                          00002040
           02 MDCOMPTAH PIC X.                                          00002050
           02 MDCOMPTAV PIC X.                                          00002060
           02 MDCOMPTAO      PIC X(10).                                 00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNDOSSIERA     PIC X.                                     00002090
           02 MNDOSSIERC     PIC X.                                     00002100
           02 MNDOSSIERP     PIC X.                                     00002110
           02 MNDOSSIERH     PIC X.                                     00002120
           02 MNDOSSIERV     PIC X.                                     00002130
           02 MNDOSSIERO     PIC X(12).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MRFOURNA  PIC X.                                          00002160
           02 MRFOURNC  PIC X.                                          00002170
           02 MRFOURNP  PIC X.                                          00002180
           02 MRFOURNH  PIC X.                                          00002190
           02 MRFOURNV  PIC X.                                          00002200
           02 MRFOURNO  PIC X(7).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLCADEAUA      PIC X.                                     00002230
           02 MLCADEAUC PIC X.                                          00002240
           02 MLCADEAUP PIC X.                                          00002250
           02 MLCADEAUH PIC X.                                          00002260
           02 MLCADEAUV PIC X.                                          00002270
           02 MLCADEAUO      PIC X(30).                                 00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MQTECA    PIC X.                                          00002300
           02 MQTECC    PIC X.                                          00002310
           02 MQTECP    PIC X.                                          00002320
           02 MQTECH    PIC X.                                          00002330
           02 MQTECV    PIC X.                                          00002340
           02 MQTECO    PIC X(2).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MADR11A   PIC X.                                          00002370
           02 MADR11C   PIC X.                                          00002380
           02 MADR11P   PIC X.                                          00002390
           02 MADR11H   PIC X.                                          00002400
           02 MADR11V   PIC X.                                          00002410
           02 MADR11O   PIC X(38).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MFIXE1A   PIC X.                                          00002440
           02 MFIXE1C   PIC X.                                          00002450
           02 MFIXE1P   PIC X.                                          00002460
           02 MFIXE1H   PIC X.                                          00002470
           02 MFIXE1V   PIC X.                                          00002480
           02 MFIXE1O   PIC X(14).                                      00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MADR21A   PIC X.                                          00002510
           02 MADR21C   PIC X.                                          00002520
           02 MADR21P   PIC X.                                          00002530
           02 MADR21H   PIC X.                                          00002540
           02 MADR21V   PIC X.                                          00002550
           02 MADR21O   PIC X(38).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MGSM1A    PIC X.                                          00002580
           02 MGSM1C    PIC X.                                          00002590
           02 MGSM1P    PIC X.                                          00002600
           02 MGSM1H    PIC X.                                          00002610
           02 MGSM1V    PIC X.                                          00002620
           02 MGSM1O    PIC X(14).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCPOST1A  PIC X.                                          00002650
           02 MCPOST1C  PIC X.                                          00002660
           02 MCPOST1P  PIC X.                                          00002670
           02 MCPOST1H  PIC X.                                          00002680
           02 MCPOST1V  PIC X.                                          00002690
           02 MCPOST1O  PIC X(5).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MADR31A   PIC X.                                          00002720
           02 MADR31C   PIC X.                                          00002730
           02 MADR31P   PIC X.                                          00002740
           02 MADR31H   PIC X.                                          00002750
           02 MADR31V   PIC X.                                          00002760
           02 MADR31O   PIC X(32).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MBUREAU1A      PIC X.                                     00002790
           02 MBUREAU1C PIC X.                                          00002800
           02 MBUREAU1P PIC X.                                          00002810
           02 MBUREAU1H PIC X.                                          00002820
           02 MBUREAU1V PIC X.                                          00002830
           02 MBUREAU1O      PIC X(14).                                 00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MSOCLIEUA      PIC X.                                     00002860
           02 MSOCLIEUC PIC X.                                          00002870
           02 MSOCLIEUP PIC X.                                          00002880
           02 MSOCLIEUH PIC X.                                          00002890
           02 MSOCLIEUV PIC X.                                          00002900
           02 MSOCLIEUO      PIC X(6).                                  00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNVENTEA  PIC X.                                          00002930
           02 MNVENTEC  PIC X.                                          00002940
           02 MNVENTEP  PIC X.                                          00002950
           02 MNVENTEH  PIC X.                                          00002960
           02 MNVENTEV  PIC X.                                          00002970
           02 MNVENTEO  PIC X(7).                                       00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MDVENTEA  PIC X.                                          00003000
           02 MDVENTEC  PIC X.                                          00003010
           02 MDVENTEP  PIC X.                                          00003020
           02 MDVENTEH  PIC X.                                          00003030
           02 MDVENTEV  PIC X.                                          00003040
           02 MDVENTEO  PIC X(10).                                      00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MNDOSSA   PIC X.                                          00003070
           02 MNDOSSC   PIC X.                                          00003080
           02 MNDOSSP   PIC X.                                          00003090
           02 MNDOSSH   PIC X.                                          00003100
           02 MNDOSSV   PIC X.                                          00003110
           02 MNDOSSO   PIC X(12).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODICA   PIC X.                                          00003140
           02 MCODICC   PIC X.                                          00003150
           02 MCODICP   PIC X.                                          00003160
           02 MCODICH   PIC X.                                          00003170
           02 MCODICV   PIC X.                                          00003180
           02 MCODICO   PIC X(7).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MFAMA     PIC X.                                          00003210
           02 MFAMC     PIC X.                                          00003220
           02 MFAMP     PIC X.                                          00003230
           02 MFAMH     PIC X.                                          00003240
           02 MFAMV     PIC X.                                          00003250
           02 MFAMO     PIC X(5).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MMARQUEA  PIC X.                                          00003280
           02 MMARQUEC  PIC X.                                          00003290
           02 MMARQUEP  PIC X.                                          00003300
           02 MMARQUEH  PIC X.                                          00003310
           02 MMARQUEV  PIC X.                                          00003320
           02 MMARQUEO  PIC X(5).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MREFFOURNA     PIC X.                                     00003350
           02 MREFFOURNC     PIC X.                                     00003360
           02 MREFFOURNP     PIC X.                                     00003370
           02 MREFFOURNH     PIC X.                                     00003380
           02 MREFFOURNV     PIC X.                                     00003390
           02 MREFFOURNO     PIC X(19).                                 00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MQTEA     PIC X.                                          00003420
           02 MQTEC     PIC X.                                          00003430
           02 MQTEP     PIC X.                                          00003440
           02 MQTEH     PIC X.                                          00003450
           02 MQTEV     PIC X.                                          00003460
           02 MQTEO     PIC X(2).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MDDELIVA  PIC X.                                          00003490
           02 MDDELIVC  PIC X.                                          00003500
           02 MDDELIVP  PIC X.                                          00003510
           02 MDDELIVH  PIC X.                                          00003520
           02 MDDELIVV  PIC X.                                          00003530
           02 MDDELIVO  PIC X(10).                                      00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MCOLISA   PIC X.                                          00003560
           02 MCOLISC   PIC X.                                          00003570
           02 MCOLISP   PIC X.                                          00003580
           02 MCOLISH   PIC X.                                          00003590
           02 MCOLISV   PIC X.                                          00003600
           02 MCOLISO   PIC X(13).                                      00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MADR12A   PIC X.                                          00003630
           02 MADR12C   PIC X.                                          00003640
           02 MADR12P   PIC X.                                          00003650
           02 MADR12H   PIC X.                                          00003660
           02 MADR12V   PIC X.                                          00003670
           02 MADR12O   PIC X(38).                                      00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MFIXE2A   PIC X.                                          00003700
           02 MFIXE2C   PIC X.                                          00003710
           02 MFIXE2P   PIC X.                                          00003720
           02 MFIXE2H   PIC X.                                          00003730
           02 MFIXE2V   PIC X.                                          00003740
           02 MFIXE2O   PIC X(14).                                      00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MADR22A   PIC X.                                          00003770
           02 MADR22C   PIC X.                                          00003780
           02 MADR22P   PIC X.                                          00003790
           02 MADR22H   PIC X.                                          00003800
           02 MADR22V   PIC X.                                          00003810
           02 MADR22O   PIC X(38).                                      00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MGSM2A    PIC X.                                          00003840
           02 MGSM2C    PIC X.                                          00003850
           02 MGSM2P    PIC X.                                          00003860
           02 MGSM2H    PIC X.                                          00003870
           02 MGSM2V    PIC X.                                          00003880
           02 MGSM2O    PIC X(14).                                      00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MCPOST2A  PIC X.                                          00003910
           02 MCPOST2C  PIC X.                                          00003920
           02 MCPOST2P  PIC X.                                          00003930
           02 MCPOST2H  PIC X.                                          00003940
           02 MCPOST2V  PIC X.                                          00003950
           02 MCPOST2O  PIC X(5).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MADR32A   PIC X.                                          00003980
           02 MADR32C   PIC X.                                          00003990
           02 MADR32P   PIC X.                                          00004000
           02 MADR32H   PIC X.                                          00004010
           02 MADR32V   PIC X.                                          00004020
           02 MADR32O   PIC X(32).                                      00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MBUREAU2A      PIC X.                                     00004050
           02 MBUREAU2C PIC X.                                          00004060
           02 MBUREAU2P PIC X.                                          00004070
           02 MBUREAU2H PIC X.                                          00004080
           02 MBUREAU2V PIC X.                                          00004090
           02 MBUREAU2O      PIC X(14).                                 00004100
           02 MLIGNEO OCCURS   4 TIMES .                                00004110
             03 FILLER       PIC X(2).                                  00004120
             03 MCSTATUTA    PIC X.                                     00004130
             03 MCSTATUTC    PIC X.                                     00004140
             03 MCSTATUTP    PIC X.                                     00004150
             03 MCSTATUTH    PIC X.                                     00004160
             03 MCSTATUTV    PIC X.                                     00004170
             03 MCSTATUTO    PIC X(5).                                  00004180
             03 FILLER       PIC X(2).                                  00004190
             03 MLSTATUTA    PIC X.                                     00004200
             03 MLSTATUTC    PIC X.                                     00004210
             03 MLSTATUTP    PIC X.                                     00004220
             03 MLSTATUTH    PIC X.                                     00004230
             03 MLSTATUTV    PIC X.                                     00004240
             03 MLSTATUTO    PIC X(15).                                 00004250
             03 FILLER       PIC X(2).                                  00004260
             03 MDCREAA      PIC X.                                     00004270
             03 MDCREAC PIC X.                                          00004280
             03 MDCREAP PIC X.                                          00004290
             03 MDCREAH PIC X.                                          00004300
             03 MDCREAV PIC X.                                          00004310
             03 MDCREAO      PIC X(10).                                 00004320
             03 FILLER       PIC X(2).                                  00004330
             03 COMMENTA     PIC X.                                     00004340
             03 COMMENTC     PIC X.                                     00004350
             03 COMMENTP     PIC X.                                     00004360
             03 COMMENTH     PIC X.                                     00004370
             03 COMMENTV     PIC X.                                     00004380
             03 COMMENTO     PIC X(46).                                 00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLIBERRA  PIC X.                                          00004410
           02 MLIBERRC  PIC X.                                          00004420
           02 MLIBERRP  PIC X.                                          00004430
           02 MLIBERRH  PIC X.                                          00004440
           02 MLIBERRV  PIC X.                                          00004450
           02 MLIBERRO  PIC X(73).                                      00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MCODTRAA  PIC X.                                          00004480
           02 MCODTRAC  PIC X.                                          00004490
           02 MCODTRAP  PIC X.                                          00004500
           02 MCODTRAH  PIC X.                                          00004510
           02 MCODTRAV  PIC X.                                          00004520
           02 MCODTRAO  PIC X(4).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MCICSA    PIC X.                                          00004550
           02 MCICSC    PIC X.                                          00004560
           02 MCICSP    PIC X.                                          00004570
           02 MCICSH    PIC X.                                          00004580
           02 MCICSV    PIC X.                                          00004590
           02 MCICSO    PIC X(5).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MNETNAMA  PIC X.                                          00004620
           02 MNETNAMC  PIC X.                                          00004630
           02 MNETNAMP  PIC X.                                          00004640
           02 MNETNAMH  PIC X.                                          00004650
           02 MNETNAMV  PIC X.                                          00004660
           02 MNETNAMO  PIC X(8).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MSCREENA  PIC X.                                          00004690
           02 MSCREENC  PIC X.                                          00004700
           02 MSCREENP  PIC X.                                          00004710
           02 MSCREENH  PIC X.                                          00004720
           02 MSCREENV  PIC X.                                          00004730
           02 MSCREENO  PIC X(4).                                       00004740
                                                                                
