      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE61   EHE61                                              00000020
      ***************************************************************** 00000030
       01   EHE61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEHETI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEHETI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDENVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDENVF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDENVI    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTIERSI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLNOMI    PIC X(20).                                      00000410
           02 TABLEI OCCURS   12 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MSELI   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLHEDL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNLHEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLHEDF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNLHEDI      PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNGHEL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNGHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNGHEF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNGHEI  PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCMARQI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLREFI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EHE61   EHE61                                              00000920
      ***************************************************************** 00000930
       01   EHE61O REDEFINES EHE61I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNUMPAGEA      PIC X.                                     00001110
           02 MNUMPAGEC PIC X.                                          00001120
           02 MNUMPAGEP PIC X.                                          00001130
           02 MNUMPAGEH PIC X.                                          00001140
           02 MNUMPAGEV PIC X.                                          00001150
           02 MNUMPAGEO      PIC X(3).                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEMAXA      PIC X.                                     00001180
           02 MPAGEMAXC PIC X.                                          00001190
           02 MPAGEMAXP PIC X.                                          00001200
           02 MPAGEMAXH PIC X.                                          00001210
           02 MPAGEMAXV PIC X.                                          00001220
           02 MPAGEMAXO      PIC X(3).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCLIEHETA      PIC X.                                     00001250
           02 MCLIEHETC PIC X.                                          00001260
           02 MCLIEHETP PIC X.                                          00001270
           02 MCLIEHETH PIC X.                                          00001280
           02 MCLIEHETV PIC X.                                          00001290
           02 MCLIEHETO      PIC X(5).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLLIEHETA      PIC X.                                     00001320
           02 MLLIEHETC PIC X.                                          00001330
           02 MLLIEHETP PIC X.                                          00001340
           02 MLLIEHETH PIC X.                                          00001350
           02 MLLIEHETV PIC X.                                          00001360
           02 MLLIEHETO      PIC X(20).                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MDENVA    PIC X.                                          00001390
           02 MDENVC    PIC X.                                          00001400
           02 MDENVP    PIC X.                                          00001410
           02 MDENVH    PIC X.                                          00001420
           02 MDENVV    PIC X.                                          00001430
           02 MDENVO    PIC X(10).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCTIERSA  PIC X.                                          00001460
           02 MCTIERSC  PIC X.                                          00001470
           02 MCTIERSP  PIC X.                                          00001480
           02 MCTIERSH  PIC X.                                          00001490
           02 MCTIERSV  PIC X.                                          00001500
           02 MCTIERSO  PIC X(5).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLNOMA    PIC X.                                          00001530
           02 MLNOMC    PIC X.                                          00001540
           02 MLNOMP    PIC X.                                          00001550
           02 MLNOMH    PIC X.                                          00001560
           02 MLNOMV    PIC X.                                          00001570
           02 MLNOMO    PIC X(20).                                      00001580
           02 TABLEO OCCURS   12 TIMES .                                00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MSELA   PIC X.                                          00001610
             03 MSELC   PIC X.                                          00001620
             03 MSELP   PIC X.                                          00001630
             03 MSELH   PIC X.                                          00001640
             03 MSELV   PIC X.                                          00001650
             03 MSELO   PIC X.                                          00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNLHEDA      PIC X.                                     00001680
             03 MNLHEDC PIC X.                                          00001690
             03 MNLHEDP PIC X.                                          00001700
             03 MNLHEDH PIC X.                                          00001710
             03 MNLHEDV PIC X.                                          00001720
             03 MNLHEDO      PIC X(3).                                  00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MNGHEA  PIC X.                                          00001750
             03 MNGHEC  PIC X.                                          00001760
             03 MNGHEP  PIC X.                                          00001770
             03 MNGHEH  PIC X.                                          00001780
             03 MNGHEV  PIC X.                                          00001790
             03 MNGHEO  PIC X(7).                                       00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MNCODICA     PIC X.                                     00001820
             03 MNCODICC     PIC X.                                     00001830
             03 MNCODICP     PIC X.                                     00001840
             03 MNCODICH     PIC X.                                     00001850
             03 MNCODICV     PIC X.                                     00001860
             03 MNCODICO     PIC X(7).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MCFAMA  PIC X.                                          00001890
             03 MCFAMC  PIC X.                                          00001900
             03 MCFAMP  PIC X.                                          00001910
             03 MCFAMH  PIC X.                                          00001920
             03 MCFAMV  PIC X.                                          00001930
             03 MCFAMO  PIC X(5).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MCMARQA      PIC X.                                     00001960
             03 MCMARQC PIC X.                                          00001970
             03 MCMARQP PIC X.                                          00001980
             03 MCMARQH PIC X.                                          00001990
             03 MCMARQV PIC X.                                          00002000
             03 MCMARQO      PIC X(5).                                  00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MLREFA  PIC X.                                          00002030
             03 MLREFC  PIC X.                                          00002040
             03 MLREFP  PIC X.                                          00002050
             03 MLREFH  PIC X.                                          00002060
             03 MLREFV  PIC X.                                          00002070
             03 MLREFO  PIC X(20).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(78).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
