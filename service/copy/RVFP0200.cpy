      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFP0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFP0200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0200.                                                            
      *}                                                                        
           02  FP02-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FP02-CASSUR                                                      
               PIC X(0005).                                                     
           02  FP02-NFACTURE                                                    
               PIC X(0010).                                                     
           02  FP02-NLIGNE                                                      
               PIC X(0003).                                                     
           02  FP02-NATURE                                                      
               PIC X(0003).                                                     
           02  FP02-MARQUE                                                      
               PIC X(0005).                                                     
           02  FP02-NCODIC                                                      
               PIC X(0007).                                                     
           02  FP02-MTHT                                                        
               PIC S9(11)V99 COMP-3.                                            
           02  FP02-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  FP02-MTTTC                                                       
               PIC S9(11)V99 COMP-3.                                            
           02  FP02-MTCLIENT                                                    
               PIC S9(11)V99 COMP-3.                                            
           02  FP02-MTREMISE                                                    
               PIC S9(11)V99 COMP-3.                                            
           02  FP02-MTPSE                                                       
               PIC S9(11)V99 COMP-3.                                            
           02  FP02-CDIAGNO                                                     
               PIC X(0003).                                                     
           02  FP02-NCRI                                                        
               PIC X(0010).                                                     
           02  FP02-DANNUL                                                      
               PIC X(0008).                                                     
           02  FP02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT8900                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CASSUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CASSUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MARQUE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MARQUE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTHT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTHT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTTTC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTTTC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTCLIENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTREMISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTREMISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTPSE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTPSE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CDIAGNO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CDIAGNO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NCRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NCRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
