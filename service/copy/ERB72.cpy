      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RBX : D�tail d'une r�ception                                    00000020
      ***************************************************************** 00000030
       01   ERB72I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE1L   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE1F   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGE1I   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGE2I   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEXPEDL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNEXPEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNEXPEDF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNEXPEDI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATENVL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATENVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATENVF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATENVI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATRECL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATRECF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATRECI  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBENVL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNBENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBENVF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNBENVI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBRECL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNBRECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBRECF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNBRECI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNSERL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNSERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNSERF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNSERI   PIC X(29).                                      00000450
           02 MLIGNEI OCCURS   6 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDETL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLDETL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLDETF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLDETI  PIC X(60).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSTATUTI     PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREGL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREGF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLREGI  PIC X(30).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(73).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * RBX : D�tail d'une r�ception                                    00000800
      ***************************************************************** 00000810
       01   ERB72O REDEFINES ERB72I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGE1A   PIC X.                                          00000990
           02 MPAGE1C   PIC X.                                          00001000
           02 MPAGE1P   PIC X.                                          00001010
           02 MPAGE1H   PIC X.                                          00001020
           02 MPAGE1V   PIC X.                                          00001030
           02 MPAGE1O   PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MPAGE2A   PIC X.                                          00001060
           02 MPAGE2C   PIC X.                                          00001070
           02 MPAGE2P   PIC X.                                          00001080
           02 MPAGE2H   PIC X.                                          00001090
           02 MPAGE2V   PIC X.                                          00001100
           02 MPAGE2O   PIC X(3).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNEXPEDA  PIC X.                                          00001130
           02 MNEXPEDC  PIC X.                                          00001140
           02 MNEXPEDP  PIC X.                                          00001150
           02 MNEXPEDH  PIC X.                                          00001160
           02 MNEXPEDV  PIC X.                                          00001170
           02 MNEXPEDO  PIC X(7).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MDATENVA  PIC X.                                          00001200
           02 MDATENVC  PIC X.                                          00001210
           02 MDATENVP  PIC X.                                          00001220
           02 MDATENVH  PIC X.                                          00001230
           02 MDATENVV  PIC X.                                          00001240
           02 MDATENVO  PIC X(10).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATRECA  PIC X.                                          00001270
           02 MDATRECC  PIC X.                                          00001280
           02 MDATRECP  PIC X.                                          00001290
           02 MDATRECH  PIC X.                                          00001300
           02 MDATRECV  PIC X.                                          00001310
           02 MDATRECO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNBENVA   PIC X.                                          00001340
           02 MNBENVC   PIC X.                                          00001350
           02 MNBENVP   PIC X.                                          00001360
           02 MNBENVH   PIC X.                                          00001370
           02 MNBENVV   PIC X.                                          00001380
           02 MNBENVO   PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNBRECA   PIC X.                                          00001410
           02 MNBRECC   PIC X.                                          00001420
           02 MNBRECP   PIC X.                                          00001430
           02 MNBRECH   PIC X.                                          00001440
           02 MNBRECV   PIC X.                                          00001450
           02 MNBRECO   PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLNSERA   PIC X.                                          00001480
           02 MLNSERC   PIC X.                                          00001490
           02 MLNSERP   PIC X.                                          00001500
           02 MLNSERH   PIC X.                                          00001510
           02 MLNSERV   PIC X.                                          00001520
           02 MLNSERO   PIC X(29).                                      00001530
           02 MLIGNEO OCCURS   6 TIMES .                                00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLDETA  PIC X.                                          00001560
             03 MLDETC  PIC X.                                          00001570
             03 MLDETP  PIC X.                                          00001580
             03 MLDETH  PIC X.                                          00001590
             03 MLDETV  PIC X.                                          00001600
             03 MLDETO  PIC X(60).                                      00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MSTATUTA     PIC X.                                     00001630
             03 MSTATUTC     PIC X.                                     00001640
             03 MSTATUTP     PIC X.                                     00001650
             03 MSTATUTH     PIC X.                                     00001660
             03 MSTATUTV     PIC X.                                     00001670
             03 MSTATUTO     PIC X.                                     00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MLREGA  PIC X.                                          00001700
             03 MLREGC  PIC X.                                          00001710
             03 MLREGP  PIC X.                                          00001720
             03 MLREGH  PIC X.                                          00001730
             03 MLREGV  PIC X.                                          00001740
             03 MLREGO  PIC X(30).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(73).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
