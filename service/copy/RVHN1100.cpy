      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RTHN11)                                    *        
      *        LIBRARY(DSA045.DEVL.SOURCE(RVHN1100))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(HN11-)                                            *        
      *        STRUCTURE(RVHN1100)                                     *        
      *        APOST                                                   *        
      *        LABEL(YES)                                              *        
      *        DBCSDELIM(NO)                                           *        
      *        COLSUFFIX(YES)                                          *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RTHN11 TABLE                                 
           ( CTIERS                         CHAR(5) NOT NULL,                   
             NENTCDE                        CHAR(5) NOT NULL,                   
             DCHANGE                        CHAR(8) NOT NULL,                   
             WFLAG                          CHAR(1) NOT NULL,                   
             DSYST                          DECIMAL(13, 0) NOT NULL             
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTHN11                      *        
      ******************************************************************        
       01  RVHN1100.                                                            
      *    *************************************************************        
      *                       CTIERS                                            
           10 HN11-CTIERS          PIC X(5).                                    
      *    *************************************************************        
      *                       NENTCDE                                           
           10 HN11-NENTCDE         PIC X(5).                                    
      *    *************************************************************        
      *                       DCHANGE                                           
           10 HN11-DCHANGE         PIC X(8).                                    
      *    *************************************************************        
      *                       WFLAG                                             
           10 HN11-WFLAG           PIC X(1).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 HN11-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTHN11.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 5 TIMES.           
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 5 TIMES.               
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
                                                                                
