      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-PC24-RECORD.                                                      
           05 TS-PC24-TABLE            OCCURS 4.                                
              10 TS-PC24-CCARTE                PIC X(07).                       
              10 TS-PC24-LIBELLE               PIC X(20).                       
              10 TS-PC24-REFLOGO               PIC X(20).                       
              10 TS-PC24-PUNIT                 PIC X(09).                       
              10 TS-PC24-QTE                   PIC X(05).                       
              10 TS-PC24-MTTTC                 PIC X(08).                       
              10 TS-PC24-NSERIEDEB             PIC X(13).                       
              10 TS-PC24-NSERIEFIN             PIC X(13).                       
              10 TS-PC24-NERR                  PIC X(05).                       
              10 TS-PC24-NCDE-REG              PIC X(12).                       
                                                                                
