      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - LISTE CLIENTS A2I                                        00000020
      ***************************************************************** 00000030
       01   EAP02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCLL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNOMCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOMCLF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNOMCLI   PIC X(25).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNVENTEI  PIC X(13).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMCLL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUMCLF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMCLI   PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCRIL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCRIF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCRII    PIC X(17).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDPOSTAL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCDPOSTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDPOSTAF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCDPOSTAI      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFACTL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNFACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNFACTF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNFACTI   PIC X(6).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIXL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCHOIXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHOIXF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCHOIXI   PIC X.                                          00000530
           02 MLIGNEI OCCURS   4 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MACTI   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMPRNML     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNMPRNML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNMPRNMF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNMPRNMI     PIC X(44).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR1L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MADR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR1F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MADR1I  PIC X(25).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR2L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MADR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR2F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MADR2I  PIC X(32).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR3L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MADR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR3F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MADR3I  PIC X(30).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(79).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      * NETNAME                                                         00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      * CODE TERMINAL                                                   00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(4).                                       00001000
      ***************************************************************** 00001010
      * DA2I - LISTE CLIENTS A2I                                        00001020
      ***************************************************************** 00001030
       01   EAP02O REDEFINES EAP02I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MDATJOUA  PIC X.                                          00001070
           02 MDATJOUC  PIC X.                                          00001080
           02 MDATJOUP  PIC X.                                          00001090
           02 MDATJOUH  PIC X.                                          00001100
           02 MDATJOUV  PIC X.                                          00001110
           02 MDATJOUO  PIC X(10).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTIMJOUA  PIC X.                                          00001140
           02 MTIMJOUC  PIC X.                                          00001150
           02 MTIMJOUP  PIC X.                                          00001160
           02 MTIMJOUH  PIC X.                                          00001170
           02 MTIMJOUV  PIC X.                                          00001180
           02 MTIMJOUO  PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MFONCA    PIC X.                                          00001210
           02 MFONCC    PIC X.                                          00001220
           02 MFONCP    PIC X.                                          00001230
           02 MFONCH    PIC X.                                          00001240
           02 MFONCV    PIC X.                                          00001250
           02 MFONCO    PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MPAGEA    PIC X.                                          00001280
           02 MPAGEC    PIC X.                                          00001290
           02 MPAGEP    PIC X.                                          00001300
           02 MPAGEH    PIC X.                                          00001310
           02 MPAGEV    PIC X.                                          00001320
           02 MPAGEO    PIC 99.                                         00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNBPA     PIC X.                                          00001350
           02 MNBPC     PIC X.                                          00001360
           02 MNBPP     PIC X.                                          00001370
           02 MNBPH     PIC X.                                          00001380
           02 MNBPV     PIC X.                                          00001390
           02 MNBPO     PIC 99.                                         00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNOMCLA   PIC X.                                          00001420
           02 MNOMCLC   PIC X.                                          00001430
           02 MNOMCLP   PIC X.                                          00001440
           02 MNOMCLH   PIC X.                                          00001450
           02 MNOMCLV   PIC X.                                          00001460
           02 MNOMCLO   PIC X(25).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNVENTEA  PIC X.                                          00001490
           02 MNVENTEC  PIC X.                                          00001500
           02 MNVENTEP  PIC X.                                          00001510
           02 MNVENTEH  PIC X.                                          00001520
           02 MNVENTEV  PIC X.                                          00001530
           02 MNVENTEO  PIC X(13).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNUMCLA   PIC X.                                          00001560
           02 MNUMCLC   PIC X.                                          00001570
           02 MNUMCLP   PIC X.                                          00001580
           02 MNUMCLH   PIC X.                                          00001590
           02 MNUMCLV   PIC X.                                          00001600
           02 MNUMCLO   PIC X(8).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNCRIA    PIC X.                                          00001630
           02 MNCRIC    PIC X.                                          00001640
           02 MNCRIP    PIC X.                                          00001650
           02 MNCRIH    PIC X.                                          00001660
           02 MNCRIV    PIC X.                                          00001670
           02 MNCRIO    PIC X(17).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCDPOSTAA      PIC X.                                     00001700
           02 MCDPOSTAC PIC X.                                          00001710
           02 MCDPOSTAP PIC X.                                          00001720
           02 MCDPOSTAH PIC X.                                          00001730
           02 MCDPOSTAV PIC X.                                          00001740
           02 MCDPOSTAO      PIC X(5).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNFACTA   PIC X.                                          00001770
           02 MNFACTC   PIC X.                                          00001780
           02 MNFACTP   PIC X.                                          00001790
           02 MNFACTH   PIC X.                                          00001800
           02 MNFACTV   PIC X.                                          00001810
           02 MNFACTO   PIC X(6).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCHOIXA   PIC X.                                          00001840
           02 MCHOIXC   PIC X.                                          00001850
           02 MCHOIXP   PIC X.                                          00001860
           02 MCHOIXH   PIC X.                                          00001870
           02 MCHOIXV   PIC X.                                          00001880
           02 MCHOIXO   PIC X.                                          00001890
           02 MLIGNEO OCCURS   4 TIMES .                                00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MACTA   PIC X.                                          00001920
             03 MACTC   PIC X.                                          00001930
             03 MACTP   PIC X.                                          00001940
             03 MACTH   PIC X.                                          00001950
             03 MACTV   PIC X.                                          00001960
             03 MACTO   PIC X.                                          00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MNMPRNMA     PIC X.                                     00001990
             03 MNMPRNMC     PIC X.                                     00002000
             03 MNMPRNMP     PIC X.                                     00002010
             03 MNMPRNMH     PIC X.                                     00002020
             03 MNMPRNMV     PIC X.                                     00002030
             03 MNMPRNMO     PIC X(44).                                 00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MADR1A  PIC X.                                          00002060
             03 MADR1C  PIC X.                                          00002070
             03 MADR1P  PIC X.                                          00002080
             03 MADR1H  PIC X.                                          00002090
             03 MADR1V  PIC X.                                          00002100
             03 MADR1O  PIC X(25).                                      00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MADR2A  PIC X.                                          00002130
             03 MADR2C  PIC X.                                          00002140
             03 MADR2P  PIC X.                                          00002150
             03 MADR2H  PIC X.                                          00002160
             03 MADR2V  PIC X.                                          00002170
             03 MADR2O  PIC X(32).                                      00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MADR3A  PIC X.                                          00002200
             03 MADR3C  PIC X.                                          00002210
             03 MADR3P  PIC X.                                          00002220
             03 MADR3H  PIC X.                                          00002230
             03 MADR3V  PIC X.                                          00002240
             03 MADR3O  PIC X(30).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLIBERRA  PIC X.                                          00002270
           02 MLIBERRC  PIC X.                                          00002280
           02 MLIBERRP  PIC X.                                          00002290
           02 MLIBERRH  PIC X.                                          00002300
           02 MLIBERRV  PIC X.                                          00002310
           02 MLIBERRO  PIC X(79).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCODTRAA  PIC X.                                          00002340
           02 MCODTRAC  PIC X.                                          00002350
           02 MCODTRAP  PIC X.                                          00002360
           02 MCODTRAH  PIC X.                                          00002370
           02 MCODTRAV  PIC X.                                          00002380
           02 MCODTRAO  PIC X(4).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MZONCMDA  PIC X.                                          00002410
           02 MZONCMDC  PIC X.                                          00002420
           02 MZONCMDP  PIC X.                                          00002430
           02 MZONCMDH  PIC X.                                          00002440
           02 MZONCMDV  PIC X.                                          00002450
           02 MZONCMDO  PIC X(15).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCICSA    PIC X.                                          00002480
           02 MCICSC    PIC X.                                          00002490
           02 MCICSP    PIC X.                                          00002500
           02 MCICSH    PIC X.                                          00002510
           02 MCICSV    PIC X.                                          00002520
           02 MCICSO    PIC X(5).                                       00002530
      * NETNAME                                                         00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MNETNAMA  PIC X.                                          00002560
           02 MNETNAMC  PIC X.                                          00002570
           02 MNETNAMP  PIC X.                                          00002580
           02 MNETNAMH  PIC X.                                          00002590
           02 MNETNAMV  PIC X.                                          00002600
           02 MNETNAMO  PIC X(8).                                       00002610
      * CODE TERMINAL                                                   00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MSCREENA  PIC X.                                          00002640
           02 MSCREENC  PIC X.                                          00002650
           02 MSCREENP  PIC X.                                          00002660
           02 MSCREENH  PIC X.                                          00002670
           02 MSCREENV  PIC X.                                          00002680
           02 MSCREENO  PIC X(4).                                       00002690
                                                                                
