      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPC0601                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPC0601.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC0601.                                                            
      *}                                                                        
      *                       CCARTE                                            
           10 PC06-CCARTE          PIC X(7).                                    
      *                       LCARTE                                            
           10 PC06-LCARTE          PIC X(20).                                   
      *                       NCOMMANDE                                         
           10 PC06-NCOMMANDE       PIC X(12).                                   
      *                       REF_LOGO                                          
           10 PC06-REF-LOGO        PIC X(20).                                   
      *                       NSERIE_DEB                                        
           10 PC06-NSERIE-DEB      PIC X(13).                                   
      *                       NSERIE_FIN                                        
           10 PC06-NSERIE-FIN      PIC X(13).                                   
      *                       PRIX_UNIT                                         
           10 PC06-PRIX-UNIT       PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       QTE                                               
           10 PC06-QTE             PIC S9(5)V USAGE COMP-3.                     
      *                       MT_TTC                                            
           10 PC06-MT-TTC          PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       NERR                                              
           10 PC06-NERR            PIC S9(5)V USAGE COMP-3.                     
      *                       DSYST                                             
           10 PC06-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       NCDE_REG                                          
           10 PC06-NCDE-REG        PIC X(12).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *        
      ******************************************************************        
                                                                                
