      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC06   EHC06                                              00000020
      ***************************************************************** 00000030
       01   EHC09I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUHCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUHCL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUHCF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUHCI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUTRL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNLIEUTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUTRF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUTRI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUTRL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLLIEUTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUTRF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUTRI      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATUTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTATUTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSTATUTI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTATUTL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTATUTF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLSTATUTI      PIC X(20).                                 00000450
           02 TABLEI OCCURS   12 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCHSL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNCHSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCHSF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNCHSI  PIC X(7).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCORIGL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCORIGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCORIGF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCORIGI      PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCMARQI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLREFI  PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EHC06   EHC06                                              00000920
      ***************************************************************** 00000930
       01   EHC09O REDEFINES EHC09I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEP    PIC X.                                          00001130
           02 MPAGEH    PIC X.                                          00001140
           02 MPAGEV    PIC X.                                          00001150
           02 MPAGEO    PIC Z9.                                         00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGETOTA      PIC X.                                     00001180
           02 MPAGETOTC PIC X.                                          00001190
           02 MPAGETOTP PIC X.                                          00001200
           02 MPAGETOTH PIC X.                                          00001210
           02 MPAGETOTV PIC X.                                          00001220
           02 MPAGETOTO      PIC Z9.                                    00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNLIEUHCA      PIC X.                                     00001250
           02 MNLIEUHCC PIC X.                                          00001260
           02 MNLIEUHCP PIC X.                                          00001270
           02 MNLIEUHCH PIC X.                                          00001280
           02 MNLIEUHCV PIC X.                                          00001290
           02 MNLIEUHCO      PIC X(3).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLLIEUHCA      PIC X.                                     00001320
           02 MLLIEUHCC PIC X.                                          00001330
           02 MLLIEUHCP PIC X.                                          00001340
           02 MLLIEUHCH PIC X.                                          00001350
           02 MLLIEUHCV PIC X.                                          00001360
           02 MLLIEUHCO      PIC X(20).                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNLIEUTRA      PIC X.                                     00001390
           02 MNLIEUTRC PIC X.                                          00001400
           02 MNLIEUTRP PIC X.                                          00001410
           02 MNLIEUTRH PIC X.                                          00001420
           02 MNLIEUTRV PIC X.                                          00001430
           02 MNLIEUTRO      PIC X(3).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLLIEUTRA      PIC X.                                     00001460
           02 MLLIEUTRC PIC X.                                          00001470
           02 MLLIEUTRP PIC X.                                          00001480
           02 MLLIEUTRH PIC X.                                          00001490
           02 MLLIEUTRV PIC X.                                          00001500
           02 MLLIEUTRO      PIC X(20).                                 00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCSTATUTA      PIC X.                                     00001530
           02 MCSTATUTC PIC X.                                          00001540
           02 MCSTATUTP PIC X.                                          00001550
           02 MCSTATUTH PIC X.                                          00001560
           02 MCSTATUTV PIC X.                                          00001570
           02 MCSTATUTO      PIC X.                                     00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLSTATUTA      PIC X.                                     00001600
           02 MLSTATUTC PIC X.                                          00001610
           02 MLSTATUTP PIC X.                                          00001620
           02 MLSTATUTH PIC X.                                          00001630
           02 MLSTATUTV PIC X.                                          00001640
           02 MLSTATUTO      PIC X(20).                                 00001650
           02 TABLEO OCCURS   12 TIMES .                                00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNCHSA  PIC X.                                          00001680
             03 MNCHSC  PIC X.                                          00001690
             03 MNCHSP  PIC X.                                          00001700
             03 MNCHSH  PIC X.                                          00001710
             03 MNCHSV  PIC X.                                          00001720
             03 MNCHSO  PIC X(7).                                       00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MCORIGA      PIC X.                                     00001750
             03 MCORIGC PIC X.                                          00001760
             03 MCORIGP PIC X.                                          00001770
             03 MCORIGH PIC X.                                          00001780
             03 MCORIGV PIC X.                                          00001790
             03 MCORIGO      PIC X(7).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MNCODICA     PIC X.                                     00001820
             03 MNCODICC     PIC X.                                     00001830
             03 MNCODICP     PIC X.                                     00001840
             03 MNCODICH     PIC X.                                     00001850
             03 MNCODICV     PIC X.                                     00001860
             03 MNCODICO     PIC X(7).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MCFAMA  PIC X.                                          00001890
             03 MCFAMC  PIC X.                                          00001900
             03 MCFAMP  PIC X.                                          00001910
             03 MCFAMH  PIC X.                                          00001920
             03 MCFAMV  PIC X.                                          00001930
             03 MCFAMO  PIC X(5).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MCMARQA      PIC X.                                     00001960
             03 MCMARQC PIC X.                                          00001970
             03 MCMARQP PIC X.                                          00001980
             03 MCMARQH PIC X.                                          00001990
             03 MCMARQV PIC X.                                          00002000
             03 MCMARQO      PIC X(5).                                  00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MLREFA  PIC X.                                          00002030
             03 MLREFC  PIC X.                                          00002040
             03 MLREFP  PIC X.                                          00002050
             03 MLREFH  PIC X.                                          00002060
             03 MLREFV  PIC X.                                          00002070
             03 MLREFO  PIC X(20).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(78).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
