      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * FRANCHISE - SAISIE PARAM EXTRACT                                00000020
      ***************************************************************** 00000030
       01   EFR06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROGL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCPROGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROGF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCPROGI   PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCORIL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MSOCORIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCORIF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MSOCORII  PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUORIL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MLIEUORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUORIF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLIEUORII      PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDSTL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MSOCDSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDSTF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MSOCDSTI  PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDSTL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLIEUDSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUDSTF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLIEUDSTI      PIC X(3).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCVTEL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MSOCVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCVTEF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MSOCVTEI  PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUVTEL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLIEUVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUVTEF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLIEUVTEI      PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCOPCOI   PIC X(6).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELL   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MLIBELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBELF   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLIBELI   PIC X(15).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLAGL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MFLAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFLAGF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MFLAGI    PIC X.                                          00000550
      * ZONE CMD AIDA                                                   00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPPRL   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MSUPPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSUPPRF   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MSUPPRI   PIC X.                                          00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLIBERRI  PIC X(79).                                      00000640
      * CODE TRANSACTION                                                00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODTRAI  PIC X(4).                                       00000690
      * CICS DE TRAVAIL                                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      * NETNAME                                                         00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MNETNAMI  PIC X(8).                                       00000790
      * CODE TERMINAL                                                   00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSCREENI  PIC X(4).                                       00000840
      ***************************************************************** 00000850
      * FRANCHISE - SAISIE PARAM EXTRACT                                00000860
      ***************************************************************** 00000870
       01   EFR06O REDEFINES EFR06I.                                    00000880
           02 FILLER    PIC X(12).                                      00000890
      * DATE DU JOUR                                                    00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MDATJOUA  PIC X.                                          00000920
           02 MDATJOUC  PIC X.                                          00000930
           02 MDATJOUP  PIC X.                                          00000940
           02 MDATJOUH  PIC X.                                          00000950
           02 MDATJOUV  PIC X.                                          00000960
           02 MDATJOUO  PIC X(10).                                      00000970
      * HEURE                                                           00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MCPROGA   PIC X.                                          00001070
           02 MCPROGC   PIC X.                                          00001080
           02 MCPROGP   PIC X.                                          00001090
           02 MCPROGH   PIC X.                                          00001100
           02 MCPROGV   PIC X.                                          00001110
           02 MCPROGO   PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MSOCORIA  PIC X.                                          00001140
           02 MSOCORIC  PIC X.                                          00001150
           02 MSOCORIP  PIC X.                                          00001160
           02 MSOCORIH  PIC X.                                          00001170
           02 MSOCORIV  PIC X.                                          00001180
           02 MSOCORIO  PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MLIEUORIA      PIC X.                                     00001210
           02 MLIEUORIC PIC X.                                          00001220
           02 MLIEUORIP PIC X.                                          00001230
           02 MLIEUORIH PIC X.                                          00001240
           02 MLIEUORIV PIC X.                                          00001250
           02 MLIEUORIO      PIC X(3).                                  00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MSOCDSTA  PIC X.                                          00001280
           02 MSOCDSTC  PIC X.                                          00001290
           02 MSOCDSTP  PIC X.                                          00001300
           02 MSOCDSTH  PIC X.                                          00001310
           02 MSOCDSTV  PIC X.                                          00001320
           02 MSOCDSTO  PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLIEUDSTA      PIC X.                                     00001350
           02 MLIEUDSTC PIC X.                                          00001360
           02 MLIEUDSTP PIC X.                                          00001370
           02 MLIEUDSTH PIC X.                                          00001380
           02 MLIEUDSTV PIC X.                                          00001390
           02 MLIEUDSTO      PIC X(3).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MSOCVTEA  PIC X.                                          00001420
           02 MSOCVTEC  PIC X.                                          00001430
           02 MSOCVTEP  PIC X.                                          00001440
           02 MSOCVTEH  PIC X.                                          00001450
           02 MSOCVTEV  PIC X.                                          00001460
           02 MSOCVTEO  PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLIEUVTEA      PIC X.                                     00001490
           02 MLIEUVTEC PIC X.                                          00001500
           02 MLIEUVTEP PIC X.                                          00001510
           02 MLIEUVTEH PIC X.                                          00001520
           02 MLIEUVTEV PIC X.                                          00001530
           02 MLIEUVTEO      PIC X(3).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCOPCOA   PIC X.                                          00001560
           02 MCOPCOC   PIC X.                                          00001570
           02 MCOPCOP   PIC X.                                          00001580
           02 MCOPCOH   PIC X.                                          00001590
           02 MCOPCOV   PIC X.                                          00001600
           02 MCOPCOO   PIC X(6).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLIBELA   PIC X.                                          00001630
           02 MLIBELC   PIC X.                                          00001640
           02 MLIBELP   PIC X.                                          00001650
           02 MLIBELH   PIC X.                                          00001660
           02 MLIBELV   PIC X.                                          00001670
           02 MLIBELO   PIC X(15).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MFLAGA    PIC X.                                          00001700
           02 MFLAGC    PIC X.                                          00001710
           02 MFLAGP    PIC X.                                          00001720
           02 MFLAGH    PIC X.                                          00001730
           02 MFLAGV    PIC X.                                          00001740
           02 MFLAGO    PIC X.                                          00001750
      * ZONE CMD AIDA                                                   00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSUPPRA   PIC X.                                          00001780
           02 MSUPPRC   PIC X.                                          00001790
           02 MSUPPRP   PIC X.                                          00001800
           02 MSUPPRH   PIC X.                                          00001810
           02 MSUPPRV   PIC X.                                          00001820
           02 MSUPPRO   PIC X.                                          00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLIBERRA  PIC X.                                          00001850
           02 MLIBERRC  PIC X.                                          00001860
           02 MLIBERRP  PIC X.                                          00001870
           02 MLIBERRH  PIC X.                                          00001880
           02 MLIBERRV  PIC X.                                          00001890
           02 MLIBERRO  PIC X(79).                                      00001900
      * CODE TRANSACTION                                                00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCODTRAA  PIC X.                                          00001930
           02 MCODTRAC  PIC X.                                          00001940
           02 MCODTRAP  PIC X.                                          00001950
           02 MCODTRAH  PIC X.                                          00001960
           02 MCODTRAV  PIC X.                                          00001970
           02 MCODTRAO  PIC X(4).                                       00001980
      * CICS DE TRAVAIL                                                 00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MCICSA    PIC X.                                          00002010
           02 MCICSC    PIC X.                                          00002020
           02 MCICSP    PIC X.                                          00002030
           02 MCICSH    PIC X.                                          00002040
           02 MCICSV    PIC X.                                          00002050
           02 MCICSO    PIC X(5).                                       00002060
      * NETNAME                                                         00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
      * CODE TERMINAL                                                   00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSCREENA  PIC X.                                          00002170
           02 MSCREENC  PIC X.                                          00002180
           02 MSCREENP  PIC X.                                          00002190
           02 MSCREENH  PIC X.                                          00002200
           02 MSCREENV  PIC X.                                          00002210
           02 MSCREENO  PIC X(4).                                       00002220
                                                                                
