      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MBC50                                                 
      * APPELLE PAR LINK DANS TBC50                                             
      *                                                                         
      *--------------------------------------------------------                 
      *                                                                         
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MBC50.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
             10 COMM-MBC50-NCARTE         PIC X(09).                            
             10 COMM-MBC50-LNOM           PIC X(25).                            
             10 COMM-MBC50-LNOM-GEN       PIC X(01).                            
             10 COMM-MBC50-NCLIENTADR     PIC X(08).                            
             10 COMM-MBC50-LPRENOM        PIC X(15).                            
      * TYPE DE L'ADRESSE 'A', 'B', 'C'                                         
             10 COMM-MBC50-ADRESSE        PIC X(01).                            
             10 COMM-MBC50-CPOST          PIC X(05).                            
             10 COMM-MBC50-CINSEE         PIC X(05).                            
             10 COMM-MBC50-LCOMN          PIC X(32).                            
             10 COMM-MBC50-COMN-ERREUR    PIC X(01).                            
             10 COMM-MBC50-COMN-FORCEE    PIC X(01).                            
             10 COMM-MBC50-CPAYS          PIC X(03).                            
             10 COMM-MBC50-TPAYS          PIC X(03).                            
             10 COMM-MBC50-NTDOM          PIC X(15).                            
             10 COMM-MBC50-NGSM           PIC X(15).                            
             10 COMM-MBC50-NTBUR          PIC X(15).                            
             10 COMM-MBC50-DNAISS         PIC X(08).                            
             10 COMM-MBC50-NSOC           PIC X(03).                            
             10 COMM-MBC50-NSOCCICS       PIC X(03).                            
             10 COMM-MBC50-NLIEU          PIC X(03).                            
             10 COMM-MBC50-NVENTE         PIC X(07).                            
             10 COMM-MBC50-EMAIL          PIC X(65).                            
             10 COMM-MBC50-DERTEL         PIC X(01).                            
             10 COMM-MBC50-ACID           PIC X(08).                            
      *         RECHERCHE PAR L ORGANISATION                                    
             10 COMM-MBC50-CORG           PIC X(01).                            
      *              POMAD                                                      
             10 COMM-MBC50-POMAD          PIC X(01).                            
      *        'RD'  DONNE POUR LA REDEVANCE : LIEU DE NAISSANCE                
             10 COMM-MBC50-CQUESTION      PIC X(03).                            
      *              FILLEUR                                                    
             10 COMM-MBC50-CFILLEUR       PIC X(07).                            
      *              CONTROLES  + DONNES  EN ENTREE   O/N                       
        05 COMM-MBC50-CTR.                                                      
      *   BC50 :    NBR MAXI DE LIGNE QUI PEUVENT ETRE TRAITES = 20             
             10 COMM-MBC50-NBROCC         PIC 9(05).                            
             10 COMM-MBC50-CTR-SYNT-OK    PIC X(01).                            
             10 COMM-MBC50-CTR-LOG-OK     PIC X(01).                            
             10 COMM-MBC50-SSAAMMJJ       PIC X(08).                            
      *                                                                         
      *      MESSAGE ERREUR DE LA CONTROLE LOGIIQUE + TS DE RETOUR              
         05 COMM-BC50-RETOUR-TS.                                                
             10 COMM-MBC50-NOM-TS        PIC X(08).                             
             10 COMM-MBC50-NBPAGE        PIC 9(03).                             
             10 COMM-MBC50-NBLIG         PIC 9(03).                             
             10 COMM-MBC50-CONTROLE      PIC X(01) .                            
                88 COMM-MBC50-CONTROLE-OK     VALUE ' '.                        
                88 COMM-MBC50-CONTROLE-PAS-OK VALUE '1'.                        
             10 COMM-MBC50-MESS          PIC X(80).                             
      *      MESSAGE ERREUR MESSAGE BC                                          
         05 COMM-BC50-RETOUR-MQ.                                                
             10 COMM-MBC50-CODRETMQ      PIC X(01).                             
                  88  COMM-MBC50-MQ-OK     VALUE ' '.                           
                  88  COMM-MBC50-MQ-ERREUR VALUE '1'.                           
             10 COMM-MBC50-LIBERRMQ      PIC X(60).                             
      *      MESSAGE ERREUR MESSAGE MQ ENVOI/ TRANSMISSION.                     
         05 COMM-BC50-RETOUR-BC.                                                
             10 COMM-MBC50-CODRETBC      PIC X(01).                             
                  88  COMM-MBC50-BC-OK     VALUE ' '.                           
                  88  COMM-MBC50-BC-ERREUR VALUE '1'.                           
             10 COMM-MBC50-LIBERRBC      PIC X(60).                             
         05 FILLER                       PIC X(34).                             
                                                                                
