      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC0400                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC0400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC0400.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUMERO DE CARTE                                   
           10 BC04-NCARTE          PIC X(9).                                    
      *    *************************************************************        
      *                       NUMERO DE CLIENT                                  
           10 BC04-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       DATE D EFFET                                      
           10 BC04-DEFFET          PIC X(8).                                    
      *    *************************************************************        
      *                       PSW INITIAL                                       
           10 BC04-PSWINIT         PIC X(40).                                   
      *    *************************************************************        
      *                       SOCIETE                                           
           10 BC04-NSOCIETE        PIC X(3).                                    
      *    *************************************************************        
      *                       LIEU                                              
           10 BC04-NLIEU           PIC X(03).                                   
      *    *************************************************************        
      *                       NUMERO DE VENTE                                   
           10 BC04-NVENTE          PIC X(07).                                   
      *    *************************************************************        
      *                       NUMERO D'ORDRE DE LA VENTE                        
           10 BC04-NORDRE          PIC X(05).                                   
      *    *************************************************************        
      *                       VENDEUR                                           
           10 BC04-CVENDEUR        PIC X(06).                                   
      *    *************************************************************        
      *                       DATE ANNULATION                                   
           10 BC04-DANNULATION     PIC X(8).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC04-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       NUMERO DE COMMANDE PROSODIE                       
           10 BC04-NCDEPROSO       PIC X(12).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *    *************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC0401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC0401-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NCARTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NCARTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-DEFFET-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-PSWINIT-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-PSWINIT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NSOCIETE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NLIEU-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NVENTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NORDRE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NORDRE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-CVENDEUR-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-CVENDEUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-DANNULATION-F   PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-DANNULATION-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC04-NCDEPROSO-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC04-NCDEPROSO-F     PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *    *************************************************************        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *        
      ******************************************************************        
                                                                                
