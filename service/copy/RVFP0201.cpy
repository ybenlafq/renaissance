      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFP0201                    *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0201.                                                            
      *}                                                                        
      *                       NSOCCOMPT                                         
           10 FP02-NSOCCOMPT       PIC X(3).                                    
      *                       CASSUR                                            
           10 FP02-CASSUR          PIC X(5).                                    
      *                       NFACTURE                                          
           10 FP02-NFACTURE        PIC X(10).                                   
      *                       NLIGNE                                            
           10 FP02-NLIGNE          PIC X(3).                                    
      *                       NATURE                                            
           10 FP02-NATURE          PIC X(3).                                    
      *                       MARQUE                                            
           10 FP02-MARQUE          PIC X(5).                                    
      *                       NCODIC                                            
           10 FP02-NCODIC          PIC X(7).                                    
      *                       MTHT                                              
           10 FP02-MTHT            PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       CTAUXTVA                                          
           10 FP02-CTAUXTVA        PIC X(5).                                    
      *                       MTTTC                                             
           10 FP02-MTTTC           PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       MTCLIENT                                          
           10 FP02-MTCLIENT        PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       MTREMISE                                          
           10 FP02-MTREMISE        PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       MTPSE                                             
           10 FP02-MTPSE           PIC S9(9)V9(2) USAGE COMP-3.                 
      *                       CDIAGNO                                           
           10 FP02-CDIAGNO         PIC X(3).                                    
      *                       NCRI                                              
           10 FP02-NCRI            PIC X(10).                                   
      *                       DANNUL                                            
           10 FP02-DANNUL          PIC X(8).                                    
      *                       DSYST                                             
           10 FP02-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DDELIV                                            
           10 FP02-DDELIV          PIC X(8).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 18      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFP0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFP0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CASSUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CASSUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NATURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NATURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MARQUE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MARQUE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTHT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTHT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTTTC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTTTC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTCLIENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTREMISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTREMISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-MTPSE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-MTPSE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-CDIAGNO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-CDIAGNO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-NCRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-NCRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP02-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP02-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
