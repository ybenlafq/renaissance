      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Calendrier magasins Click &                                     00000020
      ***************************************************************** 00000030
       01   ECM01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSOCIETEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIEUI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBELLEI      PIC X(13).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MANNEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MANNEEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MANNEEI   PIC X(4).                                       00000290
           02 NSEM01D OCCURS   16 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NSEM01L      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 NSEM01L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 NSEM01F      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 NSEM01I      PIC X(5).                                  00000340
           02 LSEM01D OCCURS   16 TIMES .                               00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LSEM01L      COMP PIC S9(4).                            00000360
      *--                                                                       
             03 LSEM01L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 LSEM01F      PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 LSEM01I      PIC X.                                     00000390
           02 MDATE01D OCCURS   16 TIMES .                              00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATE01L     COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MDATE01L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATE01F     PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MDATE01I     PIC X(8).                                  00000440
           02 MOUV01D OCCURS   16 TIMES .                               00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOUV01L      COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MOUV01L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MOUV01F      PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MOUV01I      PIC X(4).                                  00000490
           02 MFER01D OCCURS   16 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFER01L      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MFER01L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFER01F      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MFER01I      PIC X(4).                                  00000540
           02 MOUV02D OCCURS   16 TIMES .                               00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOUV02L      COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MOUV02L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MOUV02F      PIC X.                                     00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MOUV02I      PIC X(4).                                  00000590
           02 MFER02D OCCURS   16 TIMES .                               00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFER02L      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MFER02L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFER02F      PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MFER02I      PIC X(4).                                  00000640
           02 NSEM02D OCCURS   16 TIMES .                               00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NSEM02L      COMP PIC S9(4).                            00000660
      *--                                                                       
             03 NSEM02L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 NSEM02F      PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 NSEM02I      PIC X(5).                                  00000690
           02 LSEM02D OCCURS   16 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LSEM02L      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 LSEM02L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 LSEM02F      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 LSEM02I      PIC X.                                     00000740
           02 MDATE02D OCCURS   16 TIMES .                              00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATE02L     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MDATE02L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATE02F     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MDATE02I     PIC X(8).                                  00000790
           02 MOUV03D OCCURS   16 TIMES .                               00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOUV03L      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MOUV03L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MOUV03F      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MOUV03I      PIC X(4).                                  00000840
           02 MFER03D OCCURS   16 TIMES .                               00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFER03L      COMP PIC S9(4).                            00000860
      *--                                                                       
             03 MFER03L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFER03F      PIC X.                                     00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MFER03I      PIC X(4).                                  00000890
           02 MOUV04D OCCURS   16 TIMES .                               00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOUV04L      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MOUV04L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MOUV04F      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MOUV04I      PIC X(4).                                  00000940
           02 MFER04D OCCURS   16 TIMES .                               00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFER04L      COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MFER04L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFER04F      PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MFER04I      PIC X(4).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MLIBERRI  PIC X(72).                                      00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCODTRAI  PIC X(4).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MCICSI    PIC X(5).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MNETNAMI  PIC X(8).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MSCREENI  PIC X(4).                                       00001190
      ***************************************************************** 00001200
      * Calendrier magasins Click &                                     00001210
      ***************************************************************** 00001220
       01   ECM01O REDEFINES ECM01I.                                    00001230
           02 FILLER    PIC X(12).                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MDATJOUA  PIC X.                                          00001260
           02 MDATJOUC  PIC X.                                          00001270
           02 MDATJOUP  PIC X.                                          00001280
           02 MDATJOUH  PIC X.                                          00001290
           02 MDATJOUV  PIC X.                                          00001300
           02 MDATJOUO  PIC X(10).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MTIMJOUA  PIC X.                                          00001330
           02 MTIMJOUC  PIC X.                                          00001340
           02 MTIMJOUP  PIC X.                                          00001350
           02 MTIMJOUH  PIC X.                                          00001360
           02 MTIMJOUV  PIC X.                                          00001370
           02 MTIMJOUO  PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MSOCIETEA      PIC X.                                     00001400
           02 MSOCIETEC PIC X.                                          00001410
           02 MSOCIETEP PIC X.                                          00001420
           02 MSOCIETEH PIC X.                                          00001430
           02 MSOCIETEV PIC X.                                          00001440
           02 MSOCIETEO      PIC X(3).                                  00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLIEUA    PIC X.                                          00001470
           02 MLIEUC    PIC X.                                          00001480
           02 MLIEUP    PIC X.                                          00001490
           02 MLIEUH    PIC X.                                          00001500
           02 MLIEUV    PIC X.                                          00001510
           02 MLIEUO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLIBELLEA      PIC X.                                     00001540
           02 MLIBELLEC PIC X.                                          00001550
           02 MLIBELLEP PIC X.                                          00001560
           02 MLIBELLEH PIC X.                                          00001570
           02 MLIBELLEV PIC X.                                          00001580
           02 MLIBELLEO      PIC X(13).                                 00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MANNEEA   PIC X.                                          00001610
           02 MANNEEC   PIC X.                                          00001620
           02 MANNEEP   PIC X.                                          00001630
           02 MANNEEH   PIC X.                                          00001640
           02 MANNEEV   PIC X.                                          00001650
           02 MANNEEO   PIC X(4).                                       00001660
           02 DFHMS1 OCCURS   16 TIMES .                                00001670
             03 FILLER       PIC X(2).                                  00001680
             03 NSEM01A      PIC X.                                     00001690
             03 NSEM01C PIC X.                                          00001700
             03 NSEM01P PIC X.                                          00001710
             03 NSEM01H PIC X.                                          00001720
             03 NSEM01V PIC X.                                          00001730
             03 NSEM01O      PIC X(5).                                  00001740
           02 DFHMS2 OCCURS   16 TIMES .                                00001750
             03 FILLER       PIC X(2).                                  00001760
             03 LSEM01A      PIC X.                                     00001770
             03 LSEM01C PIC X.                                          00001780
             03 LSEM01P PIC X.                                          00001790
             03 LSEM01H PIC X.                                          00001800
             03 LSEM01V PIC X.                                          00001810
             03 LSEM01O      PIC X.                                     00001820
           02 DFHMS3 OCCURS   16 TIMES .                                00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDATE01A     PIC X.                                     00001850
             03 MDATE01C     PIC X.                                     00001860
             03 MDATE01P     PIC X.                                     00001870
             03 MDATE01H     PIC X.                                     00001880
             03 MDATE01V     PIC X.                                     00001890
             03 MDATE01O     PIC X(8).                                  00001900
           02 DFHMS4 OCCURS   16 TIMES .                                00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MOUV01A      PIC X.                                     00001930
             03 MOUV01C PIC X.                                          00001940
             03 MOUV01P PIC X.                                          00001950
             03 MOUV01H PIC X.                                          00001960
             03 MOUV01V PIC X.                                          00001970
             03 MOUV01O      PIC X(4).                                  00001980
           02 DFHMS5 OCCURS   16 TIMES .                                00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MFER01A      PIC X.                                     00002010
             03 MFER01C PIC X.                                          00002020
             03 MFER01P PIC X.                                          00002030
             03 MFER01H PIC X.                                          00002040
             03 MFER01V PIC X.                                          00002050
             03 MFER01O      PIC X(4).                                  00002060
           02 DFHMS6 OCCURS   16 TIMES .                                00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MOUV02A      PIC X.                                     00002090
             03 MOUV02C PIC X.                                          00002100
             03 MOUV02P PIC X.                                          00002110
             03 MOUV02H PIC X.                                          00002120
             03 MOUV02V PIC X.                                          00002130
             03 MOUV02O      PIC X(4).                                  00002140
           02 DFHMS7 OCCURS   16 TIMES .                                00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MFER02A      PIC X.                                     00002170
             03 MFER02C PIC X.                                          00002180
             03 MFER02P PIC X.                                          00002190
             03 MFER02H PIC X.                                          00002200
             03 MFER02V PIC X.                                          00002210
             03 MFER02O      PIC X(4).                                  00002220
           02 DFHMS8 OCCURS   16 TIMES .                                00002230
             03 FILLER       PIC X(2).                                  00002240
             03 NSEM02A      PIC X.                                     00002250
             03 NSEM02C PIC X.                                          00002260
             03 NSEM02P PIC X.                                          00002270
             03 NSEM02H PIC X.                                          00002280
             03 NSEM02V PIC X.                                          00002290
             03 NSEM02O      PIC X(5).                                  00002300
           02 DFHMS9 OCCURS   16 TIMES .                                00002310
             03 FILLER       PIC X(2).                                  00002320
             03 LSEM02A      PIC X.                                     00002330
             03 LSEM02C PIC X.                                          00002340
             03 LSEM02P PIC X.                                          00002350
             03 LSEM02H PIC X.                                          00002360
             03 LSEM02V PIC X.                                          00002370
             03 LSEM02O      PIC X.                                     00002380
           02 DFHMS10 OCCURS   16 TIMES .                               00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MDATE02A     PIC X.                                     00002410
             03 MDATE02C     PIC X.                                     00002420
             03 MDATE02P     PIC X.                                     00002430
             03 MDATE02H     PIC X.                                     00002440
             03 MDATE02V     PIC X.                                     00002450
             03 MDATE02O     PIC X(8).                                  00002460
           02 DFHMS11 OCCURS   16 TIMES .                               00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MOUV03A      PIC X.                                     00002490
             03 MOUV03C PIC X.                                          00002500
             03 MOUV03P PIC X.                                          00002510
             03 MOUV03H PIC X.                                          00002520
             03 MOUV03V PIC X.                                          00002530
             03 MOUV03O      PIC X(4).                                  00002540
           02 DFHMS12 OCCURS   16 TIMES .                               00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MFER03A      PIC X.                                     00002570
             03 MFER03C PIC X.                                          00002580
             03 MFER03P PIC X.                                          00002590
             03 MFER03H PIC X.                                          00002600
             03 MFER03V PIC X.                                          00002610
             03 MFER03O      PIC X(4).                                  00002620
           02 DFHMS13 OCCURS   16 TIMES .                               00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MOUV04A      PIC X.                                     00002650
             03 MOUV04C PIC X.                                          00002660
             03 MOUV04P PIC X.                                          00002670
             03 MOUV04H PIC X.                                          00002680
             03 MOUV04V PIC X.                                          00002690
             03 MOUV04O      PIC X(4).                                  00002700
           02 DFHMS14 OCCURS   16 TIMES .                               00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MFER04A      PIC X.                                     00002730
             03 MFER04C PIC X.                                          00002740
             03 MFER04P PIC X.                                          00002750
             03 MFER04H PIC X.                                          00002760
             03 MFER04V PIC X.                                          00002770
             03 MFER04O      PIC X(4).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLIBERRA  PIC X.                                          00002800
           02 MLIBERRC  PIC X.                                          00002810
           02 MLIBERRP  PIC X.                                          00002820
           02 MLIBERRH  PIC X.                                          00002830
           02 MLIBERRV  PIC X.                                          00002840
           02 MLIBERRO  PIC X(72).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCODTRAA  PIC X.                                          00002870
           02 MCODTRAC  PIC X.                                          00002880
           02 MCODTRAP  PIC X.                                          00002890
           02 MCODTRAH  PIC X.                                          00002900
           02 MCODTRAV  PIC X.                                          00002910
           02 MCODTRAO  PIC X(4).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCICSA    PIC X.                                          00002940
           02 MCICSC    PIC X.                                          00002950
           02 MCICSP    PIC X.                                          00002960
           02 MCICSH    PIC X.                                          00002970
           02 MCICSV    PIC X.                                          00002980
           02 MCICSO    PIC X(5).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNETNAMA  PIC X.                                          00003010
           02 MNETNAMC  PIC X.                                          00003020
           02 MNETNAMP  PIC X.                                          00003030
           02 MNETNAMH  PIC X.                                          00003040
           02 MNETNAMV  PIC X.                                          00003050
           02 MNETNAMO  PIC X(8).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MSCREENA  PIC X.                                          00003080
           02 MSCREENC  PIC X.                                          00003090
           02 MSCREENP  PIC X.                                          00003100
           02 MSCREENH  PIC X.                                          00003110
           02 MSCREENV  PIC X.                                          00003120
           02 MSCREENO  PIC X(4).                                       00003130
                                                                                
