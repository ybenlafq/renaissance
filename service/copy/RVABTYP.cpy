      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ABTYP FAMILLE EN EXTRACTION IPR066     *        
      *----------------------------------------------------------------*        
       01  RVABTYP.                                                             
           05  ABTYP-CTABLEG2    PIC X(15).                                     
           05  ABTYP-CTABLEG2-REDEF REDEFINES ABTYP-CTABLEG2.                   
               10  ABTYP-TYPPREST        PIC X(05).                             
           05  ABTYP-WTABLEG     PIC X(80).                                     
           05  ABTYP-WTABLEG-REDEF  REDEFINES ABTYP-WTABLEG.                    
               10  ABTYP-LTYPREST        PIC X(60).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVABTYP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ABTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ABTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ABTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ABTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
