      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1700                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1700.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NCLIENT                                           
           10 BC17-NCLIENT         PIC X(8).                                    
      *    *************************************************************        
      *                       TYPE DE LIENS                                     
           10 BC17-CTYPLIEN        PIC X(5).                                    
      *    *************************************************************        
      *                       NCLIENTLIE                                        
           10 BC17-NCLIENTLIE      PIC X(8).                                    
      *    *************************************************************        
      *                       DATE DE CREATION DU LIEN                          
           10 BC17-DCREATION       PIC X(8).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC17-DSYST           PIC S9(13)  USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1700-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC17-NCLIENT-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC17-NCLIENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC17-CTYPLIEN-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC17-CTYPLIEN-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC17-NCLIENTLIE-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC17-NCLIENTLIE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC17-DCREATION-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC17-DCREATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC17-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC17-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
