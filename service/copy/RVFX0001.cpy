      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFX0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX0001                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFX0001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFX0001.                                                            
      *}                                                                        
           02  FX00-NOECS                                                       
               PIC X(0005).                                                     
           02  FX00-CRITERE1                                                    
               PIC X(0005).                                                     
           02  FX00-CRITERE2                                                    
               PIC X(0005).                                                     
           02  FX00-CRITERE3                                                    
               PIC X(0005).                                                     
           02  FX00-COMPTECG                                                    
               PIC X(0006).                                                     
           02  FX00-DATEFF                                                      
               PIC X(0008).                                                     
           02  FX00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FX00-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  FX00-SECTION                                                     
               PIC X(0006).                                                     
           02  FX00-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  FX00-WFX00                                                       
               PIC X(0006).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFX0001                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFX0001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFX0001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-NOECS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-NOECS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-CRITERE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-CRITERE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-CRITERE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-CRITERE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-CRITERE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-CRITERE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-COMPTECG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-COMPTECG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-DATEFF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-DATEFF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-SECTION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-SECTION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-RUBRIQUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-RUBRIQUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX00-WFX00-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX00-WFX00-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
