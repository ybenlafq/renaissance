      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP2A-RECORD.                                                      
           05 TS-AP2A-TABLE            OCCURS 10.                               
              10 TS-AP2A-LATTESTATION.                                          
                 12 TS-AP2A-LATTEST-1          PIC X(78).                       
                 12 TS-AP2A-LATTEST-2          PIC X(78).                       
              10 TS-AP2A-LFACTURE REDEFINES TS-AP2A-LATTESTATION.               
                 12 TS-AP2A-LFACT-1            PIC X(78).                       
                 12 TS-AP2A-LFACT-2            PIC X(78) .                      
                                                                                
