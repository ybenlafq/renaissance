      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCSOC SOCIETES CARTES T                *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQCSOC.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQCSOC.                                                             
      *}                                                                        
           05  QCSOC-CTABLEG2    PIC X(15).                                     
           05  QCSOC-CTABLEG2-REDEF REDEFINES QCSOC-CTABLEG2.                   
               10  QCSOC-NSOCIETE        PIC X(03).                             
           05  QCSOC-WTABLEG     PIC X(80).                                     
           05  QCSOC-WTABLEG-REDEF  REDEFINES QCSOC-WTABLEG.                    
               10  QCSOC-WACTIF          PIC X(01).                             
               10  QCSOC-LSOCIETE        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQCSOC-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQCSOC-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCSOC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCSOC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCSOC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCSOC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
