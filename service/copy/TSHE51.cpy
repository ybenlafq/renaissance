      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    TS DES PROGRAMMES THE51 ET MHE51                                     
       01  :X:-NOM.                                                             
           03  FILLER                PIC X(04) VALUE 'HE51'.                    
           03  :X:-TERM              PIC X(04) VALUE SPACES.                    
      *    TAILLE DE LA TS                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-LONG                  PIC S9(4) COMP  VALUE +1992.                
      *--                                                                       
       01  :X:-LONG                  PIC S9(4) COMP-5  VALUE +1992.             
      *}                                                                        
      *    DONNEES DE LA TS                                                     
       01  :X:-DATA                            VALUE SPACES.                    
           03  :X:-LIGNE             OCCURS 12.                                 
               05  :X:-NLIEUHED      PIC X(03).                                 
               05  :X:-NGHE          PIC X(07).                                 
               05  :X:-NCODIC        PIC X(07).                                 
               05  :X:-CFAM          PIC X(05).                                 
               05  :X:-CMARQ         PIC X(05).                                 
               05  :X:-LREFFOURN     PIC X(15).                                 
               05  :X:-LPANNE        PIC X(20).                                 
               05  :X:-CPANNE        PIC X(05).                                 
               05  :X:-CREFUS        PIC X(05).                                 
               05  :X:-CTRAIT        PIC X(05).                                 
               05  :X:-ORIG.                                                    
                   07  :X:-NSOCIETE  PIC X(03).                                 
                   07  :X:-NLIEU     PIC X(03).                                 
                   07  :X:-NSSLIEU   PIC X(03).                                 
                   07  :X:-CLIEUTRT  PIC X(05).                                 
               05  :X:-TOPANO        PIC X(01).                                 
      *        GESTION DE LA MODIF. DE CODIC                                    
               05  :X:-MAJ-NCODIC    PIC X(01).                                 
               05  :X:-NCODIC-NEW    PIC X(07).                                 
               05  :X:-CFAM-NEW      PIC X(05).                                 
               05  :X:-CMARQ-NEW     PIC X(05).                                 
               05  :X:-LREFFOURN-NEW PIC X(15).                                 
      *        GESTION DE LA MODIF. DE TIERS FOURNISSEUR                        
               05  :X:-WTYPTIERS     PIC X(01).                                 
               05  :X:-CRENDU        PIC X(05).                                 
               05  :X:-NENTCDE       PIC X(05).                                 
      *        GESTION DE LA PRESENCE DU NACCORD ET NSERIE                      
               05  :X:-NACCORD       PIC X(12).                                 
               05  :X:-WNACCORD      PIC X(01).                                 
               05  :X:-NSERIE        PIC X(16).                                 
               05  :X:-WNSERIE       PIC X(01).                                 
                                                                                
