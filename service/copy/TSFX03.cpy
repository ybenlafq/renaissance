      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *       DESCRIPTION DE TSFX03                                     00000020
      *                                                                 00000030
       01  TS-FX03.                                                     00000040
      *--------------------------------  LONGUEUR TS                    00000050
           02 TS-FX03-LONG                    PIC S9(5) COMP-3          00000060
                                              VALUE +196.               00000070
      *--------------------------------  DONNEES TS                     00000080
           02  TS-FX03-DONNEES.                                         00000090
               03  TS-TABLE OCCURS 14.                                  00000100
                   05  TS-FX03-SEL              PIC X(1).               00000110
                   05  TS-FX03-WSEQPRO          PIC X(7).               00000120
                   05  TS-FX03-CFAMGL           PIC X(3).               00000130
                   05  TS-FX03-NDEPTLMELA       PIC X(3).               00000140
                                                                                
