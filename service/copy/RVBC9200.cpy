      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC9200                           *        
      ******************************************************************        
       01  RVBC9200.                                                            
      *    *************************************************************        
      *                       NUMERO SOCIETE                                    
           10 BC92-NSOCIETE        PIC X(3).                                    
      *    *************************************************************        
      *                       NUMERO LIEU                                       
           10 BC92-NLIEU           PIC X(3).                                    
      *    *************************************************************        
      *                       NUMERO DE VENTE                                   
           10 BC92-NVENTE          PIC X(7).                                    
      *    *************************************************************        
      *                       CODE VENTE                                        
           10 BC92-CVENTE          PIC X(1).                                    
      *    *************************************************************        
      *                       TYPE D ADRESSE                                    
           10 BC92-WTYPEADR        PIC X(1).                                    
      *    *************************************************************        
      *                       DATE D ENVOI                                      
           10 BC92-DENVOI          PIC X(8).                                    
      *    *************************************************************        
      *                       NUMERO CLIENT VENTE                               
           10 BC92-IDCLIENT        PIC X(8).                                    
      *    *************************************************************        
      *                       NUMERO CLIENT UCM                                 
           10 BC92-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       FLAG DE DEMANDE D'ENVOI                           
           10 BC92-FSTATUT         PIC X.                                       
      *    *************************************************************        
      *                       DSYST DE TRAITEMENT                               
           10 BC92-DSYSTTRT        PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC92-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC9200-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-NSOCIETE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-NLIEU-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-NVENTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-CVENTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-CVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-WTYPEADR-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-WTYPEADR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-DENVOI-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-DENVOI-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-IDCLIENT-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-IDCLIENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-FSTATUT-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-FSTATUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-DSYSTTRT-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-DSYSTTRT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC92-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC92-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
                                                                                
