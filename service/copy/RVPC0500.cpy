      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPC0500                      *        
      ******************************************************************        
       01  RVPC0500.                                                            
      *                       NSOC                                              
           10 PC05-NSOC            PIC X(3).                                    
      *                       NLIEU                                             
           10 PC05-NLIEU           PIC X(3).                                    
      *                       NCOMMANDE                                         
           10 PC05-NCOMMANDE       PIC X(12).                                   
      *                       STATUT                                            
           10 PC05-STATUT          PIC X(10).                                   
      *                       NCHRONO                                           
           10 PC05-NCHRONO         PIC X(13).                                   
      *                       TAUX_REM_TTC                                      
           10 PC05-TAUX-REM-TTC    PIC S9(2)V9(2) USAGE COMP-3.                 
      *                       MT_REM_TTC                                        
           10 PC05-MT-REM-TTC      PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       MT_CARTES_TTC                                     
           10 PC05-MT-CARTES-TTC   PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       MT_PR_HT                                          
           10 PC05-MT-PR-HT        PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       MT_PR_TVA                                         
           10 PC05-MT-PR-TVA       PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       MT_PR_TTC                                         
           10 PC05-MT-PR-TTC       PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       MT_TOT_TTC                                        
           10 PC05-MT-TOT-TTC      PIC S9(6)V9(2) USAGE COMP-3.                 
      *                       DCOMMANDE                                         
           10 PC05-DCOMMANDE       PIC X(8).                                    
      *                       DVALIDATION                                       
           10 PC05-DVALIDATION     PIC X(8).                                    
      *                       DENVOI                                            
           10 PC05-DENVOI          PIC X(8).                                    
      *                       CVENDEUR                                          
           10 PC05-CVENDEUR        PIC X(6).                                    
      *                       N_BON_CDE                                         
           10 PC05-N-BON-CDE       PIC X(24).                                   
      *                       NCOMPTE                                           
           10 PC05-NCOMPTE         PIC X(8).                                    
      *                       RS                                                
           10 PC05-RS              PIC X(32).                                   
      *                       CVOIE                                             
           10 PC05-CVOIE           PIC X(5).                                    
      *                       CTVOIE                                            
           10 PC05-CTVOIE          PIC X(4).                                    
      *                       LNOMVOIE                                          
           10 PC05-LNOMVOIE        PIC X(21).                                   
      *                       CPADR                                             
           10 PC05-CPADR           PIC X(32).                                   
      *                       CPOSTAL                                           
           10 PC05-CPOSTAL         PIC X(5).                                    
      *                       COMMUNE                                           
           10 PC05-COMMUNE         PIC X(32).                                   
      *                       BUREAU                                            
           10 PC05-BUREAU          PIC X(32).                                   
      *                       CPAYS                                             
           10 PC05-CPAYS           PIC X(3).                                    
      *                       FNCOMPTE                                          
           10 PC05-FNCOMPTE        PIC X(8).                                    
      *                       FRS                                               
           10 PC05-FRS             PIC X(32).                                   
      *                       FCVOIE                                            
           10 PC05-FCVOIE          PIC X(5).                                    
      *                       FCTVOIE                                           
           10 PC05-FCTVOIE         PIC X(4).                                    
      *                       FLNOMVOIE                                         
           10 PC05-FLNOMVOIE       PIC X(21).                                   
      *                       FCPADR                                            
           10 PC05-FCPADR          PIC X(32).                                   
      *                       FCPOSTAL                                          
           10 PC05-FCPOSTAL        PIC X(5).                                    
      *                       FCOMMUNE                                          
           10 PC05-FCOMMUNE        PIC X(32).                                   
      *                       FBUREAU                                           
           10 PC05-FBUREAU         PIC X(32).                                   
      *                       FCPAYS                                            
           10 PC05-FCPAYS          PIC X(3).                                    
      *                       DSYST                                             
           10 PC05-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 38      *        
      ******************************************************************        
                                                                                
