      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVFR5000                                    
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFR5000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFR5000.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NSOCFACT                                          
           10 FR50-NSOCFACT        PIC X(3).                                    
      *    *************************************************************        
      *                       NLIEUFACT                                         
           10 FR50-NLIEUFACT       PIC X(3).                                    
      *    *************************************************************        
      *                       NCODIC                                            
           10 FR50-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       DJOUR                                             
           10 FR50-DJOUR           PIC X(8).                                    
      *    *************************************************************        
      *                       PRMPAV                                            
           10 FR50-PRMPAV          PIC S9(7)V9(6) USAGE COMP-3.                 
      *    *************************************************************        
      *                       PRMP                                              
           10 FR50-PRMP            PIC S9(7)V9(6) USAGE COMP-3.                 
      *    *************************************************************        
      *                       WPRMPUTIL                                         
           10 FR50-WPRMPUTIL       PIC X(7).                                    
      *    *************************************************************        
      *                       QSTOCKINIT                                        
           10 FR50-QSTOCKINIT      PIC S9(9)  USAGE COMP-3.                     
      *    *************************************************************        
      *                       QSTOCKFINAL                                       
           10 FR50-QSTOCKFINAL     PIC S9(9)  USAGE COMP-3.                     
      *    *************************************************************        
      *                       WRECYCLSTCK                                       
           10 FR50-WRECYCLSTCK     PIC X(7).                                    
      *    *************************************************************        
      *                       DCRESYST                                          
           10 FR50-DCRESYST        PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
                                                                                
