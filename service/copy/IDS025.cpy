      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IDS025 AU 09/11/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,03,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IDS025.                                                        
            05 NOMETAT-IDS025           PIC X(6) VALUE 'IDS025'.                
            05 RUPTURES-IDS025.                                                 
           10 IDS025-NSOCIETE           PIC X(03).                      007  003
           10 IDS025-WSEQED             PIC S9(05)      COMP-3.         010  003
           10 IDS025-WSEQFAM            PIC S9(05)      COMP-3.         013  003
           10 IDS025-CMARQ              PIC X(05).                      016  005
           10 IDS025-NCODIC             PIC X(07).                      021  007
           10 IDS025-NLIEUVALO          PIC X(03).                      028  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IDS025-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 IDS025-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IDS025.                                                   
           10 IDS025-CFAM               PIC X(05).                      033  005
           10 IDS025-CRAYON             PIC X(05).                      038  005
           10 IDS025-LLIEU              PIC X(20).                      043  020
           10 IDS025-LREFFOURN          PIC X(20).                      063  020
           10 IDS025-PEXCLUS            PIC S9(09)V9(6) COMP-3.         083  008
           10 IDS025-PNET               PIC S9(09)V9(6) COMP-3.         091  008
           10 IDS025-PVALINV            PIC S9(09)V9(6) COMP-3.         099  008
           10 IDS025-QEXCLUS            PIC S9(05)      COMP-3.         107  003
           10 IDS025-QTAUX              PIC S9(03)V9(2) COMP-3.         110  003
           10 IDS025-QTEINV             PIC S9(05)      COMP-3.         113  003
           10 IDS025-QTENET             PIC S9(05)      COMP-3.         116  003
            05 FILLER                      PIC X(394).                          
                                                                                
