      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RBX: Ferraillage - sel. produits                                00000020
      ***************************************************************** 00000030
       01   ERB81I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE1L   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE1F   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGE1I   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGE2I   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATDEBI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATFINI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPDTSELL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNPDTSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNPDTSELF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNPDTSELI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNIL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNFOURNIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNFOURNIF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFOURNII      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTMVMENTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTMVMENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTMVMENTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTMVMENTI      PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREFUSEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNREFUSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNREFUSEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNREFUSEI      PIC X(5).                                  00000450
           02 MLIGNEI OCCURS   14 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSERIEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSERIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSERIEF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSERIEI     PIC X(15).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIMMOBL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNIMMOBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNIMMOBF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNIMMOBI     PIC X(10).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATMVTL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDATMVTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATMVTF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATMVTI     PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNCDEI  PIC X(14).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODFERL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCODFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODFERF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCODFERI     PIC X.                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFERL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLREFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLREFERF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREFERI     PIC X(17).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(74).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * RBX: Ferraillage - sel. produits                                00000960
      ***************************************************************** 00000970
       01   ERB81O REDEFINES ERB81I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGE1A   PIC X.                                          00001150
           02 MPAGE1C   PIC X.                                          00001160
           02 MPAGE1P   PIC X.                                          00001170
           02 MPAGE1H   PIC X.                                          00001180
           02 MPAGE1V   PIC X.                                          00001190
           02 MPAGE1O   PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGE2A   PIC X.                                          00001220
           02 MPAGE2C   PIC X.                                          00001230
           02 MPAGE2P   PIC X.                                          00001240
           02 MPAGE2H   PIC X.                                          00001250
           02 MPAGE2V   PIC X.                                          00001260
           02 MPAGE2O   PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATDEBA  PIC X.                                          00001290
           02 MDATDEBC  PIC X.                                          00001300
           02 MDATDEBP  PIC X.                                          00001310
           02 MDATDEBH  PIC X.                                          00001320
           02 MDATDEBV  PIC X.                                          00001330
           02 MDATDEBO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATFINA  PIC X.                                          00001360
           02 MDATFINC  PIC X.                                          00001370
           02 MDATFINP  PIC X.                                          00001380
           02 MDATFINH  PIC X.                                          00001390
           02 MDATFINV  PIC X.                                          00001400
           02 MDATFINO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPDTSELA      PIC X.                                     00001430
           02 MNPDTSELC PIC X.                                          00001440
           02 MNPDTSELP PIC X.                                          00001450
           02 MNPDTSELH PIC X.                                          00001460
           02 MNPDTSELV PIC X.                                          00001470
           02 MNPDTSELO      PIC X(5).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNFOURNIA      PIC X.                                     00001500
           02 MNFOURNIC PIC X.                                          00001510
           02 MNFOURNIP PIC X.                                          00001520
           02 MNFOURNIH PIC X.                                          00001530
           02 MNFOURNIV PIC X.                                          00001540
           02 MNFOURNIO      PIC X(20).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MTMVMENTA      PIC X.                                     00001570
           02 MTMVMENTC PIC X.                                          00001580
           02 MTMVMENTP PIC X.                                          00001590
           02 MTMVMENTH PIC X.                                          00001600
           02 MTMVMENTV PIC X.                                          00001610
           02 MTMVMENTO      PIC X(5).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNREFUSEA      PIC X.                                     00001640
           02 MNREFUSEC PIC X.                                          00001650
           02 MNREFUSEP PIC X.                                          00001660
           02 MNREFUSEH PIC X.                                          00001670
           02 MNREFUSEV PIC X.                                          00001680
           02 MNREFUSEO      PIC X(5).                                  00001690
           02 MLIGNEO OCCURS   14 TIMES .                               00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MNSERIEA     PIC X.                                     00001720
             03 MNSERIEC     PIC X.                                     00001730
             03 MNSERIEP     PIC X.                                     00001740
             03 MNSERIEH     PIC X.                                     00001750
             03 MNSERIEV     PIC X.                                     00001760
             03 MNSERIEO     PIC X(15).                                 00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MNIMMOBA     PIC X.                                     00001790
             03 MNIMMOBC     PIC X.                                     00001800
             03 MNIMMOBP     PIC X.                                     00001810
             03 MNIMMOBH     PIC X.                                     00001820
             03 MNIMMOBV     PIC X.                                     00001830
             03 MNIMMOBO     PIC X(10).                                 00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MNCODICA     PIC X.                                     00001860
             03 MNCODICC     PIC X.                                     00001870
             03 MNCODICP     PIC X.                                     00001880
             03 MNCODICH     PIC X.                                     00001890
             03 MNCODICV     PIC X.                                     00001900
             03 MNCODICO     PIC X(7).                                  00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MDATMVTA     PIC X.                                     00001930
             03 MDATMVTC     PIC X.                                     00001940
             03 MDATMVTP     PIC X.                                     00001950
             03 MDATMVTH     PIC X.                                     00001960
             03 MDATMVTV     PIC X.                                     00001970
             03 MDATMVTO     PIC X(8).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MNCDEA  PIC X.                                          00002000
             03 MNCDEC  PIC X.                                          00002010
             03 MNCDEP  PIC X.                                          00002020
             03 MNCDEH  PIC X.                                          00002030
             03 MNCDEV  PIC X.                                          00002040
             03 MNCDEO  PIC X(14).                                      00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MCODFERA     PIC X.                                     00002070
             03 MCODFERC     PIC X.                                     00002080
             03 MCODFERP     PIC X.                                     00002090
             03 MCODFERH     PIC X.                                     00002100
             03 MCODFERV     PIC X.                                     00002110
             03 MCODFERO     PIC X.                                     00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MLREFERA     PIC X.                                     00002140
             03 MLREFERC     PIC X.                                     00002150
             03 MLREFERP     PIC X.                                     00002160
             03 MLREFERH     PIC X.                                     00002170
             03 MLREFERV     PIC X.                                     00002180
             03 MLREFERO     PIC X(17).                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(74).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
