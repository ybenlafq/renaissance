      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * DCLGEN TABLE(RDPP.RVPC0900)                                    *        
      *        LIBRARY(DSA024.DEVL.SOURCE(RVPC0900))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        APOST                                                   *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RDPP.RVPC0900                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPC0900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC0900.                                                            
      *}                                                                        
           10 PC09-RANDOMNUM        PIC X(19).                                  
           10 PC09-CTITRENOM       PIC X(5).                                    
           10 PC09-LNOM            PIC X(25).                                   
           10 PC09-LPRENOM         PIC X(15).                                   
           10 PC09-LBATIMENT       PIC X(3).                                    
           10 PC09-LESCALIER       PIC X(3).                                    
           10 PC09-LETAGE          PIC X(3).                                    
           10 PC09-LPORTE          PIC X(3).                                    
           10 PC09-LCMPAD1         PIC X(32).                                   
           10 PC09-LCMPAD2         PIC X(32).                                   
           10 PC09-CVOIE           PIC X(5).                                    
           10 PC09-CTVOIE          PIC X(4).                                    
           10 PC09-LNOMVOIE        PIC X(21).                                   
           10 PC09-LCOMMUNE        PIC X(32).                                   
           10 PC09-CPOSTAL         PIC X(5).                                    
           10 PC09-LBUREAU         PIC X(26).                                   
           10 PC09-DSYST           PIC S9(13) USAGE COMP-3.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  IRVPC0900.                                                           
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  IRVPC0900.                                                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-RANDOMNUM-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-RANDOMNUM-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-CTITRENOM-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-CTITRENOM-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LNOM-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LNOM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LPRENOM-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LPRENOM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LBATIMENT-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LBATIMENT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LESCALIER-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LESCALIER-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LETAGE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LETAGE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LPORTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LPORTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LCMPAD1-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LCMPAD1-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LCMPAD2-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LCMPAD2-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-CVOIE-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-CVOIE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-CTVOIE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-CTVOIE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LNOMVOIE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LNOMVOIE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LCOMMUNE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LCOMMUNE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-CPOSTAL-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-CPOSTAL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-LBUREAU-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-LBUREAU-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC09-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 PC09-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 17      *        
      ******************************************************************        
                                                                                
