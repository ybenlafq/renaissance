      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP190 AU 28/04/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,04,BI,A,                          *        
      *                           16,03,PD,A,                          *        
      *                           19,05,BI,A,                          *        
      *                           24,20,BI,A,                          *        
      *                           44,07,BI,A,                          *        
      *                           51,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP190.                                                        
            05 NOMETAT-ISP190           PIC X(6) VALUE 'ISP190'.                
            05 RUPTURES-ISP190.                                                 
           10 ISP190-COULEUR            PIC X(05).                      007  005
           10 ISP190-TRANSACTION        PIC X(04).                      012  004
           10 ISP190-WSEQFAM            PIC S9(05)      COMP-3.         016  003
           10 ISP190-CMARQ              PIC X(05).                      019  005
           10 ISP190-LREFFOURN          PIC X(20).                      024  020
           10 ISP190-NCODIC             PIC X(07).                      044  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP190-SEQUENCE           PIC S9(04) COMP.                051  002
      *--                                                                       
           10 ISP190-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP190.                                                   
           10 ISP190-CACID              PIC X(07).                      053  007
           10 ISP190-CFAM               PIC X(05).                      060  005
           10 ISP190-CSTATUT            PIC X(03).                      065  003
           10 ISP190-DEFFET             PIC X(08).                      068  008
           10 ISP190-DTRAIT             PIC X(10).                      076  010
           10 ISP190-LCOMMENT           PIC X(20).                      086  020
           10 ISP190-NMAG               PIC X(03).                      106  003
           10 ISP190-NSOC               PIC X(03).                      109  003
           10 ISP190-NZONPRIX           PIC X(02).                      112  002
           10 ISP190-ECART              PIC S9(04)V9(1) COMP-3.         114  003
           10 ISP190-PVTTC              PIC S9(07)V9(2) COMP-3.         117  005
           10 ISP190-SRPTTC             PIC S9(07)V9(2) COMP-3.         122  005
            05 FILLER                      PIC X(386).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP190-LONG           PIC S9(4)   COMP  VALUE +126.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP190-LONG           PIC S9(4) COMP-5  VALUE +126.           
                                                                                
      *}                                                                        
