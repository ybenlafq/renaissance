      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1800                           *        
      ******************************************************************        
       01  RVBC1800.                                                            
      *    *************************************************************        
      *                       NCLIENTADR                                        
           10 BC18-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       CODE POSTAL DE NAISSANCE                          
           10 BC18-CPOSTAL         PIC X(5).                                    
      *    *************************************************************        
      *                       COMMUNE DE NAISSANCE                              
           10 BC18-LCOMMUNE        PIC X(32).                                   
      *    *************************************************************        
      *                       DSYST                                             
           10 BC18-DSYST           PIC S9(13)  USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC1800-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC18-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC18-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC18-CDEPT-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC18-CDEPT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC18-LCOMMUNE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC18-LCOMMUNE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC18-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC18-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
