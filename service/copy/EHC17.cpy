      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC17   EHC17                                              00000020
      ***************************************************************** 00000030
       01   EHC17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNLIEUHCI      PIC X(3).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNHSI     PIC X(7).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METATECL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 METATECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 METATECF  PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 METATECI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIVL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNSOCIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCIVF  PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MNSOCIVI  PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCIVL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLSTOCIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCIVF      PIC X.                                     00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MLSTOCIVI      PIC X(3).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCIVL     COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MSLSTOCIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCIVF     PIC X.                                     00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MSLSTOCIVI     PIC X(3).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCECL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNSOCECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCECF  PIC X.                                          00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MNSOCECI  PIC X(3).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCECL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLSTOCECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCECF      PIC X.                                     00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MLSTOCECI      PIC X(3).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCECL     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MSLSTOCECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCECF     PIC X.                                     00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MSLSTOCECI     PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITIVL     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MLTRAITIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITIVF     PIC X.                                     00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MLTRAITIVI     PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITECL     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MLTRAITECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITECF     PIC X.                                     00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MLTRAITECI     PIC X(5).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICIVL     COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MNCODICIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICIVF     PIC X.                                     00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MNCODICIVI     PIC X(7).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICECL     COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MNCODICECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICECF     PIC X.                                     00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MNCODICECI     PIC X(7).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORIGIVL     COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MCPORIGIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPORIGIVF     PIC X.                                     00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MCPORIGIVI     PIC X.                                     00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORIGIVL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MCORIGIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCORIGIVF      PIC X.                                     00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MCORIGIVI      PIC X(7).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORIGECL     COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MCPORIGECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPORIGECF     PIC X.                                     00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MCPORIGECI     PIC X.                                     00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORIGECL      COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MCORIGECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCORIGECF      PIC X.                                     00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MCORIGECI      PIC X(7).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDEMACCL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MWDEMACCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWDEMACCF      PIC X.                                     00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MWDEMACCI      PIC X.                                     00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEMACCL      COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MDDEMACCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDEMACCF      PIC X.                                     00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDDEMACCI      PIC X(10).                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDOSSIERL     COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MWDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWDOSSIERF     PIC X.                                     00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MWDOSSIERI     PIC X.                                     00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOSERIEL      COMP PIC S9(4).                            00000960
      *--                                                                       
           02 MNOSERIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNOSERIEF      PIC X.                                     00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNOSERIEI      PIC X(16).                                 00000990
      * MESSAGE ERREUR                                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MLIBERRI  PIC X(78).                                      00001040
      * CODE TRANSACTION                                                00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MCODTRAI  PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MZONCMDI  PIC X(15).                                      00001130
      * CICS DE TRAVAIL                                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      * NETNAME                                                         00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNETNAMI  PIC X(8).                                       00001230
      * CODE TERMINAL                                                   00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MSCREENI  PIC X(4).                                       00001280
      ***************************************************************** 00001290
      * SDF: EHC17   EHC17                                              00001300
      ***************************************************************** 00001310
       01   EHC17O REDEFINES EHC17I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
      * DATE DU JOUR                                                    00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUH  PIC X.                                          00001380
           02 MDATJOUO  PIC X(10).                                      00001390
      * HEURE                                                           00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUH  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNLIEUHCA      PIC X.                                     00001470
           02 MNLIEUHCC PIC X.                                          00001480
           02 MNLIEUHCH PIC X.                                          00001490
           02 MNLIEUHCO      PIC X(3).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNHSA     PIC X.                                          00001520
           02 MNHSC     PIC X.                                          00001530
           02 MNHSH     PIC X.                                          00001540
           02 MNHSO     PIC X(7).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 METATECA  PIC X.                                          00001570
           02 METATECC  PIC X.                                          00001580
           02 METATECH  PIC X.                                          00001590
           02 METATECO  PIC X(10).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNSOCIVA  PIC X.                                          00001620
           02 MNSOCIVC  PIC X.                                          00001630
           02 MNSOCIVH  PIC X.                                          00001640
           02 MNSOCIVO  PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLSTOCIVA      PIC X.                                     00001670
           02 MLSTOCIVC PIC X.                                          00001680
           02 MLSTOCIVH PIC X.                                          00001690
           02 MLSTOCIVO      PIC X(3).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSLSTOCIVA     PIC X.                                     00001720
           02 MSLSTOCIVC     PIC X.                                     00001730
           02 MSLSTOCIVH     PIC X.                                     00001740
           02 MSLSTOCIVO     PIC X(3).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNSOCECA  PIC X.                                          00001770
           02 MNSOCECC  PIC X.                                          00001780
           02 MNSOCECH  PIC X.                                          00001790
           02 MNSOCECO  PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLSTOCECA      PIC X.                                     00001820
           02 MLSTOCECC PIC X.                                          00001830
           02 MLSTOCECH PIC X.                                          00001840
           02 MLSTOCECO      PIC X(3).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSLSTOCECA     PIC X.                                     00001870
           02 MSLSTOCECC     PIC X.                                     00001880
           02 MSLSTOCECH     PIC X.                                     00001890
           02 MSLSTOCECO     PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLTRAITIVA     PIC X.                                     00001920
           02 MLTRAITIVC     PIC X.                                     00001930
           02 MLTRAITIVH     PIC X.                                     00001940
           02 MLTRAITIVO     PIC X(5).                                  00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLTRAITECA     PIC X.                                     00001970
           02 MLTRAITECC     PIC X.                                     00001980
           02 MLTRAITECH     PIC X.                                     00001990
           02 MLTRAITECO     PIC X(5).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNCODICIVA     PIC X.                                     00002020
           02 MNCODICIVC     PIC X.                                     00002030
           02 MNCODICIVH     PIC X.                                     00002040
           02 MNCODICIVO     PIC X(7).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNCODICECA     PIC X.                                     00002070
           02 MNCODICECC     PIC X.                                     00002080
           02 MNCODICECH     PIC X.                                     00002090
           02 MNCODICECO     PIC X(7).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCPORIGIVA     PIC X.                                     00002120
           02 MCPORIGIVC     PIC X.                                     00002130
           02 MCPORIGIVH     PIC X.                                     00002140
           02 MCPORIGIVO     PIC X.                                     00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCORIGIVA      PIC X.                                     00002170
           02 MCORIGIVC PIC X.                                          00002180
           02 MCORIGIVH PIC X.                                          00002190
           02 MCORIGIVO      PIC X(7).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCPORIGECA     PIC X.                                     00002220
           02 MCPORIGECC     PIC X.                                     00002230
           02 MCPORIGECH     PIC X.                                     00002240
           02 MCPORIGECO     PIC X.                                     00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCORIGECA      PIC X.                                     00002270
           02 MCORIGECC PIC X.                                          00002280
           02 MCORIGECH PIC X.                                          00002290
           02 MCORIGECO      PIC X(7).                                  00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MWDEMACCA      PIC X.                                     00002320
           02 MWDEMACCC PIC X.                                          00002330
           02 MWDEMACCH PIC X.                                          00002340
           02 MWDEMACCO      PIC X.                                     00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MDDEMACCA      PIC X.                                     00002370
           02 MDDEMACCC PIC X.                                          00002380
           02 MDDEMACCH PIC X.                                          00002390
           02 MDDEMACCO      PIC X(10).                                 00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MWDOSSIERA     PIC X.                                     00002420
           02 MWDOSSIERC     PIC X.                                     00002430
           02 MWDOSSIERH     PIC X.                                     00002440
           02 MWDOSSIERO     PIC X.                                     00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MNOSERIEA      PIC X.                                     00002470
           02 MNOSERIEC PIC X.                                          00002480
           02 MNOSERIEH PIC X.                                          00002490
           02 MNOSERIEO      PIC X(16).                                 00002500
      * MESSAGE ERREUR                                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MLIBERRA  PIC X.                                          00002530
           02 MLIBERRC  PIC X.                                          00002540
           02 MLIBERRH  PIC X.                                          00002550
           02 MLIBERRO  PIC X(78).                                      00002560
      * CODE TRANSACTION                                                00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCODTRAA  PIC X.                                          00002590
           02 MCODTRAC  PIC X.                                          00002600
           02 MCODTRAH  PIC X.                                          00002610
           02 MCODTRAO  PIC X(4).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MZONCMDA  PIC X.                                          00002640
           02 MZONCMDC  PIC X.                                          00002650
           02 MZONCMDH  PIC X.                                          00002660
           02 MZONCMDO  PIC X(15).                                      00002670
      * CICS DE TRAVAIL                                                 00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCICSA    PIC X.                                          00002700
           02 MCICSC    PIC X.                                          00002710
           02 MCICSH    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
      * NETNAME                                                         00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNETNAMA  PIC X.                                          00002760
           02 MNETNAMC  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMO  PIC X(8).                                       00002790
      * CODE TERMINAL                                                   00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENH  PIC X.                                          00002840
           02 MSCREENO  PIC X(4).                                       00002850
                                                                                
