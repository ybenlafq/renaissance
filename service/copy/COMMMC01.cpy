      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                        00000010
                                                                                
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000030
           02 COMM-MC01-APPLI REDEFINES COMM-MC00-FILLER.               00000040
      *    COMM-MC02-APPLI   EST LE MEME QUE MC01                       00000040
              03 COMM-01.                                               00000050
                 05 COMM-01-MODE-INTERO     PIC X(01).                          
                     88     01-MODE-INTERO    VALUE '1'.                        
                 05 COMM-01-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-01-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-01-VAL-GROUPE.                                 00000090
                    10 COMM-01-CGROUPE         PIC X(5).                00000100
                    10 COMM-01-LGROUPE         PIC X(20).               00000110
                    10 COMM-01-TGROUPE         PIC X(5).                00000120
                    10 COMM-01-NSEQ            PIC 9(04).               00000130
                    10 COMM-01-IND-TS          PIC 9(04).               00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-01-TYPE-TRT             PIC X .                       
                     88 CONSULT-ECRAN     VALUE ' '.                            
                     88 CREATION-GROUPE   VALUE '1'.                            
                     88 MAJ-GROUPE        VALUE '2'.                            
                 05  COMM-01-MAJ-EN-COURS    PIC X(01).                         
                     88 01-MAJ-EN-COURS VALUE '1'.                              
                 05  COMM-01-CONFIRM-SUP     PIC X(01).                         
                     88 01-CONFIRM-SUP           VALUE '1'.                     
                 05  COMM-01-DEMANDE-CONFIRM PIC X(01).                         
                     88 01-DEMANDE-CONFIRM       VALUE '1'.                     
                 05 COMM-01-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-01-FILLER PIC X(97).                           00000550
      *    COMM-02         EST LE MEME QUE COMM-01                      00000040
              03 COMM-03.                                               00000550
                 05 COMM-03-MODE-INTERO     PIC X(01).                          
                     88  03-MODE-INTERO   VALUE '1'.                            
               04 COMM-03-DATA.                                         00000550
                 05 COMM-03-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-03-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-03-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-03-VAL-ENTETE.                                 00000090
                    10 COMM-03-CGROUPE         PIC X(05).               00000100
                    10 COMM-03-LGROUPE         PIC X(20).               00000100
                    10 COMM-03-CFAMSEL         PIC X(05).               00000100
                    10 COMM-03-CCHASSIS-MAX    PIC 9(07).               00000100
      *--            NBR DE CHASS AU DEBUT DE LA TRANS ET SON EVOLUTIO  00000390
                    10 COMM-03-NMBR-CHA-INIT   PIC 9(05).               00000100
                    10 COMM-03-NMBR-CHASSIS    PIC 9(05).               00000100
                 05 COMM-03-VAL-CHASSIS.                                00000090
                    10 COMM-03-CCHASSIS        PIC X(7).                00000100
                    10 COMM-03-LCHASSIS        PIC X(20).               00000110
                    10 COMM-03-CFAM            PIC X(5).                00000120
                    10 COMM-03-DCHASSIS        PIC X(10).               00000130
                    10 COMM-03-IND-TS          PIC 9(04).               00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-03-TYPE-TRT             PIC X .                       
                     88 03-CONSULT-ECRAN     VALUE ' '.                         
                     88 03-CREATION-CHASSIS  VALUE '1'.                         
                     88 03-MAJ-CHASSIS       VALUE '2'.                         
                 05  COMM-03-MAJ-EN-COURS      PIC X(01).                       
                     88 03-MAJ-EN-COURS     VALUE '1'.                          
                 05  COMM-03-CONFIRM-SUP       PIC X(01).                       
                     88 03-CONFIRM-SUP      VALUE '1'.                          
                 05  COMM-03-DEMANDE-CONFIRM   PIC X(01).                       
                     88 03-DEMANDE-CONFIRM  VALUE '1'.                          
              03 COMM-04.                                               00000550
                 05 COMM-04-MODE-INTERO     PIC X(01).                          
                     88  04-MODE-INTERO   VALUE '1'.                            
               04 COMM-04-DATA.                                         00000550
                 05 COMM-04-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-04-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-04-VAL-ENTETE.                                 00000090
                    10 COMM-04-CGROUPE         PIC X(05).               00000100
                    10 COMM-04-LGROUPE         PIC X(20).               00000100
                    10 COMM-04-CCHASSIS        PIC X(07).               00000100
                    10 COMM-04-LCHASSIS        PIC X(20).               00000100
                    10 COMM-04-CFAMCH          PIC X(05).               00000100
      *--            NBR DE CHASS AU DEBUT DE LA TRANS ET SON EVOLUTIO  00000390
                    10 COMM-04-NMBR-COD-INIT   PIC 9(05).               00000100
                    10 COMM-04-NMBR-CODICS     PIC 9(05).               00000100
                 05 COMM-04-VAL-CODIC  .                                00000090
                    10 COMM-04-NCODIC          PIC X(07).               00000120
                 05 COMM-04-IND-TS                PIC 9(04).            00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-04-TYPE-TRT             PIC X .                       
                     88 04-CONSULT-ECRAN     VALUE ' '.                         
                     88 04-CREATION-NCODIC   VALUE '1'.                         
                     88 04-MAJ-NCODIC        VALUE '2'.                         
                 05  COMM-04-TRT-LCHASSIS         PIC X .                       
                     88 04-MAJ-LCHASSIS      VALUE '1'.                         
                 05  COMM-04-MAJ-EN-COURS      PIC X(01).                       
                     88 04-MAJ-EN-COURS     VALUE '1'.                          
                 05  COMM-04-CONFIRM-SUP       PIC X(01).                       
                     88 04-CONFIRM-SUP      VALUE '1'.                          
                 05  COMM-04-DEMANDE-CONFIRM   PIC X(01).                       
                     88 04-DEMANDE-CONFIRM  VALUE '1'.                          
              03 COMM-01-FILLER PIC X(3489).                            00000550
                                                                        00000030
      *--  ZONES RESERVEES APPLICATIVES OPTION 3 ----------------- 3855 00000020
                                                                        00000030
           02 COMM-MC05-APPLI REDEFINES COMM-MC00-FILLER.               00000040
              03 COMM-05.                                               00000550
                 05 COMM-05-MODE-INTERO     PIC X(01).                          
                     88  05-MODE-INTERO   VALUE '1'.                            
               04 COMM-05-DATA.                                         00000550
                 05 COMM-05-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-05-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-05-VAL-ENTETE.                                 00000090
                    10 COMM-05-CFAMSEL         PIC X(05).               00000100
                    10 COMM-05-LFAM            PIC X(20).               00000100
                    10 COMM-05-CMARQSEL        PIC X(05).               00000100
                    10 COMM-05-LMARQ           PIC X(20).               00000100
                 05 COMM-05-VAL-CODIC  .                                00000090
                    10 COMM-05-NCODIC          PIC X(07).               00000120
                    10 COMM-05-CFAMNW          PIC X(05).               00000120
                    10 COMM-05-CMARQNW         PIC X(05).               00000120
                    10 COMM-05-LREF            PIC X(20).               00000120
                    10 COMM-05-DCREAT          PIC X(10).               00000120
                    10 COMM-05-CCHASSIS        PIC X(07).               00000120
                    10 COMM-05-LCHASSIS        PIC X(20).               00000120
                    10 COMM-05-IND-TS          PIC 9(04).               00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-05-TYPE-TRT             PIC X .                       
                     88 05-CONSULT-ECRAN     VALUE ' '.                         
                     88 05-CREATION-NCODIC   VALUE '1'.                         
                     88 05-MAJ-NCODIC        VALUE '2'.                         
                     88 05-SUP-NCODIC        VALUE '3'.                         
                 05  COMM-05-MAJ-EN-COURS      PIC X(01).                       
                     88 05-MAJ-EN-COURS     VALUE '1'.                          
                 05  COMM-05-CONFIRM-SUP       PIC X(01).                       
                     88 05-CONFIRM-SUP      VALUE '1'.                          
                 05  COMM-05-DEMANDE-CONFIRM   PIC X(01).                       
                     88 05-DEMANDE-CONFIRM  VALUE '1'.                          
              03 COMM-05-FILLER PIC X(3689).                            00000550
                                                                        00000550
      *-- EDITION DES CHASSIS PAR GROUPE                                00000390
           02 COMM-MC06-APPLI REDEFINES COMM-MC00-FILLER.               00000040
              03 COMM-06.                                               00000050
                 05 COMM-06-MODE-INTERO     PIC X(01).                          
                     88     06-MODE-INTERO    VALUE '1'.                        
                 05 COMM-06-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-06-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-06-VAL-GROUPE.                                 00000090
                    10 COMM-06-CGROUPE         PIC X(5).                00000100
                    10 COMM-06-LGROUPE         PIC X(20).               00000110
                    10 COMM-06-NSEQ            PIC 9(04).               00000130
      *--                                                               00000390
                 05  COMM-06-DEMANDE-CONFIRM PIC X(01).                         
                     88 06-DEMANDE-CONFIRM       VALUE '1'.                     
      *--            VALIDATION EDITION                                 00000390
                 05  COMM-06-DEMAND-EDIT     PIC X(01).                         
                     88 06-DEMANDE-EDIT VALUE '1'.                              
                     88 06-EDITION-TOUT VALUE '2'.                              
                 05 COMM-06-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-06-FILLER PIC X(97).                           00000550
              03 COMM-06-FILLER PIC X(3489).                            00000550
                                                                                
                                                                        00000030
