      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFP0400                    *        
      ******************************************************************        
       01  RVFP0401.                                                            
      *                       NFACT_PAC                                         
           10 FP04-NFACT-PAC       PIC X(12).                                   
      *                       NLOT                                              
           10 FP04-NLOT            PIC X(6).                                    
      *                       NUM_SEQ                                           
           10 FP04-NUM-SEQ         PIC S9(6)V USAGE COMP-3.                     
      *                       TYP_ENREG                                         
           10 FP04-TYP-ENREG       PIC X(3).                                    
      *                       CODIC                                             
           10 FP04-CODIC           PIC X(15).                                   
      *                       NOM_PREST                                         
           10 FP04-NOM-PREST       PIC X(6).                                    
      *                       CREJET                                            
           10 FP04-CREJET          PIC X(3).                                    
      *                       LREJET                                            
           10 FP04-LREJET          PIC X(120).                                  
      *                       DREJET                                            
           10 FP04-DREJET          PIC X(8).                                    
      *                       DREJET                                            
           10 FP04-NSINISTRE       PIC X(20).                                   
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
