      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP022 AU 06/01/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,06,BI,A,                          *        
      *                           24,07,BI,A,                          *        
      *                           31,05,BI,A,                          *        
      *                           36,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP022.                                                        
            05 NOMETAT-ISP022           PIC X(6) VALUE 'ISP022'.                
            05 RUPTURES-ISP022.                                                 
           10 ISP022-NSOCEMET           PIC X(03).                      007  003
           10 ISP022-NSOCRECEP          PIC X(03).                      010  003
           10 ISP022-CMARQ              PIC X(05).                      013  005
           10 ISP022-DMOIS              PIC X(06).                      018  006
           10 ISP022-NUMFACT            PIC X(07).                      024  007
           10 ISP022-CFAM               PIC X(05).                      031  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP022-SEQUENCE           PIC S9(04) COMP.                036  002
      *--                                                                       
           10 ISP022-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP022.                                                   
           10 ISP022-DAVOIR             PIC X(05).                      038  005
           10 ISP022-DEBEXER            PIC X(05).                      043  005
           10 ISP022-FINEXER            PIC X(05).                      048  005
           10 ISP022-LMARQ              PIC X(25).                      053  025
           10 ISP022-LSOCEMET           PIC X(25).                      078  025
           10 ISP022-LSOCRECEP          PIC X(25).                      103  025
           10 ISP022-MOISTRT            PIC X(05).                      128  005
           10 ISP022-NCODIC             PIC X(07).                      133  007
           10 ISP022-REFERENCE          PIC X(20).                      140  020
           10 ISP022-TYPFACT            PIC X(01).                      160  001
           10 ISP022-QTE                PIC S9(07)      COMP-3.         161  004
           10 ISP022-VALOPRMP           PIC S9(09)V9(2) COMP-3.         165  006
           10 ISP022-VALOSRP            PIC S9(09)V9(2) COMP-3.         171  006
           10 ISP022-DATEFACT           PIC X(08).                      177  008
            05 FILLER                      PIC X(328).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP022-LONG           PIC S9(4)   COMP  VALUE +184.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP022-LONG           PIC S9(4) COMP-5  VALUE +184.           
                                                                                
      *}                                                                        
