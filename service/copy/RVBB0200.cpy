      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVBB0200                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBB0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBB0200.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 BB02-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 BB02-NLIEU           PIC X(3).                                    
      *                       NORDRE                                            
           10 BB02-NORDRE          PIC X(5).                                    
      *                       NVENTE                                            
           10 BB02-NVENTE          PIC X(7).                                    
      *                       WTYPEADR                                          
           10 BB02-WTYPEADR        PIC X(1).                                    
      *                       CTITRENOM                                         
           10 BB02-CTITRENOM       PIC X(5).                                    
      *                       LNOM                                              
           10 BB02-LNOM            PIC X(25).                                   
      *                       LPRENOM                                           
           10 BB02-LPRENOM         PIC X(15).                                   
      *                       LBATIMENT                                         
           10 BB02-LBATIMENT       PIC X(3).                                    
      *                       LESCALIER                                         
           10 BB02-LESCALIER       PIC X(3).                                    
      *                       LETAGE                                            
           10 BB02-LETAGE          PIC X(3).                                    
      *                       LPORTE                                            
           10 BB02-LPORTE          PIC X(3).                                    
      *                       LCMPAD1                                           
           10 BB02-LCMPAD1         PIC X(32).                                   
      *                       LCMPAD2                                           
           10 BB02-LCMPAD2         PIC X(32).                                   
      *                       CVOIE                                             
           10 BB02-CVOIE           PIC X(5).                                    
      *                       CTVOIE                                            
           10 BB02-CTVOIE          PIC X(4).                                    
      *                       LNOMVOIE                                          
           10 BB02-LNOMVOIE        PIC X(21).                                   
      *                       LCOMMUNE                                          
           10 BB02-LCOMMUNE        PIC X(32).                                   
      *                       CPOSTAL                                           
           10 BB02-CPOSTAL         PIC X(5).                                    
      *                       LBUREAU                                           
           10 BB02-LBUREAU         PIC X(26).                                   
      *                       TELDOM                                            
           10 BB02-TELDOM          PIC X(10).                                   
      *                       TELBUR                                            
           10 BB02-TELBUR          PIC X(10).                                   
      *                       LPOSTEBUR                                         
           10 BB02-LPOSTEBUR       PIC X(5).                                    
      *                       LCOMLIV1                                          
           10 BB02-LCOMLIV1        PIC X(78).                                   
      *                       LCOMLIV2                                          
           10 BB02-LCOMLIV2        PIC X(78).                                   
      *                       WETRANGER                                         
           10 BB02-WETRANGER       PIC X(1).                                    
      *                       CZONLIV                                           
           10 BB02-CZONLIV         PIC X(5).                                    
      *                       CINSEE                                            
           10 BB02-CINSEE          PIC X(5).                                    
      *                       WCONTRA                                           
           10 BB02-WCONTRA         PIC X(1).                                    
      *                       DSYST                                             
           10 BB02-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CILOT                                             
           10 BB02-CILOT           PIC X(8).                                    
      *                       IDCLIENT                                          
           10 BB02-IDCLIENT        PIC X(8).                                    
      *                       CPAYS                                             
           10 BB02-CPAYS           PIC X(3).                                    
      *                       NGSM                                              
           10 BB02-NGSM            PIC X(15).                                   
      *                       XNSOCIETE                                         
           10 BB02-XNSOCIETE       PIC X(3).                                    
      *                       XNLIEU                                            
           10 BB02-XNLIEU          PIC X(3).                                    
      *                       XNORDRE                                           
           10 BB02-XNORDRE         PIC X(5).                                    
      *                       XNVENTE                                           
           10 BB02-XNVENTE         PIC X(7).                                    
      *                       XWTYPEADR                                         
           10 BB02-XWTYPEADR       PIC X(1).                                    
      *                       XCTITRENOM                                        
           10 BB02-XCTITRENOM      PIC X(5).                                    
      *                       XLNOM                                             
           10 BB02-XLNOM           PIC X(25).                                   
      *                       XLPRENOM                                          
           10 BB02-XLPRENOM        PIC X(15).                                   
      *                       XLBATIMENT                                        
           10 BB02-XLBATIMENT      PIC X(3).                                    
      *                       XLESCALIER                                        
           10 BB02-XLESCALIER      PIC X(3).                                    
      *                       XLETAGE                                           
           10 BB02-XLETAGE         PIC X(3).                                    
      *                       XLPORTE                                           
           10 BB02-XLPORTE         PIC X(3).                                    
      *                       XLCMPAD1                                          
           10 BB02-XLCMPAD1        PIC X(32).                                   
      *                       XLCMPAD2                                          
           10 BB02-XLCMPAD2        PIC X(32).                                   
      *                       XCVOIE                                            
           10 BB02-XCVOIE          PIC X(5).                                    
      *                       XCTVOIE                                           
           10 BB02-XCTVOIE         PIC X(4).                                    
      *                       XLNOMVOIE                                         
           10 BB02-XLNOMVOIE       PIC X(21).                                   
      *                       XLCOMMUNE                                         
           10 BB02-XLCOMMUNE       PIC X(32).                                   
      *                       XCPOSTAL                                          
           10 BB02-XCPOSTAL        PIC X(5).                                    
      *                       XLBUREAU                                          
           10 BB02-XLBUREAU        PIC X(26).                                   
      *                       XTELDOM                                           
           10 BB02-XTELDOM         PIC X(10).                                   
      *                       XTELBUR                                           
           10 BB02-XTELBUR         PIC X(10).                                   
      *                       XLPOSTEBUR                                        
           10 BB02-XLPOSTEBUR      PIC X(5).                                    
      *                       XLCOMLIV1                                         
           10 BB02-XLCOMLIV1       PIC X(78).                                   
      *                       XLCOMLIV2                                         
           10 BB02-XLCOMLIV2       PIC X(78).                                   
      *                       XWETRANGER                                        
           10 BB02-XWETRANGER      PIC X(1).                                    
      *                       XCZONLIV                                          
           10 BB02-XCZONLIV        PIC X(5).                                    
      *                       XCINSEE                                           
           10 BB02-XCINSEE         PIC X(5).                                    
      *                       XWCONTRA                                          
           10 BB02-XWCONTRA        PIC X(1).                                    
      *                       XDSYST                                            
           10 BB02-XDSYST          PIC S9(13)V USAGE COMP-3.                    
      *                       XCILOT                                            
           10 BB02-XCILOT          PIC X(8).                                    
      *                       XIDCLIENT                                         
           10 BB02-XIDCLIENT       PIC X(8).                                    
      *                       XCPAYS                                            
           10 BB02-XCPAYS          PIC X(3).                                    
      *                       XNGSM                                             
           10 BB02-XNGSM           PIC X(15).                                   
      *                       IBMSNAP_COMMITSEQ                                 
           10 BB02-IBMSNAP-COMMITSEQ                                            
              PIC X(10).                                                        
      *                       IBMSNAP_INTENTSEQ                                 
           10 BB02-IBMSNAP-INTENTSEQ                                            
              PIC X(10).                                                        
      *                       IBMSNAP_OPERATION                                 
           10 BB02-IBMSNAP-OPERATION                                            
              PIC X(1).                                                         
      *                       IBMSNAP_LOGMARKER                                 
           10 BB02-IBMSNAP-LOGMARKER                                            
              PIC X(26).                                                        
      *                       DSYSTUPD                                          
           10 BB02-DSYSTUPD        PIC S9(13)V USAGE COMP-3.                    
      *                       CTYPEUPD                                          
           10 BB02-CTYPEUPD        PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 74      *        
      ******************************************************************        
                                                                                
