      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC03   EHC03                                              00000020
      ***************************************************************** 00000030
       01   EHC03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNLIEUHCI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHSL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCHSF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCHSI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQHISTOL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MQHISTOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQHISTOF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQHISTOI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENTREEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDENTREEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDENTREEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDENTREEI      PIC X(10).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROVORIGL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCPROVORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCPROVORIGF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPROVORIGI    PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORIGINEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCORIGINEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCORIGINEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCORIGINEI     PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUEMETL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIEUEMETL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIEUEMETF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEUEMETI     PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNMVTL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNMVTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNMVTF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNMVTI   PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMVTL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNMVTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMVTF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNMVTI    PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATUTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTATUTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCSTATUTI      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCHOIXL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCCHOIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCHOIXF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCCHOIXI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMAJADRL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCMAJADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMAJADRF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMAJADRI      PIC X.                                     00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRESSEL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MADRESSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRESSEF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MADRESSEI      PIC X(8).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTHSL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCTHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTHSF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCTHSI    PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSMAGL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNHSMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNHSMAGF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNHSMAGI  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNCODICI  PIC X(7).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCFAMI    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCMARQI   PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLREFI    PIC X(20).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSERIEL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSERIEF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCSERIEI  PIC X(16).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWAREPARERL    COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MWAREPARERL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWAREPARERF    PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MWAREPARERI    PIC X.                                     00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEMANDEL     COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MDDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDDEMANDEF     PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDDEMANDEI     PIC X(10).                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPREACCL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MDPREACCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDPREACCF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MDPREACCI      PIC X(10).                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPREACCL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNPREACCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNPREACCF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNPREACCI      PIC X(12).                                 00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCFOURL     COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDACCFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDACCFOURF     PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDACCFOURI     PIC X(10).                                 00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNACCORDI      PIC X(12).                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERLOCL     COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MINTERLOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MINTERLOCF     PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MINTERLOCI     PIC X(10).                                 00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MDENVOII  PIC X(10).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNENVOII  PIC X(7).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTENVOIL      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MCTENVOIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTENVOIF      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MCTENVOII      PIC X.                                     00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCTIERSI  PIC X(5).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINTERVL     COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MDFINTERVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDFINTERVF     PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MDFINTERVI     PIC X(10).                                 00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSOLDEL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MDSOLDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSOLDEF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MDSOLDEI  PIC X(10).                                      00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBNREL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLIBNREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBNREF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLIBNREI  PIC X(10).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MNRENDUI  PIC X(20).                                      00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBDREL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MLIBDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBDREF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MLIBDREI  PIC X(4).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MDRENDUI  PIC X(10).                                      00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCREL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MLIBCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBCREF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MLIBCREI  PIC X(6).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MCRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRENDUF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MCRENDUI  PIC X(5).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MLIBERRI  PIC X(78).                                      00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCODTRAI  PIC X(4).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MCICSI    PIC X(5).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MNETNAMI  PIC X(8).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MSCREENI  PIC X(4).                                       00001890
      ***************************************************************** 00001900
      * SDF: EHC03   EHC03                                              00001910
      ***************************************************************** 00001920
       01   EHC03O REDEFINES EHC03I.                                    00001930
           02 FILLER    PIC X(12).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDATJOUA  PIC X.                                          00001960
           02 MDATJOUC  PIC X.                                          00001970
           02 MDATJOUP  PIC X.                                          00001980
           02 MDATJOUH  PIC X.                                          00001990
           02 MDATJOUV  PIC X.                                          00002000
           02 MDATJOUO  PIC X(10).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTIMJOUA  PIC X.                                          00002030
           02 MTIMJOUC  PIC X.                                          00002040
           02 MTIMJOUP  PIC X.                                          00002050
           02 MTIMJOUH  PIC X.                                          00002060
           02 MTIMJOUV  PIC X.                                          00002070
           02 MTIMJOUO  PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNLIEUHCA      PIC X.                                     00002100
           02 MNLIEUHCC PIC X.                                          00002110
           02 MNLIEUHCP PIC X.                                          00002120
           02 MNLIEUHCH PIC X.                                          00002130
           02 MNLIEUHCV PIC X.                                          00002140
           02 MNLIEUHCO      PIC X(3).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNCHSA    PIC X.                                          00002170
           02 MNCHSC    PIC X.                                          00002180
           02 MNCHSP    PIC X.                                          00002190
           02 MNCHSH    PIC X.                                          00002200
           02 MNCHSV    PIC X.                                          00002210
           02 MNCHSO    PIC 9(7).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MQHISTOA  PIC X.                                          00002240
           02 MQHISTOC  PIC X.                                          00002250
           02 MQHISTOP  PIC X.                                          00002260
           02 MQHISTOH  PIC X.                                          00002270
           02 MQHISTOV  PIC X.                                          00002280
           02 MQHISTOO  PIC X(3).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MDENTREEA      PIC X.                                     00002310
           02 MDENTREEC PIC X.                                          00002320
           02 MDENTREEP PIC X.                                          00002330
           02 MDENTREEH PIC X.                                          00002340
           02 MDENTREEV PIC X.                                          00002350
           02 MDENTREEO      PIC X(10).                                 00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCPROVORIGA    PIC X.                                     00002380
           02 MCPROVORIGC    PIC X.                                     00002390
           02 MCPROVORIGP    PIC X.                                     00002400
           02 MCPROVORIGH    PIC X.                                     00002410
           02 MCPROVORIGV    PIC X.                                     00002420
           02 MCPROVORIGO    PIC X.                                     00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCORIGINEA     PIC X.                                     00002450
           02 MCORIGINEC     PIC X.                                     00002460
           02 MCORIGINEP     PIC X.                                     00002470
           02 MCORIGINEH     PIC X.                                     00002480
           02 MCORIGINEV     PIC X.                                     00002490
           02 MCORIGINEO     PIC X(7).                                  00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLIEUEMETA     PIC X.                                     00002520
           02 MLIEUEMETC     PIC X.                                     00002530
           02 MLIEUEMETP     PIC X.                                     00002540
           02 MLIEUEMETH     PIC X.                                     00002550
           02 MLIEUEMETV     PIC X.                                     00002560
           02 MLIEUEMETO     PIC X(3).                                  00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLNMVTA   PIC X.                                          00002590
           02 MLNMVTC   PIC X.                                          00002600
           02 MLNMVTP   PIC X.                                          00002610
           02 MLNMVTH   PIC X.                                          00002620
           02 MLNMVTV   PIC X.                                          00002630
           02 MLNMVTO   PIC X(6).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNMVTA    PIC X.                                          00002660
           02 MNMVTC    PIC X.                                          00002670
           02 MNMVTP    PIC X.                                          00002680
           02 MNMVTH    PIC X.                                          00002690
           02 MNMVTV    PIC X.                                          00002700
           02 MNMVTO    PIC X(7).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCSTATUTA      PIC X.                                     00002730
           02 MCSTATUTC PIC X.                                          00002740
           02 MCSTATUTP PIC X.                                          00002750
           02 MCSTATUTH PIC X.                                          00002760
           02 MCSTATUTV PIC X.                                          00002770
           02 MCSTATUTO      PIC X.                                     00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCCHOIXA  PIC X.                                          00002800
           02 MCCHOIXC  PIC X.                                          00002810
           02 MCCHOIXP  PIC X.                                          00002820
           02 MCCHOIXH  PIC X.                                          00002830
           02 MCCHOIXV  PIC X.                                          00002840
           02 MCCHOIXO  PIC X(3).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCMAJADRA      PIC X.                                     00002870
           02 MCMAJADRC PIC X.                                          00002880
           02 MCMAJADRP PIC X.                                          00002890
           02 MCMAJADRH PIC X.                                          00002900
           02 MCMAJADRV PIC X.                                          00002910
           02 MCMAJADRO      PIC X.                                     00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MADRESSEA      PIC X.                                     00002940
           02 MADRESSEC PIC X.                                          00002950
           02 MADRESSEP PIC X.                                          00002960
           02 MADRESSEH PIC X.                                          00002970
           02 MADRESSEV PIC X.                                          00002980
           02 MADRESSEO      PIC X(8).                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCTHSA    PIC X.                                          00003010
           02 MCTHSC    PIC X.                                          00003020
           02 MCTHSP    PIC X.                                          00003030
           02 MCTHSH    PIC X.                                          00003040
           02 MCTHSV    PIC X.                                          00003050
           02 MCTHSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNHSMAGA  PIC X.                                          00003080
           02 MNHSMAGC  PIC X.                                          00003090
           02 MNHSMAGP  PIC X.                                          00003100
           02 MNHSMAGH  PIC X.                                          00003110
           02 MNHSMAGV  PIC X.                                          00003120
           02 MNHSMAGO  PIC X(7).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MNCODICA  PIC X.                                          00003150
           02 MNCODICC  PIC X.                                          00003160
           02 MNCODICP  PIC X.                                          00003170
           02 MNCODICH  PIC X.                                          00003180
           02 MNCODICV  PIC X.                                          00003190
           02 MNCODICO  PIC 9(7).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MCFAMA    PIC X.                                          00003220
           02 MCFAMC    PIC X.                                          00003230
           02 MCFAMP    PIC X.                                          00003240
           02 MCFAMH    PIC X.                                          00003250
           02 MCFAMV    PIC X.                                          00003260
           02 MCFAMO    PIC X(5).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MCMARQA   PIC X.                                          00003290
           02 MCMARQC   PIC X.                                          00003300
           02 MCMARQP   PIC X.                                          00003310
           02 MCMARQH   PIC X.                                          00003320
           02 MCMARQV   PIC X.                                          00003330
           02 MCMARQO   PIC X(5).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MLREFA    PIC X.                                          00003360
           02 MLREFC    PIC X.                                          00003370
           02 MLREFP    PIC X.                                          00003380
           02 MLREFH    PIC X.                                          00003390
           02 MLREFV    PIC X.                                          00003400
           02 MLREFO    PIC X(20).                                      00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCSERIEA  PIC X.                                          00003430
           02 MCSERIEC  PIC X.                                          00003440
           02 MCSERIEP  PIC X.                                          00003450
           02 MCSERIEH  PIC X.                                          00003460
           02 MCSERIEV  PIC X.                                          00003470
           02 MCSERIEO  PIC X(16).                                      00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MWAREPARERA    PIC X.                                     00003500
           02 MWAREPARERC    PIC X.                                     00003510
           02 MWAREPARERP    PIC X.                                     00003520
           02 MWAREPARERH    PIC X.                                     00003530
           02 MWAREPARERV    PIC X.                                     00003540
           02 MWAREPARERO    PIC X.                                     00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MDDEMANDEA     PIC X.                                     00003570
           02 MDDEMANDEC     PIC X.                                     00003580
           02 MDDEMANDEP     PIC X.                                     00003590
           02 MDDEMANDEH     PIC X.                                     00003600
           02 MDDEMANDEV     PIC X.                                     00003610
           02 MDDEMANDEO     PIC X(10).                                 00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MDPREACCA      PIC X.                                     00003640
           02 MDPREACCC PIC X.                                          00003650
           02 MDPREACCP PIC X.                                          00003660
           02 MDPREACCH PIC X.                                          00003670
           02 MDPREACCV PIC X.                                          00003680
           02 MDPREACCO      PIC X(10).                                 00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MNPREACCA      PIC X.                                     00003710
           02 MNPREACCC PIC X.                                          00003720
           02 MNPREACCP PIC X.                                          00003730
           02 MNPREACCH PIC X.                                          00003740
           02 MNPREACCV PIC X.                                          00003750
           02 MNPREACCO      PIC X(12).                                 00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MDACCFOURA     PIC X.                                     00003780
           02 MDACCFOURC     PIC X.                                     00003790
           02 MDACCFOURP     PIC X.                                     00003800
           02 MDACCFOURH     PIC X.                                     00003810
           02 MDACCFOURV     PIC X.                                     00003820
           02 MDACCFOURO     PIC X(10).                                 00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNACCORDA      PIC X.                                     00003850
           02 MNACCORDC PIC X.                                          00003860
           02 MNACCORDP PIC X.                                          00003870
           02 MNACCORDH PIC X.                                          00003880
           02 MNACCORDV PIC X.                                          00003890
           02 MNACCORDO      PIC X(12).                                 00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MINTERLOCA     PIC X.                                     00003920
           02 MINTERLOCC     PIC X.                                     00003930
           02 MINTERLOCP     PIC X.                                     00003940
           02 MINTERLOCH     PIC X.                                     00003950
           02 MINTERLOCV     PIC X.                                     00003960
           02 MINTERLOCO     PIC X(10).                                 00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MDENVOIA  PIC X.                                          00003990
           02 MDENVOIC  PIC X.                                          00004000
           02 MDENVOIP  PIC X.                                          00004010
           02 MDENVOIH  PIC X.                                          00004020
           02 MDENVOIV  PIC X.                                          00004030
           02 MDENVOIO  PIC X(10).                                      00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MNENVOIA  PIC X.                                          00004060
           02 MNENVOIC  PIC X.                                          00004070
           02 MNENVOIP  PIC X.                                          00004080
           02 MNENVOIH  PIC X.                                          00004090
           02 MNENVOIV  PIC X.                                          00004100
           02 MNENVOIO  PIC X(7).                                       00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MCTENVOIA      PIC X.                                     00004130
           02 MCTENVOIC PIC X.                                          00004140
           02 MCTENVOIP PIC X.                                          00004150
           02 MCTENVOIH PIC X.                                          00004160
           02 MCTENVOIV PIC X.                                          00004170
           02 MCTENVOIO      PIC X.                                     00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MCTIERSA  PIC X.                                          00004200
           02 MCTIERSC  PIC X.                                          00004210
           02 MCTIERSP  PIC X.                                          00004220
           02 MCTIERSH  PIC X.                                          00004230
           02 MCTIERSV  PIC X.                                          00004240
           02 MCTIERSO  PIC X(5).                                       00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MDFINTERVA     PIC X.                                     00004270
           02 MDFINTERVC     PIC X.                                     00004280
           02 MDFINTERVP     PIC X.                                     00004290
           02 MDFINTERVH     PIC X.                                     00004300
           02 MDFINTERVV     PIC X.                                     00004310
           02 MDFINTERVO     PIC X(10).                                 00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MDSOLDEA  PIC X.                                          00004340
           02 MDSOLDEC  PIC X.                                          00004350
           02 MDSOLDEP  PIC X.                                          00004360
           02 MDSOLDEH  PIC X.                                          00004370
           02 MDSOLDEV  PIC X.                                          00004380
           02 MDSOLDEO  PIC X(10).                                      00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLIBNREA  PIC X.                                          00004410
           02 MLIBNREC  PIC X.                                          00004420
           02 MLIBNREP  PIC X.                                          00004430
           02 MLIBNREH  PIC X.                                          00004440
           02 MLIBNREV  PIC X.                                          00004450
           02 MLIBNREO  PIC X(10).                                      00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MNRENDUA  PIC X.                                          00004480
           02 MNRENDUC  PIC X.                                          00004490
           02 MNRENDUP  PIC X.                                          00004500
           02 MNRENDUH  PIC X.                                          00004510
           02 MNRENDUV  PIC X.                                          00004520
           02 MNRENDUO  PIC X(20).                                      00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MLIBDREA  PIC X.                                          00004550
           02 MLIBDREC  PIC X.                                          00004560
           02 MLIBDREP  PIC X.                                          00004570
           02 MLIBDREH  PIC X.                                          00004580
           02 MLIBDREV  PIC X.                                          00004590
           02 MLIBDREO  PIC X(4).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MDRENDUA  PIC X.                                          00004620
           02 MDRENDUC  PIC X.                                          00004630
           02 MDRENDUP  PIC X.                                          00004640
           02 MDRENDUH  PIC X.                                          00004650
           02 MDRENDUV  PIC X.                                          00004660
           02 MDRENDUO  PIC X(10).                                      00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MLIBCREA  PIC X.                                          00004690
           02 MLIBCREC  PIC X.                                          00004700
           02 MLIBCREP  PIC X.                                          00004710
           02 MLIBCREH  PIC X.                                          00004720
           02 MLIBCREV  PIC X.                                          00004730
           02 MLIBCREO  PIC X(6).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MCRENDUA  PIC X.                                          00004760
           02 MCRENDUC  PIC X.                                          00004770
           02 MCRENDUP  PIC X.                                          00004780
           02 MCRENDUH  PIC X.                                          00004790
           02 MCRENDUV  PIC X.                                          00004800
           02 MCRENDUO  PIC X(5).                                       00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MLIBERRA  PIC X.                                          00004830
           02 MLIBERRC  PIC X.                                          00004840
           02 MLIBERRP  PIC X.                                          00004850
           02 MLIBERRH  PIC X.                                          00004860
           02 MLIBERRV  PIC X.                                          00004870
           02 MLIBERRO  PIC X(78).                                      00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCODTRAA  PIC X.                                          00004900
           02 MCODTRAC  PIC X.                                          00004910
           02 MCODTRAP  PIC X.                                          00004920
           02 MCODTRAH  PIC X.                                          00004930
           02 MCODTRAV  PIC X.                                          00004940
           02 MCODTRAO  PIC X(4).                                       00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MCICSA    PIC X.                                          00004970
           02 MCICSC    PIC X.                                          00004980
           02 MCICSP    PIC X.                                          00004990
           02 MCICSH    PIC X.                                          00005000
           02 MCICSV    PIC X.                                          00005010
           02 MCICSO    PIC X(5).                                       00005020
           02 FILLER    PIC X(2).                                       00005030
           02 MNETNAMA  PIC X.                                          00005040
           02 MNETNAMC  PIC X.                                          00005050
           02 MNETNAMP  PIC X.                                          00005060
           02 MNETNAMH  PIC X.                                          00005070
           02 MNETNAMV  PIC X.                                          00005080
           02 MNETNAMO  PIC X(8).                                       00005090
           02 FILLER    PIC X(2).                                       00005100
           02 MSCREENA  PIC X.                                          00005110
           02 MSCREENC  PIC X.                                          00005120
           02 MSCREENP  PIC X.                                          00005130
           02 MSCREENH  PIC X.                                          00005140
           02 MSCREENV  PIC X.                                          00005150
           02 MSCREENO  PIC X(4).                                       00005160
                                                                                
