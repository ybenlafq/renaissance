      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSP2500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSP2500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSP2500.                                                            
           02  SP25-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  SP25-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  SP25-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  SP25-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  SP25-CMARQ                                                       
               PIC X(0005).                                                     
           02  SP25-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  SP25-WTEBRB                                                      
               PIC X(0005).                                                     
           02  SP25-MTSRP                                                       
               PIC S9(13)V9(0002) COMP-3.                                       
           02  SP25-MTPRMP                                                      
               PIC S9(13)V9(0002) COMP-3.                                       
           02  SP25-MOISTRT                                                     
               PIC X(0006).                                                     
           02  SP25-DPROV                                                       
               PIC X(0008).                                                     
           02  SP25-DAVOIR                                                      
               PIC X(0008).                                                     
           02  SP25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSP2500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSP2500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-WTEBRB-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-WTEBRB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-MTSRP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-MTSRP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-MTPRMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-MTPRMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-MOISTRT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-MOISTRT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-DPROV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-DPROV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-DAVOIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-DAVOIR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
