      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------*             
      *    COMMAREA DU MODULE MPC30                               *             
      *-----------------------------------------------------------*             
       01  COMM-MPC30-APPLI.                                                    
           02 COMM-MPC30-ZONES-ENTREE.                                          
              05 COMM-MPC30-RANDOMNUM          PIC X(19).                       
              05 COMM-MPC30-TYPMVT             PIC X(01).                       
              05 COMM-MPC30-NSOCIETE           PIC X(03).                       
              05 COMM-MPC30-NLIEU              PIC X(03).                       
              05 COMM-MPC30-NVENTE             PIC X(07).                       
              05 COMM-MPC30-NLIGNE             PIC X(02).                       
              05 COMM-MPC30-OPTYPE             PIC X(10).                       
           02 COMM-MPC30-ZONES-SORTIE.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MPC30-CODRET      COMP   PIC  S9(4).                      
      *--                                                                       
              05 COMM-MPC30-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MPC30-NSOCVTE            PIC X(03).                       
              05 COMM-MPC30-NLIEUVTE           PIC X(03).                       
              05 COMM-MPC30-LETTRAGE           PIC X(10).                       
              05 COMM-MPC30-NSOCNET            PIC X(03).                       
              05 COMM-MPC30-NLIEUNET           PIC X(03).                       
              05 COMM-MPC30-NATFACT            PIC X(05).                       
                                                                                
