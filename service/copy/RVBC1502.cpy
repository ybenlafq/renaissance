      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1502                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1502.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1502.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NSOCIETE                                          
           10 BC15-NSOCIETE        PIC X(3).                                    
      *    *************************************************************        
      *                       NLIEU                                             
           10 BC15-NLIEU           PIC X(3).                                    
      *    *************************************************************        
      *                       NVENTE                                            
           10 BC15-NVENTE          PIC X(8).                                    
      *    *************************************************************        
      *                       NCLIENTADR                                        
           10 BC15-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       NFOYERADR                                         
           10 BC15-NFOYERADR       PIC X(8).                                    
      *    *************************************************************        
      *                       DYSYT                                             
           10 BC15-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *     TYPE D'ADRESSE A OU B                                               
           10 BC15-WTYPEADR        PIC X(1).                                    
      *    *************************************************************        
      *     DATE DE LA VENTE                                                    
           10 BC15-DVENTE          PIC X(8).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC1502-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC1502-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-NSOCIETE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-NLIEU-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-NVENTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-NFOYERADR-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-NFOYERADR-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-WTYPEADR-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-WTYPEADR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-WTYPEADR-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-WTYPEADR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC15-DVENTE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC15-DVENTE-F        PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
