      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM075 AU 13/07/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM075.                                                        
            05 NOMETAT-INM075           PIC X(6) VALUE 'INM075'.                
            05 RUPTURES-INM075.                                                 
           10 INM075-NSOCIETE           PIC X(03).                      007  003
           10 INM075-NLIEU              PIC X(03).                      010  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM075-SEQUENCE           PIC S9(04) COMP.                013  002
      *--                                                                       
           10 INM075-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM075.                                                   
           10 INM075-CCOMPTEUR          PIC X(02).                      015  002
           10 INM075-CDEVAUT            PIC X(03).                      017  003
           10 INM075-CDEVRF             PIC X(03).                      020  003
           10 INM075-MOIS               PIC X(07).                      023  007
           10 INM075-NCAISSE            PIC X(03).                      030  003
           10 INM075-NSOCLIEUSAV        PIC X(07).                      033  007
           10 INM075-PMONTANT-AU-CAISSE PIC S9(08)V9(2) COMP-3.         040  006
           10 INM075-PMONTANT-AU-COMPT  PIC S9(08)V9(2) COMP-3.         046  006
           10 INM075-PMONTANT-AU-SAV    PIC S9(08)V9(2) COMP-3.         052  006
           10 INM075-PMONTANT-RF-CAISSE PIC S9(08)V9(2) COMP-3.         058  006
           10 INM075-PMONTANT-RF-COMPT  PIC S9(08)V9(2) COMP-3.         064  006
           10 INM075-PMONTANT-RF-SAV    PIC S9(08)V9(2) COMP-3.         070  006
            05 FILLER                      PIC X(437).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM075-LONG           PIC S9(4)   COMP  VALUE +075.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM075-LONG           PIC S9(4) COMP-5  VALUE +075.           
                                                                                
      *}                                                                        
