      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QCPRE TABLE DES PRESTATION             *        
      *----------------------------------------------------------------*        
       01  RVQCPRE.                                                             
           05  QCPRE-CTABLEG2    PIC X(15).                                     
           05  QCPRE-CTABLEG2-REDEF REDEFINES QCPRE-CTABLEG2.                   
               10  QCPRE-CPRESTA         PIC X(05).                             
           05  QCPRE-WTABLEG     PIC X(80).                                     
           05  QCPRE-WTABLEG-REDEF  REDEFINES QCPRE-WTABLEG.                    
               10  QCPRE-WACTIF          PIC X(01).                             
               10  QCPRE-CTYPSERV        PIC X(05).                             
               10  QCPRE-TCARTE          PIC X(01).                             
               10  QCPRE-LIEU            PIC X(06).                             
               10  QCPRE-CFAM            PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVQCPRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCPRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QCPRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QCPRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QCPRE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
