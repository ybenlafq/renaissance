      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   TABLE RTRB06                                                          
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVRB0600                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVRB0600.                                                            
      *            CODE POSTAL                                                  
           05 RB06-CPOSTAL          PIC X(05).                                  
      *            LIBELLE COMMUNE                                              
           05 RB06-LCOMMUNE         PIC X(32).                                  
      *            NUMERO INSEE                                                 
           05 RB06-CINSEE           PIC X(05).                                  
      *            CODE SAV                                                     
           05 RB06-CSAV             PIC X(06).                                  
      *            CODE PLUG & PLAY                                             
           05 RB06-CPNP             PIC X(06).                                  
      *            CODE SOUS-TRAITANT                                           
           05 RB06-CSST             PIC X(06).                                  
      *            DATE DE DISPONIBILITE                                        
           05 RB06-DTDISPO          PIC X(08).                                  
      *            CODE FILTRE                                                  
           05 RB06-CFILTRE          PIC X(02).                                  
      *            CODE TYPE DE RESEAU                                          
           05 RB06-CTYPRES          PIC X(03).                                  
      *            CODE FREQUENCE RESEAU                                        
           05 RB06-FREQRES          PIC X(06).                                  
      *            NUMERO RESEAU                                                
           05 RB06-NRESEAU          PIC X(05).                                  
      *            DYST                                                         
           05 RB06-DSYST            PIC S9(13) COMP-3.                          
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVRB0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CPOSTAL-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CPOSTAL-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-LCOMMUNE-F       PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-LCOMMUNE-F       PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CINSEE-F         PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CINSEE-F         PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CSAV-F           PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CSAV-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CPNP-F           PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CPNP-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CSST-F           PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CSST-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-DTDISPO-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-DTDISPO-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CFILTRE-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CFILTRE-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-CTYPRES-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-CTYPRES-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-FREQRES-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-FREQRES-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-NRESEAU-F        PIC S9(4) COMP.                             
      *--                                                                       
           05 RB06-NRESEAU-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 RB06-DSYST-F          PIC S9(6) COMP.                             
      *                                                                         
      *--                                                                       
           05 RB06-DSYST-F          PIC S9(6) COMP-5.                           
                                                                                
      *}                                                                        
