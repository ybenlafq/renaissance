      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * LISTE DES ECS: ANCIENS NUMEROS                                  00000020
      ***************************************************************** 00000030
       01   EFX07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOECSL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNOECSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOECSF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNOECSI   PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBECSL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLIBECSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBECSF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBECSI  PIC X(25).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSENSL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSENSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSENSF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSENSI   PIC X.                                          00000270
           02 MCRITD OCCURS   3 TIMES .                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRITL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MCRITL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCRITF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MCRITI  PIC X(5).                                       00000320
           02 MLCRITD OCCURS   3 TIMES .                                00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCRITL      COMP PIC S9(4).                            00000340
      *--                                                                       
             03 MLCRITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCRITF      PIC X.                                     00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MLCRITI      PIC X(13).                                 00000370
           02 MLIGNEI OCCURS   12 TIMES .                               00000380
             03 MCTECGD OCCURS   2 TIMES .                              00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCTECGL    COMP PIC S9(4).                            00000400
      *--                                                                       
               04 MCTECGL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MCTECGF    PIC X.                                     00000410
               04 FILLER     PIC X(4).                                  00000420
               04 MCTECGI    PIC X(6).                                  00000430
             03 MSECTIOND OCCURS   2 TIMES .                            00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSECTIONL  COMP PIC S9(4).                            00000450
      *--                                                                       
               04 MSECTIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MSECTIONF  PIC X.                                     00000460
               04 FILLER     PIC X(4).                                  00000470
               04 MSECTIONI  PIC X(6).                                  00000480
             03 MRUBRD OCCURS   2 TIMES .                               00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MRUBRL     COMP PIC S9(4).                            00000500
      *--                                                                       
               04 MRUBRL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MRUBRF     PIC X.                                     00000510
               04 FILLER     PIC X(4).                                  00000520
               04 MRUBRI     PIC X(6).                                  00000530
             03 MDEFFETD OCCURS   2 TIMES .                             00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDEFFETL   COMP PIC S9(4).                            00000550
      *--                                                                       
               04 MDEFFETL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDEFFETF   PIC X.                                     00000560
               04 FILLER     PIC X(4).                                  00000570
               04 MDEFFETI   PIC X(10).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(78).                                      00000620
      * CODE TRANSACTION                                                00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCODTRAI  PIC X(4).                                       00000670
      * ZONE MESSAGE                                                    00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MZONCMDI  PIC X(15).                                      00000720
      * NOM DU CICS                                                     00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCICSI    PIC X(5).                                       00000770
      * NOM LIGNE VTAM                                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      * CODE TERMINAL                                                   00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MSCREENI  PIC X(4).                                       00000870
      ***************************************************************** 00000880
      * LISTE DES ECS: ANCIENS NUMEROS                                  00000890
      ***************************************************************** 00000900
       01   EFX07O REDEFINES EFX07I.                                    00000910
           02 FILLER    PIC X(12).                                      00000920
      * DATE JOUR TRAITEMENT                                            00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MDATJOUA  PIC X.                                          00000950
           02 MDATJOUC  PIC X.                                          00000960
           02 MDATJOUP  PIC X.                                          00000970
           02 MDATJOUH  PIC X.                                          00000980
           02 MDATJOUV  PIC X.                                          00000990
           02 MDATJOUO  PIC X(10).                                      00001000
      * HEURE TRAITEMENT                                                00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTIMJOUA  PIC X.                                          00001030
           02 MTIMJOUC  PIC X.                                          00001040
           02 MTIMJOUP  PIC X.                                          00001050
           02 MTIMJOUH  PIC X.                                          00001060
           02 MTIMJOUV  PIC X.                                          00001070
           02 MTIMJOUO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNOECSA   PIC X.                                          00001100
           02 MNOECSC   PIC X.                                          00001110
           02 MNOECSP   PIC X.                                          00001120
           02 MNOECSH   PIC X.                                          00001130
           02 MNOECSV   PIC X.                                          00001140
           02 MNOECSO   PIC X(5).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLIBECSA  PIC X.                                          00001170
           02 MLIBECSC  PIC X.                                          00001180
           02 MLIBECSP  PIC X.                                          00001190
           02 MLIBECSH  PIC X.                                          00001200
           02 MLIBECSV  PIC X.                                          00001210
           02 MLIBECSO  PIC X(25).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNSENSA   PIC X.                                          00001240
           02 MNSENSC   PIC X.                                          00001250
           02 MNSENSP   PIC X.                                          00001260
           02 MNSENSH   PIC X.                                          00001270
           02 MNSENSV   PIC X.                                          00001280
           02 MNSENSO   PIC X.                                          00001290
           02 DFHMS1 OCCURS   3 TIMES .                                 00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MCRITA  PIC X.                                          00001320
             03 MCRITC  PIC X.                                          00001330
             03 MCRITP  PIC X.                                          00001340
             03 MCRITH  PIC X.                                          00001350
             03 MCRITV  PIC X.                                          00001360
             03 MCRITO  PIC X(5).                                       00001370
           02 DFHMS2 OCCURS   3 TIMES .                                 00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MLCRITA      PIC X.                                     00001400
             03 MLCRITC PIC X.                                          00001410
             03 MLCRITP PIC X.                                          00001420
             03 MLCRITH PIC X.                                          00001430
             03 MLCRITV PIC X.                                          00001440
             03 MLCRITO      PIC X(13).                                 00001450
           02 MLIGNEO OCCURS   12 TIMES .                               00001460
             03 DFHMS3 OCCURS   2 TIMES .                               00001470
               04 FILLER     PIC X(2).                                  00001480
               04 MCTECGA    PIC X.                                     00001490
               04 MCTECGC    PIC X.                                     00001500
               04 MCTECGP    PIC X.                                     00001510
               04 MCTECGH    PIC X.                                     00001520
               04 MCTECGV    PIC X.                                     00001530
               04 MCTECGO    PIC X(6).                                  00001540
             03 DFHMS4 OCCURS   2 TIMES .                               00001550
               04 FILLER     PIC X(2).                                  00001560
               04 MSECTIONA  PIC X.                                     00001570
               04 MSECTIONC  PIC X.                                     00001580
               04 MSECTIONP  PIC X.                                     00001590
               04 MSECTIONH  PIC X.                                     00001600
               04 MSECTIONV  PIC X.                                     00001610
               04 MSECTIONO  PIC X(6).                                  00001620
             03 DFHMS5 OCCURS   2 TIMES .                               00001630
               04 FILLER     PIC X(2).                                  00001640
               04 MRUBRA     PIC X.                                     00001650
               04 MRUBRC     PIC X.                                     00001660
               04 MRUBRP     PIC X.                                     00001670
               04 MRUBRH     PIC X.                                     00001680
               04 MRUBRV     PIC X.                                     00001690
               04 MRUBRO     PIC X(6).                                  00001700
             03 DFHMS6 OCCURS   2 TIMES .                               00001710
               04 FILLER     PIC X(2).                                  00001720
               04 MDEFFETA   PIC X.                                     00001730
               04 MDEFFETC   PIC X.                                     00001740
               04 MDEFFETP   PIC X.                                     00001750
               04 MDEFFETH   PIC X.                                     00001760
               04 MDEFFETV   PIC X.                                     00001770
               04 MDEFFETO   PIC X(10).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MLIBERRA  PIC X.                                          00001800
           02 MLIBERRC  PIC X.                                          00001810
           02 MLIBERRP  PIC X.                                          00001820
           02 MLIBERRH  PIC X.                                          00001830
           02 MLIBERRV  PIC X.                                          00001840
           02 MLIBERRO  PIC X(78).                                      00001850
      * CODE TRANSACTION                                                00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
      * ZONE MESSAGE                                                    00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MZONCMDA  PIC X.                                          00001960
           02 MZONCMDC  PIC X.                                          00001970
           02 MZONCMDP  PIC X.                                          00001980
           02 MZONCMDH  PIC X.                                          00001990
           02 MZONCMDV  PIC X.                                          00002000
           02 MZONCMDO  PIC X(15).                                      00002010
      * NOM DU CICS                                                     00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCICSA    PIC X.                                          00002040
           02 MCICSC    PIC X.                                          00002050
           02 MCICSP    PIC X.                                          00002060
           02 MCICSH    PIC X.                                          00002070
           02 MCICSV    PIC X.                                          00002080
           02 MCICSO    PIC X(5).                                       00002090
      * NOM LIGNE VTAM                                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNETNAMA  PIC X.                                          00002120
           02 MNETNAMC  PIC X.                                          00002130
           02 MNETNAMP  PIC X.                                          00002140
           02 MNETNAMH  PIC X.                                          00002150
           02 MNETNAMV  PIC X.                                          00002160
           02 MNETNAMO  PIC X(8).                                       00002170
      * CODE TERMINAL                                                   00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MSCREENA  PIC X.                                          00002200
           02 MSCREENC  PIC X.                                          00002210
           02 MSCREENP  PIC X.                                          00002220
           02 MSCREENH  PIC X.                                          00002230
           02 MSCREENV  PIC X.                                          00002240
           02 MSCREENO  PIC X(4).                                       00002250
                                                                                
