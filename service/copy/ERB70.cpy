      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RBX : Menu Ctl encodage fourniss                                00000020
      ***************************************************************** 00000030
       01   ERB70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCIETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIETF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCIETI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDATDEBI  PIC X(10).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATFINI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTIERSI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLNOMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNENVOII  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTRAITI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWECARTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWECARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWECARTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWECARTI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNUMSERL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MWNUMSERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWNUMSERF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWNUMSERI      PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCORRECL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MWCORRECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWCORRECF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWCORRECI      PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPRODPLL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MWPRODPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWPRODPLF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWPRODPLI      PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNSERIEI  PIC X(16).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNCODICI  PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBERRI  PIC X(73).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCODTRAI  PIC X(4).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCICSI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNETNAMI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(4).                                       00000850
      ***************************************************************** 00000860
      * RBX : Menu Ctl encodage fourniss                                00000870
      ***************************************************************** 00000880
       01   ERB70O REDEFINES ERB70I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MDATJOUA  PIC X.                                          00000920
           02 MDATJOUC  PIC X.                                          00000930
           02 MDATJOUP  PIC X.                                          00000940
           02 MDATJOUH  PIC X.                                          00000950
           02 MDATJOUV  PIC X.                                          00000960
           02 MDATJOUO  PIC X(10).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTIMJOUA  PIC X.                                          00000990
           02 MTIMJOUC  PIC X.                                          00001000
           02 MTIMJOUP  PIC X.                                          00001010
           02 MTIMJOUH  PIC X.                                          00001020
           02 MTIMJOUV  PIC X.                                          00001030
           02 MTIMJOUO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MNSOCIETA      PIC X.                                     00001060
           02 MNSOCIETC PIC X.                                          00001070
           02 MNSOCIETP PIC X.                                          00001080
           02 MNSOCIETH PIC X.                                          00001090
           02 MNSOCIETV PIC X.                                          00001100
           02 MNSOCIETO      PIC X(3).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATDEBA  PIC X.                                          00001130
           02 MDATDEBC  PIC X.                                          00001140
           02 MDATDEBP  PIC X.                                          00001150
           02 MDATDEBH  PIC X.                                          00001160
           02 MDATDEBV  PIC X.                                          00001170
           02 MDATDEBO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MDATFINA  PIC X.                                          00001200
           02 MDATFINC  PIC X.                                          00001210
           02 MDATFINP  PIC X.                                          00001220
           02 MDATFINH  PIC X.                                          00001230
           02 MDATFINV  PIC X.                                          00001240
           02 MDATFINO  PIC X(10).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCTIERSA  PIC X.                                          00001270
           02 MCTIERSC  PIC X.                                          00001280
           02 MCTIERSP  PIC X.                                          00001290
           02 MCTIERSH  PIC X.                                          00001300
           02 MCTIERSV  PIC X.                                          00001310
           02 MCTIERSO  PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MLNOMA    PIC X.                                          00001340
           02 MLNOMC    PIC X.                                          00001350
           02 MLNOMP    PIC X.                                          00001360
           02 MLNOMH    PIC X.                                          00001370
           02 MLNOMV    PIC X.                                          00001380
           02 MLNOMO    PIC X(20).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNENVOIA  PIC X.                                          00001410
           02 MNENVOIC  PIC X.                                          00001420
           02 MNENVOIP  PIC X.                                          00001430
           02 MNENVOIH  PIC X.                                          00001440
           02 MNENVOIV  PIC X.                                          00001450
           02 MNENVOIO  PIC X(7).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCTRAITA  PIC X.                                          00001480
           02 MCTRAITC  PIC X.                                          00001490
           02 MCTRAITP  PIC X.                                          00001500
           02 MCTRAITH  PIC X.                                          00001510
           02 MCTRAITV  PIC X.                                          00001520
           02 MCTRAITO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MWECARTA  PIC X.                                          00001550
           02 MWECARTC  PIC X.                                          00001560
           02 MWECARTP  PIC X.                                          00001570
           02 MWECARTH  PIC X.                                          00001580
           02 MWECARTV  PIC X.                                          00001590
           02 MWECARTO  PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MWNUMSERA      PIC X.                                     00001620
           02 MWNUMSERC PIC X.                                          00001630
           02 MWNUMSERP PIC X.                                          00001640
           02 MWNUMSERH PIC X.                                          00001650
           02 MWNUMSERV PIC X.                                          00001660
           02 MWNUMSERO      PIC X.                                     00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MWCORRECA      PIC X.                                     00001690
           02 MWCORRECC PIC X.                                          00001700
           02 MWCORRECP PIC X.                                          00001710
           02 MWCORRECH PIC X.                                          00001720
           02 MWCORRECV PIC X.                                          00001730
           02 MWCORRECO      PIC X.                                     00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MWPRODPLA      PIC X.                                     00001760
           02 MWPRODPLC PIC X.                                          00001770
           02 MWPRODPLP PIC X.                                          00001780
           02 MWPRODPLH PIC X.                                          00001790
           02 MWPRODPLV PIC X.                                          00001800
           02 MWPRODPLO      PIC X.                                     00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNSERIEA  PIC X.                                          00001830
           02 MNSERIEC  PIC X.                                          00001840
           02 MNSERIEP  PIC X.                                          00001850
           02 MNSERIEH  PIC X.                                          00001860
           02 MNSERIEV  PIC X.                                          00001870
           02 MNSERIEO  PIC X(16).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNCODICA  PIC X.                                          00001900
           02 MNCODICC  PIC X.                                          00001910
           02 MNCODICP  PIC X.                                          00001920
           02 MNCODICH  PIC X.                                          00001930
           02 MNCODICV  PIC X.                                          00001940
           02 MNCODICO  PIC X(7).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLIBERRA  PIC X.                                          00001970
           02 MLIBERRC  PIC X.                                          00001980
           02 MLIBERRP  PIC X.                                          00001990
           02 MLIBERRH  PIC X.                                          00002000
           02 MLIBERRV  PIC X.                                          00002010
           02 MLIBERRO  PIC X(73).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCODTRAA  PIC X.                                          00002040
           02 MCODTRAC  PIC X.                                          00002050
           02 MCODTRAP  PIC X.                                          00002060
           02 MCODTRAH  PIC X.                                          00002070
           02 MCODTRAV  PIC X.                                          00002080
           02 MCODTRAO  PIC X(4).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSP    PIC X.                                          00002130
           02 MCICSH    PIC X.                                          00002140
           02 MCICSV    PIC X.                                          00002150
           02 MCICSO    PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNETNAMA  PIC X.                                          00002180
           02 MNETNAMC  PIC X.                                          00002190
           02 MNETNAMP  PIC X.                                          00002200
           02 MNETNAMH  PIC X.                                          00002210
           02 MNETNAMV  PIC X.                                          00002220
           02 MNETNAMO  PIC X(8).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MSCREENA  PIC X.                                          00002250
           02 MSCREENC  PIC X.                                          00002260
           02 MSCREENP  PIC X.                                          00002270
           02 MSCREENH  PIC X.                                          00002280
           02 MSCREENV  PIC X.                                          00002290
           02 MSCREENO  PIC X(4).                                       00002300
                                                                                
