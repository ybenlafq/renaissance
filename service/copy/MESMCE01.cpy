      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                            DABORD LA COPY DE L"ENTETE MESSMQ  *         
      *                                                               *         
      *   COPY MESSAGES MQ ENTRE LES PROGRAMMES :                     *         
      *   - MNV38  : MISE A JOUR DES PRIX INNOVENTE                   *         
      *   - MCE01  : ENVOI DES MISE A JOUR DE PRIX AU AS400           *         
      *                                                               *         
      *   LONGUEUR : 7 + ( 6 * 500 ) -> 3007                          *         
      *****************************************************************         
      *                                                                         
              05 MESMCE01-DATA.                                                 
                 10 MESMCE01-NCODIC      PIC X(07).                             
                 10 MESMCE01-MAG OCCURS 500.                                    
                    15 MESMCE01-NSOC     PIC X(03).                             
                    15 MESMCE01-NLIEU    PIC X(03).                             
      *                                                                         
                                                                                
