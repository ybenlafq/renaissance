      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RBX: Menu gestion ferraillage                                   00000020
      ***************************************************************** 00000030
       01   ERB80I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X.                                          00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNFOURNI  PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEQUIPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MTEQUIPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTEQUIPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MTEQUIPI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATDEBI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATFINI  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIXL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCHOIXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHOIXF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCHOIXI   PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTENVL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MQTENVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTENVF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MQTENVI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFORCAGL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MFORCAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFORCAGF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFORCAGI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNCDE1I   PIC X(14).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFACTL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNFACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNFACTF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNFACTI   PIC X(14).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE2L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNCDE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE2F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNCDE2I   PIC X(14).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE3L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNCDE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE3F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCDE3I   PIC X(14).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(73).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODTRAI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCICSI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNETNAMI  PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSCREENI  PIC X(4).                                       00000810
      ***************************************************************** 00000820
      * RBX: Menu gestion ferraillage                                   00000830
      ***************************************************************** 00000840
       01   ERB80O REDEFINES ERB80I.                                    00000850
           02 FILLER    PIC X(12).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MDATJOUA  PIC X.                                          00000880
           02 MDATJOUC  PIC X.                                          00000890
           02 MDATJOUP  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUV  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MTIMJOUA  PIC X.                                          00000950
           02 MTIMJOUC  PIC X.                                          00000960
           02 MTIMJOUP  PIC X.                                          00000970
           02 MTIMJOUH  PIC X.                                          00000980
           02 MTIMJOUV  PIC X.                                          00000990
           02 MTIMJOUO  PIC X(5).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MZONCMDA  PIC X.                                          00001020
           02 MZONCMDC  PIC X.                                          00001030
           02 MZONCMDP  PIC X.                                          00001040
           02 MZONCMDH  PIC X.                                          00001050
           02 MZONCMDV  PIC X.                                          00001060
           02 MZONCMDO  PIC X.                                          00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNFOURNA  PIC X.                                          00001090
           02 MNFOURNC  PIC X.                                          00001100
           02 MNFOURNP  PIC X.                                          00001110
           02 MNFOURNH  PIC X.                                          00001120
           02 MNFOURNV  PIC X.                                          00001130
           02 MNFOURNO  PIC X(20).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTEQUIPA  PIC X.                                          00001160
           02 MTEQUIPC  PIC X.                                          00001170
           02 MTEQUIPP  PIC X.                                          00001180
           02 MTEQUIPH  PIC X.                                          00001190
           02 MTEQUIPV  PIC X.                                          00001200
           02 MTEQUIPO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MDATDEBA  PIC X.                                          00001230
           02 MDATDEBC  PIC X.                                          00001240
           02 MDATDEBP  PIC X.                                          00001250
           02 MDATDEBH  PIC X.                                          00001260
           02 MDATDEBV  PIC X.                                          00001270
           02 MDATDEBO  PIC X(10).                                      00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MDATFINA  PIC X.                                          00001300
           02 MDATFINC  PIC X.                                          00001310
           02 MDATFINP  PIC X.                                          00001320
           02 MDATFINH  PIC X.                                          00001330
           02 MDATFINV  PIC X.                                          00001340
           02 MDATFINO  PIC X(10).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCHOIXA   PIC X.                                          00001370
           02 MCHOIXC   PIC X.                                          00001380
           02 MCHOIXP   PIC X.                                          00001390
           02 MCHOIXH   PIC X.                                          00001400
           02 MCHOIXV   PIC X.                                          00001410
           02 MCHOIXO   PIC X.                                          00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MQTENVA   PIC X.                                          00001440
           02 MQTENVC   PIC X.                                          00001450
           02 MQTENVP   PIC X.                                          00001460
           02 MQTENVH   PIC X.                                          00001470
           02 MQTENVV   PIC X.                                          00001480
           02 MQTENVO   PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MFORCAGA  PIC X.                                          00001510
           02 MFORCAGC  PIC X.                                          00001520
           02 MFORCAGP  PIC X.                                          00001530
           02 MFORCAGH  PIC X.                                          00001540
           02 MFORCAGV  PIC X.                                          00001550
           02 MFORCAGO  PIC X.                                          00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNCDE1A   PIC X.                                          00001580
           02 MNCDE1C   PIC X.                                          00001590
           02 MNCDE1P   PIC X.                                          00001600
           02 MNCDE1H   PIC X.                                          00001610
           02 MNCDE1V   PIC X.                                          00001620
           02 MNCDE1O   PIC X(14).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNFACTA   PIC X.                                          00001650
           02 MNFACTC   PIC X.                                          00001660
           02 MNFACTP   PIC X.                                          00001670
           02 MNFACTH   PIC X.                                          00001680
           02 MNFACTV   PIC X.                                          00001690
           02 MNFACTO   PIC X(14).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNCDE2A   PIC X.                                          00001720
           02 MNCDE2C   PIC X.                                          00001730
           02 MNCDE2P   PIC X.                                          00001740
           02 MNCDE2H   PIC X.                                          00001750
           02 MNCDE2V   PIC X.                                          00001760
           02 MNCDE2O   PIC X(14).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNCDE3A   PIC X.                                          00001790
           02 MNCDE3C   PIC X.                                          00001800
           02 MNCDE3P   PIC X.                                          00001810
           02 MNCDE3H   PIC X.                                          00001820
           02 MNCDE3V   PIC X.                                          00001830
           02 MNCDE3O   PIC X(14).                                      00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLIBERRA  PIC X.                                          00001860
           02 MLIBERRC  PIC X.                                          00001870
           02 MLIBERRP  PIC X.                                          00001880
           02 MLIBERRH  PIC X.                                          00001890
           02 MLIBERRV  PIC X.                                          00001900
           02 MLIBERRO  PIC X(73).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCODTRAA  PIC X.                                          00001930
           02 MCODTRAC  PIC X.                                          00001940
           02 MCODTRAP  PIC X.                                          00001950
           02 MCODTRAH  PIC X.                                          00001960
           02 MCODTRAV  PIC X.                                          00001970
           02 MCODTRAO  PIC X(4).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCICSA    PIC X.                                          00002000
           02 MCICSC    PIC X.                                          00002010
           02 MCICSP    PIC X.                                          00002020
           02 MCICSH    PIC X.                                          00002030
           02 MCICSV    PIC X.                                          00002040
           02 MCICSO    PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNETNAMA  PIC X.                                          00002070
           02 MNETNAMC  PIC X.                                          00002080
           02 MNETNAMP  PIC X.                                          00002090
           02 MNETNAMH  PIC X.                                          00002100
           02 MNETNAMV  PIC X.                                          00002110
           02 MNETNAMO  PIC X(8).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MSCREENA  PIC X.                                          00002140
           02 MSCREENC  PIC X.                                          00002150
           02 MSCREENP  PIC X.                                          00002160
           02 MSCREENH  PIC X.                                          00002170
           02 MSCREENV  PIC X.                                          00002180
           02 MSCREENO  PIC X(4).                                       00002190
                                                                                
