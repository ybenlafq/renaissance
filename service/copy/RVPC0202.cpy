      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPC0202                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPC0202                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0202.                                                            
           02  PC02-RANDOMNUM                                                   
               PIC X(0019).                                                     
           02  PC02-OPTYPE                                                      
               PIC X(0010).                                                     
           02  PC02-CANAL                                                       
               PIC X(0005).                                                     
           02  PC02-CUTIL                                                       
               PIC X(0020).                                                     
           02  PC02-STATUT                                                      
               PIC X(0005).                                                     
           02  PC02-NSOCVTE                                                     
               PIC X(0003).                                                     
           02  PC02-NLIEUVTE                                                    
               PIC X(0003).                                                     
           02  PC02-NVENTE                                                      
               PIC X(0007).                                                     
           02  PC02-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  PC02-CPREST                                                      
               PIC X(0005).                                                     
           02  PC02-NSOC                                                        
               PIC X(0003).                                                     
           02  PC02-NLIEU                                                       
               PIC X(0003).                                                     
           02  PC02-NTRANS                                                      
               PIC X(0010).                                                     
           02  PC02-NCAISSE                                                     
               PIC X(0003).                                                     
           02  PC02-DDARTY                                                      
               PIC X(0008).                                                     
           02  PC02-HDARTY                                                      
               PIC X(0006).                                                     
           02  PC02-CDEV                                                        
               PIC X(0004).                                                     
           02  PC02-NACCORD                                                     
               PIC X(0010).                                                     
           02  PC02-DACCORD                                                     
               PIC X(0008).                                                     
           02  PC02-HACCORD                                                     
               PIC X(0006).                                                     
           02  PC02-MNTACCORD                                                   
               PIC S9(7)V9(2) COMP-3.                                           
           02  PC02-SOLDE                                                       
               PIC S9(7)V9(2) COMP-3.                                           
           02  PC02-MNTDARTY                                                    
               PIC S9(7)V9(2) COMP-3.                                           
           02  PC02-MNTEUROS                                                    
               PIC S9(7)V9(2) COMP-3.                                           
           02  PC02-WNETTING                                                    
               PIC X(0001).                                                     
           02  PC02-WDEVCCA                                                     
               PIC X(0001).                                                     
           02  PC02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  PC02-NSOCP                                                       
               PIC X(0003).                                                     
           02  PC02-NLIEUP                                                      
               PIC X(0003).                                                     
           02  PC02-NTRANSP                                                     
               PIC X(0008).                                                     
           02  PC02-DCOMPTA                                                     
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPC0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0202-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-RANDOMNUM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-RANDOMNUM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-OPTYPE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-OPTYPE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-CANAL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-CANAL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-CUTIL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-CUTIL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-STATUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-STATUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NSOCVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NSOCVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NLIEUVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NLIEUVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-CPREST-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-CPREST-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-DDARTY-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-DDARTY-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-HDARTY-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-HDARTY-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-DACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-DACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-HACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-HACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-MNTACCORD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-MNTACCORD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-SOLDE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-SOLDE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-MNTDARTY-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-MNTDARTY-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-MNTEUROS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-MNTEUROS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-WNETTING-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-WNETTING-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-WDEVCCA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-WDEVCCA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC02-NTRANSP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC02-NTRANSP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
