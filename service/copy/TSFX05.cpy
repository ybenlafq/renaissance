      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TFX10                                        *            
      *       TR : FX00  INTERFACE COMPTABLE                       *            
      *       PG : TFX10 MAJ DES FONDS DE CAISSE COMPTABLE         *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES -----------------------------              
      *                                                                         
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 110.                       
       01  TS-DONNEES.                                                          
           05 TS-LIGNE   OCCURS 10.                                             
              10  TS-NLIEU             PIC X(3).                                
              10  TS-NUMCAIS           PIC X(3).                                
              10  TS-MTFOND            PIC S9(7)V99   COMP-3.                   
                                                                                
