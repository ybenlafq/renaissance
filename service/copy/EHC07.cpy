      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC07   EHC07                                              00000020
      ***************************************************************** 00000030
       01   EHC07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUHCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUHCL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUHCF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUHCI      PIC X(20).                                 00000290
           02 MREPI OCCURS   12 TIMES .                                 00000300
             03 MSELD OCCURS   2 TIMES .                                00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELL      COMP PIC S9(4).                            00000320
      *--                                                                       
               04 MSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MSELF      PIC X.                                     00000330
               04 FILLER     PIC X(4).                                  00000340
               04 MSELI      PIC X.                                     00000350
             03 MCTENVOID OCCURS   2 TIMES .                            00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCTENVOIL  COMP PIC S9(4).                            00000370
      *--                                                                       
               04 MCTENVOIL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MCTENVOIF  PIC X.                                     00000380
               04 FILLER     PIC X(4).                                  00000390
               04 MCTENVOII  PIC X.                                     00000400
             03 MCTIERSD OCCURS   2 TIMES .                             00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCTIERSL   COMP PIC S9(4).                            00000420
      *--                                                                       
               04 MCTIERSL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCTIERSF   PIC X.                                     00000430
               04 FILLER     PIC X(4).                                  00000440
               04 MCTIERSI   PIC X(5).                                  00000450
             03 MDENVOID OCCURS   2 TIMES .                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDENVOIL   COMP PIC S9(4).                            00000470
      *--                                                                       
               04 MDENVOIL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDENVOIF   PIC X.                                     00000480
               04 FILLER     PIC X(4).                                  00000490
               04 MDENVOII   PIC X(10).                                 00000500
             03 MNENVOID OCCURS   2 TIMES .                             00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNENVOIL   COMP PIC S9(4).                            00000520
      *--                                                                       
               04 MNENVOIL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNENVOIF   PIC X.                                     00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MNENVOII   PIC X(7).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(78).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: EHC07   EHC07                                              00000770
      ***************************************************************** 00000780
       01   EHC07O REDEFINES EHC07I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MPAGEA    PIC X.                                          00000960
           02 MPAGEC    PIC X.                                          00000970
           02 MPAGEP    PIC X.                                          00000980
           02 MPAGEH    PIC X.                                          00000990
           02 MPAGEV    PIC X.                                          00001000
           02 MPAGEO    PIC X(2).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGETOTA      PIC X.                                     00001030
           02 MPAGETOTC PIC X.                                          00001040
           02 MPAGETOTP PIC X.                                          00001050
           02 MPAGETOTH PIC X.                                          00001060
           02 MPAGETOTV PIC X.                                          00001070
           02 MPAGETOTO      PIC X(2).                                  00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNLIEUHCA      PIC X.                                     00001100
           02 MNLIEUHCC PIC X.                                          00001110
           02 MNLIEUHCP PIC X.                                          00001120
           02 MNLIEUHCH PIC X.                                          00001130
           02 MNLIEUHCV PIC X.                                          00001140
           02 MNLIEUHCO      PIC X(3).                                  00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLLIEUHCA      PIC X.                                     00001170
           02 MLLIEUHCC PIC X.                                          00001180
           02 MLLIEUHCP PIC X.                                          00001190
           02 MLLIEUHCH PIC X.                                          00001200
           02 MLLIEUHCV PIC X.                                          00001210
           02 MLLIEUHCO      PIC X(20).                                 00001220
           02 MREPO OCCURS   12 TIMES .                                 00001230
             03 DFHMS1 OCCURS   2 TIMES .                               00001240
               04 FILLER     PIC X(2).                                  00001250
               04 MSELA      PIC X.                                     00001260
               04 MSELC PIC X.                                          00001270
               04 MSELP PIC X.                                          00001280
               04 MSELH PIC X.                                          00001290
               04 MSELV PIC X.                                          00001300
               04 MSELO      PIC X.                                     00001310
             03 DFHMS2 OCCURS   2 TIMES .                               00001320
               04 FILLER     PIC X(2).                                  00001330
               04 MCTENVOIA  PIC X.                                     00001340
               04 MCTENVOIC  PIC X.                                     00001350
               04 MCTENVOIP  PIC X.                                     00001360
               04 MCTENVOIH  PIC X.                                     00001370
               04 MCTENVOIV  PIC X.                                     00001380
               04 MCTENVOIO  PIC X.                                     00001390
             03 DFHMS3 OCCURS   2 TIMES .                               00001400
               04 FILLER     PIC X(2).                                  00001410
               04 MCTIERSA   PIC X.                                     00001420
               04 MCTIERSC   PIC X.                                     00001430
               04 MCTIERSP   PIC X.                                     00001440
               04 MCTIERSH   PIC X.                                     00001450
               04 MCTIERSV   PIC X.                                     00001460
               04 MCTIERSO   PIC X(5).                                  00001470
             03 DFHMS4 OCCURS   2 TIMES .                               00001480
               04 FILLER     PIC X(2).                                  00001490
               04 MDENVOIA   PIC X.                                     00001500
               04 MDENVOIC   PIC X.                                     00001510
               04 MDENVOIP   PIC X.                                     00001520
               04 MDENVOIH   PIC X.                                     00001530
               04 MDENVOIV   PIC X.                                     00001540
               04 MDENVOIO   PIC X(10).                                 00001550
             03 DFHMS5 OCCURS   2 TIMES .                               00001560
               04 FILLER     PIC X(2).                                  00001570
               04 MNENVOIA   PIC X.                                     00001580
               04 MNENVOIC   PIC X.                                     00001590
               04 MNENVOIP   PIC X.                                     00001600
               04 MNENVOIH   PIC X.                                     00001610
               04 MNENVOIV   PIC X.                                     00001620
               04 MNENVOIO   PIC X(7).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBERRA  PIC X.                                          00001650
           02 MLIBERRC  PIC X.                                          00001660
           02 MLIBERRP  PIC X.                                          00001670
           02 MLIBERRH  PIC X.                                          00001680
           02 MLIBERRV  PIC X.                                          00001690
           02 MLIBERRO  PIC X(78).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCODTRAA  PIC X.                                          00001720
           02 MCODTRAC  PIC X.                                          00001730
           02 MCODTRAP  PIC X.                                          00001740
           02 MCODTRAH  PIC X.                                          00001750
           02 MCODTRAV  PIC X.                                          00001760
           02 MCODTRAO  PIC X(4).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCICSA    PIC X.                                          00001790
           02 MCICSC    PIC X.                                          00001800
           02 MCICSP    PIC X.                                          00001810
           02 MCICSH    PIC X.                                          00001820
           02 MCICSV    PIC X.                                          00001830
           02 MCICSO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNETNAMA  PIC X.                                          00001860
           02 MNETNAMC  PIC X.                                          00001870
           02 MNETNAMP  PIC X.                                          00001880
           02 MNETNAMH  PIC X.                                          00001890
           02 MNETNAMV  PIC X.                                          00001900
           02 MNETNAMO  PIC X(8).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MSCREENA  PIC X.                                          00001930
           02 MSCREENC  PIC X.                                          00001940
           02 MSCREENP  PIC X.                                          00001950
           02 MSCREENH  PIC X.                                          00001960
           02 MSCREENV  PIC X.                                          00001970
           02 MSCREENO  PIC X(4).                                       00001980
                                                                                
