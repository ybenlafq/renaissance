      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: consult detail palettes                                    00000020
      ***************************************************************** 00000030
       01   ERB25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBLF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBLI     PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBNRECEPTL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNBNRECEPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNBNRECEPTF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNBNRECEPTI    PIC X(5).                                  00000290
           02 MTABLGRPI OCCURS   10 TIMES .                             00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLOTL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNLOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNLOTF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNLOTI  PIC X(15).                                      00000340
      * flag non receptionne                                            00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWNRECEPTL   COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MWNRECEPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWNRECEPTF   PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MWNRECEPTI   PIC X.                                     00000390
      * flag acceptee                                                   00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACCEPTL    COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MWACCEPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWACCEPTF    PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MWACCEPTI    PIC X.                                     00000440
      * flag refusee                                                    00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWREFUSEL    COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MWREFUSEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWREFUSEF    PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MWREFUSEI    PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(78).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCICSI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(4).                                       00000690
      ***************************************************************** 00000700
      * Rbx: consult detail palettes                                    00000710
      ***************************************************************** 00000720
       01   ERB25O REDEFINES ERB25I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTIMJOUA  PIC X.                                          00000830
           02 MTIMJOUC  PIC X.                                          00000840
           02 MTIMJOUP  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUV  PIC X.                                          00000870
           02 MTIMJOUO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 NPAGEA    PIC X.                                          00000900
           02 NPAGEC    PIC X.                                          00000910
           02 NPAGEP    PIC X.                                          00000920
           02 NPAGEH    PIC X.                                          00000930
           02 NPAGEV    PIC X.                                          00000940
           02 NPAGEO    PIC X(3).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 NBPAGEA   PIC X.                                          00000970
           02 NBPAGEC   PIC X.                                          00000980
           02 NBPAGEP   PIC X.                                          00000990
           02 NBPAGEH   PIC X.                                          00001000
           02 NBPAGEV   PIC X.                                          00001010
           02 NBPAGEO   PIC X(3).                                       00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MNBLA     PIC X.                                          00001040
           02 MNBLC     PIC X.                                          00001050
           02 MNBLP     PIC X.                                          00001060
           02 MNBLH     PIC X.                                          00001070
           02 MNBLV     PIC X.                                          00001080
           02 MNBLO     PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNBNRECEPTA    PIC X.                                     00001110
           02 MNBNRECEPTC    PIC X.                                     00001120
           02 MNBNRECEPTP    PIC X.                                     00001130
           02 MNBNRECEPTH    PIC X.                                     00001140
           02 MNBNRECEPTV    PIC X.                                     00001150
           02 MNBNRECEPTO    PIC X(5).                                  00001160
           02 MTABLGRPO OCCURS   10 TIMES .                             00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MNLOTA  PIC X.                                          00001190
             03 MNLOTC  PIC X.                                          00001200
             03 MNLOTP  PIC X.                                          00001210
             03 MNLOTH  PIC X.                                          00001220
             03 MNLOTV  PIC X.                                          00001230
             03 MNLOTO  PIC X(15).                                      00001240
      * flag non receptionne                                            00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MWNRECEPTA   PIC X.                                     00001270
             03 MWNRECEPTC   PIC X.                                     00001280
             03 MWNRECEPTP   PIC X.                                     00001290
             03 MWNRECEPTH   PIC X.                                     00001300
             03 MWNRECEPTV   PIC X.                                     00001310
             03 MWNRECEPTO   PIC X.                                     00001320
      * flag acceptee                                                   00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MWACCEPTA    PIC X.                                     00001350
             03 MWACCEPTC    PIC X.                                     00001360
             03 MWACCEPTP    PIC X.                                     00001370
             03 MWACCEPTH    PIC X.                                     00001380
             03 MWACCEPTV    PIC X.                                     00001390
             03 MWACCEPTO    PIC X.                                     00001400
      * flag refusee                                                    00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MWREFUSEA    PIC X.                                     00001430
             03 MWREFUSEC    PIC X.                                     00001440
             03 MWREFUSEP    PIC X.                                     00001450
             03 MWREFUSEH    PIC X.                                     00001460
             03 MWREFUSEV    PIC X.                                     00001470
             03 MWREFUSEO    PIC X.                                     00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLIBERRA  PIC X.                                          00001500
           02 MLIBERRC  PIC X.                                          00001510
           02 MLIBERRP  PIC X.                                          00001520
           02 MLIBERRH  PIC X.                                          00001530
           02 MLIBERRV  PIC X.                                          00001540
           02 MLIBERRO  PIC X(78).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCODTRAA  PIC X.                                          00001570
           02 MCODTRAC  PIC X.                                          00001580
           02 MCODTRAP  PIC X.                                          00001590
           02 MCODTRAH  PIC X.                                          00001600
           02 MCODTRAV  PIC X.                                          00001610
           02 MCODTRAO  PIC X(4).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCICSA    PIC X.                                          00001640
           02 MCICSC    PIC X.                                          00001650
           02 MCICSP    PIC X.                                          00001660
           02 MCICSH    PIC X.                                          00001670
           02 MCICSV    PIC X.                                          00001680
           02 MCICSO    PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNETNAMA  PIC X.                                          00001710
           02 MNETNAMC  PIC X.                                          00001720
           02 MNETNAMP  PIC X.                                          00001730
           02 MNETNAMH  PIC X.                                          00001740
           02 MNETNAMV  PIC X.                                          00001750
           02 MNETNAMO  PIC X(8).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSCREENA  PIC X.                                          00001780
           02 MSCREENC  PIC X.                                          00001790
           02 MSCREENP  PIC X.                                          00001800
           02 MSCREENH  PIC X.                                          00001810
           02 MSCREENV  PIC X.                                          00001820
           02 MSCREENO  PIC X(4).                                       00001830
                                                                                
