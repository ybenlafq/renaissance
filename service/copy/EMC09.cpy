      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: etat suivie de la gamme                                    00000020
      ***************************************************************** 00000030
       01   EMC09I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFNOUVL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFNOUVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFNOUVF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFNOUVI   PIC X.                                          00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNOUVL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDNOUVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDNOUVF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDNOUVI   PIC X(10).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFEPUISL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MFEPUISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFEPUISF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFEPUISI  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPUISL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEPUISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEPUISF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEPUISI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFTOUTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MFTOUTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFTOUTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFTOUTI   PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTOUTL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDTOUTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDTOUTF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDTOUTI   PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFDARTYL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MFDARTYL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFDARTYF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MFDARTYI  PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFDACEML  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MFDACEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFDACEMF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFDACEMI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFTLMBRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MFTLMBRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFTLMBRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MFTLMBRI  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFTLMBLL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MFTLMBLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFTLMBLF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MFTLMBLI  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFELABRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MFELABRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFELABRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MFELABRI  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFELABLL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MFELABLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFELABLF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MFELABLI  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(74).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODTRAI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MZONCMDI  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCICSI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNETNAMI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(5).                                       00000850
      ***************************************************************** 00000860
      * SDF: etat suivie de la gamme                                    00000870
      ***************************************************************** 00000880
       01   EMC09O REDEFINES EMC09I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MDATJOUA  PIC X.                                          00000920
           02 MDATJOUC  PIC X.                                          00000930
           02 MDATJOUP  PIC X.                                          00000940
           02 MDATJOUH  PIC X.                                          00000950
           02 MDATJOUV  PIC X.                                          00000960
           02 MDATJOUO  PIC X(10).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTIMJOUA  PIC X.                                          00000990
           02 MTIMJOUC  PIC X.                                          00001000
           02 MTIMJOUP  PIC X.                                          00001010
           02 MTIMJOUH  PIC X.                                          00001020
           02 MTIMJOUV  PIC X.                                          00001030
           02 MTIMJOUO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MFNOUVA   PIC X.                                          00001060
           02 MFNOUVC   PIC X.                                          00001070
           02 MFNOUVP   PIC X.                                          00001080
           02 MFNOUVH   PIC X.                                          00001090
           02 MFNOUVV   PIC X.                                          00001100
           02 MFNOUVO   PIC X.                                          00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDNOUVA   PIC X.                                          00001130
           02 MDNOUVC   PIC X.                                          00001140
           02 MDNOUVP   PIC X.                                          00001150
           02 MDNOUVH   PIC X.                                          00001160
           02 MDNOUVV   PIC X.                                          00001170
           02 MDNOUVO   PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MFEPUISA  PIC X.                                          00001200
           02 MFEPUISC  PIC X.                                          00001210
           02 MFEPUISP  PIC X.                                          00001220
           02 MFEPUISH  PIC X.                                          00001230
           02 MFEPUISV  PIC X.                                          00001240
           02 MFEPUISO  PIC X.                                          00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDEPUISA  PIC X.                                          00001270
           02 MDEPUISC  PIC X.                                          00001280
           02 MDEPUISP  PIC X.                                          00001290
           02 MDEPUISH  PIC X.                                          00001300
           02 MDEPUISV  PIC X.                                          00001310
           02 MDEPUISO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MFTOUTA   PIC X.                                          00001340
           02 MFTOUTC   PIC X.                                          00001350
           02 MFTOUTP   PIC X.                                          00001360
           02 MFTOUTH   PIC X.                                          00001370
           02 MFTOUTV   PIC X.                                          00001380
           02 MFTOUTO   PIC X.                                          00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDTOUTA   PIC X.                                          00001410
           02 MDTOUTC   PIC X.                                          00001420
           02 MDTOUTP   PIC X.                                          00001430
           02 MDTOUTH   PIC X.                                          00001440
           02 MDTOUTV   PIC X.                                          00001450
           02 MDTOUTO   PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MFDARTYA  PIC X.                                          00001480
           02 MFDARTYC  PIC X.                                          00001490
           02 MFDARTYP  PIC X.                                          00001500
           02 MFDARTYH  PIC X.                                          00001510
           02 MFDARTYV  PIC X.                                          00001520
           02 MFDARTYO  PIC X.                                          00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MFDACEMA  PIC X.                                          00001550
           02 MFDACEMC  PIC X.                                          00001560
           02 MFDACEMP  PIC X.                                          00001570
           02 MFDACEMH  PIC X.                                          00001580
           02 MFDACEMV  PIC X.                                          00001590
           02 MFDACEMO  PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MFTLMBRA  PIC X.                                          00001620
           02 MFTLMBRC  PIC X.                                          00001630
           02 MFTLMBRP  PIC X.                                          00001640
           02 MFTLMBRH  PIC X.                                          00001650
           02 MFTLMBRV  PIC X.                                          00001660
           02 MFTLMBRO  PIC X.                                          00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MFTLMBLA  PIC X.                                          00001690
           02 MFTLMBLC  PIC X.                                          00001700
           02 MFTLMBLP  PIC X.                                          00001710
           02 MFTLMBLH  PIC X.                                          00001720
           02 MFTLMBLV  PIC X.                                          00001730
           02 MFTLMBLO  PIC X.                                          00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MFELABRA  PIC X.                                          00001760
           02 MFELABRC  PIC X.                                          00001770
           02 MFELABRP  PIC X.                                          00001780
           02 MFELABRH  PIC X.                                          00001790
           02 MFELABRV  PIC X.                                          00001800
           02 MFELABRO  PIC X.                                          00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MFELABLA  PIC X.                                          00001830
           02 MFELABLC  PIC X.                                          00001840
           02 MFELABLP  PIC X.                                          00001850
           02 MFELABLH  PIC X.                                          00001860
           02 MFELABLV  PIC X.                                          00001870
           02 MFELABLO  PIC X.                                          00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLIBERRA  PIC X.                                          00001900
           02 MLIBERRC  PIC X.                                          00001910
           02 MLIBERRP  PIC X.                                          00001920
           02 MLIBERRH  PIC X.                                          00001930
           02 MLIBERRV  PIC X.                                          00001940
           02 MLIBERRO  PIC X(74).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCODTRAA  PIC X.                                          00001970
           02 MCODTRAC  PIC X.                                          00001980
           02 MCODTRAP  PIC X.                                          00001990
           02 MCODTRAH  PIC X.                                          00002000
           02 MCODTRAV  PIC X.                                          00002010
           02 MCODTRAO  PIC X(4).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MZONCMDA  PIC X.                                          00002040
           02 MZONCMDC  PIC X.                                          00002050
           02 MZONCMDP  PIC X.                                          00002060
           02 MZONCMDH  PIC X.                                          00002070
           02 MZONCMDV  PIC X.                                          00002080
           02 MZONCMDO  PIC X.                                          00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSP    PIC X.                                          00002130
           02 MCICSH    PIC X.                                          00002140
           02 MCICSV    PIC X.                                          00002150
           02 MCICSO    PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNETNAMA  PIC X.                                          00002180
           02 MNETNAMC  PIC X.                                          00002190
           02 MNETNAMP  PIC X.                                          00002200
           02 MNETNAMH  PIC X.                                          00002210
           02 MNETNAMV  PIC X.                                          00002220
           02 MNETNAMO  PIC X(8).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MSCREENA  PIC X.                                          00002250
           02 MSCREENC  PIC X.                                          00002260
           02 MSCREENP  PIC X.                                          00002270
           02 MSCREENH  PIC X.                                          00002280
           02 MSCREENV  PIC X.                                          00002290
           02 MSCREENO  PIC X(5).                                       00002300
                                                                                
