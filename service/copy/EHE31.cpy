      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE31   EHE31                                              00000020
      ***************************************************************** 00000030
       01   EHE31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLITRTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCLITRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLITRTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLITRTI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLITRTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLITRTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLITRTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLITRTI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRETL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNRETL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRETF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNRETI    PIC X(11).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRETL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDRETL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDRETF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDRETI    PIC X(10).                                      00000370
           02 MLIGNEI OCCURS   12 TIMES .                               00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNHSI   PIC X(10).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCODI   PIC X(7).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MFAMI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MMRQI   PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MREFI   PIC X(10).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOML   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCOMF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCOMI   PIC X(11).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCTRTL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCCTRTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCCTRTF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCCTRTI      PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPANL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MPANL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPANF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPANI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRFUL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MRFUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MRFUF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MRFUI   PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFAL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNFAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNFAF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNFAI   PIC X(7).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(78).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EHE31   EHE31                                              00001000
      ***************************************************************** 00001010
       01   EHE31O REDEFINES EHE31I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNUMPAGEA      PIC X.                                     00001190
           02 MNUMPAGEC PIC X.                                          00001200
           02 MNUMPAGEP PIC X.                                          00001210
           02 MNUMPAGEH PIC X.                                          00001220
           02 MNUMPAGEV PIC X.                                          00001230
           02 MNUMPAGEO      PIC 99.                                    00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEMAXA      PIC X.                                     00001260
           02 MPAGEMAXC PIC X.                                          00001270
           02 MPAGEMAXP PIC X.                                          00001280
           02 MPAGEMAXH PIC X.                                          00001290
           02 MPAGEMAXV PIC X.                                          00001300
           02 MPAGEMAXO      PIC 99.                                    00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCLITRTA  PIC X.                                          00001330
           02 MCLITRTC  PIC X.                                          00001340
           02 MCLITRTP  PIC X.                                          00001350
           02 MCLITRTH  PIC X.                                          00001360
           02 MCLITRTV  PIC X.                                          00001370
           02 MCLITRTO  PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLLITRTA  PIC X.                                          00001400
           02 MLLITRTC  PIC X.                                          00001410
           02 MLLITRTP  PIC X.                                          00001420
           02 MLLITRTH  PIC X.                                          00001430
           02 MLLITRTV  PIC X.                                          00001440
           02 MLLITRTO  PIC X(20).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNRETA    PIC X.                                          00001470
           02 MNRETC    PIC X.                                          00001480
           02 MNRETP    PIC X.                                          00001490
           02 MNRETH    PIC X.                                          00001500
           02 MNRETV    PIC X.                                          00001510
           02 MNRETO    PIC X(11).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDRETA    PIC X.                                          00001540
           02 MDRETC    PIC X.                                          00001550
           02 MDRETP    PIC X.                                          00001560
           02 MDRETH    PIC X.                                          00001570
           02 MDRETV    PIC X.                                          00001580
           02 MDRETO    PIC X(10).                                      00001590
           02 MLIGNEO OCCURS   12 TIMES .                               00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MNHSA   PIC X.                                          00001620
             03 MNHSC   PIC X.                                          00001630
             03 MNHSP   PIC X.                                          00001640
             03 MNHSH   PIC X.                                          00001650
             03 MNHSV   PIC X.                                          00001660
             03 MNHSO   PIC X(10).                                      00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MCODA   PIC X.                                          00001690
             03 MCODC   PIC X.                                          00001700
             03 MCODP   PIC X.                                          00001710
             03 MCODH   PIC X.                                          00001720
             03 MCODV   PIC X.                                          00001730
             03 MCODO   PIC X(7).                                       00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MFAMA   PIC X.                                          00001760
             03 MFAMC   PIC X.                                          00001770
             03 MFAMP   PIC X.                                          00001780
             03 MFAMH   PIC X.                                          00001790
             03 MFAMV   PIC X.                                          00001800
             03 MFAMO   PIC X(5).                                       00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MMRQA   PIC X.                                          00001830
             03 MMRQC   PIC X.                                          00001840
             03 MMRQP   PIC X.                                          00001850
             03 MMRQH   PIC X.                                          00001860
             03 MMRQV   PIC X.                                          00001870
             03 MMRQO   PIC X(5).                                       00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MREFA   PIC X.                                          00001900
             03 MREFC   PIC X.                                          00001910
             03 MREFP   PIC X.                                          00001920
             03 MREFH   PIC X.                                          00001930
             03 MREFV   PIC X.                                          00001940
             03 MREFO   PIC X(10).                                      00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MCOMA   PIC X.                                          00001970
             03 MCOMC   PIC X.                                          00001980
             03 MCOMP   PIC X.                                          00001990
             03 MCOMH   PIC X.                                          00002000
             03 MCOMV   PIC X.                                          00002010
             03 MCOMO   PIC X(11).                                      00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MCCTRTA      PIC X.                                     00002040
             03 MCCTRTC PIC X.                                          00002050
             03 MCCTRTP PIC X.                                          00002060
             03 MCCTRTH PIC X.                                          00002070
             03 MCCTRTV PIC X.                                          00002080
             03 MCCTRTO      PIC X(5).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MPANA   PIC X.                                          00002110
             03 MPANC   PIC X.                                          00002120
             03 MPANP   PIC X.                                          00002130
             03 MPANH   PIC X.                                          00002140
             03 MPANV   PIC X.                                          00002150
             03 MPANO   PIC X(5).                                       00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MRFUA   PIC X.                                          00002180
             03 MRFUC   PIC X.                                          00002190
             03 MRFUP   PIC X.                                          00002200
             03 MRFUH   PIC X.                                          00002210
             03 MRFUV   PIC X.                                          00002220
             03 MRFUO   PIC X(5).                                       00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MNFAA   PIC X.                                          00002250
             03 MNFAC   PIC X.                                          00002260
             03 MNFAP   PIC X.                                          00002270
             03 MNFAH   PIC X.                                          00002280
             03 MNFAV   PIC X.                                          00002290
             03 MNFAO   PIC X(7).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(78).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
