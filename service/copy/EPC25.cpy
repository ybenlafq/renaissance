      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ACC: LISTE DES PRESTATIONS B2B                                  00000020
      ***************************************************************** 00000030
       01   EPC25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSTATUTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEI    PIC X(12).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCDEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDCDEI    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOMPTEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCOMPTEI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSL      COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MRSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MRSF      PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MRSI      PIC X(25).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHRONOL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNCHRONOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCHRONOF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCHRONOI      PIC X(15).                                 00000450
           02 MLIGNEI OCCURS   6 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSELI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRESTAL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCPRESTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPRESTAF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCPRESTAI    PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRESTAL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLPRESTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLPRESTAF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLPRESTAI    PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPUNITL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MPUNITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPUNITF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MPUNITI      PIC X(9).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQTEI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTHTL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MMTHTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMTHTF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MMTHTI  PIC X(9).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTVAL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MMTTVAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTVAF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MMTTVAI      PIC X(9).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTTCL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MMTTTCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTTCF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMTTTCI      PIC X(9).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(74).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * ACC: LISTE DES PRESTATIONS B2B                                  00001000
      ***************************************************************** 00001010
       01   EPC25O REDEFINES EPC25I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNBPA     PIC X.                                          00001260
           02 MNBPC     PIC X.                                          00001270
           02 MNBPP     PIC X.                                          00001280
           02 MNBPH     PIC X.                                          00001290
           02 MNBPV     PIC X.                                          00001300
           02 MNBPO     PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MSTATUTA  PIC X.                                          00001330
           02 MSTATUTC  PIC X.                                          00001340
           02 MSTATUTP  PIC X.                                          00001350
           02 MSTATUTH  PIC X.                                          00001360
           02 MSTATUTV  PIC X.                                          00001370
           02 MSTATUTO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNCDEA    PIC X.                                          00001400
           02 MNCDEC    PIC X.                                          00001410
           02 MNCDEP    PIC X.                                          00001420
           02 MNCDEH    PIC X.                                          00001430
           02 MNCDEV    PIC X.                                          00001440
           02 MNCDEO    PIC X(12).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MDCDEA    PIC X.                                          00001470
           02 MDCDEC    PIC X.                                          00001480
           02 MDCDEP    PIC X.                                          00001490
           02 MDCDEH    PIC X.                                          00001500
           02 MDCDEV    PIC X.                                          00001510
           02 MDCDEO    PIC X(10).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNCOMPTEA      PIC X.                                     00001540
           02 MNCOMPTEC PIC X.                                          00001550
           02 MNCOMPTEP PIC X.                                          00001560
           02 MNCOMPTEH PIC X.                                          00001570
           02 MNCOMPTEV PIC X.                                          00001580
           02 MNCOMPTEO      PIC X(8).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MRSA      PIC X.                                          00001610
           02 MRSC      PIC X.                                          00001620
           02 MRSP      PIC X.                                          00001630
           02 MRSH      PIC X.                                          00001640
           02 MRSV      PIC X.                                          00001650
           02 MRSO      PIC X(25).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNCHRONOA      PIC X.                                     00001680
           02 MNCHRONOC PIC X.                                          00001690
           02 MNCHRONOP PIC X.                                          00001700
           02 MNCHRONOH PIC X.                                          00001710
           02 MNCHRONOV PIC X.                                          00001720
           02 MNCHRONOO      PIC X(15).                                 00001730
           02 MLIGNEO OCCURS   6 TIMES .                                00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MSELA   PIC X.                                          00001760
             03 MSELC   PIC X.                                          00001770
             03 MSELP   PIC X.                                          00001780
             03 MSELH   PIC X.                                          00001790
             03 MSELV   PIC X.                                          00001800
             03 MSELO   PIC X.                                          00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MCPRESTAA    PIC X.                                     00001830
             03 MCPRESTAC    PIC X.                                     00001840
             03 MCPRESTAP    PIC X.                                     00001850
             03 MCPRESTAH    PIC X.                                     00001860
             03 MCPRESTAV    PIC X.                                     00001870
             03 MCPRESTAO    PIC X(7).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MLPRESTAA    PIC X.                                     00001900
             03 MLPRESTAC    PIC X.                                     00001910
             03 MLPRESTAP    PIC X.                                     00001920
             03 MLPRESTAH    PIC X.                                     00001930
             03 MLPRESTAV    PIC X.                                     00001940
             03 MLPRESTAO    PIC X(20).                                 00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MPUNITA      PIC X.                                     00001970
             03 MPUNITC PIC X.                                          00001980
             03 MPUNITP PIC X.                                          00001990
             03 MPUNITH PIC X.                                          00002000
             03 MPUNITV PIC X.                                          00002010
             03 MPUNITO      PIC X(9).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MQTEA   PIC X.                                          00002040
             03 MQTEC   PIC X.                                          00002050
             03 MQTEP   PIC X.                                          00002060
             03 MQTEH   PIC X.                                          00002070
             03 MQTEV   PIC X.                                          00002080
             03 MQTEO   PIC X(5).                                       00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MMTHTA  PIC X.                                          00002110
             03 MMTHTC  PIC X.                                          00002120
             03 MMTHTP  PIC X.                                          00002130
             03 MMTHTH  PIC X.                                          00002140
             03 MMTHTV  PIC X.                                          00002150
             03 MMTHTO  PIC X(9).                                       00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MMTTVAA      PIC X.                                     00002180
             03 MMTTVAC PIC X.                                          00002190
             03 MMTTVAP PIC X.                                          00002200
             03 MMTTVAH PIC X.                                          00002210
             03 MMTTVAV PIC X.                                          00002220
             03 MMTTVAO      PIC X(9).                                  00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MMTTTCA      PIC X.                                     00002250
             03 MMTTTCC PIC X.                                          00002260
             03 MMTTTCP PIC X.                                          00002270
             03 MMTTTCH PIC X.                                          00002280
             03 MMTTTCV PIC X.                                          00002290
             03 MMTTTCO      PIC X(9).                                  00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(74).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
