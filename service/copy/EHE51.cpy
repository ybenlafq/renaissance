      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE51   EHE51                                              00000020
      ***************************************************************** 00000030
       01   EHE51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CLIEUHTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 CLIEUHTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CLIEUHTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 CLIEUHTI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LLIEUHTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 LLIEUHTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LLIEUHTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 LLIEUHTI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NLOTDL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 NLOTDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NLOTDF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 NLOTDI    PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CLIEUHIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 CLIEUHIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CLIEUHIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 CLIEUHII  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LLIEUHIL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 LLIEUHIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LLIEUHIF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 LLIEUHII  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CTIERSL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 CTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 CTIERSF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 CTIERSI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LTIERSL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 LTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 LTIERSF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 LTIERSI   PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DLOTDL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 DLOTDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 DLOTDF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 DLOTDI    PIC X(10).                                      00000530
           02 MLIGNEI OCCURS   12 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNHSI   PIC X(11).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCODI   PIC X(7).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MFAMI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MMRQI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MREFI   PIC X(14).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOML   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MCOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCOMF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCOMI   PIC X(20).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPANL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MPANL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPANF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MPANI   PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRFUL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MRFUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MRFUF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MRFUI   PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EHE51   EHE51                                              00001080
      ***************************************************************** 00001090
       01   EHE51O REDEFINES EHE51I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNUMPAGEA      PIC X.                                     00001270
           02 MNUMPAGEC PIC X.                                          00001280
           02 MNUMPAGEP PIC X.                                          00001290
           02 MNUMPAGEH PIC X.                                          00001300
           02 MNUMPAGEV PIC X.                                          00001310
           02 MNUMPAGEO      PIC 99.                                    00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMAXA      PIC X.                                     00001340
           02 MPAGEMAXC PIC X.                                          00001350
           02 MPAGEMAXP PIC X.                                          00001360
           02 MPAGEMAXH PIC X.                                          00001370
           02 MPAGEMAXV PIC X.                                          00001380
           02 MPAGEMAXO      PIC 99.                                    00001390
           02 FILLER    PIC X(2).                                       00001400
           02 CLIEUHTA  PIC X.                                          00001410
           02 CLIEUHTC  PIC X.                                          00001420
           02 CLIEUHTP  PIC X.                                          00001430
           02 CLIEUHTH  PIC X.                                          00001440
           02 CLIEUHTV  PIC X.                                          00001450
           02 CLIEUHTO  PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 LLIEUHTA  PIC X.                                          00001480
           02 LLIEUHTC  PIC X.                                          00001490
           02 LLIEUHTP  PIC X.                                          00001500
           02 LLIEUHTH  PIC X.                                          00001510
           02 LLIEUHTV  PIC X.                                          00001520
           02 LLIEUHTO  PIC X(20).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 NLOTDA    PIC X.                                          00001550
           02 NLOTDC    PIC X.                                          00001560
           02 NLOTDP    PIC X.                                          00001570
           02 NLOTDH    PIC X.                                          00001580
           02 NLOTDV    PIC X.                                          00001590
           02 NLOTDO    PIC X(7).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 CLIEUHIA  PIC X.                                          00001620
           02 CLIEUHIC  PIC X.                                          00001630
           02 CLIEUHIP  PIC X.                                          00001640
           02 CLIEUHIH  PIC X.                                          00001650
           02 CLIEUHIV  PIC X.                                          00001660
           02 CLIEUHIO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 LLIEUHIA  PIC X.                                          00001690
           02 LLIEUHIC  PIC X.                                          00001700
           02 LLIEUHIP  PIC X.                                          00001710
           02 LLIEUHIH  PIC X.                                          00001720
           02 LLIEUHIV  PIC X.                                          00001730
           02 LLIEUHIO  PIC X(20).                                      00001740
           02 FILLER    PIC X(2).                                       00001750
           02 CTIERSA   PIC X.                                          00001760
           02 CTIERSC   PIC X.                                          00001770
           02 CTIERSP   PIC X.                                          00001780
           02 CTIERSH   PIC X.                                          00001790
           02 CTIERSV   PIC X.                                          00001800
           02 CTIERSO   PIC X(5).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 LTIERSA   PIC X.                                          00001830
           02 LTIERSC   PIC X.                                          00001840
           02 LTIERSP   PIC X.                                          00001850
           02 LTIERSH   PIC X.                                          00001860
           02 LTIERSV   PIC X.                                          00001870
           02 LTIERSO   PIC X(20).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 DLOTDA    PIC X.                                          00001900
           02 DLOTDC    PIC X.                                          00001910
           02 DLOTDP    PIC X.                                          00001920
           02 DLOTDH    PIC X.                                          00001930
           02 DLOTDV    PIC X.                                          00001940
           02 DLOTDO    PIC X(10).                                      00001950
           02 MLIGNEO OCCURS   12 TIMES .                               00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MNHSA   PIC X.                                          00001980
             03 MNHSC   PIC X.                                          00001990
             03 MNHSP   PIC X.                                          00002000
             03 MNHSH   PIC X.                                          00002010
             03 MNHSV   PIC X.                                          00002020
             03 MNHSO   PIC X(11).                                      00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MCODA   PIC X.                                          00002050
             03 MCODC   PIC X.                                          00002060
             03 MCODP   PIC X.                                          00002070
             03 MCODH   PIC X.                                          00002080
             03 MCODV   PIC X.                                          00002090
             03 MCODO   PIC X(7).                                       00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MFAMA   PIC X.                                          00002120
             03 MFAMC   PIC X.                                          00002130
             03 MFAMP   PIC X.                                          00002140
             03 MFAMH   PIC X.                                          00002150
             03 MFAMV   PIC X.                                          00002160
             03 MFAMO   PIC X(5).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MMRQA   PIC X.                                          00002190
             03 MMRQC   PIC X.                                          00002200
             03 MMRQP   PIC X.                                          00002210
             03 MMRQH   PIC X.                                          00002220
             03 MMRQV   PIC X.                                          00002230
             03 MMRQO   PIC X(5).                                       00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MREFA   PIC X.                                          00002260
             03 MREFC   PIC X.                                          00002270
             03 MREFP   PIC X.                                          00002280
             03 MREFH   PIC X.                                          00002290
             03 MREFV   PIC X.                                          00002300
             03 MREFO   PIC X(14).                                      00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCOMA   PIC X.                                          00002330
             03 MCOMC   PIC X.                                          00002340
             03 MCOMP   PIC X.                                          00002350
             03 MCOMH   PIC X.                                          00002360
             03 MCOMV   PIC X.                                          00002370
             03 MCOMO   PIC X(20).                                      00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MPANA   PIC X.                                          00002400
             03 MPANC   PIC X.                                          00002410
             03 MPANP   PIC X.                                          00002420
             03 MPANH   PIC X.                                          00002430
             03 MPANV   PIC X.                                          00002440
             03 MPANO   PIC X(5).                                       00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MRFUA   PIC X.                                          00002470
             03 MRFUC   PIC X.                                          00002480
             03 MRFUP   PIC X.                                          00002490
             03 MRFUH   PIC X.                                          00002500
             03 MRFUV   PIC X.                                          00002510
             03 MRFUO   PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
