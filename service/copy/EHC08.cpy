      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC08   EHC08                                              00000020
      ***************************************************************** 00000030
       01   EHC08I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUHCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUHCL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUHCF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUHCI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNENVOII  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDENVOII  PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTIERSI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTENVOIL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCTENVOIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTENVOIF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTENVOII      PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRENDUL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MRENDUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRENDUF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MRENDUI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCTRANSI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRANSF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLTRANSI  PIC X(20).                                      00000570
           02 TABLEI OCCURS   12 TIMES .                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANNULL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MANNULL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MANNULF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MANNULI      PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCHSL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNCHSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCHSF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNCHSI  PIC X(7).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNCODICI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCFAMI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLREFI  PIC X(20).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSERIEL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MSERIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSERIEF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSERIEI      PIC X(16).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EHC08   EHC08                                              00001080
      ***************************************************************** 00001090
       01   EHC08O REDEFINES EHC08I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC X(2).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGETOTA      PIC X.                                     00001340
           02 MPAGETOTC PIC X.                                          00001350
           02 MPAGETOTP PIC X.                                          00001360
           02 MPAGETOTH PIC X.                                          00001370
           02 MPAGETOTV PIC X.                                          00001380
           02 MPAGETOTO      PIC X(2).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNLIEUHCA      PIC X.                                     00001410
           02 MNLIEUHCC PIC X.                                          00001420
           02 MNLIEUHCP PIC X.                                          00001430
           02 MNLIEUHCH PIC X.                                          00001440
           02 MNLIEUHCV PIC X.                                          00001450
           02 MNLIEUHCO      PIC X(3).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLLIEUHCA      PIC X.                                     00001480
           02 MLLIEUHCC PIC X.                                          00001490
           02 MLLIEUHCP PIC X.                                          00001500
           02 MLLIEUHCH PIC X.                                          00001510
           02 MLLIEUHCV PIC X.                                          00001520
           02 MLLIEUHCO      PIC X(20).                                 00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNENVOIA  PIC X.                                          00001550
           02 MNENVOIC  PIC X.                                          00001560
           02 MNENVOIP  PIC X.                                          00001570
           02 MNENVOIH  PIC X.                                          00001580
           02 MNENVOIV  PIC X.                                          00001590
           02 MNENVOIO  PIC X(7).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MDENVOIA  PIC X.                                          00001620
           02 MDENVOIC  PIC X.                                          00001630
           02 MDENVOIP  PIC X.                                          00001640
           02 MDENVOIH  PIC X.                                          00001650
           02 MDENVOIV  PIC X.                                          00001660
           02 MDENVOIO  PIC X(10).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCTIERSA  PIC X.                                          00001690
           02 MCTIERSC  PIC X.                                          00001700
           02 MCTIERSP  PIC X.                                          00001710
           02 MCTIERSH  PIC X.                                          00001720
           02 MCTIERSV  PIC X.                                          00001730
           02 MCTIERSO  PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCTENVOIA      PIC X.                                     00001760
           02 MCTENVOIC PIC X.                                          00001770
           02 MCTENVOIP PIC X.                                          00001780
           02 MCTENVOIH PIC X.                                          00001790
           02 MCTENVOIV PIC X.                                          00001800
           02 MCTENVOIO      PIC X.                                     00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MRENDUA   PIC X.                                          00001830
           02 MRENDUC   PIC X.                                          00001840
           02 MRENDUP   PIC X.                                          00001850
           02 MRENDUH   PIC X.                                          00001860
           02 MRENDUV   PIC X.                                          00001870
           02 MRENDUO   PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCTRANSA  PIC X.                                          00001900
           02 MCTRANSC  PIC X.                                          00001910
           02 MCTRANSP  PIC X.                                          00001920
           02 MCTRANSH  PIC X.                                          00001930
           02 MCTRANSV  PIC X.                                          00001940
           02 MCTRANSO  PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLTRANSA  PIC X.                                          00001970
           02 MLTRANSC  PIC X.                                          00001980
           02 MLTRANSP  PIC X.                                          00001990
           02 MLTRANSH  PIC X.                                          00002000
           02 MLTRANSV  PIC X.                                          00002010
           02 MLTRANSO  PIC X(20).                                      00002020
           02 TABLEO OCCURS   12 TIMES .                                00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MANNULA      PIC X.                                     00002050
             03 MANNULC PIC X.                                          00002060
             03 MANNULP PIC X.                                          00002070
             03 MANNULH PIC X.                                          00002080
             03 MANNULV PIC X.                                          00002090
             03 MANNULO      PIC X.                                     00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MNCHSA  PIC X.                                          00002120
             03 MNCHSC  PIC X.                                          00002130
             03 MNCHSP  PIC X.                                          00002140
             03 MNCHSH  PIC X.                                          00002150
             03 MNCHSV  PIC X.                                          00002160
             03 MNCHSO  PIC 9(7).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MNCODICA     PIC X.                                     00002190
             03 MNCODICC     PIC X.                                     00002200
             03 MNCODICP     PIC X.                                     00002210
             03 MNCODICH     PIC X.                                     00002220
             03 MNCODICV     PIC X.                                     00002230
             03 MNCODICO     PIC X(7).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MCFAMA  PIC X.                                          00002260
             03 MCFAMC  PIC X.                                          00002270
             03 MCFAMP  PIC X.                                          00002280
             03 MCFAMH  PIC X.                                          00002290
             03 MCFAMV  PIC X.                                          00002300
             03 MCFAMO  PIC X(5).                                       00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCMARQA      PIC X.                                     00002330
             03 MCMARQC PIC X.                                          00002340
             03 MCMARQP PIC X.                                          00002350
             03 MCMARQH PIC X.                                          00002360
             03 MCMARQV PIC X.                                          00002370
             03 MCMARQO      PIC X(5).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MLREFA  PIC X.                                          00002400
             03 MLREFC  PIC X.                                          00002410
             03 MLREFP  PIC X.                                          00002420
             03 MLREFH  PIC X.                                          00002430
             03 MLREFV  PIC X.                                          00002440
             03 MLREFO  PIC X(20).                                      00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MSERIEA      PIC X.                                     00002470
             03 MSERIEC PIC X.                                          00002480
             03 MSERIEP PIC X.                                          00002490
             03 MSERIEH PIC X.                                          00002500
             03 MSERIEV PIC X.                                          00002510
             03 MSERIEO      PIC X(16).                                 00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
