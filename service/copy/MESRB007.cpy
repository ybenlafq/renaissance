      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:11 >
      
      *****************************************************************
      *   COPY MESSAGE MQ reponse demandeInfoTransaction REDBOX
      *   VERS LE S.I. REDBOX. (RBxQSouRes)
      *****************************************************************
      *
      *
       05 MESRB007-DATA.
      *
      *
      *   N� DE TRANSACTION DE VENTE
          10 MESRB07-NTRANV        PIC    X(30).
      *   CODE SOCI�T� VENTE "GV"
          10 MESRB07-NSOC          PIC    X(03).
      *   CODE LIEU VENTE "GV"
          10 MESRB07-NLIEU         PIC    X(03).
      *   N� VENTE GV
          10 MESRB07-NVENTEGV      PIC    X(13).
      *   code erreur
          10 MESRB07-Nerreur       PIC    X(04).
      *   libelle erreur
          10 MESRB07-lerreur       PIC    X(200).
      *   type    erreur  i=info b=bloquant
          10 MESRB07-terreur       PIC    X(01).
      *   ADRESSES FACTURATION ET LIVRAISON
          10 MESRB07-ADRESSE-filler.
          11 MESRB07-ADRESSE OCCURS 2.
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)
      *   CIVILIT�
             15 MESRB07-CIVILITE      PIC    X(05).
      *   NOM CLIENT
             15 MESRB07-NOMCLI        PIC    X(25).
      *   PR�NOM CLIENT
             15 MESRB07-PRENOM        PIC    X(15).
      *   T�L�PHONE FIXE
             15 MESrb07-TELFIXE       PIC    X(10).
      *   T�L�PHONE PORTABLE
             15 MESrb07-TELPORT       PIC    X(10).
      *   T�L�PHONE BUREAU
             15 MESrb07-TELBUR        PIC    X(10).
      *   POSTE BUREAU
             15 MESrb07-PSTBUR        PIC    X(05).
      *   E-MAIL
             15 MESrb07-EMAIL         PIC    X(63).
             15 MESRB07-TYPADR        PIC    X(01).
      *      NUM�RO DE VOIE
             15 MESRB07-NUMVOIE       PIC    X(05).
      *      TYPE DE VOIE
             15 MESRB07-TYPVOIE       PIC    X(04).
      *      NOM DE VOIE
             15 MESRB07-NOMVOIE       PIC    X(21).
      *      COMPL�MENT ADRESSE DE LIVRAISON 1
             15 MESRB07-CADRFAC1      PIC    X(32).
      *      COMPL�MENT ADRESSE DE LIVRAISON2
             15 MESRB07-CADRFAC2      PIC    X(32).
      *      B�TIMENT
             15 MESRB07-BATIMENT      PIC    X(03).
      *      ESCALIER
             15 MESRB07-ESCALIER      PIC    X(03).
      *      �TAGE
             15 MESRB07-ETAGE         PIC    X(03).
      *      PORTE
             15 MESRB07-PORTE         PIC    X(03).
      *      CODE PAYS
             15 MESRB07-CPAYS         PIC    X(03).
      *      CODE INSEE
             15 MESRB07-CINSEE        PIC    X(05).
      *      COMMUNE
             15 MESRB07-COMMUNE       PIC    X(32).
      *      CODE POSTAL
             15 MESRB07-CPOSTAL       PIC    X(05).
      *      CODE client ucm
      *      15 MESRB07-idcliucm      PIC    X(08).
      *      CODE adresse ucm
             15 MESRB07-idadrucm      PIC    X(08).
      *
      *      DONN�ES REDBOX
      *
      *
      *      DONN�ES LIGNE DE VENTE
      *
      *   *
          10 MESRB07-DETVENTE-filler.
          11 MESRB07-DETVENTE OCCURS 80.
      *      sequence unique
             15 MESRB07-OffreRedbox PIC    X(19).
      *      sequence unique
             15 MESRB07-nseqnq     PIC    X(05).
      *      type de ligne
             15 MESRB07-typenreg   PIC    X(01).
      *      CODe offre
             15 MESRB07-Coffre     PIC    X(07).
      *      CODIC OU CODE PRESTATION
             15 MESRB07-CPREST     PIC    X(07).
      *      sequence reference
             15 MESRB07-nseqref    PIC    X(05).
      *      reference
             15 MESRB07-lreffourn  PIC    X(20).
      *      code marque
             15 MESRB07-cmarq      PIC    X(05).
      *      libelle marque
             15 MESRB07-lmarq      PIC    X(20).
      *      code famille
             15 MESRB07-cfam       PIC    X(05).
      *      libelle famille
             15 MESRB07-lfam       PIC    X(20).
      *      QUANTIT�
             15 MESRB07-QUANTITE   PIC    9(05).
      *      PRIX DE VENTE
             15 MESRB07-PVENTE     PIC   S9(07)V99.
      *      MODE DE D�LIVRANCE
             15 MESRB07-CMODDEL    PIC    X(05).
      *      libell� D�LIVRANCE
             15 MESRB07-LMODDEL    PIC    X(20).
      *      DATE DE D�LIVRANCE
             15 MESRB07-DDELIV     PIC    X(08).
      *      MATRICULE DU VENDEUR
             15 MESRB07-CVEND      PIC    X(07).
      *      topage
             15 MESRB07-wtopelivre PIC    X(01).
      *      statut de ligne
             15 MESRB07-statut     PIC    X(01).
      *      libelle statut
             15 MESRB07-lstatut    PIC    X(200).
      *      type de statut
             15 MESRB07-tstatut    PIC    X(1).
      *
      
