      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   TABLE RTFP03                                                          
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVFP0300                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVFP0300.                                                            
           02  FP03-NSINISTRE                                                   
               PIC X(0020).                                                     
           02  FP03-CTYPFAC2                                                    
               PIC X(0001).                                                     
           02  FP03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFP0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFP0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP03-NSINISTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP03-NSINISTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP03-CTYPFAC2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP03-CTYPFAC2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
