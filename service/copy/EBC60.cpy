      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Ebc60   Ebc60                                              00000020
      ***************************************************************** 00000030
       01   EBC60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPADDRL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MTYPADDRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPADDRF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTYPADDRI      PIC X(10).                                 00000170
      * DARTY PRO                                                       00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLB2BL    COMP PIC S9(4).                                 00000190
      *--                                                                       
           02 MLB2BL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLB2BF    PIC X.                                          00000200
           02 FILLER    PIC X(4).                                       00000210
           02 MLB2BI    PIC X(20).                                      00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITREL  COMP PIC S9(4).                                 00000230
      *--                                                                       
           02 MCTITREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTITREF  PIC X.                                          00000240
           02 FILLER    PIC X(4).                                       00000250
           02 MCTITREI  PIC X(5).                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLNOMI    PIC X(25).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MLPRENOMI      PIC X(15).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTORGL    COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MTORGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTORGF    PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MTORGI    PIC X(5).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORGL    COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MLORGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLORGF    PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MLORGI    PIC X(25).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCARTEL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLCARTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCARTEF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLCARTEI  PIC X(8).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCARTEL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNCARTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCARTEF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNCARTEI  PIC X(9).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIEL   COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MNVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNVOIEF   PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNVOIEI   PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIEL   COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVOIEF   PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLVOIEI   PIC X(33).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPADRL      COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MCOMPADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMPADRF      PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCOMPADRI      PIC X(30).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCBATL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCBATF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCBATI    PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCESCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCESCF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCESCI    PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCETAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETAGF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCETAGI   PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASCL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCASCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCASCF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCASCI    PIC X.                                          00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTEL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCPORTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPORTEF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCPORTEI  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDIGICODL      COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MDIGICODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDIGICODF      PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MDIGICODI      PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTL   COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPOSTF   PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCPOSTI   PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMNL   COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLCOMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMNF   PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLCOMNI   PIC X(32).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTL   COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPOSTF   PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLPOSTI   PIC X(26).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRESSECL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MRESSECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRESSECF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MRESSECI  PIC X.                                          00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYSL   COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPAYSF   PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLPAYSI   PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINDTELL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MINDTELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINDTELF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MINDTELI  PIC X(22).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTDOML   COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNTDOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTDOMF   PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNTDOMI   PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSML    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNGSML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNGSMF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNGSMI    PIC X(15).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTBURL   COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNTBURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTBURF   PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNTBURI   PIC X(15).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEMAILL   COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MEMAILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEMAILF   PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MEMAILI   PIC X(65).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNAISSL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MDNAISSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDNAISSF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MDNAISSI  PIC X(10).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLNAISL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLLNAISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLNAISF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLLNAISI  PIC X(9).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPNAISSL      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MCPNAISSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPNAISSF      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCPNAISSI      PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNAISSL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLNAISSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLNAISSF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLNAISSI  PIC X(32).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINFO1L  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MLINFO1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINFO1F  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MLINFO1I  PIC X(79).                                      00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINFO2L  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MLINFO2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINFO2F  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MLINFO2I  PIC X(79).                                      00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINFO3L  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MLINFO3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINFO3F  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MLINFO3I  PIC X(79).                                      00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRQUESTL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MRQUESTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRQUESTF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MRQUESTI  PIC X.                                          00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUESTL   COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MQUESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQUESTF   PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MQUESTI   PIC X(75).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MLIBERRI  PIC X(74).                                      00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCODTRAI  PIC X(4).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCICSI    PIC X(5).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MNETNAMI  PIC X(8).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MSCREENI  PIC X(4).                                       00001820
      ***************************************************************** 00001830
      * SDF: Ebc60   Ebc60                                              00001840
      ***************************************************************** 00001850
       01   EBC60O REDEFINES EBC60I.                                    00001860
           02 FILLER    PIC X(12).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATJOUA  PIC X.                                          00001890
           02 MDATJOUC  PIC X.                                          00001900
           02 MDATJOUP  PIC X.                                          00001910
           02 MDATJOUH  PIC X.                                          00001920
           02 MDATJOUV  PIC X.                                          00001930
           02 MDATJOUO  PIC X(10).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MTIMJOUA  PIC X.                                          00001960
           02 MTIMJOUC  PIC X.                                          00001970
           02 MTIMJOUP  PIC X.                                          00001980
           02 MTIMJOUH  PIC X.                                          00001990
           02 MTIMJOUV  PIC X.                                          00002000
           02 MTIMJOUO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTYPADDRA      PIC X.                                     00002030
           02 MTYPADDRC PIC X.                                          00002040
           02 MTYPADDRP PIC X.                                          00002050
           02 MTYPADDRH PIC X.                                          00002060
           02 MTYPADDRV PIC X.                                          00002070
           02 MTYPADDRO      PIC X(10).                                 00002080
      * DARTY PRO                                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLB2BA    PIC X.                                          00002110
           02 MLB2BC    PIC X.                                          00002120
           02 MLB2BP    PIC X.                                          00002130
           02 MLB2BH    PIC X.                                          00002140
           02 MLB2BV    PIC X.                                          00002150
           02 MLB2BO    PIC X(20).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCTITREA  PIC X.                                          00002180
           02 MCTITREC  PIC X.                                          00002190
           02 MCTITREP  PIC X.                                          00002200
           02 MCTITREH  PIC X.                                          00002210
           02 MCTITREV  PIC X.                                          00002220
           02 MCTITREO  PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLNOMA    PIC X.                                          00002250
           02 MLNOMC    PIC X.                                          00002260
           02 MLNOMP    PIC X.                                          00002270
           02 MLNOMH    PIC X.                                          00002280
           02 MLNOMV    PIC X.                                          00002290
           02 MLNOMO    PIC X(25).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLPRENOMA      PIC X.                                     00002320
           02 MLPRENOMC PIC X.                                          00002330
           02 MLPRENOMP PIC X.                                          00002340
           02 MLPRENOMH PIC X.                                          00002350
           02 MLPRENOMV PIC X.                                          00002360
           02 MLPRENOMO      PIC X(15).                                 00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MTORGA    PIC X.                                          00002390
           02 MTORGC    PIC X.                                          00002400
           02 MTORGP    PIC X.                                          00002410
           02 MTORGH    PIC X.                                          00002420
           02 MTORGV    PIC X.                                          00002430
           02 MTORGO    PIC X(5).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLORGA    PIC X.                                          00002460
           02 MLORGC    PIC X.                                          00002470
           02 MLORGP    PIC X.                                          00002480
           02 MLORGH    PIC X.                                          00002490
           02 MLORGV    PIC X.                                          00002500
           02 MLORGO    PIC X(25).                                      00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MLCARTEA  PIC X.                                          00002530
           02 MLCARTEC  PIC X.                                          00002540
           02 MLCARTEP  PIC X.                                          00002550
           02 MLCARTEH  PIC X.                                          00002560
           02 MLCARTEV  PIC X.                                          00002570
           02 MLCARTEO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MNCARTEA  PIC X.                                          00002600
           02 MNCARTEC  PIC X.                                          00002610
           02 MNCARTEP  PIC X.                                          00002620
           02 MNCARTEH  PIC X.                                          00002630
           02 MNCARTEV  PIC X.                                          00002640
           02 MNCARTEO  PIC X(9).                                       00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MNVOIEA   PIC X.                                          00002670
           02 MNVOIEC   PIC X.                                          00002680
           02 MNVOIEP   PIC X.                                          00002690
           02 MNVOIEH   PIC X.                                          00002700
           02 MNVOIEV   PIC X.                                          00002710
           02 MNVOIEO   PIC X(5).                                       00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MLVOIEA   PIC X.                                          00002740
           02 MLVOIEC   PIC X.                                          00002750
           02 MLVOIEP   PIC X.                                          00002760
           02 MLVOIEH   PIC X.                                          00002770
           02 MLVOIEV   PIC X.                                          00002780
           02 MLVOIEO   PIC X(33).                                      00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MCOMPADRA      PIC X.                                     00002810
           02 MCOMPADRC PIC X.                                          00002820
           02 MCOMPADRP PIC X.                                          00002830
           02 MCOMPADRH PIC X.                                          00002840
           02 MCOMPADRV PIC X.                                          00002850
           02 MCOMPADRO      PIC X(30).                                 00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MCBATA    PIC X.                                          00002880
           02 MCBATC    PIC X.                                          00002890
           02 MCBATP    PIC X.                                          00002900
           02 MCBATH    PIC X.                                          00002910
           02 MCBATV    PIC X.                                          00002920
           02 MCBATO    PIC X(3).                                       00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MCESCA    PIC X.                                          00002950
           02 MCESCC    PIC X.                                          00002960
           02 MCESCP    PIC X.                                          00002970
           02 MCESCH    PIC X.                                          00002980
           02 MCESCV    PIC X.                                          00002990
           02 MCESCO    PIC X(3).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MCETAGA   PIC X.                                          00003020
           02 MCETAGC   PIC X.                                          00003030
           02 MCETAGP   PIC X.                                          00003040
           02 MCETAGH   PIC X.                                          00003050
           02 MCETAGV   PIC X.                                          00003060
           02 MCETAGO   PIC X(3).                                       00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MCASCA    PIC X.                                          00003090
           02 MCASCC    PIC X.                                          00003100
           02 MCASCP    PIC X.                                          00003110
           02 MCASCH    PIC X.                                          00003120
           02 MCASCV    PIC X.                                          00003130
           02 MCASCO    PIC X.                                          00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCPORTEA  PIC X.                                          00003160
           02 MCPORTEC  PIC X.                                          00003170
           02 MCPORTEP  PIC X.                                          00003180
           02 MCPORTEH  PIC X.                                          00003190
           02 MCPORTEV  PIC X.                                          00003200
           02 MCPORTEO  PIC X(3).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MDIGICODA      PIC X.                                     00003230
           02 MDIGICODC PIC X.                                          00003240
           02 MDIGICODP PIC X.                                          00003250
           02 MDIGICODH PIC X.                                          00003260
           02 MDIGICODV PIC X.                                          00003270
           02 MDIGICODO      PIC X(6).                                  00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MCPOSTA   PIC X.                                          00003300
           02 MCPOSTC   PIC X.                                          00003310
           02 MCPOSTP   PIC X.                                          00003320
           02 MCPOSTH   PIC X.                                          00003330
           02 MCPOSTV   PIC X.                                          00003340
           02 MCPOSTO   PIC X(5).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MLCOMNA   PIC X.                                          00003370
           02 MLCOMNC   PIC X.                                          00003380
           02 MLCOMNP   PIC X.                                          00003390
           02 MLCOMNH   PIC X.                                          00003400
           02 MLCOMNV   PIC X.                                          00003410
           02 MLCOMNO   PIC X(32).                                      00003420
           02 FILLER    PIC X(2).                                       00003430
           02 MLPOSTA   PIC X.                                          00003440
           02 MLPOSTC   PIC X.                                          00003450
           02 MLPOSTP   PIC X.                                          00003460
           02 MLPOSTH   PIC X.                                          00003470
           02 MLPOSTV   PIC X.                                          00003480
           02 MLPOSTO   PIC X(26).                                      00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MRESSECA  PIC X.                                          00003510
           02 MRESSECC  PIC X.                                          00003520
           02 MRESSECP  PIC X.                                          00003530
           02 MRESSECH  PIC X.                                          00003540
           02 MRESSECV  PIC X.                                          00003550
           02 MRESSECO  PIC X.                                          00003560
           02 FILLER    PIC X(2).                                       00003570
           02 MLPAYSA   PIC X.                                          00003580
           02 MLPAYSC   PIC X.                                          00003590
           02 MLPAYSP   PIC X.                                          00003600
           02 MLPAYSH   PIC X.                                          00003610
           02 MLPAYSV   PIC X.                                          00003620
           02 MLPAYSO   PIC X(15).                                      00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MINDTELA  PIC X.                                          00003650
           02 MINDTELC  PIC X.                                          00003660
           02 MINDTELP  PIC X.                                          00003670
           02 MINDTELH  PIC X.                                          00003680
           02 MINDTELV  PIC X.                                          00003690
           02 MINDTELO  PIC X(22).                                      00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MNTDOMA   PIC X.                                          00003720
           02 MNTDOMC   PIC X.                                          00003730
           02 MNTDOMP   PIC X.                                          00003740
           02 MNTDOMH   PIC X.                                          00003750
           02 MNTDOMV   PIC X.                                          00003760
           02 MNTDOMO   PIC X(15).                                      00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MNGSMA    PIC X.                                          00003790
           02 MNGSMC    PIC X.                                          00003800
           02 MNGSMP    PIC X.                                          00003810
           02 MNGSMH    PIC X.                                          00003820
           02 MNGSMV    PIC X.                                          00003830
           02 MNGSMO    PIC X(15).                                      00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MNTBURA   PIC X.                                          00003860
           02 MNTBURC   PIC X.                                          00003870
           02 MNTBURP   PIC X.                                          00003880
           02 MNTBURH   PIC X.                                          00003890
           02 MNTBURV   PIC X.                                          00003900
           02 MNTBURO   PIC X(15).                                      00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MEMAILA   PIC X.                                          00003930
           02 MEMAILC   PIC X.                                          00003940
           02 MEMAILP   PIC X.                                          00003950
           02 MEMAILH   PIC X.                                          00003960
           02 MEMAILV   PIC X.                                          00003970
           02 MEMAILO   PIC X(65).                                      00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MDNAISSA  PIC X.                                          00004000
           02 MDNAISSC  PIC X.                                          00004010
           02 MDNAISSP  PIC X.                                          00004020
           02 MDNAISSH  PIC X.                                          00004030
           02 MDNAISSV  PIC X.                                          00004040
           02 MDNAISSO  PIC X(10).                                      00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MLLNAISA  PIC X.                                          00004070
           02 MLLNAISC  PIC X.                                          00004080
           02 MLLNAISP  PIC X.                                          00004090
           02 MLLNAISH  PIC X.                                          00004100
           02 MLLNAISV  PIC X.                                          00004110
           02 MLLNAISO  PIC X(9).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MCPNAISSA      PIC X.                                     00004140
           02 MCPNAISSC PIC X.                                          00004150
           02 MCPNAISSP PIC X.                                          00004160
           02 MCPNAISSH PIC X.                                          00004170
           02 MCPNAISSV PIC X.                                          00004180
           02 MCPNAISSO      PIC X(5).                                  00004190
           02 FILLER    PIC X(2).                                       00004200
           02 MLNAISSA  PIC X.                                          00004210
           02 MLNAISSC  PIC X.                                          00004220
           02 MLNAISSP  PIC X.                                          00004230
           02 MLNAISSH  PIC X.                                          00004240
           02 MLNAISSV  PIC X.                                          00004250
           02 MLNAISSO  PIC X(32).                                      00004260
           02 FILLER    PIC X(2).                                       00004270
           02 MLINFO1A  PIC X.                                          00004280
           02 MLINFO1C  PIC X.                                          00004290
           02 MLINFO1P  PIC X.                                          00004300
           02 MLINFO1H  PIC X.                                          00004310
           02 MLINFO1V  PIC X.                                          00004320
           02 MLINFO1O  PIC X(79).                                      00004330
           02 FILLER    PIC X(2).                                       00004340
           02 MLINFO2A  PIC X.                                          00004350
           02 MLINFO2C  PIC X.                                          00004360
           02 MLINFO2P  PIC X.                                          00004370
           02 MLINFO2H  PIC X.                                          00004380
           02 MLINFO2V  PIC X.                                          00004390
           02 MLINFO2O  PIC X(79).                                      00004400
           02 FILLER    PIC X(2).                                       00004410
           02 MLINFO3A  PIC X.                                          00004420
           02 MLINFO3C  PIC X.                                          00004430
           02 MLINFO3P  PIC X.                                          00004440
           02 MLINFO3H  PIC X.                                          00004450
           02 MLINFO3V  PIC X.                                          00004460
           02 MLINFO3O  PIC X(79).                                      00004470
           02 FILLER    PIC X(2).                                       00004480
           02 MRQUESTA  PIC X.                                          00004490
           02 MRQUESTC  PIC X.                                          00004500
           02 MRQUESTP  PIC X.                                          00004510
           02 MRQUESTH  PIC X.                                          00004520
           02 MRQUESTV  PIC X.                                          00004530
           02 MRQUESTO  PIC X.                                          00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MQUESTA   PIC X.                                          00004560
           02 MQUESTC   PIC X.                                          00004570
           02 MQUESTP   PIC X.                                          00004580
           02 MQUESTH   PIC X.                                          00004590
           02 MQUESTV   PIC X.                                          00004600
           02 MQUESTO   PIC X(75).                                      00004610
           02 FILLER    PIC X(2).                                       00004620
           02 MLIBERRA  PIC X.                                          00004630
           02 MLIBERRC  PIC X.                                          00004640
           02 MLIBERRP  PIC X.                                          00004650
           02 MLIBERRH  PIC X.                                          00004660
           02 MLIBERRV  PIC X.                                          00004670
           02 MLIBERRO  PIC X(74).                                      00004680
           02 FILLER    PIC X(2).                                       00004690
           02 MCODTRAA  PIC X.                                          00004700
           02 MCODTRAC  PIC X.                                          00004710
           02 MCODTRAP  PIC X.                                          00004720
           02 MCODTRAH  PIC X.                                          00004730
           02 MCODTRAV  PIC X.                                          00004740
           02 MCODTRAO  PIC X(4).                                       00004750
           02 FILLER    PIC X(2).                                       00004760
           02 MCICSA    PIC X.                                          00004770
           02 MCICSC    PIC X.                                          00004780
           02 MCICSP    PIC X.                                          00004790
           02 MCICSH    PIC X.                                          00004800
           02 MCICSV    PIC X.                                          00004810
           02 MCICSO    PIC X(5).                                       00004820
           02 FILLER    PIC X(2).                                       00004830
           02 MNETNAMA  PIC X.                                          00004840
           02 MNETNAMC  PIC X.                                          00004850
           02 MNETNAMP  PIC X.                                          00004860
           02 MNETNAMH  PIC X.                                          00004870
           02 MNETNAMV  PIC X.                                          00004880
           02 MNETNAMO  PIC X(8).                                       00004890
           02 FILLER    PIC X(2).                                       00004900
           02 MSCREENA  PIC X.                                          00004910
           02 MSCREENC  PIC X.                                          00004920
           02 MSCREENP  PIC X.                                          00004930
           02 MSCREENH  PIC X.                                          00004940
           02 MSCREENV  PIC X.                                          00004950
           02 MSCREENO  PIC X(4).                                       00004960
                                                                                
