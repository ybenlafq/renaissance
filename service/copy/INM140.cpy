      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM140 AU 13/06/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                           20,03,BI,A,                          *        
      *                           23,08,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM140.                                                        
            05 NOMETAT-INM140           PIC X(6) VALUE 'INM140'.                
            05 RUPTURES-INM140.                                                 
           10 INM140-DATETR             PIC X(08).                      007  008
           10 INM140-NMAG               PIC X(03).                      015  003
           10 INM140-CODOPER            PIC X(02).                      018  002
           10 INM140-NCAISS             PIC X(03).                      020  003
           10 INM140-NTRANS             PIC X(08).                      023  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM140-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 INM140-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM140.                                                   
           10 INM140-DEVAUT             PIC X(03).                      033  003
           10 INM140-DEVREF             PIC X(03).                      036  003
           10 INM140-DOCUMENT           PIC X(17).                      039  017
           10 INM140-LDEVREF            PIC X(25).                      056  025
           10 INM140-LIBOPER            PIC X(25).                      081  025
           10 INM140-LMAG               PIC X(20).                      106  020
           10 INM140-MONPAUT            PIC S9(07)V9(2) COMP-3.         126  005
           10 INM140-MONTANT            PIC S9(07)V9(2) COMP-3.         131  005
            05 FILLER                      PIC X(377).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM140-LONG           PIC S9(4)   COMP  VALUE +135.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM140-LONG           PIC S9(4) COMP-5  VALUE +135.           
                                                                                
      *}                                                                        
