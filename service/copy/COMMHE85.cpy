      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -AIDA ********************************************************* 00000010
      *  ZONES DE COMMAREA                                              00000020
      ***************************************************************** 00000030
      *                                                                 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-HE85-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *--                                                                       
       01  COM-HE85-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000060
       01  Z-COMMAREA.                                                  00000070
      *                                                                 00000080
      * ZONES RESERVEES A AIDA -----------------------------------      00000090
          02 FILLER-COM-AIDA      PIC X(100).                           00000100
      *                                                                 00000110
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------      00000120
          02 COMM-CICS-APPLID     PIC X(8).                             00000130
          02 COMM-CICS-NETNAM     PIC X(8).                             00000140
          02 COMM-CICS-TRANSA     PIC X(4).                             00000150
      *                                                                 00000160
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------      00000170
          02 COMM-DATE-SIECLE     PIC XX.                               00000180
          02 COMM-DATE-ANNEE      PIC XX.                               00000190
          02 COMM-DATE-MOIS       PIC XX.                               00000200
          02 COMM-DATE-JOUR       PIC XX.                               00000210
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000220
          02 COMM-DATE-QNTA       PIC 999.                              00000230
          02 COMM-DATE-QNT0       PIC 99999.                            00000240
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000250
          02 COMM-DATE-BISX       PIC 9.                                00000260
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000270
          02 COMM-DATE-JSM        PIC 9.                                00000280
      *   LIBELLES DU JOUR COURT - LONG                                 00000290
          02 COMM-DATE-JSM-LC     PIC XXX.                              00000300
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00000310
      *   LIBELLES DU MOIS COURT - LONG                                 00000320
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00000330
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00000340
      *   DIFFERENTES FORMES DE DATE                                    00000350
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00000360
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00000370
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00000380
          02 COMM-DATE-JJMMAA     PIC X(6).                             00000390
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00000400
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00000410
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000420
          02 COMM-DATE-WEEK.                                            00000430
             05  COMM-DATE-SEMSS  PIC 99.                               00000440
             05  COMM-DATE-SEMAA  PIC 99.                               00000450
             05  COMM-DATE-SEMNU  PIC 99.                               00000460
          02 COMM-DATE-FILLER     PIC X(08).                            00000470
      *                                                                 00000480
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------      00000490
      *                                                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000510
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR       PIC X  OCCURS 150.                    00000520
          02 COMM-HE85.                                                 00000720
             03 COMM-HE85-OPTION       PIC X(01).                          00000
             03 COMM-HE85-CACID        PIC X(04).                          00000
             03 COMM-HE85-IMPR         PIC X(04).                          00000
             03 COMM-HE85-NLIEU        PIC X(03).                          00000
             03 COMM-HE85-LLIEU        PIC X(20).                          00000
             03 COMM-HE85-CCTRT        PIC X(05).                          00000
             03 COMM-HE85-LCTRT        PIC X(20).                          00000
             03 COMM-HE85-TIERS        PIC X(05).                          00000
             03 COMM-HE85-LTIERS       PIC X(20).                          00000
             03 COMM-HE85-NPREPA       PIC X(07).                          00000
             03 COMM-HE85-DPREPA       PIC X(08).                          00000
             03 COMM-HE85-MESS         PIC X(60).                          00000
             03 COMM-HE85-ADRESSE      PIC X(07).                               
             03 COMM-HE85-DACCORD      PIC X(08).                               
             03 COMM-HE85-DDACCORD     PIC X(08).                               
             03 COMM-HE85-NACCORD      PIC X(12).                               
             03 COMM-HEGOP.                                             20     1
               04 COMM-HE-I                     PIC  9(02).                     
               04 COMM-HE-MAX                   PIC  9(02).                     
               04 COMM-HE-LIGNES                          OCCURS 12 .           
                  05 COMM-HE-FLAGAVAR.                                          
                     06 COMM-HE-FLAV            PIC  X(04).                     
                     06 COMM-HE-FLAR            PIC  X(04).                     
                  05 COMM-HE-OPAV.                                              
                     06 COMM-HE-OPAVDE          PIC  X(03).                     
                     06 COMM-HE-OPAVFI          PIC  X(03).                     
                  05 COMM-HE-OPAR.                                              
                     06 COMM-HE-OPARDE          PIC  X(03).                     
                     06 COMM-HE-OPARFI          PIC  X(03).                     
             03 COMM-HE85-QDEMANDE     PIC 9(05).                       00000720
             03 COMM-HE85-TIERS-SAISI  PIC X(01).                       00000720
             03 COMM-HE85-APPLI        PIC X(494).                      00000720
          02 COMM-HE86.                                                 00000720
             03 COMM-HE86-NPAGE        PIC 9(02).                       00000720
             03 COMM-HE86-NPAGE-MAX    PIC 9(02).                       00000720
             03 COMM-HE86-APPLI        PIC X(500).                      00000720
          02 COMM-HE87.                                                 00000720
             03 COMM-HE87-NPAGE        PIC 9(02).                       00000720
             03 COMM-HE87-NPAGE-MAX    PIC 9(02).                       00000720
             03 COMM-HE87-APPLI        PIC X(500).                      00000720
      *   02 COMM-HEXX.             PIC X(3217).                        00000720
      *                                                                 00000730
                                                                                
