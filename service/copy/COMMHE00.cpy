      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -AIDA ********************************************************* 00000010
      *  ZONES DE COMMAREA                                              00000020
      ***************************************************************** 00000030
      *                                                                 00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-HE00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *--                                                                       
       01  COM-HE00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000060
       01  Z-COMMAREA.                                                  00000070
      *                                                                 00000080
      * ZONES RESERVEES A AIDA -----------------------------------      00000090
          02 FILLER-COM-AIDA      PIC X(100).                           00000100
      *                                                                 00000110
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------      00000120
          02 COMM-CICS-APPLID     PIC X(8).                             00000130
          02 COMM-CICS-NETNAM     PIC X(8).                             00000140
          02 COMM-CICS-TRANSA     PIC X(4).                             00000150
      *                                                                 00000160
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------      00000170
          02 COMM-DATE-SIECLE     PIC XX.                               00000180
          02 COMM-DATE-ANNEE      PIC XX.                               00000190
          02 COMM-DATE-MOIS       PIC XX.                               00000200
          02 COMM-DATE-JOUR       PIC XX.                               00000210
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000220
          02 COMM-DATE-QNTA       PIC 999.                              00000230
          02 COMM-DATE-QNT0       PIC 99999.                            00000240
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000250
          02 COMM-DATE-BISX       PIC 9.                                00000260
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000270
          02 COMM-DATE-JSM        PIC 9.                                00000280
      *   LIBELLES DU JOUR COURT - LONG                                 00000290
          02 COMM-DATE-JSM-LC     PIC XXX.                              00000300
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00000310
      *   LIBELLES DU MOIS COURT - LONG                                 00000320
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00000330
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00000340
      *   DIFFERENTES FORMES DE DATE                                    00000350
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00000360
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00000370
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00000380
          02 COMM-DATE-JJMMAA     PIC X(6).                             00000390
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00000400
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00000410
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000420
          02 COMM-DATE-WEEK.                                            00000430
             05  COMM-DATE-SEMSS  PIC 99.                               00000440
             05  COMM-DATE-SEMAA  PIC 99.                               00000450
             05  COMM-DATE-SEMNU  PIC 99.                               00000460
          02 COMM-DATE-FILLER     PIC X(08).                            00000470
      *                                                                 00000480
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------      00000490
      *                                                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000510
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR       PIC X  OCCURS 150.                    00000520
      *                                                                 00000530
      *          ZONES DE COMMAREA CHARGEES PAR LE PROG THE00           00000540
      *          ''''''''''''''''''''''''''''''''''''''''''''           00000550
                                                                        00000530
          02     COMM-HE00-MESS   PIC X(50).                                    
                                                                        00000530
          02     COMM-HE00.                                                     
            05   COMM-WSAVELA     PIC X.                                        
            05   COMM-MODTRT      PIC X.                                        
            05   COMM-CTIERS      PIC X(05).                                    
            05   COMM-LNOM        PIC X(20).                                    
            05   COMM-LADR1       PIC X(32).                                    
            05   COMM-LADR2       PIC X(32).                                    
            05   COMM-CPOSTAL     PIC X(05).                                    
            05   COMM-LCOMMUNE    PIC X(26).                                    
            05   COMM-NTEL        PIC X(08).                                    
            05   COMM-NFAX        PIC X(15).                                    
            05   COMM-LINTERLOCUT PIC X(20).                                    
            05   COMM-CTYPTRAN    PIC X(05).                                    
            05   COMM-WTYPTIERS   PIC X(01).                                    
            05   COMM-NENTCDE     PIC X(05).                                    
            05   COMM-CTIERSELA   PIC X(04).                                    
            05   COMM-CTYPDEST    PIC X(03).                                    
MBEN00*     05   FILLER           PIC X(10).                                    
CP0808*     05   FILLER           PIC X(08).                                    
  "         05   COMM-RTHE01      PIC X(01).                                    
  "           88 COMM-TIERS-SANS  VALUE '0'.                                    
  "           88 COMM-TIERS-AVOIR VALUE '1'.                                    
  "           88 COMM-TIERS-EFEXL VALUE '2'.                                    
CP0808*     05   FILLER           PIC X(07).                                    
PA0610      05   COMM-MSOLD       PIC X(01).                                    
PA0610      05   FILLER           PIC X(06).                                    
MBEN00      05   COMM-CPAYS       PIC X(02).                                    
      *     05   COMM-DSYST       PIC 9(13).                                    
            05   COMM-CISOC       PIC X(03).                                    
                                                                        00000540
      *          ZONES DE COMMAREA CHARGEES PAR LE PROG THE02           00000540
      *          ''''''''''''''''''''''''''''''''''''''''''''           00000550
                                                                        00000530
          02     COMM-HE02.                                                     
            05   COMM-HE02-NBL    PIC 9(05).                                    
            05   COMM-HE02-NBP    PIC 9(05).                                    
            05   COMM-HE02-PAGE   PIC 9(03).                                    
      *                                                                 00000550
      ***** FIN COMMAREA 'COMMHE00' ************************************00000550
                                                                                
