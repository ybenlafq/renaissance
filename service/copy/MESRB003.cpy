      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ question �change/Retour �QUIPEMENT                    
      *   VERS S.I. REDBOX. (RBxQEchRet)                                        
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESRB003-DATA.                                                        
      *                                                                         
      *                                                                         
         03 mesrb003-tableau    occurs 40.                                      
      *                                                                         
          04 mesrb003-ligne.                                                    
      *   SOCI�T� DE L'�CHANGE                                                  
          05 MESRB03-NSOCE         PIC    9(03).                                
      *   LIEU DE L'�CHANGE                                                     
          05 MESRB03-NLIEUE        PIC    9(03).                                
      *   TYPE DE MOUVEMENT ("R"/"E")                                           
          05 MESRB03-TYPMVT        PIC    X(01).                                
      *   DATE DE L'�CHANGE                                                     
          05 MESRB03-DATEE         PIC    X(08).                                
      *   HEURE DE L'�CHANGE                                                    
          05 MESRB03-HEUREE        PIC    X(06).                                
      *   CODIC REPRIS                                                          
          05 MESRB03-CODICRP       PIC    X(07).                                
      *   CODE EAN REPRIS                                                       
          05 MESRB03-CEANRP        PIC    X(13).                                
      *   N� DE S�RIE DE CODIC REPRIS                                           
          05 MESRB03-NSERRP        PIC    X(15).                                
      *   �TAT DU PRODUIT REPRIS                                                
          05 MESRB03-ETAPRP        PIC    X(03).                                
      *   APPAREIL REPRIS COMPLET ("O"/"N")                                     
          05 MESRB03-APPRPC        PIC    X(01).                                
      *   CODIC REMIS                                                           
          05 MESRB03-CODICRM       PIC    X(07).                                
      *   CODE EAN REMIS                                                        
          05 MESRB03-CEANRM        PIC    X(13).                                
      *   NUM�RO DE S�RIE REMIS                                                 
          05 MESRB03-NSERRM        PIC    X(15).                                
                                                                                
