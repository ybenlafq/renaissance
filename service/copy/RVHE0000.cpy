      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHE0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE0000.                                                            
      *}                                                                        
           02  HE00-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE00-LNOM                                                        
               PIC X(0020).                                                     
           02  HE00-LADR1                                                       
               PIC X(0032).                                                     
           02  HE00-LADR2                                                       
               PIC X(0032).                                                     
           02  HE00-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  HE00-LCOMMUNE                                                    
               PIC X(0026).                                                     
           02  HE00-NTEL                                                        
               PIC X(0008).                                                     
           02  HE00-NFAX                                                        
               PIC X(0015).                                                     
           02  HE00-LINTERLOCUT                                                 
               PIC X(0020).                                                     
           02  HE00-CTYPTRAN                                                    
               PIC X(0005).                                                     
           02  HE00-WTYPTIERS                                                   
               PIC X(0001).                                                     
           02  HE00-NENTCDE                                                     
               PIC X(0005).                                                     
           02  HE00-CTIERSELA                                                   
               PIC X(0004).                                                     
           02  HE00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-LADR1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-LADR1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-LADR2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-LADR2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-NTEL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-NTEL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-NFAX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-NFAX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-LINTERLOCUT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-LINTERLOCUT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-CTYPTRAN-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-CTYPTRAN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-WTYPTIERS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-WTYPTIERS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-CTIERSELA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-CTIERSELA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
