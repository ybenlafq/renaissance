      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FPTYP TYPOLOGIE PRESTATION             *        
      *----------------------------------------------------------------*        
       01  RVFPTYP .                                                            
           05  FPTYP-CTABLEG2    PIC X(15).                                     
           05  FPTYP-CTABLEG2-REDEF REDEFINES FPTYP-CTABLEG2.                   
               10  FPTYP-CTYPOC          PIC X(03).                             
           05  FPTYP-WTABLEG     PIC X(80).                                     
           05  FPTYP-WTABLEG-REDEF  REDEFINES FPTYP-WTABLEG.                    
               10  FPTYP-VALO            PIC X(01).                             
               10  FPTYP-CTYPOL          PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVFPTYP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FPTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FPTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FPTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FPTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
