      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE30   EHE30                                              00000020
      ***************************************************************** 00000030
       01   EHE30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEHETI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEHETI      PIC X(20).                                 00000290
           02 FILLER  OCCURS   12 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEL1L  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MSEL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEL1F  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSEL1I  PIC X.                                          00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRET1L      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDRET1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDRET1F      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDRET1I      PIC X(10).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLHED1L     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNLHED1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLHED1F     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNLHED1I     PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRET1L      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNRET1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNRET1F      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNRET1I      PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEL2L  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSEL2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEL2F  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSEL2I  PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRET2L      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDRET2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDRET2F      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDRET2I      PIC X(10).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLHED2L     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNLHED2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLHED2F     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNLHED2I     PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRET2L      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNRET2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNRET2F      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNRET2I      PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EHE30   EHE30                                              00000840
      ***************************************************************** 00000850
       01   EHE30O REDEFINES EHE30I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNUMPAGEA      PIC X.                                     00001030
           02 MNUMPAGEC PIC X.                                          00001040
           02 MNUMPAGEP PIC X.                                          00001050
           02 MNUMPAGEH PIC X.                                          00001060
           02 MNUMPAGEV PIC X.                                          00001070
           02 MNUMPAGEO      PIC 99.                                    00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MPAGEMAXA      PIC X.                                     00001100
           02 MPAGEMAXC PIC X.                                          00001110
           02 MPAGEMAXP PIC X.                                          00001120
           02 MPAGEMAXH PIC X.                                          00001130
           02 MPAGEMAXV PIC X.                                          00001140
           02 MPAGEMAXO      PIC 99.                                    00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCLIEHETA      PIC X.                                     00001170
           02 MCLIEHETC PIC X.                                          00001180
           02 MCLIEHETP PIC X.                                          00001190
           02 MCLIEHETH PIC X.                                          00001200
           02 MCLIEHETV PIC X.                                          00001210
           02 MCLIEHETO      PIC X(5).                                  00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLLIEHETA      PIC X.                                     00001240
           02 MLLIEHETC PIC X.                                          00001250
           02 MLLIEHETP PIC X.                                          00001260
           02 MLLIEHETH PIC X.                                          00001270
           02 MLLIEHETV PIC X.                                          00001280
           02 MLLIEHETO      PIC X(20).                                 00001290
           02 FILLER  OCCURS   12 TIMES .                               00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MSEL1A  PIC X.                                          00001320
             03 MSEL1C  PIC X.                                          00001330
             03 MSEL1P  PIC X.                                          00001340
             03 MSEL1H  PIC X.                                          00001350
             03 MSEL1V  PIC X.                                          00001360
             03 MSEL1O  PIC X.                                          00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MDRET1A      PIC X.                                     00001390
             03 MDRET1C PIC X.                                          00001400
             03 MDRET1P PIC X.                                          00001410
             03 MDRET1H PIC X.                                          00001420
             03 MDRET1V PIC X.                                          00001430
             03 MDRET1O      PIC X(10).                                 00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MNLHED1A     PIC X.                                     00001460
             03 MNLHED1C     PIC X.                                     00001470
             03 MNLHED1P     PIC X.                                     00001480
             03 MNLHED1H     PIC X.                                     00001490
             03 MNLHED1V     PIC X.                                     00001500
             03 MNLHED1O     PIC X(3).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MNRET1A      PIC X.                                     00001530
             03 MNRET1C PIC X.                                          00001540
             03 MNRET1P PIC X.                                          00001550
             03 MNRET1H PIC X.                                          00001560
             03 MNRET1V PIC X.                                          00001570
             03 MNRET1O      PIC X(7).                                  00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MSEL2A  PIC X.                                          00001600
             03 MSEL2C  PIC X.                                          00001610
             03 MSEL2P  PIC X.                                          00001620
             03 MSEL2H  PIC X.                                          00001630
             03 MSEL2V  PIC X.                                          00001640
             03 MSEL2O  PIC X.                                          00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MDRET2A      PIC X.                                     00001670
             03 MDRET2C PIC X.                                          00001680
             03 MDRET2P PIC X.                                          00001690
             03 MDRET2H PIC X.                                          00001700
             03 MDRET2V PIC X.                                          00001710
             03 MDRET2O      PIC X(10).                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MNLHED2A     PIC X.                                     00001740
             03 MNLHED2C     PIC X.                                     00001750
             03 MNLHED2P     PIC X.                                     00001760
             03 MNLHED2H     PIC X.                                     00001770
             03 MNLHED2V     PIC X.                                     00001780
             03 MNLHED2O     PIC X(3).                                  00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MNRET2A      PIC X.                                     00001810
             03 MNRET2C PIC X.                                          00001820
             03 MNRET2P PIC X.                                          00001830
             03 MNRET2H PIC X.                                          00001840
             03 MNRET2V PIC X.                                          00001850
             03 MNRET2O      PIC X(7).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(78).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
