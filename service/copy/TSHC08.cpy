      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE RTHC08                                       *            
      *       TR : HC00  GESTION CENTRE HS                         *            
      *       PG : THC08 TOPE DES LOTS D'ENVOI                     *            
      **************************************************************            
      *                                                                         
      *------------------------------ LONGUEUR                                  
       01  TS-HC08-LONG         PIC S9(4) COMP-3 VALUE +2052.                   
       01  TS-DONNEES.                                                          
      *------------------------------                                           
             07 TS-OCCURS       OCCURS 12.                                      
      *------------------------------ LIGNE ANNULEE OU NON                      
              10 TS-ANNUL       PIC X(01).                                      
      *------------------------------ LIGNE EN ANOMALIE MHE90                   
              10 TS-TOPANO      PIC X(01).                                      
      *------------------------------ NUMERO DE HS                              
              10 TS-NCHS        PIC X(07).                                      
      *------------------------------ CODIC                                     
              10 TS-NCODIC      PIC X(07).                                      
      *------------------------------ FAMILLE                                   
              10 TS-CFAM        PIC X(05).                                      
      *------------------------------ MARQUE                                    
              10 TS-CMARQ       PIC X(05).                                      
      *------------------------------ REFERENCE                                 
              10 TS-LREF        PIC X(20).                                      
      *------------------------------ SERIE                                     
              10 TS-CSERIE      PIC X(16).                                      
      *------------------------------ N� ACCORD                                 
              10 TS-NACCORD     PIC X(12).                                      
      *------------------------------ DATE ACCORD                               
              10 TS-DACCFOUR    PIC X(08).                                      
      *------------------------------ INTERLOCUTEUR                             
              10 TS-INTERLOC    PIC X(10).                                      
      *------------------------------ CLIENT                                    
              10 TS-CLIENT      PIC X(20).                                      
      *------------------------------ ADRESSE                                   
              10 TS-ADRESSE     PIC X(08).                                      
      *------------------------------ LIEU STOCKAGE                             
              10 TS-NSOCIETE    PIC X(03).                                      
              10 TS-NLIEU       PIC X(03).                                      
              10 TS-NSSLIEU     PIC X(03).                                      
              10 TS-CLIEUTRT    PIC X(05).                                      
      *------------------------------ COMMENTAIRE DE PANNE                      
              10 TS-LCOMMENT    PIC X(30).                                      
      *------------------------------ SEQUENCE FAMILLE                          
              10 TS-WSEQFAM     PIC S9(5).                                      
      *------------------------------ FLAG PRESENCE N� ACCORD                   
              10 TS-WNACCORD    PIC X(01).                                      
      *------------------------------ FLAG PRESENCE N� SERIE                    
              10 TS-WCSERIE     PIC X(01).                                      
      *                                                                         
      *--- F I N  'T S H C 0 8' ---------------------------------------*        
                                                                                
