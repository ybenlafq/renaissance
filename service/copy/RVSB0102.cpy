      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSB0102                      *        
      ******************************************************************        
       01  RVSB0102.                                                            
      *                       REFCMDEXT                                         
           10 SB01-REFCMDEXT       PIC X(30).                                   
      *                       TYPE                                              
           10 SB01-TYPE            PIC X(10).                                   
      *                       CVENDEUR                                          
           10 SB01-CVENDEUR        PIC X(30).                                   
      *                       MODPAI                                            
           10 SB01-MODPAI          PIC X(30).                                   
      *                       IDSIEBEL                                          
           10 SB01-IDSIEBEL        PIC X(30).                                   
      *                       MSERVICE                                          
           10 SB01-MSERVICE        PIC X(30).                                   
      *                       DEPOSE                                            
           10 SB01-DEPOSE          PIC X(30).                                   
      *                       REPRISEANC                                        
           10 SB01-REPRISEANC      PIC X(30).                                   
      *                       SOCLIEUP                                          
           10 SB01-SOCLIEUP        PIC X(30).                                   
      *                       NCOMPTE                                           
           10 SB01-NCOMPTE         PIC X(30).                                   
      *                       FARF                                              
           10 SB01-FARF            PIC X(1).                                    
      *                       RAISON                                            
           10 SB01-RAISON          PIC X(100).                                  
      *                       TELBUR                                            
           10 SB01-TELBUR          PIC X(15).                                   
      *                       CFILIALE                                          
           10 SB01-CFILIALE        PIC X(20).                                   
      *                       FLAGB2B                                           
           10 SB01-FLAGB2B         PIC X(1).                                    
      *                       FCVOIE                                            
           10 SB01-FCVOIE          PIC X(5).                                    
      *                       FCTVOIE                                           
           10 SB01-FCTVOIE         PIC X(30).                                   
      *                       FLVOIE                                            
           10 SB01-FLVOIE          PIC X(30).                                   
      *                       CPOSTAL                                           
           10 SB01-CPOSTAL         PIC X(30).                                   
      *                       COMMUNE                                           
           10 SB01-COMMUNE         PIC X(50).                                   
      *                       CPAYS                                             
           10 SB01-CPAYS           PIC X(5).                                    
      *                       CMPAD                                             
      *{Post-translation Transform-Var-varchar      
      *     10 SB01-CMPAD           PIC X(255).                                  
        10 SB01-CMPAD.
           49 SB01-CMPAD-LEN PIC S9(4) COMP-5 VALUE 255.
           49 SB01-CMPAD-TEXT PIC X(255).
      *} Post-Translation
      *                       LNCOMPTE                                          
           10 SB01-LNCOMPTE        PIC X(30).                                   
      *                       NOM                                               
           10 SB01-NOM             PIC X(50).                                   
      *                       PRENOM                                            
           10 SB01-PRENOM          PIC X(50).                                   
      *                       CIVILITE                                          
           10 SB01-CIVILITE        PIC X(15).                                   
      *                       LTELBUR                                           
           10 SB01-LTELBUR         PIC X(15).                                   
      *                       EMAIL                                             
           10 SB01-EMAIL           PIC X(50).                                   
      *                       TELGSM                                            
           10 SB01-TELGSM          PIC X(15).                                   
      *                       TELDOM                                            
           10 SB01-TELDOM          PIC X(15).                                   
      *                       NCARTE                                            
           10 SB01-NCARTE          PIC X(20).                                   
      *                       FADRLIVR                                          
           10 SB01-FADRLIVR        PIC X(1).                                    
      *                       CVOIE                                             
           10 SB01-CVOIE           PIC X(30).                                   
      *                       CTVOIE                                            
           10 SB01-CTVOIE          PIC X(100).                                  
      *                       LVOIE                                             
           10 SB01-LVOIE           PIC X(100).                                  
      *                       LBATIMENT                                         
           10 SB01-LBATIMENT       PIC X(50).                                   
      *                       LESCALIER                                         
           10 SB01-LESCALIER       PIC X(100).                                  
      *                       LETAGE                                            
           10 SB01-LETAGE          PIC X(40).                                   
      *                       LPORTE                                            
           10 SB01-LPORTE          PIC X(40).                                   
      *                       LCPOSTAL                                          
           10 SB01-LCPOSTAL        PIC X(30).                                   
      *                       LCOMMUNE                                          
           10 SB01-LCOMMUNE        PIC X(50).                                   
      *                       LCMPAD                                            
      *{Post-translation Transform-Var-varchar
      *     10 SB01-LCMPAD          PIC X(255).                                  
        10 SB01-LCMPAD.
           49 SB01-LCMPAD-LEN PIC S9(4) COMP-5 VALUE 255.
           49 SB01-LCMPAD-TEXT PIC X(255).
      *} Post-Translation
      *                       LCPAYS                                            
           10 SB01-LCPAYS          PIC X(5).                                    
      *                       PVTOTAL                                           
           10 SB01-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       COMPTANT                                          
           10 SB01-COMPTANT        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MNTARF                                            
           10 SB01-MNTARF          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       GVPVTOTAL                                         
           10 SB01-GVPVTOTAL       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       GVCOMPTANT                                        
           10 SB01-GVCOMPTANT      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       GVMNTARF                                          
           10 SB01-GVMNTARF        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DCREATION                                         
           10 SB01-DCREATION       PIC X(10).                                   
      *                       NSOCIETE                                          
           10 SB01-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 SB01-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 SB01-NVENTE          PIC X(7).                                    
      *                       NORDRE                                            
           10 SB01-NORDRE          PIC X(5).                                    
      *                       DSYST                                             
           10 SB01-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DATENC                                            
           10 SB01-DATENC          PIC X(8).                                    
      *                       FLAG_TOPE                                         
           10 SB01-FLAG-TOPE       PIC X(1).                                    
      *                       FCIVILITE                                         
           10 SB01-FCIVILITE       PIC X(10).                                   
      *                       FNOM                                              
           10 SB01-FNOM            PIC X(50).                                   
      *                       FPRENOM                                           
           10 SB01-FPRENOM         PIC X(50).                                   
      *                       FTELDOM                                           
           10 SB01-FTELDOM         PIC X(15).                                   
      *                       FTELGSM                                           
           10 SB01-FTELGSM         PIC X(15).                                   
      *                       FEMAIL                                            
           10 SB01-FEMAIL          PIC X(50).                                   
      *                       FADRESSE                                          
           10 SB01-FADRESSE        PIC X(50).                                   
      *                       LADRESSE                                          
           10 SB01-LADRESSE        PIC X(50).                                   
      *                       IDCLIENT_EXT                                      
           10 SB01-IDCLIENT-EXT    PIC X(15).                                   
      *                       DT_WEB                                            
           10 SB01-DT-WEB          PIC X(8).                                    
      *                       DTOPE_TOT                                         
           10 SB01-DTOPE-TOT       PIC X(8).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 68      *        
      ******************************************************************        
                                                                                
