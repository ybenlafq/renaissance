      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRB0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRB0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0000.                                                            
      *}                                                                        
           02  RB00-NBL                                                         
               PIC X(10).                                                       
           02  RB00-NFOURNISSEUR                                                
               PIC X(25).                                                       
           02  RB00-NRECEPTION                                                  
               PIC S9(8) COMP-3.                                                
           02  RB00-NCOMMANDE                                                   
               PIC X(07).                                                       
           02  RB00-DCOMMANDE                                                   
               PIC X(08).                                                       
           02  RB00-DRECEPTION                                                  
               PIC X(08).                                                       
           02  RB00-HRECEPTION                                                  
               PIC X(06).                                                       
           02  RB00-WRECEPTION                                                  
               PIC X(01).                                                       
           02  RB00-DENVOICT                                                    
               PIC X(08).                                                       
           02  RB00-HENVOICT                                                    
               PIC X(06).                                                       
           02  RB00-DRETOURCT                                                   
               PIC X(08).                                                       
           02  RB00-HRETOURCT                                                   
               PIC X(06).                                                       
           02  RB00-DENVOIRX                                                    
               PIC X(08).                                                       
           02  RB00-HENVOIRX                                                    
               PIC X(06).                                                       
           02  RB00-DRETOURRX                                                   
               PIC X(08).                                                       
           02  RB00-HRETOURRX                                                   
               PIC X(06).                                                       
           02  RB00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRB0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-NFOURNISSEUR-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-NFOURNISSEUR-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-NRECEPTION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-NRECEPTION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-NCOMMANDE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-NCOMMANDE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DCOMMANDE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DCOMMANDE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DRECEPTION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DRECEPTION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-HRECEPTION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-HRECEPTION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-WRECEPTION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-WRECEPTION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DENVOICT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DENVOICT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-HENVOICT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-HENVOICT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DRETOURCT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DRETOURCT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-HRETOURCT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-HRETOURCT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DENVOIRX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DENVOIRX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-HENVOIRX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-HENVOIRX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DRETOURRX-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DRETOURRX-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-HRETOURRX-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-HRETOURRX-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
