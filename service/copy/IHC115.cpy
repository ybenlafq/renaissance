      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHC115 AU 10/04/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,09,BI,A,                          *        
      *                           16,03,PD,A,                          *        
      *                           19,05,BI,A,                          *        
      *                           24,07,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHC115.                                                        
            05 NOMETAT-IHC115           PIC X(6) VALUE 'IHC115'.                
            05 RUPTURES-IHC115.                                                 
           10 IHC115-TEBRB              PIC X(09).                      007  009
           10 IHC115-WSEQFAM            PIC S9(05)      COMP-3.         016  003
           10 IHC115-CMARQ              PIC X(05).                      019  005
           10 IHC115-NCODIC             PIC X(07).                      024  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHC115-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 IHC115-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHC115.                                                   
           10 IHC115-CFAM               PIC X(05).                      033  005
           10 IHC115-LREFFOURN          PIC X(20).                      038  020
           10 IHC115-NLIEUHC-E          PIC X(03).                      058  003
           10 IHC115-QMAG               PIC S9(04)      COMP-3.         061  003
           10 IHC115-QTOTAL             PIC S9(04)      COMP-3.         064  003
            05 FILLER                      PIC X(446).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHC115-LONG           PIC S9(4)   COMP  VALUE +066.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHC115-LONG           PIC S9(4) COMP-5  VALUE +066.           
                                                                                
      *}                                                                        
