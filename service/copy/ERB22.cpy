      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: consult recept effectues                                   00000020
      ***************************************************************** 00000030
       01   ERB22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MDATDEBI  PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDATFINI  PIC X(10).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 NPAGEI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 NBPAGEI   PIC X(3).                                       00000290
           02 MTABLGRPI OCCURS   14 TIMES .                             00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSELI   PIC X.                                          00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLL   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBLF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNBLI   PIC X(10).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFOURNISSEURL    COMP PIC S9(4).                       00000390
      *--                                                                       
             03 MNFOURNISSEURL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MNFOURNISSEURF    PIC X.                                00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNFOURNISSEURI    PIC X(14).                            00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTREPOTL  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNENTREPOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNENTREPOTF  PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNENTREPOTI  PIC X(6).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQACCEPTL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQACCEPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQACCEPTF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQACCEPTI    PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECEPTL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDRECEPTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDRECEPTF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDRECEPTI    PIC X(8).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDENVOIOPL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDENVOIOPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDENVOIOPF   PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDENVOIOPI   PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRETOUROPL  COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDRETOUROPL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDRETOUROPF  PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDRETOUROPI  PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDENVOIRXL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDENVOIRXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDENVOIRXF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDENVOIRXI   PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * Rbx: consult recept effectues                                   00000880
      ***************************************************************** 00000890
       01   ERB22O REDEFINES ERB22I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MDATDEBA  PIC X.                                          00001070
           02 MDATDEBC  PIC X.                                          00001080
           02 MDATDEBP  PIC X.                                          00001090
           02 MDATDEBH  PIC X.                                          00001100
           02 MDATDEBV  PIC X.                                          00001110
           02 MDATDEBO  PIC X(10).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATFINA  PIC X.                                          00001140
           02 MDATFINC  PIC X.                                          00001150
           02 MDATFINP  PIC X.                                          00001160
           02 MDATFINH  PIC X.                                          00001170
           02 MDATFINV  PIC X.                                          00001180
           02 MDATFINO  PIC X(10).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 NPAGEA    PIC X.                                          00001210
           02 NPAGEC    PIC X.                                          00001220
           02 NPAGEP    PIC X.                                          00001230
           02 NPAGEH    PIC X.                                          00001240
           02 NPAGEV    PIC X.                                          00001250
           02 NPAGEO    PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 NBPAGEA   PIC X.                                          00001280
           02 NBPAGEC   PIC X.                                          00001290
           02 NBPAGEP   PIC X.                                          00001300
           02 NBPAGEH   PIC X.                                          00001310
           02 NBPAGEV   PIC X.                                          00001320
           02 NBPAGEO   PIC X(3).                                       00001330
           02 MTABLGRPO OCCURS   14 TIMES .                             00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MSELA   PIC X.                                          00001360
             03 MSELC   PIC X.                                          00001370
             03 MSELP   PIC X.                                          00001380
             03 MSELH   PIC X.                                          00001390
             03 MSELV   PIC X.                                          00001400
             03 MSELO   PIC X.                                          00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNBLA   PIC X.                                          00001430
             03 MNBLC   PIC X.                                          00001440
             03 MNBLP   PIC X.                                          00001450
             03 MNBLH   PIC X.                                          00001460
             03 MNBLV   PIC X.                                          00001470
             03 MNBLO   PIC X(10).                                      00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNFOURNISSEURA    PIC X.                                00001500
             03 MNFOURNISSEURC    PIC X.                                00001510
             03 MNFOURNISSEURP    PIC X.                                00001520
             03 MNFOURNISSEURH    PIC X.                                00001530
             03 MNFOURNISSEURV    PIC X.                                00001540
             03 MNFOURNISSEURO    PIC X(14).                            00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MNENTREPOTA  PIC X.                                     00001570
             03 MNENTREPOTC  PIC X.                                     00001580
             03 MNENTREPOTP  PIC X.                                     00001590
             03 MNENTREPOTH  PIC X.                                     00001600
             03 MNENTREPOTV  PIC X.                                     00001610
             03 MNENTREPOTO  PIC X(6).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MQACCEPTA    PIC X.                                     00001640
             03 MQACCEPTC    PIC X.                                     00001650
             03 MQACCEPTP    PIC X.                                     00001660
             03 MQACCEPTH    PIC X.                                     00001670
             03 MQACCEPTV    PIC X.                                     00001680
             03 MQACCEPTO    PIC X(5).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MDRECEPTA    PIC X.                                     00001710
             03 MDRECEPTC    PIC X.                                     00001720
             03 MDRECEPTP    PIC X.                                     00001730
             03 MDRECEPTH    PIC X.                                     00001740
             03 MDRECEPTV    PIC X.                                     00001750
             03 MDRECEPTO    PIC X(8).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MDENVOIOPA   PIC X.                                     00001780
             03 MDENVOIOPC   PIC X.                                     00001790
             03 MDENVOIOPP   PIC X.                                     00001800
             03 MDENVOIOPH   PIC X.                                     00001810
             03 MDENVOIOPV   PIC X.                                     00001820
             03 MDENVOIOPO   PIC X(8).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDRETOUROPA  PIC X.                                     00001850
             03 MDRETOUROPC  PIC X.                                     00001860
             03 MDRETOUROPP  PIC X.                                     00001870
             03 MDRETOUROPH  PIC X.                                     00001880
             03 MDRETOUROPV  PIC X.                                     00001890
             03 MDRETOUROPO  PIC X(8).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MDENVOIRXA   PIC X.                                     00001920
             03 MDENVOIRXC   PIC X.                                     00001930
             03 MDENVOIRXP   PIC X.                                     00001940
             03 MDENVOIRXH   PIC X.                                     00001950
             03 MDENVOIRXV   PIC X.                                     00001960
             03 MDENVOIRXO   PIC X(8).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(78).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
