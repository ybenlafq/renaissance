      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBNRE NUMERO RESEAU THD                *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBNRE.                                                             
           05  RBNRE-CTABLEG2    PIC X(15).                                     
           05  RBNRE-CTABLEG2-REDEF REDEFINES RBNRE-CTABLEG2.                   
               10  RBNRE-NRESEAU         PIC X(05).                             
           05  RBNRE-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBNRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBNRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBNRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBNRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBNRE-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
