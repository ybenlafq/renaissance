      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * MAP DE SAISIE DES HELPS                                         00000020
      ***************************************************************** 00000030
       01   EHLP0I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMMAPL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOMMAPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMMAPF  PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MNOMMAPI  PIC X(8).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000190
           02 FILLER    PIC X(2).                                       00000200
           02 MLIBERRI  PIC X(79).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000230
           02 FILLER    PIC X(2).                                       00000240
           02 MCODTRAI  PIC X(4).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000270
           02 FILLER    PIC X(2).                                       00000280
           02 MZONCMDI  PIC X(15).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000310
           02 FILLER    PIC X(2).                                       00000320
           02 MCICSI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000350
           02 FILLER    PIC X(2).                                       00000360
           02 MNETNAMI  PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000390
           02 FILLER    PIC X(2).                                       00000400
           02 MSCREENI  PIC X(4).                                       00000410
      ***************************************************************** 00000420
      * MAP DE SAISIE DES HELPS                                         00000430
      ***************************************************************** 00000440
       01   EHLP0O REDEFINES EHLP0I.                                    00000450
           02 FILLER    PIC X(12).                                      00000460
           02 FILLER    PIC X(2).                                       00000470
           02 MDATJOUA  PIC X.                                          00000480
           02 MDATJOUC  PIC X.                                          00000490
           02 MDATJOUH  PIC X.                                          00000500
           02 MDATJOUO  PIC X(10).                                      00000510
           02 FILLER    PIC X(2).                                       00000520
           02 MTIMJOUA  PIC X.                                          00000530
           02 MTIMJOUC  PIC X.                                          00000540
           02 MTIMJOUH  PIC X.                                          00000550
           02 MTIMJOUO  PIC X(5).                                       00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MNOMMAPA  PIC X.                                          00000580
           02 MNOMMAPC  PIC X.                                          00000590
           02 MNOMMAPH  PIC X.                                          00000600
           02 MNOMMAPO  PIC X(8).                                       00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MLIBERRA  PIC X.                                          00000630
           02 MLIBERRC  PIC X.                                          00000640
           02 MLIBERRH  PIC X.                                          00000650
           02 MLIBERRO  PIC X(79).                                      00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MCODTRAA  PIC X.                                          00000680
           02 MCODTRAC  PIC X.                                          00000690
           02 MCODTRAH  PIC X.                                          00000700
           02 MCODTRAO  PIC X(4).                                       00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MZONCMDA  PIC X.                                          00000730
           02 MZONCMDC  PIC X.                                          00000740
           02 MZONCMDH  PIC X.                                          00000750
           02 MZONCMDO  PIC X(15).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MCICSA    PIC X.                                          00000780
           02 MCICSC    PIC X.                                          00000790
           02 MCICSH    PIC X.                                          00000800
           02 MCICSO    PIC X(5).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MNETNAMA  PIC X.                                          00000830
           02 MNETNAMC  PIC X.                                          00000840
           02 MNETNAMH  PIC X.                                          00000850
           02 MNETNAMO  PIC X(8).                                       00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MSCREENA  PIC X.                                          00000880
           02 MSCREENC  PIC X.                                          00000890
           02 MSCREENH  PIC X.                                          00000900
           02 MSCREENO  PIC X(4).                                       00000910
                                                                                
