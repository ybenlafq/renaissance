      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE15   EHE15                                              00000020
      ***************************************************************** 00000030
       01   EHE15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCDEPOTI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLDEPOTI  PIC X(23).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATRETL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATRETF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATRETI  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCLIEHETI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLLIEHETI      PIC X(23).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCTRAITI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLTRAITI  PIC X(23).                                      00000490
           02 FILLER  OCCURS   12 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSELI   PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNLIEUI      PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNHSI   PIC X(7).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATHSL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDATHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATHSF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDATHSI      PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCODICI      PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MFAMI   PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MMRQI   PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MREFI   PIC X(20).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EHE15   EHE15                                              00001040
      ***************************************************************** 00001050
       01   EHE15O REDEFINES EHE15I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNUMPAGEA      PIC X.                                     00001230
           02 MNUMPAGEC PIC X.                                          00001240
           02 MNUMPAGEP PIC X.                                          00001250
           02 MNUMPAGEH PIC X.                                          00001260
           02 MNUMPAGEV PIC X.                                          00001270
           02 MNUMPAGEO      PIC X(2).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEMAXA      PIC X.                                     00001300
           02 MPAGEMAXC PIC X.                                          00001310
           02 MPAGEMAXP PIC X.                                          00001320
           02 MPAGEMAXH PIC X.                                          00001330
           02 MPAGEMAXV PIC X.                                          00001340
           02 MPAGEMAXO      PIC X(2).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCDEPOTA  PIC X.                                          00001370
           02 MCDEPOTC  PIC X.                                          00001380
           02 MCDEPOTP  PIC X.                                          00001390
           02 MCDEPOTH  PIC X.                                          00001400
           02 MCDEPOTV  PIC X.                                          00001410
           02 MCDEPOTO  PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLDEPOTA  PIC X.                                          00001440
           02 MLDEPOTC  PIC X.                                          00001450
           02 MLDEPOTP  PIC X.                                          00001460
           02 MLDEPOTH  PIC X.                                          00001470
           02 MLDEPOTV  PIC X.                                          00001480
           02 MLDEPOTO  PIC X(23).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MDATRETA  PIC X.                                          00001510
           02 MDATRETC  PIC X.                                          00001520
           02 MDATRETP  PIC X.                                          00001530
           02 MDATRETH  PIC X.                                          00001540
           02 MDATRETV  PIC X.                                          00001550
           02 MDATRETO  PIC X(10).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCLIEHETA      PIC X.                                     00001580
           02 MCLIEHETC PIC X.                                          00001590
           02 MCLIEHETP PIC X.                                          00001600
           02 MCLIEHETH PIC X.                                          00001610
           02 MCLIEHETV PIC X.                                          00001620
           02 MCLIEHETO      PIC X(5).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLLIEHETA      PIC X.                                     00001650
           02 MLLIEHETC PIC X.                                          00001660
           02 MLLIEHETP PIC X.                                          00001670
           02 MLLIEHETH PIC X.                                          00001680
           02 MLLIEHETV PIC X.                                          00001690
           02 MLLIEHETO      PIC X(23).                                 00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCTRAITA  PIC X.                                          00001720
           02 MCTRAITC  PIC X.                                          00001730
           02 MCTRAITP  PIC X.                                          00001740
           02 MCTRAITH  PIC X.                                          00001750
           02 MCTRAITV  PIC X.                                          00001760
           02 MCTRAITO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MLTRAITA  PIC X.                                          00001790
           02 MLTRAITC  PIC X.                                          00001800
           02 MLTRAITP  PIC X.                                          00001810
           02 MLTRAITH  PIC X.                                          00001820
           02 MLTRAITV  PIC X.                                          00001830
           02 MLTRAITO  PIC X(23).                                      00001840
           02 FILLER  OCCURS   12 TIMES .                               00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MSELA   PIC X.                                          00001870
             03 MSELC   PIC X.                                          00001880
             03 MSELP   PIC X.                                          00001890
             03 MSELH   PIC X.                                          00001900
             03 MSELV   PIC X.                                          00001910
             03 MSELO   PIC X.                                          00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MNLIEUA      PIC X.                                     00001940
             03 MNLIEUC PIC X.                                          00001950
             03 MNLIEUP PIC X.                                          00001960
             03 MNLIEUH PIC X.                                          00001970
             03 MNLIEUV PIC X.                                          00001980
             03 MNLIEUO      PIC X(3).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MNHSA   PIC X.                                          00002010
             03 MNHSC   PIC X.                                          00002020
             03 MNHSP   PIC X.                                          00002030
             03 MNHSH   PIC X.                                          00002040
             03 MNHSV   PIC X.                                          00002050
             03 MNHSO   PIC X(7).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MDATHSA      PIC X.                                     00002080
             03 MDATHSC PIC X.                                          00002090
             03 MDATHSP PIC X.                                          00002100
             03 MDATHSH PIC X.                                          00002110
             03 MDATHSV PIC X.                                          00002120
             03 MDATHSO      PIC X(10).                                 00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MCODICA      PIC X.                                     00002150
             03 MCODICC PIC X.                                          00002160
             03 MCODICP PIC X.                                          00002170
             03 MCODICH PIC X.                                          00002180
             03 MCODICV PIC X.                                          00002190
             03 MCODICO      PIC X(7).                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MFAMA   PIC X.                                          00002220
             03 MFAMC   PIC X.                                          00002230
             03 MFAMP   PIC X.                                          00002240
             03 MFAMH   PIC X.                                          00002250
             03 MFAMV   PIC X.                                          00002260
             03 MFAMO   PIC X(5).                                       00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MMRQA   PIC X.                                          00002290
             03 MMRQC   PIC X.                                          00002300
             03 MMRQP   PIC X.                                          00002310
             03 MMRQH   PIC X.                                          00002320
             03 MMRQV   PIC X.                                          00002330
             03 MMRQO   PIC X(5).                                       00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MREFA   PIC X.                                          00002360
             03 MREFC   PIC X.                                          00002370
             03 MREFP   PIC X.                                          00002380
             03 MREFH   PIC X.                                          00002390
             03 MREFV   PIC X.                                          00002400
             03 MREFO   PIC X(20).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(78).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
