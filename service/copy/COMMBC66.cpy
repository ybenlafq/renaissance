      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    RECHERCHE INFO CLOMPLEMENTAIRES POUR LE CLIENT                       
      * COMM DE PROGRAMME MBC66                                                 
      * APPELLE PAR LINK DANS TBC60                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MBC66.                                                     
      *      MESSAGE ERREUR DE LA CONTROLE SYNT +LOG+ MESSAGE MQ                
         05 COMM-MBC66-RETOUR-MQ.                                               
             10 COMM-MBC66-CODRETMQ      PIC X(01).                             
                  88  COMM-MBC66-MQ-OK     VALUE ' '.                           
                  88  COMM-MBC66-MQ-ERREUR VALUE '1'.                           
             10 COMM-MBC66-LIBERRMQ      PIC X(60).                             
         05 COMM-MBC66-RETOUR-BC.                                               
             10 COMM-MBC66-CODRETBC      PIC X(01).                             
                  88  COMM-MBC66-BC-OK     VALUE ' '.                           
                  88  COMM-MBC66-BC-ERREUR VALUE '1'.                           
             10 COMM-MBC66-LIBERRBC      PIC X(60).                             
      * CRITERES DE SELECTION CLIENT                                            
         05 COMM-MBC66-DATA.                                                    
              10 COMM-MBC66-DATA-SELECTION.                                     
      *          CLIENT CONCERN�                                                
                 15 COMM-MBC66-NCLIENTADR PIC X(08).                            
      *          TYPE DE QUESTION                                               
                 15 COMM-MBC66-SELECTION PIC X(01).                             
                    88 COMM-MBC66-SELECTION-RED VALUE 'R'.                      
                    88 COMM-MBC66-SELECTION-ORG VALUE 'O'.                      
      *          TYPE DE REPONSE DE MODULE MBC92 VERS HOST/AS400                
                 15 COMM-MBC66-TYPE     PIC X(03).                              
      *          DATA LIBRE                                                     
                 15 COMM-MBC66-SSAAMMJJ PIC X(08).                              
                 15 COMM-MBC66-NSOCCICS PIC X(03).                              
                 15 COMM-MBC66-NSOC     PIC X(03).                              
                 15 COMM-MBC66-NLIEU    PIC X(03).                              
                 15 COMM-MBC66-SFILLER  PIC X(088).                             
      * LONGUEUR DE 100 OCTETS                                                  
           05 COMM-MBC66-DATA-REPONSE.                                          
              10 COMM-MBC66-REDEVANCE   PIC X(01).                              
              10 COMM-MBC66-DNAISS      PIC X(08).                              
              10 COMM-MBC66-CPOSTAL     PIC X(05).                              
              10 COMM-MBC66-LCOMMUNE    PIC X(32).                              
              10 COMM-MBC66-DATA-FILLER PIC X(54).                              
           05 COMM-MBC66-DATA-REP-ORG REDEFINES COMM-MBC66-DATA-REPONSE.        
              10 COMM-MBC66-ORGANISATION PIC X(01).                             
              10 COMM-MBC66-TORG        PIC X(05).                              
              10 COMM-MBC66-LORG        PIC X(32).                              
              10 COMM-MBC66-DATA-FILORG PIC X(62).                              
      *                                                                         
                                                                                
