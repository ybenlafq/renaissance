      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-HT00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-HT00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-HT00-ENTETE.                                         00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION  .                               00000540
                    10 COMM-CODE-FONCTION      OCCURS 3.                   00000
                       15  COMM-CFONC     PIC  X(03).                           
                 05 COMM-CFONCTION-OPTION   PIC X(03).                          
                 05 COMM-COPTION            PIC X(02).                          
      *    02 COMM-HT00-APPLI.                                          00000500
           02 COMM-HT00-FILLER              PIC X(3854).                00000620
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000010
           02 COMM-HT00-APPLI REDEFINES COMM-HT00-FILLER.               00000040
      *    COMM-MC02-APPLI   EST LE MEME QUE HT00                       00000040
              03 COMM-00.                                               00000050
                 05 COMM-00-MODE-INTERO     PIC X(01).                          
                     88     00-MODE-INTERO    VALUE '1'.                        
                 05 COMM-00-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-00-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-00-NLIGNE          PIC S9(5) COMP-3.           00000070
      *        NOM    PROGRAMME    RETOUR LEVEL-MAX                             
                 05 COMM-00-LEVEL-MAX       PIC X(06).                  00000070
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-00-TYPE-TRT             PIC X .                       
                     88 CONSULT-ECRAN     VALUE ' '.                            
                     88 CREATION-GROUPE   VALUE '1'.                            
                     88 MAJ-GROUPE        VALUE '2'.                            
                 05 COMM-00-FILLER PIC X(97).                           00000550
      *    COMM-01                                                      00000040
              03 COMM-01.                                               00000550
                 05 COMM-01-MODE-INTERO     PIC X(01).                          
                     88  01-MODE-INTERO   VALUE '1'.                            
               04 COMM-01-DATA.                                         00000550
                 05 COMM-01-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-01-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-01-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-01-VAL-ENTETE.                                 00000090
                    10 COMM-01-CAPPLI          PIC X(05).               00000100
                    10 COMM-01-LAPPLI          PIC X(30).               00000100
              03 COMM-02.                                               00000550
                 05 COMM-02-MODE-INTERO     PIC X(01).                          
                     88  04-MODE-INTERO   VALUE '1'.                            
               04 COMM-02-DATA.                                         00000550
                 05 COMM-02-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-02-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-02-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-02-VAL-ENTETE.                                 00000090
                    10 COMM-02-CTRANSF         PIC X(05).               00000100
                    10 COMM-02-LTRANSF         PIC X(20).               00000100
                 05 COMM-02-IND-TS          PIC 9(04).                  00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
              03 COMM-00-FILLER PIC X(3656).                            00000550
                                                                                
                                                                        00000030
