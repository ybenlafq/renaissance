      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP03-RECORD.                                                      
           05 TS-AP03-TABLE            OCCURS 5.                                
              10 TS-AP03-NCLIENT        PIC X(08).                              
              10 TS-AP03-NMPRNM         PIC X(44).                              
              10 TS-AP03-ADR1           PIC X(25).                              
              10 TS-AP03-ADR2           PIC X(32).                              
              10 TS-AP03-ADR3           PIC X(30).                              
                                                                                
