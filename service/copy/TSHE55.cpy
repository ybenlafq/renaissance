      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *            TS DES PROGRAMMES THE55 ET MHE55                             
       01               :X:-NOM.                                                
            02          FILLER    PIC X(04) VALUE 'HE55'.                       
            02          :X:-TERM  PIC X(04) VALUE SPACES.                       
       01               :X:-DATA            VALUE SPACES.                       
              03        :X:-LIGNE                OCCURS 12.                     
                 04     :X:-NLIEUHED PIC X(03).                                 
                 04     :X:-NGHE     PIC X(07).                                 
                 04     :X:-NCODIC   PIC X(07).                                 
                 04     :X:-CFAM     PIC X(05).                                 
                 04     :X:-CMARQ    PIC X(05).                                 
                 04     :X:-LREFFOURN PIC X(20).                                
                 04     :X:-CLIEUHEI PIC X(05).                                 
                 04     :X:-NLOTDIAG PIC X(07).                                 
                 04     :X:-CPANNE   PIC X(05).                                 
                 04     :X:-CREFUS   PIC X(05).                                 
                 04     :X:-CTRAIT   PIC X(05).                                 
                 04     :X:-ORIG.                                               
                    05  :X:-NSOCIETE PIC X(03).                                 
                    05  :X:-NLIEU    PIC X(03).                                 
                    05  :X:-NSSLIEU  PIC X(03).                                 
                    05  :X:-CLIEUTRT PIC X(05).                                 
                 04     :X:-TOPANO   PIC X(01).                                 
      *               GESTION DE LA MODIF. DE CODIC                             
                 04     :X:-MAJ-NCODIC   PIC X(01).                             
                 04     :X:-NCODIC-NEW   PIC X(07).                             
                 04     :X:-CFAM-NEW     PIC X(05).                             
                 04     :X:-CMARQ-NEW    PIC X(05).                             
                 04     :X:-LREFFOURN-NEW PIC X(20).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01               TS-LONG     PIC S9(4) COMP VALUE 1560.                  
      *                                                                         
      *--                                                                       
       01               :X:-LONG     PIC S9(4) COMP-5 VALUE 1560.               
                                                                                
      *}                                                                        
