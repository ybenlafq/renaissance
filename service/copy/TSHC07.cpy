      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * TS SPECIFIQUE RTHC06                                       *    00020000
      *       TR : HC00  GESTION CENTRE HS                         *    00030000
      *       PG : THC07 SELECTION DES LOTS A ENVOYER              *    00040000
      **************************************************************    00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 648.             00080000
       01  TS-DONNEES.                                                  00090000
      *------------------------------ CODIC                             00100000
         05 TS-HORIZON          OCCURS 2.                               00110000
            07 TS-VERTICAL      OCCURS 12.                              00120000
      *------------------------------ TYPE D'ENVOI                      00130000
              10 TS-CTENVOI     PIC X(01).                              00140000
      *------------------------------ CODE TIERS                        00150000
              10 TS-CTIERS      PIC X(05).                              00160000
      *------------------------------ CODE SOCIETE                      00170000
              10 TS-NSOCIETE    PIC X(03).                              00180000
      *------------------------------ CODE LIEU                         00190000
              10 TS-NLIEU       PIC X(03).                              00200000
      *------------------------------ DATE ENVOI                        00210000
              10 TS-DENVOI      PIC X(08).                              00220000
      *------------------------------ N� ENVOI                          00230000
              10 TS-NENVOI      PIC X(07).                              00240000
                                                                                
