      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHE0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE0100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVHE0100.                                                            
           02  HE01-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE01-CMARQ                                                       
               PIC X(0005).                                                     
           02  HE01-CFAM                                                        
               PIC X(0005).                                                     
           02  HE01-WTRI                                                        
               PIC X(0001).                                                     
           02  HE01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HE01-CRENDU                                                      
               PIC X(0005).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHE0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-WTRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-WTRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE01-CRENDU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE01-CRENDU-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
