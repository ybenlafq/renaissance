      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1600                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1600.                                                            
           02  AP16-NSEQID         PIC S9(18) COMP-3.                           
           02  AP16-NDOSSIER       PIC X(0009).                                 
           02  AP16-NCRI           PIC X(0017).                                 
           02  AP16-NINTER         PIC X(0008).                                 
           02  AP16-CSTATUT        PIC X(0001).                                 
           02  AP16-NSOCSAV        PIC X(0003).                                 
           02  AP16-NLIEUSAV       PIC X(0003).                                 
           02  AP16-CREGIME        PIC X(0005).                                 
           02  AP16-NVENTE         PIC X(0014).                                 
           02  AP16-NLIGNE         PIC X(0003).                                 
           02  AP16-CPRESTA        PIC X(0005).                                 
           02  AP16-CTARIF         PIC X(0005).                                 
           02  AP16-NSOCA2I        PIC X(0003).                                 
           02  AP16-NMATRICULE     PIC X(0012).                                 
           02  AP16-LNOMTECH       PIC X(0030).                                 
           02  AP16-LPNOMTECH      PIC X(0030).                                 
           02  AP16-NCLIENT        PIC X(0008).                                 
           02  AP16-DPRESTA        PIC X(0008).                                 
           02  AP16-PPRESTA        PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP16-DATECRI        PIC X(0008).                                 
           02  AP16-TIMECRI        PIC X(0006).                                 
           02  AP16-DCREATION      PIC X(0008).                                 
           02  AP16-DCOMPTA        PIC X(0008).                                 
           02  AP16-DRAPPRO        PIC X(0008).                                 
           02  AP16-NATTEST        PIC S9(18)  USAGE COMP-3.                    
           02  AP16-WISPAYED       PIC X(0001).                                 
           02  AP16-DFACTURE       PIC X(0008).                                 
           02  AP16-CTITRENOM1     PIC X(0005).                                 
           02  AP16-LNOM1          PIC X(0025).                                 
           02  AP16-LPRENOM1       PIC X(0015).                                 
           02  AP16-LADR1A         PIC X(0038).                                 
           02  AP16-LADR1B         PIC X(0038).                                 
           02  AP16-LADR1C         PIC X(0038).                                 
           02  AP16-LADR1D         PIC X(0038).                                 
           02  AP16-CTITRENOM2     PIC X(0005).                                 
           02  AP16-LNOM2          PIC X(0025).                                 
           02  AP16-LPRENOM2       PIC X(0015).                                 
           02  AP16-LADR2A         PIC X(0038).                                 
           02  AP16-LADR2B         PIC X(0038).                                 
           02  AP16-LADR2C         PIC X(0038).                                 
           02  AP16-LADR2D         PIC X(0038).                                 
           02  AP16-CTXHORAIRE     PIC X(0005).                                 
           02  AP16-DUREE          PIC X(0040).                                 
           02  AP16-PFACTURE       PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP16-NUMFACT        PIC S9(06) COMP-3.                           
           02  AP16-CODEANO        PIC X(0002).                                 
           02  AP16-NIDANO         PIC S9(18)  USAGE COMP-3.                    
           02  AP16-DATEANO        PIC X(0008).                                 
           02  AP16-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NDOSSIER-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NDOSSIER-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NCRI-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NCRI-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NINTER-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NINTER-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CSTATUT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CSTATUT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NSOCSAV-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NSOCSAV-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NLIEUSAV-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NLIEUSAV-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CREGIME-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CREGIME-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NLIGNE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NLIGNE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CTARIF-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CTARIF-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NSOCA2I-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NSOCA2I-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NMATRICULE-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NMATRICULE-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LNOMTECH-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LNOMTECH-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LPNOMTECH-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LPNOMTECH-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NCLIENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NCLIENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-PPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-PPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DATECRI-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DATECRI-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-TIMECRI-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-TIMECRI-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DCREATION-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DCREATION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DCOMPTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DCOMPTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NATTEST-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NATTEST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-WISPAYED-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-WISPAYED-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DFACTURE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DFACTURE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CTITRENOM1-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CTITRENOM1-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LNOM1-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LNOM1-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LPRENOM1-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LPRENOM1-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR1A-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR1A-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR1B-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR1B-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR1C-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR1C-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR1D-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR1D-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CTITRENOM2-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CTITRENOM2-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LNOM2-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LNOM2-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LPRENOM2-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LPRENOM2-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR2A-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR2A-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR2B-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR2B-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR2C-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR2C-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-LADR2D-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-LADR2D-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CTXHORAIRE-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CTXHORAIRE-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-QNBHEURE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-QNBHEURE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-PFACTURE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-PFACTURE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NUMFACT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NUMFACT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-CODEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-CODEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-NIDANO-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-NIDANO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DATEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP16-DATEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP16-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP16-DSYST-F        PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
