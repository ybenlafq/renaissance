      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - LISTE FACTURES A2I                                       00000020
      ***************************************************************** 00000030
       01   EAP3AI.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
           02 MLIGNEI OCCURS   10 TIMES .                               00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MACTI   PIC X.                                          00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MLIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIGF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLIGI   PIC X(76).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLIBERRI  PIC X(79).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCODTRAI  PIC X(4).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MZONCMDI  PIC X(15).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCICSI    PIC X(5).                                       00000500
      * NETNAME                                                         00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNETNAMI  PIC X(8).                                       00000550
      * CODE TERMINAL                                                   00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MSCREENI  PIC X(4).                                       00000600
      ***************************************************************** 00000610
      * DA2I - LISTE FACTURES A2I                                       00000620
      ***************************************************************** 00000630
       01   EAP3AO REDEFINES EAP3AI.                                    00000640
           02 FILLER    PIC X(12).                                      00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MDATJOUA  PIC X.                                          00000670
           02 MDATJOUC  PIC X.                                          00000680
           02 MDATJOUP  PIC X.                                          00000690
           02 MDATJOUH  PIC X.                                          00000700
           02 MDATJOUV  PIC X.                                          00000710
           02 MDATJOUO  PIC X(10).                                      00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MTIMJOUA  PIC X.                                          00000740
           02 MTIMJOUC  PIC X.                                          00000750
           02 MTIMJOUP  PIC X.                                          00000760
           02 MTIMJOUH  PIC X.                                          00000770
           02 MTIMJOUV  PIC X.                                          00000780
           02 MTIMJOUO  PIC X(5).                                       00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MFONCA    PIC X.                                          00000810
           02 MFONCC    PIC X.                                          00000820
           02 MFONCP    PIC X.                                          00000830
           02 MFONCH    PIC X.                                          00000840
           02 MFONCV    PIC X.                                          00000850
           02 MFONCO    PIC X(3).                                       00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MPAGEA    PIC X.                                          00000880
           02 MPAGEC    PIC X.                                          00000890
           02 MPAGEP    PIC X.                                          00000900
           02 MPAGEH    PIC X.                                          00000910
           02 MPAGEV    PIC X.                                          00000920
           02 MPAGEO    PIC 99.                                         00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNBPA     PIC X.                                          00000950
           02 MNBPC     PIC X.                                          00000960
           02 MNBPP     PIC X.                                          00000970
           02 MNBPH     PIC X.                                          00000980
           02 MNBPV     PIC X.                                          00000990
           02 MNBPO     PIC 99.                                         00001000
           02 MLIGNEO OCCURS   10 TIMES .                               00001010
             03 FILLER       PIC X(2).                                  00001020
             03 MACTA   PIC X.                                          00001030
             03 MACTC   PIC X.                                          00001040
             03 MACTP   PIC X.                                          00001050
             03 MACTH   PIC X.                                          00001060
             03 MACTV   PIC X.                                          00001070
             03 MACTO   PIC X.                                          00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MLIGA   PIC X.                                          00001100
             03 MLIGC   PIC X.                                          00001110
             03 MLIGP   PIC X.                                          00001120
             03 MLIGH   PIC X.                                          00001130
             03 MLIGV   PIC X.                                          00001140
             03 MLIGO   PIC X(76).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MLIBERRA  PIC X.                                          00001170
           02 MLIBERRC  PIC X.                                          00001180
           02 MLIBERRP  PIC X.                                          00001190
           02 MLIBERRH  PIC X.                                          00001200
           02 MLIBERRV  PIC X.                                          00001210
           02 MLIBERRO  PIC X(79).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MCODTRAA  PIC X.                                          00001240
           02 MCODTRAC  PIC X.                                          00001250
           02 MCODTRAP  PIC X.                                          00001260
           02 MCODTRAH  PIC X.                                          00001270
           02 MCODTRAV  PIC X.                                          00001280
           02 MCODTRAO  PIC X(4).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MZONCMDA  PIC X.                                          00001310
           02 MZONCMDC  PIC X.                                          00001320
           02 MZONCMDP  PIC X.                                          00001330
           02 MZONCMDH  PIC X.                                          00001340
           02 MZONCMDV  PIC X.                                          00001350
           02 MZONCMDO  PIC X(15).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCICSA    PIC X.                                          00001380
           02 MCICSC    PIC X.                                          00001390
           02 MCICSP    PIC X.                                          00001400
           02 MCICSH    PIC X.                                          00001410
           02 MCICSV    PIC X.                                          00001420
           02 MCICSO    PIC X(5).                                       00001430
      * NETNAME                                                         00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNETNAMA  PIC X.                                          00001460
           02 MNETNAMC  PIC X.                                          00001470
           02 MNETNAMP  PIC X.                                          00001480
           02 MNETNAMH  PIC X.                                          00001490
           02 MNETNAMV  PIC X.                                          00001500
           02 MNETNAMO  PIC X(8).                                       00001510
      * CODE TERMINAL                                                   00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MSCREENA  PIC X.                                          00001540
           02 MSCREENC  PIC X.                                          00001550
           02 MSCREENP  PIC X.                                          00001560
           02 MSCREENH  PIC X.                                          00001570
           02 MSCREENV  PIC X.                                          00001580
           02 MSCREENO  PIC X(4).                                       00001590
                                                                                
