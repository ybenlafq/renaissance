      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVMC0001                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMC0001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMC0001.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 MC00-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 MC00-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 MC00-NVENTE          PIC X(7).                                    
      *                       NSEQNQ                                            
           10 MC00-NSEQNQ          PIC X(5).                                    
      *                       DATENC                                            
           10 MC00-DATENC          PIC X(8).                                    
      *                       IDCLIF                                            
           10 MC00-IDCLIF          PIC X(8).                                    
      *                       IDCLIL                                            
           10 MC00-IDCLIL          PIC X(8).                                    
      *                       CTITRENOM                                         
           10 MC00-CTITRENOM       PIC X(5).                                    
      *                       NOM                                               
           10 MC00-NOM             PIC X(25).                                   
      *                       PRENOM                                            
           10 MC00-PRENOM          PIC X(15).                                   
      *                       ADRESSE1                                          
           10 MC00-ADRESSE1        PIC X(50).                                   
      *                       ADRESSE2                                          
           10 MC00-ADRESSE2        PIC X(32).                                   
      *                       LBATIMENT                                         
           10 MC00-LBATIMENT       PIC X(3).                                    
      *                       LESCALIER                                         
           10 MC00-LESCALIER       PIC X(3).                                    
      *                       LETAGE                                            
           10 MC00-LETAGE          PIC X(3).                                    
      *                       LPORTE                                            
           10 MC00-LPORTE          PIC X(3).                                    
      *                       CPOSTAL                                           
           10 MC00-CPOSTAL         PIC X(5).                                    
      *                       CINSEE                                            
           10 MC00-CINSEE          PIC X(5).                                    
      *                       LCOMMUNE                                          
           10 MC00-LCOMMUNE        PIC X(32).                                   
      *                       TELDOM                                            
           10 MC00-TELDOM          PIC X(15).                                   
      *                       TELBUR                                            
           10 MC00-TELBUR          PIC X(15).                                   
      *                       NGSM                                              
           10 MC00-NGSM            PIC X(15).                                   
      *                       ADR_MAIL                                          
           10 MC00-ADR-MAIL        PIC X(50).                                   
      *                       RECH_SIEBEL                                       
           10 MC00-RECH-SIEBEL     PIC X(1).                                    
      *                       NCODIC                                            
           10 MC00-NCODIC          PIC X(7).                                    
      *                       CFAM                                              
           10 MC00-CFAM            PIC X(5).                                    
      *                       LFAM                                              
           10 MC00-LFAM            PIC X(20).                                   
      *                       CMARQ                                             
           10 MC00-CMARQ           PIC X(5).                                    
      *                       LMARQ                                             
           10 MC00-LMARQ           PIC X(20).                                   
      *                       LREF                                              
           10 MC00-LREF            PIC X(20).                                   
      *                       QVENDUE                                           
           10 MC00-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
      *                       CMODDEL                                           
           10 MC00-CMODDEL         PIC X(3).                                    
      *                       DDELIV                                            
           10 MC00-DDELIV          PIC X(8).                                    
      *                       DUREE                                             
           10 MC00-DUREE           PIC X(8).                                    
      *                       CPSE                                              
           10 MC00-CPSE            PIC X(5).                                    
      *                       PPSE                                              
           10 MC00-PPSE            PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVNET                                             
           10 MC00-PVNET           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DEXTRACT                                          
           10 MC00-DEXTRACT        PIC X(8).                                    
      *                       NVENTEA                                           
           10 MC00-NVENTEA         PIC X(18).                                   
      *                       DSYST                                             
           10 MC00-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       NSEQREF                                           
           10 MC00-NSEQREF         PIC X(5).                                    
      *                       CVENTE                                            
           10 MC00-CVENTE          PIC X(1).                                    
      *                       DTOPE                                             
           10 MC00-DTOPE           PIC X(8).                                    
      *                       DANNUL                                            
           10 MC00-DANNUL          PIC X(8).                                    
      *                       DVENTE                                            
           10 MC00-DVENTE          PIC X(8).                                    
      *                       LCTITRENOM                                        
           10 MC00-LCTITRENOM      PIC X(5).                                    
      *                       LNOM                                              
           10 MC00-LNOM            PIC X(25).                                   
      *                       LPRENOM                                           
           10 MC00-LPRENOM         PIC X(15).                                   
      *                       LADRESSE1                                         
           10 MC00-LADRESSE1       PIC X(50).                                   
      *                       LADRESSE2                                         
           10 MC00-LADRESSE2       PIC X(50).                                   
      *                       LCPOSTAL                                          
           10 MC00-LCPOSTAL        PIC X(5).                                    
      *                       LLCOMMUNE                                         
           10 MC00-LLCOMMUNE       PIC X(32).                                   
      *                       LTELDOM                                           
           10 MC00-LTELDOM         PIC X(15).                                   
      *                       LTELBUR                                           
           10 MC00-LTELBUR         PIC X(15).                                   
      *                       LNGSM                                             
           10 MC00-LNGSM           PIC X(15).                                   
      *                       NCODICGRP                                         
           10 MC00-NCODICGRP       PIC X(7).                                    
      *                       LREFGRP                                           
           10 MC00-LREFGRP         PIC X(20).                                   
      *                       LSTATCOMP                                         
           10 MC00-LSTATCOMP       PIC X(3).                                    
      *                       CMODPAIMT                                         
           10 MC00-CMODPAIMT       PIC X(5).                                    
      *                       MODDEL                                            
           10 MC00-MODDEL          PIC X(1).                                    
      *                       PVUNIT                                            
           10 MC00-PVUNIT          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       LIEN_PSE1                                         
           10 MC00-LIEN-PSE1       PIC X(5).                                    
      *                       PRIX_PSE1                                         
           10 MC00-PRIX-PSE1       PIC S9(5)V9(2) USAGE COMP-3.                 
      *                       DUREE_PSE1                                        
           10 MC00-DUREE-PSE1      PIC S9(3)V USAGE COMP-3.                     
      *                       LIEN_PSE2                                         
           10 MC00-LIEN-PSE2       PIC X(5).                                    
      *                       PRIX_PSE2                                         
           10 MC00-PRIX-PSE2       PIC S9(5)V9(2) USAGE COMP-3.                 
      *                       DUREE_PSE2                                        
           10 MC00-DUREE-PSE2      PIC S9(3)V USAGE COMP-3.                     
      *                       CCLIENT                                           
           10 MC00-CCLIENT         PIC X(9).                                    
      *                       SATISF                                            
           10 MC00-SATISF          PIC X(2).                                    
      *                       FLAG_PSE                                          
           10 MC00-FLAG-PSE        PIC X(1).                                    
      *                       LOGIN                                             
           10 MC00-LOGIN           PIC X(15).                                   
      *                       MDP                                               
           10 MC00-MDP             PIC X(15).                                   
      *                       TOPCOMPTE                                         
           10 MC00-TOPCOMPTE       PIC X(1).                                    
      *                       TOPIDCLI                                          
           10 MC00-TOPIDCLI        PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 74      *        
      ******************************************************************        
                                                                                
