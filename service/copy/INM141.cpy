      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM141 AU 03/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,08,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM141.                                                        
            05 NOMETAT-INM141           PIC X(6) VALUE 'INM141'.                
            05 RUPTURES-INM141.                                                 
           10 INM141-DATETR             PIC X(08).                      007  008
           10 INM141-NSOC               PIC X(03).                      015  003
           10 INM141-NLIEU              PIC X(03).                      018  003
           10 INM141-NCAISSE            PIC X(03).                      021  003
           10 INM141-NTRANS             PIC X(08).                      024  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM141-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 INM141-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM141.                                                   
           10 INM141-CDEVAUT            PIC X(03).                      034  003
           10 INM141-CDEVRF             PIC X(03).                      037  003
           10 INM141-CIMPAI             PIC X(16).                      040  016
           10 INM141-LDEVAUT            PIC X(05).                      056  005
           10 INM141-LDEVRF             PIC X(05).                      061  005
           10 INM141-CUMUL-AU-PDPAIE    PIC 9(10)V9(2).                 066  012
           10 INM141-PDPAIE             PIC S9(08)V9(2) COMP-3.         078  006
           10 INM141-DATED              PIC X(08).                      084  008
            05 FILLER                      PIC X(421).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM141-LONG           PIC S9(4)   COMP  VALUE +091.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM141-LONG           PIC S9(4) COMP-5  VALUE +091.           
                                                                                
      *}                                                                        
