      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIGP2   EIGP2                                              00000020
      ***************************************************************** 00000030
       01   EIGP2I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * APPLID CICS COURANT                                             00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MCICSI    PIC X(8).                                       00000150
      * HEURE                                                           00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MTIMJOUI  PIC X(10).                                      00000200
      * NETNAME                                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNETNAMI  PIC X(8).                                       00000250
      * TERMINAL USER                                                   00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MSCREENI  PIC X(4).                                       00000300
      * TACHE MENU                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCODTRAI  PIC X(4).                                       00000350
      * MAP BMS                                                         00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAPBMSL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MMAPBMSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAPBMSF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MMAPBMSI  PIC X(8).                                       00000400
      * PAGE DEBUT                                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGDEBL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPAGDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGDEBF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPAGDEBI  PIC X(3).                                       00000450
      * PAGE FIN                                                        00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGFINL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MPAGFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGFINF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MPAGFINI  PIC X(3).                                       00000500
           02 M3I OCCURS   12 TIMES .                                   00000510
      * TRAITEMENT                                                      00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRATEML     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MTRATEML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTRATEMF     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MTRATEMI     PIC X.                                     00000560
      * CODE PRT-TCT                                                    00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODTCTL     COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MCODTCTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODTCTF     PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MCODTCTI     PIC X(4).                                  00000610
      * CODE PRT-APPLID                                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAPPLIDL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MAPPLIDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAPPLIDF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MAPPLIDI     PIC X(8).                                  00000660
      * MESSAGE                                                         00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLIBERRI  PIC X(73).                                      00000710
      ***************************************************************** 00000720
      * SDF: EIGP2   EIGP2                                              00000730
      ***************************************************************** 00000740
       01   EIGP2O REDEFINES EIGP2I.                                    00000750
           02 FILLER    PIC X(12).                                      00000760
      * DATE DU JOUR                                                    00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MDATJOUA  PIC X.                                          00000790
           02 MDATJOUC  PIC X.                                          00000800
           02 MDATJOUP  PIC X.                                          00000810
           02 MDATJOUH  PIC X.                                          00000820
           02 MDATJOUV  PIC X.                                          00000830
           02 MDATJOUO  PIC X(10).                                      00000840
      * APPLID CICS COURANT                                             00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MCICSA    PIC X.                                          00000870
           02 MCICSC    PIC X.                                          00000880
           02 MCICSP    PIC X.                                          00000890
           02 MCICSH    PIC X.                                          00000900
           02 MCICSV    PIC X.                                          00000910
           02 MCICSO    PIC X(8).                                       00000920
      * HEURE                                                           00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MTIMJOUA  PIC X.                                          00000950
           02 MTIMJOUC  PIC X.                                          00000960
           02 MTIMJOUP  PIC X.                                          00000970
           02 MTIMJOUH  PIC X.                                          00000980
           02 MTIMJOUV  PIC X.                                          00000990
           02 MTIMJOUO  PIC X(10).                                      00001000
      * NETNAME                                                         00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNETNAMA  PIC X.                                          00001030
           02 MNETNAMC  PIC X.                                          00001040
           02 MNETNAMP  PIC X.                                          00001050
           02 MNETNAMH  PIC X.                                          00001060
           02 MNETNAMV  PIC X.                                          00001070
           02 MNETNAMO  PIC X(8).                                       00001080
      * TERMINAL USER                                                   00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MSCREENA  PIC X.                                          00001110
           02 MSCREENC  PIC X.                                          00001120
           02 MSCREENP  PIC X.                                          00001130
           02 MSCREENH  PIC X.                                          00001140
           02 MSCREENV  PIC X.                                          00001150
           02 MSCREENO  PIC X(4).                                       00001160
      * TACHE MENU                                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCODTRAA  PIC X.                                          00001190
           02 MCODTRAC  PIC X.                                          00001200
           02 MCODTRAP  PIC X.                                          00001210
           02 MCODTRAH  PIC X.                                          00001220
           02 MCODTRAV  PIC X.                                          00001230
           02 MCODTRAO  PIC X(4).                                       00001240
      * MAP BMS                                                         00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MMAPBMSA  PIC X.                                          00001270
           02 MMAPBMSC  PIC X.                                          00001280
           02 MMAPBMSP  PIC X.                                          00001290
           02 MMAPBMSH  PIC X.                                          00001300
           02 MMAPBMSV  PIC X.                                          00001310
           02 MMAPBMSO  PIC X(8).                                       00001320
      * PAGE DEBUT                                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGDEBA  PIC X.                                          00001350
           02 MPAGDEBC  PIC X.                                          00001360
           02 MPAGDEBP  PIC X.                                          00001370
           02 MPAGDEBH  PIC X.                                          00001380
           02 MPAGDEBV  PIC X.                                          00001390
           02 MPAGDEBO  PIC X(3).                                       00001400
      * PAGE FIN                                                        00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MPAGFINA  PIC X.                                          00001430
           02 MPAGFINC  PIC X.                                          00001440
           02 MPAGFINP  PIC X.                                          00001450
           02 MPAGFINH  PIC X.                                          00001460
           02 MPAGFINV  PIC X.                                          00001470
           02 MPAGFINO  PIC X(3).                                       00001480
           02 M3O OCCURS   12 TIMES .                                   00001490
      * TRAITEMENT                                                      00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MTRATEMA     PIC X.                                     00001520
             03 MTRATEMC     PIC X.                                     00001530
             03 MTRATEMP     PIC X.                                     00001540
             03 MTRATEMH     PIC X.                                     00001550
             03 MTRATEMV     PIC X.                                     00001560
             03 MTRATEMO     PIC X.                                     00001570
      * CODE PRT-TCT                                                    00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MCODTCTA     PIC X.                                     00001600
             03 MCODTCTC     PIC X.                                     00001610
             03 MCODTCTP     PIC X.                                     00001620
             03 MCODTCTH     PIC X.                                     00001630
             03 MCODTCTV     PIC X.                                     00001640
             03 MCODTCTO     PIC X(4).                                  00001650
      * CODE PRT-APPLID                                                 00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MAPPLIDA     PIC X.                                     00001680
             03 MAPPLIDC     PIC X.                                     00001690
             03 MAPPLIDP     PIC X.                                     00001700
             03 MAPPLIDH     PIC X.                                     00001710
             03 MAPPLIDV     PIC X.                                     00001720
             03 MAPPLIDO     PIC X(8).                                  00001730
      * MESSAGE                                                         00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLIBERRA  PIC X.                                          00001760
           02 MLIBERRC  PIC X.                                          00001770
           02 MLIBERRP  PIC X.                                          00001780
           02 MLIBERRH  PIC X.                                          00001790
           02 MLIBERRV  PIC X.                                          00001800
           02 MLIBERRO  PIC X(73).                                      00001810
                                                                                
