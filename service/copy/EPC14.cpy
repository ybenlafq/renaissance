      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: consult mvts sur les cartes                                00000020
      ***************************************************************** 00000030
       01   EPC14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVTL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MLMVTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMVTF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLMVTI    PIC X(20).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMNTDARTYL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MMNTDARTYL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MMNTDARTYF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MMNTDARTYI     PIC X(9).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEPAR1L  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSEPAR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEPAR1F  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSEPAR1I  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMNTEUROSL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MMNTEUROSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MMNTEUROSF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MMNTEUROSI     PIC X(9).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRANDOMNUML    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MRANDOMNUML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MRANDOMNUMF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MRANDOMNUMI    PIC X(16).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADDARTYL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MADDARTYL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADDARTYF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MADDARTYI      PIC X(10).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAHDARTYL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MAHDARTYL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MAHDARTYF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MAHDARTYI      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERIALNUML    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSERIALNUML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSERIALNUMF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSERIALNUMI    PIC X(13).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANSOCVTEL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MANSOCVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MANSOCVTEF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MANSOCVTEI     PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MALIEUVTEL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MALIEUVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MALIEUVTEF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MALIEUVTEI     PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANVENTEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MANVENTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MANVENTEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MANVENTEI      PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRTYPL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPRTYPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRTYPF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPRTYPI   PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAMNTDARTYL    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MAMNTDARTYL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MAMNTDARTYF    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MAMNTDARTYI    PIC X(8).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAMNTEUROSL    COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MAMNTEUROSL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MAMNTEUROSF    PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MAMNTEUROSI    PIC X(8).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSTATUTI  PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRESTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLPRESTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPRESTF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLPRESTI  PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALIDFL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MDVALIDFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALIDFF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDVALIDFI      PIC X(8).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALIDRL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MDVALIDRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDVALIDRF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDVALIDRI      PIC X(8).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMAPL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCMAPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCMAPF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCMAPI    PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDARTYL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MDDARTYL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDARTYF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDDARTYI  PIC X(10).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHDARTYL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MHDARTYL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MHDARTYF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MHDARTYI  PIC X(7).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCORDL      COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MDACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACCORDF      PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDACCORDI      PIC X(10).                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHACCORDL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MHACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MHACCORDF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MHACCORDI      PIC X(7).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNSOCI    PIC X(3).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNLIEUI   PIC X(3).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNVENTEI  PIC X(8).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCANALL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCANALL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCANALF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCANALI   PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNTRANSI  PIC X(10).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNACCORDI      PIC X(10).                                 00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISSEL      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MNCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCAISSEF      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNCAISSEI      PIC X(3).                                  00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUTILL   COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCUTILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCUTILF   PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCUTILI   PIC X(18).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOLDDARTYL    COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MSOLDDARTYL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSOLDDARTYF    PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MSOLDDARTYI    PIC X(9).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEPAR2L  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MSEPAR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEPAR2F  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MSEPAR2I  PIC X.                                          00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOLDDEURL     COMP PIC S9(4).                            00001460
      *--                                                                       
           02 MSOLDDEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSOLDDEURF     PIC X.                                     00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MSOLDDEURI     PIC X(9).                                  00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOLDPARL      COMP PIC S9(4).                            00001500
      *--                                                                       
           02 MSOLDPARL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOLDPARF      PIC X.                                     00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MSOLDPARI      PIC X(9).                                  00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MLIBERRI  PIC X(78).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCODTRAI  PIC X(4).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCICSI    PIC X(5).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MNETNAMI  PIC X(8).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MSCREENI  PIC X(4).                                       00001730
      ***************************************************************** 00001740
      * SDF: consult mvts sur les cartes                                00001750
      ***************************************************************** 00001760
       01   EPC14O REDEFINES EPC14I.                                    00001770
           02 FILLER    PIC X(12).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MDATJOUA  PIC X.                                          00001800
           02 MDATJOUC  PIC X.                                          00001810
           02 MDATJOUP  PIC X.                                          00001820
           02 MDATJOUH  PIC X.                                          00001830
           02 MDATJOUV  PIC X.                                          00001840
           02 MDATJOUO  PIC X(10).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MTIMJOUA  PIC X.                                          00001870
           02 MTIMJOUC  PIC X.                                          00001880
           02 MTIMJOUP  PIC X.                                          00001890
           02 MTIMJOUH  PIC X.                                          00001900
           02 MTIMJOUV  PIC X.                                          00001910
           02 MTIMJOUO  PIC X(5).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MLMVTA    PIC X.                                          00001940
           02 MLMVTC    PIC X.                                          00001950
           02 MLMVTP    PIC X.                                          00001960
           02 MLMVTH    PIC X.                                          00001970
           02 MLMVTV    PIC X.                                          00001980
           02 MLMVTO    PIC X(20).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MMNTDARTYA     PIC X.                                     00002010
           02 MMNTDARTYC     PIC X.                                     00002020
           02 MMNTDARTYP     PIC X.                                     00002030
           02 MMNTDARTYH     PIC X.                                     00002040
           02 MMNTDARTYV     PIC X.                                     00002050
           02 MMNTDARTYO     PIC X(9).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MSEPAR1A  PIC X.                                          00002080
           02 MSEPAR1C  PIC X.                                          00002090
           02 MSEPAR1P  PIC X.                                          00002100
           02 MSEPAR1H  PIC X.                                          00002110
           02 MSEPAR1V  PIC X.                                          00002120
           02 MSEPAR1O  PIC X.                                          00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MMNTEUROSA     PIC X.                                     00002150
           02 MMNTEUROSC     PIC X.                                     00002160
           02 MMNTEUROSP     PIC X.                                     00002170
           02 MMNTEUROSH     PIC X.                                     00002180
           02 MMNTEUROSV     PIC X.                                     00002190
           02 MMNTEUROSO     PIC X(9).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MRANDOMNUMA    PIC X.                                     00002220
           02 MRANDOMNUMC    PIC X.                                     00002230
           02 MRANDOMNUMP    PIC X.                                     00002240
           02 MRANDOMNUMH    PIC X.                                     00002250
           02 MRANDOMNUMV    PIC X.                                     00002260
           02 MRANDOMNUMO    PIC X(16).                                 00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MADDARTYA      PIC X.                                     00002290
           02 MADDARTYC PIC X.                                          00002300
           02 MADDARTYP PIC X.                                          00002310
           02 MADDARTYH PIC X.                                          00002320
           02 MADDARTYV PIC X.                                          00002330
           02 MADDARTYO      PIC X(10).                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MAHDARTYA      PIC X.                                     00002360
           02 MAHDARTYC PIC X.                                          00002370
           02 MAHDARTYP PIC X.                                          00002380
           02 MAHDARTYH PIC X.                                          00002390
           02 MAHDARTYV PIC X.                                          00002400
           02 MAHDARTYO      PIC X(7).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MSERIALNUMA    PIC X.                                     00002430
           02 MSERIALNUMC    PIC X.                                     00002440
           02 MSERIALNUMP    PIC X.                                     00002450
           02 MSERIALNUMH    PIC X.                                     00002460
           02 MSERIALNUMV    PIC X.                                     00002470
           02 MSERIALNUMO    PIC X(13).                                 00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MANSOCVTEA     PIC X.                                     00002500
           02 MANSOCVTEC     PIC X.                                     00002510
           02 MANSOCVTEP     PIC X.                                     00002520
           02 MANSOCVTEH     PIC X.                                     00002530
           02 MANSOCVTEV     PIC X.                                     00002540
           02 MANSOCVTEO     PIC X(3).                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MALIEUVTEA     PIC X.                                     00002570
           02 MALIEUVTEC     PIC X.                                     00002580
           02 MALIEUVTEP     PIC X.                                     00002590
           02 MALIEUVTEH     PIC X.                                     00002600
           02 MALIEUVTEV     PIC X.                                     00002610
           02 MALIEUVTEO     PIC X(3).                                  00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MANVENTEA      PIC X.                                     00002640
           02 MANVENTEC PIC X.                                          00002650
           02 MANVENTEP PIC X.                                          00002660
           02 MANVENTEH PIC X.                                          00002670
           02 MANVENTEV PIC X.                                          00002680
           02 MANVENTEO      PIC X(7).                                  00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MPRTYPA   PIC X.                                          00002710
           02 MPRTYPC   PIC X.                                          00002720
           02 MPRTYPP   PIC X.                                          00002730
           02 MPRTYPH   PIC X.                                          00002740
           02 MPRTYPV   PIC X.                                          00002750
           02 MPRTYPO   PIC X(20).                                      00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MAMNTDARTYA    PIC X.                                     00002780
           02 MAMNTDARTYC    PIC X.                                     00002790
           02 MAMNTDARTYP    PIC X.                                     00002800
           02 MAMNTDARTYH    PIC X.                                     00002810
           02 MAMNTDARTYV    PIC X.                                     00002820
           02 MAMNTDARTYO    PIC X(8).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MAMNTEUROSA    PIC X.                                     00002850
           02 MAMNTEUROSC    PIC X.                                     00002860
           02 MAMNTEUROSP    PIC X.                                     00002870
           02 MAMNTEUROSH    PIC X.                                     00002880
           02 MAMNTEUROSV    PIC X.                                     00002890
           02 MAMNTEUROSO    PIC X(8).                                  00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MSTATUTA  PIC X.                                          00002920
           02 MSTATUTC  PIC X.                                          00002930
           02 MSTATUTP  PIC X.                                          00002940
           02 MSTATUTH  PIC X.                                          00002950
           02 MSTATUTV  PIC X.                                          00002960
           02 MSTATUTO  PIC X(5).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MLPRESTA  PIC X.                                          00002990
           02 MLPRESTC  PIC X.                                          00003000
           02 MLPRESTP  PIC X.                                          00003010
           02 MLPRESTH  PIC X.                                          00003020
           02 MLPRESTV  PIC X.                                          00003030
           02 MLPRESTO  PIC X(20).                                      00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MDVALIDFA      PIC X.                                     00003060
           02 MDVALIDFC PIC X.                                          00003070
           02 MDVALIDFP PIC X.                                          00003080
           02 MDVALIDFH PIC X.                                          00003090
           02 MDVALIDFV PIC X.                                          00003100
           02 MDVALIDFO      PIC X(8).                                  00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MDVALIDRA      PIC X.                                     00003130
           02 MDVALIDRC PIC X.                                          00003140
           02 MDVALIDRP PIC X.                                          00003150
           02 MDVALIDRH PIC X.                                          00003160
           02 MDVALIDRV PIC X.                                          00003170
           02 MDVALIDRO      PIC X(8).                                  00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MCMAPA    PIC X.                                          00003200
           02 MCMAPC    PIC X.                                          00003210
           02 MCMAPP    PIC X.                                          00003220
           02 MCMAPH    PIC X.                                          00003230
           02 MCMAPV    PIC X.                                          00003240
           02 MCMAPO    PIC X(8).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MDDARTYA  PIC X.                                          00003270
           02 MDDARTYC  PIC X.                                          00003280
           02 MDDARTYP  PIC X.                                          00003290
           02 MDDARTYH  PIC X.                                          00003300
           02 MDDARTYV  PIC X.                                          00003310
           02 MDDARTYO  PIC X(10).                                      00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MHDARTYA  PIC X.                                          00003340
           02 MHDARTYC  PIC X.                                          00003350
           02 MHDARTYP  PIC X.                                          00003360
           02 MHDARTYH  PIC X.                                          00003370
           02 MHDARTYV  PIC X.                                          00003380
           02 MHDARTYO  PIC X(7).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MDACCORDA      PIC X.                                     00003410
           02 MDACCORDC PIC X.                                          00003420
           02 MDACCORDP PIC X.                                          00003430
           02 MDACCORDH PIC X.                                          00003440
           02 MDACCORDV PIC X.                                          00003450
           02 MDACCORDO      PIC X(10).                                 00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MHACCORDA      PIC X.                                     00003480
           02 MHACCORDC PIC X.                                          00003490
           02 MHACCORDP PIC X.                                          00003500
           02 MHACCORDH PIC X.                                          00003510
           02 MHACCORDV PIC X.                                          00003520
           02 MHACCORDO      PIC X(7).                                  00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MNSOCA    PIC X.                                          00003550
           02 MNSOCC    PIC X.                                          00003560
           02 MNSOCP    PIC X.                                          00003570
           02 MNSOCH    PIC X.                                          00003580
           02 MNSOCV    PIC X.                                          00003590
           02 MNSOCO    PIC X(3).                                       00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MNLIEUA   PIC X.                                          00003620
           02 MNLIEUC   PIC X.                                          00003630
           02 MNLIEUP   PIC X.                                          00003640
           02 MNLIEUH   PIC X.                                          00003650
           02 MNLIEUV   PIC X.                                          00003660
           02 MNLIEUO   PIC X(3).                                       00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MNVENTEA  PIC X.                                          00003690
           02 MNVENTEC  PIC X.                                          00003700
           02 MNVENTEP  PIC X.                                          00003710
           02 MNVENTEH  PIC X.                                          00003720
           02 MNVENTEV  PIC X.                                          00003730
           02 MNVENTEO  PIC X(8).                                       00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MCANALA   PIC X.                                          00003760
           02 MCANALC   PIC X.                                          00003770
           02 MCANALP   PIC X.                                          00003780
           02 MCANALH   PIC X.                                          00003790
           02 MCANALV   PIC X.                                          00003800
           02 MCANALO   PIC X(5).                                       00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MNTRANSA  PIC X.                                          00003830
           02 MNTRANSC  PIC X.                                          00003840
           02 MNTRANSP  PIC X.                                          00003850
           02 MNTRANSH  PIC X.                                          00003860
           02 MNTRANSV  PIC X.                                          00003870
           02 MNTRANSO  PIC X(10).                                      00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MNACCORDA      PIC X.                                     00003900
           02 MNACCORDC PIC X.                                          00003910
           02 MNACCORDP PIC X.                                          00003920
           02 MNACCORDH PIC X.                                          00003930
           02 MNACCORDV PIC X.                                          00003940
           02 MNACCORDO      PIC X(10).                                 00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MNCAISSEA      PIC X.                                     00003970
           02 MNCAISSEC PIC X.                                          00003980
           02 MNCAISSEP PIC X.                                          00003990
           02 MNCAISSEH PIC X.                                          00004000
           02 MNCAISSEV PIC X.                                          00004010
           02 MNCAISSEO      PIC X(3).                                  00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCUTILA   PIC X.                                          00004040
           02 MCUTILC   PIC X.                                          00004050
           02 MCUTILP   PIC X.                                          00004060
           02 MCUTILH   PIC X.                                          00004070
           02 MCUTILV   PIC X.                                          00004080
           02 MCUTILO   PIC X(18).                                      00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MSOLDDARTYA    PIC X.                                     00004110
           02 MSOLDDARTYC    PIC X.                                     00004120
           02 MSOLDDARTYP    PIC X.                                     00004130
           02 MSOLDDARTYH    PIC X.                                     00004140
           02 MSOLDDARTYV    PIC X.                                     00004150
           02 MSOLDDARTYO    PIC X(9).                                  00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MSEPAR2A  PIC X.                                          00004180
           02 MSEPAR2C  PIC X.                                          00004190
           02 MSEPAR2P  PIC X.                                          00004200
           02 MSEPAR2H  PIC X.                                          00004210
           02 MSEPAR2V  PIC X.                                          00004220
           02 MSEPAR2O  PIC X.                                          00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSOLDDEURA     PIC X.                                     00004250
           02 MSOLDDEURC     PIC X.                                     00004260
           02 MSOLDDEURP     PIC X.                                     00004270
           02 MSOLDDEURH     PIC X.                                     00004280
           02 MSOLDDEURV     PIC X.                                     00004290
           02 MSOLDDEURO     PIC X(9).                                  00004300
           02 FILLER    PIC X(2).                                       00004310
           02 MSOLDPARA      PIC X.                                     00004320
           02 MSOLDPARC PIC X.                                          00004330
           02 MSOLDPARP PIC X.                                          00004340
           02 MSOLDPARH PIC X.                                          00004350
           02 MSOLDPARV PIC X.                                          00004360
           02 MSOLDPARO      PIC X(9).                                  00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MLIBERRA  PIC X.                                          00004390
           02 MLIBERRC  PIC X.                                          00004400
           02 MLIBERRP  PIC X.                                          00004410
           02 MLIBERRH  PIC X.                                          00004420
           02 MLIBERRV  PIC X.                                          00004430
           02 MLIBERRO  PIC X(78).                                      00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MCODTRAA  PIC X.                                          00004460
           02 MCODTRAC  PIC X.                                          00004470
           02 MCODTRAP  PIC X.                                          00004480
           02 MCODTRAH  PIC X.                                          00004490
           02 MCODTRAV  PIC X.                                          00004500
           02 MCODTRAO  PIC X(4).                                       00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MCICSA    PIC X.                                          00004530
           02 MCICSC    PIC X.                                          00004540
           02 MCICSP    PIC X.                                          00004550
           02 MCICSH    PIC X.                                          00004560
           02 MCICSV    PIC X.                                          00004570
           02 MCICSO    PIC X(5).                                       00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MNETNAMA  PIC X.                                          00004600
           02 MNETNAMC  PIC X.                                          00004610
           02 MNETNAMP  PIC X.                                          00004620
           02 MNETNAMH  PIC X.                                          00004630
           02 MNETNAMV  PIC X.                                          00004640
           02 MNETNAMO  PIC X(8).                                       00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MSCREENA  PIC X.                                          00004670
           02 MSCREENC  PIC X.                                          00004680
           02 MSCREENP  PIC X.                                          00004690
           02 MSCREENH  PIC X.                                          00004700
           02 MSCREENV  PIC X.                                          00004710
           02 MSCREENO  PIC X(4).                                       00004720
                                                                                
