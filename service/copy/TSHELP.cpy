      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  NOM-TS-MAP.                                                          
           05  FILLER             PIC X(4)      VALUE 'HLPM'.                   
           05  TS-MAP-TRMID       PIC X(4)      VALUE SPACES.                   
       01  TS-HLP-ECRAN.                                                        
           05  TS-CDIV.                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TS-LGMESS          PIC S9(4)     COMP  VALUE ZERO.           
      *--                                                                       
               10  TS-LGMESS          PIC S9(4) COMP-5  VALUE ZERO.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TS-CURPOS          PIC S9(4)     COMP  VALUE ZERO.           
      *--                                                                       
               10  TS-CURPOS          PIC S9(4) COMP-5  VALUE ZERO.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TS-ADRCHAMP        PIC S9(4)     COMP  VALUE ZERO.           
      *--                                                                       
               10  TS-ADRCHAMP        PIC S9(4) COMP-5  VALUE ZERO.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TS-LGCHAMP         PIC S9(4)     COMP  VALUE ZERO.           
      *--                                                                       
               10  TS-LGCHAMP         PIC S9(4) COMP-5  VALUE ZERO.             
      *}                                                                        
               10  TS-SCRNATT.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  TS-SCRNWD          PIC S9(4) COMP  VALUE ZERO.   .       
      *--                                                                       
                   15  TS-SCRNWD          PIC S9(4) COMP-5  VALUE ZERO.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  TS-SCRNHT          PIC S9(4) COMP  VALUE ZERO.   .       
      *--                                                                       
                   15  TS-SCRNHT          PIC S9(4) COMP-5  VALUE ZERO.         
      *}                                                                        
      *                                                                         
                   15  TS-DCOLOR          PIC X   VALUE LOW-VALUE.              
                   15  TS-BLUE            PIC X   VALUE LOW-VALUE.              
                   15  TS-RED             PIC X   VALUE LOW-VALUE.              
                   15  TS-PINK            PIC X   VALUE LOW-VALUE.              
                   15  TS-GREEN           PIC X   VALUE LOW-VALUE.              
                   15  TS-TURQ            PIC X   VALUE LOW-VALUE.              
                   15  TS-YELLOW          PIC X   VALUE LOW-VALUE.              
                   15  TS-NEUTR           PIC X   VALUE LOW-VALUE.              
      *                                                                         
                   15  TS-DHILI           PIC X   VALUE LOW-VALUE.              
                   15  TS-BLINK           PIC X   VALUE LOW-VALUE.              
                   15  TS-REVRS           PIC X   VALUE LOW-VALUE.              
                   15  TS-UNDLN           PIC X   VALUE LOW-VALUE.              
      *                                                                         
                   15  TS-APLTXT          PIC X   VALUE LOW-VALUE.              
                   15  TS-FSET            PIC X   VALUE LOW-VALUE.              
                   15  TS-14BITS          PIC X   VALUE LOW-VALUE.              
           05  TS-MESSAGE         PIC X(4096)  VALUE LOW-VALUE.                 
                                                                                
