      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TBC51 (TBC51 -> TBC61)   TR: BC51  *    00020000
      *                 CONSULTATION CLIENTS B2B                   *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 5000  00050000
      *                                                                 00060000
      *            TRANSACTION BC51 : CONSULTATIONS CLIENTS B2B    *    00070000
      *                                                                 00080000
      **************************************************************    00090000
      * COMMAREA SPECIFIQUE PRG TBC51 (MENU) LISTE       TR: BC51  *    00100000
      *                          TBC61       FICHE                 *    00110000
      *                                                            *    00120000
      *                                                            *    00130000
      **************************************************************    00140000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00150000
      **************************************************************    00160000
      *                                                                 00170000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00180000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00190000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00200000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00210000
      *                                                                 00220000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +5372    00230000
      * COMPRENANT :                                                    00240000
      * 1 - LES ZONES RESERVEES A AIDA                                  00250000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00260000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00270000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00280000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00290000
      *                                                                 00300000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00310000
      * PAR AIDA                                                        00320000
      *                                                                 00330000
      *---------------------------------372 + 3724  ----------------    00340000
      *                                                                 00350000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-BC51-LONG-COMMAREA PIC S9(4) COMP VALUE +5372.           00360000
      *--                                                                       
       01  COM-BC51-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +5372.                 
      *}                                                                        
      *                                                                 00370000
       01  Z-COMMAREA.                                                  00380000
      *                                                                 00390000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00400000
          02 FILLER-COM-AIDA      PIC X(100).                           00410000
      *                                                                 00420000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00430000
          02 COMM-CICS-APPLID     PIC X(8).                             00440000
          02 COMM-CICS-NETNAM     PIC X(8).                             00450000
          02 COMM-CICS-TRANSA     PIC X(4).                             00460000
      *                                                                 00470000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00480000
          02 COMM-DATE-SIECLE     PIC XX.                               00490000
          02 COMM-DATE-ANNEE      PIC XX.                               00500000
          02 COMM-DATE-MOIS       PIC XX.                               00510000
          02 COMM-DATE-JOUR       PIC XX.                               00520000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00530000
                                                                        00540000
          02 COMM-DATE-QNTA       PIC 999.                              00550000
          02 COMM-DATE-QNT0       PIC 99999.                            00560000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00570000
          02 COMM-DATE-BISX       PIC 9.                                00580000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00590000
          02 COMM-DATE-JSM        PIC 9.                                00600000
      *   LIBELLES DU JOUR COURT - LONG                                 00610000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00620000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00630000
      *   LIBELLES DU MOIS COURT - LONG                                 00640000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00650000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00660000
      *   DIFFERENTES FORMES DE DATE                                    00670000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00680000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00690000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00700000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00710000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00720000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00730000
      *   DIFFERENTES FORMES DE DATE                                    00740000
INTERF*   02 COMM-DATE-FILLER     PIC X(14).                            00750000
INTERF    02 COMM-BC51-INTERFIL   PIC X(01).                            00751000
INTERF    02 COMM-DATE-FILLER     PIC X(13).                            00752000
      *                                                                 00760000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00770000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 BC51-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00790000
      *                                                                 00800000
      ***************************************************************** 00810000
      *                                                                 00820000
      * ZONES RESERVEES APPLICATIVES ---------------------4000--- 3724  00830000
      *                                                                 00840000
      *            TRANSACTION BC51 : CONSULTATION    CLIENTS  B2B    * 00850000
      *                                                                 00860000
      ***************************************************************** 00870000
          02 COMM-BC51-APPLI .                                          00880000
      *------------------------------ ZONE DONNEES TBC51                00890000
          05 COMM-BC51-DONNEES-TBC51.                                   00900000
      *------CRITERES DE SELECTION-----------------           283       00910000
      *-                              NUMERO  CARTE             0       00920000
             10 COMM-BC51-NCARTE     PIC X(09).                         00930000
      *-                              LIBELLE NOM                       00940000
             10 COMM-BC51-LNOM       PIC X(60).                         00950000
      *-                              NUM  CLIETN = NUM COMPTE          00960000
             10 COMM-BC51-NCLIENTADR PIC X(08).                         00970000
      *-                              NUM  SIRET                        00980000
             10 COMM-BC51-NSIRET     PIC X(14).                         00990000
      *-                              CODE POSTAL                       01000000
             10 COMM-BC51-CPOST      PIC X(05).                         01010000
      *-                              LIBELLE VILLE                     01020000
             10 COMM-BC51-LCOMN      PIC X(32).                         01030000
      *-                                 CODE PAYS PLAQUES NUMEROLOGIQUE01040000
             10 COMM-BC51-CPAYS      PIC X(03).                         01050000
      *-                                 CODE PAYS AFNOR                01060000
             10 COMM-BC51-TPAYS      PIC X(03).                         01070000
      *-                              LIBELLE PAYS                      01080000
             10 COMM-BC51-LPAYS      PIC X(15).                         01090000
      *-                              NUM  DE TELEPHONE                 01100000
             10 COMM-BC51-NTDOM      PIC X(15).                         01110000
      *                               LONGEUR NUM TEL (SSTABLE PAYS)    01120000
             10 COMM-BC51-LONGTEL    PIC X(02).                         01130000
      *                               CODE NSOC DU CICS                 01140000
             10 COMM-BC51-NSOCCICS   PIC X(03).                         01150000
      *-                              NUM  DE SOCIETE                   01160000
             10 COMM-BC51-NSOC       PIC X(3).                          01170000
      *-                              NUM  DE LIEU                      01180000
             10 COMM-BC51-NLIEU      PIC X(3).                          01190000
      *                               AFICHAGE COMMUNE SUR L'ECRAN      01200000
             10 COMM-BC51-COMN-MODIF     PIC X(01).                     01210000
      *                               LA COMMUNNE ETAIS FORCEE (''/O)   01220000
             10 COMM-BC51-COMN-FORCEE    PIC X(01).                     01230000
      *                               LA COMMUNNE N'EXISTE PAS          01240000
             10 COMM-BC51-COMN-ERREUR    PIC X(01).                     01250000
                                                                        01260000
                                                                        01270000
      *------ZONES  GESTION ECRAN - PAGINATION                 16       01280000
          05 COMM-BC51-EBC51.                                           01290000
      *-                              NUMERO PAGE COURANTE              01300000
             10 COMM-BC51-NUMPAG    PIC 9(03).                          01310000
      *-                              NUMERO PAGE MAXI                  01320000
             10 COMM-BC51-PAGE-MAX  PIC 9(03).                          01330000
      *-                              NUMERO PAGE ECRAN                 01340000
             10 COMM-BC51-ECRAN     PIC 9(03).                          01350000
      *-                              NUMERO PAGE ECRAN MAXI            01360000
             10 COMM-BC51-ECRAN-MAX PIC 9(03).                          01370000
      *-                              NB LIGNES                         01380000
             10 COMM-BC51-I-CHOIX-MAX  PIC 9(02).                       01390000
      *-                              NUMERO PAGE ECRAN MAXI            01400000
             10 COMM-BC51-I-CHOIX   PIC 9(02).                          01410000
      *-                              NUM TS                            01420000
             10 COMM-BC51-INDTS     PIC S9(5) COMP-3.                   01430000
      *-                              MAX TS                            01440000
             10 COMM-BC51-INDMAX    PIC S9(5) COMP-3.                   01450000
      *      ZONE MESSAGE D'ERREUR POUR LA TRANSACT 'APPELANTE'         01460000
          05 COMM-BC51-MLIBERR      PIC X(80).                          01470000
      *      NOM    PROGRAMME    RETOUR LEVEL-MAX             42        01480000
          05 COMM-BC51-LEVEL-MAX      PIC X(06).                        01490000
      *      CODE TRAITEMENT : PF3 SUR BC61                             01500000
          05 COMM-BC51-CODETR         PIC X(03).                        01510000
      *      PARAMETRAGES DIVERS                                        01520000
      *                               ACCES BDCLI AUTORISE O/N          01530000
          05 COMM-BC51-ACCES          PIC X(01).                        01540000
      *                               MODE DEGRADE (O/N)                01550000
          05 COMM-BC51-DEGRADE        PIC X(01).                        01560000
      *                               SIGNATURE                         01570000
          05 COMM-BC51-ACID           PIC X(08).                        01580000
          05 COMM-BC51-DEBRANCHEMENT  PIC X(01).                        01590000
          05 COMM-BC51-FILLER         PIC X(099).                       01600000
      *                                                                 01610000
      *------GESTION ECRAN - FICHES---------------------------3620      01620000
      *----------------------------------LONGUEUR ???                   01630000
          05 COMM-BC61.                                                 01640000
             10 COMM-BC61-INDTS             PIC S9(5) COMP-3.           01650000
             10 COMM-BC61-IND-TS            PIC S9(5) COMP-3.           01660000
             10 COMM-BC61-INDMAX            PIC S9(5) COMP-3.           01670000
             10 COMM-BC61-NLIGNE            PIC S9(3) COMP-3.           01680000
      *         COMPTE FACTURE = CMPT DONNEUR D"ORDRE:C"EST LE MEME     01690000
             10 COMM-BC61-COMPTE            PIC X(1) VALUE SPACE.       01700000
                 88 COMM-BC61-CPT-UNIQUE             VALUE '1'.         01710000
             10 COMM-BC61-CLIENT-B2B.                                   01720000
                15 COMM-BC61-LIGNE  OCCURS 2.                           01730000
                   20 COMM-BC61-NCARTE            PIC X(09).            01740000
                   20 COMM-BC61-TORG              PIC X(05).            01750000
                   20 COMM-BC61-LNOM              PIC X(60).            01760000
                   20 COMM-BC61-NCLIENTADR        PIC X(08).            01770000
                   20 COMM-BC61-NSIRET            PIC X(14).            01780000
                   20 COMM-BC61-NTDOM             PIC X(15).            01790000
                   20 COMM-BC61-CPOST             PIC X(05).            01800000
                   20 COMM-BC61-LCOMN             PIC X(32).            01810000
                   20 COMM-BC61-CVOIE             PIC X(05).            01820000
                   20 COMM-BC61-CTVOIE            PIC X(04).            01830000
                   20 COMM-BC61-LNOMVOIE          PIC X(25).            01840000
                   20 COMM-BC61-CMPAD1            PIC X(32).            01850000
                   20 COMM-BC61-LBUREAU           PIC X(26).            01860000
                   20 COMM-BC61-CPAYS             PIC X(03).            01870000
                   20 COMM-BC61-TPAYS             PIC X(03).            01880000
                   20 COMM-BC61-ORGMODIF          PIC X(01).            01890000
                   20 COMM-BC61-TFACTU            PIC X(01).            01900000
                   20 COMM-BC61-ARF               PIC X(01).            01910000
                   20 COMM-BC61-MONTANT           PIC Z(10).            01920000
                   20 COMM-BC61-NIERRARCH         PIC X(01).            01930000
                   20 COMM-BC61-FACT              PIC X(01).            01940000
                   20 COMM-BC61-CPTEPARENT        PIC X(08).            01950000
                   20 COMM-BC61-CTYPCLIENT        PIC X(01).            01960000
                   20 COMM-BC61-NCOMPTE           PIC X(08).            01970000
                   20 COMM-BC61-EFILLER           PIC X(050).           01980000
                   20 COMM-BC61-CINSEE            PIC X(005).           01990000
                   20 COMM-BC61-CILOT             PIC X(008).           02000000
                   20 COMM-BC61-REMISE            PIC X(5).             02010000
             10 COMM-BC61-FILLER           PIC X(3897).                 02020000
      *                                                                 02030000
                                                                                
