      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TSSP91                                         *  00020000
      *       TR : SP91  GESTION DU SEUIL DE REVENTE A PERTE         *  00030000
      *       PG : TSP91 SAISIE/MAJ DEROGATION                       *  00040000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG           PIC S9(2) COMP-3 VALUE 46.                 00080006
       01  TS-DONNEES.                                                  00090000
           05 TS-NSOCIETE    PIC X(03).                                         
           05 TS-NLIEU       PIC X(03).                                         
           05 TS-NZONPRIX    PIC X(02).                                         
           05 TS-LLIEU       PIC X(20).                                         
           05 TS-DATEDEB     PIC X(08).                                         
           05 TS-DATEFIN     PIC X(08).                                         
           05 TS-S           PIC X.                                             
           05 TS-ETAT        PIC X.                                             
                                                                                
