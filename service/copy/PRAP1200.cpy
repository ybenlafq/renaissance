      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE AP1200                       
      ******************************************************************        
      *                                                                         
       CLEF-AP1200             SECTION.                                         
      *                                                                         
           MOVE 'RVAP1200          '       TO   TABLE-NAME.                     
           MOVE 'AP1200'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-AP1200. EXIT.                                                   
                EJECT                                                           
                                                                                
