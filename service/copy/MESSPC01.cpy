      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:10 >
      
      *****************************************************************
      *
      *   COPY MESSPC01 STRICTEMENT IDENTIQUE A CCBEMQEHCC SUR AS400
      *
      *   MESSAGE MQ ENVOYE PAR L'AS400 POUR ALIMENTATION RTPC01 ET
      *   RTPC02.
      *
      *****************************************************************
      *
      *    Description du message.
       05  MESSPC01-MSG.
      *    Type de requ�te.
           10 MESSPC01-OPTYPE             PIC X(10).
      *    Identifiant unique de transaction.
           10 MESSPC01-TRANSACTIONID      PIC X(10).
      *    Date de transaction.
           10 MESSPC01-DATE               PIC X(08).
      *    Heure de transaction.
           10 MESSPC01-HEURE              PIC X(06).
      *    Soci�t� de vente (� l'activation).
           10 MESSPC01-NSOCVTE            PIC X(03).
      *    Magasin de vente (� l'activation).
           10 MESSPC01-NLIEUVTE           PIC X(03).
      *    Num�ro de vente  (� l'activation).
           10 MESSPC01-NVENTE             PIC X(07).
      *    Soci�t� de paiement.
           10 MESSPC01-NSOCP              PIC X(03).
      *    Magasin de paiement.
           10 MESSPC01-NLIEUP             PIC X(03).
      *    Transaction de paiement.
           10 MESSPC01-NTRANSP            PIC 9(08).
      *    Num�ro de caisse.
           10 MESSPC01-NCAISSE            PIC X(03).
      *    Nombre de cartes.
           10 MESSPC01-NBC                PIC 9(03).
      *    Table des cartes.
           10 MESSPC01-CARTE           OCCURS 100.
      *       Num�ro de ligne de vente (NSEQNQ de la prestation).
      *                                (A l'activation).
              15 MESSPC01-NSEQNQ          PIC 9(05).
      *       Code prestation.         (A l'activation).
              15 MESSPC01-CPREST          PIC X(05).
      *       Code type de carte.
              15 MESSPC01-CARDTYPECODE    PIC X(20).
      *       Num�ro de s�rie.
              15 MESSPC01-SERIALNUM       PIC X(13).
      *       Num�ro al�atoire.
              15 MESSPC01-RANDOMNUM       PIC X(19).
      *       Date de validit� r�elle.
              15 MESSPC01-DVALIDR         PIC X(08).
      *       Date de validit� faciale.
              15 MESSPC01-DVALIDF         PIC X(08).
      *       Montant dans la devise.
              15 MESSPC01-MONTANT         PIC 9(07)V99.
      *       Montant de l'op�ration en Euro.
              15 MESSPC01-MNTEUROS        PIC 9(07)V99.
      *       Code devise.
              15 MESSPC01-CDEV            PIC X(04).
      
