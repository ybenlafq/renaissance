      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP161 AU 17/01/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,29,BI,A,                          *        
      *                           41,03,PD,A,                          *        
      *                           44,05,BI,A,                          *        
      *                           49,20,BI,A,                          *        
      *                           69,07,BI,A,                          *        
      *                           76,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP161.                                                        
            05 NOMETAT-ISP161           PIC X(6) VALUE 'ISP161'.                
            05 RUPTURES-ISP161.                                                 
           10 ISP161-COULEUR            PIC X(05).                      007  005
           10 ISP161-TITRE              PIC X(29).                      012  029
           10 ISP161-WSEQFAM            PIC S9(05)      COMP-3.         041  003
           10 ISP161-CMARQ              PIC X(05).                      044  005
           10 ISP161-LREFFOURN          PIC X(20).                      049  020
           10 ISP161-NCODIC             PIC X(07).                      069  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP161-SEQUENCE           PIC S9(04) COMP.                076  002
      *--                                                                       
           10 ISP161-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP161.                                                   
           10 ISP161-CFAM               PIC X(05).                      078  005
           10 ISP161-DETAIL1            PIC X(29).                      083  029
           10 ISP161-DETAIL2            PIC X(30).                      112  030
           10 ISP161-DETAIL3            PIC X(28).                      142  028
           10 ISP161-DTRAIT             PIC X(10).                      170  010
           10 ISP161-ENTETE1            PIC X(29).                      180  029
           10 ISP161-ENTETE2            PIC X(30).                      209  030
           10 ISP161-ENTETE3            PIC X(28).                      239  028
           10 ISP161-NSOC               PIC X(03).                      267  003
            05 FILLER                      PIC X(243).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP161-LONG           PIC S9(4)   COMP  VALUE +269.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP161-LONG           PIC S9(4) COMP-5  VALUE +269.           
                                                                                
      *}                                                                        
