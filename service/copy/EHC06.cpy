      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC06   EHC06                                              00000020
      ***************************************************************** 00000030
       01   EHC06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUHCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUHCL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUHCF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUHCI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDENVOII  PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTENVOIL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCTENVOIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTENVOIF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTENVOII      PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSOCIETEI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNLIEUI   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNSSLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSSLIEUF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNSSLIEUI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUTRL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCLIEUTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEUTRF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCLIEUTRI      PIC X(4).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLLIEUI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTIERSI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLTIERSI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRENDUL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRENDUF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCRENDUI  PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRENDUL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRENDUF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLRENDUI  PIC X(20).                                      00000730
           02 TABLEI OCCURS   12 TIMES .                                00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCHSL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNCHSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCHSF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCHSI  PIC X(7).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCSTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCSTAF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCSTAI  PIC X.                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNCODICI     PIC X(7).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCFAMI  PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCMARQI      PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLREFI  PIC X(20).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(78).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EHC06   EHC06                                              00001200
      ***************************************************************** 00001210
       01   EHC06O REDEFINES EHC06I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEA    PIC X.                                          00001390
           02 MPAGEC    PIC X.                                          00001400
           02 MPAGEP    PIC X.                                          00001410
           02 MPAGEH    PIC X.                                          00001420
           02 MPAGEV    PIC X.                                          00001430
           02 MPAGEO    PIC Z9.                                         00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGETOTA      PIC X.                                     00001460
           02 MPAGETOTC PIC X.                                          00001470
           02 MPAGETOTP PIC X.                                          00001480
           02 MPAGETOTH PIC X.                                          00001490
           02 MPAGETOTV PIC X.                                          00001500
           02 MPAGETOTO      PIC Z9.                                    00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNLIEUHCA      PIC X.                                     00001530
           02 MNLIEUHCC PIC X.                                          00001540
           02 MNLIEUHCP PIC X.                                          00001550
           02 MNLIEUHCH PIC X.                                          00001560
           02 MNLIEUHCV PIC X.                                          00001570
           02 MNLIEUHCO      PIC X(3).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLLIEUHCA      PIC X.                                     00001600
           02 MLLIEUHCC PIC X.                                          00001610
           02 MLLIEUHCP PIC X.                                          00001620
           02 MLLIEUHCH PIC X.                                          00001630
           02 MLLIEUHCV PIC X.                                          00001640
           02 MLLIEUHCO      PIC X(20).                                 00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MDENVOIA  PIC X.                                          00001670
           02 MDENVOIC  PIC X.                                          00001680
           02 MDENVOIP  PIC X.                                          00001690
           02 MDENVOIH  PIC X.                                          00001700
           02 MDENVOIV  PIC X.                                          00001710
           02 MDENVOIO  PIC X(10).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCTENVOIA      PIC X.                                     00001740
           02 MCTENVOIC PIC X.                                          00001750
           02 MCTENVOIP PIC X.                                          00001760
           02 MCTENVOIH PIC X.                                          00001770
           02 MCTENVOIV PIC X.                                          00001780
           02 MCTENVOIO      PIC X.                                     00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MSOCIETEA      PIC X.                                     00001810
           02 MSOCIETEC PIC X.                                          00001820
           02 MSOCIETEP PIC X.                                          00001830
           02 MSOCIETEH PIC X.                                          00001840
           02 MSOCIETEV PIC X.                                          00001850
           02 MSOCIETEO      PIC X(3).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNLIEUA   PIC X.                                          00001880
           02 MNLIEUC   PIC X.                                          00001890
           02 MNLIEUP   PIC X.                                          00001900
           02 MNLIEUH   PIC X.                                          00001910
           02 MNLIEUV   PIC X.                                          00001920
           02 MNLIEUO   PIC X(3).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNSSLIEUA      PIC X.                                     00001950
           02 MNSSLIEUC PIC X.                                          00001960
           02 MNSSLIEUP PIC X.                                          00001970
           02 MNSSLIEUH PIC X.                                          00001980
           02 MNSSLIEUV PIC X.                                          00001990
           02 MNSSLIEUO      PIC X(3).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCLIEUTRA      PIC X.                                     00002020
           02 MCLIEUTRC PIC X.                                          00002030
           02 MCLIEUTRP PIC X.                                          00002040
           02 MCLIEUTRH PIC X.                                          00002050
           02 MCLIEUTRV PIC X.                                          00002060
           02 MCLIEUTRO      PIC X(4).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLLIEUA   PIC X.                                          00002090
           02 MLLIEUC   PIC X.                                          00002100
           02 MLLIEUP   PIC X.                                          00002110
           02 MLLIEUH   PIC X.                                          00002120
           02 MLLIEUV   PIC X.                                          00002130
           02 MLLIEUO   PIC X(20).                                      00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCTIERSA  PIC X.                                          00002160
           02 MCTIERSC  PIC X.                                          00002170
           02 MCTIERSP  PIC X.                                          00002180
           02 MCTIERSH  PIC X.                                          00002190
           02 MCTIERSV  PIC X.                                          00002200
           02 MCTIERSO  PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLTIERSA  PIC X.                                          00002230
           02 MLTIERSC  PIC X.                                          00002240
           02 MLTIERSP  PIC X.                                          00002250
           02 MLTIERSH  PIC X.                                          00002260
           02 MLTIERSV  PIC X.                                          00002270
           02 MLTIERSO  PIC X(20).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCRENDUA  PIC X.                                          00002300
           02 MCRENDUC  PIC X.                                          00002310
           02 MCRENDUP  PIC X.                                          00002320
           02 MCRENDUH  PIC X.                                          00002330
           02 MCRENDUV  PIC X.                                          00002340
           02 MCRENDUO  PIC X(5).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MLRENDUA  PIC X.                                          00002370
           02 MLRENDUC  PIC X.                                          00002380
           02 MLRENDUP  PIC X.                                          00002390
           02 MLRENDUH  PIC X.                                          00002400
           02 MLRENDUV  PIC X.                                          00002410
           02 MLRENDUO  PIC X(20).                                      00002420
           02 TABLEO OCCURS   12 TIMES .                                00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MNCHSA  PIC X.                                          00002450
             03 MNCHSC  PIC X.                                          00002460
             03 MNCHSP  PIC X.                                          00002470
             03 MNCHSH  PIC X.                                          00002480
             03 MNCHSV  PIC X.                                          00002490
             03 MNCHSO  PIC X(7).                                       00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MCSTAA  PIC X.                                          00002520
             03 MCSTAC  PIC X.                                          00002530
             03 MCSTAP  PIC X.                                          00002540
             03 MCSTAH  PIC X.                                          00002550
             03 MCSTAV  PIC X.                                          00002560
             03 MCSTAO  PIC X.                                          00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MNCODICA     PIC X.                                     00002590
             03 MNCODICC     PIC X.                                     00002600
             03 MNCODICP     PIC X.                                     00002610
             03 MNCODICH     PIC X.                                     00002620
             03 MNCODICV     PIC X.                                     00002630
             03 MNCODICO     PIC X(7).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MCFAMA  PIC X.                                          00002660
             03 MCFAMC  PIC X.                                          00002670
             03 MCFAMP  PIC X.                                          00002680
             03 MCFAMH  PIC X.                                          00002690
             03 MCFAMV  PIC X.                                          00002700
             03 MCFAMO  PIC X(5).                                       00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MCMARQA      PIC X.                                     00002730
             03 MCMARQC PIC X.                                          00002740
             03 MCMARQP PIC X.                                          00002750
             03 MCMARQH PIC X.                                          00002760
             03 MCMARQV PIC X.                                          00002770
             03 MCMARQO      PIC X(5).                                  00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MLREFA  PIC X.                                          00002800
             03 MLREFC  PIC X.                                          00002810
             03 MLREFP  PIC X.                                          00002820
             03 MLREFH  PIC X.                                          00002830
             03 MLREFV  PIC X.                                          00002840
             03 MLREFO  PIC X(20).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(78).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
