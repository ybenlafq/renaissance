      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PCSAP TYPE CARTE COMPTE SAP            *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPCSAP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPCSAP.                                                             
      *}                                                                        
           05  PCSAP-CTABLEG2    PIC X(15).                                     
           05  PCSAP-CTABLEG2-REDEF REDEFINES PCSAP-CTABLEG2.                   
               10  PCSAP-COMPTE          PIC X(08).                             
               10  PCSAP-DDEB            PIC X(06).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PCSAP-DDEB-N         REDEFINES PCSAP-DDEB                    
                                         PIC 9(06).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  PCSAP-NSEQ            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  PCSAP-NSEQ-N         REDEFINES PCSAP-NSEQ                    
                                         PIC 9(01).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  PCSAP-WTABLEG     PIC X(80).                                     
           05  PCSAP-WTABLEG-REDEF  REDEFINES PCSAP-WTABLEG.                    
               10  PCSAP-PRTYP           PIC X(05).                             
               10  PCSAP-CPREST          PIC X(05).                             
               10  PCSAP-LIBELLE         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPCSAP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPCSAP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCSAP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PCSAP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCSAP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PCSAP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
