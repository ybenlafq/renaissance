      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBC50   EBC50                                              00000020
      ***************************************************************** 00000030
       01   EBC50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPADDRL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MTYPADDRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPADDRF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTYPADDRI      PIC X(10).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLSOCI    PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUI   PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENTEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVENTEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLVENTEI  PIC X(11).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNVENTEI  PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECRANL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MECRANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MECRANF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MECRANI   PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECRANMXL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MECRANMXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MECRANMXF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MECRANMXI      PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCARTEL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLCARTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCARTEF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLCARTEI  PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCARTEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNCARTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCARTEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNCARTEI  PIC X(9).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLNOMI    PIC X(25).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLPRENOMI      PIC X(15).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLB2BL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLB2BL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLB2BF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLB2BI    PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCB2BL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCB2BL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCB2BF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCB2BI    PIC X.                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPOSTF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCPOSTI   PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMNL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLCOMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMNF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLCOMNI   PIC X(32).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTDOML   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLTDOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTDOMF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLTDOMI   PIC X(10).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTDOML   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNTDOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTDOMF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNTDOMI   PIC X(15).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGSML    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLGSML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLGSMF    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLGSMI    PIC X(9).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSML    COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNGSML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNGSMF    PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNGSMI    PIC X(15).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTBURL   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLTBURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTBURF   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLTBURI   PIC X(7).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTBURL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNTBURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTBURF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNTBURI   PIC X(15).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEMAILL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLEMAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLEMAILF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLEMAILI  PIC X(10).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEMAILL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MEMAILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEMAILF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MEMAILI   PIC X(65).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYSL   COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPAYSF   PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLPAYSI   PIC X(15).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDNAISSL      COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MLDNAISSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDNAISSF      PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLDNAISSI      PIC X(16).                                 00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNAISSL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MDNAISSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDNAISSF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MDNAISSI  PIC X(10).                                      00001330
      * entete colonnes                                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTETL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MLENTETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTETF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLENTETI  PIC X(50).                                      00001380
           02 MLCLIENTI OCCURS   10 TIMES .                             00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00001400
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00001410
             03 FILLER  PIC X(4).                                       00001420
             03 MLCHOIXI     PIC X.                                     00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOML   COMP PIC S9(4).                                 00001440
      *--                                                                       
             03 MNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOMF   PIC X.                                          00001450
             03 FILLER  PIC X(4).                                       00001460
             03 MNOMI   PIC X(25).                                      00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFOL  COMP PIC S9(4).                                 00001480
      *--                                                                       
             03 MINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINFOF  PIC X.                                          00001490
             03 FILLER  PIC X(4).                                       00001500
             03 MINFOI  PIC X(50).                                      00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MLIBERRI  PIC X(74).                                      00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MCODTRAI  PIC X(4).                                       00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MCICSI    PIC X(5).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MNETNAMI  PIC X(8).                                       00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MSCREENI  PIC X(4).                                       00001710
      ***************************************************************** 00001720
      * SDF: EBC50   EBC50                                              00001730
      ***************************************************************** 00001740
       01   EBC50O REDEFINES EBC50I.                                    00001750
           02 FILLER    PIC X(12).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MDATJOUA  PIC X.                                          00001780
           02 MDATJOUC  PIC X.                                          00001790
           02 MDATJOUP  PIC X.                                          00001800
           02 MDATJOUH  PIC X.                                          00001810
           02 MDATJOUV  PIC X.                                          00001820
           02 MDATJOUO  PIC X(10).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MTIMJOUA  PIC X.                                          00001850
           02 MTIMJOUC  PIC X.                                          00001860
           02 MTIMJOUP  PIC X.                                          00001870
           02 MTIMJOUH  PIC X.                                          00001880
           02 MTIMJOUV  PIC X.                                          00001890
           02 MTIMJOUO  PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTYPADDRA      PIC X.                                     00001920
           02 MTYPADDRC PIC X.                                          00001930
           02 MTYPADDRP PIC X.                                          00001940
           02 MTYPADDRH PIC X.                                          00001950
           02 MTYPADDRV PIC X.                                          00001960
           02 MTYPADDRO      PIC X(10).                                 00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MPAGEA    PIC X.                                          00001990
           02 MPAGEC    PIC X.                                          00002000
           02 MPAGEP    PIC X.                                          00002010
           02 MPAGEH    PIC X.                                          00002020
           02 MPAGEV    PIC X.                                          00002030
           02 MPAGEO    PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MPAGEMAXA      PIC X.                                     00002060
           02 MPAGEMAXC PIC X.                                          00002070
           02 MPAGEMAXP PIC X.                                          00002080
           02 MPAGEMAXH PIC X.                                          00002090
           02 MPAGEMAXV PIC X.                                          00002100
           02 MPAGEMAXO      PIC X(3).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLSOCA    PIC X.                                          00002130
           02 MLSOCC    PIC X.                                          00002140
           02 MLSOCP    PIC X.                                          00002150
           02 MLSOCH    PIC X.                                          00002160
           02 MLSOCV    PIC X.                                          00002170
           02 MLSOCO    PIC X(9).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNSOCA    PIC X.                                          00002200
           02 MNSOCC    PIC X.                                          00002210
           02 MNSOCP    PIC X.                                          00002220
           02 MNSOCH    PIC X.                                          00002230
           02 MNSOCV    PIC X.                                          00002240
           02 MNSOCO    PIC X(3).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLLIEUA   PIC X.                                          00002270
           02 MLLIEUC   PIC X.                                          00002280
           02 MLLIEUP   PIC X.                                          00002290
           02 MLLIEUH   PIC X.                                          00002300
           02 MLLIEUV   PIC X.                                          00002310
           02 MLLIEUO   PIC X(10).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNLIEUA   PIC X.                                          00002340
           02 MNLIEUC   PIC X.                                          00002350
           02 MNLIEUP   PIC X.                                          00002360
           02 MNLIEUH   PIC X.                                          00002370
           02 MNLIEUV   PIC X.                                          00002380
           02 MNLIEUO   PIC X(3).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLVENTEA  PIC X.                                          00002410
           02 MLVENTEC  PIC X.                                          00002420
           02 MLVENTEP  PIC X.                                          00002430
           02 MLVENTEH  PIC X.                                          00002440
           02 MLVENTEV  PIC X.                                          00002450
           02 MLVENTEO  PIC X(11).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNVENTEA  PIC X.                                          00002480
           02 MNVENTEC  PIC X.                                          00002490
           02 MNVENTEP  PIC X.                                          00002500
           02 MNVENTEH  PIC X.                                          00002510
           02 MNVENTEV  PIC X.                                          00002520
           02 MNVENTEO  PIC X(7).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MECRANA   PIC X.                                          00002550
           02 MECRANC   PIC X.                                          00002560
           02 MECRANP   PIC X.                                          00002570
           02 MECRANH   PIC X.                                          00002580
           02 MECRANV   PIC X.                                          00002590
           02 MECRANO   PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MECRANMXA      PIC X.                                     00002620
           02 MECRANMXC PIC X.                                          00002630
           02 MECRANMXP PIC X.                                          00002640
           02 MECRANMXH PIC X.                                          00002650
           02 MECRANMXV PIC X.                                          00002660
           02 MECRANMXO      PIC X(3).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLCARTEA  PIC X.                                          00002690
           02 MLCARTEC  PIC X.                                          00002700
           02 MLCARTEP  PIC X.                                          00002710
           02 MLCARTEH  PIC X.                                          00002720
           02 MLCARTEV  PIC X.                                          00002730
           02 MLCARTEO  PIC X(10).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNCARTEA  PIC X.                                          00002760
           02 MNCARTEC  PIC X.                                          00002770
           02 MNCARTEP  PIC X.                                          00002780
           02 MNCARTEH  PIC X.                                          00002790
           02 MNCARTEV  PIC X.                                          00002800
           02 MNCARTEO  PIC X(9).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLNOMA    PIC X.                                          00002830
           02 MLNOMC    PIC X.                                          00002840
           02 MLNOMP    PIC X.                                          00002850
           02 MLNOMH    PIC X.                                          00002860
           02 MLNOMV    PIC X.                                          00002870
           02 MLNOMO    PIC X(25).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MLPRENOMA      PIC X.                                     00002900
           02 MLPRENOMC PIC X.                                          00002910
           02 MLPRENOMP PIC X.                                          00002920
           02 MLPRENOMH PIC X.                                          00002930
           02 MLPRENOMV PIC X.                                          00002940
           02 MLPRENOMO      PIC X(15).                                 00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLB2BA    PIC X.                                          00002970
           02 MLB2BC    PIC X.                                          00002980
           02 MLB2BP    PIC X.                                          00002990
           02 MLB2BH    PIC X.                                          00003000
           02 MLB2BV    PIC X.                                          00003010
           02 MLB2BO    PIC X(4).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCB2BA    PIC X.                                          00003040
           02 MCB2BC    PIC X.                                          00003050
           02 MCB2BP    PIC X.                                          00003060
           02 MCB2BH    PIC X.                                          00003070
           02 MCB2BV    PIC X.                                          00003080
           02 MCB2BO    PIC X.                                          00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCPOSTA   PIC X.                                          00003110
           02 MCPOSTC   PIC X.                                          00003120
           02 MCPOSTP   PIC X.                                          00003130
           02 MCPOSTH   PIC X.                                          00003140
           02 MCPOSTV   PIC X.                                          00003150
           02 MCPOSTO   PIC X(5).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLCOMNA   PIC X.                                          00003180
           02 MLCOMNC   PIC X.                                          00003190
           02 MLCOMNP   PIC X.                                          00003200
           02 MLCOMNH   PIC X.                                          00003210
           02 MLCOMNV   PIC X.                                          00003220
           02 MLCOMNO   PIC X(32).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLTDOMA   PIC X.                                          00003250
           02 MLTDOMC   PIC X.                                          00003260
           02 MLTDOMP   PIC X.                                          00003270
           02 MLTDOMH   PIC X.                                          00003280
           02 MLTDOMV   PIC X.                                          00003290
           02 MLTDOMO   PIC X(10).                                      00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNTDOMA   PIC X.                                          00003320
           02 MNTDOMC   PIC X.                                          00003330
           02 MNTDOMP   PIC X.                                          00003340
           02 MNTDOMH   PIC X.                                          00003350
           02 MNTDOMV   PIC X.                                          00003360
           02 MNTDOMO   PIC X(15).                                      00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLGSMA    PIC X.                                          00003390
           02 MLGSMC    PIC X.                                          00003400
           02 MLGSMP    PIC X.                                          00003410
           02 MLGSMH    PIC X.                                          00003420
           02 MLGSMV    PIC X.                                          00003430
           02 MLGSMO    PIC X(9).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MNGSMA    PIC X.                                          00003460
           02 MNGSMC    PIC X.                                          00003470
           02 MNGSMP    PIC X.                                          00003480
           02 MNGSMH    PIC X.                                          00003490
           02 MNGSMV    PIC X.                                          00003500
           02 MNGSMO    PIC X(15).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLTBURA   PIC X.                                          00003530
           02 MLTBURC   PIC X.                                          00003540
           02 MLTBURP   PIC X.                                          00003550
           02 MLTBURH   PIC X.                                          00003560
           02 MLTBURV   PIC X.                                          00003570
           02 MLTBURO   PIC X(7).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MNTBURA   PIC X.                                          00003600
           02 MNTBURC   PIC X.                                          00003610
           02 MNTBURP   PIC X.                                          00003620
           02 MNTBURH   PIC X.                                          00003630
           02 MNTBURV   PIC X.                                          00003640
           02 MNTBURO   PIC X(15).                                      00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MLEMAILA  PIC X.                                          00003670
           02 MLEMAILC  PIC X.                                          00003680
           02 MLEMAILP  PIC X.                                          00003690
           02 MLEMAILH  PIC X.                                          00003700
           02 MLEMAILV  PIC X.                                          00003710
           02 MLEMAILO  PIC X(10).                                      00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MEMAILA   PIC X.                                          00003740
           02 MEMAILC   PIC X.                                          00003750
           02 MEMAILP   PIC X.                                          00003760
           02 MEMAILH   PIC X.                                          00003770
           02 MEMAILV   PIC X.                                          00003780
           02 MEMAILO   PIC X(65).                                      00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MLPAYSA   PIC X.                                          00003810
           02 MLPAYSC   PIC X.                                          00003820
           02 MLPAYSP   PIC X.                                          00003830
           02 MLPAYSH   PIC X.                                          00003840
           02 MLPAYSV   PIC X.                                          00003850
           02 MLPAYSO   PIC X(15).                                      00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MLDNAISSA      PIC X.                                     00003880
           02 MLDNAISSC PIC X.                                          00003890
           02 MLDNAISSP PIC X.                                          00003900
           02 MLDNAISSH PIC X.                                          00003910
           02 MLDNAISSV PIC X.                                          00003920
           02 MLDNAISSO      PIC X(16).                                 00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MDNAISSA  PIC X.                                          00003950
           02 MDNAISSC  PIC X.                                          00003960
           02 MDNAISSP  PIC X.                                          00003970
           02 MDNAISSH  PIC X.                                          00003980
           02 MDNAISSV  PIC X.                                          00003990
           02 MDNAISSO  PIC X(10).                                      00004000
      * entete colonnes                                                 00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MLENTETA  PIC X.                                          00004030
           02 MLENTETC  PIC X.                                          00004040
           02 MLENTETP  PIC X.                                          00004050
           02 MLENTETH  PIC X.                                          00004060
           02 MLENTETV  PIC X.                                          00004070
           02 MLENTETO  PIC X(50).                                      00004080
           02 MLCLIENTO OCCURS   10 TIMES .                             00004090
             03 FILLER       PIC X(2).                                  00004100
             03 MLCHOIXA     PIC X.                                     00004110
             03 MLCHOIXC     PIC X.                                     00004120
             03 MLCHOIXP     PIC X.                                     00004130
             03 MLCHOIXH     PIC X.                                     00004140
             03 MLCHOIXV     PIC X.                                     00004150
             03 MLCHOIXO     PIC X.                                     00004160
             03 FILLER       PIC X(2).                                  00004170
             03 MNOMA   PIC X.                                          00004180
             03 MNOMC   PIC X.                                          00004190
             03 MNOMP   PIC X.                                          00004200
             03 MNOMH   PIC X.                                          00004210
             03 MNOMV   PIC X.                                          00004220
             03 MNOMO   PIC X(25).                                      00004230
             03 FILLER       PIC X(2).                                  00004240
             03 MINFOA  PIC X.                                          00004250
             03 MINFOC  PIC X.                                          00004260
             03 MINFOP  PIC X.                                          00004270
             03 MINFOH  PIC X.                                          00004280
             03 MINFOV  PIC X.                                          00004290
             03 MINFOO  PIC X(50).                                      00004300
           02 FILLER    PIC X(2).                                       00004310
           02 MLIBERRA  PIC X.                                          00004320
           02 MLIBERRC  PIC X.                                          00004330
           02 MLIBERRP  PIC X.                                          00004340
           02 MLIBERRH  PIC X.                                          00004350
           02 MLIBERRV  PIC X.                                          00004360
           02 MLIBERRO  PIC X(74).                                      00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MCODTRAA  PIC X.                                          00004390
           02 MCODTRAC  PIC X.                                          00004400
           02 MCODTRAP  PIC X.                                          00004410
           02 MCODTRAH  PIC X.                                          00004420
           02 MCODTRAV  PIC X.                                          00004430
           02 MCODTRAO  PIC X(4).                                       00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MCICSA    PIC X.                                          00004460
           02 MCICSC    PIC X.                                          00004470
           02 MCICSP    PIC X.                                          00004480
           02 MCICSH    PIC X.                                          00004490
           02 MCICSV    PIC X.                                          00004500
           02 MCICSO    PIC X(5).                                       00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MNETNAMA  PIC X.                                          00004530
           02 MNETNAMC  PIC X.                                          00004540
           02 MNETNAMP  PIC X.                                          00004550
           02 MNETNAMH  PIC X.                                          00004560
           02 MNETNAMV  PIC X.                                          00004570
           02 MNETNAMO  PIC X(8).                                       00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MSCREENA  PIC X.                                          00004600
           02 MSCREENC  PIC X.                                          00004610
           02 MSCREENP  PIC X.                                          00004620
           02 MSCREENH  PIC X.                                          00004630
           02 MSCREENV  PIC X.                                          00004640
           02 MSCREENO  PIC X(4).                                       00004650
                                                                                
