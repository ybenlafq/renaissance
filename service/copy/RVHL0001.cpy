      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHL0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHL0001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL0001.                                                            
           02  HL00-NOMMAP                                                      
               PIC X(0008).                                                     
           02  HL00-ZECRAN                                                      
               PIC X(0008).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-NOCCUR                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-NOCCUR                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-POSIT                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-POSIT                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  HL00-CTABLE                                                      
               PIC X(0018).                                                     
           02  HL00-CSTABLE                                                     
               PIC X(0005).                                                     
           02  HL00-CCHAMPR                                                     
               PIC X(0018).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-LGCHAMPR                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-LGCHAMPR                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  HL00-CCHAMPL                                                     
               PIC X(0018).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-LGCHAMPL                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-LGCHAMPL                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  HL00-CCRITERE                                                    
               PIC X(0008).                                                     
           02  HL00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HL00-NCHAMPL                                                     
               PIC S9(3) COMP-3.                                                
           02  HL00-NCRITERE                                                    
               PIC S9(3) COMP-3.                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHL0001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL0001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-NOMMAP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-NOMMAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-ZECRAN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-ZECRAN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-NOCCUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-NOCCUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-POSIT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-POSIT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-CTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-CTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-CSTABLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-CSTABLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-CCHAMPR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-CCHAMPR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-LGCHAMPR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-LGCHAMPR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-CCHAMPL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-CCHAMPL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-LGCHAMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-LGCHAMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-CCRITERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-CCRITERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-NCHAMPL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-NCHAMPL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL00-NCRITERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL00-NCRITERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
