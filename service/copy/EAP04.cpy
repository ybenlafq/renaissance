      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - LISTE VENTES NON REALISEE                                00000020
      ***************************************************************** 00000030
       01   EAP04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE                                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * FONCTION                                                        00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MFONCI    PIC X(3).                                       00000200
      * PAGE EN COURS                                                   00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEI    PIC X(2).                                       00000250
      * NOMBRE DE PAGE                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNBPI     PIC X(2).                                       00000300
      * SOCIETE                                                         00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNSOCI    PIC X(3).                                       00000350
      * LIEU                                                            00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNLIEUI   PIC X(3).                                       00000400
      * JOUR DEB PERIODE ENCAI                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCJDL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDENCJDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCJDF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDENCJDI  PIC X(2).                                       00000450
      * MOIS DEB PERIODE ENCAI                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCMDL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDENCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCMDF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDENCMDI  PIC X(2).                                       00000500
      * ANNEE DEB PERIODE ENCA                                          00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCADL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MDENCADL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCADF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDENCADI  PIC X(4).                                       00000550
      * JOUR FIN PERIODE ENCAI                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCJFL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MDENCJFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCJFF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MDENCJFI  PIC X(2).                                       00000600
      * MOIS FIN PERIODE ENCAI                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCMFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDENCMFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCMFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDENCMFI  PIC X(2).                                       00000650
      * ANNEE FIN PERIODE ENCA                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENCAFL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MDENCAFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENCAFF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MDENCAFI  PIC X(4).                                       00000700
           02 MLIGNEI OCCURS   10 TIMES .                               00000710
      * ACTION                                                          00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MACTI   PIC X.                                          00000760
      * DATE ENCAISSEMENT                                               00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDENCL  COMP PIC S9(4).                                 00000780
      *--                                                                       
             03 MDENCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDENCF  PIC X.                                          00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MDENCI  PIC X(8).                                       00000810
      * LIEU                                                            00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUVL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNLIEUVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUVF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNLIEUVI     PIC X(3).                                  00000860
      * VENTE                                                           00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVTEL  COMP PIC S9(4).                                 00000880
      *--                                                                       
             03 MNVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNVTEF  PIC X.                                          00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MNVTEI  PIC X(7).                                       00000910
      * CODE PRESTATION                                                 00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRESL      COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MCPRESL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPRESF      PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCPRESI      PIC X(5).                                  00000960
      * MONTANT PRESTATION                                              00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPPRESL      COMP PIC S9(4).                            00000980
      *--                                                                       
             03 MPPRESL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPPRESF      PIC X.                                     00000990
             03 FILLER  PIC X(4).                                       00001000
             03 MPPRESI      PIC X(6).                                  00001010
      * IDENTITE CLIENT                                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIENL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MCLIENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLIENF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCLIENI      PIC X(20).                                 00001060
      * CODE POSTAL                                                     00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOSTL      COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MCPOSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPOSTF      PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MCPOSTI      PIC X(5).                                  00001110
      * VILLE                                                           00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVILLEL      COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MVILLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVILLEF      PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MVILLEI      PIC X(14).                                 00001160
      * LIEBELLE ERREUR                                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBERRI  PIC X(79).                                      00001210
      * CODE TANSACTION                                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCODTRAI  PIC X(4).                                       00001260
      * ZONE COMMANDE                                                   00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MZONCMDI  PIC X(15).                                      00001310
      * CICS                                                            00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MCICSI    PIC X(5).                                       00001360
      * NETNAME                                                         00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNETNAMI  PIC X(8).                                       00001410
      * CODE TERMINAL                                                   00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MSCREENI  PIC X(4).                                       00001460
      ***************************************************************** 00001470
      * DA2I - LISTE VENTES NON REALISEE                                00001480
      ***************************************************************** 00001490
       01   EAP04O REDEFINES EAP04I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
      * DATE                                                            00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATJOUA  PIC X.                                          00001540
           02 MDATJOUC  PIC X.                                          00001550
           02 MDATJOUP  PIC X.                                          00001560
           02 MDATJOUH  PIC X.                                          00001570
           02 MDATJOUV  PIC X.                                          00001580
           02 MDATJOUO  PIC X(10).                                      00001590
      * HEURE                                                           00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MTIMJOUA  PIC X.                                          00001620
           02 MTIMJOUC  PIC X.                                          00001630
           02 MTIMJOUP  PIC X.                                          00001640
           02 MTIMJOUH  PIC X.                                          00001650
           02 MTIMJOUV  PIC X.                                          00001660
           02 MTIMJOUO  PIC X(5).                                       00001670
      * FONCTION                                                        00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MFONCA    PIC X.                                          00001700
           02 MFONCC    PIC X.                                          00001710
           02 MFONCP    PIC X.                                          00001720
           02 MFONCH    PIC X.                                          00001730
           02 MFONCV    PIC X.                                          00001740
           02 MFONCO    PIC X(3).                                       00001750
      * PAGE EN COURS                                                   00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MPAGEA    PIC X.                                          00001780
           02 MPAGEC    PIC X.                                          00001790
           02 MPAGEP    PIC X.                                          00001800
           02 MPAGEH    PIC X.                                          00001810
           02 MPAGEV    PIC X.                                          00001820
           02 MPAGEO    PIC 99.                                         00001830
      * NOMBRE DE PAGE                                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNBPA     PIC X.                                          00001860
           02 MNBPC     PIC X.                                          00001870
           02 MNBPP     PIC X.                                          00001880
           02 MNBPH     PIC X.                                          00001890
           02 MNBPV     PIC X.                                          00001900
           02 MNBPO     PIC 99.                                         00001910
      * SOCIETE                                                         00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNSOCA    PIC X.                                          00001940
           02 MNSOCC    PIC X.                                          00001950
           02 MNSOCP    PIC X.                                          00001960
           02 MNSOCH    PIC X.                                          00001970
           02 MNSOCV    PIC X.                                          00001980
           02 MNSOCO    PIC X(3).                                       00001990
      * LIEU                                                            00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNLIEUA   PIC X.                                          00002020
           02 MNLIEUC   PIC X.                                          00002030
           02 MNLIEUP   PIC X.                                          00002040
           02 MNLIEUH   PIC X.                                          00002050
           02 MNLIEUV   PIC X.                                          00002060
           02 MNLIEUO   PIC X(3).                                       00002070
      * JOUR DEB PERIODE ENCAI                                          00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MDENCJDA  PIC X.                                          00002100
           02 MDENCJDC  PIC X.                                          00002110
           02 MDENCJDP  PIC X.                                          00002120
           02 MDENCJDH  PIC X.                                          00002130
           02 MDENCJDV  PIC X.                                          00002140
           02 MDENCJDO  PIC X(2).                                       00002150
      * MOIS DEB PERIODE ENCAI                                          00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MDENCMDA  PIC X.                                          00002180
           02 MDENCMDC  PIC X.                                          00002190
           02 MDENCMDP  PIC X.                                          00002200
           02 MDENCMDH  PIC X.                                          00002210
           02 MDENCMDV  PIC X.                                          00002220
           02 MDENCMDO  PIC X(2).                                       00002230
      * ANNEE DEB PERIODE ENCA                                          00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDENCADA  PIC X.                                          00002260
           02 MDENCADC  PIC X.                                          00002270
           02 MDENCADP  PIC X.                                          00002280
           02 MDENCADH  PIC X.                                          00002290
           02 MDENCADV  PIC X.                                          00002300
           02 MDENCADO  PIC X(4).                                       00002310
      * JOUR FIN PERIODE ENCAI                                          00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDENCJFA  PIC X.                                          00002340
           02 MDENCJFC  PIC X.                                          00002350
           02 MDENCJFP  PIC X.                                          00002360
           02 MDENCJFH  PIC X.                                          00002370
           02 MDENCJFV  PIC X.                                          00002380
           02 MDENCJFO  PIC X(2).                                       00002390
      * MOIS FIN PERIODE ENCAI                                          00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDENCMFA  PIC X.                                          00002420
           02 MDENCMFC  PIC X.                                          00002430
           02 MDENCMFP  PIC X.                                          00002440
           02 MDENCMFH  PIC X.                                          00002450
           02 MDENCMFV  PIC X.                                          00002460
           02 MDENCMFO  PIC X(2).                                       00002470
      * ANNEE FIN PERIODE ENCA                                          00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MDENCAFA  PIC X.                                          00002500
           02 MDENCAFC  PIC X.                                          00002510
           02 MDENCAFP  PIC X.                                          00002520
           02 MDENCAFH  PIC X.                                          00002530
           02 MDENCAFV  PIC X.                                          00002540
           02 MDENCAFO  PIC X(4).                                       00002550
           02 MLIGNEO OCCURS   10 TIMES .                               00002560
      * ACTION                                                          00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MACTA   PIC X.                                          00002590
             03 MACTC   PIC X.                                          00002600
             03 MACTP   PIC X.                                          00002610
             03 MACTH   PIC X.                                          00002620
             03 MACTV   PIC X.                                          00002630
             03 MACTO   PIC X.                                          00002640
      * DATE ENCAISSEMENT                                               00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MDENCA  PIC X.                                          00002670
             03 MDENCC  PIC X.                                          00002680
             03 MDENCP  PIC X.                                          00002690
             03 MDENCH  PIC X.                                          00002700
             03 MDENCV  PIC X.                                          00002710
             03 MDENCO  PIC X(8).                                       00002720
      * LIEU                                                            00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MNLIEUVA     PIC X.                                     00002750
             03 MNLIEUVC     PIC X.                                     00002760
             03 MNLIEUVP     PIC X.                                     00002770
             03 MNLIEUVH     PIC X.                                     00002780
             03 MNLIEUVV     PIC X.                                     00002790
             03 MNLIEUVO     PIC X(3).                                  00002800
      * VENTE                                                           00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MNVTEA  PIC X.                                          00002830
             03 MNVTEC  PIC X.                                          00002840
             03 MNVTEP  PIC X.                                          00002850
             03 MNVTEH  PIC X.                                          00002860
             03 MNVTEV  PIC X.                                          00002870
             03 MNVTEO  PIC X(7).                                       00002880
      * CODE PRESTATION                                                 00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MCPRESA      PIC X.                                     00002910
             03 MCPRESC PIC X.                                          00002920
             03 MCPRESP PIC X.                                          00002930
             03 MCPRESH PIC X.                                          00002940
             03 MCPRESV PIC X.                                          00002950
             03 MCPRESO      PIC X(5).                                  00002960
      * MONTANT PRESTATION                                              00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MPPRESA      PIC X.                                     00002990
             03 MPPRESC PIC X.                                          00003000
             03 MPPRESP PIC X.                                          00003010
             03 MPPRESH PIC X.                                          00003020
             03 MPPRESV PIC X.                                          00003030
             03 MPPRESO      PIC X(6).                                  00003040
      * IDENTITE CLIENT                                                 00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MCLIENA      PIC X.                                     00003070
             03 MCLIENC PIC X.                                          00003080
             03 MCLIENP PIC X.                                          00003090
             03 MCLIENH PIC X.                                          00003100
             03 MCLIENV PIC X.                                          00003110
             03 MCLIENO      PIC X(20).                                 00003120
      * CODE POSTAL                                                     00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MCPOSTA      PIC X.                                     00003150
             03 MCPOSTC PIC X.                                          00003160
             03 MCPOSTP PIC X.                                          00003170
             03 MCPOSTH PIC X.                                          00003180
             03 MCPOSTV PIC X.                                          00003190
             03 MCPOSTO      PIC X(5).                                  00003200
      * VILLE                                                           00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MVILLEA      PIC X.                                     00003230
             03 MVILLEC PIC X.                                          00003240
             03 MVILLEP PIC X.                                          00003250
             03 MVILLEH PIC X.                                          00003260
             03 MVILLEV PIC X.                                          00003270
             03 MVILLEO      PIC X(14).                                 00003280
      * LIEBELLE ERREUR                                                 00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(79).                                      00003360
      * CODE TANSACTION                                                 00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MCODTRAA  PIC X.                                          00003390
           02 MCODTRAC  PIC X.                                          00003400
           02 MCODTRAP  PIC X.                                          00003410
           02 MCODTRAH  PIC X.                                          00003420
           02 MCODTRAV  PIC X.                                          00003430
           02 MCODTRAO  PIC X(4).                                       00003440
      * ZONE COMMANDE                                                   00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MZONCMDA  PIC X.                                          00003470
           02 MZONCMDC  PIC X.                                          00003480
           02 MZONCMDP  PIC X.                                          00003490
           02 MZONCMDH  PIC X.                                          00003500
           02 MZONCMDV  PIC X.                                          00003510
           02 MZONCMDO  PIC X(15).                                      00003520
      * CICS                                                            00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MCICSA    PIC X.                                          00003550
           02 MCICSC    PIC X.                                          00003560
           02 MCICSP    PIC X.                                          00003570
           02 MCICSH    PIC X.                                          00003580
           02 MCICSV    PIC X.                                          00003590
           02 MCICSO    PIC X(5).                                       00003600
      * NETNAME                                                         00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNETNAMA  PIC X.                                          00003630
           02 MNETNAMC  PIC X.                                          00003640
           02 MNETNAMP  PIC X.                                          00003650
           02 MNETNAMH  PIC X.                                          00003660
           02 MNETNAMV  PIC X.                                          00003670
           02 MNETNAMO  PIC X(8).                                       00003680
      * CODE TERMINAL                                                   00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MSCREENA  PIC X.                                          00003710
           02 MSCREENC  PIC X.                                          00003720
           02 MSCREENP  PIC X.                                          00003730
           02 MSCREENH  PIC X.                                          00003740
           02 MSCREENV  PIC X.                                          00003750
           02 MSCREENO  PIC X(4).                                       00003760
                                                                                
