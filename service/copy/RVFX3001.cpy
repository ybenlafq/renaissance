      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFX3001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX3001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX3001.                                                            
           02  FX30-NSOC                                                        
               PIC X(0003).                                                     
           02  FX30-NLIEU                                                       
               PIC X(0003).                                                     
           02  FX30-NUMCAIS                                                     
               PIC X(0003).                                                     
           02  FX30-MTFOND                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  FX30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FX30-DATTRANS                                                    
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFX3000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX3001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-NUMCAIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-NUMCAIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-MTFOND-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-MTFOND-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX30-DATTRANS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX30-DATTRANS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
