      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE65   EHE65                                              00000020
      ***************************************************************** 00000030
       01   EHE64I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEHETI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEHETI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTIERSI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLNOMI    PIC X(20).                                      00000370
           02 TABLEI OCCURS   13 TIMES .                                00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLHEDL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNLHEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLHEDF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNLHEDI      PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNGHEL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MNGHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNGHEF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNGHEI  PIC X(7).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNCODICI     PIC X(7).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCFAMI  PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCMARQI      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLREFI  PIC X(20).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSELI   PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EHE65   EHE65                                              00000880
      ***************************************************************** 00000890
       01   EHE64O REDEFINES EHE64I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNUMPAGEA      PIC X.                                     00001070
           02 MNUMPAGEC PIC X.                                          00001080
           02 MNUMPAGEP PIC X.                                          00001090
           02 MNUMPAGEH PIC X.                                          00001100
           02 MNUMPAGEV PIC X.                                          00001110
           02 MNUMPAGEO      PIC X(2).                                  00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEMAXA      PIC X.                                     00001140
           02 MPAGEMAXC PIC X.                                          00001150
           02 MPAGEMAXP PIC X.                                          00001160
           02 MPAGEMAXH PIC X.                                          00001170
           02 MPAGEMAXV PIC X.                                          00001180
           02 MPAGEMAXO      PIC X(2).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MCLIEHETA      PIC X.                                     00001210
           02 MCLIEHETC PIC X.                                          00001220
           02 MCLIEHETP PIC X.                                          00001230
           02 MCLIEHETH PIC X.                                          00001240
           02 MCLIEHETV PIC X.                                          00001250
           02 MCLIEHETO      PIC X(5).                                  00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MLLIEHETA      PIC X.                                     00001280
           02 MLLIEHETC PIC X.                                          00001290
           02 MLLIEHETP PIC X.                                          00001300
           02 MLLIEHETH PIC X.                                          00001310
           02 MLLIEHETV PIC X.                                          00001320
           02 MLLIEHETO      PIC X(20).                                 00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCTIERSA  PIC X.                                          00001350
           02 MCTIERSC  PIC X.                                          00001360
           02 MCTIERSP  PIC X.                                          00001370
           02 MCTIERSH  PIC X.                                          00001380
           02 MCTIERSV  PIC X.                                          00001390
           02 MCTIERSO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLNOMA    PIC X.                                          00001420
           02 MLNOMC    PIC X.                                          00001430
           02 MLNOMP    PIC X.                                          00001440
           02 MLNOMH    PIC X.                                          00001450
           02 MLNOMV    PIC X.                                          00001460
           02 MLNOMO    PIC X(20).                                      00001470
           02 TABLEO OCCURS   13 TIMES .                                00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNLHEDA      PIC X.                                     00001500
             03 MNLHEDC PIC X.                                          00001510
             03 MNLHEDP PIC X.                                          00001520
             03 MNLHEDH PIC X.                                          00001530
             03 MNLHEDV PIC X.                                          00001540
             03 MNLHEDO      PIC 9(3).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MNGHEA  PIC X.                                          00001570
             03 MNGHEC  PIC X.                                          00001580
             03 MNGHEP  PIC X.                                          00001590
             03 MNGHEH  PIC X.                                          00001600
             03 MNGHEV  PIC X.                                          00001610
             03 MNGHEO  PIC 9(7).                                       00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MNCODICA     PIC X.                                     00001640
             03 MNCODICC     PIC X.                                     00001650
             03 MNCODICP     PIC X.                                     00001660
             03 MNCODICH     PIC X.                                     00001670
             03 MNCODICV     PIC X.                                     00001680
             03 MNCODICO     PIC X(7).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MCFAMA  PIC X.                                          00001710
             03 MCFAMC  PIC X.                                          00001720
             03 MCFAMP  PIC X.                                          00001730
             03 MCFAMH  PIC X.                                          00001740
             03 MCFAMV  PIC X.                                          00001750
             03 MCFAMO  PIC X(5).                                       00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCMARQA      PIC X.                                     00001780
             03 MCMARQC PIC X.                                          00001790
             03 MCMARQP PIC X.                                          00001800
             03 MCMARQH PIC X.                                          00001810
             03 MCMARQV PIC X.                                          00001820
             03 MCMARQO      PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MLREFA  PIC X.                                          00001850
             03 MLREFC  PIC X.                                          00001860
             03 MLREFP  PIC X.                                          00001870
             03 MLREFH  PIC X.                                          00001880
             03 MLREFV  PIC X.                                          00001890
             03 MLREFO  PIC X(20).                                      00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MSELA   PIC X.                                          00001920
             03 MSELC   PIC X.                                          00001930
             03 MSELP   PIC X.                                          00001940
             03 MSELH   PIC X.                                          00001950
             03 MSELV   PIC X.                                          00001960
             03 MSELO   PIC X.                                          00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(78).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
