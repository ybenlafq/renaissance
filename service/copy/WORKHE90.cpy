      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *    MISE A JOUR DES TABLES STOCKS (GS10, 30, 40, 43, 60, 70)   *         
      *****************************************************************         
       01 WORK-HE90-APPLI.                                                      
                                                                        00000830
        02 WORK-HE90-ENTREE.                                                    
           05 WORK-HE90-NSOC          PIC X(03).                                
           05 WORK-HE90-CODLANG       PIC X(02).                                
           05 WORK-HE90-NTACHE        PIC 9(10).                                
           05 WORK-HE90-CFONCTION     PIC X(01).                                
           05 WORK-HE90-QTE           PIC 9(05).                                
           05 WORK-HE90-CUTIL         PIC X(05).                                
           05 WORK-HE90-DOPER         PIC X(08).                                
           05 WORK-HE90-NORIGINE      PIC X(07).                                
           05 WORK-HE90-NHS           PIC X(07).                                
           05 WORK-HE90-NCODIC        PIC X(07).                                
           05 WORK-HE90-CMOTIF        PIC X(10).                                
           05 WORK-HE90-NLIEUENT      PIC X(03).                                
           05 WORK-HE90-ORIG.                                                   
               10 WORK-HE90-NSOCORIG      PIC X(03).                            
               10 WORK-HE90-NLIEUORIG     PIC X(03).                            
               10 WORK-HE90-NSSLIEUORIG   PIC X(03).                            
               10 WORK-HE90-CLIEUTRTORIG  PIC X(05).                            
           05 WORK-HE90-DEST.                                                   
               10 WORK-HE90-NSOCDEST      PIC X(03).                            
               10 WORK-HE90-NLIEUDEST     PIC X(03).                            
               10 WORK-HE90-NSSLIEUDEST   PIC X(03).                            
               10 WORK-HE90-CLIEUTRTDEST  PIC X(05).                            
      * ZONES RETOURNEES PAR MHE90                                              
      *                                                                         
      * REPONSE  OK = 0   SINON = 1 : ERREUR NON BLOQUANTE                      
      *                           2 : ERREUR     BLOQUANTE                      
      *                           3 : ERREUR     BLOQUANTE DANS MSL011          
        02 WORK-HE90-SORTIE.                                                    
           05 WORK-HE90-CRETOUR       PIC 9.                                    
           05 WORK-HE90-MESS-ERR      PIC X(78).                                
      *    05 WORK-HE90-GS43-CACTION  PIC X.                                    
      *        88  WORK-HE90-GS43-INACTION     VALUE SPACE.                     
      *        88  WORK-HE90-GS43-CREATION     VALUE 'C'.                       
                                                                                
