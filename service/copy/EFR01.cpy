      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * FRANCHISE - REGLE DE FACTURATION                                00000020
      ***************************************************************** 00000030
       01   EFR01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREGLEFL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MREGLEFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREGLEFF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MREGLEFI  PIC X(15).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGNEL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIGNEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIGNEF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIGNEI  PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTITULL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MINTITULL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MINTITULF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MINTITULI      PIC X(30).                                 00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MTITREI   PIC X(35).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLUXL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MFLUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFLUXF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MFLUXI    PIC X(12).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWENVOISAPL    COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MWENVOISAPL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWENVOISAPF    PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MWENVOISAPI    PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFPRMPL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MFPRMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFPRMPF   PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MFPRMPI   PIC X.                                          00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIBELLEI      PIC X(35).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFSOCEL   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MFSOCEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFSOCEF   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MFSOCEI   PIC X.                                          00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENSOCL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MENSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MENSOCF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MENSOCI   PIC X(3).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENLIEUL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MENLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENLIEUF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MENLIEUI  PIC X(3).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFSOCDL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MFSOCDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFSOCDF   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MFSOCDI   PIC X.                                          00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNSOCL   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MDNSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDNSOCF   PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MDNSOCI   PIC X(3).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNLIEUL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MDNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDNLIEUF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MDNLIEUI  PIC X(3).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFSOCVL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MFSOCVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFSOCVF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MFSOCVI   PIC X.                                          00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVNSOCL   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MVNSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVNSOCF   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MVNSOCI   PIC X(3).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVNLIEUL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MVNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVNLIEUF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MVNLIEUI  PIC X(3).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPECLGL   COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MPECLGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPECLGF   PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MPECLGI   PIC X.                                          00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREML     COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MREML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MREMF     PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MREMI     PIC X(10).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTXL      COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MTXL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MTXF      PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MTXI      PIC X(2).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVETUSTL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MVETUSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVETUSTF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MVETUSTI  PIC X(10).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPXFICL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MPXFICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPXFICF   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MPXFICI   PIC X.                                          00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRMPL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MPRMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRMPF    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MPRMPI    PIC X.                                          00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTAXESL   COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MTAXESL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTAXESF   PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MTAXESI   PIC X.                                          00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVAL     COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MTVAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTVAF     PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MTVAI     PIC X.                                          00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTHTL    COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MMTHTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMTHTF    PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MMTHTI    PIC X(7).                                       00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC1L      COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MC1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC1F      PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MC1I      PIC X.                                          00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC2L      COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MC2L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC2F      PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MC2I      PIC X.                                          00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC3L      COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MC3L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC3F      PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MC3I      PIC X.                                          00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC4L      COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MC4L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC4F      PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MC4I      PIC X.                                          00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC5L      COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MC5L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC5F      PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MC5I      PIC X.                                          00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC6L      COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MC6L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC6F      PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MC6I      PIC X.                                          00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC7L      COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MC7L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC7F      PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MC7I      PIC X.                                          00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC8L      COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MC8L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC8F      PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MC8I      PIC X.                                          00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC9L      COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MC9L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MC9F      PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MC9I      PIC X.                                          00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC10L     COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MC10L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MC10F     PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MC10I     PIC X.                                          00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC11L     COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MC11L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MC11F     PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MC11I     PIC X.                                          00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MC12L     COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MC12L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MC12F     PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MC12I     PIC X.                                          00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREGRPL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MNREGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNREGRPF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MNREGRPI  PIC X.                                          00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREGRPL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MLREGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREGRPF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MLREGRPI  PIC X(30).                                      00001750
           02 MTABI OCCURS   3 TIMES .                                  00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MARTICLL     COMP PIC S9(4).                            00001770
      *--                                                                       
             03 MARTICLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MARTICLF     PIC X.                                     00001780
             03 FILLER  PIC X(4).                                       00001790
             03 MARTICLI     PIC X(7).                                  00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00001810
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00001820
             03 FILLER  PIC X(4).                                       00001830
             03 MLIBELI      PIC X(20).                                 00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSMTHTL      COMP PIC S9(4).                            00001850
      *--                                                                       
             03 MSMTHTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSMTHTF      PIC X.                                     00001860
             03 FILLER  PIC X(4).                                       00001870
             03 MSMTHTI      PIC X(9).                                  00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTVAL  COMP PIC S9(4).                                 00001890
      *--                                                                       
             03 MSTVAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTVAF  PIC X.                                          00001900
             03 FILLER  PIC X(4).                                       00001910
             03 MSTVAI  PIC X.                                          00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTXPXL  COMP PIC S9(4).                                 00001930
      *--                                                                       
             03 MTXPXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTXPXF  PIC X.                                          00001940
             03 FILLER  PIC X(4).                                       00001950
             03 MTXPXI  PIC X(6).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSC1L   COMP PIC S9(4).                                 00001970
      *--                                                                       
             03 MSC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSC1F   PIC X.                                          00001980
             03 FILLER  PIC X(4).                                       00001990
             03 MSC1I   PIC X.                                          00002000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSC2L   COMP PIC S9(4).                                 00002010
      *--                                                                       
             03 MSC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSC2F   PIC X.                                          00002020
             03 FILLER  PIC X(4).                                       00002030
             03 MSC2I   PIC X.                                          00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSC3L   COMP PIC S9(4).                                 00002050
      *--                                                                       
             03 MSC3L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSC3F   PIC X.                                          00002060
             03 FILLER  PIC X(4).                                       00002070
             03 MSC3I   PIC X.                                          00002080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSC4L   COMP PIC S9(4).                                 00002090
      *--                                                                       
             03 MSC4L COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSC4F   PIC X.                                          00002100
             03 FILLER  PIC X(4).                                       00002110
             03 MSC4I   PIC X.                                          00002120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTAXEL      COMP PIC S9(4).                            00002130
      *--                                                                       
             03 MSTAXEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTAXEF      PIC X.                                     00002140
             03 FILLER  PIC X(4).                                       00002150
             03 MSTAXEI      PIC X.                                     00002160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEDUPL      COMP PIC S9(4).                            00002170
      *--                                                                       
             03 MDEDUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEDUPF      PIC X.                                     00002180
             03 FILLER  PIC X(4).                                       00002190
             03 MDEDUPI      PIC X.                                     00002200
      * ZONE CMD AIDA                                                   00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPPRL   COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MSUPPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSUPPRF   PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MSUPPRI   PIC X.                                          00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MLIBERRI  PIC X(79).                                      00002290
      * CODE TRANSACTION                                                00002300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002320
           02 FILLER    PIC X(4).                                       00002330
           02 MCODTRAI  PIC X(4).                                       00002340
      * CICS DE TRAVAIL                                                 00002350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002360
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002370
           02 FILLER    PIC X(4).                                       00002380
           02 MCICSI    PIC X(5).                                       00002390
      * NETNAME                                                         00002400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002410
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002420
           02 FILLER    PIC X(4).                                       00002430
           02 MNETNAMI  PIC X(8).                                       00002440
      * CODE TERMINAL                                                   00002450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002460
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002470
           02 FILLER    PIC X(4).                                       00002480
           02 MSCREENI  PIC X(4).                                       00002490
      ***************************************************************** 00002500
      * FRANCHISE - REGLE DE FACTURATION                                00002510
      ***************************************************************** 00002520
       01   EFR01O REDEFINES EFR01I.                                    00002530
           02 FILLER    PIC X(12).                                      00002540
      * DATE DU JOUR                                                    00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MDATJOUA  PIC X.                                          00002570
           02 MDATJOUC  PIC X.                                          00002580
           02 MDATJOUP  PIC X.                                          00002590
           02 MDATJOUH  PIC X.                                          00002600
           02 MDATJOUV  PIC X.                                          00002610
           02 MDATJOUO  PIC X(10).                                      00002620
      * HEURE                                                           00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MTIMJOUA  PIC X.                                          00002650
           02 MTIMJOUC  PIC X.                                          00002660
           02 MTIMJOUP  PIC X.                                          00002670
           02 MTIMJOUH  PIC X.                                          00002680
           02 MTIMJOUV  PIC X.                                          00002690
           02 MTIMJOUO  PIC X(5).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MREGLEFA  PIC X.                                          00002720
           02 MREGLEFC  PIC X.                                          00002730
           02 MREGLEFP  PIC X.                                          00002740
           02 MREGLEFH  PIC X.                                          00002750
           02 MREGLEFV  PIC X.                                          00002760
           02 MREGLEFO  PIC X(15).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MNLIGNEA  PIC X.                                          00002790
           02 MNLIGNEC  PIC X.                                          00002800
           02 MNLIGNEP  PIC X.                                          00002810
           02 MNLIGNEH  PIC X.                                          00002820
           02 MNLIGNEV  PIC X.                                          00002830
           02 MNLIGNEO  PIC X(2).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MINTITULA      PIC X.                                     00002860
           02 MINTITULC PIC X.                                          00002870
           02 MINTITULP PIC X.                                          00002880
           02 MINTITULH PIC X.                                          00002890
           02 MINTITULV PIC X.                                          00002900
           02 MINTITULO      PIC X(30).                                 00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MTITREA   PIC X.                                          00002930
           02 MTITREC   PIC X.                                          00002940
           02 MTITREP   PIC X.                                          00002950
           02 MTITREH   PIC X.                                          00002960
           02 MTITREV   PIC X.                                          00002970
           02 MTITREO   PIC X(35).                                      00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MFLUXA    PIC X.                                          00003000
           02 MFLUXC    PIC X.                                          00003010
           02 MFLUXP    PIC X.                                          00003020
           02 MFLUXH    PIC X.                                          00003030
           02 MFLUXV    PIC X.                                          00003040
           02 MFLUXO    PIC X(12).                                      00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MWENVOISAPA    PIC X.                                     00003070
           02 MWENVOISAPC    PIC X.                                     00003080
           02 MWENVOISAPP    PIC X.                                     00003090
           02 MWENVOISAPH    PIC X.                                     00003100
           02 MWENVOISAPV    PIC X.                                     00003110
           02 MWENVOISAPO    PIC X.                                     00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MFPRMPA   PIC X.                                          00003140
           02 MFPRMPC   PIC X.                                          00003150
           02 MFPRMPP   PIC X.                                          00003160
           02 MFPRMPH   PIC X.                                          00003170
           02 MFPRMPV   PIC X.                                          00003180
           02 MFPRMPO   PIC X.                                          00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MLIBELLEA      PIC X.                                     00003210
           02 MLIBELLEC PIC X.                                          00003220
           02 MLIBELLEP PIC X.                                          00003230
           02 MLIBELLEH PIC X.                                          00003240
           02 MLIBELLEV PIC X.                                          00003250
           02 MLIBELLEO      PIC X(35).                                 00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MFSOCEA   PIC X.                                          00003280
           02 MFSOCEC   PIC X.                                          00003290
           02 MFSOCEP   PIC X.                                          00003300
           02 MFSOCEH   PIC X.                                          00003310
           02 MFSOCEV   PIC X.                                          00003320
           02 MFSOCEO   PIC X.                                          00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MENSOCA   PIC X.                                          00003350
           02 MENSOCC   PIC X.                                          00003360
           02 MENSOCP   PIC X.                                          00003370
           02 MENSOCH   PIC X.                                          00003380
           02 MENSOCV   PIC X.                                          00003390
           02 MENSOCO   PIC X(3).                                       00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MENLIEUA  PIC X.                                          00003420
           02 MENLIEUC  PIC X.                                          00003430
           02 MENLIEUP  PIC X.                                          00003440
           02 MENLIEUH  PIC X.                                          00003450
           02 MENLIEUV  PIC X.                                          00003460
           02 MENLIEUO  PIC X(3).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MFSOCDA   PIC X.                                          00003490
           02 MFSOCDC   PIC X.                                          00003500
           02 MFSOCDP   PIC X.                                          00003510
           02 MFSOCDH   PIC X.                                          00003520
           02 MFSOCDV   PIC X.                                          00003530
           02 MFSOCDO   PIC X.                                          00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MDNSOCA   PIC X.                                          00003560
           02 MDNSOCC   PIC X.                                          00003570
           02 MDNSOCP   PIC X.                                          00003580
           02 MDNSOCH   PIC X.                                          00003590
           02 MDNSOCV   PIC X.                                          00003600
           02 MDNSOCO   PIC X(3).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MDNLIEUA  PIC X.                                          00003630
           02 MDNLIEUC  PIC X.                                          00003640
           02 MDNLIEUP  PIC X.                                          00003650
           02 MDNLIEUH  PIC X.                                          00003660
           02 MDNLIEUV  PIC X.                                          00003670
           02 MDNLIEUO  PIC X(3).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MFSOCVA   PIC X.                                          00003700
           02 MFSOCVC   PIC X.                                          00003710
           02 MFSOCVP   PIC X.                                          00003720
           02 MFSOCVH   PIC X.                                          00003730
           02 MFSOCVV   PIC X.                                          00003740
           02 MFSOCVO   PIC X.                                          00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MVNSOCA   PIC X.                                          00003770
           02 MVNSOCC   PIC X.                                          00003780
           02 MVNSOCP   PIC X.                                          00003790
           02 MVNSOCH   PIC X.                                          00003800
           02 MVNSOCV   PIC X.                                          00003810
           02 MVNSOCO   PIC X(3).                                       00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MVNLIEUA  PIC X.                                          00003840
           02 MVNLIEUC  PIC X.                                          00003850
           02 MVNLIEUP  PIC X.                                          00003860
           02 MVNLIEUH  PIC X.                                          00003870
           02 MVNLIEUV  PIC X.                                          00003880
           02 MVNLIEUO  PIC X(3).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MPECLGA   PIC X.                                          00003910
           02 MPECLGC   PIC X.                                          00003920
           02 MPECLGP   PIC X.                                          00003930
           02 MPECLGH   PIC X.                                          00003940
           02 MPECLGV   PIC X.                                          00003950
           02 MPECLGO   PIC X.                                          00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MREMA     PIC X.                                          00003980
           02 MREMC     PIC X.                                          00003990
           02 MREMP     PIC X.                                          00004000
           02 MREMH     PIC X.                                          00004010
           02 MREMV     PIC X.                                          00004020
           02 MREMO     PIC X(10).                                      00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MTXA      PIC X.                                          00004050
           02 MTXC      PIC X.                                          00004060
           02 MTXP      PIC X.                                          00004070
           02 MTXH      PIC X.                                          00004080
           02 MTXV      PIC X.                                          00004090
           02 MTXO      PIC X(2).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MVETUSTA  PIC X.                                          00004120
           02 MVETUSTC  PIC X.                                          00004130
           02 MVETUSTP  PIC X.                                          00004140
           02 MVETUSTH  PIC X.                                          00004150
           02 MVETUSTV  PIC X.                                          00004160
           02 MVETUSTO  PIC X(10).                                      00004170
           02 FILLER    PIC X(2).                                       00004180
           02 MPXFICA   PIC X.                                          00004190
           02 MPXFICC   PIC X.                                          00004200
           02 MPXFICP   PIC X.                                          00004210
           02 MPXFICH   PIC X.                                          00004220
           02 MPXFICV   PIC X.                                          00004230
           02 MPXFICO   PIC X.                                          00004240
           02 FILLER    PIC X(2).                                       00004250
           02 MPRMPA    PIC X.                                          00004260
           02 MPRMPC    PIC X.                                          00004270
           02 MPRMPP    PIC X.                                          00004280
           02 MPRMPH    PIC X.                                          00004290
           02 MPRMPV    PIC X.                                          00004300
           02 MPRMPO    PIC X.                                          00004310
           02 FILLER    PIC X(2).                                       00004320
           02 MTAXESA   PIC X.                                          00004330
           02 MTAXESC   PIC X.                                          00004340
           02 MTAXESP   PIC X.                                          00004350
           02 MTAXESH   PIC X.                                          00004360
           02 MTAXESV   PIC X.                                          00004370
           02 MTAXESO   PIC X.                                          00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MTVAA     PIC X.                                          00004400
           02 MTVAC     PIC X.                                          00004410
           02 MTVAP     PIC X.                                          00004420
           02 MTVAH     PIC X.                                          00004430
           02 MTVAV     PIC X.                                          00004440
           02 MTVAO     PIC X.                                          00004450
           02 FILLER    PIC X(2).                                       00004460
           02 MMTHTA    PIC X.                                          00004470
           02 MMTHTC    PIC X.                                          00004480
           02 MMTHTP    PIC X.                                          00004490
           02 MMTHTH    PIC X.                                          00004500
           02 MMTHTV    PIC X.                                          00004510
           02 MMTHTO    PIC X(7).                                       00004520
           02 FILLER    PIC X(2).                                       00004530
           02 MC1A      PIC X.                                          00004540
           02 MC1C      PIC X.                                          00004550
           02 MC1P      PIC X.                                          00004560
           02 MC1H      PIC X.                                          00004570
           02 MC1V      PIC X.                                          00004580
           02 MC1O      PIC X.                                          00004590
           02 FILLER    PIC X(2).                                       00004600
           02 MC2A      PIC X.                                          00004610
           02 MC2C      PIC X.                                          00004620
           02 MC2P      PIC X.                                          00004630
           02 MC2H      PIC X.                                          00004640
           02 MC2V      PIC X.                                          00004650
           02 MC2O      PIC X.                                          00004660
           02 FILLER    PIC X(2).                                       00004670
           02 MC3A      PIC X.                                          00004680
           02 MC3C      PIC X.                                          00004690
           02 MC3P      PIC X.                                          00004700
           02 MC3H      PIC X.                                          00004710
           02 MC3V      PIC X.                                          00004720
           02 MC3O      PIC X.                                          00004730
           02 FILLER    PIC X(2).                                       00004740
           02 MC4A      PIC X.                                          00004750
           02 MC4C      PIC X.                                          00004760
           02 MC4P      PIC X.                                          00004770
           02 MC4H      PIC X.                                          00004780
           02 MC4V      PIC X.                                          00004790
           02 MC4O      PIC X.                                          00004800
           02 FILLER    PIC X(2).                                       00004810
           02 MC5A      PIC X.                                          00004820
           02 MC5C      PIC X.                                          00004830
           02 MC5P      PIC X.                                          00004840
           02 MC5H      PIC X.                                          00004850
           02 MC5V      PIC X.                                          00004860
           02 MC5O      PIC X.                                          00004870
           02 FILLER    PIC X(2).                                       00004880
           02 MC6A      PIC X.                                          00004890
           02 MC6C      PIC X.                                          00004900
           02 MC6P      PIC X.                                          00004910
           02 MC6H      PIC X.                                          00004920
           02 MC6V      PIC X.                                          00004930
           02 MC6O      PIC X.                                          00004940
           02 FILLER    PIC X(2).                                       00004950
           02 MC7A      PIC X.                                          00004960
           02 MC7C      PIC X.                                          00004970
           02 MC7P      PIC X.                                          00004980
           02 MC7H      PIC X.                                          00004990
           02 MC7V      PIC X.                                          00005000
           02 MC7O      PIC X.                                          00005010
           02 FILLER    PIC X(2).                                       00005020
           02 MC8A      PIC X.                                          00005030
           02 MC8C      PIC X.                                          00005040
           02 MC8P      PIC X.                                          00005050
           02 MC8H      PIC X.                                          00005060
           02 MC8V      PIC X.                                          00005070
           02 MC8O      PIC X.                                          00005080
           02 FILLER    PIC X(2).                                       00005090
           02 MC9A      PIC X.                                          00005100
           02 MC9C      PIC X.                                          00005110
           02 MC9P      PIC X.                                          00005120
           02 MC9H      PIC X.                                          00005130
           02 MC9V      PIC X.                                          00005140
           02 MC9O      PIC X.                                          00005150
           02 FILLER    PIC X(2).                                       00005160
           02 MC10A     PIC X.                                          00005170
           02 MC10C     PIC X.                                          00005180
           02 MC10P     PIC X.                                          00005190
           02 MC10H     PIC X.                                          00005200
           02 MC10V     PIC X.                                          00005210
           02 MC10O     PIC X.                                          00005220
           02 FILLER    PIC X(2).                                       00005230
           02 MC11A     PIC X.                                          00005240
           02 MC11C     PIC X.                                          00005250
           02 MC11P     PIC X.                                          00005260
           02 MC11H     PIC X.                                          00005270
           02 MC11V     PIC X.                                          00005280
           02 MC11O     PIC X.                                          00005290
           02 FILLER    PIC X(2).                                       00005300
           02 MC12A     PIC X.                                          00005310
           02 MC12C     PIC X.                                          00005320
           02 MC12P     PIC X.                                          00005330
           02 MC12H     PIC X.                                          00005340
           02 MC12V     PIC X.                                          00005350
           02 MC12O     PIC X.                                          00005360
           02 FILLER    PIC X(2).                                       00005370
           02 MNREGRPA  PIC X.                                          00005380
           02 MNREGRPC  PIC X.                                          00005390
           02 MNREGRPP  PIC X.                                          00005400
           02 MNREGRPH  PIC X.                                          00005410
           02 MNREGRPV  PIC X.                                          00005420
           02 MNREGRPO  PIC X.                                          00005430
           02 FILLER    PIC X(2).                                       00005440
           02 MLREGRPA  PIC X.                                          00005450
           02 MLREGRPC  PIC X.                                          00005460
           02 MLREGRPP  PIC X.                                          00005470
           02 MLREGRPH  PIC X.                                          00005480
           02 MLREGRPV  PIC X.                                          00005490
           02 MLREGRPO  PIC X(30).                                      00005500
           02 MTABO OCCURS   3 TIMES .                                  00005510
             03 FILLER       PIC X(2).                                  00005520
             03 MARTICLA     PIC X.                                     00005530
             03 MARTICLC     PIC X.                                     00005540
             03 MARTICLP     PIC X.                                     00005550
             03 MARTICLH     PIC X.                                     00005560
             03 MARTICLV     PIC X.                                     00005570
             03 MARTICLO     PIC X(7).                                  00005580
             03 FILLER       PIC X(2).                                  00005590
             03 MLIBELA      PIC X.                                     00005600
             03 MLIBELC PIC X.                                          00005610
             03 MLIBELP PIC X.                                          00005620
             03 MLIBELH PIC X.                                          00005630
             03 MLIBELV PIC X.                                          00005640
             03 MLIBELO      PIC X(20).                                 00005650
             03 FILLER       PIC X(2).                                  00005660
             03 MSMTHTA      PIC X.                                     00005670
             03 MSMTHTC PIC X.                                          00005680
             03 MSMTHTP PIC X.                                          00005690
             03 MSMTHTH PIC X.                                          00005700
             03 MSMTHTV PIC X.                                          00005710
             03 MSMTHTO      PIC X(9).                                  00005720
             03 FILLER       PIC X(2).                                  00005730
             03 MSTVAA  PIC X.                                          00005740
             03 MSTVAC  PIC X.                                          00005750
             03 MSTVAP  PIC X.                                          00005760
             03 MSTVAH  PIC X.                                          00005770
             03 MSTVAV  PIC X.                                          00005780
             03 MSTVAO  PIC X.                                          00005790
             03 FILLER       PIC X(2).                                  00005800
             03 MTXPXA  PIC X.                                          00005810
             03 MTXPXC  PIC X.                                          00005820
             03 MTXPXP  PIC X.                                          00005830
             03 MTXPXH  PIC X.                                          00005840
             03 MTXPXV  PIC X.                                          00005850
             03 MTXPXO  PIC X(6).                                       00005860
             03 FILLER       PIC X(2).                                  00005870
             03 MSC1A   PIC X.                                          00005880
             03 MSC1C   PIC X.                                          00005890
             03 MSC1P   PIC X.                                          00005900
             03 MSC1H   PIC X.                                          00005910
             03 MSC1V   PIC X.                                          00005920
             03 MSC1O   PIC X.                                          00005930
             03 FILLER       PIC X(2).                                  00005940
             03 MSC2A   PIC X.                                          00005950
             03 MSC2C   PIC X.                                          00005960
             03 MSC2P   PIC X.                                          00005970
             03 MSC2H   PIC X.                                          00005980
             03 MSC2V   PIC X.                                          00005990
             03 MSC2O   PIC X.                                          00006000
             03 FILLER       PIC X(2).                                  00006010
             03 MSC3A   PIC X.                                          00006020
             03 MSC3C   PIC X.                                          00006030
             03 MSC3P   PIC X.                                          00006040
             03 MSC3H   PIC X.                                          00006050
             03 MSC3V   PIC X.                                          00006060
             03 MSC3O   PIC X.                                          00006070
             03 FILLER       PIC X(2).                                  00006080
             03 MSC4A   PIC X.                                          00006090
             03 MSC4C   PIC X.                                          00006100
             03 MSC4P   PIC X.                                          00006110
             03 MSC4H   PIC X.                                          00006120
             03 MSC4V   PIC X.                                          00006130
             03 MSC4O   PIC X.                                          00006140
             03 FILLER       PIC X(2).                                  00006150
             03 MSTAXEA      PIC X.                                     00006160
             03 MSTAXEC PIC X.                                          00006170
             03 MSTAXEP PIC X.                                          00006180
             03 MSTAXEH PIC X.                                          00006190
             03 MSTAXEV PIC X.                                          00006200
             03 MSTAXEO      PIC X.                                     00006210
             03 FILLER       PIC X(2).                                  00006220
             03 MDEDUPA      PIC X.                                     00006230
             03 MDEDUPC PIC X.                                          00006240
             03 MDEDUPP PIC X.                                          00006250
             03 MDEDUPH PIC X.                                          00006260
             03 MDEDUPV PIC X.                                          00006270
             03 MDEDUPO      PIC X.                                     00006280
      * ZONE CMD AIDA                                                   00006290
           02 FILLER    PIC X(2).                                       00006300
           02 MSUPPRA   PIC X.                                          00006310
           02 MSUPPRC   PIC X.                                          00006320
           02 MSUPPRP   PIC X.                                          00006330
           02 MSUPPRH   PIC X.                                          00006340
           02 MSUPPRV   PIC X.                                          00006350
           02 MSUPPRO   PIC X.                                          00006360
           02 FILLER    PIC X(2).                                       00006370
           02 MLIBERRA  PIC X.                                          00006380
           02 MLIBERRC  PIC X.                                          00006390
           02 MLIBERRP  PIC X.                                          00006400
           02 MLIBERRH  PIC X.                                          00006410
           02 MLIBERRV  PIC X.                                          00006420
           02 MLIBERRO  PIC X(79).                                      00006430
      * CODE TRANSACTION                                                00006440
           02 FILLER    PIC X(2).                                       00006450
           02 MCODTRAA  PIC X.                                          00006460
           02 MCODTRAC  PIC X.                                          00006470
           02 MCODTRAP  PIC X.                                          00006480
           02 MCODTRAH  PIC X.                                          00006490
           02 MCODTRAV  PIC X.                                          00006500
           02 MCODTRAO  PIC X(4).                                       00006510
      * CICS DE TRAVAIL                                                 00006520
           02 FILLER    PIC X(2).                                       00006530
           02 MCICSA    PIC X.                                          00006540
           02 MCICSC    PIC X.                                          00006550
           02 MCICSP    PIC X.                                          00006560
           02 MCICSH    PIC X.                                          00006570
           02 MCICSV    PIC X.                                          00006580
           02 MCICSO    PIC X(5).                                       00006590
      * NETNAME                                                         00006600
           02 FILLER    PIC X(2).                                       00006610
           02 MNETNAMA  PIC X.                                          00006620
           02 MNETNAMC  PIC X.                                          00006630
           02 MNETNAMP  PIC X.                                          00006640
           02 MNETNAMH  PIC X.                                          00006650
           02 MNETNAMV  PIC X.                                          00006660
           02 MNETNAMO  PIC X(8).                                       00006670
      * CODE TERMINAL                                                   00006680
           02 FILLER    PIC X(2).                                       00006690
           02 MSCREENA  PIC X.                                          00006700
           02 MSCREENC  PIC X.                                          00006710
           02 MSCREENP  PIC X.                                          00006720
           02 MSCREENH  PIC X.                                          00006730
           02 MSCREENV  PIC X.                                          00006740
           02 MSCREENO  PIC X(4).                                       00006750
                                                                                
