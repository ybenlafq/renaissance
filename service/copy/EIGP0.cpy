      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIGP0   EIGP0                                              00000020
      ***************************************************************** 00000030
       01   EIGP0I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * APPLID CICS COURANT                                             00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MCICSI    PIC X(8).                                       00000150
      * HEURE                                                           00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MTIMJOUI  PIC X(10).                                      00000200
      * NETNAME                                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNETNAMI  PIC X(8).                                       00000250
      * TERMINAL USER                                                   00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MSCREENI  PIC X(4).                                       00000300
      * TACHE MENU                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCODTRAI  PIC X(4).                                       00000350
      * MAP BMS                                                         00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAPBMSL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MMAPBMSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAPBMSF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MMAPBMSI  PIC X(8).                                       00000400
      * OPTION TRAITEMENT                                               00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTIONL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MOPTIONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOPTIONF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MOPTIONI  PIC X.                                          00000450
      * CODE IMPRIMANTE TCT                                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTCTL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTCTF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTCTI  PIC X(4).                                       00000500
      * CODE APPLID COPURANT                                            00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAPPLIDL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MAPPLIDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAPPLIDF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MAPPLIDI  PIC X(8).                                       00000550
      * MESSAGE                                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLIBERRI  PIC X(78).                                      00000600
      ***************************************************************** 00000610
      * SDF: EIGP0   EIGP0                                              00000620
      ***************************************************************** 00000630
       01   EIGP0O REDEFINES EIGP0I.                                    00000640
           02 FILLER    PIC X(12).                                      00000650
      * DATE DU JOUR                                                    00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MDATJOUA  PIC X.                                          00000680
           02 MDATJOUC  PIC X.                                          00000690
           02 MDATJOUP  PIC X.                                          00000700
           02 MDATJOUH  PIC X.                                          00000710
           02 MDATJOUV  PIC X.                                          00000720
           02 MDATJOUO  PIC X(10).                                      00000730
      * APPLID CICS COURANT                                             00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MCICSA    PIC X.                                          00000760
           02 MCICSC    PIC X.                                          00000770
           02 MCICSP    PIC X.                                          00000780
           02 MCICSH    PIC X.                                          00000790
           02 MCICSV    PIC X.                                          00000800
           02 MCICSO    PIC X(8).                                       00000810
      * HEURE                                                           00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(10).                                      00000890
      * NETNAME                                                         00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MNETNAMA  PIC X.                                          00000920
           02 MNETNAMC  PIC X.                                          00000930
           02 MNETNAMP  PIC X.                                          00000940
           02 MNETNAMH  PIC X.                                          00000950
           02 MNETNAMV  PIC X.                                          00000960
           02 MNETNAMO  PIC X(8).                                       00000970
      * TERMINAL USER                                                   00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MSCREENA  PIC X.                                          00001000
           02 MSCREENC  PIC X.                                          00001010
           02 MSCREENP  PIC X.                                          00001020
           02 MSCREENH  PIC X.                                          00001030
           02 MSCREENV  PIC X.                                          00001040
           02 MSCREENO  PIC X(4).                                       00001050
      * TACHE MENU                                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MCODTRAA  PIC X.                                          00001080
           02 MCODTRAC  PIC X.                                          00001090
           02 MCODTRAP  PIC X.                                          00001100
           02 MCODTRAH  PIC X.                                          00001110
           02 MCODTRAV  PIC X.                                          00001120
           02 MCODTRAO  PIC X(4).                                       00001130
      * MAP BMS                                                         00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MMAPBMSA  PIC X.                                          00001160
           02 MMAPBMSC  PIC X.                                          00001170
           02 MMAPBMSP  PIC X.                                          00001180
           02 MMAPBMSH  PIC X.                                          00001190
           02 MMAPBMSV  PIC X.                                          00001200
           02 MMAPBMSO  PIC X(8).                                       00001210
      * OPTION TRAITEMENT                                               00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MOPTIONA  PIC X.                                          00001240
           02 MOPTIONC  PIC X.                                          00001250
           02 MOPTIONP  PIC X.                                          00001260
           02 MOPTIONH  PIC X.                                          00001270
           02 MOPTIONV  PIC X.                                          00001280
           02 MOPTIONO  PIC X.                                          00001290
      * CODE IMPRIMANTE TCT                                             00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCODTCTA  PIC X.                                          00001320
           02 MCODTCTC  PIC X.                                          00001330
           02 MCODTCTP  PIC X.                                          00001340
           02 MCODTCTH  PIC X.                                          00001350
           02 MCODTCTV  PIC X.                                          00001360
           02 MCODTCTO  PIC X(4).                                       00001370
      * CODE APPLID COPURANT                                            00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MAPPLIDA  PIC X.                                          00001400
           02 MAPPLIDC  PIC X.                                          00001410
           02 MAPPLIDP  PIC X.                                          00001420
           02 MAPPLIDH  PIC X.                                          00001430
           02 MAPPLIDV  PIC X.                                          00001440
           02 MAPPLIDO  PIC X(8).                                       00001450
      * MESSAGE                                                         00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MLIBERRA  PIC X.                                          00001480
           02 MLIBERRC  PIC X.                                          00001490
           02 MLIBERRP  PIC X.                                          00001500
           02 MLIBERRH  PIC X.                                          00001510
           02 MLIBERRV  PIC X.                                          00001520
           02 MLIBERRO  PIC X(78).                                      00001530
                                                                                
