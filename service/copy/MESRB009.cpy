      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:07 >
      
      *****************************************************************
      *   COPY MESSAGE MQ QUESTION SOUSCRIPTION/R�SILIATION REDBOX
      *   VERS LE S.I. REDBOX. (RBxQSouRes2)
      *   message mesrb001 enrichi pour la telephonie
      *****************************************************************
      *
      *
       05 MESRB001-DATA.
      *
      * DONN�ES CLIENT
      *
      *   CODE CLIENT
          10 MESRB01-CCLIENT       PIC    X(08).
      *   CIVILIT�
          10 MESRB01-CIVILITE      PIC    X(05).
      *   NOM CLIENT
          10 MESRB01-NOMCLI        PIC    X(25).
      *   PR�NOM CLIENT
          10 MESRB01-PRENOM        PIC    X(15).
      *   T�L�PHONE FIXE
          10 MESRB01-TELFIXE       PIC    X(10).
      *   T�L�PHONE PORTABLE
          10 MESRB01-TELPORT       PIC    X(10).
      *   T�L�PHONE BUREAU
          10 MESRB01-TELBUR        PIC    X(10).
      *   POSTE BUREAU
          10 MESRB01-PSTBUR        PIC    X(05).
      *   E-MAIL
          10 MESRB01-EMAIL         PIC    X(63).
      *   ADRESSES FACTURATION ET LIVRAISON
          10 MESRB01-ADRESSE OCCURS 2.
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)
             15 MESRB01-TYPADR        PIC    X(01).
      *      NUM�RO DE VOIE
             15 MESRB01-NUMVOIE       PIC    X(05).
      *      TYPE DE VOIE
             15 MESRB01-TYPVOIE       PIC    X(04).
      *      NOM DE VOIE
             15 MESRB01-NOMVOIE       PIC    X(21).
      *      COMPL�MENT ADRESSE DE LIVRAISON 1
             15 MESRB01-CADRFAC1      PIC    X(32).
      *      COMPL�MENT ADRESSE DE LIVRAISON2
             15 MESRB01-CADRFAC2      PIC    X(32).
      *      B�TIMENT
             15 MESRB01-BATIMENT      PIC    X(03).
      *      ESCALIER
             15 MESRB01-ESCALIER      PIC    X(03).
      *      �TAGE
             15 MESRB01-ETAGE         PIC    X(03).
      *      PORTE
             15 MESRB01-PORTE         PIC    X(03).
      *      CODE PAYS
             15 MESRB01-CPAYS         PIC    X(03).
      *      CODE INSEE
             15 MESRB01-CINSEE        PIC    X(05).
      *      COMMUNE
             15 MESRB01-COMMUNE       PIC    X(32).
      *      CODE POSTAL
             15 MESRB01-CPOSTAL       PIC    X(05).
      *      CODE client ucm
             15 MESRB01-idcliucm      PIC    X(08).
      *      CODE adresse ucm
             15 MESRB01-idadrucm      PIC    X(08).
      *
      *      DONN�ES REDBOX
      *
      *   N� TRANSACTION REDBOX
          10 MESRB01-TREDBOX       PIC    X(10).
      *   N� VENTE GV
          10 MESRB01-NVENTEGV      PIC    X(13).
      *   N� DE CONTRAT
          10 MESRB01-NCONTRAT      PIC    X(18).
      *   ST� DE TRANSACTION
          10 MESRB01-TSOC          PIC    X(03).
      *   LIEU DE TRANSACTION
          10 MESRB01-TLIEU         PIC    X(03).
      *   DATE DE TRANSACTION
          10 MESRB01-TDATE         PIC    X(08).
      *   HEURE DE TRANSACTION
          10 MESRB01-THEURE        PIC    X(06).
      *   TYPE D'ENLEVEMENT ('M' / 'E')
          10 MESRB01-TENLV         PIC    X(01).
      *
      *      DONN�ES LIGNE DE VENTE
      *
      *   *
          10 MESRB09-DETVENTE OCCURS 80.
            12 mesrb09-detvente1.
      *      TYPE DE TRAITEMENT   ('A' / 'C')
             15 MESRB09-TTRAIT     PIC    X(01).
      *      CODE OFFRE
             15 MESRB09-COFFRE     PIC    X(07).
      *      CODIC OU CODE PRESTATION
             15 MESRB09-CPREST     PIC    X(07).
      *      QUANTIT�
             15 MESRB09-QUANTITE   PIC    9(05).
      *      PRIX ligne DE VENTE
             15 MESRB09-PVENTE     PIC   S9(07)V99.
      *      MODE DE D�LIVRANCE
             15 MESRB09-CMODDEL    PIC    X(05).
      *      DATE DE D�LIVRANCE
             15 MESRB09-DDELIV     PIC    X(08).
      *      FILIALE DU VENDEUR
             15 MESRB09-FVEND      PIC    X(03).
      *      MATRICULE DU VENDEUR
             15 MESRB09-CVEND      PIC    X(07).
      *      N� DE S�RIE DE L'�QUIPEMENT
             15 MESRB09-NSEREQ     PIC    X(15).
      *      ligne de vente
             15 MESRB09-orderitem  PIC    X(11).
      *      nature de la ligne de vente
             15 MESRB09-tentite    PIC    X(03).
      *      PRIX DE VENTE ventil�
             15 MESRB09-PVENTEV    PIC   S9(07)V99.
      *      PRIX DE l'offre avec ses options
             15 MESRB09-PVENTEO    PIC   S9(07)V99.
      *      lien d'offre
             15 MESRB09-relatcode  PIC    9(03).
      *      taux de tva
             15 MESRB09-vatrate    PIC    9(03)v99.
      *      ligne de vente  de l'offre
             15 MESRB09-parentid   PIC    X(11).
      *      ligne de vente  de r�f�rence pas pour les offres
             15 MESRB09-slitelid   PIC    X(11).
      *
      *   donnees aditionnelles de l'offre
          10 MESRB09-adition1.
           12 MESRB09-adition  OCCURS 20.
      *      ligne de vente
             15 MESRB09-orderitem2 PIC    X(11).
      *      application qui g�re
             15 MESRB09-system     PIC    X(15).
      *      code dossier(gv05)
             15 MESRB09-folder     PIC    X(25).
      *      code control cctrl (gv08)
             15 MESRB09-code       PIC    X(15).
      *      valeur du controle valctrl (gv08)
             15 MESRB09-value      PIC    x(255).
      *
      
