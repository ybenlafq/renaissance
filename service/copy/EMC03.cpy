      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: liste des chassis de groupe                                00000020
      ***************************************************************** 00000030
       01   EMC03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGRPL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCGRPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCGRPF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCGRPI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGRPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLGRPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLGRPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLGRPI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMSELL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCFAMSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFAMSELF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMSELI      PIC X(5).                                  00000330
           02 MTABLGRPI OCCURS   10 TIMES .                             00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MSELI   PIC X.                                          00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFACTIFL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MFACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFACTIFF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MFACTIFI     PIC X.                                     00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHASSL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLCHASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHASSF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLCHASSI     PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCHASSL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCCHASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCHASSF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCCHASSI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDCREATL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDCREATF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDCREATI     PIC X(10).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NBCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 NBCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 NBCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 NBCODICI     PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMNWL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCFAMNWL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFAMNWF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCFAMNWI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHNWL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLCHNWL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCHNWF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLCHNWI   PIC X(20).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCHNWL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCCHNWL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCHNWF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCCHNWI   PIC X(7).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATNWL     COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MDCREATNWL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDCREATNWF     PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MDCREATNWI     PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(78).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: liste des chassis de groupe                                00001000
      ***************************************************************** 00001010
       01   EMC03O REDEFINES EMC03I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 NPAGEA    PIC X.                                          00001190
           02 NPAGEC    PIC X.                                          00001200
           02 NPAGEP    PIC X.                                          00001210
           02 NPAGEH    PIC X.                                          00001220
           02 NPAGEV    PIC X.                                          00001230
           02 NPAGEO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 NBPAGEA   PIC X.                                          00001260
           02 NBPAGEC   PIC X.                                          00001270
           02 NBPAGEP   PIC X.                                          00001280
           02 NBPAGEH   PIC X.                                          00001290
           02 NBPAGEV   PIC X.                                          00001300
           02 NBPAGEO   PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCGRPA    PIC X.                                          00001330
           02 MCGRPC    PIC X.                                          00001340
           02 MCGRPP    PIC X.                                          00001350
           02 MCGRPH    PIC X.                                          00001360
           02 MCGRPV    PIC X.                                          00001370
           02 MCGRPO    PIC X(7).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLGRPA    PIC X.                                          00001400
           02 MLGRPC    PIC X.                                          00001410
           02 MLGRPP    PIC X.                                          00001420
           02 MLGRPH    PIC X.                                          00001430
           02 MLGRPV    PIC X.                                          00001440
           02 MLGRPO    PIC X(20).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCFAMSELA      PIC X.                                     00001470
           02 MCFAMSELC PIC X.                                          00001480
           02 MCFAMSELP PIC X.                                          00001490
           02 MCFAMSELH PIC X.                                          00001500
           02 MCFAMSELV PIC X.                                          00001510
           02 MCFAMSELO      PIC X(5).                                  00001520
           02 MTABLGRPO OCCURS   10 TIMES .                             00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MSELA   PIC X.                                          00001550
             03 MSELC   PIC X.                                          00001560
             03 MSELP   PIC X.                                          00001570
             03 MSELH   PIC X.                                          00001580
             03 MSELV   PIC X.                                          00001590
             03 MSELO   PIC X.                                          00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MFACTIFA     PIC X.                                     00001620
             03 MFACTIFC     PIC X.                                     00001630
             03 MFACTIFP     PIC X.                                     00001640
             03 MFACTIFH     PIC X.                                     00001650
             03 MFACTIFV     PIC X.                                     00001660
             03 MFACTIFO     PIC X.                                     00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MCFAMA  PIC X.                                          00001690
             03 MCFAMC  PIC X.                                          00001700
             03 MCFAMP  PIC X.                                          00001710
             03 MCFAMH  PIC X.                                          00001720
             03 MCFAMV  PIC X.                                          00001730
             03 MCFAMO  PIC X(5).                                       00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MLCHASSA     PIC X.                                     00001760
             03 MLCHASSC     PIC X.                                     00001770
             03 MLCHASSP     PIC X.                                     00001780
             03 MLCHASSH     PIC X.                                     00001790
             03 MLCHASSV     PIC X.                                     00001800
             03 MLCHASSO     PIC X(20).                                 00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MCCHASSA     PIC X.                                     00001830
             03 MCCHASSC     PIC X.                                     00001840
             03 MCCHASSP     PIC X.                                     00001850
             03 MCCHASSH     PIC X.                                     00001860
             03 MCCHASSV     PIC X.                                     00001870
             03 MCCHASSO     PIC X(7).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MDCREATA     PIC X.                                     00001900
             03 MDCREATC     PIC X.                                     00001910
             03 MDCREATP     PIC X.                                     00001920
             03 MDCREATH     PIC X.                                     00001930
             03 MDCREATV     PIC X.                                     00001940
             03 MDCREATO     PIC X(10).                                 00001950
             03 FILLER       PIC X(2).                                  00001960
             03 NBCODICA     PIC X.                                     00001970
             03 NBCODICC     PIC X.                                     00001980
             03 NBCODICP     PIC X.                                     00001990
             03 NBCODICH     PIC X.                                     00002000
             03 NBCODICV     PIC X.                                     00002010
             03 NBCODICO     PIC X(6).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCFAMNWA  PIC X.                                          00002040
           02 MCFAMNWC  PIC X.                                          00002050
           02 MCFAMNWP  PIC X.                                          00002060
           02 MCFAMNWH  PIC X.                                          00002070
           02 MCFAMNWV  PIC X.                                          00002080
           02 MCFAMNWO  PIC X(5).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLCHNWA   PIC X.                                          00002110
           02 MLCHNWC   PIC X.                                          00002120
           02 MLCHNWP   PIC X.                                          00002130
           02 MLCHNWH   PIC X.                                          00002140
           02 MLCHNWV   PIC X.                                          00002150
           02 MLCHNWO   PIC X(20).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCCHNWA   PIC X.                                          00002180
           02 MCCHNWC   PIC X.                                          00002190
           02 MCCHNWP   PIC X.                                          00002200
           02 MCCHNWH   PIC X.                                          00002210
           02 MCCHNWV   PIC X.                                          00002220
           02 MCCHNWO   PIC X(7).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MDCREATNWA     PIC X.                                     00002250
           02 MDCREATNWC     PIC X.                                     00002260
           02 MDCREATNWP     PIC X.                                     00002270
           02 MDCREATNWH     PIC X.                                     00002280
           02 MDCREATNWV     PIC X.                                     00002290
           02 MDCREATNWO     PIC X(10).                                 00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(78).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
