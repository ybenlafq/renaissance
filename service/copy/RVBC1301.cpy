      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1300                           *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVBC1301.                                                            
      *    *************************************************************        
      *                       NADRESSE                                          
           10 BC13-NCLIENTADR      PIC X(8).                                    
      *    *************************************************************        
      *                       CTYPDON                                           
           10 BC13-CTYPDON         PIC X(1).                                    
      *    *************************************************************        
      *                       LDONNEES                                          
           10 BC13-LDONNEES        PIC X(15).                                   
      *    *************************************************************        
      *                       DSYST                                             
           10 BC13-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       LDONNEES COMPLEMENTAIRES                          
           10 BC13-LDONNEE1        PIC X(05).                                   
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC1301-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC13-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC13-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC13-CTYPDON-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC13-CTYPDON-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC13-LDONNEES-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC13-LDONNEES-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC13-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC13-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC13-LDONNEE1-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC13-LDONNEE1-F      PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
                                                                                
