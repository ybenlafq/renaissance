      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHC0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHC0300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHC0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHC0300.                                                            
      *}                                                                        
           02  HC03-NLIEUHC                                                     
               PIC X(0003).                                                     
           02  HC03-NCHS                                                        
               PIC X(0007).                                                     
           02  HC03-NSEQ                                                        
               PIC X(0003).                                                     
           02  HC03-NCODIC                                                      
               PIC X(0007).                                                     
           02  HC03-NENVOI                                                      
               PIC X(0007).                                                     
           02  HC03-DDEPOT                                                      
               PIC X(0008).                                                     
           02  HC03-CPROVORIG                                                   
               PIC X(0001).                                                     
           02  HC03-CORIGINE                                                    
               PIC X(0007).                                                     
           02  HC03-CSERIE                                                      
               PIC X(0016).                                                     
           02  HC03-DACCFOUR                                                    
               PIC X(0008).                                                     
           02  HC03-NACCORD                                                     
               PIC X(0012).                                                     
           02  HC03-INTERLOC                                                    
               PIC X(0010).                                                     
           02  HC03-DSOLDE                                                      
               PIC X(0008).                                                     
           02  HC03-CLIENT                                                      
               PIC X(0020).                                                     
           02  HC03-NSAV                                                        
               PIC X(0003).                                                     
           02  HC03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHC0300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHC0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHC0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NLIEUHC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NLIEUHC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NCHS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NCHS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-DDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-DDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-CPROVORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-CPROVORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-CORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-CORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-CSERIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-CSERIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-DACCFOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-DACCFOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-INTERLOC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-INTERLOC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-DSOLDE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-DSOLDE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-CLIENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-CLIENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-NSAV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-NSAV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
