      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIGP3   EIGP3                                              00000020
      ***************************************************************** 00000030
       01   EIGP3I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * APPLID CICS COURANT                                             00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MCICSI    PIC X(8).                                       00000150
      * HEURE                                                           00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MTIMJOUI  PIC X(10).                                      00000200
      * NETNAME                                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNETNAMI  PIC X(8).                                       00000250
      * TERMINAL USER                                                   00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MSCREENI  PIC X(4).                                       00000300
      * TRAITEMENT                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRAITEL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MTRAITEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTRAITEF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTRAITEI  PIC X(12).                                      00000350
      * TACHE MENU                                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCODTRAI  PIC X(4).                                       00000400
      * MAP BMS                                                         00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAPBMSL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MMAPBMSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMAPBMSF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MMAPBMSI  PIC X(8).                                       00000450
      * CODE TCT IMPRIMANTE                                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTCTL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTCTF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTCTI  PIC X(4).                                       00000500
      * CICS APPARTENANCE                                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAPPLIDL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MAPPLIDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MAPPLIDF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MAPPLIDI  PIC X(8).                                       00000550
      * MARQUE IMPRIMANTE                                               00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMARQUEL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MMARQUEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMARQUEF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MMARQUEI  PIC X(15).                                      00000600
      * MODELE IMPRIMANTE                                               00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODELEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MMODELEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODELEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MMODELEI  PIC X(8).                                       00000650
      * PROTOCOLE IMPRIMANTE                                            00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROTOCL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MPROTOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROTOCF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MPROTOCI  PIC X.                                          00000700
      * PROGRAMME ASSOCIE                                               00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROGRAL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MPROGRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPROGRAF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MPROGRAI  PIC X(8).                                       00000750
      * LOCALISATION SOCIETE                                            00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSOCIETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCIETF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSOCIETI  PIC X(3).                                       00000800
      * LOCALISATION LIEU                                               00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUUUL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIEUUUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUUUF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIEUUUI  PIC X(3).                                       00000850
      * NUMERO DE SPOOL                                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMSPLL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNUMSPLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMSPLF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNUMSPLI  PIC X(3).                                       00000900
      * POUR EMULATION 36                                               00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEMUL36L  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MEMUL36L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MEMUL36F  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MEMUL36I  PIC X(3).                                       00000950
      * LIBELLE TOUCHES FONC                                            00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOUCHEL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MTOUCHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOUCHEF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MTOUCHEI  PIC X(38).                                      00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBCONL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIBCONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBCONF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLIBCONI  PIC X(20).                                      00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONFIRL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCONFIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCONFIRF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCONFIRI  PIC X.                                          00001080
      * MESSAGE                                                         00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(78).                                      00001130
      ***************************************************************** 00001140
      * SDF: EIGP3   EIGP3                                              00001150
      ***************************************************************** 00001160
       01   EIGP3O REDEFINES EIGP3I.                                    00001170
           02 FILLER    PIC X(12).                                      00001180
      * DATE DU JOUR                                                    00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
      * APPLID CICS COURANT                                             00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCICSA    PIC X.                                          00001290
           02 MCICSC    PIC X.                                          00001300
           02 MCICSP    PIC X.                                          00001310
           02 MCICSH    PIC X.                                          00001320
           02 MCICSV    PIC X.                                          00001330
           02 MCICSO    PIC X(8).                                       00001340
      * HEURE                                                           00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MTIMJOUA  PIC X.                                          00001370
           02 MTIMJOUC  PIC X.                                          00001380
           02 MTIMJOUP  PIC X.                                          00001390
           02 MTIMJOUH  PIC X.                                          00001400
           02 MTIMJOUV  PIC X.                                          00001410
           02 MTIMJOUO  PIC X(10).                                      00001420
      * NETNAME                                                         00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNETNAMA  PIC X.                                          00001450
           02 MNETNAMC  PIC X.                                          00001460
           02 MNETNAMP  PIC X.                                          00001470
           02 MNETNAMH  PIC X.                                          00001480
           02 MNETNAMV  PIC X.                                          00001490
           02 MNETNAMO  PIC X(8).                                       00001500
      * TERMINAL USER                                                   00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MSCREENA  PIC X.                                          00001530
           02 MSCREENC  PIC X.                                          00001540
           02 MSCREENP  PIC X.                                          00001550
           02 MSCREENH  PIC X.                                          00001560
           02 MSCREENV  PIC X.                                          00001570
           02 MSCREENO  PIC X(4).                                       00001580
      * TRAITEMENT                                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MTRAITEA  PIC X.                                          00001610
           02 MTRAITEC  PIC X.                                          00001620
           02 MTRAITEP  PIC X.                                          00001630
           02 MTRAITEH  PIC X.                                          00001640
           02 MTRAITEV  PIC X.                                          00001650
           02 MTRAITEO  PIC X(12).                                      00001660
      * TACHE MENU                                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCODTRAA  PIC X.                                          00001690
           02 MCODTRAC  PIC X.                                          00001700
           02 MCODTRAP  PIC X.                                          00001710
           02 MCODTRAH  PIC X.                                          00001720
           02 MCODTRAV  PIC X.                                          00001730
           02 MCODTRAO  PIC X(4).                                       00001740
      * MAP BMS                                                         00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MMAPBMSA  PIC X.                                          00001770
           02 MMAPBMSC  PIC X.                                          00001780
           02 MMAPBMSP  PIC X.                                          00001790
           02 MMAPBMSH  PIC X.                                          00001800
           02 MMAPBMSV  PIC X.                                          00001810
           02 MMAPBMSO  PIC X(8).                                       00001820
      * CODE TCT IMPRIMANTE                                             00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCODTCTA  PIC X.                                          00001850
           02 MCODTCTC  PIC X.                                          00001860
           02 MCODTCTP  PIC X.                                          00001870
           02 MCODTCTH  PIC X.                                          00001880
           02 MCODTCTV  PIC X.                                          00001890
           02 MCODTCTO  PIC X(4).                                       00001900
      * CICS APPARTENANCE                                               00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MAPPLIDA  PIC X.                                          00001930
           02 MAPPLIDC  PIC X.                                          00001940
           02 MAPPLIDP  PIC X.                                          00001950
           02 MAPPLIDH  PIC X.                                          00001960
           02 MAPPLIDV  PIC X.                                          00001970
           02 MAPPLIDO  PIC X(8).                                       00001980
      * MARQUE IMPRIMANTE                                               00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MMARQUEA  PIC X.                                          00002010
           02 MMARQUEC  PIC X.                                          00002020
           02 MMARQUEP  PIC X.                                          00002030
           02 MMARQUEH  PIC X.                                          00002040
           02 MMARQUEV  PIC X.                                          00002050
           02 MMARQUEO  PIC X(15).                                      00002060
      * MODELE IMPRIMANTE                                               00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MMODELEA  PIC X.                                          00002090
           02 MMODELEC  PIC X.                                          00002100
           02 MMODELEP  PIC X.                                          00002110
           02 MMODELEH  PIC X.                                          00002120
           02 MMODELEV  PIC X.                                          00002130
           02 MMODELEO  PIC X(8).                                       00002140
      * PROTOCOLE IMPRIMANTE                                            00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MPROTOCA  PIC X.                                          00002170
           02 MPROTOCC  PIC X.                                          00002180
           02 MPROTOCP  PIC X.                                          00002190
           02 MPROTOCH  PIC X.                                          00002200
           02 MPROTOCV  PIC X.                                          00002210
           02 MPROTOCO  PIC X.                                          00002220
      * PROGRAMME ASSOCIE                                               00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MPROGRAA  PIC X.                                          00002250
           02 MPROGRAC  PIC X.                                          00002260
           02 MPROGRAP  PIC X.                                          00002270
           02 MPROGRAH  PIC X.                                          00002280
           02 MPROGRAV  PIC X.                                          00002290
           02 MPROGRAO  PIC X(8).                                       00002300
      * LOCALISATION SOCIETE                                            00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MSOCIETA  PIC X.                                          00002330
           02 MSOCIETC  PIC X.                                          00002340
           02 MSOCIETP  PIC X.                                          00002350
           02 MSOCIETH  PIC X.                                          00002360
           02 MSOCIETV  PIC X.                                          00002370
           02 MSOCIETO  PIC X(3).                                       00002380
      * LOCALISATION LIEU                                               00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIEUUUA  PIC X.                                          00002410
           02 MLIEUUUC  PIC X.                                          00002420
           02 MLIEUUUP  PIC X.                                          00002430
           02 MLIEUUUH  PIC X.                                          00002440
           02 MLIEUUUV  PIC X.                                          00002450
           02 MLIEUUUO  PIC X(3).                                       00002460
      * NUMERO DE SPOOL                                                 00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNUMSPLA  PIC X.                                          00002490
           02 MNUMSPLC  PIC X.                                          00002500
           02 MNUMSPLP  PIC X.                                          00002510
           02 MNUMSPLH  PIC X.                                          00002520
           02 MNUMSPLV  PIC X.                                          00002530
           02 MNUMSPLO  PIC X(3).                                       00002540
      * POUR EMULATION 36                                               00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MEMUL36A  PIC X.                                          00002570
           02 MEMUL36C  PIC X.                                          00002580
           02 MEMUL36P  PIC X.                                          00002590
           02 MEMUL36H  PIC X.                                          00002600
           02 MEMUL36V  PIC X.                                          00002610
           02 MEMUL36O  PIC X(3).                                       00002620
      * LIBELLE TOUCHES FONC                                            00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MTOUCHEA  PIC X.                                          00002650
           02 MTOUCHEC  PIC X.                                          00002660
           02 MTOUCHEP  PIC X.                                          00002670
           02 MTOUCHEH  PIC X.                                          00002680
           02 MTOUCHEV  PIC X.                                          00002690
           02 MTOUCHEO  PIC X(38).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MLIBCONA  PIC X.                                          00002720
           02 MLIBCONC  PIC X.                                          00002730
           02 MLIBCONP  PIC X.                                          00002740
           02 MLIBCONH  PIC X.                                          00002750
           02 MLIBCONV  PIC X.                                          00002760
           02 MLIBCONO  PIC X(20).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCONFIRA  PIC X.                                          00002790
           02 MCONFIRC  PIC X.                                          00002800
           02 MCONFIRP  PIC X.                                          00002810
           02 MCONFIRH  PIC X.                                          00002820
           02 MCONFIRV  PIC X.                                          00002830
           02 MCONFIRO  PIC X.                                          00002840
      * MESSAGE                                                         00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(78).                                      00002920
                                                                                
