      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSB0201                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSB0201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSB0201.                                                            
      *}                                                                        
      *                       REFCMDEXT                                         
           10 SB02-REFCMDEXT       PIC X(30).                                   
      *                       TYPE                                              
           10 SB02-TYPE            PIC X(10).                                   
      *                       TYPEV                                             
           10 SB02-TYPEV           PIC X(2).                                    
      *                       NLIGNE                                            
           10 SB02-NLIGNE          PIC S9(2)V USAGE COMP-3.                     
      *                       CODIC                                             
           10 SB02-CODIC           PIC X(7).                                    
      *                       QTE                                               
           10 SB02-QTE             PIC S9(10)V USAGE COMP-3.                    
      *                       PVTOTAL                                           
           10 SB02-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVUNIT                                            
           10 SB02-PVUNIT          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DDELIV                                            
           10 SB02-DDELIV          PIC X(8).                                    
      *                       CMODDEL                                           
           10 SB02-CMODDEL         PIC X(5).                                    
      *                       WEMPORTE                                          
           10 SB02-WEMPORTE        PIC X(1).                                    
      *                       DTOPE                                             
           10 SB02-DTOPE           PIC X(8).                                    
      *                       DSYST                                             
           10 SB02-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CVENDEUR                                          
           10 SB02-CVENDEUR        PIC X(6).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *        
      ******************************************************************        
                                                                                
