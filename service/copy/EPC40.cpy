      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CC - LISTES DES REGLES                                          00000020
      ***************************************************************** 00000030
       01   EPC40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MTABI OCCURS   13 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREGLEL     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MNREGLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNREGLEF     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MNREGLEI     PIC X(10).                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MTYPEI  PIC X(3).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIGNEL     COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNLIGNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIGNEF     PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNLIGNEI     PIC X(2).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLIBELLEI    PIC X(25).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTVAL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MTVAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTVAF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MTVAI   PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINTERFL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MINTERFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MINTERFF     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MINTERFI     PIC X(5).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEOPL     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MTYPEOPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPEOPF     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MTYPEOPI     PIC X(5).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREGLE1L      COMP PIC S9(4).                            00000530
      *--                                                                       
           02 MNREGLE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNREGLE1F      PIC X.                                     00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MNREGLE1I      PIC X(10).                                 00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPE1L   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MTYPE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTYPE1F   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MTYPE1I   PIC X(3).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGNE1L      COMP PIC S9(4).                            00000610
      *--                                                                       
           02 MNLIGNE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIGNE1F      PIC X.                                     00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNLIGNE1I      PIC X(2).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPPRL   COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MSUPPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSUPPRF   PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MSUPPRI   PIC X.                                          00000680
      * ZONE CMD AIDA                                                   00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(79).                                      00000730
      * CODE TRANSACTION                                                00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      * CICS DE TRAVAIL                                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCICSI    PIC X(5).                                       00000830
      * NETNAME                                                         00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MNETNAMI  PIC X(8).                                       00000880
      * CODE TERMINAL                                                   00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * CC - LISTES DES REGLES                                          00000950
      ***************************************************************** 00000960
       01   EPC40O REDEFINES EPC40I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
      * DATE DU JOUR                                                    00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
      * HEURE                                                           00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MTIMJOUA  PIC X.                                          00001090
           02 MTIMJOUC  PIC X.                                          00001100
           02 MTIMJOUP  PIC X.                                          00001110
           02 MTIMJOUH  PIC X.                                          00001120
           02 MTIMJOUV  PIC X.                                          00001130
           02 MTIMJOUO  PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MPAGEA    PIC X.                                          00001160
           02 MPAGEC    PIC X.                                          00001170
           02 MPAGEP    PIC X.                                          00001180
           02 MPAGEH    PIC X.                                          00001190
           02 MPAGEV    PIC X.                                          00001200
           02 MPAGEO    PIC Z9.                                         00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNBPA     PIC X.                                          00001230
           02 MNBPC     PIC X.                                          00001240
           02 MNBPP     PIC X.                                          00001250
           02 MNBPH     PIC X.                                          00001260
           02 MNBPV     PIC X.                                          00001270
           02 MNBPO     PIC Z9.                                         00001280
           02 MTABO OCCURS   13 TIMES .                                 00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MNREGLEA     PIC X.                                     00001310
             03 MNREGLEC     PIC X.                                     00001320
             03 MNREGLEP     PIC X.                                     00001330
             03 MNREGLEH     PIC X.                                     00001340
             03 MNREGLEV     PIC X.                                     00001350
             03 MNREGLEO     PIC X(10).                                 00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MTYPEA  PIC X.                                          00001380
             03 MTYPEC  PIC X.                                          00001390
             03 MTYPEP  PIC X.                                          00001400
             03 MTYPEH  PIC X.                                          00001410
             03 MTYPEV  PIC X.                                          00001420
             03 MTYPEO  PIC X(3).                                       00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MNLIGNEA     PIC X.                                     00001450
             03 MNLIGNEC     PIC X.                                     00001460
             03 MNLIGNEP     PIC X.                                     00001470
             03 MNLIGNEH     PIC X.                                     00001480
             03 MNLIGNEV     PIC X.                                     00001490
             03 MNLIGNEO     PIC X(2).                                  00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MLIBELLEA    PIC X.                                     00001520
             03 MLIBELLEC    PIC X.                                     00001530
             03 MLIBELLEP    PIC X.                                     00001540
             03 MLIBELLEH    PIC X.                                     00001550
             03 MLIBELLEV    PIC X.                                     00001560
             03 MLIBELLEO    PIC X(25).                                 00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MTVAA   PIC X.                                          00001590
             03 MTVAC   PIC X.                                          00001600
             03 MTVAP   PIC X.                                          00001610
             03 MTVAH   PIC X.                                          00001620
             03 MTVAV   PIC X.                                          00001630
             03 MTVAO   PIC X(3).                                       00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MINTERFA     PIC X.                                     00001660
             03 MINTERFC     PIC X.                                     00001670
             03 MINTERFP     PIC X.                                     00001680
             03 MINTERFH     PIC X.                                     00001690
             03 MINTERFV     PIC X.                                     00001700
             03 MINTERFO     PIC X(5).                                  00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MTYPEOPA     PIC X.                                     00001730
             03 MTYPEOPC     PIC X.                                     00001740
             03 MTYPEOPP     PIC X.                                     00001750
             03 MTYPEOPH     PIC X.                                     00001760
             03 MTYPEOPV     PIC X.                                     00001770
             03 MTYPEOPO     PIC X(5).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNREGLE1A      PIC X.                                     00001800
           02 MNREGLE1C PIC X.                                          00001810
           02 MNREGLE1P PIC X.                                          00001820
           02 MNREGLE1H PIC X.                                          00001830
           02 MNREGLE1V PIC X.                                          00001840
           02 MNREGLE1O      PIC X(10).                                 00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MTYPE1A   PIC X.                                          00001870
           02 MTYPE1C   PIC X.                                          00001880
           02 MTYPE1P   PIC X.                                          00001890
           02 MTYPE1H   PIC X.                                          00001900
           02 MTYPE1V   PIC X.                                          00001910
           02 MTYPE1O   PIC X(3).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNLIGNE1A      PIC X.                                     00001940
           02 MNLIGNE1C PIC X.                                          00001950
           02 MNLIGNE1P PIC X.                                          00001960
           02 MNLIGNE1H PIC X.                                          00001970
           02 MNLIGNE1V PIC X.                                          00001980
           02 MNLIGNE1O      PIC X(2).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MSUPPRA   PIC X.                                          00002010
           02 MSUPPRC   PIC X.                                          00002020
           02 MSUPPRP   PIC X.                                          00002030
           02 MSUPPRH   PIC X.                                          00002040
           02 MSUPPRV   PIC X.                                          00002050
           02 MSUPPRO   PIC X.                                          00002060
      * ZONE CMD AIDA                                                   00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLIBERRA  PIC X.                                          00002090
           02 MLIBERRC  PIC X.                                          00002100
           02 MLIBERRP  PIC X.                                          00002110
           02 MLIBERRH  PIC X.                                          00002120
           02 MLIBERRV  PIC X.                                          00002130
           02 MLIBERRO  PIC X(79).                                      00002140
      * CODE TRANSACTION                                                00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
      * CICS DE TRAVAIL                                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCICSA    PIC X.                                          00002250
           02 MCICSC    PIC X.                                          00002260
           02 MCICSP    PIC X.                                          00002270
           02 MCICSH    PIC X.                                          00002280
           02 MCICSV    PIC X.                                          00002290
           02 MCICSO    PIC X(5).                                       00002300
      * NETNAME                                                         00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNETNAMA  PIC X.                                          00002330
           02 MNETNAMC  PIC X.                                          00002340
           02 MNETNAMP  PIC X.                                          00002350
           02 MNETNAMH  PIC X.                                          00002360
           02 MNETNAMV  PIC X.                                          00002370
           02 MNETNAMO  PIC X(8).                                       00002380
      * CODE TERMINAL                                                   00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MSCREENA  PIC X.                                          00002410
           02 MSCREENC  PIC X.                                          00002420
           02 MSCREENP  PIC X.                                          00002430
           02 MSCREENH  PIC X.                                          00002440
           02 MSCREENV  PIC X.                                          00002450
           02 MSCREENO  PIC X(4).                                       00002460
                                                                                
