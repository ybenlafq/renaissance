      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ------------------------------------------------------------- * 00010000
      * TSPC31                                                        * 00020000
      *                                                               * 00030000
      *                                                               * 00040000
      *                                                               * 00050000
      * ------------------------------------------------------------- * 00060000
       01 TS-PC31.                                                      00070011
          02 TS-PC31-MSITEM                 PIC S9(02) COMP-3 VALUE 20. 00070113
          02 TS-PC31-ITS                    PIC S9(02) COMP-3.          00071013
          02 TS-DATA-PC31 OCCURS 20.                                    00080008
          05 TS-PC31-ADRESSE.                                           00080110
           10 TS-PC31-CTITRENOM       PIC X(5).                         00081609
           10 TS-PC31-LNOM            PIC X(25).                        00081709
           10 TS-PC31-LPRENOM         PIC X(15).                        00081809
           10 TS-PC31-LBATIMENT       PIC X(3).                         00081909
           10 TS-PC31-LESCALIER       PIC X(3).                         00082009
           10 TS-PC31-LETAGE          PIC X(3).                         00082109
           10 TS-PC31-LPORTE          PIC X(3).                         00082209
           10 TS-PC31-LCMPAD1         PIC X(32).                        00082309
           10 TS-PC31-LCMPAD2         PIC X(32).                        00082409
           10 TS-PC31-CVOIE           PIC X(5).                         00082509
           10 TS-PC31-CTVOIE          PIC X(4).                         00082609
           10 TS-PC31-LNOMVOIE        PIC X(21).                        00082709
           10 TS-PC31-LCOMMUNE        PIC X(32).                        00082809
           10 TS-PC31-CPOSTAL         PIC X(5).                         00082909
           10 TS-PC31-LBUREAU         PIC X(26).                        00083009
          05 TS-DONNEES-PC31.                                           00086006
             10 TS-PC31-RANDOMNUM           PIC X(19).                  00090000
             10 TS-PC31-NSOCIETE            PIC X(03).                  00100000
             10 TS-PC31-NLIEU               PIC X(03).                  00110000
             10 TS-PC31-NVENTE              PIC X(07).                  00120000
             10 TS-PC31-CPRESTATION         PIC X(05).                  00130000
             10 TS-PC31-DACTIV              PIC X(08).                  00140000
             10 TS-PC31-CTITRENOM2          PIC X(05).                  00150012
             10 TS-PC31-LNOM2               PIC X(25).                  00160012
             10 TS-PC31-LPRENOM3            PIC X(15).                  00170012
             10 TS-PC31-STATUT              PIC X(01).                  00171014
             10 NB-PC31-SITEM               PIC 9(03).                  00180000
             10 TS-PC31-SDONNEES            OCCURS 10.                  00190000
                15 TS-PC31-NSOCIETER        PIC X(03).                  00200000
                15 TS-PC31-NLIEUR           PIC X(03).                  00210000
                15 TS-PC31-NVENTER          PIC X(07).                  00220000
                                                                                
