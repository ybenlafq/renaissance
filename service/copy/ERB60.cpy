      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV00   EAV00                                              00000020
      ***************************************************************** 00000030
       01   ERB60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * soc de modif                                                    00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCML    COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MSOCML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCMF    PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MSOCMI    PIC X(3).                                       00000180
      * lieu                                                            00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUML   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLIEUML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUMF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIEUMI   PIC X(3).                                       00000230
      * libelle lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUML  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MLLIEUML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUMF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUMI  PIC X(20).                                      00000280
      * soc de vente                                                    00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCVL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSOCVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCVF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSOCVI    PIC X(3).                                       00000330
      * lieu                                                            00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUVL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLIEUVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUVF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLIEUVI   PIC X(3).                                       00000380
      * vente                                                           00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVENTEVL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MVENTEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVENTEVF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MVENTEVI  PIC X(7).                                       00000430
      * contrat Dartybox                                                00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONTRAL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MCONTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCONTRAF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCONTRAI  PIC X(18).                                      00000480
      * zonede commande       oirs                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MZONCMDI  PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(78).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: EAV00   EAV00                                              00000750
      ***************************************************************** 00000760
       01   ERB60O REDEFINES ERB60I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
      * soc de modif                                                    00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MSOCMA    PIC X.                                          00000950
           02 MSOCMC    PIC X.                                          00000960
           02 MSOCMP    PIC X.                                          00000970
           02 MSOCMH    PIC X.                                          00000980
           02 MSOCMV    PIC X.                                          00000990
           02 MSOCMO    PIC X(3).                                       00001000
      * lieu                                                            00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MLIEUMA   PIC X.                                          00001030
           02 MLIEUMC   PIC X.                                          00001040
           02 MLIEUMP   PIC X.                                          00001050
           02 MLIEUMH   PIC X.                                          00001060
           02 MLIEUMV   PIC X.                                          00001070
           02 MLIEUMO   PIC X(3).                                       00001080
      * libelle lieu                                                    00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MLLIEUMA  PIC X.                                          00001110
           02 MLLIEUMC  PIC X.                                          00001120
           02 MLLIEUMP  PIC X.                                          00001130
           02 MLLIEUMH  PIC X.                                          00001140
           02 MLLIEUMV  PIC X.                                          00001150
           02 MLLIEUMO  PIC X(20).                                      00001160
      * soc de vente                                                    00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MSOCVA    PIC X.                                          00001190
           02 MSOCVC    PIC X.                                          00001200
           02 MSOCVP    PIC X.                                          00001210
           02 MSOCVH    PIC X.                                          00001220
           02 MSOCVV    PIC X.                                          00001230
           02 MSOCVO    PIC X(3).                                       00001240
      * lieu                                                            00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MLIEUVA   PIC X.                                          00001270
           02 MLIEUVC   PIC X.                                          00001280
           02 MLIEUVP   PIC X.                                          00001290
           02 MLIEUVH   PIC X.                                          00001300
           02 MLIEUVV   PIC X.                                          00001310
           02 MLIEUVO   PIC X(3).                                       00001320
      * vente                                                           00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MVENTEVA  PIC X.                                          00001350
           02 MVENTEVC  PIC X.                                          00001360
           02 MVENTEVP  PIC X.                                          00001370
           02 MVENTEVH  PIC X.                                          00001380
           02 MVENTEVV  PIC X.                                          00001390
           02 MVENTEVO  PIC X(7).                                       00001400
      * contrat Dartybox                                                00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCONTRAA  PIC X.                                          00001430
           02 MCONTRAC  PIC X.                                          00001440
           02 MCONTRAP  PIC X.                                          00001450
           02 MCONTRAH  PIC X.                                          00001460
           02 MCONTRAV  PIC X.                                          00001470
           02 MCONTRAO  PIC X(18).                                      00001480
      * zonede commande       oirs                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MZONCMDA  PIC X.                                          00001510
           02 MZONCMDC  PIC X.                                          00001520
           02 MZONCMDP  PIC X.                                          00001530
           02 MZONCMDH  PIC X.                                          00001540
           02 MZONCMDV  PIC X.                                          00001550
           02 MZONCMDO  PIC X(10).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLIBERRA  PIC X.                                          00001580
           02 MLIBERRC  PIC X.                                          00001590
           02 MLIBERRP  PIC X.                                          00001600
           02 MLIBERRH  PIC X.                                          00001610
           02 MLIBERRV  PIC X.                                          00001620
           02 MLIBERRO  PIC X(78).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCODTRAA  PIC X.                                          00001650
           02 MCODTRAC  PIC X.                                          00001660
           02 MCODTRAP  PIC X.                                          00001670
           02 MCODTRAH  PIC X.                                          00001680
           02 MCODTRAV  PIC X.                                          00001690
           02 MCODTRAO  PIC X(4).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCICSA    PIC X.                                          00001720
           02 MCICSC    PIC X.                                          00001730
           02 MCICSP    PIC X.                                          00001740
           02 MCICSH    PIC X.                                          00001750
           02 MCICSV    PIC X.                                          00001760
           02 MCICSO    PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNETNAMA  PIC X.                                          00001790
           02 MNETNAMC  PIC X.                                          00001800
           02 MNETNAMP  PIC X.                                          00001810
           02 MNETNAMH  PIC X.                                          00001820
           02 MNETNAMV  PIC X.                                          00001830
           02 MNETNAMO  PIC X(8).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MSCREENA  PIC X.                                          00001860
           02 MSCREENC  PIC X.                                          00001870
           02 MSCREENP  PIC X.                                          00001880
           02 MSCREENH  PIC X.                                          00001890
           02 MSCREENV  PIC X.                                          00001900
           02 MSCREENO  PIC X(4).                                       00001910
                                                                                
