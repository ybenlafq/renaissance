      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: consult recepts attendues                                  00000020
      ***************************************************************** 00000030
       01   ERB21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
           02 MTABLGRPI OCCURS   14 TIMES .                             00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MSELI   PIC X.                                          00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLL   COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MNBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBLF   PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNBLI   PIC X(10).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFOURNISSEURL    COMP PIC S9(4).                       00000310
      *--                                                                       
             03 MNFOURNISSEURL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MNFOURNISSEURF    PIC X.                                00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNFOURNISSEURI    PIC X(17).                            00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTREPOTL  COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNENTREPOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNENTREPOTF  PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNENTREPOTI  PIC X(6).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCOMMANDEL  COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCOMMANDEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNCOMMANDEF  PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCOMMANDEI  PIC X(9).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOMMANDEL  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDCOMMANDEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDCOMMANDEF  PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDCOMMANDEI  PIC X(10).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPALETTEL  COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNBPALETTEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNBPALETTEF  PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNBPALETTEI  PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPRODUITL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNBPRODUITL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNBPRODUITF  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNBPRODUITI  PIC X(6).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * Rbx: consult recepts attendues                                  00000760
      ***************************************************************** 00000770
       01   ERB21O REDEFINES ERB21I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 NPAGEA    PIC X.                                          00000950
           02 NPAGEC    PIC X.                                          00000960
           02 NPAGEP    PIC X.                                          00000970
           02 NPAGEH    PIC X.                                          00000980
           02 NPAGEV    PIC X.                                          00000990
           02 NPAGEO    PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 NBPAGEA   PIC X.                                          00001020
           02 NBPAGEC   PIC X.                                          00001030
           02 NBPAGEP   PIC X.                                          00001040
           02 NBPAGEH   PIC X.                                          00001050
           02 NBPAGEV   PIC X.                                          00001060
           02 NBPAGEO   PIC X(3).                                       00001070
           02 MTABLGRPO OCCURS   14 TIMES .                             00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MSELA   PIC X.                                          00001100
             03 MSELC   PIC X.                                          00001110
             03 MSELP   PIC X.                                          00001120
             03 MSELH   PIC X.                                          00001130
             03 MSELV   PIC X.                                          00001140
             03 MSELO   PIC X.                                          00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MNBLA   PIC X.                                          00001170
             03 MNBLC   PIC X.                                          00001180
             03 MNBLP   PIC X.                                          00001190
             03 MNBLH   PIC X.                                          00001200
             03 MNBLV   PIC X.                                          00001210
             03 MNBLO   PIC X(10).                                      00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MNFOURNISSEURA    PIC X.                                00001240
             03 MNFOURNISSEURC    PIC X.                                00001250
             03 MNFOURNISSEURP    PIC X.                                00001260
             03 MNFOURNISSEURH    PIC X.                                00001270
             03 MNFOURNISSEURV    PIC X.                                00001280
             03 MNFOURNISSEURO    PIC X(17).                            00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MNENTREPOTA  PIC X.                                     00001310
             03 MNENTREPOTC  PIC X.                                     00001320
             03 MNENTREPOTP  PIC X.                                     00001330
             03 MNENTREPOTH  PIC X.                                     00001340
             03 MNENTREPOTV  PIC X.                                     00001350
             03 MNENTREPOTO  PIC X(6).                                  00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MNCOMMANDEA  PIC X.                                     00001380
             03 MNCOMMANDEC  PIC X.                                     00001390
             03 MNCOMMANDEP  PIC X.                                     00001400
             03 MNCOMMANDEH  PIC X.                                     00001410
             03 MNCOMMANDEV  PIC X.                                     00001420
             03 MNCOMMANDEO  PIC X(9).                                  00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MDCOMMANDEA  PIC X.                                     00001450
             03 MDCOMMANDEC  PIC X.                                     00001460
             03 MDCOMMANDEP  PIC X.                                     00001470
             03 MDCOMMANDEH  PIC X.                                     00001480
             03 MDCOMMANDEV  PIC X.                                     00001490
             03 MDCOMMANDEO  PIC X(10).                                 00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MNBPALETTEA  PIC X.                                     00001520
             03 MNBPALETTEC  PIC X.                                     00001530
             03 MNBPALETTEP  PIC X.                                     00001540
             03 MNBPALETTEH  PIC X.                                     00001550
             03 MNBPALETTEV  PIC X.                                     00001560
             03 MNBPALETTEO  PIC X(3).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MNBPRODUITA  PIC X.                                     00001590
             03 MNBPRODUITC  PIC X.                                     00001600
             03 MNBPRODUITP  PIC X.                                     00001610
             03 MNBPRODUITH  PIC X.                                     00001620
             03 MNBPRODUITV  PIC X.                                     00001630
             03 MNBPRODUITO  PIC X(6).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(78).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
