      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF:redbox ctrl livraison                                       00000020
      ***************************************************************** 00000030
       01   ERB11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNBLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBLF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNBLI     PIC X(10).                                      00000170
      * nb lots controlÚs                                               00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTL   COMP PIC S9(4).                                 00000190
      *--                                                                       
           02 MNBLOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBLOTF   PIC X.                                          00000200
           02 FILLER    PIC X(4).                                       00000210
           02 MNBLOTI   PIC X(5).                                       00000220
      * nb lots present fch                                             00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLCNTRL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNBLCNTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBLCNTRF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNBLCNTRI      PIC X(5).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLIBERRI  PIC X(19).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCICSI    PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNETNAMI  PIC X(8).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MSCREENI  PIC X(4).                                       00000430
      ***************************************************************** 00000440
      * SDF:redbox ctrl livraison                                       00000450
      ***************************************************************** 00000460
       01   ERB11O REDEFINES ERB11I.                                    00000470
           02 FILLER    PIC X(12).                                      00000480
           02 FILLER    PIC X(2).                                       00000490
           02 MDATJOUA  PIC X.                                          00000500
           02 MDATJOUC  PIC X.                                          00000510
           02 MDATJOUP  PIC X.                                          00000520
           02 MDATJOUH  PIC X.                                          00000530
           02 MDATJOUV  PIC X.                                          00000540
           02 MDATJOUO  PIC X(10).                                      00000550
           02 FILLER    PIC X(2).                                       00000560
           02 MTIMJOUA  PIC X.                                          00000570
           02 MTIMJOUC  PIC X.                                          00000580
           02 MTIMJOUP  PIC X.                                          00000590
           02 MTIMJOUH  PIC X.                                          00000600
           02 MTIMJOUV  PIC X.                                          00000610
           02 MTIMJOUO  PIC X(5).                                       00000620
           02 FILLER    PIC X(2).                                       00000630
           02 MNBLA     PIC X.                                          00000640
           02 MNBLC     PIC X.                                          00000650
           02 MNBLP     PIC X.                                          00000660
           02 MNBLH     PIC X.                                          00000670
           02 MNBLV     PIC X.                                          00000680
           02 MNBLO     PIC X(10).                                      00000690
      * nb lots controlÚs                                               00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MNBLOTA   PIC X.                                          00000720
           02 MNBLOTC   PIC X.                                          00000730
           02 MNBLOTP   PIC X.                                          00000740
           02 MNBLOTH   PIC X.                                          00000750
           02 MNBLOTV   PIC X.                                          00000760
           02 MNBLOTO   PIC X(5).                                       00000770
      * nb lots present fch                                             00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MNBLCNTRA      PIC X.                                     00000800
           02 MNBLCNTRC PIC X.                                          00000810
           02 MNBLCNTRP PIC X.                                          00000820
           02 MNBLCNTRH PIC X.                                          00000830
           02 MNBLCNTRV PIC X.                                          00000840
           02 MNBLCNTRO      PIC X(5).                                  00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MLIBERRA  PIC X.                                          00000870
           02 MLIBERRC  PIC X.                                          00000880
           02 MLIBERRP  PIC X.                                          00000890
           02 MLIBERRH  PIC X.                                          00000900
           02 MLIBERRV  PIC X.                                          00000910
           02 MLIBERRO  PIC X(19).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MCICSA    PIC X.                                          00000940
           02 MCICSC    PIC X.                                          00000950
           02 MCICSP    PIC X.                                          00000960
           02 MCICSH    PIC X.                                          00000970
           02 MCICSV    PIC X.                                          00000980
           02 MCICSO    PIC X(5).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNETNAMA  PIC X.                                          00001010
           02 MNETNAMC  PIC X.                                          00001020
           02 MNETNAMP  PIC X.                                          00001030
           02 MNETNAMH  PIC X.                                          00001040
           02 MNETNAMV  PIC X.                                          00001050
           02 MNETNAMO  PIC X(8).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MSCREENA  PIC X.                                          00001080
           02 MSCREENC  PIC X.                                          00001090
           02 MSCREENP  PIC X.                                          00001100
           02 MSCREENH  PIC X.                                          00001110
           02 MSCREENV  PIC X.                                          00001120
           02 MSCREENO  PIC X(4).                                       00001130
                                                                                
