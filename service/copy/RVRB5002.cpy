      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RVRB5002)                                  *        
      *        LIBRARY(DSA016.DEVL.RVRB5002.COBOL)                     *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(RB50-)                                            *        
      *        STRUCTURE(RVRB5002)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVRB5002 TABLE                               
           ( NTYPEFAS                       CHAR(4) NOT NULL,                   
             LIBFAS                         CHAR(30) NOT NULL,                  
             DCREATION                      CHAR(8) NOT NULL,                   
             DMAJ                           CHAR(8) NOT NULL,                   
             LIBOPE                         CHAR(15) NOT NULL,                  
             DEFFET                         CHAR(8) NOT NULL,                   
             WRESIL                         CHAR(1) NOT NULL,                   
             MONTANT                        DECIMAL(5, 2) NOT NULL,             
             CTVA                           CHAR(1) NOT NULL,                   
             TYPCOMPTA                      CHAR(1) NOT NULL,                   
             CPTIMMO                        CHAR(6) NOT NULL,                   
             WMODAMORT                      CHAR(1) NOT NULL,                   
             DURECO                         DECIMAL(4, 2) NOT NULL,             
             DURFISC                        DECIMAL(4, 2) NOT NULL,             
             WCDETRTABEL                    CHAR(1) NOT NULL,                   
             RUBRIQUE                       CHAR(6) NOT NULL,                   
             SECTION                        CHAR(6) NOT NULL,                   
             TYPINTER                       CHAR(5) NOT NULL,                   
             TYPCTR                         CHAR(3) NOT NULL                    
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVRB5002                    *        
      ******************************************************************        
       01  RVRB5002.                                                            
      *                       NTYPEFAS                                          
           10 RB50-NTYPEFAS        PIC X(4).                                    
      *                       LIBFAS                                            
           10 RB50-LIBFAS          PIC X(30).                                   
      *                       DCREATION                                         
           10 RB50-DCREATION       PIC X(8).                                    
      *                       DMAJ                                              
           10 RB50-DMAJ            PIC X(8).                                    
      *                       LIBOPE                                            
           10 RB50-LIBOPE          PIC X(15).                                   
      *                       DEFFET                                            
           10 RB50-DEFFET          PIC X(8).                                    
      *                       WRESIL                                            
           10 RB50-WRESIL          PIC X(1).                                    
      *                       MONTANT                                           
           10 RB50-MONTANT         PIC S9(3)V9(2) USAGE COMP-3.                 
      *                       CTVA                                              
           10 RB50-CTVA            PIC X(1).                                    
      *                       TYPCOMPTA                                         
           10 RB50-TYPCOMPTA       PIC X(1).                                    
      *                       CPTIMMO                                           
           10 RB50-CPTIMMO         PIC X(6).                                    
      *                       WMODAMORT                                         
           10 RB50-WMODAMORT       PIC X(1).                                    
      *                       DURECO                                            
           10 RB50-DURECO          PIC S9(2)V9(2) USAGE COMP-3.                 
      *                       DURFISC                                           
           10 RB50-DURFISC         PIC S9(2)V9(2) USAGE COMP-3.                 
      *                       WCDETRTABEL                                       
           10 RB50-WCDETRTABEL     PIC X(1).                                    
      *                       RUBRIQUE                                          
           10 RB50-RUBRIQUE        PIC X(6).                                    
      *                       SECTION                                           
           10 RB50-SECTION         PIC X(6).                                    
      *                       TYPINTER                                          
           10 RB50-TYPINTER        PIC X(5).                                    
      *                       TYPCTR                                            
           10 RB50-TYPCTR          PIC X(3).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
                                                                                
