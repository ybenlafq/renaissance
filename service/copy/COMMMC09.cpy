      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MMC00                                                 
      * APPELLE PAR LINK DANS TMC00                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MMC00.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
              10 COMM-MMC00-AAMMJJ       PIC X(06).                     01560   
              10 COMM-MMC00-JJMMSSAA     PIC X(08).                     01560   
              10 COMM-MMC00-HEURE        PIC 9(08).                     01560   
              10 COMM-MMC00-TYPE-EDIT    PIC X.                         01560   
                 88 COMM-MMC00-NOUV         VALUE '1'.                     01570
                 88 COMM-MMC00-EPF          VALUE '2'.                     01570
                 88 COMM-MMC00-NOUV-EPF     VALUE '4'.                     01570
                 88 COMM-MMC00-TOUT         VALUE '3'.                     01570
              10 COMM-MMC00-DNOUVEAU     PIC X(8).                      01570   
              10 COMM-MMC00-DEPUISE      PIC X(8).                      01570   
              10 COMM-MMC00-DTOUT-EPF    PIC X(8).                      01570   
              10 COMM-MMC00-PRODUIT .                                   01570   
                 15 COMM-MMC00-DARTY        PIC X.                         01570
                 15 COMM-MMC00-DACEM        PIC X.                         01570
              10 COMM-MMC00-WDACEM REDEFINES COMM-MMC00-PRODUIT .       01570   
                 15 COMM-WDACEM   OCCURS 2  PIC X.                         01570
              10 COMM-MMC00-RAYON .                                     01570   
                 15 COMM-MMC00-TLMBR        PIC X(20).                     01570
                 15 COMM-MMC00-TLMBL        PIC X(20).                     01570
                 15 COMM-MMC00-ELABR        PIC X(20).                     01570
                 15 COMM-MMC00-ELABL        PIC X(20).                     01570
              10 COMM-MMC00-LVPARAM  REDEFINES COMM-MMC00-RAYON .       01570   
                 15 COMM-LVPARAM   OCCURS 4 PIC X(20).                     01570
      *      MESSAGE ERREUR                                                     
         05 COMM-MMC00-RETOUR.                                                  
             10 COMM-MMC00-CODRET        PIC X(01).                             
                  88  COMM-MMC00-OK     VALUE ' '.                              
                  88  COMM-MMC00-ERREUR VALUE '1'.                              
             10 COMM-MMC00-LIBERR        PIC X(60).                             
         05 FILLER                       PIC X(30).                             
                                                                                
