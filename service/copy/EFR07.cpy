      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Franchise - Admin r�gles IFR100                                 00000020
      ***************************************************************** 00000030
       01   EFR07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXTRL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCEXTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXTRF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCEXTRI   PIC X(5).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEXTRL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLIBEXTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBEXTRF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLIBEXTRI      PIC X(20).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCOPCOI   PIC X(6).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBOPCOL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLIBOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBOPCOF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLIBOPCOI      PIC X(20).                                 00000390
           02 MTABI OCCURS   9 TIMES .                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROGL      COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MCPROGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPROGF      PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MCPROGI      PIC X(5).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCORIL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MSOCORIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCORIF     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MSOCORII     PIC X(3).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUORIL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MLIEUORIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUORIF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MLIEUORII    PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCDSTL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MSOCDSTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCDSTF     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MSOCDSTI     PIC X(3).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUDSTL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MLIEUDSTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUDSTF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MLIEUDSTI    PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCVTEL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MSOCVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCVTEF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MSOCVTEI     PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUVTEL    COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MLIEUVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUVTEF    PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MLIEUVTEI    PIC X(3).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINTITL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MINTITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MINTITF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MINTITI      PIC X(30).                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCPROGL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCCPROGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCPROGF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCCPROGI  PIC X(5).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCORIL      COMP PIC S9(4).                            00000770
      *--                                                                       
           02 MCSOCORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCORIF      PIC X.                                     00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCSOCORII      PIC X(3).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUORIL     COMP PIC S9(4).                            00000810
      *--                                                                       
           02 MCLIEUORIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUORIF     PIC X.                                     00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MCLIEUORII     PIC X(3).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCDSTL      COMP PIC S9(4).                            00000850
      *--                                                                       
           02 MCSOCDSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCDSTF      PIC X.                                     00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCSOCDSTI      PIC X(3).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUDSTL     COMP PIC S9(4).                            00000890
      *--                                                                       
           02 MCLIEUDSTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUDSTF     PIC X.                                     00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCLIEUDSTI     PIC X(3).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCVTEL      COMP PIC S9(4).                            00000930
      *--                                                                       
           02 MCSOCVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCVTEF      PIC X.                                     00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MCSOCVTEI      PIC X(3).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUVTEL     COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MCLIEUVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUVTEF     PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCLIEUVTEI     PIC X(3).                                  00001000
      * ZONE CMD AIDA                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(79).                                      00001050
      * CODE TRANSACTION                                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      * CICS DE TRAVAIL                                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCICSI    PIC X(5).                                       00001150
      * NETNAME                                                         00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MNETNAMI  PIC X(8).                                       00001200
      * CODE TERMINAL                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(4).                                       00001250
      ***************************************************************** 00001260
      * Franchise - Admin r�gles IFR100                                 00001270
      ***************************************************************** 00001280
       01   EFR07O REDEFINES EFR07I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
      * DATE DU JOUR                                                    00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
      * HEURE                                                           00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MTIMJOUA  PIC X.                                          00001410
           02 MTIMJOUC  PIC X.                                          00001420
           02 MTIMJOUP  PIC X.                                          00001430
           02 MTIMJOUH  PIC X.                                          00001440
           02 MTIMJOUV  PIC X.                                          00001450
           02 MTIMJOUO  PIC X(5).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MPAGEA    PIC X.                                          00001480
           02 MPAGEC    PIC X.                                          00001490
           02 MPAGEP    PIC X.                                          00001500
           02 MPAGEH    PIC X.                                          00001510
           02 MPAGEV    PIC X.                                          00001520
           02 MPAGEO    PIC Z9.                                         00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNBPA     PIC X.                                          00001550
           02 MNBPC     PIC X.                                          00001560
           02 MNBPP     PIC X.                                          00001570
           02 MNBPH     PIC X.                                          00001580
           02 MNBPV     PIC X.                                          00001590
           02 MNBPO     PIC Z9.                                         00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCEXTRA   PIC X.                                          00001620
           02 MCEXTRC   PIC X.                                          00001630
           02 MCEXTRP   PIC X.                                          00001640
           02 MCEXTRH   PIC X.                                          00001650
           02 MCEXTRV   PIC X.                                          00001660
           02 MCEXTRO   PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLIBEXTRA      PIC X.                                     00001690
           02 MLIBEXTRC PIC X.                                          00001700
           02 MLIBEXTRP PIC X.                                          00001710
           02 MLIBEXTRH PIC X.                                          00001720
           02 MLIBEXTRV PIC X.                                          00001730
           02 MLIBEXTRO      PIC X(20).                                 00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCOPCOA   PIC X.                                          00001760
           02 MCOPCOC   PIC X.                                          00001770
           02 MCOPCOP   PIC X.                                          00001780
           02 MCOPCOH   PIC X.                                          00001790
           02 MCOPCOV   PIC X.                                          00001800
           02 MCOPCOO   PIC X(6).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLIBOPCOA      PIC X.                                     00001830
           02 MLIBOPCOC PIC X.                                          00001840
           02 MLIBOPCOP PIC X.                                          00001850
           02 MLIBOPCOH PIC X.                                          00001860
           02 MLIBOPCOV PIC X.                                          00001870
           02 MLIBOPCOO      PIC X(20).                                 00001880
           02 MTABO OCCURS   9 TIMES .                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MCPROGA      PIC X.                                     00001910
             03 MCPROGC PIC X.                                          00001920
             03 MCPROGP PIC X.                                          00001930
             03 MCPROGH PIC X.                                          00001940
             03 MCPROGV PIC X.                                          00001950
             03 MCPROGO      PIC X(5).                                  00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MSOCORIA     PIC X.                                     00001980
             03 MSOCORIC     PIC X.                                     00001990
             03 MSOCORIP     PIC X.                                     00002000
             03 MSOCORIH     PIC X.                                     00002010
             03 MSOCORIV     PIC X.                                     00002020
             03 MSOCORIO     PIC X(3).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MLIEUORIA    PIC X.                                     00002050
             03 MLIEUORIC    PIC X.                                     00002060
             03 MLIEUORIP    PIC X.                                     00002070
             03 MLIEUORIH    PIC X.                                     00002080
             03 MLIEUORIV    PIC X.                                     00002090
             03 MLIEUORIO    PIC X(3).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MSOCDSTA     PIC X.                                     00002120
             03 MSOCDSTC     PIC X.                                     00002130
             03 MSOCDSTP     PIC X.                                     00002140
             03 MSOCDSTH     PIC X.                                     00002150
             03 MSOCDSTV     PIC X.                                     00002160
             03 MSOCDSTO     PIC X(3).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MLIEUDSTA    PIC X.                                     00002190
             03 MLIEUDSTC    PIC X.                                     00002200
             03 MLIEUDSTP    PIC X.                                     00002210
             03 MLIEUDSTH    PIC X.                                     00002220
             03 MLIEUDSTV    PIC X.                                     00002230
             03 MLIEUDSTO    PIC X(3).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MSOCVTEA     PIC X.                                     00002260
             03 MSOCVTEC     PIC X.                                     00002270
             03 MSOCVTEP     PIC X.                                     00002280
             03 MSOCVTEH     PIC X.                                     00002290
             03 MSOCVTEV     PIC X.                                     00002300
             03 MSOCVTEO     PIC X(3).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MLIEUVTEA    PIC X.                                     00002330
             03 MLIEUVTEC    PIC X.                                     00002340
             03 MLIEUVTEP    PIC X.                                     00002350
             03 MLIEUVTEH    PIC X.                                     00002360
             03 MLIEUVTEV    PIC X.                                     00002370
             03 MLIEUVTEO    PIC X(3).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MINTITA      PIC X.                                     00002400
             03 MINTITC PIC X.                                          00002410
             03 MINTITP PIC X.                                          00002420
             03 MINTITH PIC X.                                          00002430
             03 MINTITV PIC X.                                          00002440
             03 MINTITO      PIC X(30).                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCCPROGA  PIC X.                                          00002470
           02 MCCPROGC  PIC X.                                          00002480
           02 MCCPROGP  PIC X.                                          00002490
           02 MCCPROGH  PIC X.                                          00002500
           02 MCCPROGV  PIC X.                                          00002510
           02 MCCPROGO  PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCSOCORIA      PIC X.                                     00002540
           02 MCSOCORIC PIC X.                                          00002550
           02 MCSOCORIP PIC X.                                          00002560
           02 MCSOCORIH PIC X.                                          00002570
           02 MCSOCORIV PIC X.                                          00002580
           02 MCSOCORIO      PIC X(3).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCLIEUORIA     PIC X.                                     00002610
           02 MCLIEUORIC     PIC X.                                     00002620
           02 MCLIEUORIP     PIC X.                                     00002630
           02 MCLIEUORIH     PIC X.                                     00002640
           02 MCLIEUORIV     PIC X.                                     00002650
           02 MCLIEUORIO     PIC X(3).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCSOCDSTA      PIC X.                                     00002680
           02 MCSOCDSTC PIC X.                                          00002690
           02 MCSOCDSTP PIC X.                                          00002700
           02 MCSOCDSTH PIC X.                                          00002710
           02 MCSOCDSTV PIC X.                                          00002720
           02 MCSOCDSTO      PIC X(3).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCLIEUDSTA     PIC X.                                     00002750
           02 MCLIEUDSTC     PIC X.                                     00002760
           02 MCLIEUDSTP     PIC X.                                     00002770
           02 MCLIEUDSTH     PIC X.                                     00002780
           02 MCLIEUDSTV     PIC X.                                     00002790
           02 MCLIEUDSTO     PIC X(3).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCSOCVTEA      PIC X.                                     00002820
           02 MCSOCVTEC PIC X.                                          00002830
           02 MCSOCVTEP PIC X.                                          00002840
           02 MCSOCVTEH PIC X.                                          00002850
           02 MCSOCVTEV PIC X.                                          00002860
           02 MCSOCVTEO      PIC X(3).                                  00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MCLIEUVTEA     PIC X.                                     00002890
           02 MCLIEUVTEC     PIC X.                                     00002900
           02 MCLIEUVTEP     PIC X.                                     00002910
           02 MCLIEUVTEH     PIC X.                                     00002920
           02 MCLIEUVTEV     PIC X.                                     00002930
           02 MCLIEUVTEO     PIC X(3).                                  00002940
      * ZONE CMD AIDA                                                   00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLIBERRA  PIC X.                                          00002970
           02 MLIBERRC  PIC X.                                          00002980
           02 MLIBERRP  PIC X.                                          00002990
           02 MLIBERRH  PIC X.                                          00003000
           02 MLIBERRV  PIC X.                                          00003010
           02 MLIBERRO  PIC X(79).                                      00003020
      * CODE TRANSACTION                                                00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
      * CICS DE TRAVAIL                                                 00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MCICSA    PIC X.                                          00003130
           02 MCICSC    PIC X.                                          00003140
           02 MCICSP    PIC X.                                          00003150
           02 MCICSH    PIC X.                                          00003160
           02 MCICSV    PIC X.                                          00003170
           02 MCICSO    PIC X(5).                                       00003180
      * NETNAME                                                         00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MNETNAMA  PIC X.                                          00003210
           02 MNETNAMC  PIC X.                                          00003220
           02 MNETNAMP  PIC X.                                          00003230
           02 MNETNAMH  PIC X.                                          00003240
           02 MNETNAMV  PIC X.                                          00003250
           02 MNETNAMO  PIC X(8).                                       00003260
      * CODE TERMINAL                                                   00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MSCREENA  PIC X.                                          00003290
           02 MSCREENC  PIC X.                                          00003300
           02 MSCREENP  PIC X.                                          00003310
           02 MSCREENH  PIC X.                                          00003320
           02 MSCREENV  PIC X.                                          00003330
           02 MSCREENO  PIC X(4).                                       00003340
                                                                                
