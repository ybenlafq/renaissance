      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHC0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHC0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHC0100.                                                            
           02  HC01-NLIEUHC                                                     
               PIC X(0003).                                                     
           02  HC01-NCHS                                                        
               PIC X(0007).                                                     
           02  HC01-DDEMANDE                                                    
               PIC X(0008).                                                     
           02  HC01-DCDE                                                        
               PIC X(0008).                                                     
           02  HC01-CTYPE                                                       
               PIC X(0003).                                                     
           02  HC01-NCDE1                                                       
               PIC X(0006).                                                     
           02  HC01-NCDE2                                                       
               PIC X(0006).                                                     
           02  HC01-NCDE3                                                       
               PIC X(0006).                                                     
           02  HC01-DREL1                                                       
               PIC X(0008).                                                     
           02  HC01-DREL2                                                       
               PIC X(0008).                                                     
           02  HC01-DREC                                                        
               PIC X(0008).                                                     
           02  HC01-CTEC1                                                       
               PIC X(0003).                                                     
           02  HC01-CTEC2                                                       
               PIC X(0003).                                                     
           02  HC01-DDEMAVOIR                                                   
               PIC X(0008).                                                     
           02  HC01-NDEMAVOIR                                                   
               PIC X(0008).                                                     
           02  HC01-DRECAVOIR                                                   
               PIC X(0008).                                                     
           02  HC01-CSTATUT                                                     
               PIC X(0001).                                                     
           02  HC01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHC0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHC0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NLIEUHC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NLIEUHC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NCHS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NCHS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-CTYPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-CTYPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NCDE1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NCDE1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NCDE2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NCDE2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NCDE3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NCDE3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DREL1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DREL1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DREL2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DREL2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-CTEC1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-CTEC1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-CTEC2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-CTEC2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DDEMAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DDEMAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-NDEMAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-NDEMAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DRECAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DRECAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
