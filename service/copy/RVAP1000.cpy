      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1000                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1000.                                                            
           02  AP10-NSEQID         PIC S9(18)  USAGE COMP-3.                    
           02  AP10-DCREATION      PIC X(0008).                                 
           02  AP10-NVENTE         PIC X(0014).                                 
           02  AP10-NLIGNE         PIC X(0003).                                 
           02  AP10-DLGVTE         PIC X(0008).                                 
           02  AP10-CPRESTA        PIC X(0005).                                 
           02  AP10-PPRESTA        PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP10-NSEQREF        PIC S9(5)   USAGE COMP-3.                    
           02  AP10-NENCAISSE      PIC X(0014).                                 
           02  AP10-NLIEU          PIC X(0003).                                 
           02  AP10-DENCAISSE      PIC X(0008).                                 
           02  AP10-NCLIENT        PIC X(0008).                                 
           02  AP10-NSOCA2I        PIC X(0003).                                 
           02  AP10-DATETRAIT      PIC X(0008).                                 
           02  AP10-NATTEST        PIC S9(18)  USAGE COMP-3.                    
           02  AP10-CODEANO        PIC X(0002).                                 
           02  AP10-NIDANO         PIC S9(18)  USAGE COMP-3.                    
           02  AP10-DATEANO        PIC X(0008).                                 
           02  AP10-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DCREATION-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-DCREATION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NLIGNE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NLIGNE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DLGVTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-DLGVTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-CPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-CPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-PPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-PPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NSEQREF-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NSEQREF-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-DENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NCLIENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NCLIENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NSOCA2I-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NSOCA2I-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DATETRAIT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-DATETRAIT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NATTEST-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NATTEST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-CODEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-CODEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-NIDANO-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-NIDANO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DATEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP10-DATEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP10-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP10-DSYST-F        PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
