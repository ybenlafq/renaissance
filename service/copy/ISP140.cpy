      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP140 AU 10/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,PD,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,07,BI,A,                          *        
      *                           47,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP140.                                                        
            05 NOMETAT-ISP140           PIC X(6) VALUE 'ISP140'.                
            05 RUPTURES-ISP140.                                                 
           10 ISP140-COULEUR            PIC X(05).                      007  005
           10 ISP140-WSEQFAM            PIC S9(05)      COMP-3.         012  003
           10 ISP140-CMARQ              PIC X(05).                      015  005
           10 ISP140-LREFFOURN          PIC X(20).                      020  020
           10 ISP140-NCODIC             PIC X(07).                      040  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP140-SEQUENCE           PIC S9(04) COMP.                047  002
      *--                                                                       
           10 ISP140-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP140.                                                   
           10 ISP140-CFAM               PIC X(05).                      049  005
           10 ISP140-DTRAIT             PIC X(10).                      054  010
           10 ISP140-NLIEU              PIC X(03).                      064  003
           10 ISP140-NSOCIETE           PIC X(03).                      067  003
           10 ISP140-NZONPRIX           PIC X(03).                      070  003
           10 ISP140-DEFFET             PIC X(08).                      073  008
            05 FILLER                      PIC X(432).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP140-LONG           PIC S9(4)   COMP  VALUE +080.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP140-LONG           PIC S9(4) COMP-5  VALUE +080.           
                                                                                
      *}                                                                        
