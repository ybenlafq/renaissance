      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPC0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPC0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0100.                                                            
           02  PC01-RANDOMNUM                                                   
               PIC X(0019).                                                     
           02  PC01-SERIALNUM                                                   
               PIC X(0013).                                                     
           02  PC01-CARDTYPECODE                                                
               PIC X(0020).                                                     
           02  PC01-CDEV                                                        
               PIC X(0004).                                                     
           02  PC01-DVALIDF                                                     
               PIC X(0008).                                                     
           02  PC01-DVALIDR                                                     
               PIC X(0008).                                                     
           02  PC01-STATUT                                                      
               PIC X(0005).                                                     
           02  PC01-CMAP                                                        
               PIC S9(07)V9(04) COMP-3.                                         
           02  PC01-SOLDED                                                      
               PIC S9(07)V9(02) COMP-3.                                         
           02  PC01-SOLDEP                                                      
               PIC S9(07)V9(02) COMP-3.                                         
           02  PC01-NSOCVTE                                                     
               PIC X(0003).                                                     
           02  PC01-NLIEUVTE                                                    
               PIC X(0003).                                                     
           02  PC01-DVENTE                                                      
               PIC X(0008).                                                     
           02  PC01-CPREST                                                      
               PIC X(0005).                                                     
           02  PC01-NSOCNET                                                     
               PIC X(0003).                                                     
           02  PC01-WDEVCCP                                                     
               PIC X(0001).                                                     
           02  PC01-COMMENT                                                     
               PIC X(0080).                                                     
           02  PC01-DSYST                                                       
               PIC S9(13)      COMP-3.                                          
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPC0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-RANDOMNUM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-RANDOMNUM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-SERIALNUM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-SERIALNUM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-CARDTYPECODE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-CARDTYPECODE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-DVALIDF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-DVALIDF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-DVALIDR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-DVALIDR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-STATUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-STATUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-CMAP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-CMAP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-SOLDED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-SOLDED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-SOLDEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-SOLDEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-NSOCVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-NSOCVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-NLIEUVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-NLIEUVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-CPREST-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-CPREST-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-NSOCNET-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-NSOCNET-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-WDEVCCP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-WDEVCCP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-COMMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-COMMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
