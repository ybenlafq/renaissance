      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      * COMMAREA TRANSACTION IGP0                                    *          
      ***************************************************** COMMIGP0 *          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-IGP0-LONG-COMMAREA PIC S9(4) COMP VALUE +0446.                   
      *--                                                                       
       01  COM-IGP0-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +0446.                 
      *}                                                                        
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC 99.                                       
          02 COMM-DATE-ANNEE      PIC 99.                                       
          02 COMM-DATE-MOIS       PIC 99.                                       
          02 COMM-DATE-JOUR       PIC 99.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT NUMERO DE SEMAINE                                          
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC 99.                                       
             05 COMM-DATE-SEMAA   PIC 99.                                       
             05 COMM-DATE-SEMNU   PIC 99.                                       
          02 COMM-DATE-FILLER     PIC X(08).                                    
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      * ZONES RESERVEES APPLICATIVES ----------------------------- 442          
          02 COM-IGP0-TRATEM     PIC  X.                                        
          02 COM-IGP0-RESULT     PIC  X.                                        
          02 COM-IGP0-CODTCT     PIC  XXXX.                                     
          02 COM-IGP0-APPLID     PIC  XXXXXXXX.                                 
      *   TRAITEMENT DE LA TS                                                   
          02 COM-IGP0-TSNAME     PIC  XXXXXXXX.                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-TSLONG     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-TSLONG     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-TSITEM     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-TSITEM     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-TSUTIL     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-TSUTIL     PIC S9(4) COMP-5.                              
      *}                                                                        
      *   COMPTEURS                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-CTRCON     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-CTRCON     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-CTRMAJ     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-CTRMAJ     PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-CTRSUP     PIC S9(4) COMP.                                
      *--                                                                       
          02 COM-IGP0-CTRSUP     PIC S9(4) COMP-5.                              
      *}                                                                        
      *   TS DE CONSULTATION - MAJ - SUPPRESSION                                
          02 COM-IGP0-TSACON     PIC  XXXXXXXX.                                 
          02 COM-IGP0-TSAMAJ     PIC  XXXXXXXX.                                 
          02 COM-IGP0-TSASUP     PIC  XXXXXXXX.                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COM-IGP0-TSLNGH     PIC S9(4) COMP   VALUE +13.                    
      *--                                                                       
          02 COM-IGP0-TSLNGH     PIC S9(4) COMP-5   VALUE +13.                  
      *}                                                                        
       EJECT                                                                    
                                                                                
