      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVRB0701.                                                             
      *}                                                                        
      *  NUMERO DE CONTRAT REDBOX                                               
          05 RB07-NCONTREDBOX           PIC X(18).                              
      *  NUMERO DE VENTE NCG'                                                   
          05 RB07-NVENTENCG             PIC X(13).                              
      * LE NUMERO  IDENT EXTERNE'                                               
          05 RB07-NIDENTEXT             PIC X(20).                              
      * NUMERO D'IMMOBILISATION'                                                
          05 RB07-NUMIMMO               PIC X(12).                              
      * CODE SOUS TRAITANT'                                                     
          05 RB07-CSST                  PIC X(10).                              
      * TYPE D'INTERVENTION'                                                    
          05 RB07-CTYPINTER             PIC X(05).                              
      * LE N� D'INTERVENTION'                                                   
          05 RB07-NINTER                PIC X(20).                              
      * MONTANT A AMORTIR'                                                      
          05 RB07-MONTANT               PIC S9(07)V9(02) COMP-3.                
      * DATE ACQUISITION'                                                       
          05 RB07-DATERB                PIC X(08).                              
      * DATE DE RESILIATION REDBOX'                                             
          05 RB07-DATERBANN             PIC X(08).                              
      * DATE NASC'                                                              
          05 RB07-DATENASC              PIC X(08).                              
      * DATE DEBUT AMORTISSEMENT'                                               
          05 RB07-DAMORT                PIC X(08).                              
      * DATE DU DEVERS ABEL'                                                    
          05 RB07-DAMORTDEV             PIC X(08).                              
      * DATE DE RESILIATION ABEL'                                               
          05 RB07-DRESIL                PIC X(08).                              
      * DATE DE DEVERS RESIL ABEL'                                              
          05 RB07-DRESILDEV             PIC X(08).                              
      * DSYST'                                                                  
          05 RB07-DSYST                 PIC S9(13) COMP-3.                      
      * CODE STAUT DE TRAITEMENT                                                
          05 RB07-CSTATUT               PIC X(05).                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVRB0701-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVRB0701-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-NCONTREDBOX-F         PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-NCONTREDBOX-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-NVENTENCG-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-NVENTENCG-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-NIDENTEXT-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-NIDENTEXT-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-NUMIMMO-F             PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-NUMIMMO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-CSST-F                PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-CSST-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-CTYPINTER-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-CTYPINTER-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-NINTER-F              PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-NINTER-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-MONTANT-F             PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-MONTANT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DATERB-F              PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DATERB-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DATERBANN-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DATERBANN-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DATENASC-F            PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DATENASC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DAMORT-F              PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DAMORT-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DAMORTDEV-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DAMORTDEV-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DRESIL-F              PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DRESIL-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DRESILDEV-F           PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DRESILDEV-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-DSYST-F               PIC S9(4) COMP.                         
      *--                                                                       
          05 RB07-DSYST-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 RB07-CSTATUT-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
          05 RB07-CSTATUT-F             PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
