      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1100.                                                            
           02  AP11-NSEQID         PIC S9(18) COMP-3.                           
           02  AP11-NENCAISSE      PIC X(0014).                                 
           02  AP11-NVENTE         PIC X(0014).                                 
           02  AP11-CMOPAI         PIC X(0005).                                 
           02  AP11-PMONTANT       PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP11-WISCESU        PIC X(0001).                                 
           02  AP11-DATETRAIT      PIC X(0008).                                 
           02  AP11-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-CMOPAI-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-CMOPAI-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-PMONTANT-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-PMONTANT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-WISCESU-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-WISCESU-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-DATETRAIT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP11-DATETRAIT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP11-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP11-DSYST-F        PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
