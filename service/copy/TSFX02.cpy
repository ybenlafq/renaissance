      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *       DESCRIPTION DE TSFX02                                     00000020
      *                                                                 00000030
       01  TS-FX02.                                                     00000040
      *--------------------------------  LONGUEUR TS                    00000050
           02 TS-FX02-LONG                    PIC S9(5) COMP-3          00000060
                                              VALUE +216.               00000070
      *--------------------------------  DONNEES TS                     00000080
           02  TS-FX02-DONNEES.                                         00000090
               03  TS-TABLE OCCURS 12.                                  00000100
                   05  TS-FX02-CPTEGL           PIC X(6).               00000110
                   05  TS-FX02-CPTECOSCREDIT    PIC X(6).               00000120
                   05  TS-FX02-CPTECOSDEBIT     PIC X(6).               00000130
                                                                                
