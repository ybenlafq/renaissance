      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0100.                                                            
           02  QC01-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC01-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC01-NCARTE                                                      
               PIC X(0007).                                                     
           02  QC01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  QC01-NLIEU                                                       
               PIC X(0003).                                                     
           02  QC01-NVENTE                                                      
               PIC X(0007).                                                     
           02  QC01-DDELIV                                                      
               PIC X(0008).                                                     
           02  QC01-DVENTE                                                      
               PIC X(0008).                                                     
           02  QC01-NCODIC                                                      
               PIC X(0007).                                                     
           02  QC01-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  QC01-CLIVR1                                                      
               PIC X(0010).                                                     
           02  QC01-CLIVR2                                                      
               PIC X(0010).                                                     
           02  QC01-CTLIVREUR                                                   
               PIC X(0005).                                                     
           02  QC01-CMODDEL                                                     
               PIC X(0003).                                                     
           02  QC01-CPERIM                                                      
               PIC X(0005).                                                     
           02  QC01-WCREDIT                                                     
               PIC X(0001).                                                     
           02  QC01-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  QC01-DSAISIE                                                     
               PIC X(0008).                                                     
           02  QC01-DCREATION                                                   
               PIC X(0008).                                                     
           02  QC01-DDESCENTE                                                   
               PIC X(0008).                                                     
           02  QC01-DEDIT                                                       
               PIC X(0008).                                                     
           02  QC01-FREPONSE                                                    
               PIC X(0040).                                                     
           02  QC01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CLIVR1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CLIVR1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CLIVR2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CLIVR2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CTLIVREUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CTLIVREUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CPERIM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CPERIM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-WCREDIT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-WCREDIT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DDESCENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DDESCENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DEDIT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DEDIT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-FREPONSE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-FREPONSE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
