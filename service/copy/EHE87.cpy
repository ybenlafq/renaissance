      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE87   EHE87                                              00000020
      ***************************************************************** 00000030
       01   EHE87I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCTRTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCCTRTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCTRTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCCTRTI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCTRTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCTRTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCTRTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCTRTI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTIERSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTIERSF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTIERSI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPREPL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNPREPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPREPF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNPREPI   PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPREPL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDPREPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDPREPF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDPREPI   PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADSTKL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MADSTKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MADSTKF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MADSTKI   PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNACCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNACCF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNACCI    PIC X(12).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDACCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDACCF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDACCI    PIC X(10).                                      00000530
           02 FILLER  OCCURS   13 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSELI   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNLIEUI      PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNHSI   PIC X(7).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNCODICI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCFAMI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLREFI  PIC X(18).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDESTL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MDESTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDESTF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDESTI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADSTKLL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MADSTKLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MADSTKLF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MADSTKLI     PIC X(7).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNACCLL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNACCLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNACCLF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNACCLI      PIC X(12).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(79).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EHE87   EHE87                                              00001160
      ***************************************************************** 00001170
       01   EHE87O REDEFINES EHE87I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNPAGEA   PIC X.                                          00001350
           02 MNPAGEC   PIC X.                                          00001360
           02 MNPAGEP   PIC X.                                          00001370
           02 MNPAGEH   PIC X.                                          00001380
           02 MNPAGEV   PIC X.                                          00001390
           02 MNPAGEO   PIC 99.                                         00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC 99.                                    00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCCTRTA   PIC X.                                          00001490
           02 MCCTRTC   PIC X.                                          00001500
           02 MCCTRTP   PIC X.                                          00001510
           02 MCCTRTH   PIC X.                                          00001520
           02 MCCTRTV   PIC X.                                          00001530
           02 MCCTRTO   PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLCTRTA   PIC X.                                          00001560
           02 MLCTRTC   PIC X.                                          00001570
           02 MLCTRTP   PIC X.                                          00001580
           02 MLCTRTH   PIC X.                                          00001590
           02 MLCTRTV   PIC X.                                          00001600
           02 MLCTRTO   PIC X(20).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTIERSA   PIC X.                                          00001630
           02 MTIERSC   PIC X.                                          00001640
           02 MTIERSP   PIC X.                                          00001650
           02 MTIERSH   PIC X.                                          00001660
           02 MTIERSV   PIC X.                                          00001670
           02 MTIERSO   PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNPREPA   PIC X.                                          00001700
           02 MNPREPC   PIC X.                                          00001710
           02 MNPREPP   PIC X.                                          00001720
           02 MNPREPH   PIC X.                                          00001730
           02 MNPREPV   PIC X.                                          00001740
           02 MNPREPO   PIC X(7).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDPREPA   PIC X.                                          00001770
           02 MDPREPC   PIC X.                                          00001780
           02 MDPREPP   PIC X.                                          00001790
           02 MDPREPH   PIC X.                                          00001800
           02 MDPREPV   PIC X.                                          00001810
           02 MDPREPO   PIC X(10).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MADSTKA   PIC X.                                          00001840
           02 MADSTKC   PIC X.                                          00001850
           02 MADSTKP   PIC X.                                          00001860
           02 MADSTKH   PIC X.                                          00001870
           02 MADSTKV   PIC X.                                          00001880
           02 MADSTKO   PIC X(7).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNACCA    PIC X.                                          00001910
           02 MNACCC    PIC X.                                          00001920
           02 MNACCP    PIC X.                                          00001930
           02 MNACCH    PIC X.                                          00001940
           02 MNACCV    PIC X.                                          00001950
           02 MNACCO    PIC X(12).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MDACCA    PIC X.                                          00001980
           02 MDACCC    PIC X.                                          00001990
           02 MDACCP    PIC X.                                          00002000
           02 MDACCH    PIC X.                                          00002010
           02 MDACCV    PIC X.                                          00002020
           02 MDACCO    PIC X(10).                                      00002030
           02 FILLER  OCCURS   13 TIMES .                               00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MSELA   PIC X.                                          00002060
             03 MSELC   PIC X.                                          00002070
             03 MSELP   PIC X.                                          00002080
             03 MSELH   PIC X.                                          00002090
             03 MSELV   PIC X.                                          00002100
             03 MSELO   PIC X.                                          00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MNLIEUA      PIC X.                                     00002130
             03 MNLIEUC PIC X.                                          00002140
             03 MNLIEUP PIC X.                                          00002150
             03 MNLIEUH PIC X.                                          00002160
             03 MNLIEUV PIC X.                                          00002170
             03 MNLIEUO      PIC X(3).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MNHSA   PIC X.                                          00002200
             03 MNHSC   PIC X.                                          00002210
             03 MNHSP   PIC X.                                          00002220
             03 MNHSH   PIC X.                                          00002230
             03 MNHSV   PIC X.                                          00002240
             03 MNHSO   PIC X(7).                                       00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MNCODICA     PIC X.                                     00002270
             03 MNCODICC     PIC X.                                     00002280
             03 MNCODICP     PIC X.                                     00002290
             03 MNCODICH     PIC X.                                     00002300
             03 MNCODICV     PIC X.                                     00002310
             03 MNCODICO     PIC X(7).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MCFAMA  PIC X.                                          00002340
             03 MCFAMC  PIC X.                                          00002350
             03 MCFAMP  PIC X.                                          00002360
             03 MCFAMH  PIC X.                                          00002370
             03 MCFAMV  PIC X.                                          00002380
             03 MCFAMO  PIC X(5).                                       00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MCMARQA      PIC X.                                     00002410
             03 MCMARQC PIC X.                                          00002420
             03 MCMARQP PIC X.                                          00002430
             03 MCMARQH PIC X.                                          00002440
             03 MCMARQV PIC X.                                          00002450
             03 MCMARQO      PIC X(5).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MLREFA  PIC X.                                          00002480
             03 MLREFC  PIC X.                                          00002490
             03 MLREFP  PIC X.                                          00002500
             03 MLREFH  PIC X.                                          00002510
             03 MLREFV  PIC X.                                          00002520
             03 MLREFO  PIC X(18).                                      00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MDESTA  PIC X.                                          00002550
             03 MDESTC  PIC X.                                          00002560
             03 MDESTP  PIC X.                                          00002570
             03 MDESTH  PIC X.                                          00002580
             03 MDESTV  PIC X.                                          00002590
             03 MDESTO  PIC X(5).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MADSTKLA     PIC X.                                     00002620
             03 MADSTKLC     PIC X.                                     00002630
             03 MADSTKLP     PIC X.                                     00002640
             03 MADSTKLH     PIC X.                                     00002650
             03 MADSTKLV     PIC X.                                     00002660
             03 MADSTKLO     PIC X(7).                                  00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MNACCLA      PIC X.                                     00002690
             03 MNACCLC PIC X.                                          00002700
             03 MNACCLP PIC X.                                          00002710
             03 MNACCLH PIC X.                                          00002720
             03 MNACCLV PIC X.                                          00002730
             03 MNACCLO      PIC X(12).                                 00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(79).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
