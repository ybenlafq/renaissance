      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-PC40-RECORD.                                                      
           05 TS-PC40-TABLE            OCCURS 13.                               
              10 TS-PC40-NREGLE                PIC X(10).                       
              10 TS-PC40-TYPE                  PIC X(03).                       
              10 TS-PC40-NLIGNE                PIC X(02).                       
              10 TS-PC40-LIBELLE               PIC X(25).                       
              10 TS-PC40-TVA                   PIC X(03).                       
              10 TS-PC40-CINTERFACE            PIC X(05).                       
              10 TS-PC40-CTYPEOP               PIC X(05).                       
                                                                                
