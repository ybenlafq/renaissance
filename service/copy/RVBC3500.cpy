      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVBC3500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBC3500                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVBC3500.                                                            
           02  BC35-CPROGRAMME                                                  
               PIC X(0006).                                                     
           02  BC35-DDEBUT                                                      
               PIC X(0008).                                                     
           02  BC35-DFIN                                                        
               PIC X(0008).                                                     
           02  BC35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBC3500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBC3500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC35-CPROGRAMME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC35-CPROGRAMME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC35-DDEBUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC35-DDEBUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC35-DFIN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC35-DFIN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
