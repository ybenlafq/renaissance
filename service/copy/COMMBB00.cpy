      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BB00-LONG-COMMAREA             PIC S9(4) COMP           00000020
      *                                                  VALUE +4096.   00000020
      *--                                                                       
       01  COMM-BB00-LONG-COMMAREA             PIC S9(4) COMP-5                 
                                                         VALUE +4096.           
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
      *    ZONES RESERVEES A AIDA -------------------------------- 100  00000060
           05  FILLER-COM-AIDA                 PIC X(100).              00000070
                                                                        00000080
      *    ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020  00000090
           05  COMM-CICS-APPLID                PIC X(008).              00000100
           05  COMM-CICS-NETNAM                PIC X(008).              00000110
           05  COMM-CICS-TRANSA                PIC X(004).              00000120
                                                                        00000130
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00000140
           05  COMM-DATE-SIECLE                PIC X(002).              00000150
           05  COMM-DATE-ANNEE                 PIC X(002).              00000160
           05  COMM-DATE-MOIS                  PIC X(002).              00000170
           05  COMM-DATE-JOUR                  PIC 9(002).              00000180
                                                                        00000130
      *    QUANTIEMES CALENDAIRE ET STANDARD                            00000190
           05  COMM-DATE-QNTA                  PIC 9(03).               00000200
           05  COMM-DATE-QNT0                  PIC 9(05).               00000210
                                                                        00000130
      *    ANNEE BISSEXTILE 1=OUI 0=NON                                 00000220
           05  COMM-DATE-BISX                  PIC 9(01).               00000230
                                                                        00000130
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           05  COMM-DATE-JSM                   PIC 9(01).               00000250
                                                                        00000130
      *    LIBELLES DU JOUR COURT - LONG                                00000260
           05  COMM-DATE-JSM-LC                PIC X(03).               00000270
           05  COMM-DATE-JSM-LL                PIC X(08).               00000280
                                                                        00000130
      *    LIBELLES DU MOIS COURT - LONG                                00000290
           05  COMM-DATE-MOIS-LC               PIC X(03).               00000300
           05  COMM-DATE-MOIS-LL               PIC X(08).               00000310
                                                                        00000130
      *    DIFFERENTES FORMES DE DATE                                   00000320
           05  COMM-DATE-SSAAMMJJ              PIC X(08).               00000330
           05  COMM-DATE-AAMMJJ                PIC X(06).               00000340
           05  COMM-DATE-JJMMSSAA              PIC X(08).               00000350
           05  COMM-DATE-JJMMAA                PIC X(06).               00000360
           05  COMM-DATE-JJ-MM-AA              PIC X(08).               00000370
           05  COMM-DATE-JJ-MM-SSAA            PIC X(10).               00000380
                                                                        00000130
      *    TRAITEMENT DU NUMERO DE SEMAINE                              00000390
           05  COMM-DATE-WEEK.                                          00000400
               10  COMM-DATE-SEMSS             PIC 9(02).               00000410
               10  COMM-DATE-SEMAA             PIC 9(02).               00000420
               10  COMM-DATE-SEMNU             PIC 9(02).               00000430
           05  COMM-DATE-FILLER                PIC X(08).               00000440
                                                                        00000130
      *    ZONES RESERVEES TRAITEMENT DU SWAP                           00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-SWAP-CURS                  PIC S9(4) COMP VALUE -1. 00000460
      *                                                                         
      *--                                                                       
           05  COMM-SWAP-CURS                  PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
           05  COMM-BB00-ENTETE.                                        00000500
               10  COMM-ENTETE.                                         00000510
                   15  COMM-CODLANG            PIC X(02).               00000520
                   15  COMM-CODPIC             PIC X(02).               00000530
                   15  COMM-LEVEL-SUP          PIC X(05).               00000540
                   15  COMM-LEVEL-MAX          PIC X(05).               00000540
                   15  COMM-ACID               PIC X(08).                       
               10  COMM-MES-ERREUR.                                             
                   15  COMM-COD-ERREUR         PIC X(01).                       
                   15  COMM-LIB-MESSAG         PIC X(59).                       
               10  COMM-DAT-DEBUT              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-DEBUT.              
                   15  COMM-DAT-DEBJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBSA          PIC X(04).                       
               10  COMM-DAT-FINAL              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-FINAL.              
                   15  COMM-DAT-FINJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINSA          PIC X(04).                       
                                                                        00000130
      *--- MENU CONTROLE ENCODAGE FOURNISSEUR - TBB00                   00000390
           05  COMM-BB00-APPLI.                                         00000500
               10  COMM-BB00-DATDEB-CPT        PIC X(10).               00000520
               10  COMM-BB00-DATFIN-CPT        PIC X(10).               00000520
               10  COMM-BB00-DATDEB-CRE        PIC X(10).               00000520
               10  COMM-BB00-DATFIN-CRE        PIC X(10).               00000520
               10  COMM-BB00-NCOMMANDE         PIC X(12).               00000520
               10  COMM-BB00-NDOSSIER          PIC X(12).               00000520
               10  COMM-BB00-ADR-LIVR1         PIC X(38).               00000620
               10  COMM-BB00-NVENTE            PIC X(07).               00000620
               10  COMM-BB00-TELDOM            PIC X(10).               00000620
               10  COMM-BB00-CERR              PIC X(05).               00000620
               10  COMM-BB00-WNBB00            PIC X(01).               00000620
               10  COMM-BB00-WNBB01            PIC X(01).               00000620
               10  COMM-BB00-WORAPP            PIC X(01).               00000620
      *--- LISTE DES RECEPTIONS - TBB01                                 00000390
           05  COMM-BB01-APPLI.                                         00000040
               10  COMM-BB01.                                           00000050
                   15  COMM-BB01-CPTEURS.                                     00
                       20  COMM-BB01-INDTS     PIC 9(03).                     00
                       20  COMM-BB01-INDMAX    PIC 9(03).                     00
                       20  COMM-BB01-NLIGNE    PIC 9(03).                     00
                   15  COMM-BB01-LIGNE.                                    00000
                       20  COMM-BB01-NCOMMANDE     PIC X(12).                 00
                       20  COMM-BB01-NDOSSIER      PIC X(12).                 00
                       20  COMM-BB01-RFOURN        PIC X(07).                 00
                       20  COMM-BB01-ADR-LIVR1     PIC X(10).                 00
                       20  COMM-BB01-QTE-COMMANDEE PIC X(02).                 00
                       20  COMM-BB01-NVENTE        PIC X(07).                 00
                       20  COMM-BB01-NSEQNQ   PIC  S9(5)V USAGE COMP-3.       00
                       20  COMM-BB01-CMODDEL       PIC X(03).                 00
                       20  COMM-BB01-CFAM          PIC X(05).                   
                       20  COMM-BB01-CMARQ         PIC X(05).                   
      *--- DETAIL D'UNE RECEPTION - TBB02                               00000390
           05  COMM-BB02-APPLI.                                         00000040
               10  COMM-BB02.                                           00000050
                   15  COMM-BB02-CPTEURS.                                     00
                       20  COMM-BB02-INDTS     PIC 9(03).                     00
                       20  COMM-BB02-INDMAX    PIC 9(03).                     00
                       20  COMM-BB02-NLIGNE    PIC 9(03).                     00
                   15  COMM-BB02-INFOS.                                         
                       20  COMM-BB02-DCOMPTA         PIC X(10).                 
                       20  COMM-BB02-ADR-LIVR1       PIC X(38).                 
                       20  COMM-BB02-ADR-LIVR2       PIC X(38).                 
                       20  COMM-BB02-ADR-LIVR6       PIC X(50).                 
                       20  COMM-BB02-CPOST1          PIC X(05).                 
                       20  COMM-BB02-RCADEAU         PIC X(03).                 
                       20  COMM-BB02-LCADEAU         PIC X(30).                 
                       20  COMM-BB02-TEL-DOM         PIC X(14).                 
                       20  COMM-BB02-TEL-PORT        PIC X(14).                 
                       20  COMM-BB02-TEL-BUR         PIC X(14).                 
                       20  COMM-BB02-NSOCIETE        PIC X(03).                 
                       20  COMM-BB02-NLIEU           PIC X(03).                 
                       20  COMM-BB02-DVENTE          PIC X(10).                 
                       20  COMM-BB02-DDELIV          PIC X(10).                 
                       20  COMM-BB02-NDOS            PIC X(12).                 
                       20  COMM-BB02-NCODIC          PIC X(07).                 
                       20  COMM-BB02-CFAM            PIC X(05).                 
                       20  COMM-BB02-CMARQ           PIC X(05).                 
                       20  COMM-BB02-LREFFOURN       PIC X(20).                 
                       20  COMM-BB02-QVENDUE         PIC ZZ.                    
                       20  COMM-BB02-NCOLIS          PIC X(13).                 
                       20  COMM-BB02-NOM             PIC X(38).                 
                       20  COMM-BB02-ADRESSE         PIC X(38).                 
                       20  COMM-BB02-COMMUNE         PIC X(50).                 
                       20  COMM-BB02-CPOSTAL         PIC X(05).                 
                       20  COMM-BB02-TELDOM          PIC X(14).                 
                       20  COMM-BB02-NGSM            PIC X(14).                 
                       20  COMM-BB02-TELBUR          PIC X(14).                 
                       20  COMM-BB02-INDICE          PIC 9(01).                 
                       20  COMM-BB02-STATUT OCCURS 4.                           
                           23 COMM-BB02-CSTATUT      PIC X(05).                 
                           23 COMM-BB02-LSTATUT      PIC X(15).                 
                           23 COMM-BB02-DCREA        PIC X(10).                 
                           23 COMM-BB02-COMMENT      PIC X(46).                 
                           23 COMM-BB02-INTENTSEQ    PIC X(10).                 
                           23 COMM-BB02-WTOPELIVRE1   PIC X(01).                
                           23 COMM-BB02-WTOPELIVRE2   PIC X(01).                
           05  COMM-BB00-FILLER                PIC X(2729).             00000620
      *---                                                              00000390
                                                                                
