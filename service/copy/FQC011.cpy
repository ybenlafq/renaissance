      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *   COPY DU FICHIER POUR BQC170 : EXTRACTION CARTES T 5                   
      *   EXTRACTION DE VENTES DES FAMILLES D'UN RAYON                          
      *                                                                         
      * REMARQUES :                                                             
      *                                                                         
      * ---> LONGUEUR ARTICLE = 100                                             
      *                                                                         
      * ---> CTYPE                                                              
      *       A = INFOS NB DE VENTES PAR PLATEFORME                             
      *       B = LIGNES VENTES DE LA PLATEFORME                                
      * ---> NVENTE                                                             
      *       LIGNE A --->  = 0                                                 
      *       LIGNE B ---> <> 0                                                 
      * ---> NBVENTE                                                            
      *       LIGNE A ---> <> 0                                                 
      *       LIGNE B --->  = 0                                                 
      ******************************************************************        
      *                                                                         
       01  DSECT-FQC011.                                                        
           10  FQC011-NSOCLIVR       PIC X(03).                                 
           10  FQC011-NDEPOT         PIC X(03).                                 
           10  FQC011-CTYPE          PIC X(01).                                 
           10  FQC011-NBVENTE        PIC S9(7) COMP-3.                          
           10  FQC011-NVENTE         PIC X(07).                                 
           10  FQC011-DDELIV         PIC X(08).                                 
           10  FQC011-DVENTE         PIC X(08).                                 
           10  FQC011-NCODIC         PIC X(07).                                 
           10  FQC011-CVENDEUR       PIC X(06).                                 
           10  FQC011-CINSEE         PIC X(05).                                 
           10  FQC011-CPROTOUR       PIC X(05).                                 
           10  FQC011-CTOURNEE       PIC X(08).                                 
           10  FQC011-CEQUIPE        PIC X(05).                                 
           10  FQC011-CMODDEL        PIC X(03).                                 
           10  FQC011-WCREDIT        PIC X(01).                                 
           10  FQC011-CPOSTAL        PIC X(05).                                 
           10  FQC011-NSOCIETE       PIC X(03).                                 
           10  FQC011-NLIEU          PIC X(03).                                 
           10  FQC011-CPLAGE         PIC X(02).                                 
           10  FQC011-FILLER         PIC X(19).                                 
                                                                                
