      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE130 AU 10/08/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,11,BI,A,                          *        
      *                           21,06,BI,A,                          *        
      *                           27,03,BI,A,                          *        
      *                           30,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE130.                                                        
1           05 NOMETAT-IHE130           PIC X(6) VALUE 'IHE130'.                
            05 RUPTURES-IHE130.                                                 
7          10 IHE130-LVPARAM            PIC X(03).                      007  003
10         10 IHE130-CLIEUTRT           PIC X(11).                      010  011
21         10 IHE130-CLIEUHET           PIC X(06).                      021  006
27         10 IHE130-NLIEUHED           PIC X(03).                      027  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
30    *    10 IHE130-SEQUENCE           PIC S9(04) COMP.                030  002
      *--                                                                       
           10 IHE130-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE130.                                                   
32         10 IHE130-CDEVEQU            PIC X(03).                      032  003
35         10 IHE130-CDEVREF            PIC X(03).                      035  003
38         10 IHE130-CFAM               PIC X(05).                      038  005
43         10 IHE130-CMARQ              PIC X(05).                      043  005
48         10 IHE130-DENVOI             PIC X(08).                      048  008
56         10 IHE130-GHE-GEF            PIC X(03).                      056  003
59         10 IHE130-NCODIC             PIC X(07).                      059  007
66         10 IHE130-NGHE               PIC X(07).                      066  007
73         10 IHE130-PRMP               PIC 9(07)V9(2).                 073  009
82         10 IHE130-QTE                PIC 9(05)     .                 082  005
87         10 IHE130-VALO               PIC 9(08)V9(2).                 087  010
97         10 IHE130-TAUX-DEV           PIC S9(01)V9(6) COMP-3.         097  004
101         05 FILLER                      PIC X(412).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE130-LONG           PIC S9(4)   COMP  VALUE +100.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE130-LONG           PIC S9(4) COMP-5  VALUE +100.           
                                                                                
      *}                                                                        
