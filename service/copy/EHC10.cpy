      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC10   EHC10                                              00000020
      ***************************************************************** 00000030
       01   EHC10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUHCI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHSL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCHSF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCHSI    PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENTREEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDENTREEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDENTREEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDENTREEI      PIC X(10).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROVORIGL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPROVORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPROVORIGF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPROVORIGI     PIC X.                                     00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORIGINEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCORIGINEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCORIGINEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCORIGINEI     PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODICI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLREFI    PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSERIEL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSERIEF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCSERIEI  PIC X(16).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENTL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCLIENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENTF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCLIENTI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACCFOURL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MDACCFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDACCFOURF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDACCFOURI     PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNACCORDI      PIC X(12).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERLOCL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MINTERLOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MINTERLOCF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MINTERLOCI     PIC X(10).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDENVOII  PIC X(10).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNENVOII  PIC X(7).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTENVOIL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCTENVOIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTENVOIF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCTENVOII      PIC X.                                     00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCTIERSI  PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSOLDEL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MDSOLDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSOLDEF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDSOLDEI  PIC X(10).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBERRI  PIC X(78).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCODTRAI  PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCICSI    PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * SDF: EHC10   EHC10                                              00001190
      ***************************************************************** 00001200
       01   EHC10O REDEFINES EHC10I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MPAGEA    PIC X.                                          00001380
           02 MPAGEC    PIC X.                                          00001390
           02 MPAGEP    PIC X.                                          00001400
           02 MPAGEH    PIC X.                                          00001410
           02 MPAGEV    PIC X.                                          00001420
           02 MPAGEO    PIC X(2).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MPAGETOTA      PIC X.                                     00001450
           02 MPAGETOTC PIC X.                                          00001460
           02 MPAGETOTP PIC X.                                          00001470
           02 MPAGETOTH PIC X.                                          00001480
           02 MPAGETOTV PIC X.                                          00001490
           02 MPAGETOTO      PIC X(2).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNLIEUHCA      PIC X.                                     00001520
           02 MNLIEUHCC PIC X.                                          00001530
           02 MNLIEUHCP PIC X.                                          00001540
           02 MNLIEUHCH PIC X.                                          00001550
           02 MNLIEUHCV PIC X.                                          00001560
           02 MNLIEUHCO      PIC X(3).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNCHSA    PIC X.                                          00001590
           02 MNCHSC    PIC X.                                          00001600
           02 MNCHSP    PIC X.                                          00001610
           02 MNCHSH    PIC X.                                          00001620
           02 MNCHSV    PIC X.                                          00001630
           02 MNCHSO    PIC 9(7).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MDENTREEA      PIC X.                                     00001660
           02 MDENTREEC PIC X.                                          00001670
           02 MDENTREEP PIC X.                                          00001680
           02 MDENTREEH PIC X.                                          00001690
           02 MDENTREEV PIC X.                                          00001700
           02 MDENTREEO      PIC X(10).                                 00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MPROVORIGA     PIC X.                                     00001730
           02 MPROVORIGC     PIC X.                                     00001740
           02 MPROVORIGP     PIC X.                                     00001750
           02 MPROVORIGH     PIC X.                                     00001760
           02 MPROVORIGV     PIC X.                                     00001770
           02 MPROVORIGO     PIC X.                                     00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCORIGINEA     PIC X.                                     00001800
           02 MCORIGINEC     PIC X.                                     00001810
           02 MCORIGINEP     PIC X.                                     00001820
           02 MCORIGINEH     PIC X.                                     00001830
           02 MCORIGINEV     PIC X.                                     00001840
           02 MCORIGINEO     PIC X(7).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNCODICA  PIC X.                                          00001870
           02 MNCODICC  PIC X.                                          00001880
           02 MNCODICP  PIC X.                                          00001890
           02 MNCODICH  PIC X.                                          00001900
           02 MNCODICV  PIC X.                                          00001910
           02 MNCODICO  PIC 9(7).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCFAMA    PIC X.                                          00001940
           02 MCFAMC    PIC X.                                          00001950
           02 MCFAMP    PIC X.                                          00001960
           02 MCFAMH    PIC X.                                          00001970
           02 MCFAMV    PIC X.                                          00001980
           02 MCFAMO    PIC X(5).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MCMARQA   PIC X.                                          00002010
           02 MCMARQC   PIC X.                                          00002020
           02 MCMARQP   PIC X.                                          00002030
           02 MCMARQH   PIC X.                                          00002040
           02 MCMARQV   PIC X.                                          00002050
           02 MCMARQO   PIC X(5).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLREFA    PIC X.                                          00002080
           02 MLREFC    PIC X.                                          00002090
           02 MLREFP    PIC X.                                          00002100
           02 MLREFH    PIC X.                                          00002110
           02 MLREFV    PIC X.                                          00002120
           02 MLREFO    PIC X(20).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCSERIEA  PIC X.                                          00002150
           02 MCSERIEC  PIC X.                                          00002160
           02 MCSERIEP  PIC X.                                          00002170
           02 MCSERIEH  PIC X.                                          00002180
           02 MCSERIEV  PIC X.                                          00002190
           02 MCSERIEO  PIC X(16).                                      00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCLIENTA  PIC X.                                          00002220
           02 MCLIENTC  PIC X.                                          00002230
           02 MCLIENTP  PIC X.                                          00002240
           02 MCLIENTH  PIC X.                                          00002250
           02 MCLIENTV  PIC X.                                          00002260
           02 MCLIENTO  PIC X(20).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MDACCFOURA     PIC X.                                     00002290
           02 MDACCFOURC     PIC X.                                     00002300
           02 MDACCFOURP     PIC X.                                     00002310
           02 MDACCFOURH     PIC X.                                     00002320
           02 MDACCFOURV     PIC X.                                     00002330
           02 MDACCFOURO     PIC X(10).                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MNACCORDA      PIC X.                                     00002360
           02 MNACCORDC PIC X.                                          00002370
           02 MNACCORDP PIC X.                                          00002380
           02 MNACCORDH PIC X.                                          00002390
           02 MNACCORDV PIC X.                                          00002400
           02 MNACCORDO      PIC X(12).                                 00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MINTERLOCA     PIC X.                                     00002430
           02 MINTERLOCC     PIC X.                                     00002440
           02 MINTERLOCP     PIC X.                                     00002450
           02 MINTERLOCH     PIC X.                                     00002460
           02 MINTERLOCV     PIC X.                                     00002470
           02 MINTERLOCO     PIC X(10).                                 00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MDENVOIA  PIC X.                                          00002500
           02 MDENVOIC  PIC X.                                          00002510
           02 MDENVOIP  PIC X.                                          00002520
           02 MDENVOIH  PIC X.                                          00002530
           02 MDENVOIV  PIC X.                                          00002540
           02 MDENVOIO  PIC X(10).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNENVOIA  PIC X.                                          00002570
           02 MNENVOIC  PIC X.                                          00002580
           02 MNENVOIP  PIC X.                                          00002590
           02 MNENVOIH  PIC X.                                          00002600
           02 MNENVOIV  PIC X.                                          00002610
           02 MNENVOIO  PIC X(7).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MCTENVOIA      PIC X.                                     00002640
           02 MCTENVOIC PIC X.                                          00002650
           02 MCTENVOIP PIC X.                                          00002660
           02 MCTENVOIH PIC X.                                          00002670
           02 MCTENVOIV PIC X.                                          00002680
           02 MCTENVOIO      PIC X.                                     00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MCTIERSA  PIC X.                                          00002710
           02 MCTIERSC  PIC X.                                          00002720
           02 MCTIERSP  PIC X.                                          00002730
           02 MCTIERSH  PIC X.                                          00002740
           02 MCTIERSV  PIC X.                                          00002750
           02 MCTIERSO  PIC X(5).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MDSOLDEA  PIC X.                                          00002780
           02 MDSOLDEC  PIC X.                                          00002790
           02 MDSOLDEP  PIC X.                                          00002800
           02 MDSOLDEH  PIC X.                                          00002810
           02 MDSOLDEV  PIC X.                                          00002820
           02 MDSOLDEO  PIC X(10).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLIBERRA  PIC X.                                          00002850
           02 MLIBERRC  PIC X.                                          00002860
           02 MLIBERRP  PIC X.                                          00002870
           02 MLIBERRH  PIC X.                                          00002880
           02 MLIBERRV  PIC X.                                          00002890
           02 MLIBERRO  PIC X(78).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCODTRAA  PIC X.                                          00002920
           02 MCODTRAC  PIC X.                                          00002930
           02 MCODTRAP  PIC X.                                          00002940
           02 MCODTRAH  PIC X.                                          00002950
           02 MCODTRAV  PIC X.                                          00002960
           02 MCODTRAO  PIC X(4).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCICSA    PIC X.                                          00002990
           02 MCICSC    PIC X.                                          00003000
           02 MCICSP    PIC X.                                          00003010
           02 MCICSH    PIC X.                                          00003020
           02 MCICSV    PIC X.                                          00003030
           02 MCICSO    PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNETNAMA  PIC X.                                          00003060
           02 MNETNAMC  PIC X.                                          00003070
           02 MNETNAMP  PIC X.                                          00003080
           02 MNETNAMH  PIC X.                                          00003090
           02 MNETNAMV  PIC X.                                          00003100
           02 MNETNAMO  PIC X(8).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSCREENA  PIC X.                                          00003130
           02 MSCREENC  PIC X.                                          00003140
           02 MSCREENP  PIC X.                                          00003150
           02 MSCREENH  PIC X.                                          00003160
           02 MSCREENV  PIC X.                                          00003170
           02 MSCREENO  PIC X(4).                                       00003180
                                                                                
