      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBCTL CONTROLE REDBOX                  *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBCTL .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBCTL .                                                            
      *}                                                                        
           05  RBCTL-CTABLEG2    PIC X(15).                                     
           05  RBCTL-CTABLEG2-REDEF REDEFINES RBCTL-CTABLEG2.                   
               10  RBCTL-CVAL            PIC X(05).                             
               10  RBCTL-NSEQ            PIC X(05).                             
           05  RBCTL-WTABLEG     PIC X(80).                                     
           05  RBCTL-WTABLEG-REDEF  REDEFINES RBCTL-WTABLEG.                    
               10  RBCTL-VALEUR          PIC X(50).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBCTL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBCTL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBCTL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBCTL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBCTL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBCTL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
