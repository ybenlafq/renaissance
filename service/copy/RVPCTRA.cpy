      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PCTRA TRANSPO TYPE CARTE/INTERFACE     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPCTRA.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPCTRA.                                                             
      *}                                                                        
           05  PCTRA-CTABLEG2    PIC X(15).                                     
           05  PCTRA-CTABLEG2-REDEF REDEFINES PCTRA-CTABLEG2.                   
               10  PCTRA-OPTYPE          PIC X(04).                             
               10  PCTRA-TYPE            PIC X(03).                             
               10  PCTRA-CTYPE           PIC X(07).                             
           05  PCTRA-WTABLEG     PIC X(80).                                     
           05  PCTRA-WTABLEG-REDEF  REDEFINES PCTRA-WTABLEG.                    
               10  PCTRA-WFLAG           PIC X(01).                             
               10  PCTRA-NREGLE          PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPCTRA-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPCTRA-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCTRA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PCTRA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCTRA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PCTRA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
