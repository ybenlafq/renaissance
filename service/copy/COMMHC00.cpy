      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-HC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-HC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ------------------------------------100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS ---------------------20           
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --------100         
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ------------------------182          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *-------------------------------------------------------3694-----*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME THC00)                *        
      *----------------------------------------------------------------*        
           02 COMM-HC00-APPLI.                                                  
      *----------------------------------------------------------------*        
              03 COMM-HC00-FIC.                                                 
      *..............................................CODE SOCIETE               
                 04 COMM-HC00-NSOC           PIC X(3).                          
      *..............................................TYPE LIEU CHS              
                 04 COMM-HC00-NLIEUHC        PIC X(3).                          
                 04 COMM-HC00-LLIEUHC        PIC X(20).                         
                 04 COMM-HC00-WCMOTIF        PIC X(10).                         
      *..............................................N� DE HS                   
                 04 COMM-HC00-NCHS           PIC X(7).                          
      *..............................................LIEU DE TRAITEMENT         
                 04 COMM-HC00-CTRAIT         PIC X(5).                          
                 04 COMM-HC00-LTRAIT         PIC X(20).                         
      *..............................................IMPRESSION                 
                 04 COMM-HC00-CIMP           PIC X(04).                         
                 04 COMM-HC00-MSG            PIC X(40).                         
                 04 COMM-HC00-PGM            PIC X(05).                         
      *..............................................LIEU DE STOCKAGE           
                 04 COMM-HC00-NSOCIETE       PIC X(3).                          
                 04 COMM-HC00-NLIEU          PIC X(3).                          
                 04 COMM-HC00-NSSLIEU        PIC X(3).                          
                 04 COMM-HC00-CLIEUTRT       PIC X(5).                          
      *--------------------------------------------- ITEM PAGINATION --*        
                 04 COMM-HC00-PAGE           PIC 9(2).                        00
                 04 COMM-HC00-PAGE-MAX       PIC 9(2).                        00
      *..............................................                           
              03 COMM-HC00-FILLER            PIC X(59).                         
              03 COMM-HC00-PROG              PIC X(3500).                       
      *------------------------------------------- TR THC03                     
              03 COMM-HC03-PROG       REDEFINES COMM-HC00-PROG.                 
      *..............................................DATE DEMANDE PD            
                 04 COMM-HC03-DDEMANDE       PIC X(8).                          
      *..............................................FONCTION                   
                 04 COMM-HC03-FONC           PIC X(1).                          
                    88 COMM-HC03-CRE         VALUE '0'.                         
                    88 COMM-HC03-MAJ         VALUE '1'.                         
      *..............................................SOLDE OU NON               
                 04 COMM-HC03-WSOLDE         PIC X(1).                          
                    88 COMM-HC03-NON-SOLDE   VALUE '0'.                         
                    88 COMM-HC03-SOLDE       VALUE '1'.                         
      *..............................................ENVOYE ?                   
                 04 COMM-HC03-WENVOI         PIC X(1).                          
                    88 COMM-HC03-NON-ENVOYE  VALUE '0'.                         
                    88 COMM-HC03-ENVOYE      VALUE '1'.                         
      *..............................................SI PF11                    
                 04 COMM-HC03-PF11           PIC X(1).                          
                    88 COMM-HC03-NON-PF11    VALUE '0'.                         
                    88 COMM-HC03-OUI-PF11    VALUE '1'.                         
      *..............................................SAISIE 1 OU 2              
                 04 COMM-HC03-WSAISIE        PIC X(1).                          
                    88 COMM-HC03-SAISIE1     VALUE '0'.                         
                    88 COMM-HC03-SAISIE2     VALUE '1'.                         
      *..............................................VALIDATION 1 OU 2          
                 04 COMM-HC03-VALIDATION     PIC X(1).                          
                    88 COMM-HC03-VALIDATION-1  VALUE '0'.                       
                    88 COMM-HC03-VALIDATION-2  VALUE '1'.                       
      *............................INDICE DE TABLE-TS OU SE TROUVE NCHS         
                 04 COMM-HC03-TS-I           PIC S999.                          
      *......................................DONNEES DE LA GEF                  
                 04 COMM-HC03-EF00WSOLD      PIC X(01).                         
                 04 COMM-HC03-NRENDU         PIC X(20).                         
                 04 COMM-HC03-DRENDU         PIC X(08).                         
                 04 COMM-HC03-CRENDU         PIC X(05).                         
                 04 COMM-HC03-NCODIC-PREC    PIC X(07).                         
      *..............................................NSEQ MAX DE RTHC03         
                 04 COMM-HC03-NSEQ           PIC XXX.                           
                 04 COMM-HC03-NSEQ-9 REDEFINES COMM-HC03-NSEQ                   
                                             PIC 999.                           
      *.....................................SI PF11, SAVE POUR RTHC03           
                 04 COMM-HC03-NCODIC         PIC X(7).                          
                 04 COMM-HC03-NENVOI         PIC X(7).                          
                 04 COMM-HC03-DDEPOT         PIC X(8).                          
                 04 COMM-HC03-CPROVORIG      PIC X(1).                          
                 04 COMM-HC03-CORIGINE       PIC X(7).                          
                 04 COMM-HC03-CSERIE         PIC X(16).                         
                 04 COMM-HC03-DACCFOUR       PIC X(8).                          
                 04 COMM-HC03-NACCORD        PIC X(12).                         
                 04 COMM-HC03-INTERLOC       PIC X(10).                         
                 04 COMM-HC03-DSOLDE         PIC X(8).                          
      *------------------------------------------- TR THC04 -*                  
              03 COMM-HC04-PROG       REDEFINES COMM-HC00-PROG.                 
      *..............................................FONCTION                   
                 04 COMM-HC04-FONC           PIC X(1).                          
                    88 COMM-HC04-CRE         VALUE '0'.                         
                    88 COMM-HC04-MAJ         VALUE '1'.                         
      *..............................................SOLDE OU NON               
                 04 COMM-HC04-WSOLDE         PIC X(1).                          
                    88 COMM-HC04-NON-SOLDE   VALUE '0'.                         
                    88 COMM-HC04-SOLDE       VALUE '1'.                         
      *..............................................PF11 EFFECTUE              
                 04 COMM-HC04-PF11           PIC X(1).                          
                    88 COMM-HC04-NON-PF11    VALUE '0'.                         
                    88 COMM-HC04-OUI-PF11    VALUE '1'.                         
      *..............................................SAISIE 1 OU 2              
                 04 COMM-HC04-WSAISIE        PIC X(1).                          
                    88 COMM-HC04-SAISIE1     VALUE '0'.                         
                    88 COMM-HC04-SAISIE2     VALUE '1'.                         
      *..............................................ENVOYE ?                   
                 04 COMM-HC04-WENVOI         PIC X(1).                          
                    88 COMM-HC04-NON-ENVOYE  VALUE '0'.                         
                    88 COMM-HC04-ENVOYE      VALUE '1'.                         
      *..............................................VALIDATION 1 OU 2          
                 04 COMM-HC04-VALIDATION     PIC X(1).                          
                    88 COMM-HC04-VALIDATION-1  VALUE '0'.                       
                    88 COMM-HC04-VALIDATION-2  VALUE '1'.                       
      *..........................................DATE DE DEPOT                  
                 04 COMM-HC04-DDEPOT-PREC    PIC X(8).                          
      *..........................................CORIGINE                       
                 04 COMM-HC04-CORIGINE-PREC  PIC X(7).                          
                 04 COMM-HC04-CORIGINE-NEW   PIC X(7).                          
      *............................INDICE DE TABLE-TS OU SE TROUVE NCHS         
                 04 COMM-HC04-TS-I           PIC S999.                          
      *......................................DONNEES DE LA GEF                  
                 04 COMM-HC04-EF00WSOLD      PIC X(01).                         
                 04 COMM-HC04-NRENDU         PIC X(20).                         
                 04 COMM-HC04-DRENDU         PIC X(08).                         
                 04 COMM-HC04-CRENDU         PIC X(05).                         
      *..............................................NSEQ MAX DE RTHC03         
                 04 COMM-HC04-NSEQ           PIC XXX.                           
                 04 COMM-HC04-NSEQ-9 REDEFINES COMM-HC04-NSEQ                   
                                             PIC 999.                           
      *.....................................SI PF11, SAVE POUR RTHC03           
                 04 COMM-HC04-NCODIC         PIC X(7).                          
                 04 COMM-HC04-NENVOI         PIC X(7).                          
                 04 COMM-HC04-DDEPOT         PIC X(8).                          
                 04 COMM-HC04-CPROVORIG      PIC X(1).                          
                 04 COMM-HC04-CORIGINE       PIC X(7).                          
                 04 COMM-HC04-CSERIE         PIC X(16).                         
                 04 COMM-HC04-DACCFOUR       PIC X(8).                          
                 04 COMM-HC04-NACCORD        PIC X(12).                         
                 04 COMM-HC04-INTERLOC       PIC X(10).                         
                 04 COMM-HC04-DSOLDE         PIC X(8).                          
                 04 COMM-HC04-CLIENT         PIC X(20).                         
      *--------------------------------------------- TR THC05 ---------*        
              03 COMM-HC05-PROG       REDEFINES COMM-HC00-PROG.                 
      *                                                                         
                 04 COMM-HC05-CSERIE            PIC X(16).                      
      *..............................................SOLDE OU NON               
                 04 COMM-HC05-WSOLDE         PIC X(1).                          
                    88 COMM-HC05-NON-SOLDE   VALUE '0'.                         
                    88 COMM-HC05-SOLDE       VALUE '1'.                         
      *..............................................SAISIE 1 OU 2              
                 04 COMM-HC05-WSAISIE        PIC X(1).                          
                    88 COMM-HC05-SAISIE1     VALUE '0'.                         
                    88 COMM-HC05-SAISIE2     VALUE '1'.                         
      *..............................................ENVOYE ?                   
                 04 COMM-HC05-WENVOI         PIC X(1).                          
                    88 COMM-HC05-NON-ENVOYE  VALUE '0'.                         
                    88 COMM-HC05-ENVOYE      VALUE '1'.                         
      *............................INDICE DE TABLE-TS OU SE TROUVE NCHS         
                 04 COMM-HC05-TS-I           PIC S999.                          
      *                                                                         
                 04 COMM-HC05-CMAJADR           PIC X(1).                       
                 04 COMM-HC05-ADRESSE           PIC X(8).                       
      *--------------------------------------------- TR THC06 ---------*        
              03 COMM-HC06-PROG       REDEFINES COMM-HC00-PROG.                 
      *..............................................SAISIE 1 OU 2              
                 04 COMM-HC06-WSAISIE        PIC X(1).                          
                    88 COMM-HC06-SAISIE1     VALUE '0'.                         
                    88 COMM-HC06-SAISIE2     VALUE '1'.                         
      *..............................................SAISIE 1 OU 2              
                 04 COMM-HC06-TABLE-STA.                                        
                    05 COMM-HC06-LIGNE  OCCURS 50.                              
                       10 COMM-HC06-STATUT            PIC X(1).                 
                       10 COMM-HC06-CONTROLE.                                   
      *STATUT ATTENTE STATUT POUR TRANSFERT AAF -> 020                          
                          15 FILLER                   PIC X(1).                 
      *STATUT ENVOYABLE                                                         
                          15 COMM-HC06-CONT-ENV       PIC X(1).                 
      *STATUT NUMERO ACCORD OBLIGATOIRE                                         
                          15 COMM-HC06-CONT-ACC       PIC X(1).                 
      *STATUT DATE DE FIN INTERVENTION OBLIGATOIRE                              
                          15 COMM-HC06-CONT-DFI       PIC X(1).                 
      *4 TESTS DE LIBRE                                                         
                          15 FILLER                   PIC X(4).                 
      *ZONES D'ENTETES SAISIES DANS CAS DU SAISIE1                              
                 04 COMM-HC06-ENTETE.                                           
                    05 COMM-HC06-DENVOI         PIC X(08).                      
                    05 COMM-HC06-CTENVOI        PIC X(1).                       
                    05 COMM-HC06-NSOCIETE       PIC X(3).                       
                    05 COMM-HC06-NLIEU          PIC X(3).                       
                    05 COMM-HC06-NSSLIEU        PIC X(3).                       
                    05 COMM-HC06-CLIEUTRT       PIC X(5).                       
                    05 COMM-HC06-LLIEU          PIC X(20).                      
                    05 COMM-HC06-CTIERS         PIC X(5).                       
                    05 COMM-HC06-LTIERS         PIC X(20).                      
                    05 COMM-HC06-CRENDU         PIC X(5).                       
                    05 COMM-HC06-LRENDU         PIC X(20).                      
                    05 COMM-HC06-WTYPTIERS      PIC X(1).                       
                    05 COMM-HC06-LADR1          PIC X(32).                      
                    05 COMM-HC06-LADR2          PIC X(32).                      
                    05 COMM-HC06-CPOSTAL        PIC X(05).                      
                    05 COMM-HC06-LCOMMUNE       PIC X(26).                      
      *--------------------------------------------- TR THC08 ---------*        
              03 COMM-HC08-PROG       REDEFINES COMM-HC00-PROG.                 
      *                                                                         
                 04 COMM-HC08-NENVOI         PIC X(7).                        00
                 04 COMM-HC08-CTIERS         PIC X(5).                        00
                 04 COMM-HC08-DENVOI         PIC X(08).                       00
                 04 COMM-HC08-CTENVOI        PIC X(1).                        00
                 04 COMM-HC08-CRENDU         PIC X(5).                        00
                 04 COMM-HC08-NSOCIETE       PIC X(3).                        00
                 04 COMM-HC08-NLIEU          PIC X(3).                        00
                 04 COMM-HC08-NSSLIEU        PIC X(3).                        00
                 04 COMM-HC08-CLIEUTRT       PIC X(5).                        00
                 04 COMM-HC08-CTRANS         PIC X(5).                        00
                 04 COMM-HC08-LTRANS         PIC X(20).                       00
                 04 COMM-HC08-WTYPTIERS      PIC X(1).                        00
                 04 COMM-HC08-NENTCDE        PIC X(05).                       00
      *--------------------------------------------- TR THC09 ---------*        
              03 COMM-HC09-PROG       REDEFINES COMM-HC00-PROG.                 
      *                                                                         
                 04 COMM-HC09-NLIEUTR        PIC X(3).                        00
                 04 COMM-HC09-LLIEUTR        PIC X(20).                       00
                 04 COMM-HC09-NSOCIETE       PIC X(3).                        00
                 04 COMM-HC09-NLIEU          PIC X(3).                        00
                 04 COMM-HC09-NSSLIEU        PIC X(3).                        00
                 04 COMM-HC09-CLIEUTRT       PIC X(5).                        00
                 04 COMM-HC09-CSTATUT        PIC X(5).                        00
                 04 COMM-HC09-LSTATUT        PIC X(20).                       00
                 04 COMM-HC09-WCMOTIF        PIC X(10).                       00
      *                                                                         
                                                                                
