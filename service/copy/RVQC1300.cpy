      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVQC1300                      *        
      ******************************************************************        
       01  RVQC1300.                                                            
      *                       CTQUEST                                           
           10 QC13-CTQUEST         PIC S9(2)V USAGE COMP-3.                     
      *                       NCARTE                                            
           10 QC13-NCARTE          PIC X(15).                                   
      *                       CQUESTION                                         
           10 QC13-CQUESTION       PIC S9(3)V USAGE COMP-3.                     
      *                       CREPONSE                                          
           10 QC13-CREPONSE        PIC S9(3)V USAGE COMP-3.                     
      *                       DCREATION                                         
           10 QC13-DCREATION       PIC X(8).                                    
      *                       DENVOI_QV                                         
           10 QC13-DENVOI-QV       PIC X(8).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
