      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBTRE CODE TYPE RESEAU                 *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBTRE.                                                             
           05  RBTRE-CTABLEG2    PIC X(15).                                     
           05  RBTRE-CTABLEG2-REDEF REDEFINES RBTRE-CTABLEG2.                   
               10  RBTRE-CTYPRES         PIC X(03).                             
           05  RBTRE-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBTRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBTRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBTRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBTRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBTRE-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
