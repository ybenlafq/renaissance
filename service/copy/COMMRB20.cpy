      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *01  COMM-RB20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-RB20-ENTETE.                                         00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION  .                               00000540
                    10 COMM-CODE-FONCTION      OCCURS 3.                   00000
                       15  COMM-CFONC     PIC  X(03).                           
                 05 COMM-CFONCTION-OPTION   PIC X(03).                          
                 05 COMM-COPTION            PIC X(02).                          
                 05 COMM-ACID               PIC X(08).                  00000520
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ---------3874-26= 3848 00000480
           02 COMM-RB20-APPLI.                                          00000500
                 05 COMM-20-DATFIN          PIC X(10).                  00000520
                 05 COMM-20-DATDEB          PIC X(10).                  00000520
                 05 COMM-20-NSOC            PIC X(03).                  00000620
                 05 COMM-20-NLIEU           PIC X(03).                  00000620
                 05 COMM-20-ENTREPOT        PIC X(01).                  00000620
                    88 COMM-F-ENTREPOT      VALUE '1'.                          
                 05 COMM-20-FILLER          PIC X(261).                 00000620
           02 COMM-RB20-FILLER              PIC X(3560).                00000620
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
      *--  CONSULT DES RECEPTION ATTENDUES                              00000390
           02 COMM-RB21-APPLI REDEFINES COMM-RB20-FILLER.               00000040
              03 COMM-21.                                               00000050
               04 COMM-21-DATA.                                         00000550
                 05 COMM-21-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-21-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-21-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-23-DETAIL   .                                  00000070
                    10 COMM-23-ENTETE .                                    00000
                       15 COMM-23-NBL             PIC X(10).                  00
                       15 COMM-23-NFOURNISSEUR    PIC X(25).                  00
                       15 COMM-23-DRECEPTION      PIC X(10).                  00
                       15 COMM-23-DENVOICT        PIC X(10).                  00
                       15 COMM-23-DENVOIRX        PIC X(10).                  00
                       15 COMM-23-DRETOURCT       PIC X(10).                  00
                       15 COMM-23-DRETOURRX       PIC X(10).                  00
                       15 COMM-23-NRECEPTION PIC S9(8)V USAGE COMP-3.         00
                    10 COMM-23-DATA.                                          00
                       15 COMM-23-INDTS           PIC S9(5) COMP-3.           00
                       15 COMM-23-INDMAX          PIC S9(5) COMP-3.           00
                       15 COMM-23-NLIGNE          PIC S9(5) COMP-3.           00
                    10 COMM-23-NTRA      PIC X(4).                      00000550
                    10 COMM-23-NPROG-MAX PIC X(06).                     00000550
                    10 COMM-23-FILLER PIC X(442).                       00000550
                 05 COMM-21-FILLER PIC X(3000).                         00000550
                                                                        00000550
      *--  CONSULT DES RECEPTION EFFECTUEE                              00000390
      *                                                                 00000390
           02 COMM-RB24-APPLI REDEFINES COMM-RB20-FILLER.               00000040
              03 COMM-24.                                               00000050
               04 COMM-24-ACID            PIC X(01).                    000     
                  88 COMM-F-ACID-OK       VALUE '1'.                            
               04 COMM-24-DATA.                                         00000550
                 05 COMM-24-NBL             PIC X(10).                  00000070
                 05 COMM-24-DRECEPTION      PIC X(10).                  00000080
                 05 COMM-24-NFOURNISSEUR    PIC X(25).                  00000070
                 05 COMM-24-NCOMMANDE       PIC X(07).                  00000550
                 05 COMM-24-NBPRODUIT       PIC X(25).                  00000070
                 05 COMM-24-NBLOT           PIC X(05).                  00000070
                 05 COMM-24-NBLOT-RECEPT    PIC X(05).                  00000070
                 05 COMM-24-NBLOT-ACCEPT    PIC X(05).                  00000070
                 05 COMM-24-NBLOT-REFUSE    PIC X(05).                  00000070
                 05 COMM-24-NBLOT-RESTANT   PIC X(05).                  00000070
                 05 COMM-24-WRECEPTION      PIC X(05).                  00000070
                 05 COMM-24-NRECEPTION PIC S9(8)V USAGE COMP-3.               00
                 05 COMM-24-MLIBENTITE      PIC X(15).                  00000550
                 05 COMM-24-FILLER PIC X(432).                          00000550
                 05 COMM-25-DATA.                                       00000550
                    10 COMM-25-INDTS           PIC S9(5) COMP-3.           00000
                    10 COMM-25-INDMAX          PIC S9(5) COMP-3.           00000
                    10 COMM-25-NLIGNE          PIC S9(5) COMP-3.           00000
                    10 COMM-25-FILLER PIC X(491).                          00000
                 05 COMM-25-FILLER PIC X(2500).                         00000   
                                                                                
                                                                        00000550
