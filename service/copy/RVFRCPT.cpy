      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FRCPT PARAM ENCAISSEMENT POUR COMPTE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFRCPT .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFRCPT .                                                            
      *}                                                                        
           05  FRCPT-CTABLEG2    PIC X(15).                                     
           05  FRCPT-CTABLEG2-REDEF REDEFINES FRCPT-CTABLEG2.                   
               10  FRCPT-GV              PIC X(04).                             
               10  FRCPT-CODE            PIC X(05).                             
           05  FRCPT-WTABLEG     PIC X(80).                                     
           05  FRCPT-WTABLEG-REDEF  REDEFINES FRCPT-WTABLEG.                    
               10  FRCPT-WFLAG           PIC X(01).                             
               10  FRCPT-COMMENT         PIC X(15).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFRCPT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFRCPT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FRCPT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FRCPT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FRCPT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FRCPT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
