      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESP07   ESP07                                              00000020
      ***************************************************************** 00000030
       01   ESP91I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTOTPAGEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNTOTPAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNTOTPAGEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNTOTPAGEI     PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCTIONL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCFONCTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCFONCTIONF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFONCTIONI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(20).                                 00000410
           02 FILLER  OCCURS   14 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNSOCIETEI   PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNLIEUI      PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZONPRIXL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNZONPRIXL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNZONPRIXF   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNZONPRIXI   PIC X(2).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLLIEUI      PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEDEBL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDATEDEBL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEDEBF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATEDEBI    PIC X(10).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEFINL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDATEFINL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEFINF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDATEFINI    PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MSF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSI     PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDOUBLEL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDOUBLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDOUBLEF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDOUBLEI     PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(79).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: ESP07   ESP07                                              00000960
      ***************************************************************** 00000970
       01   ESP91O REDEFINES ESP91I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNPAGEA   PIC X.                                          00001150
           02 MNPAGEC   PIC X.                                          00001160
           02 MNPAGEP   PIC X.                                          00001170
           02 MNPAGEH   PIC X.                                          00001180
           02 MNPAGEV   PIC X.                                          00001190
           02 MNPAGEO   PIC X(2).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNTOTPAGEA     PIC X.                                     00001220
           02 MNTOTPAGEC     PIC X.                                     00001230
           02 MNTOTPAGEP     PIC X.                                     00001240
           02 MNTOTPAGEH     PIC X.                                     00001250
           02 MNTOTPAGEV     PIC X.                                     00001260
           02 MNTOTPAGEO     PIC X(2).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCFONCTIONA    PIC X.                                     00001290
           02 MCFONCTIONC    PIC X.                                     00001300
           02 MCFONCTIONP    PIC X.                                     00001310
           02 MCFONCTIONH    PIC X.                                     00001320
           02 MCFONCTIONV    PIC X.                                     00001330
           02 MCFONCTIONO    PIC X(3).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNCODICA  PIC X.                                          00001360
           02 MNCODICC  PIC X.                                          00001370
           02 MNCODICP  PIC X.                                          00001380
           02 MNCODICH  PIC X.                                          00001390
           02 MNCODICV  PIC X.                                          00001400
           02 MNCODICO  PIC X(7).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCFAMA    PIC X.                                          00001430
           02 MCFAMC    PIC X.                                          00001440
           02 MCFAMP    PIC X.                                          00001450
           02 MCFAMH    PIC X.                                          00001460
           02 MCFAMV    PIC X.                                          00001470
           02 MCFAMO    PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCMARQA   PIC X.                                          00001500
           02 MCMARQC   PIC X.                                          00001510
           02 MCMARQP   PIC X.                                          00001520
           02 MCMARQH   PIC X.                                          00001530
           02 MCMARQV   PIC X.                                          00001540
           02 MCMARQO   PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLREFFOURNA    PIC X.                                     00001570
           02 MLREFFOURNC    PIC X.                                     00001580
           02 MLREFFOURNP    PIC X.                                     00001590
           02 MLREFFOURNH    PIC X.                                     00001600
           02 MLREFFOURNV    PIC X.                                     00001610
           02 MLREFFOURNO    PIC X(20).                                 00001620
           02 FILLER  OCCURS   14 TIMES .                               00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MNSOCIETEA   PIC X.                                     00001650
             03 MNSOCIETEC   PIC X.                                     00001660
             03 MNSOCIETEP   PIC X.                                     00001670
             03 MNSOCIETEH   PIC X.                                     00001680
             03 MNSOCIETEV   PIC X.                                     00001690
             03 MNSOCIETEO   PIC X(3).                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MNLIEUA      PIC X.                                     00001720
             03 MNLIEUC PIC X.                                          00001730
             03 MNLIEUP PIC X.                                          00001740
             03 MNLIEUH PIC X.                                          00001750
             03 MNLIEUV PIC X.                                          00001760
             03 MNLIEUO      PIC X(3).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MNZONPRIXA   PIC X.                                     00001790
             03 MNZONPRIXC   PIC X.                                     00001800
             03 MNZONPRIXP   PIC X.                                     00001810
             03 MNZONPRIXH   PIC X.                                     00001820
             03 MNZONPRIXV   PIC X.                                     00001830
             03 MNZONPRIXO   PIC X(2).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MLLIEUA      PIC X.                                     00001860
             03 MLLIEUC PIC X.                                          00001870
             03 MLLIEUP PIC X.                                          00001880
             03 MLLIEUH PIC X.                                          00001890
             03 MLLIEUV PIC X.                                          00001900
             03 MLLIEUO      PIC X(20).                                 00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MDATEDEBA    PIC X.                                     00001930
             03 MDATEDEBC    PIC X.                                     00001940
             03 MDATEDEBP    PIC X.                                     00001950
             03 MDATEDEBH    PIC X.                                     00001960
             03 MDATEDEBV    PIC X.                                     00001970
             03 MDATEDEBO    PIC X(10).                                 00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MDATEFINA    PIC X.                                     00002000
             03 MDATEFINC    PIC X.                                     00002010
             03 MDATEFINP    PIC X.                                     00002020
             03 MDATEFINH    PIC X.                                     00002030
             03 MDATEFINV    PIC X.                                     00002040
             03 MDATEFINO    PIC X(10).                                 00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MSA     PIC X.                                          00002070
             03 MSC     PIC X.                                          00002080
             03 MSP     PIC X.                                          00002090
             03 MSH     PIC X.                                          00002100
             03 MSV     PIC X.                                          00002110
             03 MSO     PIC X.                                          00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MDOUBLEA     PIC X.                                     00002140
             03 MDOUBLEC     PIC X.                                     00002150
             03 MDOUBLEP     PIC X.                                     00002160
             03 MDOUBLEH     PIC X.                                     00002170
             03 MDOUBLEV     PIC X.                                     00002180
             03 MDOUBLEO     PIC X(2).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(79).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
