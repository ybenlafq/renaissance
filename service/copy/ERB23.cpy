      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: consult detail eqpment                                     00000020
      ***************************************************************** 00000030
       01   ERB23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBLF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBLI     PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNFOURNI  PIC X(25).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDRECEPTI      PIC X(10).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVCTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDENVCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVCTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDENVCTI  PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENTRXL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDENTRXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENTRXF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDENTRXI  PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRETCTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDRETCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRETCTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDRETCTI  PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRETRXL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDRETRXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRETRXF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDRETRXI  PIC X(10).                                      00000490
           02 MTABLGRPI OCCURS   10 TIMES .                             00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCODICI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCMARQI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLREFI  PIC X(20).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSERIEL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNSERIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSERIEF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSERIEI     PIC X(15).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATUTL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLSTATUTF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLSTATUTI    PIC X(22).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(78).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * Rbx: consult detail eqpment                                     00000960
      ***************************************************************** 00000970
       01   ERB23O REDEFINES ERB23I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 NPAGEA    PIC X.                                          00001150
           02 NPAGEC    PIC X.                                          00001160
           02 NPAGEP    PIC X.                                          00001170
           02 NPAGEH    PIC X.                                          00001180
           02 NPAGEV    PIC X.                                          00001190
           02 NPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 NBPAGEA   PIC X.                                          00001220
           02 NBPAGEC   PIC X.                                          00001230
           02 NBPAGEP   PIC X.                                          00001240
           02 NBPAGEH   PIC X.                                          00001250
           02 NBPAGEV   PIC X.                                          00001260
           02 NBPAGEO   PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNBLA     PIC X.                                          00001290
           02 MNBLC     PIC X.                                          00001300
           02 MNBLP     PIC X.                                          00001310
           02 MNBLH     PIC X.                                          00001320
           02 MNBLV     PIC X.                                          00001330
           02 MNBLO     PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNFOURNA  PIC X.                                          00001360
           02 MNFOURNC  PIC X.                                          00001370
           02 MNFOURNP  PIC X.                                          00001380
           02 MNFOURNH  PIC X.                                          00001390
           02 MNFOURNV  PIC X.                                          00001400
           02 MNFOURNO  PIC X(25).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MDRECEPTA      PIC X.                                     00001430
           02 MDRECEPTC PIC X.                                          00001440
           02 MDRECEPTP PIC X.                                          00001450
           02 MDRECEPTH PIC X.                                          00001460
           02 MDRECEPTV PIC X.                                          00001470
           02 MDRECEPTO      PIC X(10).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDENVCTA  PIC X.                                          00001500
           02 MDENVCTC  PIC X.                                          00001510
           02 MDENVCTP  PIC X.                                          00001520
           02 MDENVCTH  PIC X.                                          00001530
           02 MDENVCTV  PIC X.                                          00001540
           02 MDENVCTO  PIC X(10).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDENTRXA  PIC X.                                          00001570
           02 MDENTRXC  PIC X.                                          00001580
           02 MDENTRXP  PIC X.                                          00001590
           02 MDENTRXH  PIC X.                                          00001600
           02 MDENTRXV  PIC X.                                          00001610
           02 MDENTRXO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MDRETCTA  PIC X.                                          00001640
           02 MDRETCTC  PIC X.                                          00001650
           02 MDRETCTP  PIC X.                                          00001660
           02 MDRETCTH  PIC X.                                          00001670
           02 MDRETCTV  PIC X.                                          00001680
           02 MDRETCTO  PIC X(10).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MDRETRXA  PIC X.                                          00001710
           02 MDRETRXC  PIC X.                                          00001720
           02 MDRETRXP  PIC X.                                          00001730
           02 MDRETRXH  PIC X.                                          00001740
           02 MDRETRXV  PIC X.                                          00001750
           02 MDRETRXO  PIC X(10).                                      00001760
           02 MTABLGRPO OCCURS   10 TIMES .                             00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MNCODICA     PIC X.                                     00001790
             03 MNCODICC     PIC X.                                     00001800
             03 MNCODICP     PIC X.                                     00001810
             03 MNCODICH     PIC X.                                     00001820
             03 MNCODICV     PIC X.                                     00001830
             03 MNCODICO     PIC X(7).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MCFAMA  PIC X.                                          00001860
             03 MCFAMC  PIC X.                                          00001870
             03 MCFAMP  PIC X.                                          00001880
             03 MCFAMH  PIC X.                                          00001890
             03 MCFAMV  PIC X.                                          00001900
             03 MCFAMO  PIC X(5).                                       00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MCMARQA      PIC X.                                     00001930
             03 MCMARQC PIC X.                                          00001940
             03 MCMARQP PIC X.                                          00001950
             03 MCMARQH PIC X.                                          00001960
             03 MCMARQV PIC X.                                          00001970
             03 MCMARQO      PIC X(5).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MLREFA  PIC X.                                          00002000
             03 MLREFC  PIC X.                                          00002010
             03 MLREFP  PIC X.                                          00002020
             03 MLREFH  PIC X.                                          00002030
             03 MLREFV  PIC X.                                          00002040
             03 MLREFO  PIC X(20).                                      00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MNSERIEA     PIC X.                                     00002070
             03 MNSERIEC     PIC X.                                     00002080
             03 MNSERIEP     PIC X.                                     00002090
             03 MNSERIEH     PIC X.                                     00002100
             03 MNSERIEV     PIC X.                                     00002110
             03 MNSERIEO     PIC X(15).                                 00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MLSTATUTA    PIC X.                                     00002140
             03 MLSTATUTC    PIC X.                                     00002150
             03 MLSTATUTP    PIC X.                                     00002160
             03 MLSTATUTH    PIC X.                                     00002170
             03 MLSTATUTV    PIC X.                                     00002180
             03 MLSTATUTO    PIC X(22).                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(78).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
