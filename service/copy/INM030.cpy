      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM030 AU 01/07/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM030.                                                        
            05 NOMETAT-INM030           PIC X(6) VALUE 'INM030'.                
            05 RUPTURES-INM030.                                                 
           10 INM030-DATTRANS           PIC X(08).                      007  008
           10 INM030-CMOPAI             PIC X(05).                      015  005
           10 INM030-MAG                PIC X(03).                      020  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM030-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 INM030-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM030.                                                   
           10 INM030-CBQ1               PIC X(03).                      025  003
           10 INM030-CBQ2               PIC X(03).                      028  003
           10 INM030-CBQ3               PIC X(03).                      031  003
           10 INM030-CDEVAUT            PIC X(05).                      034  005
           10 INM030-CDEVREF            PIC X(05).                      039  005
           10 INM030-LDEVREF            PIC X(25).                      044  025
           10 INM030-LLIEU              PIC X(20).                      069  020
           10 INM030-LMOPAI             PIC X(15).                      089  015
           10 INM030-NLIEU              PIC X(03).                      104  003
           10 INM030-NSOC               PIC X(03).                      107  003
           10 INM030-MONT-AU-BQ1        PIC 9(07)V9(2).                 110  009
           10 INM030-MONT-AU-BQ2        PIC 9(07)V9(2).                 119  009
           10 INM030-MONT-AU-BQ3        PIC 9(07)V9(2).                 128  009
           10 INM030-MONT-AU-CONV-BQ1   PIC 9(08)V9(2).                 137  010
           10 INM030-MONT-AU-CONV-BQ2   PIC 9(08)V9(2).                 147  010
           10 INM030-MONT-AU-CONV-BQ3   PIC 9(08)V9(2).                 157  010
           10 INM030-MONT-RF-BQ1        PIC 9(07)V9(2).                 167  009
           10 INM030-MONT-RF-BQ2        PIC 9(07)V9(2).                 176  009
           10 INM030-MONT-RF-BQ3        PIC 9(07)V9(2).                 185  009
           10 INM030-MONT-RF-CONV-BQ1   PIC 9(08)V9(2).                 194  010
           10 INM030-MONT-RF-CONV-BQ2   PIC 9(08)V9(2).                 204  010
           10 INM030-MONT-RF-CONV-BQ3   PIC 9(08)V9(2).                 214  010
           10 INM030-DATETR             PIC X(08).                      224  008
            05 FILLER                      PIC X(281).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM030-LONG           PIC S9(4)   COMP  VALUE +231.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM030-LONG           PIC S9(4) COMP-5  VALUE +231.           
                                                                                
      *}                                                                        
