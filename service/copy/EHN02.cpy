      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHN02   EHN02                                              00000020
      ***************************************************************** 00000030
       01   EHN02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMPGML  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNOMPGML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMPGMF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNOMPGMI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTIERSF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNTIERSI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLTIERSI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNENTCDEI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLENTCDEI      PIC X(20).                                 00000410
           02 MLIGNEI OCCURS   12 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MMRQI   PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MFAMI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCODI   PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRIL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MTRIL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTRIF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTRII   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRDUL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MRDUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MRDUF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MRDUI   PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MWACL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWACF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWACI   PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSRL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MWSRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWSRF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MWSRI   PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUPPRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLSUPPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSUPPRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLSUPPRI  PIC X(60).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EHN02   EHN02                                              00000960
      ***************************************************************** 00000970
       01   EHN02O REDEFINES EHN02I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNUMPAGEA      PIC X.                                     00001150
           02 MNUMPAGEC PIC X.                                          00001160
           02 MNUMPAGEP PIC X.                                          00001170
           02 MNUMPAGEH PIC X.                                          00001180
           02 MNUMPAGEV PIC X.                                          00001190
           02 MNUMPAGEO      PIC 99.                                    00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC 99.                                    00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNOMPGMA  PIC X.                                          00001290
           02 MNOMPGMC  PIC X.                                          00001300
           02 MNOMPGMP  PIC X.                                          00001310
           02 MNOMPGMH  PIC X.                                          00001320
           02 MNOMPGMV  PIC X.                                          00001330
           02 MNOMPGMO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNTIERSA  PIC X.                                          00001360
           02 MNTIERSC  PIC X.                                          00001370
           02 MNTIERSP  PIC X.                                          00001380
           02 MNTIERSH  PIC X.                                          00001390
           02 MNTIERSV  PIC X.                                          00001400
           02 MNTIERSO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLTIERSA  PIC X.                                          00001430
           02 MLTIERSC  PIC X.                                          00001440
           02 MLTIERSP  PIC X.                                          00001450
           02 MLTIERSH  PIC X.                                          00001460
           02 MLTIERSV  PIC X.                                          00001470
           02 MLTIERSO  PIC X(20).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNENTCDEA      PIC X.                                     00001500
           02 MNENTCDEC PIC X.                                          00001510
           02 MNENTCDEP PIC X.                                          00001520
           02 MNENTCDEH PIC X.                                          00001530
           02 MNENTCDEV PIC X.                                          00001540
           02 MNENTCDEO      PIC X(5).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLENTCDEA      PIC X.                                     00001570
           02 MLENTCDEC PIC X.                                          00001580
           02 MLENTCDEP PIC X.                                          00001590
           02 MLENTCDEH PIC X.                                          00001600
           02 MLENTCDEV PIC X.                                          00001610
           02 MLENTCDEO      PIC X(20).                                 00001620
           02 MLIGNEO OCCURS   12 TIMES .                               00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MMRQA   PIC X.                                          00001650
             03 MMRQC   PIC X.                                          00001660
             03 MMRQP   PIC X.                                          00001670
             03 MMRQH   PIC X.                                          00001680
             03 MMRQV   PIC X.                                          00001690
             03 MMRQO   PIC X(5).                                       00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MFAMA   PIC X.                                          00001720
             03 MFAMC   PIC X.                                          00001730
             03 MFAMP   PIC X.                                          00001740
             03 MFAMH   PIC X.                                          00001750
             03 MFAMV   PIC X.                                          00001760
             03 MFAMO   PIC X(5).                                       00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MCODA   PIC X.                                          00001790
             03 MCODC   PIC X.                                          00001800
             03 MCODP   PIC X.                                          00001810
             03 MCODH   PIC X.                                          00001820
             03 MCODV   PIC X.                                          00001830
             03 MCODO   PIC X(7).                                       00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MTRIA   PIC X.                                          00001860
             03 MTRIC   PIC X.                                          00001870
             03 MTRIP   PIC X.                                          00001880
             03 MTRIH   PIC X.                                          00001890
             03 MTRIV   PIC X.                                          00001900
             03 MTRIO   PIC X.                                          00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MRDUA   PIC X.                                          00001930
             03 MRDUC   PIC X.                                          00001940
             03 MRDUP   PIC X.                                          00001950
             03 MRDUH   PIC X.                                          00001960
             03 MRDUV   PIC X.                                          00001970
             03 MRDUO   PIC X(5).                                       00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MWACA   PIC X.                                          00002000
             03 MWACC   PIC X.                                          00002010
             03 MWACP   PIC X.                                          00002020
             03 MWACH   PIC X.                                          00002030
             03 MWACV   PIC X.                                          00002040
             03 MWACO   PIC X.                                          00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MWSRA   PIC X.                                          00002070
             03 MWSRC   PIC X.                                          00002080
             03 MWSRP   PIC X.                                          00002090
             03 MWSRH   PIC X.                                          00002100
             03 MWSRV   PIC X.                                          00002110
             03 MWSRO   PIC X.                                          00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIBERRA  PIC X.                                          00002140
           02 MLIBERRC  PIC X.                                          00002150
           02 MLIBERRP  PIC X.                                          00002160
           02 MLIBERRH  PIC X.                                          00002170
           02 MLIBERRV  PIC X.                                          00002180
           02 MLIBERRO  PIC X(78).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLSUPPRA  PIC X.                                          00002210
           02 MLSUPPRC  PIC X.                                          00002220
           02 MLSUPPRP  PIC X.                                          00002230
           02 MLSUPPRH  PIC X.                                          00002240
           02 MLSUPPRV  PIC X.                                          00002250
           02 MLSUPPRO  PIC X(60).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
