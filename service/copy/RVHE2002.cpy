      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVHE2002                    *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVHE2002.                                                            
      *                       NLIEUHED                                          
           10 HE20-NLIEUHED        PIC X(3).                                    
      *                       CLIEUHET                                          
           10 HE20-CLIEUHET        PIC X(5).                                    
      *                       NRETOUR                                           
           10 HE20-NRETOUR         PIC X(7).                                    
      *                       DRETOUR                                           
           10 HE20-DRETOUR         PIC X(8).                                    
      *                       QRETOUR                                           
           10 HE20-QRETOUR         PIC S9(7)V USAGE COMP-3.                     
      *                       WFLAG                                             
           10 HE20-WFLAG           PIC X(1).                                    
      *                       DSYST                                             
           10 HE20-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DRECEPTION                                        
           10 HE20-DRECEPTION      PIC X(8).                                    
      *                       NUMPAL                                            
           10 HE20-NUMPAL          PIC X(17).                                   
       EXEC SQL END DECLARE SECTION END-EXEC.
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
