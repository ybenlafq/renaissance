      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVBB0100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBB0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBB0100.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 BB01-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 BB01-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 BB01-NVENTE          PIC X(7).                                    
      *                       IDFAC                                             
           10 BB01-IDFAC           PIC X(5).                                    
      *                       IDCLIENT                                          
           10 BB01-IDCLIENT        PIC X(8).                                    
      *                       NODOS                                             
           10 BB01-NODOS           PIC X(12).                                   
      *                       IBMSNAP_LOGMARKER                                 
           10 BB01-IBMSNAP-LOGMARKER                                            
              PIC X(26).                                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
                                                                                
