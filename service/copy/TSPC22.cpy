      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-PC22-RECORD.                                                      
           05 TS-PC22-TABLE            OCCURS 4.                                
              10 TS-PC22-CCARTE                PIC X(07).                       
              10 TS-PC22-NSERIE-DEB            PIC X(20).                       
              10 TS-PC22-NCOMMANDE             PIC X(12).                       
              10 TS-PC22-LIBELLE               PIC X(20).                       
              10 TS-PC22-REFLOGO               PIC X(20).                       
              10 TS-PC22-PUNIT                 PIC X(09).                       
              10 TS-PC22-QTE                   PIC X(05).                       
              10 TS-PC22-MTHT                  PIC X(08).                       
              10 TS-PC22-MTTVA                 PIC X(08).                       
              10 TS-PC22-MTTTC                 PIC X(09).                       
              10 TS-PC22-FLAG                  PIC X.                           
                                                                                
