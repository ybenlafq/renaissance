      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRB2100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRB2100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRB2100.                                                            
           02  RB21-DTRAITEMENT                                                 
               PIC X(0008).                                                     
           02  RB21-DCREATION                                                   
               PIC X(0008).                                                     
           02  RB21-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RB21-NLIEU                                                       
               PIC X(0003).                                                     
           02  RB21-NVENTE                                                      
               PIC X(0007).                                                     
           02  RB21-CPRESTATION                                                 
               PIC X(0005).                                                     
           02  RB21-DTOPEE                                                      
               PIC X(0008).                                                     
           02  RB21-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  RB21-WSTATUT                                                     
               PIC X(0002).                                                     
           02  RB21-CSTATUT                                                     
               PIC X(0005).                                                     
           02  RB21-DSTATUT                                                     
               PIC X(0008).                                                     
           02  RB21-LSTATUT                                                     
               PIC X(0050).                                                     
           02  RB21-DMAJ                                                        
               PIC X(0008).                                                     
           02  RB21-WPB                                                         
               PIC X(0005).                                                     
           02  RB21-CPRESTATION2                                                
               PIC X(0005).                                                     
           02  RB21-NSEQNQ2                                                     
               PIC S9(5) COMP-3.                                                
           02  RB21-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  RB21-DEFFET                                                      
               PIC X(0008).                                                     
           02  RB21-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRB2105                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRB2100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DTRAITEMENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DTRAITEMENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-CPRESTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DTOPEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DTOPEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-WSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-WSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-LSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-LSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-WPB-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-WPB-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-CPRESTATION2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-CPRESTATION2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NSEQNQ2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NSEQNQ2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB21-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RB21-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
