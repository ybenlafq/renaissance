      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBPNP PROFIL PLUG & PLAY               *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBPNP.                                                             
           05  RBPNP-CTABLEG2    PIC X(15).                                     
           05  RBPNP-CTABLEG2-REDEF REDEFINES RBPNP-CTABLEG2.                   
               10  RBPNP-NLIEU           PIC X(06).                             
           05  RBPNP-WTABLEG     PIC X(80).                                     
           05  RBPNP-WTABLEG-REDEF  REDEFINES RBPNP-WTABLEG.                    
               10  RBPNP-CPROFIL         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBPNP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBPNP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBPNP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBPNP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBPNP-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
