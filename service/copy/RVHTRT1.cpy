      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE HTRT1 HISTORIQUE TRAITEMENT BATCH      *        
      *----------------------------------------------------------------*        
       01  RVHTRT1.                                                             
           05  HTRT1-CTABLEG2    PIC X(15).                                     
           05  HTRT1-CTABLEG2-REDEF REDEFINES HTRT1-CTABLEG2.                   
               10  HTRT1-CAPPLI          PIC X(05).                             
           05  HTRT1-WTABLEG     PIC X(80).                                     
           05  HTRT1-WTABLEG-REDEF  REDEFINES HTRT1-WTABLEG.                    
               10  HTRT1-LAPPLI          PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVHTRT1-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HTRT1-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  HTRT1-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  HTRT1-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  HTRT1-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
