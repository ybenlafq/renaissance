      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FR00-RECORD.                                                      
           05 TS-FR00-TABLE            OCCURS 13.                               
              10 TS-FR00-REGLEF          PIC X(15).                             
              10 TS-FR00-NLIGNE          PIC 9(2).                              
              10 TS-FR00-INTITULE        PIC X(30).                             
              10 TS-FR00-COMMENT         PIC X(25).                             
                                                                                
