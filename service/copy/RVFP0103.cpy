      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFP0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFP0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFP0100.                                                            
           02  FP01-NSOCCOMPT                                                   
               PIC X(0003).                                                     
           02  FP01-NSOC                                                        
               PIC X(0003).                                                     
           02  FP01-NLIEU                                                       
               PIC X(0003).                                                     
           02  FP01-CASSUR                                                      
               PIC X(0005).                                                     
           02  FP01-NFACTURE                                                    
               PIC X(0010).                                                     
           02  FP01-DFACTURE                                                    
               PIC X(0008).                                                     
           02  FP01-NSINISTRE                                                   
               PIC X(0020).                                                     
           02  FP01-DSINISTRE                                                   
               PIC X(0008).                                                     
           02  FP01-NOM                                                         
               PIC X(0025).                                                     
           02  FP01-PRENOM                                                      
               PIC X(0025).                                                     
           02  FP01-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  FP01-REFDOC                                                      
               PIC X(0010).                                                     
           02  FP01-CTYPINTER                                                   
               PIC X(0003).                                                     
           02  FP01-CLIEUINTER                                                  
               PIC X(0003).                                                     
           02  FP01-MTFRANCHISE                                                 
               PIC S9(11)V99 COMP-3.                                            
           02  FP01-WFRANCHISE                                                  
               PIC X(0001).                                                     
           02  FP01-TXREMISE                                                    
               PIC S9(05)V99 COMP-3.                                            
           02  FP01-MTLIVR                                                      
               PIC S9(11)V99 COMP-3.                                            
           02  FP01-MTFORFREP                                                   
               PIC S9(11)V99 COMP-3.                                            
           02  FP01-NDOSSAV                                                     
               PIC X(0010).                                                     
           02  FP01-DANNUL                                                      
               PIC X(0008).                                                     
           02  FP01-DSELECT                                                     
               PIC X(0008).                                                     
           02  FP01-NPRESENT                                                    
               PIC X(0002).                                                     
           02  FP01-NLOT                                                        
               PIC X(0006).                                                     
           02  FP01-DVALID                                                      
               PIC X(0008).                                                     
           02  FP01-WREJET                                                      
               PIC X(0001).                                                     
           02  FP01-DREJET                                                      
               PIC X(0008).                                                     
           02  FP01-CREJET                                                      
               PIC X(0003).                                                     
           02  FP01-LREJET                                                      
               PIC X(0025).                                                     
           02  FP01-DCREAT                                                      
               PIC X(0008).                                                     
           02  FP01-ORIGINE                                                     
               PIC X(0008).                                                     
           02  FP01-DNETTING                                                    
               PIC X(0008).                                                     
           02  FP01-CDEVISE                                                     
               PIC X(0003).                                                     
           02  FP01-DMAJ                                                        
               PIC X(0008).                                                     
           02  FP01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FP01-MTFORFEXP                                                   
               PIC S9(11)V99 COMP-3.                                            
           02  FP01-WDERFACT                                                    
               PIC X(0001).                                                     
           02  FP01-WCARTE                                                      
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFT8900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFP0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSOCCOMPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSOCCOMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CASSUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CASSUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NSINISTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NSINISTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSINISTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSINISTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NOM-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NOM-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-PRENOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-PRENOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-REFDOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-REFDOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CTYPINTER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CTYPINTER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CLIEUINTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CLIEUINTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFRANCHISE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFRANCHISE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WFRANCHISE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WFRANCHISE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-TXREMISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-TXREMISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTLIVR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTLIVR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFORFREP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFORFREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NDOSSAV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NDOSSAV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSELECT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSELECT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NPRESENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NPRESENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-NLOT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-NLOT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DVALID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-LREJET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-LREJET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-ORIGINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-ORIGINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DNETTING-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DNETTING-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-MTFORFEXP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-MTFORFEXP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WDERFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WDERFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FP01-WCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FP01-WCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
