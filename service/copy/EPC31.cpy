      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPC31   EPC31                                              00000020
      ***************************************************************** 00000030
       01   EPC31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEVL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEVF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEVI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRANDOMNUML    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MRANDOMNUML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MRANDOMNUMF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MRANDOMNUMI    PIC X(16).                                 00000250
           02 MTABI OCCURS   5 TIMES .                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNSOCIETEI   PIC X(3).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNLIEUI      PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNVENTEI     PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGUL    COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MPAGUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGUF    PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MPAGUI    PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGDL    COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MPAGDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGDF    PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MPAGDI    PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCTITRENOMI    PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLNOMI    PIC X(25).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLPRENOMI      PIC X(15).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVOIEL   COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVOIEF   PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCVOIEI   PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVOIEL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTVOIEF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCTVOIEI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIEL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVOIEF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLVOIEI   PIC X(21).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCPOSTALI      PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLCOMMUNEI     PIC X(32).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBUREAUL      COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MLBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBUREAUF      PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLBUREAUI      PIC X(26).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(5).                                       00001060
      ***************************************************************** 00001070
      * SDF: EPC31   EPC31                                              00001080
      ***************************************************************** 00001090
       01   EPC31O REDEFINES EPC31I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEVA   PIC X.                                          00001270
           02 MPAGEVC   PIC X.                                          00001280
           02 MPAGEVP   PIC X.                                          00001290
           02 MPAGEVH   PIC X.                                          00001300
           02 MPAGEVV   PIC X.                                          00001310
           02 MPAGEVO   PIC X(2).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMA   PIC X.                                          00001340
           02 MPAGEMC   PIC X.                                          00001350
           02 MPAGEMP   PIC X.                                          00001360
           02 MPAGEMH   PIC X.                                          00001370
           02 MPAGEMV   PIC X.                                          00001380
           02 MPAGEMO   PIC X(2).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MRANDOMNUMA    PIC X.                                     00001410
           02 MRANDOMNUMC    PIC X.                                     00001420
           02 MRANDOMNUMP    PIC X.                                     00001430
           02 MRANDOMNUMH    PIC X.                                     00001440
           02 MRANDOMNUMV    PIC X.                                     00001450
           02 MRANDOMNUMO    PIC X(16).                                 00001460
           02 MTABO OCCURS   5 TIMES .                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MNSOCIETEA   PIC X.                                     00001490
             03 MNSOCIETEC   PIC X.                                     00001500
             03 MNSOCIETEP   PIC X.                                     00001510
             03 MNSOCIETEH   PIC X.                                     00001520
             03 MNSOCIETEV   PIC X.                                     00001530
             03 MNSOCIETEO   PIC X(3).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MNLIEUA      PIC X.                                     00001560
             03 MNLIEUC PIC X.                                          00001570
             03 MNLIEUP PIC X.                                          00001580
             03 MNLIEUH PIC X.                                          00001590
             03 MNLIEUV PIC X.                                          00001600
             03 MNLIEUO      PIC X(3).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNVENTEA     PIC X.                                     00001630
             03 MNVENTEC     PIC X.                                     00001640
             03 MNVENTEP     PIC X.                                     00001650
             03 MNVENTEH     PIC X.                                     00001660
             03 MNVENTEV     PIC X.                                     00001670
             03 MNVENTEO     PIC X(7).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPAGUA    PIC X.                                          00001700
           02 MPAGUC    PIC X.                                          00001710
           02 MPAGUP    PIC X.                                          00001720
           02 MPAGUH    PIC X.                                          00001730
           02 MPAGUV    PIC X.                                          00001740
           02 MPAGUO    PIC X.                                          00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MPAGDA    PIC X.                                          00001770
           02 MPAGDC    PIC X.                                          00001780
           02 MPAGDP    PIC X.                                          00001790
           02 MPAGDH    PIC X.                                          00001800
           02 MPAGDV    PIC X.                                          00001810
           02 MPAGDO    PIC X.                                          00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCTITRENOMA    PIC X.                                     00001840
           02 MCTITRENOMC    PIC X.                                     00001850
           02 MCTITRENOMP    PIC X.                                     00001860
           02 MCTITRENOMH    PIC X.                                     00001870
           02 MCTITRENOMV    PIC X.                                     00001880
           02 MCTITRENOMO    PIC X(5).                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLNOMA    PIC X.                                          00001910
           02 MLNOMC    PIC X.                                          00001920
           02 MLNOMP    PIC X.                                          00001930
           02 MLNOMH    PIC X.                                          00001940
           02 MLNOMV    PIC X.                                          00001950
           02 MLNOMO    PIC X(25).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLPRENOMA      PIC X.                                     00001980
           02 MLPRENOMC PIC X.                                          00001990
           02 MLPRENOMP PIC X.                                          00002000
           02 MLPRENOMH PIC X.                                          00002010
           02 MLPRENOMV PIC X.                                          00002020
           02 MLPRENOMO      PIC X(15).                                 00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCVOIEA   PIC X.                                          00002050
           02 MCVOIEC   PIC X.                                          00002060
           02 MCVOIEP   PIC X.                                          00002070
           02 MCVOIEH   PIC X.                                          00002080
           02 MCVOIEV   PIC X.                                          00002090
           02 MCVOIEO   PIC X(5).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCTVOIEA  PIC X.                                          00002120
           02 MCTVOIEC  PIC X.                                          00002130
           02 MCTVOIEP  PIC X.                                          00002140
           02 MCTVOIEH  PIC X.                                          00002150
           02 MCTVOIEV  PIC X.                                          00002160
           02 MCTVOIEO  PIC X(4).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLVOIEA   PIC X.                                          00002190
           02 MLVOIEC   PIC X.                                          00002200
           02 MLVOIEP   PIC X.                                          00002210
           02 MLVOIEH   PIC X.                                          00002220
           02 MLVOIEV   PIC X.                                          00002230
           02 MLVOIEO   PIC X(21).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCPOSTALA      PIC X.                                     00002260
           02 MCPOSTALC PIC X.                                          00002270
           02 MCPOSTALP PIC X.                                          00002280
           02 MCPOSTALH PIC X.                                          00002290
           02 MCPOSTALV PIC X.                                          00002300
           02 MCPOSTALO      PIC X(5).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLCOMMUNEA     PIC X.                                     00002330
           02 MLCOMMUNEC     PIC X.                                     00002340
           02 MLCOMMUNEP     PIC X.                                     00002350
           02 MLCOMMUNEH     PIC X.                                     00002360
           02 MLCOMMUNEV     PIC X.                                     00002370
           02 MLCOMMUNEO     PIC X(32).                                 00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLBUREAUA      PIC X.                                     00002400
           02 MLBUREAUC PIC X.                                          00002410
           02 MLBUREAUP PIC X.                                          00002420
           02 MLBUREAUH PIC X.                                          00002430
           02 MLBUREAUV PIC X.                                          00002440
           02 MLBUREAUO      PIC X(26).                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLIBERRA  PIC X.                                          00002470
           02 MLIBERRC  PIC X.                                          00002480
           02 MLIBERRP  PIC X.                                          00002490
           02 MLIBERRH  PIC X.                                          00002500
           02 MLIBERRV  PIC X.                                          00002510
           02 MLIBERRO  PIC X(78).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCODTRAA  PIC X.                                          00002540
           02 MCODTRAC  PIC X.                                          00002550
           02 MCODTRAP  PIC X.                                          00002560
           02 MCODTRAH  PIC X.                                          00002570
           02 MCODTRAV  PIC X.                                          00002580
           02 MCODTRAO  PIC X(4).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MZONCMDA  PIC X.                                          00002610
           02 MZONCMDC  PIC X.                                          00002620
           02 MZONCMDP  PIC X.                                          00002630
           02 MZONCMDH  PIC X.                                          00002640
           02 MZONCMDV  PIC X.                                          00002650
           02 MZONCMDO  PIC X(15).                                      00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(5).                                       00002870
                                                                                
