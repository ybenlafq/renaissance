      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFX04   EFX04                                              00000020
      ***************************************************************** 00000030
       01   EFX04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
           02 MSTRUCTUREI OCCURS   14 TIMES .                           00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPSTATL    COMP PIC S9(4).                            00000170
      *--                                                                       
             03 MTYPSTATL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTYPSTATF    PIC X.                                     00000180
             03 FILLER  PIC X(4).                                       00000190
             03 MTYPSTATI    PIC X.                                     00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEPL   COMP PIC S9(4).                                 00000210
      *--                                                                       
             03 MDEPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDEPF   PIC X.                                          00000220
             03 FILLER  PIC X(4).                                       00000230
             03 MDEPI   PIC X.                                          00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTIONL    COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MSECTIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECTIONF    PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MSECTIONI    PIC X(3).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MZONCMDI  PIC X(15).                                      00000320
      * ZONE MESSAGE                                                    00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBERRI  PIC X(55).                                      00000370
      * CODE TRANSACTION                                                00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCODTRAI  PIC X(4).                                       00000420
      * NOM DU CICS                                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCICSI    PIC X(5).                                       00000470
      * NOM LIGNE VTAM                                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MNETNAMI  PIC X(8).                                       00000520
      * CODE TERMINAL                                                   00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSCREENI  PIC X(4).                                       00000570
      ***************************************************************** 00000580
      * SDF: EFX04   EFX04                                              00000590
      ***************************************************************** 00000600
       01   EFX04O REDEFINES EFX04I.                                    00000610
           02 FILLER    PIC X(12).                                      00000620
      * DATE JOUR TRAITEMENT                                            00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MDATJOUA  PIC X.                                          00000650
           02 MDATJOUC  PIC X.                                          00000660
           02 MDATJOUP  PIC X.                                          00000670
           02 MDATJOUH  PIC X.                                          00000680
           02 MDATJOUV  PIC X.                                          00000690
           02 MDATJOUO  PIC X(10).                                      00000700
      * HEURE TRAITEMENT                                                00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MTIMJOUA  PIC X.                                          00000730
           02 MTIMJOUC  PIC X.                                          00000740
           02 MTIMJOUP  PIC X.                                          00000750
           02 MTIMJOUH  PIC X.                                          00000760
           02 MTIMJOUV  PIC X.                                          00000770
           02 MTIMJOUO  PIC X(5).                                       00000780
           02 MSTRUCTUREO OCCURS   14 TIMES .                           00000790
             03 FILLER       PIC X(2).                                  00000800
             03 MTYPSTATA    PIC X.                                     00000810
             03 MTYPSTATC    PIC X.                                     00000820
             03 MTYPSTATP    PIC X.                                     00000830
             03 MTYPSTATH    PIC X.                                     00000840
             03 MTYPSTATV    PIC X.                                     00000850
             03 MTYPSTATO    PIC X.                                     00000860
             03 FILLER       PIC X(2).                                  00000870
             03 MDEPA   PIC X.                                          00000880
             03 MDEPC   PIC X.                                          00000890
             03 MDEPP   PIC X.                                          00000900
             03 MDEPH   PIC X.                                          00000910
             03 MDEPV   PIC X.                                          00000920
             03 MDEPO   PIC X.                                          00000930
             03 FILLER       PIC X(2).                                  00000940
             03 MSECTIONA    PIC X.                                     00000950
             03 MSECTIONC    PIC X.                                     00000960
             03 MSECTIONP    PIC X.                                     00000970
             03 MSECTIONH    PIC X.                                     00000980
             03 MSECTIONV    PIC X.                                     00000990
             03 MSECTIONO    PIC X(3).                                  00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MZONCMDA  PIC X.                                          00001020
           02 MZONCMDC  PIC X.                                          00001030
           02 MZONCMDP  PIC X.                                          00001040
           02 MZONCMDH  PIC X.                                          00001050
           02 MZONCMDV  PIC X.                                          00001060
           02 MZONCMDO  PIC X(15).                                      00001070
      * ZONE MESSAGE                                                    00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MLIBERRA  PIC X.                                          00001100
           02 MLIBERRC  PIC X.                                          00001110
           02 MLIBERRP  PIC X.                                          00001120
           02 MLIBERRH  PIC X.                                          00001130
           02 MLIBERRV  PIC X.                                          00001140
           02 MLIBERRO  PIC X(55).                                      00001150
      * CODE TRANSACTION                                                00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCODTRAA  PIC X.                                          00001180
           02 MCODTRAC  PIC X.                                          00001190
           02 MCODTRAP  PIC X.                                          00001200
           02 MCODTRAH  PIC X.                                          00001210
           02 MCODTRAV  PIC X.                                          00001220
           02 MCODTRAO  PIC X(4).                                       00001230
      * NOM DU CICS                                                     00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCICSA    PIC X.                                          00001260
           02 MCICSC    PIC X.                                          00001270
           02 MCICSP    PIC X.                                          00001280
           02 MCICSH    PIC X.                                          00001290
           02 MCICSV    PIC X.                                          00001300
           02 MCICSO    PIC X(5).                                       00001310
      * NOM LIGNE VTAM                                                  00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNETNAMA  PIC X.                                          00001340
           02 MNETNAMC  PIC X.                                          00001350
           02 MNETNAMP  PIC X.                                          00001360
           02 MNETNAMH  PIC X.                                          00001370
           02 MNETNAMV  PIC X.                                          00001380
           02 MNETNAMO  PIC X(8).                                       00001390
      * CODE TERMINAL                                                   00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MSCREENA  PIC X.                                          00001420
           02 MSCREENC  PIC X.                                          00001430
           02 MSCREENP  PIC X.                                          00001440
           02 MSCREENH  PIC X.                                          00001450
           02 MSCREENV  PIC X.                                          00001460
           02 MSCREENO  PIC X(4).                                       00001470
                                                                                
