      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ENM01   ENM01                                              00000020
      ***************************************************************** 00000030
       01   ENM01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCPTL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCCPTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCCPTF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCCPTI    PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIBL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCLIBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCLIBF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIBI    PIC X(30).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCRIL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCCRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCCRIF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCCRII    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAUTL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCAUTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCAUTF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCAUTI    PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSNTL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCSNTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSNTF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSNTI    PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCEXCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCEXCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCEXCI    PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEMPL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCEMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCEMPF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCEMPI    PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCHEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCCHEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCCHEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCCHEI    PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWICL     COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MWICL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MWICF     PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWICI     PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRCRIL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCTRCRIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRCRIF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTRCRII  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MZONCMDI  PIC X(15).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBERRI  PIC X(56).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCODTRAI  PIC X(4).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCICSI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNETNAMI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MSCREENI  PIC X(4).                                       00000850
      ***************************************************************** 00000860
      * SDF: ENM01   ENM01                                              00000870
      ***************************************************************** 00000880
       01   ENM01O REDEFINES ENM01I.                                    00000890
           02 FILLER    PIC X(12).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MDATJOUA  PIC X.                                          00000920
           02 MDATJOUC  PIC X.                                          00000930
           02 MDATJOUP  PIC X.                                          00000940
           02 MDATJOUH  PIC X.                                          00000950
           02 MDATJOUV  PIC X.                                          00000960
           02 MDATJOUO  PIC X(10).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTIMJOUA  PIC X.                                          00000990
           02 MTIMJOUC  PIC X.                                          00001000
           02 MTIMJOUP  PIC X.                                          00001010
           02 MTIMJOUH  PIC X.                                          00001020
           02 MTIMJOUV  PIC X.                                          00001030
           02 MTIMJOUO  PIC X(5).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MWFONCA   PIC X.                                          00001060
           02 MWFONCC   PIC X.                                          00001070
           02 MWFONCP   PIC X.                                          00001080
           02 MWFONCH   PIC X.                                          00001090
           02 MWFONCV   PIC X.                                          00001100
           02 MWFONCO   PIC X(3).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MNSOCA    PIC X.                                          00001130
           02 MNSOCC    PIC X.                                          00001140
           02 MNSOCP    PIC X.                                          00001150
           02 MNSOCH    PIC X.                                          00001160
           02 MNSOCV    PIC X.                                          00001170
           02 MNSOCO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MCCPTA    PIC X.                                          00001200
           02 MCCPTC    PIC X.                                          00001210
           02 MCCPTP    PIC X.                                          00001220
           02 MCCPTH    PIC X.                                          00001230
           02 MCCPTV    PIC X.                                          00001240
           02 MCCPTO    PIC X(2).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCLIBA    PIC X.                                          00001270
           02 MCLIBC    PIC X.                                          00001280
           02 MCLIBP    PIC X.                                          00001290
           02 MCLIBH    PIC X.                                          00001300
           02 MCLIBV    PIC X.                                          00001310
           02 MCLIBO    PIC X(30).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MCCRIA    PIC X.                                          00001340
           02 MCCRIC    PIC X.                                          00001350
           02 MCCRIP    PIC X.                                          00001360
           02 MCCRIH    PIC X.                                          00001370
           02 MCCRIV    PIC X.                                          00001380
           02 MCCRIO    PIC X(10).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCAUTA    PIC X.                                          00001410
           02 MCAUTC    PIC X.                                          00001420
           02 MCAUTP    PIC X.                                          00001430
           02 MCAUTH    PIC X.                                          00001440
           02 MCAUTV    PIC X.                                          00001450
           02 MCAUTO    PIC X.                                          00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCSNTA    PIC X.                                          00001480
           02 MCSNTC    PIC X.                                          00001490
           02 MCSNTP    PIC X.                                          00001500
           02 MCSNTH    PIC X.                                          00001510
           02 MCSNTV    PIC X.                                          00001520
           02 MCSNTO    PIC X.                                          00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCEXCA    PIC X.                                          00001550
           02 MCEXCC    PIC X.                                          00001560
           02 MCEXCP    PIC X.                                          00001570
           02 MCEXCH    PIC X.                                          00001580
           02 MCEXCV    PIC X.                                          00001590
           02 MCEXCO    PIC X.                                          00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCEMPA    PIC X.                                          00001620
           02 MCEMPC    PIC X.                                          00001630
           02 MCEMPP    PIC X.                                          00001640
           02 MCEMPH    PIC X.                                          00001650
           02 MCEMPV    PIC X.                                          00001660
           02 MCEMPO    PIC X.                                          00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCCHEA    PIC X.                                          00001690
           02 MCCHEC    PIC X.                                          00001700
           02 MCCHEP    PIC X.                                          00001710
           02 MCCHEH    PIC X.                                          00001720
           02 MCCHEV    PIC X.                                          00001730
           02 MCCHEO    PIC X.                                          00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MWICA     PIC X.                                          00001760
           02 MWICC     PIC X.                                          00001770
           02 MWICP     PIC X.                                          00001780
           02 MWICH     PIC X.                                          00001790
           02 MWICV     PIC X.                                          00001800
           02 MWICO     PIC X.                                          00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCTRCRIA  PIC X.                                          00001830
           02 MCTRCRIC  PIC X.                                          00001840
           02 MCTRCRIP  PIC X.                                          00001850
           02 MCTRCRIH  PIC X.                                          00001860
           02 MCTRCRIV  PIC X.                                          00001870
           02 MCTRCRIO  PIC X.                                          00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MZONCMDA  PIC X.                                          00001900
           02 MZONCMDC  PIC X.                                          00001910
           02 MZONCMDP  PIC X.                                          00001920
           02 MZONCMDH  PIC X.                                          00001930
           02 MZONCMDV  PIC X.                                          00001940
           02 MZONCMDO  PIC X(15).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLIBERRA  PIC X.                                          00001970
           02 MLIBERRC  PIC X.                                          00001980
           02 MLIBERRP  PIC X.                                          00001990
           02 MLIBERRH  PIC X.                                          00002000
           02 MLIBERRV  PIC X.                                          00002010
           02 MLIBERRO  PIC X(56).                                      00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCODTRAA  PIC X.                                          00002040
           02 MCODTRAC  PIC X.                                          00002050
           02 MCODTRAP  PIC X.                                          00002060
           02 MCODTRAH  PIC X.                                          00002070
           02 MCODTRAV  PIC X.                                          00002080
           02 MCODTRAO  PIC X(4).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCICSA    PIC X.                                          00002110
           02 MCICSC    PIC X.                                          00002120
           02 MCICSP    PIC X.                                          00002130
           02 MCICSH    PIC X.                                          00002140
           02 MCICSV    PIC X.                                          00002150
           02 MCICSO    PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNETNAMA  PIC X.                                          00002180
           02 MNETNAMC  PIC X.                                          00002190
           02 MNETNAMP  PIC X.                                          00002200
           02 MNETNAMH  PIC X.                                          00002210
           02 MNETNAMV  PIC X.                                          00002220
           02 MNETNAMO  PIC X(8).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MSCREENA  PIC X.                                          00002250
           02 MSCREENC  PIC X.                                          00002260
           02 MSCREENP  PIC X.                                          00002270
           02 MSCREENH  PIC X.                                          00002280
           02 MSCREENV  PIC X.                                          00002290
           02 MSCREENO  PIC X(4).                                       00002300
                                                                                
