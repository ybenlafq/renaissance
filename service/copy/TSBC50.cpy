      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------                     
      * TS-LISTE DE CLIENTS                                                     
      * REPONSE  MQ-SERISE                                                      
      *                                                                         
      *                                                                         
      *----------------------------------------------------                     
      *----------------------------------LONGUEUR          X 10                 
       01 TS-BC50-DATA.                                                         
          05 TS-BC50-LIGNE    OCCURS 10.                                        
             10 TS-BC50-NCARTE        PIC X(09).                                
             10 TS-BC50-LNOM          PIC X(25).                                
             10 TS-BC50-LPRENOM       PIC X(15).                                
             10 TS-BC50-CTITRENOM     PIC X(05).                                
             10 TS-BC50-TORG          PIC X(05).                                
             10 TS-BC50-LORG          PIC X(32).                                
             10 TS-BC50-NCLIENTADR    PIC X(08).                                
             10 TS-BC50-NFOYERADR     PIC X(08).                                
             10 TS-BC50-DNAISS        PIC X(08).                                
             10 TS-BC50-CPNAISS       PIC X(05).                                
             10 TS-BC50-LNAISS        PIC X(32).                                
             10 TS-BC50-ECRAN1.                                                 
                 15 TS-BC50-CVOIE      PIC X(05).                               
                 15 TS-BC50-CTVOIE     PIC X(04).                               
                 15 TS-BC50-LNOMVOIE   PIC X(25).                               
             10 TS-BC50-ECRAN2.                                                 
                 15 TS-BC50-CPOST      PIC X(05).                               
                 15 TS-BC50-LCOMN      PIC X(32).                               
                 15 TS-BC50-NTDOM      PIC X(15).                               
             10 TS-BC50-ECRAN3.                                                 
                 15 TS-BC50-NGSM       PIC X(15).                               
                 15 TS-BC50-NTBUR      PIC X(15).                               
                 15 TS-BC50-POSTEBUR   PIC X(05).                               
             10 TS-BC50-ECRAN4.                                                 
                 15 TS-BC50-EMAIL      PIC X(65).                               
             10 TS-BC50-ADRESSE-COMPLETE.                                       
                 15 TS-BC50-LBATIMENT  PIC X(03).                               
                 15 TS-BC50-LESCALIER  PIC X(03).                               
                 15 TS-BC50-LETAGE     PIC X(03).                               
                 15 TS-BC50-LPORTE     PIC X(03).                               
                 15 TS-BC50-CMPAD1     PIC X(32).                               
                 15 TS-BC50-LBUREAU    PIC X(26).                               
                 15 TS-BC50-CPAYS      PIC X(03).                               
                 15 TS-BC50-TPAYS      PIC X(03).                               
                 15 TS-BC50-RESSEC     PIC X(01).                               
                 15 TS-BC50-ORGMODIF   PIC X(01).                               
                 15 TS-BC50-RQUESTION  PIC X(03).                               
                 15 TS-BC50-CTYPCLIENT PIC X(01).                               
JC0807       10 TS-BC50-B2B            PIC X(01).                               
JC0807*      10 TS-BC50-FILER     PIC X(01).                                    
                                                                                
