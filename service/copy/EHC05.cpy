      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC05   EHC05                                              00000020
      ***************************************************************** 00000030
       01   EHC05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNLIEUHCI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHSL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCHSF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCHSI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSERIEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSERIEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSERIEI  PIC X(16).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMAJADRL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCMAJADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMAJADRF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMAJADRI      PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRESSEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MADRESSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRESSEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MADRESSEI      PIC X(8).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEMANDEL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDDEMANDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDDEMANDEF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDDEMANDEI     PIC X(10).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCDEF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDCDEI    PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTYPEI   PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDREL1L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDREL1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDREL1F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDREL1I   PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNCDE1I   PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDREL2L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDREL2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDREL2F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDREL2I   PIC X(10).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE2L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNCDE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE2F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNCDE2I   PIC X(6).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE3L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNCDE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE3F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNCDE3I   PIC X(6).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MDRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDRECF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDRECI    PIC X(10).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTEC1L   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCTEC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTEC1F   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCTEC1I   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEMAVOIRL    COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MDDEMAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDDEMAVOIRF    PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDDEMAVOIRI    PIC X(10).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTEC2L   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCTEC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTEC2F   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCTEC2I   PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMAVOIRL    COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MNDEMAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNDEMAVOIRF    PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNDEMAVOIRI    PIC X(8).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECAVOIRL    COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MDRECAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDRECAVOIRF    PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MDRECAVOIRI    PIC X(10).                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(78).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCICSI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(4).                                       00001250
      ***************************************************************** 00001260
      * SDF: EHC05   EHC05                                              00001270
      ***************************************************************** 00001280
       01   EHC05O REDEFINES EHC05I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MDATJOUA  PIC X.                                          00001320
           02 MDATJOUC  PIC X.                                          00001330
           02 MDATJOUP  PIC X.                                          00001340
           02 MDATJOUH  PIC X.                                          00001350
           02 MDATJOUV  PIC X.                                          00001360
           02 MDATJOUO  PIC X(10).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTIMJOUA  PIC X.                                          00001390
           02 MTIMJOUC  PIC X.                                          00001400
           02 MTIMJOUP  PIC X.                                          00001410
           02 MTIMJOUH  PIC X.                                          00001420
           02 MTIMJOUV  PIC X.                                          00001430
           02 MTIMJOUO  PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNLIEUHCA      PIC X.                                     00001460
           02 MNLIEUHCC PIC X.                                          00001470
           02 MNLIEUHCP PIC X.                                          00001480
           02 MNLIEUHCH PIC X.                                          00001490
           02 MNLIEUHCV PIC X.                                          00001500
           02 MNLIEUHCO      PIC X(3).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNCHSA    PIC X.                                          00001530
           02 MNCHSC    PIC X.                                          00001540
           02 MNCHSP    PIC X.                                          00001550
           02 MNCHSH    PIC X.                                          00001560
           02 MNCHSV    PIC X.                                          00001570
           02 MNCHSO    PIC X(7).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNCODICA  PIC X.                                          00001600
           02 MNCODICC  PIC X.                                          00001610
           02 MNCODICP  PIC X.                                          00001620
           02 MNCODICH  PIC X.                                          00001630
           02 MNCODICV  PIC X.                                          00001640
           02 MNCODICO  PIC X(7).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCFAMA    PIC X.                                          00001670
           02 MCFAMC    PIC X.                                          00001680
           02 MCFAMP    PIC X.                                          00001690
           02 MCFAMH    PIC X.                                          00001700
           02 MCFAMV    PIC X.                                          00001710
           02 MCFAMO    PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCMARQA   PIC X.                                          00001740
           02 MCMARQC   PIC X.                                          00001750
           02 MCMARQP   PIC X.                                          00001760
           02 MCMARQH   PIC X.                                          00001770
           02 MCMARQV   PIC X.                                          00001780
           02 MCMARQO   PIC X(5).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLREFA    PIC X.                                          00001810
           02 MLREFC    PIC X.                                          00001820
           02 MLREFP    PIC X.                                          00001830
           02 MLREFH    PIC X.                                          00001840
           02 MLREFV    PIC X.                                          00001850
           02 MLREFO    PIC X(20).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCSERIEA  PIC X.                                          00001880
           02 MCSERIEC  PIC X.                                          00001890
           02 MCSERIEP  PIC X.                                          00001900
           02 MCSERIEH  PIC X.                                          00001910
           02 MCSERIEV  PIC X.                                          00001920
           02 MCSERIEO  PIC X(16).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCMAJADRA      PIC X.                                     00001950
           02 MCMAJADRC PIC X.                                          00001960
           02 MCMAJADRP PIC X.                                          00001970
           02 MCMAJADRH PIC X.                                          00001980
           02 MCMAJADRV PIC X.                                          00001990
           02 MCMAJADRO      PIC X.                                     00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MADRESSEA      PIC X.                                     00002020
           02 MADRESSEC PIC X.                                          00002030
           02 MADRESSEP PIC X.                                          00002040
           02 MADRESSEH PIC X.                                          00002050
           02 MADRESSEV PIC X.                                          00002060
           02 MADRESSEO      PIC X(8).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MDDEMANDEA     PIC X.                                     00002090
           02 MDDEMANDEC     PIC X.                                     00002100
           02 MDDEMANDEP     PIC X.                                     00002110
           02 MDDEMANDEH     PIC X.                                     00002120
           02 MDDEMANDEV     PIC X.                                     00002130
           02 MDDEMANDEO     PIC X(10).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MDCDEA    PIC X.                                          00002160
           02 MDCDEC    PIC X.                                          00002170
           02 MDCDEP    PIC X.                                          00002180
           02 MDCDEH    PIC X.                                          00002190
           02 MDCDEV    PIC X.                                          00002200
           02 MDCDEO    PIC X(10).                                      00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCTYPEA   PIC X.                                          00002230
           02 MCTYPEC   PIC X.                                          00002240
           02 MCTYPEP   PIC X.                                          00002250
           02 MCTYPEH   PIC X.                                          00002260
           02 MCTYPEV   PIC X.                                          00002270
           02 MCTYPEO   PIC X(3).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MDREL1A   PIC X.                                          00002300
           02 MDREL1C   PIC X.                                          00002310
           02 MDREL1P   PIC X.                                          00002320
           02 MDREL1H   PIC X.                                          00002330
           02 MDREL1V   PIC X.                                          00002340
           02 MDREL1O   PIC X(10).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MNCDE1A   PIC X.                                          00002370
           02 MNCDE1C   PIC X.                                          00002380
           02 MNCDE1P   PIC X.                                          00002390
           02 MNCDE1H   PIC X.                                          00002400
           02 MNCDE1V   PIC X.                                          00002410
           02 MNCDE1O   PIC X(6).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MDREL2A   PIC X.                                          00002440
           02 MDREL2C   PIC X.                                          00002450
           02 MDREL2P   PIC X.                                          00002460
           02 MDREL2H   PIC X.                                          00002470
           02 MDREL2V   PIC X.                                          00002480
           02 MDREL2O   PIC X(10).                                      00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNCDE2A   PIC X.                                          00002510
           02 MNCDE2C   PIC X.                                          00002520
           02 MNCDE2P   PIC X.                                          00002530
           02 MNCDE2H   PIC X.                                          00002540
           02 MNCDE2V   PIC X.                                          00002550
           02 MNCDE2O   PIC X(6).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MNCDE3A   PIC X.                                          00002580
           02 MNCDE3C   PIC X.                                          00002590
           02 MNCDE3P   PIC X.                                          00002600
           02 MNCDE3H   PIC X.                                          00002610
           02 MNCDE3V   PIC X.                                          00002620
           02 MNCDE3O   PIC X(6).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDRECA    PIC X.                                          00002650
           02 MDRECC    PIC X.                                          00002660
           02 MDRECP    PIC X.                                          00002670
           02 MDRECH    PIC X.                                          00002680
           02 MDRECV    PIC X.                                          00002690
           02 MDRECO    PIC X(10).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCTEC1A   PIC X.                                          00002720
           02 MCTEC1C   PIC X.                                          00002730
           02 MCTEC1P   PIC X.                                          00002740
           02 MCTEC1H   PIC X.                                          00002750
           02 MCTEC1V   PIC X.                                          00002760
           02 MCTEC1O   PIC X(3).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MDDEMAVOIRA    PIC X.                                     00002790
           02 MDDEMAVOIRC    PIC X.                                     00002800
           02 MDDEMAVOIRP    PIC X.                                     00002810
           02 MDDEMAVOIRH    PIC X.                                     00002820
           02 MDDEMAVOIRV    PIC X.                                     00002830
           02 MDDEMAVOIRO    PIC X(10).                                 00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCTEC2A   PIC X.                                          00002860
           02 MCTEC2C   PIC X.                                          00002870
           02 MCTEC2P   PIC X.                                          00002880
           02 MCTEC2H   PIC X.                                          00002890
           02 MCTEC2V   PIC X.                                          00002900
           02 MCTEC2O   PIC X(3).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNDEMAVOIRA    PIC X.                                     00002930
           02 MNDEMAVOIRC    PIC X.                                     00002940
           02 MNDEMAVOIRP    PIC X.                                     00002950
           02 MNDEMAVOIRH    PIC X.                                     00002960
           02 MNDEMAVOIRV    PIC X.                                     00002970
           02 MNDEMAVOIRO    PIC X(8).                                  00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MDRECAVOIRA    PIC X.                                     00003000
           02 MDRECAVOIRC    PIC X.                                     00003010
           02 MDRECAVOIRP    PIC X.                                     00003020
           02 MDRECAVOIRH    PIC X.                                     00003030
           02 MDRECAVOIRV    PIC X.                                     00003040
           02 MDRECAVOIRO    PIC X(10).                                 00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MLIBERRA  PIC X.                                          00003070
           02 MLIBERRC  PIC X.                                          00003080
           02 MLIBERRP  PIC X.                                          00003090
           02 MLIBERRH  PIC X.                                          00003100
           02 MLIBERRV  PIC X.                                          00003110
           02 MLIBERRO  PIC X(78).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAP  PIC X.                                          00003160
           02 MCODTRAH  PIC X.                                          00003170
           02 MCODTRAV  PIC X.                                          00003180
           02 MCODTRAO  PIC X(4).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCICSA    PIC X.                                          00003210
           02 MCICSC    PIC X.                                          00003220
           02 MCICSP    PIC X.                                          00003230
           02 MCICSH    PIC X.                                          00003240
           02 MCICSV    PIC X.                                          00003250
           02 MCICSO    PIC X(5).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MNETNAMA  PIC X.                                          00003280
           02 MNETNAMC  PIC X.                                          00003290
           02 MNETNAMP  PIC X.                                          00003300
           02 MNETNAMH  PIC X.                                          00003310
           02 MNETNAMV  PIC X.                                          00003320
           02 MNETNAMO  PIC X(8).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MSCREENA  PIC X.                                          00003350
           02 MSCREENC  PIC X.                                          00003360
           02 MSCREENP  PIC X.                                          00003370
           02 MSCREENH  PIC X.                                          00003380
           02 MSCREENV  PIC X.                                          00003390
           02 MSCREENO  PIC X(4).                                       00003400
                                                                                
