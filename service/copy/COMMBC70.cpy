      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MBC70                                                 
      * APPELLE PAR LINK DANS TBC60                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MBC70.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
             10 COMM-MBC70-NCLIENTADR     PIC X(08).                            
             10 COMM-MBC70-LPRENOM        PIC X(15).                            
             10 COMM-MBC70-LNOM           PIC X(25).                            
             10 COMM-MBC70-CPOST          PIC X(05).                            
             10 COMM-MBC70-LCOMN          PIC X(32).                            
             10 COMM-MBC70-NTDOM          PIC X(15).                            
             10 COMM-MBC70-NTBUR          PIC X(15).                            
             10 COMM-MBC70-NSOCCICS       PIC X(03).                            
             10 COMM-MBC70-NLIEU          PIC X(03).                            
      *              FILLEUR                                                    
             10 COMM-MBC70-CFILLEUR       PIC X(10).                            
      *              CONTROLES  + DONNES  EN ENTREE   O/N                       
        05 COMM-MBC70-CTR.                                                      
      *   BC70 :                                                                
             10 COMM-MBC70-NBROCC         PIC 9(05).                            
             10 COMM-MBC70-CTR-SYNT-OK    PIC X(01).                            
             10 COMM-MBC70-CTR-LOG-OK     PIC X(01).                            
             10 COMM-MBC70-SSAAMMJJ       PIC X(08).                            
      *                                                                         
      *      MESSAGE ERREUR DE LA CONTROLE LOGIIQUE + TS DE RETOUR              
         05 COMM-BC70-RETOUR-TS.                                                
             10 COMM-MBC70-NOM-TS        PIC X(08).                             
             10 COMM-MBC70-NBPAGE        PIC 9(03).                             
             10 COMM-MBC70-NBLIG         PIC 9(03) VALUE 11.                    
             10 COMM-MBC70-ITEM          PIC 9(03).                             
             10 COMM-MBC70-CONTROLE      PIC X(01) .                            
                88 COMM-MBC70-CONTROLE-OK     VALUE ' '.                        
                88 COMM-MBC70-CONTROLE-PAS-OK VALUE '1'.                        
             10 COMM-MBC70-MESS          PIC X(80).                             
         05 FILLER                       PIC X(34).                             
                                                                                
