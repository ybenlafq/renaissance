      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVMC0100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMC0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMC0100.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 MC01-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 MC01-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 MC01-NVENTE          PIC X(7).                                    
      *                       DVENTE                                            
           10 MC01-DVENTE          PIC X(8).                                    
      *                       ADR_MAIL                                          
           10 MC01-ADR-MAIL        PIC X(100).                                  
      *                       FLAGTRT                                           
           10 MC01-FLAGTRT         PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
