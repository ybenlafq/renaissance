      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM080 AU 26/11/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,04,BI,A,                          *        
      *                           28,04,BI,A,                          *        
      *                           32,03,BI,A,                          *        
      *                           35,07,BI,A,                          *        
      *                           42,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM080.                                                        
            05 NOMETAT-INM080           PIC X(6) VALUE 'INM080'.                
            05 RUPTURES-INM080.                                                 
           10 INM080-DATETR             PIC X(08).                      007  008
           10 INM080-NSOC               PIC X(03).                      015  003
           10 INM080-NMAG               PIC X(03).                      018  003
           10 INM080-NCAISS             PIC X(03).                      021  003
           10 INM080-CODOPE             PIC X(04).                      024  004
           10 INM080-NTRANS             PIC X(04).                      028  004
           10 INM080-CTYPE              PIC X(03).                      032  003
           10 INM080-NBCOM              PIC X(07).                      035  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM080-SEQUENCE           PIC S9(04) COMP.                042  002
      *--                                                                       
           10 INM080-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM080.                                                   
           10 INM080-CDEVAUT            PIC X(03).                      044  003
           10 INM080-CDEVREF            PIC X(03).                      047  003
           10 INM080-CDEV1              PIC X(01).                      050  001
           10 INM080-CTVA               PIC X(01).                      051  001
           10 INM080-ENTETE             PIC X(01).                      052  001
           10 INM080-IND-AUT            PIC X(01).                      053  001
           10 INM080-IND-CDEV           PIC X(01).                      054  001
           10 INM080-IND-REF            PIC X(01).                      055  001
           10 INM080-LDEVREF            PIC X(25).                      056  025
           10 INM080-LIB11              PIC X(09).                      081  009
           10 INM080-LIB12              PIC X(09).                      090  009
           10 INM080-LIB21              PIC X(09).                      099  009
           10 INM080-LIB22              PIC X(09).                      108  009
           10 INM080-LIB31              PIC X(09).                      117  009
           10 INM080-LIB32              PIC X(09).                      126  009
           10 INM080-LIB41              PIC X(09).                      135  009
           10 INM080-LIB42              PIC X(09).                      144  009
           10 INM080-LIB51              PIC X(09).                      153  009
           10 INM080-LIB52              PIC X(09).                      162  009
           10 INM080-MODP               PIC X(01).                      171  001
           10 INM080-NCODIC             PIC X(07).                      172  007
           10 INM080-PARTIE1            PIC X(01).                      179  001
           10 INM080-PARTIE2            PIC X(01).                      180  001
           10 INM080-RAYON              PIC X(02).                      181  002
           10 INM080-SIGNE              PIC X(01).                      183  001
           10 INM080-TYPE-PAIE          PIC X(02).                      184  002
           10 INM080-TYPE-RAY           PIC X(02).                      186  002
           10 INM080-TYPE-TRANS         PIC X(03).                      188  003
           10 INM080-AUT-PAIE           PIC S9(09)V9(2) COMP-3.         191  006
           10 INM080-AUT-RAY            PIC S9(09)V9(2) COMP-3.         197  006
           10 INM080-AUT-TRANS          PIC S9(09)V9(2) COMP-3.         203  006
           10 INM080-ENCTTC             PIC S9(05)V9(2) COMP-3.         209  004
           10 INM080-MONT-PAIE          PIC S9(09)V9(2) COMP-3.         213  006
           10 INM080-MONT-PAIE-AUT      PIC S9(09)V9(2) COMP-3.         219  006
           10 INM080-MONT-RAY           PIC S9(09)V9(2) COMP-3.         225  006
           10 INM080-MONT-TRANS         PIC S9(09)V9(2) COMP-3.         231  006
           10 INM080-MONTANT1           PIC S9(06)V9(2) COMP-3.         237  005
           10 INM080-MONTANT2           PIC S9(06)V9(2) COMP-3.         242  005
           10 INM080-MONTANT3           PIC S9(06)V9(2) COMP-3.         247  005
           10 INM080-MONTANT4           PIC S9(06)V9(2) COMP-3.         252  005
           10 INM080-MONTANT5           PIC S9(06)V9(2) COMP-3.         257  005
           10 INM080-MONTANT6           PIC S9(07)V9(2) COMP-3.         262  005
           10 INM080-MONTANT7           PIC S9(07)V9(2) COMP-3.         267  005
            05 FILLER                      PIC X(241).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM080-LONG           PIC S9(4)   COMP  VALUE +271.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM080-LONG           PIC S9(4) COMP-5  VALUE +271.           
                                                                                
      *}                                                                        
