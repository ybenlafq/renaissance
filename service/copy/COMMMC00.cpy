      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-MC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(08).                                   
           02 COMM-CICS-NETNAM     PIC X(08).                                   
           02 COMM-CICS-TRANSA     PIC X(04).                                   
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC X(02).                                   
           02 COMM-DATE-ANNEE      PIC X(02).                                   
           02 COMM-DATE-MOIS       PIC X(02).                                   
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC X(03).                                   
           02 COMM-DATE-JSM-LL     PIC X(08).                                   
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC X(03).                                   
           02 COMM-DATE-MOIS-LL    PIC X(08).                                   
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                                   
           02 COMM-DATE-AAMMJJ     PIC X(06).                                   
           02 COMM-DATE-JJMMSSAA   PIC X(08).                                   
           02 COMM-DATE-JJMMAA     PIC X(06).                                   
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                                   
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *   ZONES RESERVEES TRAITEMENT DU SWAP                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874         
           02 COMM-MC00-ENTETE.                                                 
              03 COMM-ENTETE.                                                   
                 05 COMM-CODLANG            PIC X(02).                          
                 05 COMM-CODPIC             PIC X(02).                          
                 05 COMM-CODESFONCTION  .                                       
                    10 COMM-CODE-FONCTION      OCCURS 3.                        
                       15  COMM-CFONC     PIC  X(03).                           
                 05 COMM-CFONCTION-OPTION   PIC X(03).                          
                 05 COMM-01-CFONC-OPTION    PIC X(03).                          
                 05 COMM-03-CFONC-OPTION    PIC X(03).                          
                 05 COMM-05-CFONC-OPTION    PIC X(03).                          
                 05 COMM-MODE-INTEROGATION    PIC X VALUE ' '.                  
                     88 NON-MODE-INTEROGATION VALUE ' '.                        
                     88     MODE-INTEROGATION VALUE '1'.                        
                 05 COMM-COPTION            PIC X(02).                          
      *    02 COMM-MC00-APPLI.                                                  
           02 COMM-MC00-FILLER              PIC X(3854).                        
                                                                                
