      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PCCOP PROJ. CARTES, CODES OPERATIONS   *        
      *----------------------------------------------------------------*        
       01  RVPCCOP.                                                             
           05  PCCOP-CTABLEG2    PIC X(15).                                     
           05  PCCOP-CTABLEG2-REDEF REDEFINES PCCOP-CTABLEG2.                   
               10  PCCOP-COPER           PIC X(10).                             
           05  PCCOP-WTABLEG     PIC X(80).                                     
           05  PCCOP-WTABLEG-REDEF  REDEFINES PCCOP-WTABLEG.                    
               10  PCCOP-LOPER           PIC X(25).                             
               10  PCCOP-WCRE            PIC X(01).                             
               10  PCCOP-WSENS           PIC X(01).                             
               10  PCCOP-WRECONCI        PIC X(01).                             
               10  PCCOP-COPERA          PIC X(06).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPCCOP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCCOP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PCCOP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCCOP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PCCOP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
