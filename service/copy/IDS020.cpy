      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IDS020 AU 09/11/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,03,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IDS020.                                                        
            05 NOMETAT-IDS020           PIC X(6) VALUE 'IDS020'.                
            05 RUPTURES-IDS020.                                                 
           10 IDS020-NSOCIETE           PIC X(03).                      007  003
           10 IDS020-WSEQED             PIC S9(05)      COMP-3.         010  003
           10 IDS020-WSEQFAM            PIC S9(05)      COMP-3.         013  003
           10 IDS020-CMARQ              PIC X(05).                      016  005
           10 IDS020-NCODIC             PIC X(07).                      021  007
           10 IDS020-NLIEUVALO          PIC X(03).                      028  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IDS020-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 IDS020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IDS020.                                                   
           10 IDS020-CFAM               PIC X(05).                      033  005
           10 IDS020-CRAYON             PIC X(05).                      038  005
           10 IDS020-LLIEU              PIC X(20).                      043  020
           10 IDS020-LREFFOURN          PIC X(20).                      063  020
           10 IDS020-PEXCLUS            PIC S9(09)V9(6) COMP-3.         083  008
           10 IDS020-PNET               PIC S9(09)V9(6) COMP-3.         091  008
           10 IDS020-PVALINV            PIC S9(09)V9(6) COMP-3.         099  008
           10 IDS020-QEXCLUS            PIC S9(05)      COMP-3.         107  003
           10 IDS020-QTAUX              PIC S9(03)V9(2) COMP-3.         110  003
           10 IDS020-QTEINV             PIC S9(05)      COMP-3.         113  003
           10 IDS020-QTENET             PIC S9(05)      COMP-3.         116  003
            05 FILLER                      PIC X(394).                          
                                                                                
