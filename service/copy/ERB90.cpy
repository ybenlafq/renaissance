      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: maj intervention thd                                       00000020
      ***************************************************************** 00000030
       01   ERB90I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONTRATL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCONTRATL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCONTRATF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCONTRATI      PIC X(18).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVENTEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MVENTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVENTEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MVENTEI   PIC X(13).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTATUTI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSSTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSSTF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSSTI     PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTINTERL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTINTERF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTINTERI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNINTERL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNINTERF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNINTERI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNASC1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDNASC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDNASC1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDNASC1I  PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDNASC2L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDNASC2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDNASC2F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDNASC2I  PIC X(10).                                      00000530
           02 MCONTRLD OCCURS   5 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCONTRLL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCONTRLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCONTRLF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCONTRLI     PIC X(18).                                 00000580
           02 MVENTELD OCCURS   5 TIMES .                               00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVENTELL     COMP PIC S9(4).                            00000600
      *--                                                                       
             03 MVENTELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVENTELF     PIC X.                                     00000610
             03 FILLER  PIC X(4).                                       00000620
             03 MVENTELI     PIC X(13).                                 00000630
           02 MIDENTLD OCCURS   5 TIMES .                               00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MIDENTLL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MIDENTLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MIDENTLF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MIDENTLI     PIC X(20).                                 00000680
           02 MNIMMOLD OCCURS   5 TIMES .                               00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNIMMOLL     COMP PIC S9(4).                            00000700
      *--                                                                       
             03 MNIMMOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNIMMOLF     PIC X.                                     00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MNIMMOLI     PIC X(12).                                 00000730
           02 MTINTERLD OCCURS   5 TIMES .                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTINTERLL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MTINTERLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MTINTERLF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MTINTERLI    PIC X(5).                                  00000780
           02 MNINTERLD OCCURS   5 TIMES .                              00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNINTERLL    COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MNINTERLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNINTERLF    PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MNINTERLI    PIC X(20).                                 00000830
           02 MMONTANLD OCCURS   5 TIMES .                              00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMONTANLL    COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MMONTANLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMONTANLF    PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MMONTANLI    PIC X(10).                                 00000880
           02 MDREDLD OCCURS   5 TIMES .                                00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDREDLL      COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MDREDLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDREDLF      PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MDREDLI      PIC X(8).                                  00000930
           02 MDREDALD OCCURS   5 TIMES .                               00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDREDALL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDREDALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDREDALF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDREDALI     PIC X(8).                                  00000980
           02 MDNASCLD OCCURS   5 TIMES .                               00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDNASCLL     COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MDNASCLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDNASCLF     PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MDNASCLI     PIC X(8).                                  00001030
           02 MSTATLD OCCURS   5 TIMES .                                00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATLL      COMP PIC S9(4).                            00001050
      *--                                                                       
             03 MSTATLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTATLF      PIC X.                                     00001060
             03 FILLER  PIC X(4).                                       00001070
             03 MSTATLI      PIC X(5).                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MLIBERRI  PIC X(74).                                      00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MCODTRAI  PIC X(4).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MCICSI    PIC X(5).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MSCREENI  PIC X(4).                                       00001280
      ***************************************************************** 00001290
      * Rbx: maj intervention thd                                       00001300
      ***************************************************************** 00001310
       01   ERB90O REDEFINES ERB90I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDATJOUA  PIC X.                                          00001350
           02 MDATJOUC  PIC X.                                          00001360
           02 MDATJOUP  PIC X.                                          00001370
           02 MDATJOUH  PIC X.                                          00001380
           02 MDATJOUV  PIC X.                                          00001390
           02 MDATJOUO  PIC X(10).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUP  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUV  PIC X.                                          00001460
           02 MTIMJOUO  PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MPAGEA    PIC X.                                          00001490
           02 MPAGEC    PIC X.                                          00001500
           02 MPAGEP    PIC X.                                          00001510
           02 MPAGEH    PIC X.                                          00001520
           02 MPAGEV    PIC X.                                          00001530
           02 MPAGEO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MPAGEMAXA      PIC X.                                     00001560
           02 MPAGEMAXC PIC X.                                          00001570
           02 MPAGEMAXP PIC X.                                          00001580
           02 MPAGEMAXH PIC X.                                          00001590
           02 MPAGEMAXV PIC X.                                          00001600
           02 MPAGEMAXO      PIC X(3).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCONTRATA      PIC X.                                     00001630
           02 MCONTRATC PIC X.                                          00001640
           02 MCONTRATP PIC X.                                          00001650
           02 MCONTRATH PIC X.                                          00001660
           02 MCONTRATV PIC X.                                          00001670
           02 MCONTRATO      PIC X(18).                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MVENTEA   PIC X.                                          00001700
           02 MVENTEC   PIC X.                                          00001710
           02 MVENTEP   PIC X.                                          00001720
           02 MVENTEH   PIC X.                                          00001730
           02 MVENTEV   PIC X.                                          00001740
           02 MVENTEO   PIC X(13).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSTATUTA  PIC X.                                          00001770
           02 MSTATUTC  PIC X.                                          00001780
           02 MSTATUTP  PIC X.                                          00001790
           02 MSTATUTH  PIC X.                                          00001800
           02 MSTATUTV  PIC X.                                          00001810
           02 MSTATUTO  PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MSSTA     PIC X.                                          00001840
           02 MSSTC     PIC X.                                          00001850
           02 MSSTP     PIC X.                                          00001860
           02 MSSTH     PIC X.                                          00001870
           02 MSSTV     PIC X.                                          00001880
           02 MSSTO     PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MTINTERA  PIC X.                                          00001910
           02 MTINTERC  PIC X.                                          00001920
           02 MTINTERP  PIC X.                                          00001930
           02 MTINTERH  PIC X.                                          00001940
           02 MTINTERV  PIC X.                                          00001950
           02 MTINTERO  PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNINTERA  PIC X.                                          00001980
           02 MNINTERC  PIC X.                                          00001990
           02 MNINTERP  PIC X.                                          00002000
           02 MNINTERH  PIC X.                                          00002010
           02 MNINTERV  PIC X.                                          00002020
           02 MNINTERO  PIC X(20).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MDNASC1A  PIC X.                                          00002050
           02 MDNASC1C  PIC X.                                          00002060
           02 MDNASC1P  PIC X.                                          00002070
           02 MDNASC1H  PIC X.                                          00002080
           02 MDNASC1V  PIC X.                                          00002090
           02 MDNASC1O  PIC X(10).                                      00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDNASC2A  PIC X.                                          00002120
           02 MDNASC2C  PIC X.                                          00002130
           02 MDNASC2P  PIC X.                                          00002140
           02 MDNASC2H  PIC X.                                          00002150
           02 MDNASC2V  PIC X.                                          00002160
           02 MDNASC2O  PIC X(10).                                      00002170
           02 DFHMS1 OCCURS   5 TIMES .                                 00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MCONTRLA     PIC X.                                     00002200
             03 MCONTRLC     PIC X.                                     00002210
             03 MCONTRLP     PIC X.                                     00002220
             03 MCONTRLH     PIC X.                                     00002230
             03 MCONTRLV     PIC X.                                     00002240
             03 MCONTRLO     PIC X(18).                                 00002250
           02 DFHMS2 OCCURS   5 TIMES .                                 00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MVENTELA     PIC X.                                     00002280
             03 MVENTELC     PIC X.                                     00002290
             03 MVENTELP     PIC X.                                     00002300
             03 MVENTELH     PIC X.                                     00002310
             03 MVENTELV     PIC X.                                     00002320
             03 MVENTELO     PIC X(13).                                 00002330
           02 DFHMS3 OCCURS   5 TIMES .                                 00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MIDENTLA     PIC X.                                     00002360
             03 MIDENTLC     PIC X.                                     00002370
             03 MIDENTLP     PIC X.                                     00002380
             03 MIDENTLH     PIC X.                                     00002390
             03 MIDENTLV     PIC X.                                     00002400
             03 MIDENTLO     PIC X(20).                                 00002410
           02 DFHMS4 OCCURS   5 TIMES .                                 00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MNIMMOLA     PIC X.                                     00002440
             03 MNIMMOLC     PIC X.                                     00002450
             03 MNIMMOLP     PIC X.                                     00002460
             03 MNIMMOLH     PIC X.                                     00002470
             03 MNIMMOLV     PIC X.                                     00002480
             03 MNIMMOLO     PIC X(12).                                 00002490
           02 DFHMS5 OCCURS   5 TIMES .                                 00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MTINTERLA    PIC X.                                     00002520
             03 MTINTERLC    PIC X.                                     00002530
             03 MTINTERLP    PIC X.                                     00002540
             03 MTINTERLH    PIC X.                                     00002550
             03 MTINTERLV    PIC X.                                     00002560
             03 MTINTERLO    PIC X(5).                                  00002570
           02 DFHMS6 OCCURS   5 TIMES .                                 00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MNINTERLA    PIC X.                                     00002600
             03 MNINTERLC    PIC X.                                     00002610
             03 MNINTERLP    PIC X.                                     00002620
             03 MNINTERLH    PIC X.                                     00002630
             03 MNINTERLV    PIC X.                                     00002640
             03 MNINTERLO    PIC X(20).                                 00002650
           02 DFHMS7 OCCURS   5 TIMES .                                 00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MMONTANLA    PIC X.                                     00002680
             03 MMONTANLC    PIC X.                                     00002690
             03 MMONTANLP    PIC X.                                     00002700
             03 MMONTANLH    PIC X.                                     00002710
             03 MMONTANLV    PIC X.                                     00002720
             03 MMONTANLO    PIC X(10).                                 00002730
           02 DFHMS8 OCCURS   5 TIMES .                                 00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MDREDLA      PIC X.                                     00002760
             03 MDREDLC PIC X.                                          00002770
             03 MDREDLP PIC X.                                          00002780
             03 MDREDLH PIC X.                                          00002790
             03 MDREDLV PIC X.                                          00002800
             03 MDREDLO      PIC X(8).                                  00002810
           02 DFHMS9 OCCURS   5 TIMES .                                 00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MDREDALA     PIC X.                                     00002840
             03 MDREDALC     PIC X.                                     00002850
             03 MDREDALP     PIC X.                                     00002860
             03 MDREDALH     PIC X.                                     00002870
             03 MDREDALV     PIC X.                                     00002880
             03 MDREDALO     PIC X(8).                                  00002890
           02 DFHMS10 OCCURS   5 TIMES .                                00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MDNASCLA     PIC X.                                     00002920
             03 MDNASCLC     PIC X.                                     00002930
             03 MDNASCLP     PIC X.                                     00002940
             03 MDNASCLH     PIC X.                                     00002950
             03 MDNASCLV     PIC X.                                     00002960
             03 MDNASCLO     PIC X(8).                                  00002970
           02 DFHMS11 OCCURS   5 TIMES .                                00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MSTATLA      PIC X.                                     00003000
             03 MSTATLC PIC X.                                          00003010
             03 MSTATLP PIC X.                                          00003020
             03 MSTATLH PIC X.                                          00003030
             03 MSTATLV PIC X.                                          00003040
             03 MSTATLO      PIC X(5).                                  00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MLIBERRA  PIC X.                                          00003070
           02 MLIBERRC  PIC X.                                          00003080
           02 MLIBERRP  PIC X.                                          00003090
           02 MLIBERRH  PIC X.                                          00003100
           02 MLIBERRV  PIC X.                                          00003110
           02 MLIBERRO  PIC X(74).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAP  PIC X.                                          00003160
           02 MCODTRAH  PIC X.                                          00003170
           02 MCODTRAV  PIC X.                                          00003180
           02 MCODTRAO  PIC X(4).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCICSA    PIC X.                                          00003210
           02 MCICSC    PIC X.                                          00003220
           02 MCICSP    PIC X.                                          00003230
           02 MCICSH    PIC X.                                          00003240
           02 MCICSV    PIC X.                                          00003250
           02 MCICSO    PIC X(5).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MNETNAMA  PIC X.                                          00003280
           02 MNETNAMC  PIC X.                                          00003290
           02 MNETNAMP  PIC X.                                          00003300
           02 MNETNAMH  PIC X.                                          00003310
           02 MNETNAMV  PIC X.                                          00003320
           02 MNETNAMO  PIC X(8).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MSCREENA  PIC X.                                          00003350
           02 MSCREENC  PIC X.                                          00003360
           02 MSCREENP  PIC X.                                          00003370
           02 MSCREENH  PIC X.                                          00003380
           02 MSCREENV  PIC X.                                          00003390
           02 MSCREENO  PIC X(4).                                       00003400
                                                                                
