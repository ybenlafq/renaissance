      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * B2B : Suivi des commandes B2B                                   00000020
      ***************************************************************** 00000030
       01   EBB01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE1L   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE1F   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGE1I   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGE2I   PIC X(3).                                       00000210
           02 MLIGNEI OCCURS   14 TIMES .                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHOIXL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCHOIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCHOIXF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCHOIXI      PIC X.                                     00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDOSSIEL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MDOSSIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDOSSIEF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MDOSSIEI     PIC X(12).                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCDEL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MDCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDCDEF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MDCDEI  PIC X(8).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVENTEL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MVENTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVENTEF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MVENTEI      PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOML   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOMF   PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNOMI   PIC X(13).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCODICI      PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MFAMI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQUEL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MMARQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMARQUEF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MMARQUEI     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTEI   PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMODDELL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MMODDELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMODDELF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MMODDELI     PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSTATUTI     PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(73).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * B2B : Suivi des commandes B2B                                   00000880
      ***************************************************************** 00000890
       01   EBB01O REDEFINES EBB01I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGE1A   PIC X.                                          00001070
           02 MPAGE1C   PIC X.                                          00001080
           02 MPAGE1P   PIC X.                                          00001090
           02 MPAGE1H   PIC X.                                          00001100
           02 MPAGE1V   PIC X.                                          00001110
           02 MPAGE1O   PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGE2A   PIC X.                                          00001140
           02 MPAGE2C   PIC X.                                          00001150
           02 MPAGE2P   PIC X.                                          00001160
           02 MPAGE2H   PIC X.                                          00001170
           02 MPAGE2V   PIC X.                                          00001180
           02 MPAGE2O   PIC X(3).                                       00001190
           02 MLIGNEO OCCURS   14 TIMES .                               00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MCHOIXA      PIC X.                                     00001220
             03 MCHOIXC PIC X.                                          00001230
             03 MCHOIXP PIC X.                                          00001240
             03 MCHOIXH PIC X.                                          00001250
             03 MCHOIXV PIC X.                                          00001260
             03 MCHOIXO      PIC X.                                     00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MDOSSIEA     PIC X.                                     00001290
             03 MDOSSIEC     PIC X.                                     00001300
             03 MDOSSIEP     PIC X.                                     00001310
             03 MDOSSIEH     PIC X.                                     00001320
             03 MDOSSIEV     PIC X.                                     00001330
             03 MDOSSIEO     PIC X(12).                                 00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MDCDEA  PIC X.                                          00001360
             03 MDCDEC  PIC X.                                          00001370
             03 MDCDEP  PIC X.                                          00001380
             03 MDCDEH  PIC X.                                          00001390
             03 MDCDEV  PIC X.                                          00001400
             03 MDCDEO  PIC X(8).                                       00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MVENTEA      PIC X.                                     00001430
             03 MVENTEC PIC X.                                          00001440
             03 MVENTEP PIC X.                                          00001450
             03 MVENTEH PIC X.                                          00001460
             03 MVENTEV PIC X.                                          00001470
             03 MVENTEO      PIC X(7).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNOMA   PIC X.                                          00001500
             03 MNOMC   PIC X.                                          00001510
             03 MNOMP   PIC X.                                          00001520
             03 MNOMH   PIC X.                                          00001530
             03 MNOMV   PIC X.                                          00001540
             03 MNOMO   PIC X(13).                                      00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MCODICA      PIC X.                                     00001570
             03 MCODICC PIC X.                                          00001580
             03 MCODICP PIC X.                                          00001590
             03 MCODICH PIC X.                                          00001600
             03 MCODICV PIC X.                                          00001610
             03 MCODICO      PIC X(7).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MFAMA   PIC X.                                          00001640
             03 MFAMC   PIC X.                                          00001650
             03 MFAMP   PIC X.                                          00001660
             03 MFAMH   PIC X.                                          00001670
             03 MFAMV   PIC X.                                          00001680
             03 MFAMO   PIC X(5).                                       00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MMARQUEA     PIC X.                                     00001710
             03 MMARQUEC     PIC X.                                     00001720
             03 MMARQUEP     PIC X.                                     00001730
             03 MMARQUEH     PIC X.                                     00001740
             03 MMARQUEV     PIC X.                                     00001750
             03 MMARQUEO     PIC X(5).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MQTEA   PIC X.                                          00001780
             03 MQTEC   PIC X.                                          00001790
             03 MQTEP   PIC X.                                          00001800
             03 MQTEH   PIC X.                                          00001810
             03 MQTEV   PIC X.                                          00001820
             03 MQTEO   PIC X(2).                                       00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MMODDELA     PIC X.                                     00001850
             03 MMODDELC     PIC X.                                     00001860
             03 MMODDELP     PIC X.                                     00001870
             03 MMODDELH     PIC X.                                     00001880
             03 MMODDELV     PIC X.                                     00001890
             03 MMODDELO     PIC X(3).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MSTATUTA     PIC X.                                     00001920
             03 MSTATUTC     PIC X.                                     00001930
             03 MSTATUTP     PIC X.                                     00001940
             03 MSTATUTH     PIC X.                                     00001950
             03 MSTATUTV     PIC X.                                     00001960
             03 MSTATUTO     PIC X(5).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(73).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
