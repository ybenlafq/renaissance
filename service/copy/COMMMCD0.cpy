      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE DE MAJ DU STATUT D'EXPO        *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MCD00                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MCD0-LONG-COMMAREA PIC S9(4) COMP VALUE +58.                    
      *--                                                                       
       01  COMM-MCD0-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +58.                  
      *}                                                                        
       01  Z-COMMAREA-MCD0.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MCD0-I.                                                      
      *                                                                         
              03 COMM-MCD0-NSOCIETE        PIC    X(3).                         
              03 COMM-MCD0-NCODIC          PIC    X(7).                         
              03 COMM-MCD0-CFAM            PIC    X(5).                         
              03 COMM-MCD0-ASTATUT         PIC    X(1).                         
              03 COMM-MCD0-NSTATUT         PIC    X(1).                         
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MCD0-O.                                                      
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MCD0-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MCD0-OK           VALUE '0'.                           
                 88 COMM-MCD0-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MCD0-MSG             PIC    X(40).                        
                                                                                
