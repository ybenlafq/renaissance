      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      ******************************************************************
      *****   COPY MESSAGES MQ POUR TRAITEMENT DE LA GHE DEPUIS    *****
      *****    APPLICATION AS400 NMD STOCK LOCAL                   *****
      *
      *****  !!!  ATTENTION !!!                                    *****
      *****  MAINTENIR CETTE COPIE EN PHASE AVEC CELLE C�T� AS400  *****
      *****    (CCBMDMQGHE DANS FICHIER SOURCE EXPSRC/QLBLSRC)     *****
      ******************************************************************
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *
      ******************************************************************
      *  SSAAMMJJ / AUTEUR / REPAIRE / DESCRIPTIF MAINTENANCE          *
      *  -------- / ------ / ------- / ------------------------------- *
      *  20030625 / DSA010 / LP01    / MODIFICATIONS POUR PASSAGE MQHI *
      ******************************************************************
      *  POUR MHE01
      *
      * ENTETE QUEUE (CORRELID) + ENTETE STANDARD
      *    COPY MESSLENT.
      *****************************************************************
      *   COPY ENTETE DES MESSAGES MQ RECUPERES PAR LE HOST
      *****************************************************************
      *
       01  WS-MQ10.
      * ZONES D'ECHANGE AVEC MQHI (IDENTIFIANT MSG / CODE RET TRT)
         02 WS-QUEUE.
               10   MQ10-MSGID       PIC  X(24).
               10   MQ10-CORRELID    PIC  X(24).
         02 WS-CODRET                PIC  XX.
      * DESCRIPTION DU MSG
         02 MESSL.
      *    ENTETE STANDARD                                       LG=105C
           05  MESSL-ENTETE.
               10   MESSL-TYPE       PIC  X(03).
               10   MESSL-NSOCMSG    PIC  X(03).
               10   MESSL-NLIEUMSG   PIC  X(03).
               10   MESSL-NSOCDST    PIC  X(03).
               10   MESSL-NLIEUDST   PIC  X(03).
               10   MESSL-NORD       PIC  9(08).
               10   MESSL-LPROG      PIC  X(10).
               10   MESSL-DJOUR      PIC  X(08).
               10   MESSL-WSID       PIC  X(10).
               10   MESSL-USER       PIC  X(10).
               10   MESSL-CHRONO     PIC  9(07).
               10   MESSL-NBRMSG     PIC  9(07).
               10   MESSL-NBRENR     PIC  9(05).
               10   MESSL-TAILLE     PIC  9(05).
               10   MESSL-VERSION    PIC  X(02).
               10   MESSL-DSYST      PIC S9(13).
               10   MESSL-FILLER     PIC  X(05).
      *
      *---- FIN COPIE
           05 MESHE01-MESSAGE.
      *    Ent�te du traitement                         (LG.--> 71)
           10 HE01-ENTETE.
      *       Traitement
              15 HE01-TYPTRT             PIC 9.
                 88 HE01-DEPOT               VALUE 1.
                 88 HE01-ANNUL-DEPOT         VALUE 2.
                 88 HE01-EXPEDITION          VALUE 3.
      *       Soci�t�/Lieu d'exp�dition magasin
              15 HE01-NSOC               PIC X(03).
              15 HE01-NLIEU              PIC X(03).
      *       Centre de GHE
              15 HE01-GHE-LIEU           PIC X(05).
      *       Code retour.
              15 HE01-CDRET              PIC 9.
                 88 HE01-RET-OK              VALUE 0.
                 88 HE01-RET-KO              VALUE 1.
      *       Libell� message erreur
              15 HE01-LIBERR.
                 20 HE01-NSEQERR            PIC X(04).
                 20 FILLER                  PIC X.
                 20 HE01-MESSAGE            PIC X(53).
      *
      *    DEPOT DE PRODUIT / ANNULATION. (HOST : THE10 & THE11).
      *
           10 HE01-DEPOT-ANNUL.
      *
      *       Informations concernant le codic           (Lg.-->74)
              15 HE01-CODIC.
      *          Num�ro code article
                 20 HE01-NCODIC             PIC X(07).
      *          Code famille
                 20 HE01-CFAM               PIC X(05).
      *          Code marque
                 20 HE01-CMARQ              PIC X(05).
      *          R�g�rence du produit
                 20 HE01-LREF               PIC X(20).
      *          Num�ro de s�rie
                 20 HE01-NSERIE             PIC X(20).
      *          Soci�t� du document
                 20 HE01-NSOCDOC            PIC X(03).
      *          Lieu du document
                 20 HE01-NLIEUDOC           PIC X(03).
      *          type du document
                 20 HE01-CTYPDOC            PIC X(02).
      *          Num�ro de document
                 20 HE01-NUMDOC             PIC 9(09).
      *
      *       Informations concernant le traitement GHE  (LG.--> 90)
              15 HE01-GHE.
      *          Centre de GHE
                 20 HE01-CLIEUGHE           PIC X(05).
      *          Type de traitement
                 20 HE01-CTRAIT             PIC X(05).
      *          Type de HS
                 20 HE01-CTYPHS             PIC X(05).
      *          Code panne
                 20 HE01-CPANNE             PIC X(05).
      *          Libell� de la panne
                 20 HE01-LPANNE             PIC X(20).
      *          N� accord
                 20 HE01-NOACC              PIC X(10).
      *          Nom du signataire de l'accord
                 20 HE01-NMACC              PIC X(10).
      *          Soci�t� de la vente
                 20 HE01-NSOCVTE            PIC X(03).
      *          Lieu de la vente
                 20 HE01-NLIEUVTE           PIC X(03).
      *          N� de la vente
                 20 HE01-NVENTE             PIC X(08).
      *          Date de la vente
                 20 HE01-DVENTE             PIC X(08).
      *          Date de mise en GHE
                 20 HE01-DDEPOT             PIC X(08).
      *
      *       Informations concernant le client          (LG.--> 208)
              15 HE01-CLIENT.
      *          Titre du client
                 20 HE01-TITRE              PIC X(05).
      *          Nom du client
                 20 HE01-LNOM               PIC X(25).
      *          Pr�nom du client
                 20 HE01-LPRENOM            PIC X(15).
      *          B�timent
                 20 HE01-BATIMENT           PIC X(03).
      *          Escalier
                 20 HE01-ESCALIER           PIC X(03).
      *          Etage
                 20 HE01-ETAGE              PIC X(03).
      *          Porte
                 20 HE01-PORTE              PIC X(03).
      *          Compl�ment d'adresse
                 20 HE01-CMPADR             PIC X(32).
      *          N� de voie
                 20 HE01-CVOIE              PIC X(05).
      *          Type de voie
                 20 HE01-CTVOIE             PIC X(04).
      *          Libell� de la voie
                 20 HE01-LNOMVOIE           PIC X(21).
      *          Libell� de la commune
                 20 HE01-LCOMMUNE           PIC X(32).
      *          Code postal
                 20 HE01-CPOSTAL            PIC X(05).
      *          Bureau distributeur
                 20 HE01-LBUREAU            PIC X(26).
      *          T�l�phone domicile
                 20 HE01-TELDOM             PIC X(10).
      *          T�l�phone bureau
                 20 HE01-TELBUR             PIC X(10).
      *          N� de poste
                 20 HE01-NPOSTE             PIC X(05).
      *          Adresse � l'�tranger
                 20 HE01-WETRANGER          PIC X(01).
      *
      *       Informations concernant NASC d'origine     (LG.--> 17)
              15 HE01-NASC.
      *          Pr�fixe du num�ro de dossier NASC
                 20 HE01-PDOSNASC        PIC X(03).
      *          Num�ro de dossier NASC
                 20 HE01-DOSNASC         PIC 9(09).
      *          Type d'intervention NASC
                 20 HE01-CTYINASC        PIC X(05).
      *       Filler                                     (LG.--> 3146).
              15 FILLER                  PIC X(3129).
      *
      *    EXPEDITION. (HOST : THE15)                    (Lg.--> 3518).
      *
           10 HE01-MSG-EXPED  REDEFINES  HE01-DEPOT-ANNUL.
      *       N� de l'exp�dition
              15 HE01-NUMEXP             PIC 9(07).
      *       Date d'exp�dition
              15 HE01-DEXP               PIC X(08).
      *       Nombre de HS
              15 HE01-NBHS               PIC 9(03).
      *       Table des HS exp�di�s
              15 HE01-HS              OCCURS 250.
      *          Soci�t�
                 20 HE01-NSOCHS             PIC X(03).
      *          Lieu
                 20 HE01-LIEUHS             PIC X(03).
      *          Num�ro
                 20 HE01-NUMHS              PIC X(07).
      *          Code OK/KO (0=OK, 1=KO)
                 20 HE01-CD-OK-KO           PIC 9.
                    88 HE01-CD-OK               VALUE 0.
                    88 HE01-CD-KO               VALUE 1.
      *
      
