      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHC00   EHC00                                              00000020
      ***************************************************************** 00000030
       01   EHC00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNLIEUHCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUHCI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHSL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCHSF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCHSI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCIMPI    PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUTRL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNLIEUTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUTRF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUTRI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCIETEI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEUI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSLIEUL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSSLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSLIEUF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSSLIEUI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRTL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCTRTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRTF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTRTI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNENVOII  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(78).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: EHC00   EHC00                                              00000750
      ***************************************************************** 00000760
       01   EHC00O REDEFINES EHC00I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MZONCMDA  PIC X.                                          00000940
           02 MZONCMDC  PIC X.                                          00000950
           02 MZONCMDP  PIC X.                                          00000960
           02 MZONCMDH  PIC X.                                          00000970
           02 MZONCMDV  PIC X.                                          00000980
           02 MZONCMDO  PIC X(15).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNLIEUHCA      PIC X.                                     00001010
           02 MNLIEUHCC PIC X.                                          00001020
           02 MNLIEUHCP PIC X.                                          00001030
           02 MNLIEUHCH PIC X.                                          00001040
           02 MNLIEUHCV PIC X.                                          00001050
           02 MNLIEUHCO      PIC X(3).                                  00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNCHSA    PIC X.                                          00001080
           02 MNCHSC    PIC X.                                          00001090
           02 MNCHSP    PIC X.                                          00001100
           02 MNCHSH    PIC X.                                          00001110
           02 MNCHSV    PIC X.                                          00001120
           02 MNCHSO    PIC X(7).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MCIMPA    PIC X.                                          00001150
           02 MCIMPC    PIC X.                                          00001160
           02 MCIMPP    PIC X.                                          00001170
           02 MCIMPH    PIC X.                                          00001180
           02 MCIMPV    PIC X.                                          00001190
           02 MCIMPO    PIC X(4).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNLIEUTRA      PIC X.                                     00001220
           02 MNLIEUTRC PIC X.                                          00001230
           02 MNLIEUTRP PIC X.                                          00001240
           02 MNLIEUTRH PIC X.                                          00001250
           02 MNLIEUTRV PIC X.                                          00001260
           02 MNLIEUTRO      PIC X(3).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MSOCIETEA      PIC X.                                     00001290
           02 MSOCIETEC PIC X.                                          00001300
           02 MSOCIETEP PIC X.                                          00001310
           02 MSOCIETEH PIC X.                                          00001320
           02 MSOCIETEV PIC X.                                          00001330
           02 MSOCIETEO      PIC X(3).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLIEUA    PIC X.                                          00001360
           02 MLIEUC    PIC X.                                          00001370
           02 MLIEUP    PIC X.                                          00001380
           02 MLIEUH    PIC X.                                          00001390
           02 MLIEUV    PIC X.                                          00001400
           02 MLIEUO    PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MSSLIEUA  PIC X.                                          00001430
           02 MSSLIEUC  PIC X.                                          00001440
           02 MSSLIEUP  PIC X.                                          00001450
           02 MSSLIEUH  PIC X.                                          00001460
           02 MSSLIEUV  PIC X.                                          00001470
           02 MSSLIEUO  PIC X(3).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCTRTA    PIC X.                                          00001500
           02 MCTRTC    PIC X.                                          00001510
           02 MCTRTP    PIC X.                                          00001520
           02 MCTRTH    PIC X.                                          00001530
           02 MCTRTV    PIC X.                                          00001540
           02 MCTRTO    PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNENVOIA  PIC X.                                          00001570
           02 MNENVOIC  PIC X.                                          00001580
           02 MNENVOIP  PIC X.                                          00001590
           02 MNENVOIH  PIC X.                                          00001600
           02 MNENVOIV  PIC X.                                          00001610
           02 MNENVOIO  PIC X(7).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBERRA  PIC X.                                          00001640
           02 MLIBERRC  PIC X.                                          00001650
           02 MLIBERRP  PIC X.                                          00001660
           02 MLIBERRH  PIC X.                                          00001670
           02 MLIBERRV  PIC X.                                          00001680
           02 MLIBERRO  PIC X(78).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCODTRAA  PIC X.                                          00001710
           02 MCODTRAC  PIC X.                                          00001720
           02 MCODTRAP  PIC X.                                          00001730
           02 MCODTRAH  PIC X.                                          00001740
           02 MCODTRAV  PIC X.                                          00001750
           02 MCODTRAO  PIC X(4).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCICSA    PIC X.                                          00001780
           02 MCICSC    PIC X.                                          00001790
           02 MCICSP    PIC X.                                          00001800
           02 MCICSH    PIC X.                                          00001810
           02 MCICSV    PIC X.                                          00001820
           02 MCICSO    PIC X(5).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNETNAMA  PIC X.                                          00001850
           02 MNETNAMC  PIC X.                                          00001860
           02 MNETNAMP  PIC X.                                          00001870
           02 MNETNAMH  PIC X.                                          00001880
           02 MNETNAMV  PIC X.                                          00001890
           02 MNETNAMO  PIC X(8).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSCREENA  PIC X.                                          00001920
           02 MSCREENC  PIC X.                                          00001930
           02 MSCREENP  PIC X.                                          00001940
           02 MSCREENH  PIC X.                                          00001950
           02 MSCREENV  PIC X.                                          00001960
           02 MSCREENO  PIC X(4).                                       00001970
                                                                                
