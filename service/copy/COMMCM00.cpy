      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CM00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-CM00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC 9(02).                                    
             05 COMM-DATE-SEMAA   PIC 9(02).                                    
             05 COMM-DATE-SEMNU   PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 200 PIC X(1).                                
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3674          
      *                                                                         
      *            TRANSACTION CM00 : CALENDRIER MAGASIN              *         
      *                                                                         
      *------------------------------ ZONE COMMUNE -----------------            
      *****************************************************************         
      *   SAISIE CALENDRIER                                                     
      *   DEFINIS EN FONCTION DE LA COMPETENCE                                  
      *****************************************************************         
          02 COMM-CM00-APPLI.                                                   
      * SAISIE  : SOCIETE                                                       
      *           LIEU                                                          
      * OPTION  : 1 CALENDRIER                                                  
      *           2 ELIGIBILIT�                                                 
             03 COMM-CM00-NSOCIETE       PIC X(3).                              
             03 COMM-CM00-NLIEU          PIC X(3).                              
             03 COMM-CM00-LLIEU          PIC X(16).                             
             03 COMM-CM00-COPTION        PIC X(1).                              
             03 COMM-CM00-CTYPSOC        PIC X(1).                              
             03 COMM-CM00-SEM-REF        PIC X(5).                              
             03 COMM-CM00-SEM-INIT       PIC X(5).                              
             03 COMM-CM00-NB-SEM         PIC 99.                                
             03 COMM-CM00-PASSAGE        PIC X.                                 
             03 COMM-CM00-NSOCIETE-AV    PIC X(3).                              
             03 COMM-CM00-NLIEU-AV       PIC X(3).                              
             03 COMM-CM00-LLIEU-AV       PIC X(16).                             
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
             03 COMM-DATE-QNTA-W     PIC 999.                                   
             03 COMM-DATE-QNT0-W     PIC 99999.                                 
CR3005       03 COMM-DATE-QNT0-DEB   PIC 99999.                                 
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
             03 COMM-DATE-JSM-W      PIC 9.                                     
      *PROGRAMME D'ORIGINE                                                      
             03 COMM-CM00-PROG           PIC X(5).                              
      *PARAMETRE HEURE PAR JOUR                                                 
             03 COMM-CM00-HEURE-JOUR.                                           
                04 COMM-CM00-HJ          PIC 99V99.                             
CR1104* LA LONGUEUR DU TABLEAU SUIVANT DEPASS 4099 D' O�                        
CR1104*      03 COMM-CM00-FILLER         PIC X(4099).                           
CR2504*      03 COMM-CM00-FILLER         PIC X(8000).                           
CR1604*  DATE DE DEBUT DE LA PERIODE DE CONSULTATION                            
             03 COMM-CM00-PERDEB.                                               
                05 COMM-CM00-DSS       PIC X(2).                                
                05 COMM-CM00-DAA       PIC X(2).                                
                05 COMM-CM00-DMM       PIC X(2).                                
                05 COMM-CM00-DJJ       PIC X(2).                                
             03    COMM-CM00-DLC       PIC X(3).                                
      *  DATE DE DEBUT PERIODE EQUIVALENTE  ANNEE - 1                           
             03 COMM-CM00-PERDEBEN1.                                            
                05 COMM-CM00-DSSEN1    PIC X(2).                                
                05 COMM-CM00-DAAEN1    PIC X(2).                                
                05 COMM-CM00-DMMEN1    PIC X(2).                                
                05 COMM-CM00-DJJEN1    PIC X(2).                                
             03    COMM-CM00-DLCEN1    PIC X(3).                                
      *  DATE DE DEBUT DE PERIODE DE CONSULTATION MEME JOUR ANNEE-1             
             03 COMM-CM00-PERDEBJN1.                                            
                05 COMM-CM00-DSSJN1    PIC X(2).                                
                05 COMM-CM00-DAAJN1    PIC X(2).                                
                05 COMM-CM00-DMMJN1    PIC X(2).                                
                05 COMM-CM00-DJJJN1    PIC X(2).                                
             03    COMM-CM00-DLCJN1    PIC X(3).                                
      *  DATE DE FIN DE LA PERIODE DE CONSULTATION                              
             03 COMM-CM00-PERFIN.                                               
                05 COMM-CM00-FSS       PIC X(2).                                
                05 COMM-CM00-FAA       PIC X(2).                                
                05 COMM-CM00-FMM       PIC X(2).                                
                05 COMM-CM00-FJJ       PIC X(2).                                
             03    COMM-CM00-FLC       PIC X(3).                                
      *  DATE DE FIN PERIODE EQUIVALENTE  ANNEE - 1                             
             03 COMM-CM00-PERFINEN1.                                            
                05 COMM-CM00-FSSEN1    PIC X(2).                                
                05 COMM-CM00-FAAEN1    PIC X(2).                                
                05 COMM-CM00-FMMEN1    PIC X(2).                                
                05 COMM-CM00-FJJEN1    PIC X(2).                                
             03    COMM-CM00-FLCEN1    PIC X(3).                                
      *  DATE DE FIN DE PERIODE DE CONSULTATION MEME JOUR ANNEE-1               
             03 COMM-CM00-PERFINJN1.                                            
                05 COMM-CM00-FSSJN1    PIC X(2).                                
                05 COMM-CM00-FAAJN1    PIC X(2).                                
                05 COMM-CM00-FMMJN1    PIC X(2).                                
                05 COMM-CM00-FJJJN1    PIC X(2).                                
             03    COMM-CM00-FLCJN1    PIC X(3).                                
      *  DATE DE DEBUT DE SEMAINE DE LA PERIODE DE CONSULTATION                 
             03 COMM-CM00-PERDEBSEM.                                            
                05 COMM-CM00-SSS       PIC X(2).                                
                05 COMM-CM00-SAA       PIC X(2).                                
                05 COMM-CM00-SMM       PIC X(2).                                
                05 COMM-CM00-SJJ       PIC X(2).                                
             03    COMM-CM00-SLC       PIC X(3).                                
      *  DATE DE DEBUT DE MOIS DE LA PERIODE DE CONSULTATION                    
             03 COMM-CM00-PERDEBMOIS.                                           
                05 COMM-CM00-MSS       PIC X(2).                                
                05 COMM-CM00-MAA       PIC X(2).                                
                05 COMM-CM00-MMM       PIC X(2).                                
                05 COMM-CM00-MJJ       PIC X(2).                                
                05 COMM-CM00-MLC       PIC X(3).                                
             03 COMM-CM00-FILLER         PIC X(5000).                           
      *****************************************************************         
      *                                                               *         
      *                    ********************                       *         
      *                    * COMM-CM00-FILLER *                       *         
      *                    *--------+---------*                       *         
      *                             !                                 *         
      *                +------------+----------+                      *         
      *       *--------!--------+              !                      *         
      *       ! COMM-CM01-DONNEE!              !                      *         
      *       *--------!--------+              !                      *         
      *                !                       !                      *         
      *       *--------!--------+              !                      *         
      *       !                 !              !                      *         
      *       *--------+--------+              !                      *         
      *                !                       !                      *         
      *    +-----------!-----------+           !                      *         
      *                                        !                      *         
      *                                        !                      *         
      *                               +--------!--------+             *         
      *                               !                 !             *         
      *                               +--------!--------+             *         
      *                                        !                      *         
      *                               +--------!--------+             *         
      *                               !                 !             *         
      *                               +-----------------+             *         
      *                                        !                      *         
      *                       +----------------!------------+         *         
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               !                  !     !   !                 !*         
      *               +-------!----------+     !   +--------!--------+          
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               !                  !     !   !                 !          
      *               +------------------+     !   +-----------------+          
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               !                  !     !   !                 !          
      *               +------------------+     !   +-----------------+          
      *                                        !                                
      *                               +--------!---------+            *         
      *                               ! COMM-AC0X-DONNEE !            *         
      *                               +------------------+            *         
      *                                                               *         
      *****************************************************************         
      *   SAISIE PERIODE OUVERTURE - FERMETURE MAGASIN                *         
      *                                                               *         
      *****************************************************************         
             03 COMM-CM01-APPLI          REDEFINES COMM-CM00-FILLER.            
      *  AFFICHAGE A PARTIR DE                                                  
                05 COMM-CM01-REF-SEM.                                           
                   07 COMM-REF-SS-SEM        PIC XX.                            
                   07 COMM-REF-AA-SEM        PIC XX.                            
                   07 COMM-REF-SEM           PIC XX.                            
      *  PREMIERE DATE DE LA MAP                                                
                05 COMM-CM01-DATE-DEB        PIC X(8).                          
      *  DERNIERE DATE DE LA MAP                                                
                05 COMM-CM01-DATE-FIN        PIC X(8).                          
      *  DATE SUIVANTE                                                          
                05 COMM-CM01-DATE-SUIV       PIC X(8).                          
      *  EXISTENCE TS                                                           
                05 COMM-CM01-EXISTE-TS        PIC X.                            
      *  NB DE PAGE DE LA TS                                                    
                05 COMM-CM01-NB-PAGE          PIC 99.                           
      *  NB DE LIGNE DE LA TS                                                   
                05 COMM-CM01-NB-LIG-TS        PIC 999.                          
      *  N� ITEM DE LA TS                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-CM01-ITEM             PIC S9(4) COMP.                   
      *--                                                                       
                05 COMM-CM01-ITEM             PIC S9(4) COMP-5.                 
      *}                                                                        
      *  TABLEAU OUVERTURE - FERMETURE MAGASIN                                  
                05 COMM-CM01-TAB-OUV    OCCURS 16.                              
      *  CONTENU D'UNE LIGNE                                                    
CR1904              07 COMM-CM01-LIG          OCCURS 2.                         
      *  MOIS    OUVERTURE - FERMETURE MAGASIN                                  
                       10 COMM-CM01-SEMAINE              PIC X(5).              
      *  PREMIERE LETTRE DU JOUR DE LA SEMAINE                                  
                       10 COMM-CM01-LIB-JOUR             PIC X.                 
      *  DATE    OUVERTURE - FERMETURE MAGASIN                                  
                       10 COMM-CM01-DATE                 PIC X(8).              
      *  OUVERTURE MAGASIN DANS LE MOIS                                         
                       10 COMM-CM01-HOUV      OCCURS 2   PIC X(5).              
      *  FERMETURE MAGASIN DANS LE MOIS                                         
                       10 COMM-CM01-HFER      OCCURS 2   PIC X(5).              
      *  INDICATEUR DE MODIFICATION                                             
                05 COMM-CM01-MODIFIE          PIC X.                            
CR1206          05 COMM-CM01-SEM-REF          PIC X(4).                         
                05 COMM-CM01-FILLER           PIC X(3779).                      
                                                                                
