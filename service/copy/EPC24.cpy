      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ACC: LISTE CARTES CADEAUX B2B                                   00000020
      ***************************************************************** 00000030
       01   EPC24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSTATUTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEI    PIC X(12).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCDEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDCDEI    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOMPTEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCOMPTEI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSL      COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MRSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MRSF      PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MRSI      PIC X(25).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHRONOL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNCHRONOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCHRONOF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCHRONOI      PIC X(15).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTREMISEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTREMISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTREMISEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTREMISEI      PIC X(5).                                  00000490
           02 MLIGNEI OCCURS   4 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCARTEL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCCARTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCARTEF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCCARTEI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLIBELLEI    PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFLOGOL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MREFLOGOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MREFLOGOF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MREFLOGOI    PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPUNITL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MPUNITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPUNITF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MPUNITI      PIC X(9).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTEI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTTCL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MMTTTCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTTCF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MMTTTCI      PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEORIL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCDEORIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCDEORIF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCDEORII     PIC X(12).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSERIEDL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNSERIEDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSERIEDF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNSERIEDI    PIC X(13).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSERIEFL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNSERIEFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSERIEFF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSERIEFI    PIC X(13).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEERRL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQTEERRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTEERRF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQTEERRI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(74).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * ACC: LISTE CARTES CADEAUX B2B                                   00001120
      ***************************************************************** 00001130
       01   EPC24O REDEFINES EPC24I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNBPA     PIC X.                                          00001380
           02 MNBPC     PIC X.                                          00001390
           02 MNBPP     PIC X.                                          00001400
           02 MNBPH     PIC X.                                          00001410
           02 MNBPV     PIC X.                                          00001420
           02 MNBPO     PIC X(3).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MSTATUTA  PIC X.                                          00001450
           02 MSTATUTC  PIC X.                                          00001460
           02 MSTATUTP  PIC X.                                          00001470
           02 MSTATUTH  PIC X.                                          00001480
           02 MSTATUTV  PIC X.                                          00001490
           02 MSTATUTO  PIC X(10).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNCDEA    PIC X.                                          00001520
           02 MNCDEC    PIC X.                                          00001530
           02 MNCDEP    PIC X.                                          00001540
           02 MNCDEH    PIC X.                                          00001550
           02 MNCDEV    PIC X.                                          00001560
           02 MNCDEO    PIC X(12).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MDCDEA    PIC X.                                          00001590
           02 MDCDEC    PIC X.                                          00001600
           02 MDCDEP    PIC X.                                          00001610
           02 MDCDEH    PIC X.                                          00001620
           02 MDCDEV    PIC X.                                          00001630
           02 MDCDEO    PIC X(10).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNCOMPTEA      PIC X.                                     00001660
           02 MNCOMPTEC PIC X.                                          00001670
           02 MNCOMPTEP PIC X.                                          00001680
           02 MNCOMPTEH PIC X.                                          00001690
           02 MNCOMPTEV PIC X.                                          00001700
           02 MNCOMPTEO      PIC X(8).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MRSA      PIC X.                                          00001730
           02 MRSC      PIC X.                                          00001740
           02 MRSP      PIC X.                                          00001750
           02 MRSH      PIC X.                                          00001760
           02 MRSV      PIC X.                                          00001770
           02 MRSO      PIC X(25).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNCHRONOA      PIC X.                                     00001800
           02 MNCHRONOC PIC X.                                          00001810
           02 MNCHRONOP PIC X.                                          00001820
           02 MNCHRONOH PIC X.                                          00001830
           02 MNCHRONOV PIC X.                                          00001840
           02 MNCHRONOO      PIC X(15).                                 00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MTREMISEA      PIC X.                                     00001870
           02 MTREMISEC PIC X.                                          00001880
           02 MTREMISEP PIC X.                                          00001890
           02 MTREMISEH PIC X.                                          00001900
           02 MTREMISEV PIC X.                                          00001910
           02 MTREMISEO      PIC X(5).                                  00001920
           02 MLIGNEO OCCURS   4 TIMES .                                00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MCCARTEA     PIC X.                                     00001950
             03 MCCARTEC     PIC X.                                     00001960
             03 MCCARTEP     PIC X.                                     00001970
             03 MCCARTEH     PIC X.                                     00001980
             03 MCCARTEV     PIC X.                                     00001990
             03 MCCARTEO     PIC X(7).                                  00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MLIBELLEA    PIC X.                                     00002020
             03 MLIBELLEC    PIC X.                                     00002030
             03 MLIBELLEP    PIC X.                                     00002040
             03 MLIBELLEH    PIC X.                                     00002050
             03 MLIBELLEV    PIC X.                                     00002060
             03 MLIBELLEO    PIC X(20).                                 00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MREFLOGOA    PIC X.                                     00002090
             03 MREFLOGOC    PIC X.                                     00002100
             03 MREFLOGOP    PIC X.                                     00002110
             03 MREFLOGOH    PIC X.                                     00002120
             03 MREFLOGOV    PIC X.                                     00002130
             03 MREFLOGOO    PIC X(20).                                 00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MPUNITA      PIC X.                                     00002160
             03 MPUNITC PIC X.                                          00002170
             03 MPUNITP PIC X.                                          00002180
             03 MPUNITH PIC X.                                          00002190
             03 MPUNITV PIC X.                                          00002200
             03 MPUNITO      PIC X(9).                                  00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MQTEA   PIC X.                                          00002230
             03 MQTEC   PIC X.                                          00002240
             03 MQTEP   PIC X.                                          00002250
             03 MQTEH   PIC X.                                          00002260
             03 MQTEV   PIC X.                                          00002270
             03 MQTEO   PIC X(5).                                       00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MMTTTCA      PIC X.                                     00002300
             03 MMTTTCC PIC X.                                          00002310
             03 MMTTTCP PIC X.                                          00002320
             03 MMTTTCH PIC X.                                          00002330
             03 MMTTTCV PIC X.                                          00002340
             03 MMTTTCO      PIC X(8).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MCDEORIA     PIC X.                                     00002370
             03 MCDEORIC     PIC X.                                     00002380
             03 MCDEORIP     PIC X.                                     00002390
             03 MCDEORIH     PIC X.                                     00002400
             03 MCDEORIV     PIC X.                                     00002410
             03 MCDEORIO     PIC X(12).                                 00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MNSERIEDA    PIC X.                                     00002440
             03 MNSERIEDC    PIC X.                                     00002450
             03 MNSERIEDP    PIC X.                                     00002460
             03 MNSERIEDH    PIC X.                                     00002470
             03 MNSERIEDV    PIC X.                                     00002480
             03 MNSERIEDO    PIC X(13).                                 00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MNSERIEFA    PIC X.                                     00002510
             03 MNSERIEFC    PIC X.                                     00002520
             03 MNSERIEFP    PIC X.                                     00002530
             03 MNSERIEFH    PIC X.                                     00002540
             03 MNSERIEFV    PIC X.                                     00002550
             03 MNSERIEFO    PIC X(13).                                 00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MQTEERRA     PIC X.                                     00002580
             03 MQTEERRC     PIC X.                                     00002590
             03 MQTEERRP     PIC X.                                     00002600
             03 MQTEERRH     PIC X.                                     00002610
             03 MQTEERRV     PIC X.                                     00002620
             03 MQTEERRO     PIC X(5).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(74).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
