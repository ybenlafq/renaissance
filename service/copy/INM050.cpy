      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM050 AU 05/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,03,BI,A,                          *        
      *                           29,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM050.                                                        
            05 NOMETAT-INM050           PIC X(6) VALUE 'INM050'.                
            05 RUPTURES-INM050.                                                 
           10 INM050-DATETR             PIC X(08).                      007  008
           10 INM050-NSOC               PIC X(03).                      015  003
           10 INM050-NLIEU              PIC X(03).                      018  003
           10 INM050-CMOPAI             PIC X(05).                      021  005
           10 INM050-NCAISS             PIC X(03).                      026  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM050-SEQUENCE           PIC S9(04) COMP.                029  002
      *--                                                                       
           10 INM050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM050.                                                   
           10 INM050-CDEVAUT            PIC X(03).                      031  003
           10 INM050-CDEVRF             PIC X(03).                      034  003
           10 INM050-CIMPAI             PIC X(16).                      037  016
           10 INM050-DATED              PIC X(10).                      053  010
           10 INM050-LDEVAUT            PIC X(05).                      063  005
           10 INM050-LDEVRF             PIC X(05).                      068  005
           10 INM050-LMOPAI             PIC X(20).                      073  020
           10 INM050-NTRANS             PIC X(08).                      093  008
           10 INM050-CUMUL-AU-PDPAIE    PIC S9(10)V9(2) COMP-3.         101  007
           10 INM050-PDPAIE             PIC S9(08)V9(2) COMP-3.         108  006
            05 FILLER                      PIC X(399).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM050-LONG           PIC S9(4)   COMP  VALUE +113.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM050-LONG           PIC S9(4) COMP-5  VALUE +113.           
                                                                                
      *}                                                                        
