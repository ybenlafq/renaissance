      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **********************************************************                
      *   COPY DU FICHIER POUR BQC140 : EXTRACTION CARTES T 7&8                 
      *   LG = 250                                                              
      **********************************************************                
      *                                                                         
       01  DSECT-FQC013.                                                        
           02  FQC013-TCARTE         PIC X(01).                                 
           02  FQC013-NSOCLIEU       PIC X(06).                                 
           02  FQC013-INTER          PIC 9(10).                                 
           02  FQC013-CFAM           PIC X(05).                                 
           02  FQC013-CMARQ          PIC X(05).                                 
           02  FQC013-REF            PIC X(10).                                 
           02  FQC013-HOTESSE        PIC X(03).                                 
           02  FQC013-TECH           PIC X(03).                                 
           02  FQC013-CGAR           PIC X(03).                                 
           02  FQC013-INTERVENTION   PIC X(02).                                 
           02  FQC013-DELAI          PIC X(03).                                 
           02  FQC013-CTITRENOM      PIC X(05).                                 
           02  FQC013-LNOM           PIC X(25).                                 
           02  FQC013-LPRENOM        PIC X(15).                                 
           02  FQC013-LCMPAD1        PIC X(32).                                 
           02  FQC013-ADRESSE        PIC X(32).                                 
           02  FQC013-CPOSTAL        PIC X(05).                                 
           02  FQC013-LCOMMUNE       PIC X(32).                                 
           02  FQC013-TELDOM         PIC X(10).                                 
           02  FQC013-DDELIV         PIC X(08).                                 
           02  FQC013-COMPTOIR       PIC X(06).                                 
           02  FQC013-CTYPSERV       PIC X(05).                                 
           02  FQC013-CENREG         PIC X(05).                                 
           02  FQC013-TOP-MAIL       PIC X(01).                                 
           02  FILLER                PIC X(09).                                 
           02  FQC013-WEMPORTE       PIC X(1).                                  
           02  FQC013-PVTOTAL        PIC S9(7)V9(2) USAGE COMP-3.               
           02  FQC013-NSEQNQ         PIC S9(5)V USAGE COMP-3.                   
           02  FQC013-EMAIL          PIC X(50).                                 
           02  FQC013-NGSM           PIC X(10).                                 
           02  FQC013-IDCLIENT       PIC X(08).                                 
           02 FILLER                 PIC X(32).                                 
                                                                                
