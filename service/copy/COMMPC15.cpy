      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MPC15                                                 
      * APPELLE PAR LINK DANS TMC00                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MPC15.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
              10 COMM-MPC15-AAMMJJ       PIC X(06).                     01560   
              10 COMM-MPC15-JJMMSSAA     PIC X(08).                     01560   
              10 COMM-MPC15-INDMAX       PIC S9(5) COMP-3.              01560   
              10 COMM-MPC15-HEURE        PIC 9(08).                     01560   
         05  COMM-MPC15-DEMAND-EDIT     PIC X(01) .                             
              88 COMM-MPC15-EDIT-TOUT     VALUE '1'.                            
         05  COMM-MPC15-FANOMALIE       PIC X(01) .                             
              88 COMM-MPC15-ANOMALIE      VALUE '1'.                            
         05  COMM-MPC15-NSOC            PIC X(03) .                             
         05  COMM-MPC15-DATDEB          PIC X(08).                              
         05  COMM-MPC15-DATFIN          PIC X(08).                              
      *      MESSAGE ERREUR                                                     
         05 COMM-MPC15-RETOUR.                                                  
             10 COMM-MPC15-CODRET        PIC X(01).                             
                  88  COMM-MPC15-OK     VALUE ' '.                              
                  88  COMM-MPC15-ERREUR VALUE '1'.                              
             10 COMM-MPC15-LIBERR        PIC X(60).                             
         05 FILLER                       PIC X(30).                             
                                                                                
