      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FR10-RECORD.                                                      
           05 TS-FR10-TABLE            OCCURS 10.                               
              10 TS-FR10-NS              PIC X(10).                             
              10 TS-FR10-MDATE           PIC X(08).                             
              10 TS-FR10-MFRAN           PIC X(01).                             
              10 TS-FR10-MCODIC          PIC X(07).                             
              10 TS-FR10-MFAM            PIC X(05).                             
              10 TS-FR10-MMARQ           PIC X(05).                             
              10 TS-FR10-MREF            PIC X(20).                             
              10 TS-FR10-MSTOCK          PIC S9(9)V USAGE COMP-3.               
              10 TS-FR10-DTRAIT          PIC X(08).                             
              10 TS-FR10-MMNT            PIC S9(7)V9(2) USAGE COMP-3.           
                                                                                
