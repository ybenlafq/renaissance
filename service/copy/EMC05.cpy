      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Affectation des chassis aux ref                                 00000020
      ***************************************************************** 00000030
       01   EMC05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMSELL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCFAMSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFAMSELF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMSELI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQSELL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCMARQSELL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMARQSELF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMARQSELI     PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLFAMI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMARQI   PIC X(20).                                      00000370
           02 MTABLGRPI OCCURS   10 TIMES .                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCODICI     PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQUEL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCMARQUEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCMARQUEF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCMARQUEI    PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFI  PIC X(18).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDCREATL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDCREATF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDCREATI     PIC X(10).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCHASSL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCCHASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCHASSF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCCHASSI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHASSL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLCHASSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHASSF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLCHASSI     PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICNWL     COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MNCODICNWL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICNWF     PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNCODICNWI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMNWL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCFAMNWL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFAMNWF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCFAMNWI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQNWL      COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MCMARQNWL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMARQNWF      PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCMARQNWI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFNWL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLREFNWL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFNWF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLREFNWI  PIC X(18).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATNWL     COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MDCREATNWL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDCREATNWF     PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MDCREATNWI     PIC X(10).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCHASSNWL     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MCCHASSNWL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCHASSNWF     PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCCHASSNWI     PIC X(7).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHASSNWL     COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MLCHASSNWL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCHASSNWF     PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLCHASSNWI     PIC X(20).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(78).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * Affectation des chassis aux ref                                 00001160
      ***************************************************************** 00001170
       01   EMC05O REDEFINES EMC05I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 NPAGEA    PIC X.                                          00001350
           02 NPAGEC    PIC X.                                          00001360
           02 NPAGEP    PIC X.                                          00001370
           02 NPAGEH    PIC X.                                          00001380
           02 NPAGEV    PIC X.                                          00001390
           02 NPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 NBPAGEA   PIC X.                                          00001420
           02 NBPAGEC   PIC X.                                          00001430
           02 NBPAGEP   PIC X.                                          00001440
           02 NBPAGEH   PIC X.                                          00001450
           02 NBPAGEV   PIC X.                                          00001460
           02 NBPAGEO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCFAMSELA      PIC X.                                     00001490
           02 MCFAMSELC PIC X.                                          00001500
           02 MCFAMSELP PIC X.                                          00001510
           02 MCFAMSELH PIC X.                                          00001520
           02 MCFAMSELV PIC X.                                          00001530
           02 MCFAMSELO      PIC X(5).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCMARQSELA     PIC X.                                     00001560
           02 MCMARQSELC     PIC X.                                     00001570
           02 MCMARQSELP     PIC X.                                     00001580
           02 MCMARQSELH     PIC X.                                     00001590
           02 MCMARQSELV     PIC X.                                     00001600
           02 MCMARQSELO     PIC X(5).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLFAMA    PIC X.                                          00001630
           02 MLFAMC    PIC X.                                          00001640
           02 MLFAMP    PIC X.                                          00001650
           02 MLFAMH    PIC X.                                          00001660
           02 MLFAMV    PIC X.                                          00001670
           02 MLFAMO    PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLMARQA   PIC X.                                          00001700
           02 MLMARQC   PIC X.                                          00001710
           02 MLMARQP   PIC X.                                          00001720
           02 MLMARQH   PIC X.                                          00001730
           02 MLMARQV   PIC X.                                          00001740
           02 MLMARQO   PIC X(20).                                      00001750
           02 MTABLGRPO OCCURS   10 TIMES .                             00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNCODICA     PIC X.                                     00001780
             03 MNCODICC     PIC X.                                     00001790
             03 MNCODICP     PIC X.                                     00001800
             03 MNCODICH     PIC X.                                     00001810
             03 MNCODICV     PIC X.                                     00001820
             03 MNCODICO     PIC X(7).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCFAMA  PIC X.                                          00001850
             03 MCFAMC  PIC X.                                          00001860
             03 MCFAMP  PIC X.                                          00001870
             03 MCFAMH  PIC X.                                          00001880
             03 MCFAMV  PIC X.                                          00001890
             03 MCFAMO  PIC X(5).                                       00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MCMARQUEA    PIC X.                                     00001920
             03 MCMARQUEC    PIC X.                                     00001930
             03 MCMARQUEP    PIC X.                                     00001940
             03 MCMARQUEH    PIC X.                                     00001950
             03 MCMARQUEV    PIC X.                                     00001960
             03 MCMARQUEO    PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MLREFA  PIC X.                                          00001990
             03 MLREFC  PIC X.                                          00002000
             03 MLREFP  PIC X.                                          00002010
             03 MLREFH  PIC X.                                          00002020
             03 MLREFV  PIC X.                                          00002030
             03 MLREFO  PIC X(18).                                      00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MDCREATA     PIC X.                                     00002060
             03 MDCREATC     PIC X.                                     00002070
             03 MDCREATP     PIC X.                                     00002080
             03 MDCREATH     PIC X.                                     00002090
             03 MDCREATV     PIC X.                                     00002100
             03 MDCREATO     PIC X(10).                                 00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCCHASSA     PIC X.                                     00002130
             03 MCCHASSC     PIC X.                                     00002140
             03 MCCHASSP     PIC X.                                     00002150
             03 MCCHASSH     PIC X.                                     00002160
             03 MCCHASSV     PIC X.                                     00002170
             03 MCCHASSO     PIC X(7).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MLCHASSA     PIC X.                                     00002200
             03 MLCHASSC     PIC X.                                     00002210
             03 MLCHASSP     PIC X.                                     00002220
             03 MLCHASSH     PIC X.                                     00002230
             03 MLCHASSV     PIC X.                                     00002240
             03 MLCHASSO     PIC X(20).                                 00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNCODICNWA     PIC X.                                     00002270
           02 MNCODICNWC     PIC X.                                     00002280
           02 MNCODICNWP     PIC X.                                     00002290
           02 MNCODICNWH     PIC X.                                     00002300
           02 MNCODICNWV     PIC X.                                     00002310
           02 MNCODICNWO     PIC X(7).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCFAMNWA  PIC X.                                          00002340
           02 MCFAMNWC  PIC X.                                          00002350
           02 MCFAMNWP  PIC X.                                          00002360
           02 MCFAMNWH  PIC X.                                          00002370
           02 MCFAMNWV  PIC X.                                          00002380
           02 MCFAMNWO  PIC X(5).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCMARQNWA      PIC X.                                     00002410
           02 MCMARQNWC PIC X.                                          00002420
           02 MCMARQNWP PIC X.                                          00002430
           02 MCMARQNWH PIC X.                                          00002440
           02 MCMARQNWV PIC X.                                          00002450
           02 MCMARQNWO      PIC X(5).                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MLREFNWA  PIC X.                                          00002480
           02 MLREFNWC  PIC X.                                          00002490
           02 MLREFNWP  PIC X.                                          00002500
           02 MLREFNWH  PIC X.                                          00002510
           02 MLREFNWV  PIC X.                                          00002520
           02 MLREFNWO  PIC X(18).                                      00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MDCREATNWA     PIC X.                                     00002550
           02 MDCREATNWC     PIC X.                                     00002560
           02 MDCREATNWP     PIC X.                                     00002570
           02 MDCREATNWH     PIC X.                                     00002580
           02 MDCREATNWV     PIC X.                                     00002590
           02 MDCREATNWO     PIC X(10).                                 00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCCHASSNWA     PIC X.                                     00002620
           02 MCCHASSNWC     PIC X.                                     00002630
           02 MCCHASSNWP     PIC X.                                     00002640
           02 MCCHASSNWH     PIC X.                                     00002650
           02 MCCHASSNWV     PIC X.                                     00002660
           02 MCCHASSNWO     PIC X(7).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLCHASSNWA     PIC X.                                     00002690
           02 MLCHASSNWC     PIC X.                                     00002700
           02 MLCHASSNWP     PIC X.                                     00002710
           02 MLCHASSNWH     PIC X.                                     00002720
           02 MLCHASSNWV     PIC X.                                     00002730
           02 MLCHASSNWO     PIC X(20).                                 00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(78).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
