      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-PC20-LONG-COMMAREA             PIC S9(4) COMP           00000020
      *                                                  VALUE +4096.   00000020
      *--                                                                       
       01  COMM-PC20-LONG-COMMAREA             PIC S9(4) COMP-5                 
                                                         VALUE +4096.           
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
      *    ZONES RESERVEES A AIDA -------------------------------- 100  00000060
           05  FILLER-COM-AIDA                 PIC X(100).              00000070
                                                                        00000080
      *    ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020  00000090
           05  COMM-CICS-APPLID                PIC X(008).              00000100
           05  COMM-CICS-NETNAM                PIC X(008).              00000110
           05  COMM-CICS-TRANSA                PIC X(004).              00000120
                                                                        00000130
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00000140
           05  COMM-DATE-SIECLE                PIC X(002).              00000150
           05  COMM-DATE-ANNEE                 PIC X(002).              00000160
           05  COMM-DATE-MOIS                  PIC X(002).              00000170
           05  COMM-DATE-JOUR                  PIC 9(002).              00000180
                                                                        00000130
      *    QUANTIEMES CALENDAIRE ET STANDARD                            00000190
           05  COMM-DATE-QNTA                  PIC 9(03).               00000200
           05  COMM-DATE-QNT0                  PIC 9(05).               00000210
                                                                        00000130
      *    ANNEE BISSEXTILE 1=OUI 0=NON                                 00000220
           05  COMM-DATE-BISX                  PIC 9(01).               00000230
                                                                        00000130
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           05  COMM-DATE-JSM                   PIC 9(01).               00000250
                                                                        00000130
      *    LIBELLES DU JOUR COURT - LONG                                00000260
           05  COMM-DATE-JSM-LC                PIC X(03).               00000270
           05  COMM-DATE-JSM-LL                PIC X(08).               00000280
                                                                        00000130
      *    LIBELLES DU MOIS COURT - LONG                                00000290
           05  COMM-DATE-MOIS-LC               PIC X(03).               00000300
           05  COMM-DATE-MOIS-LL               PIC X(08).               00000310
                                                                        00000130
      *    DIFFERENTES FORMES DE DATE                                   00000320
           05  COMM-DATE-SSAAMMJJ              PIC X(08).               00000330
           05  COMM-DATE-AAMMJJ                PIC X(06).               00000340
           05  COMM-DATE-JJMMSSAA              PIC X(08).               00000350
           05  COMM-DATE-JJMMAA                PIC X(06).               00000360
           05  COMM-DATE-JJ-MM-AA              PIC X(08).               00000370
           05  COMM-DATE-JJ-MM-SSAA            PIC X(10).               00000380
                                                                        00000130
      *    TRAITEMENT DU NUMERO DE SEMAINE                              00000390
           05  COMM-DATE-WEEK.                                          00000400
               10  COMM-DATE-SEMSS             PIC 9(02).               00000410
               10  COMM-DATE-SEMAA             PIC 9(02).               00000420
               10  COMM-DATE-SEMNU             PIC 9(02).               00000430
           05  COMM-DATE-FILLER                PIC X(08).               00000440
                                                                        00000130
      *    ZONES RESERVEES TRAITEMENT DU SWAP                           00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-SWAP-CURS                  PIC S9(4) COMP VALUE -1. 00000460
      *                                                                         
      *--                                                                       
           05  COMM-SWAP-CURS                  PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
           05  COMM-PC20-ENTETE.                                        00000500
               10  COMM-ENTETE.                                         00000510
                   15  COMM-CODLANG            PIC X(02).               00000520
                   15  COMM-CODPIC             PIC X(02).               00000530
                   15  COMM-LEVEL-SUP          PIC X(05).               00000540
                   15  COMM-LEVEL-MAX          PIC X(05).               00000540
                   15  COMM-ACID               PIC X(08).                       
               10  COMM-MES-ERREUR.                                             
                   15  COMM-COD-ERREUR         PIC X(01).                       
                   15  COMM-LIB-MESSAG         PIC X(58).                       
               10  COMM-DAT-TIMJOU             PIC X(05).                       
               10  COMM-DAT-DEBUT              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-DEBUT.              
                   15  COMM-DAT-DEBJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBSA          PIC X(04).                       
               10  COMM-DAT-FINAL              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-FINAL.              
                   15  COMM-DAT-FINJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINSA          PIC X(04).                       
                                                                        00000130
      *--- MENU APPLICATION CARTES CADEAUX B2B (ACC) - TPC20            00000390
           05  COMM-PC20-APPLI.                                         00000500
               10  COMM-PC20-OPTION            PIC X(01).               00000520
               10  COMM-PC20-STATUT            PIC X(10).               00000520
               10  COMM-PC20-NCDE              PIC X(12).               00000520
               10  COMM-PC20-DATDEB            PIC X(10).               00000520
               10  FILLER                REDEFINES COMM-PC20-DATDEB.            
                   15  COMM-PC20-DEBJOUR       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-PC20-DEBMOIS       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-PC20-DEBSSAA       PIC X(04).                       
               10  COMM-PC20-DATFIN            PIC X(10).               00000520
               10  FILLER                REDEFINES COMM-PC20-DATFIN.            
                   15  COMM-PC20-FINJOUR       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-PC20-FINMOIS       PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-PC20-FINSSAA       PIC X(04).                       
               10  COMM-PC20-NCOMPTE           PIC X(08).               00000520
               10  COMM-PC20-NSERIE            PIC X(13).               00000520
      *--- SELECTION DES COMMANDES - TPC21                              00000390
           05  COMM-PC21-APPLI.                                         00000040
               10  COMM-PC21-PAGEMAX           PIC 9(03).                       
               10  COMM-PC21-PAGE              PIC 9(03).                       
               10  COMM-PC21-NBP               PIC 9(03).                       
               10  COMM-PC21-NCDE              PIC X(12).                       
               10  COMM-PC21-STATUT            PIC X(10).               00000520
      *--- COMPOSITION DE LA COMMANDE - TPC22                           00000390
           05  COMM-PC22-APPLI.                                         00000040
               10  COMM-PC22-PAGEMAX           PIC 9(03).                       
               10  COMM-PC22-PAGE              PIC 9(03).                       
               10  COMM-PC22-NBP               PIC 9(03).                       
               10  COMM-PC22-DCDE              PIC X(10).                       
               10  COMM-PC22-NCOMPTE           PIC X(08).                       
               10  COMM-PC22-RS                PIC X(25).                       
               10  COMM-PC22-NCHRONO           PIC X(15).                       
               10  COMM-PC22-TREMISE           PIC X(05).                       
               10  COMM-PC22-CVENDEUR          PIC X(07).                       
               10  COMM-PC22-NBONCDE           PIC X(24).                       
               10  COMM-PC22-MT-CARTES-TTC     PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-MT-REMISE-TTC     PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-TOT-PRESTAS-HT    PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-TOT-PRESTAS-TVA   PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-TOT-PRESTAS-TTC   PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-MT-TOTAL          PIC 9(06)V99 COMP-3.             
               10  COMM-PC22-PF5               PIC X(01).                       
               10  COMM-PC22-MT-CARTES-TMP     PIC 9(06)V99 COMP-3.             
      *--- LISTE DES CARTES CADEAUX COMPOSANT LA COMMANDE - TPC24       00000390
           05  COMM-PC24-APPLI.                                         00000040
               10  COMM-PC24-PAGEMAX           PIC 9(03).                       
               10  COMM-PC24-PAGE              PIC 9(03).                       
               10  COMM-PC24-NBP               PIC 9(03).                       
      *--- LISTE DES PRESTATIONS COMPOSANT LA COMMANDE - TPC25          00000390
           05  COMM-PC25-APPLI.                                         00000040
               10  COMM-PC25-PAGEMAX           PIC 9(03).                       
               10  COMM-PC25-PAGE              PIC 9(03).                       
               10  COMM-PC25-NBP               PIC 9(03).                       
      *--- FILLER                                                       00000390
           05  COMM-PC20-FILLER                PIC X(3516).             00000620
      *---                                                              00000390
                                                                                
