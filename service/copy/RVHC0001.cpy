      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHC0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHC0001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHC0001.                                                            
           02  HC00-NLIEUHC                                                     
               PIC X(0003).                                                     
           02  HC00-NCHS                                                        
               PIC X(0007).                                                     
           02  HC00-NCODIC                                                      
               PIC X(0007).                                                     
           02  HC00-NENVOI                                                      
               PIC X(0007).                                                     
           02  HC00-DDEPOT                                                      
               PIC X(0008).                                                     
           02  HC00-CPROVORIG                                                   
               PIC X(0001).                                                     
           02  HC00-CORIGINE                                                    
               PIC X(0007).                                                     
           02  HC00-CSTATUT                                                     
               PIC X(0001).                                                     
           02  HC00-CCHOIX                                                      
               PIC X(0003).                                                     
           02  HC00-CMAJADR                                                     
               PIC X(0001).                                                     
           02  HC00-ADRESSE                                                     
               PIC X(0008).                                                     
           02  HC00-CSERIE                                                      
               PIC X(0016).                                                     
           02  HC00-WAREPARER                                                   
               PIC X(0001).                                                     
           02  HC00-DPREACC                                                     
               PIC X(0008).                                                     
           02  HC00-NPREACC                                                     
               PIC X(0012).                                                     
           02  HC00-DACCFOUR                                                    
               PIC X(0008).                                                     
           02  HC00-NACCORD                                                     
               PIC X(0012).                                                     
           02  HC00-INTERLOC                                                    
               PIC X(0010).                                                     
           02  HC00-DFINTERV                                                    
               PIC X(0008).                                                     
           02  HC00-DSOLDE                                                      
               PIC X(0008).                                                     
           02  HC00-CLIENT                                                      
               PIC X(0020).                                                     
           02  HC00-LCOMMENT                                                    
               PIC X(0030).                                                     
           02  HC00-WDEMACC                                                     
               PIC X(0001).                                                     
           02  HC00-DDEMACC                                                     
               PIC X(0008).                                                     
           02  HC00-WDOSSIER                                                    
               PIC X(0001).                                                     
           02  HC00-NSAV                                                        
               PIC X(0003).                                                     
           02  HC00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HC00-NLIEU                                                       
               PIC X(0003).                                                     
           02  HC00-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  HC00-CLIEUTRT                                                    
               PIC X(0005).                                                     
           02  HC00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HC00-NLIEUEMET                                                   
               PIC X(0003).                                                     
           02  HC00-CTHS                                                        
               PIC X(0005).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHC0001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHC0001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NLIEUHC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NLIEUHC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NCHS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NCHS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CPROVORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CPROVORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CCHOIX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CCHOIX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CMAJADR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CMAJADR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-ADRESSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-ADRESSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CSERIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CSERIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-WAREPARER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-WAREPARER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DPREACC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DPREACC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NPREACC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NPREACC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DACCFOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DACCFOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NACCORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NACCORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-INTERLOC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-INTERLOC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DFINTERV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DFINTERV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DSOLDE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DSOLDE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CLIENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CLIENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-WDEMACC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-WDEMACC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DDEMACC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DDEMACC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-WDOSSIER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-WDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NSAV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NSAV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NSSLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CLIEUTRT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CLIEUTRT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-NLIEUEMET-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-NLIEUEMET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HC00-CTHS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HC00-CTHS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
