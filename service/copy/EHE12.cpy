      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE60   EHE60                                              00000020
      ***************************************************************** 00000030
       01   EHE12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(12).                                      00000170
           02 FILLER  OCCURS   16 TIMES .                               00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU1L      COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MLIEU1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU1F      PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MLIEU1I      PIC X(3).                                  00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHS1L  COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MNHS1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNHS1F  PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNHS1I  PIC X(7).                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR1L  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MADR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR1F  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MADR1I  PIC X(7).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU2L      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLIEU2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU2F      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLIEU2I      PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHS2L  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNHS2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNHS2F  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNHS2I  PIC X(7).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR2L  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MADR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR2F  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MADR2I  PIC X(7).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEU3L      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIEU3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEU3F      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIEU3I      PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHS3L  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNHS3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNHS3F  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNHS3I  PIC X(7).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR3L  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MADR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR3F  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MADR3I  PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMMAPL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MNOMMAPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMMAPF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNOMMAPI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: EHE60   EHE60                                              00000800
      ***************************************************************** 00000810
       01   EHE12O REDEFINES EHE12I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MTITREA   PIC X.                                          00000990
           02 MTITREC   PIC X.                                          00001000
           02 MTITREP   PIC X.                                          00001010
           02 MTITREH   PIC X.                                          00001020
           02 MTITREV   PIC X.                                          00001030
           02 MTITREO   PIC X(12).                                      00001040
           02 FILLER  OCCURS   16 TIMES .                               00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MLIEU1A      PIC X.                                     00001070
             03 MLIEU1C PIC X.                                          00001080
             03 MLIEU1P PIC X.                                          00001090
             03 MLIEU1H PIC X.                                          00001100
             03 MLIEU1V PIC X.                                          00001110
             03 MLIEU1O      PIC X(3).                                  00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MNHS1A  PIC X.                                          00001140
             03 MNHS1C  PIC X.                                          00001150
             03 MNHS1P  PIC X.                                          00001160
             03 MNHS1H  PIC X.                                          00001170
             03 MNHS1V  PIC X.                                          00001180
             03 MNHS1O  PIC X(7).                                       00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MADR1A  PIC X.                                          00001210
             03 MADR1C  PIC X.                                          00001220
             03 MADR1P  PIC X.                                          00001230
             03 MADR1H  PIC X.                                          00001240
             03 MADR1V  PIC X.                                          00001250
             03 MADR1O  PIC X(7).                                       00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MLIEU2A      PIC X.                                     00001280
             03 MLIEU2C PIC X.                                          00001290
             03 MLIEU2P PIC X.                                          00001300
             03 MLIEU2H PIC X.                                          00001310
             03 MLIEU2V PIC X.                                          00001320
             03 MLIEU2O      PIC X(3).                                  00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MNHS2A  PIC X.                                          00001350
             03 MNHS2C  PIC X.                                          00001360
             03 MNHS2P  PIC X.                                          00001370
             03 MNHS2H  PIC X.                                          00001380
             03 MNHS2V  PIC X.                                          00001390
             03 MNHS2O  PIC X(7).                                       00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MADR2A  PIC X.                                          00001420
             03 MADR2C  PIC X.                                          00001430
             03 MADR2P  PIC X.                                          00001440
             03 MADR2H  PIC X.                                          00001450
             03 MADR2V  PIC X.                                          00001460
             03 MADR2O  PIC X(7).                                       00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MLIEU3A      PIC X.                                     00001490
             03 MLIEU3C PIC X.                                          00001500
             03 MLIEU3P PIC X.                                          00001510
             03 MLIEU3H PIC X.                                          00001520
             03 MLIEU3V PIC X.                                          00001530
             03 MLIEU3O      PIC X(3).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MNHS3A  PIC X.                                          00001560
             03 MNHS3C  PIC X.                                          00001570
             03 MNHS3P  PIC X.                                          00001580
             03 MNHS3H  PIC X.                                          00001590
             03 MNHS3V  PIC X.                                          00001600
             03 MNHS3O  PIC X(7).                                       00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MADR3A  PIC X.                                          00001630
             03 MADR3C  PIC X.                                          00001640
             03 MADR3P  PIC X.                                          00001650
             03 MADR3H  PIC X.                                          00001660
             03 MADR3V  PIC X.                                          00001670
             03 MADR3O  PIC X(7).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLIBERRA  PIC X.                                          00001700
           02 MLIBERRC  PIC X.                                          00001710
           02 MLIBERRP  PIC X.                                          00001720
           02 MLIBERRH  PIC X.                                          00001730
           02 MLIBERRV  PIC X.                                          00001740
           02 MLIBERRO  PIC X(78).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCODTRAA  PIC X.                                          00001770
           02 MCODTRAC  PIC X.                                          00001780
           02 MCODTRAP  PIC X.                                          00001790
           02 MCODTRAH  PIC X.                                          00001800
           02 MCODTRAV  PIC X.                                          00001810
           02 MCODTRAO  PIC X(4).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNOMMAPA  PIC X.                                          00001840
           02 MNOMMAPC  PIC X.                                          00001850
           02 MNOMMAPP  PIC X.                                          00001860
           02 MNOMMAPH  PIC X.                                          00001870
           02 MNOMMAPV  PIC X.                                          00001880
           02 MNOMMAPO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
