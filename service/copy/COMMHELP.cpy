      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
E0452 ******************************************************************        
      * DE02005 26/09/08 SUPPORT EVOLUTION D004238                              
      *                  MESSAGE CORRECT SI AIDE NON SAISISSABLE                
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-HELP-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-HELP-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA      PIC X(4096).                                         
       01  FILLER REDEFINES Z-COMMAREA.                                         
      * ZONES RESERVEES AIDA ------------------------------------- 100          
          02 FILLER-COM-AIDA      PIC  X(100).                                  
      * ZONES RESERVEES CICS ------------------------------------- 020          
          02 COMM-CICS-APPLID     PIC  X(8).                                    
          02 COMM-CICS-NETNAM     PIC  X(8).                                    
          02 COMM-CICS-TRANSA     PIC  X(4).                                    
      * DATE DU JOUR --------------------------------------------- 100          
          02 COMM-DATE-SIECLE     PIC  99.                                      
          02 COMM-DATE-ANNEE      PIC  99.                                      
          02 COMM-DATE-MOIS       PIC  99.                                      
          02 COMM-DATE-JOUR       PIC  99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC  999.                                     
          02 COMM-DATE-QNT0       PIC  99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC  9.                                       
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC  9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC  XXX.                                     
          02 COMM-DATE-JSM-LL     PIC  X(8).                                    
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC  XXX.                                     
          02 COMM-DATE-MOIS-LL    PIC  X(8).                                    
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC  X(8).                                    
          02 COMM-DATE-AAMMJJ     PIC  X(6).                                    
          02 COMM-DATE-JJMMSSAA   PIC  X(8).                                    
          02 COMM-DATE-JJMMAA     PIC  X(6).                                    
          02 COMM-DATE-JJ-MM-AA   PIC  X(8).                                    
          02 COMM-DATE-JJ-MM-SSAA PIC  X(10).                                   
      *   TRAITEMENT NUMERO DE SEMAINE                                          
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC  99.                                      
             05 COMM-DATE-SEMAA   PIC  99.                                      
             05 COMM-DATE-SEMNU   PIC  99.                                      
          02 COMM-DATE-FILLER     PIC  X(08).                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4)     COMP.                           
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5.                             
      *}                                                                        
      *                                                                         
      * ZONES SPECIFIQUES AU HELP                                               
      *                                                                         
          02  COMMAREA-HELP.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-POS-DEBUT       PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-POS-DEBUT       PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-LENGTH          PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-LENGTH          PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-OCCH            PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-OCCH            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-OCCV            PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-OCCV            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-LGKEY           PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-LGKEY           PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-LGDATA          PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-LGDATA          PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-NOITEM          PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-NOITEM          PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-NBITEM          PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-NBITEM          PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-PASS            PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-PASS            PIC S9(4) COMP-5.                   
      *}                                                                        
              05  COMM-HELP-ZECRAN          PIC X(8).                           
              05  COMM-HELP-PCURSEUR.                                           
                  10  FILLER     OCCURS 2.                                      
                      15  FILLER    OCCURS 60.                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                   20  COMM-HELP-POSCUR      PIC S9(4)   COMP.           
      *--                                                                       
                          20  COMM-HELP-POSCUR      PIC S9(4) COMP-5.           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-HELP-NOCCUR          PIC S9(4)   COMP.                   
      *--                                                                       
              05  COMM-HELP-NOCCUR          PIC S9(4) COMP-5.                   
      *}                                                                        
              05  COMM-HELP-CTABLE          PIC X(18).                          
              05  COMM-HELP-CSTABLE         PIC X(5).                           
              05  COMM-HELP-CCHAMPR         PIC X(18).                          
              05  COMM-HELP-CCHAMPL         PIC X(18).                          
              05  COMM-HELP-CCRITERE        PIC X(8).                           
JB            05  COMM-HELP-NCHAMPL         PIC 9(3).                           
JB            05  COMM-HELP-NCRITERE        PIC 9(3).                           
JB            05  COMM-HELP-NCHAMPL-OLD     PIC 9(3).                           
JB            05  COMM-HELP-NCRITERE-OLD    PIC 9(3).                           
JB            05  COMM-HELP-NLIGNES         PIC 9(3).                           
JB            05  COMM-HELP-NREQUETE        PIC 9(5).                           
JB*   *       05  COMM-HELP-POS-HLP2        OCCURS 7.                           
JB            05  COMM-HELP-POS-HLP2        OCCURS 9.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10  COMM-HELP-PHLP2       PIC S9(4)    COMP.                  
      *--                                                                       
                  10  COMM-HELP-PHLP2       PIC S9(4) COMP-5.                   
      *}                                                                        
JB            05  COMM-HELP-TAB-CRIT        OCCURS 10.                          
                  10  COMM-HELP-CRITERE     PIC X(50).                          
JB            05  COMM-HELP-POS-HLP3        OCCURS 40.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10  COMM-HELP-PHLP3       PIC S9(4)    COMP.                  
      *--                                                                       
                  10  COMM-HELP-PHLP3       PIC S9(4) COMP-5.                   
      *}                                                                        
JB            05  COMM-HELP-MAJ             PIC 9(01).                          
JB            05  COMM-HELP-LIGNE-MODIF     OCCURS 10.                          
                  10  COMM-HELP-LIGNE-MAJ   PIC 9(01).                          
JB            05  COMM-HELP-LONG-LIBELLE    OCCURS 10.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10  COMM-HELP-LGLIB       PIC 9(3)    COMP.                   
      *--                                                                       
                  10  COMM-HELP-LGLIB       PIC 9(3) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10  COMM-HELP-LGTABLE     PIC 9(3)    COMP.                   
      *--                                                                       
                  10  COMM-HELP-LGTABLE     PIC 9(3) COMP-5.                    
      *}                                                                        
JB            05  COMM-HELP-NBENR           PIC 9(4).                           
JB            05  COMM-HELP-LIMITE          PIC 9(4).                           
E0452-        05  COMM-HELP-MODE            PIC 9(1).                           
                  88  COMM-HELP-MODE-INT       VALUE 0.                         
-E0452            88  COMM-HELP-MODE-MAJ       VALUE 1.                         
                                                                                
