      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: client pro selection                                       00000020
      ***************************************************************** 00000030
       01   EBC51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLNOMI    PIC X(60).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPOSTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPOSTI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMNL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLCOMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMNF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCOMNI   PIC X(32).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSIRETL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSIRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSIRETF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSIRETI  PIC X(14).                                      00000370
      * N� de compte client                                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCLIENTADRL   COMP PIC S9(4).                            00000390
      *--                                                                       
           02 MNCLIENTADRL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNCLIENTADRF   PIC X.                                     00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MNCLIENTADRI   PIC X(8).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTDOML   COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MNTDOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTDOMF   PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MNTDOMI   PIC X(15).                                      00000460
      * entete colonnes                                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTETL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MLENTETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTETF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLENTETI  PIC X(40).                                      00000510
           02 MLCLIENTI OCCURS   10 TIMES .                             00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MLCHOIXI     PIC X.                                     00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOML   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOMF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MNOMI   PIC X(35).                                      00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFOL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINFOF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MINFOI  PIC X(41).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIBERRI  PIC X(75).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCICSI    PIC X(5).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSCREENI  PIC X(4).                                       00000840
      ***************************************************************** 00000850
      * SDF: client pro selection                                       00000860
      ***************************************************************** 00000870
       01   EBC51O REDEFINES EBC51I.                                    00000880
           02 FILLER    PIC X(12).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDATJOUA  PIC X.                                          00000910
           02 MDATJOUC  PIC X.                                          00000920
           02 MDATJOUP  PIC X.                                          00000930
           02 MDATJOUH  PIC X.                                          00000940
           02 MDATJOUV  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MPAGEA    PIC X.                                          00001050
           02 MPAGEC    PIC X.                                          00001060
           02 MPAGEP    PIC X.                                          00001070
           02 MPAGEH    PIC X.                                          00001080
           02 MPAGEV    PIC X.                                          00001090
           02 MPAGEO    PIC X(3).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MPAGEMAXA      PIC X.                                     00001120
           02 MPAGEMAXC PIC X.                                          00001130
           02 MPAGEMAXP PIC X.                                          00001140
           02 MPAGEMAXH PIC X.                                          00001150
           02 MPAGEMAXV PIC X.                                          00001160
           02 MPAGEMAXO      PIC X(3).                                  00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MLNOMA    PIC X.                                          00001190
           02 MLNOMC    PIC X.                                          00001200
           02 MLNOMP    PIC X.                                          00001210
           02 MLNOMH    PIC X.                                          00001220
           02 MLNOMV    PIC X.                                          00001230
           02 MLNOMO    PIC X(60).                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCPOSTA   PIC X.                                          00001260
           02 MCPOSTC   PIC X.                                          00001270
           02 MCPOSTP   PIC X.                                          00001280
           02 MCPOSTH   PIC X.                                          00001290
           02 MCPOSTV   PIC X.                                          00001300
           02 MCPOSTO   PIC X(5).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLCOMNA   PIC X.                                          00001330
           02 MLCOMNC   PIC X.                                          00001340
           02 MLCOMNP   PIC X.                                          00001350
           02 MLCOMNH   PIC X.                                          00001360
           02 MLCOMNV   PIC X.                                          00001370
           02 MLCOMNO   PIC X(32).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNSIRETA  PIC X.                                          00001400
           02 MNSIRETC  PIC X.                                          00001410
           02 MNSIRETP  PIC X.                                          00001420
           02 MNSIRETH  PIC X.                                          00001430
           02 MNSIRETV  PIC X.                                          00001440
           02 MNSIRETO  PIC X(14).                                      00001450
      * N� de compte client                                             00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNCLIENTADRA   PIC X.                                     00001480
           02 MNCLIENTADRC   PIC X.                                     00001490
           02 MNCLIENTADRP   PIC X.                                     00001500
           02 MNCLIENTADRH   PIC X.                                     00001510
           02 MNCLIENTADRV   PIC X.                                     00001520
           02 MNCLIENTADRO   PIC X(8).                                  00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNTDOMA   PIC X.                                          00001550
           02 MNTDOMC   PIC X.                                          00001560
           02 MNTDOMP   PIC X.                                          00001570
           02 MNTDOMH   PIC X.                                          00001580
           02 MNTDOMV   PIC X.                                          00001590
           02 MNTDOMO   PIC X(15).                                      00001600
      * entete colonnes                                                 00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLENTETA  PIC X.                                          00001630
           02 MLENTETC  PIC X.                                          00001640
           02 MLENTETP  PIC X.                                          00001650
           02 MLENTETH  PIC X.                                          00001660
           02 MLENTETV  PIC X.                                          00001670
           02 MLENTETO  PIC X(40).                                      00001680
           02 MLCLIENTO OCCURS   10 TIMES .                             00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MLCHOIXA     PIC X.                                     00001710
             03 MLCHOIXC     PIC X.                                     00001720
             03 MLCHOIXP     PIC X.                                     00001730
             03 MLCHOIXH     PIC X.                                     00001740
             03 MLCHOIXV     PIC X.                                     00001750
             03 MLCHOIXO     PIC X.                                     00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNOMA   PIC X.                                          00001780
             03 MNOMC   PIC X.                                          00001790
             03 MNOMP   PIC X.                                          00001800
             03 MNOMH   PIC X.                                          00001810
             03 MNOMV   PIC X.                                          00001820
             03 MNOMO   PIC X(35).                                      00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MINFOA  PIC X.                                          00001850
             03 MINFOC  PIC X.                                          00001860
             03 MINFOP  PIC X.                                          00001870
             03 MINFOH  PIC X.                                          00001880
             03 MINFOV  PIC X.                                          00001890
             03 MINFOO  PIC X(41).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLIBERRA  PIC X.                                          00001920
           02 MLIBERRC  PIC X.                                          00001930
           02 MLIBERRP  PIC X.                                          00001940
           02 MLIBERRH  PIC X.                                          00001950
           02 MLIBERRV  PIC X.                                          00001960
           02 MLIBERRO  PIC X(75).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCODTRAA  PIC X.                                          00001990
           02 MCODTRAC  PIC X.                                          00002000
           02 MCODTRAP  PIC X.                                          00002010
           02 MCODTRAH  PIC X.                                          00002020
           02 MCODTRAV  PIC X.                                          00002030
           02 MCODTRAO  PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCICSA    PIC X.                                          00002060
           02 MCICSC    PIC X.                                          00002070
           02 MCICSP    PIC X.                                          00002080
           02 MCICSH    PIC X.                                          00002090
           02 MCICSV    PIC X.                                          00002100
           02 MCICSO    PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNETNAMA  PIC X.                                          00002130
           02 MNETNAMC  PIC X.                                          00002140
           02 MNETNAMP  PIC X.                                          00002150
           02 MNETNAMH  PIC X.                                          00002160
           02 MNETNAMV  PIC X.                                          00002170
           02 MNETNAMO  PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MSCREENA  PIC X.                                          00002200
           02 MSCREENC  PIC X.                                          00002210
           02 MSCREENP  PIC X.                                          00002220
           02 MSCREENH  PIC X.                                          00002230
           02 MSCREENV  PIC X.                                          00002240
           02 MSCREENO  PIC X(4).                                       00002250
                                                                                
