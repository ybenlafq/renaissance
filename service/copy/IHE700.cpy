      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE700 AU 21/07/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,07,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE700.                                                        
            05 NOMETAT-IHE700           PIC X(6) VALUE 'IHE700'.                
            05 RUPTURES-IHE700.                                                 
           10 IHE700-CLIEUHET           PIC X(05).                      007  005
           10 IHE700-NLIEUHED           PIC X(03).                      012  003
           10 IHE700-NGHE               PIC X(07).                      015  007
           10 IHE700-NSEQ               PIC X(03).                      022  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE700-SEQUENCE           PIC S9(04) COMP.                025  002
      *--                                                                       
           10 IHE700-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE700.                                                   
           10 IHE700-CFAMAP             PIC X(05).                      027  005
           10 IHE700-CFAMAV             PIC X(05).                      032  005
           10 IHE700-CLIEUHETAP         PIC X(05).                      037  005
           10 IHE700-CMARQAP            PIC X(05).                      042  005
           10 IHE700-CMARQAV            PIC X(05).                      047  005
           10 IHE700-CTRAITAP           PIC X(05).                      052  005
           10 IHE700-CTRAITAV           PIC X(05).                      057  005
           10 IHE700-CTYPMVTAP          PIC X(14).                      062  014
           10 IHE700-CTYPMVTAV          PIC X(14).                      076  014
           10 IHE700-LLIEUHET           PIC X(20).                      090  020
           10 IHE700-LREFAP             PIC X(20).                      110  020
           10 IHE700-LREFAV             PIC X(20).                      130  020
           10 IHE700-NCODICAP           PIC X(07).                      150  007
           10 IHE700-NCODICAV           PIC X(07).                      157  007
           10 IHE700-NTYOPEAP           PIC X(03).                      164  003
           10 IHE700-NTYOPEAV           PIC X(03).                      167  003
           10 IHE700-WSOLDEAP           PIC X(01).                      170  001
           10 IHE700-WSOLDEAV           PIC X(01).                      171  001
           10 IHE700-DATE               PIC X(08).                      172  008
            05 FILLER                      PIC X(333).                          
                                                                                
