      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * FRANCHISE - PARAM EXTRACT IFR100                                00000020
      ***************************************************************** 00000030
       01   EFR08I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXTRL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCEXTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEXTRF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCEXTRI   PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEXTRL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLIBEXTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBEXTRF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBEXTRI      PIC X(30).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCOPCOI   PIC X(6).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBOPCOL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MLIBOPCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBOPCOF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLIBOPCOI      PIC X(30).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROGL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCPROGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPROGF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCPROGI   PIC X(5).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCORIL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MSOCORIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCORIF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MSOCORII  PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUORIL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLIEUORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUORIF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLIEUORII      PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDSTL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MSOCDSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDSTF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MSOCDSTI  PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDSTL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MLIEUDSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUDSTF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLIEUDSTI      PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCVTEL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MSOCVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCVTEF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MSOCVTEI  PIC X(3).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUVTEL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MLIEUVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUVTEF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIEUVTEI      PIC X(3).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTITL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MINTITL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINTITF   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MINTITI   PIC X(30).                                      00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNSEQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSEQF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNSEQI    PIC X(3).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESFILL   COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MESFILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MESFILF   PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MESFILI   PIC X.                                          00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALOL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MVALOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MVALOF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MVALOI    PIC X.                                          00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPUTL   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MIMPUTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MIMPUTF   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MIMPUTI   PIC X.                                          00000790
      * ZONE CMD AIDA                                                   00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPPRL   COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSUPPRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSUPPRF   PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSUPPRI   PIC X.                                          00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MLIBERRI  PIC X(79).                                      00000880
      * CODE TRANSACTION                                                00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCODTRAI  PIC X(4).                                       00000930
      * CICS DE TRAVAIL                                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      * NETNAME                                                         00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      * CODE TERMINAL                                                   00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MSCREENI  PIC X(4).                                       00001080
      ***************************************************************** 00001090
      * FRANCHISE - PARAM EXTRACT IFR100                                00001100
      ***************************************************************** 00001110
       01   EFR08O REDEFINES EFR08I.                                    00001120
           02 FILLER    PIC X(12).                                      00001130
      * DATE DU JOUR                                                    00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDATJOUA  PIC X.                                          00001160
           02 MDATJOUC  PIC X.                                          00001170
           02 MDATJOUP  PIC X.                                          00001180
           02 MDATJOUH  PIC X.                                          00001190
           02 MDATJOUV  PIC X.                                          00001200
           02 MDATJOUO  PIC X(10).                                      00001210
      * HEURE                                                           00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCEXTRA   PIC X.                                          00001310
           02 MCEXTRC   PIC X.                                          00001320
           02 MCEXTRP   PIC X.                                          00001330
           02 MCEXTRH   PIC X.                                          00001340
           02 MCEXTRV   PIC X.                                          00001350
           02 MCEXTRO   PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLIBEXTRA      PIC X.                                     00001380
           02 MLIBEXTRC PIC X.                                          00001390
           02 MLIBEXTRP PIC X.                                          00001400
           02 MLIBEXTRH PIC X.                                          00001410
           02 MLIBEXTRV PIC X.                                          00001420
           02 MLIBEXTRO      PIC X(30).                                 00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCOPCOA   PIC X.                                          00001450
           02 MCOPCOC   PIC X.                                          00001460
           02 MCOPCOP   PIC X.                                          00001470
           02 MCOPCOH   PIC X.                                          00001480
           02 MCOPCOV   PIC X.                                          00001490
           02 MCOPCOO   PIC X(6).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLIBOPCOA      PIC X.                                     00001520
           02 MLIBOPCOC PIC X.                                          00001530
           02 MLIBOPCOP PIC X.                                          00001540
           02 MLIBOPCOH PIC X.                                          00001550
           02 MLIBOPCOV PIC X.                                          00001560
           02 MLIBOPCOO      PIC X(30).                                 00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MCPROGA   PIC X.                                          00001590
           02 MCPROGC   PIC X.                                          00001600
           02 MCPROGP   PIC X.                                          00001610
           02 MCPROGH   PIC X.                                          00001620
           02 MCPROGV   PIC X.                                          00001630
           02 MCPROGO   PIC X(5).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MSOCORIA  PIC X.                                          00001660
           02 MSOCORIC  PIC X.                                          00001670
           02 MSOCORIP  PIC X.                                          00001680
           02 MSOCORIH  PIC X.                                          00001690
           02 MSOCORIV  PIC X.                                          00001700
           02 MSOCORIO  PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLIEUORIA      PIC X.                                     00001730
           02 MLIEUORIC PIC X.                                          00001740
           02 MLIEUORIP PIC X.                                          00001750
           02 MLIEUORIH PIC X.                                          00001760
           02 MLIEUORIV PIC X.                                          00001770
           02 MLIEUORIO      PIC X(3).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MSOCDSTA  PIC X.                                          00001800
           02 MSOCDSTC  PIC X.                                          00001810
           02 MSOCDSTP  PIC X.                                          00001820
           02 MSOCDSTH  PIC X.                                          00001830
           02 MSOCDSTV  PIC X.                                          00001840
           02 MSOCDSTO  PIC X(3).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLIEUDSTA      PIC X.                                     00001870
           02 MLIEUDSTC PIC X.                                          00001880
           02 MLIEUDSTP PIC X.                                          00001890
           02 MLIEUDSTH PIC X.                                          00001900
           02 MLIEUDSTV PIC X.                                          00001910
           02 MLIEUDSTO      PIC X(3).                                  00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSOCVTEA  PIC X.                                          00001940
           02 MSOCVTEC  PIC X.                                          00001950
           02 MSOCVTEP  PIC X.                                          00001960
           02 MSOCVTEH  PIC X.                                          00001970
           02 MSOCVTEV  PIC X.                                          00001980
           02 MSOCVTEO  PIC X(3).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLIEUVTEA      PIC X.                                     00002010
           02 MLIEUVTEC PIC X.                                          00002020
           02 MLIEUVTEP PIC X.                                          00002030
           02 MLIEUVTEH PIC X.                                          00002040
           02 MLIEUVTEV PIC X.                                          00002050
           02 MLIEUVTEO      PIC X(3).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MINTITA   PIC X.                                          00002080
           02 MINTITC   PIC X.                                          00002090
           02 MINTITP   PIC X.                                          00002100
           02 MINTITH   PIC X.                                          00002110
           02 MINTITV   PIC X.                                          00002120
           02 MINTITO   PIC X(30).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNSEQA    PIC X.                                          00002150
           02 MNSEQC    PIC X.                                          00002160
           02 MNSEQP    PIC X.                                          00002170
           02 MNSEQH    PIC X.                                          00002180
           02 MNSEQV    PIC X.                                          00002190
           02 MNSEQO    PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MESFILA   PIC X.                                          00002220
           02 MESFILC   PIC X.                                          00002230
           02 MESFILP   PIC X.                                          00002240
           02 MESFILH   PIC X.                                          00002250
           02 MESFILV   PIC X.                                          00002260
           02 MESFILO   PIC X.                                          00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MVALOA    PIC X.                                          00002290
           02 MVALOC    PIC X.                                          00002300
           02 MVALOP    PIC X.                                          00002310
           02 MVALOH    PIC X.                                          00002320
           02 MVALOV    PIC X.                                          00002330
           02 MVALOO    PIC X.                                          00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MIMPUTA   PIC X.                                          00002360
           02 MIMPUTC   PIC X.                                          00002370
           02 MIMPUTP   PIC X.                                          00002380
           02 MIMPUTH   PIC X.                                          00002390
           02 MIMPUTV   PIC X.                                          00002400
           02 MIMPUTO   PIC X.                                          00002410
      * ZONE CMD AIDA                                                   00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MSUPPRA   PIC X.                                          00002440
           02 MSUPPRC   PIC X.                                          00002450
           02 MSUPPRP   PIC X.                                          00002460
           02 MSUPPRH   PIC X.                                          00002470
           02 MSUPPRV   PIC X.                                          00002480
           02 MSUPPRO   PIC X.                                          00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MLIBERRA  PIC X.                                          00002510
           02 MLIBERRC  PIC X.                                          00002520
           02 MLIBERRP  PIC X.                                          00002530
           02 MLIBERRH  PIC X.                                          00002540
           02 MLIBERRV  PIC X.                                          00002550
           02 MLIBERRO  PIC X(79).                                      00002560
      * CODE TRANSACTION                                                00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCODTRAA  PIC X.                                          00002590
           02 MCODTRAC  PIC X.                                          00002600
           02 MCODTRAP  PIC X.                                          00002610
           02 MCODTRAH  PIC X.                                          00002620
           02 MCODTRAV  PIC X.                                          00002630
           02 MCODTRAO  PIC X(4).                                       00002640
      * CICS DE TRAVAIL                                                 00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MCICSA    PIC X.                                          00002670
           02 MCICSC    PIC X.                                          00002680
           02 MCICSP    PIC X.                                          00002690
           02 MCICSH    PIC X.                                          00002700
           02 MCICSV    PIC X.                                          00002710
           02 MCICSO    PIC X(5).                                       00002720
      * NETNAME                                                         00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
      * CODE TERMINAL                                                   00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSCREENA  PIC X.                                          00002830
           02 MSCREENC  PIC X.                                          00002840
           02 MSCREENP  PIC X.                                          00002850
           02 MSCREENH  PIC X.                                          00002860
           02 MSCREENV  PIC X.                                          00002870
           02 MSCREENO  PIC X(4).                                       00002880
                                                                                
