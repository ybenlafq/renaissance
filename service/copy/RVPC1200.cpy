      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPC1200                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPC1200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC1200.                                                            
      *}                                                                        
      *                       NREGLE                                            
           10 PC12-NREGLE          PIC X(10).                                   
      *                       NLIGNE                                            
           10 PC12-NLIGNE          PIC X(2).                                    
      *                       LIBELLE                                           
           10 PC12-LIBELLE         PIC X(25).                                   
      *                       CAPPLI                                            
           10 PC12-CAPPLI          PIC X(3).                                    
      *                       NSOCORIG                                          
           10 PC12-NSOCORIG        PIC X(3).                                    
      *                       NLIEUORIG                                         
           10 PC12-NLIEUORIG       PIC X(3).                                    
      *                       NSOCDEST                                          
           10 PC12-NSOCDEST        PIC X(3).                                    
      *                       NLIEUDEST                                         
           10 PC12-NLIEUDEST       PIC X(3).                                    
      *                       CTYPFACT                                          
           10 PC12-CTYPFACT        PIC X(2).                                    
      *                       NATFACT                                           
           10 PC12-NATFACT         PIC X(5).                                    
      *                       CVENT                                             
           10 PC12-CVENT           PIC X(5).                                    
      *                       NTIERSE                                           
           10 PC12-NTIERSE         PIC X(8).                                    
      *                       NLETTRE                                           
           10 PC12-NLETTRE         PIC X(10).                                   
      *                       NTIERSR                                           
           10 PC12-NTIERSR         PIC X(8).                                    
      *                       NLETTRR                                           
           10 PC12-NLETTRR         PIC X(10).                                   
      *                       PC02                                              
           10 PC12-PC02            PIC X(1).                                    
      *                       PERIOD                                            
           10 PC12-PERIOD          PIC X(1).                                    
      *                       JOUR                                              
           10 PC12-JOUR            PIC X(2).                                    
      *                       ACID                                              
           10 PC12-ACID            PIC X(8).                                    
      *                       DSYST                                             
           10 PC12-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 20      *        
      ******************************************************************        
                                                                                
