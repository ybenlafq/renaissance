      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: saisie parm. eligi. THD                                    00000020
      ***************************************************************** 00000030
       01   ERB47I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPOSTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCPOSTI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMNL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCOMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMNF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCOMNI   PIC X(30).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPNPUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPNPUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPNPUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPNPUI   PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSAVUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCSAVUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSAVUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCSAVUI   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSSTUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCSSTUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSSTUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSSTUI   PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTDISPOUL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDTDISPOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDTDISPOUF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDTDISPOUI     PIC X(6).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFILTREUL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCFILTREUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCFILTREUF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFILTREUI     PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPRESUL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCTYPRESUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPRESUF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCTYPRESUI     PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFREQRESUL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MFREQRESUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFREQRESUF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MFREQRESUI     PIC X(6).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRESEAUUL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNRESEAUUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNRESEAUUF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNRESEAUUI     PIC X(5).                                  00000610
           02 MLIGNEI OCCURS   10 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOSTALL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCPOSTALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPOSTALF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCPOSTALI    PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMUNL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLCOMMUNL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMMUNF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLCOMMUNI    PIC X(21).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCONFL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MZONCONFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZONCONFF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MZONCONFI    PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPNPL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MCPNPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCPNPF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCPNPI  PIC X(6).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSAVL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCSAVL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCSAVF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCSAVI  PIC X(6).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSSTL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCSSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCSSTF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCSSTI  PIC X(6).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTDISPOL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDTDISPOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDTDISPOF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDTDISPOI    PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFILTREL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCFILTREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCFILTREF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCFILTREI    PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPRESL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCTYPRESL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTYPRESF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCTYPRESI    PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFREQRESL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MFREQRESL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MFREQRESF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MFREQRESI    PIC X(6).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRESEAUL    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNRESEAUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNRESEAUF    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNRESEAUI    PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(74).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * Rbx: saisie parm. eligi. THD                                    00001280
      ***************************************************************** 00001290
       01   ERB47O REDEFINES ERB47I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MPAGEA    PIC X.                                          00001470
           02 MPAGEC    PIC X.                                          00001480
           02 MPAGEP    PIC X.                                          00001490
           02 MPAGEH    PIC X.                                          00001500
           02 MPAGEV    PIC X.                                          00001510
           02 MPAGEO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MPAGEMAXA      PIC X.                                     00001540
           02 MPAGEMAXC PIC X.                                          00001550
           02 MPAGEMAXP PIC X.                                          00001560
           02 MPAGEMAXH PIC X.                                          00001570
           02 MPAGEMAXV PIC X.                                          00001580
           02 MPAGEMAXO      PIC X(3).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCPOSTA   PIC X.                                          00001610
           02 MCPOSTC   PIC X.                                          00001620
           02 MCPOSTP   PIC X.                                          00001630
           02 MCPOSTH   PIC X.                                          00001640
           02 MCPOSTV   PIC X.                                          00001650
           02 MCPOSTO   PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLCOMNA   PIC X.                                          00001680
           02 MLCOMNC   PIC X.                                          00001690
           02 MLCOMNP   PIC X.                                          00001700
           02 MLCOMNH   PIC X.                                          00001710
           02 MLCOMNV   PIC X.                                          00001720
           02 MLCOMNO   PIC X(30).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCPNPUA   PIC X.                                          00001750
           02 MCPNPUC   PIC X.                                          00001760
           02 MCPNPUP   PIC X.                                          00001770
           02 MCPNPUH   PIC X.                                          00001780
           02 MCPNPUV   PIC X.                                          00001790
           02 MCPNPUO   PIC X(6).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCSAVUA   PIC X.                                          00001820
           02 MCSAVUC   PIC X.                                          00001830
           02 MCSAVUP   PIC X.                                          00001840
           02 MCSAVUH   PIC X.                                          00001850
           02 MCSAVUV   PIC X.                                          00001860
           02 MCSAVUO   PIC X(6).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCSSTUA   PIC X.                                          00001890
           02 MCSSTUC   PIC X.                                          00001900
           02 MCSSTUP   PIC X.                                          00001910
           02 MCSSTUH   PIC X.                                          00001920
           02 MCSSTUV   PIC X.                                          00001930
           02 MCSSTUO   PIC X(6).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDTDISPOUA     PIC X.                                     00001960
           02 MDTDISPOUC     PIC X.                                     00001970
           02 MDTDISPOUP     PIC X.                                     00001980
           02 MDTDISPOUH     PIC X.                                     00001990
           02 MDTDISPOUV     PIC X.                                     00002000
           02 MDTDISPOUO     PIC X(6).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCFILTREUA     PIC X.                                     00002030
           02 MCFILTREUC     PIC X.                                     00002040
           02 MCFILTREUP     PIC X.                                     00002050
           02 MCFILTREUH     PIC X.                                     00002060
           02 MCFILTREUV     PIC X.                                     00002070
           02 MCFILTREUO     PIC X.                                     00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCTYPRESUA     PIC X.                                     00002100
           02 MCTYPRESUC     PIC X.                                     00002110
           02 MCTYPRESUP     PIC X.                                     00002120
           02 MCTYPRESUH     PIC X.                                     00002130
           02 MCTYPRESUV     PIC X.                                     00002140
           02 MCTYPRESUO     PIC X.                                     00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MFREQRESUA     PIC X.                                     00002170
           02 MFREQRESUC     PIC X.                                     00002180
           02 MFREQRESUP     PIC X.                                     00002190
           02 MFREQRESUH     PIC X.                                     00002200
           02 MFREQRESUV     PIC X.                                     00002210
           02 MFREQRESUO     PIC X(6).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNRESEAUUA     PIC X.                                     00002240
           02 MNRESEAUUC     PIC X.                                     00002250
           02 MNRESEAUUP     PIC X.                                     00002260
           02 MNRESEAUUH     PIC X.                                     00002270
           02 MNRESEAUUV     PIC X.                                     00002280
           02 MNRESEAUUO     PIC X(5).                                  00002290
           02 MLIGNEO OCCURS   10 TIMES .                               00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MCPOSTALA    PIC X.                                     00002320
             03 MCPOSTALC    PIC X.                                     00002330
             03 MCPOSTALP    PIC X.                                     00002340
             03 MCPOSTALH    PIC X.                                     00002350
             03 MCPOSTALV    PIC X.                                     00002360
             03 MCPOSTALO    PIC X(5).                                  00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MLCOMMUNA    PIC X.                                     00002390
             03 MLCOMMUNC    PIC X.                                     00002400
             03 MLCOMMUNP    PIC X.                                     00002410
             03 MLCOMMUNH    PIC X.                                     00002420
             03 MLCOMMUNV    PIC X.                                     00002430
             03 MLCOMMUNO    PIC X(21).                                 00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MZONCONFA    PIC X.                                     00002460
             03 MZONCONFC    PIC X.                                     00002470
             03 MZONCONFP    PIC X.                                     00002480
             03 MZONCONFH    PIC X.                                     00002490
             03 MZONCONFV    PIC X.                                     00002500
             03 MZONCONFO    PIC X.                                     00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MCPNPA  PIC X.                                          00002530
             03 MCPNPC  PIC X.                                          00002540
             03 MCPNPP  PIC X.                                          00002550
             03 MCPNPH  PIC X.                                          00002560
             03 MCPNPV  PIC X.                                          00002570
             03 MCPNPO  PIC X(6).                                       00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MCSAVA  PIC X.                                          00002600
             03 MCSAVC  PIC X.                                          00002610
             03 MCSAVP  PIC X.                                          00002620
             03 MCSAVH  PIC X.                                          00002630
             03 MCSAVV  PIC X.                                          00002640
             03 MCSAVO  PIC X(6).                                       00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MCSSTA  PIC X.                                          00002670
             03 MCSSTC  PIC X.                                          00002680
             03 MCSSTP  PIC X.                                          00002690
             03 MCSSTH  PIC X.                                          00002700
             03 MCSSTV  PIC X.                                          00002710
             03 MCSSTO  PIC X(6).                                       00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MDTDISPOA    PIC X.                                     00002740
             03 MDTDISPOC    PIC X.                                     00002750
             03 MDTDISPOP    PIC X.                                     00002760
             03 MDTDISPOH    PIC X.                                     00002770
             03 MDTDISPOV    PIC X.                                     00002780
             03 MDTDISPOO    PIC X(6).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MCFILTREA    PIC X.                                     00002810
             03 MCFILTREC    PIC X.                                     00002820
             03 MCFILTREP    PIC X.                                     00002830
             03 MCFILTREH    PIC X.                                     00002840
             03 MCFILTREV    PIC X.                                     00002850
             03 MCFILTREO    PIC X.                                     00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MCTYPRESA    PIC X.                                     00002880
             03 MCTYPRESC    PIC X.                                     00002890
             03 MCTYPRESP    PIC X.                                     00002900
             03 MCTYPRESH    PIC X.                                     00002910
             03 MCTYPRESV    PIC X.                                     00002920
             03 MCTYPRESO    PIC X.                                     00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MFREQRESA    PIC X.                                     00002950
             03 MFREQRESC    PIC X.                                     00002960
             03 MFREQRESP    PIC X.                                     00002970
             03 MFREQRESH    PIC X.                                     00002980
             03 MFREQRESV    PIC X.                                     00002990
             03 MFREQRESO    PIC X(6).                                  00003000
             03 FILLER       PIC X(2).                                  00003010
             03 MNRESEAUA    PIC X.                                     00003020
             03 MNRESEAUC    PIC X.                                     00003030
             03 MNRESEAUP    PIC X.                                     00003040
             03 MNRESEAUH    PIC X.                                     00003050
             03 MNRESEAUV    PIC X.                                     00003060
             03 MNRESEAUO    PIC X(5).                                  00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(74).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
