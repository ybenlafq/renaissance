      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP052 AU 17/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP052.                                                        
            05 NOMETAT-ISP052           PIC X(6) VALUE 'ISP052'.                
            05 RUPTURES-ISP052.                                                 
           10 ISP052-NSOCEMET           PIC X(03).                      007  003
           10 ISP052-NSOCRECEP          PIC X(03).                      010  003
           10 ISP052-NUMFACT            PIC X(07).                      013  007
           10 ISP052-CMARQ              PIC X(05).                      020  005
           10 ISP052-CFAM               PIC X(05).                      025  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP052-SEQUENCE           PIC S9(04) COMP.                030  002
      *--                                                                       
           10 ISP052-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP052.                                                   
           10 ISP052-LSOCEMET           PIC X(25).                      032  025
           10 ISP052-LSOCRECEP          PIC X(25).                      057  025
           10 ISP052-MOISTRT            PIC X(05).                      082  005
           10 ISP052-NCODIC             PIC X(07).                      087  007
           10 ISP052-REFERENCE          PIC X(20).                      094  020
           10 ISP052-TYPFACT            PIC X(01).                      114  001
           10 ISP052-QTE                PIC S9(07)      COMP-3.         115  004
           10 ISP052-VALOPRAK           PIC S9(09)V9(2) COMP-3.         119  006
           10 ISP052-VALOPRMP           PIC S9(09)V9(2) COMP-3.         125  006
           10 ISP052-DATEFACT           PIC X(08).                      131  008
            05 FILLER                      PIC X(374).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP052-LONG           PIC S9(4)   COMP  VALUE +138.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP052-LONG           PIC S9(4) COMP-5  VALUE +138.           
                                                                                
      *}                                                                        
