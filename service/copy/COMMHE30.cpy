      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      *                                                                 00008900
      * COMMAREA SPECIFIQUE PRG THE30 (THE30 -> TBC61)   TR: HE30  *    00002209
      *                                                                 00008900
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-HE30-LONG-COMMAREA PIC S9(4) COMP VALUE +300.                   
      *--                                                                       
       01  COMM-HE30-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +300.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA-MHE30.                                                    
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------4000--- 3724          
      *                                                                         
      *            TRANSACTION HE30 : CONSULTATION                    *         
      *                                                                         
      *****************************************************************         
      *------------------------------ ZONE DONNEES MHE30                        
          02 COMM-DONNEES-MHE30.                                                
      *---      INFO CICS                                                       
             05 COMM-MHE30-APPLID        PIC X(8).                              
             05 COMM-MHE30-NSOCCICS      PIC X(03).                             
             05 COMM-MHE30-SSAAMMJJ      PIC X(8).                              
      *---      PREFIXE NUMERO NASC D ORIGINE                                   
             05 COMM-MHE30-PDOSNASC      PIC X(03).                             
      *---      NUMERO NASC D ORIGINE                                           
             05 COMM-MHE30-DOSNASC       PIC X(09).                             
      *---      ORIGINE HS                                                      
             05 COMM-MHE30-NLIEUHED      PIC X(03).                             
      *---      NUMERO HS                                                       
             05 COMM-MHE30-NGHE          PIC X(07).                             
      *---      CODIC                                                           
             05 COMM-MHE30-NCODIC        PIC X(07).                             
      *---      NUMERO DE SERIE                                                 
             05 COMM-MHE30-NSERIE        PIC X(16).                             
      *---      NUMERO DE SOCIETE SAV                                           
             05 COMM-MHE30-NSOC          PIC X(03).                             
      *---      NUMERO DE LIEU SAV                                              
             05 COMM-MHE30-LIEU          PIC X(03).                             
      *---      NUMERO DE PREFIXE SAV                                           
             05 COMM-MHE30-PREFIXE       PIC X(03).                             
      *------------------------------ ZONE RETOUR MHE30                         
          02 COMM-MHE30-RETOUR.                                                 
             05 COMM-MHE30RETOUR-MQ.                                            
                 10 COMM-MHE30-CODRETMQ      PIC X(01).                         
                      88  COMM-MHE30-MQ-OK     VALUE ' '.                       
                      88  COMM-MHE30-MQ-ERREUR VALUE '1'.                       
                 10 COMM-MHE30-LIBERRMQ      PIC X(60).                         
      *---      FILLER                                                          
          02 FILLER                     PIC X(226).                             
      *                                                                         
                                                                                
