      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPC1100                      *        
      ******************************************************************        
       01  RVPC1100.                                                            
      *                       NREGLE                                            
           10 PC11-NREGLE          PIC X(10).                                   
      *                       LIBELLE                                           
           10 PC11-LIBELLE         PIC X(25).                                   
      *                       NLIGNE                                            
           10 PC11-NLIGNE          PIC X(2).                                    
      *                       TVA                                               
           10 PC11-TVA             PIC X(3).                                    
      *                       CINTERFACE                                        
           10 PC11-CINTERFACE      PIC X(5).                                    
      *                       CTYPOPER                                          
           10 PC11-CTYPOPER        PIC X(5).                                    
      *                       CNATOPER                                          
           10 PC11-CNATOPER        PIC X(5).                                    
      *                       CRITERE1                                          
           10 PC11-CRITERE1        PIC X(5).                                    
      *                       CRITERE2                                          
           10 PC11-CRITERE2        PIC X(5).                                    
      *                       CRITERE3                                          
           10 PC11-CRITERE3        PIC X(5).                                    
      *                       CRITERE4                                          
           10 PC11-CRITERE4        PIC X(5).                                    
      *                       CRITERE5                                          
           10 PC11-CRITERE5        PIC X(5).                                    
      *                       CRITERE6                                          
           10 PC11-CRITERE6        PIC X(5).                                    
      *                       WSENS                                             
           10 PC11-WSENS           PIC X(1).                                    
      *                       NCLIENT                                           
           10 PC11-NCLIENT         PIC X(15).                                   
      *                       NSOC_E                                            
           10 PC11-NSOC-E          PIC X(3).                                    
      *                       NLIEU_E                                           
           10 PC11-NLIEU-E         PIC X(3).                                    
      *                       NSOC_R                                            
           10 PC11-NSOC-R          PIC X(3).                                    
      *                       NLIEU_R                                           
           10 PC11-NLIEU-R         PIC X(3).                                    
      *                       NOPERATION                                        
           10 PC11-NOPERATION      PIC X(15).                                   
      *                       LETTRAGE4                                         
           10 PC11-LETTRAGE4       PIC X(15).                                   
      *                       LETTRAGE5                                         
           10 PC11-LETTRAGE5       PIC X(15).                                   
      *                       CDEVISE                                           
           10 PC11-CDEVISE         PIC X(3).                                    
      *                       LMVT1                                             
           10 PC11-LMVT1           PIC X(25).                                   
      *                       LMVT2                                             
           10 PC11-LMVT2           PIC X(25).                                   
      *                       LMVT3                                             
           10 PC11-LMVT3           PIC X(25).                                   
      *                       LMVT4                                             
           10 PC11-LMVT4           PIC X(25).                                   
      *                       LMVT5                                             
           10 PC11-LMVT5           PIC X(25).                                   
      *                       CPAYS                                             
           10 PC11-CPAYS           PIC X(3).                                    
      *                       WTVA                                              
           10 PC11-WTVA            PIC X(1).                                    
      *                       REFDOC                                            
           10 PC11-REFDOC          PIC X(12).                                   
      *                       PC02                                              
           10 PC11-PC02            PIC X(1).                                    
      *                       PERIOD                                            
           10 PC11-PERIOD          PIC X(1).                                    
      *                       JOUR                                              
           10 PC11-JOUR            PIC X(2).                                    
      *                       MONTANT                                           
           10 PC11-MONTANT         PIC X(5).                                    
      *                       ACID                                              
           10 PC11-ACID            PIC X(8).                                    
      *                       DSYST                                             
           10 PC11-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DOPERATION                                        
           10 PC11-DOPERATION      PIC X(8).                                    
      *                       NFOUR                                             
           10 PC11-NFOUR           PIC X(15).                                   
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 39      *        
      ******************************************************************        
                                                                                
