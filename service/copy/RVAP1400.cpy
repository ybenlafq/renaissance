      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1400                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1400.                                                            
           02  AP14-NENCAISSE      PIC X(0014).                                 
           02  AP14-PMONTCESU      PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP14-CAUTREMODE     PIC X(0005).                                 
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP14-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP14-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP14-PMONTCESU-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP14-PMONTCESU-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP14-CAUTREMODE-F   PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP14-CAUTREMODE-F   PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
