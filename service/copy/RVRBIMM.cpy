      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBIMM PLAGES NUMEROTATION ABEL IMMO    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBIMM.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBIMM.                                                             
      *}                                                                        
           05  RBIMM-CTABLEG2    PIC X(15).                                     
           05  RBIMM-CTABLEG2-REDEF REDEFINES RBIMM-CTABLEG2.                   
               10  RBIMM-TYEQP           PIC X(05).                             
           05  RBIMM-WTABLEG     PIC X(80).                                     
           05  RBIMM-WTABLEG-REDEF  REDEFINES RBIMM-WTABLEG.                    
               10  RBIMM-BINFERIE        PIC X(10).                             
               10  RBIMM-BSUPERIE        PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBIMM-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBIMM-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBIMM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBIMM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBIMM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBIMM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
