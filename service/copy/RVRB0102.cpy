      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTRB01                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0102.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0102.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUM.RECEPTION                                     
           10 RB01-NRECEPTION      PIC S9(8)V USAGE COMP-3.                     
      *    *************************************************************        
      *                       LOT PRODUCTION                                    
           10 RB01-NLOTPROD        PIC X(15).                                   
      *    *************************************************************        
      *                       CODE EAN                                          
           10 RB01-NEAN            PIC X(13).                                   
      *    *************************************************************        
      *                       N.SERIE                                           
           10 RB01-NSERIE          PIC X(15).                                   
      *    *************************************************************        
      *                       REF.FOURNISSEUR                                   
           10 RB01-LREFFOURN       PIC X(25).                                   
      *    *************************************************************        
      *                       REF.COMMERCIALE                                   
           10 RB01-LREFCOMM        PIC X(20).                                   
      *    *************************************************************        
      *                       ADR.MAC                                           
           10 RB01-ADRMAC          PIC X(12).                                   
      *    *************************************************************        
      *                       VERS.HARDWARE                                     
           10 RB01-NHARDWARE       PIC X(9).                                    
      *    *************************************************************        
      *                       VERS.FIRMWARE                                     
           10 RB01-NFIRMWARE       PIC X(6).                                    
      *    *************************************************************        
      *                       D.PRODUCTION                                      
           10 RB01-DPRODUCTION     PIC X(8).                                    
      *    *************************************************************        
      *                       LOGISTICIEN RECEPTION                             
           10 RB01-LLGRECEPTION    PIC X(13).                                   
      *    *************************************************************        
      *                       LOGIN                                             
           10 RB01-CLOGIN          PIC X(16).                                   
      *    *************************************************************        
      *                       MOT DE PASSE                                      
           10 RB01-CPASSE          PIC X(16).                                   
      *    *************************************************************        
      *                       NOM.CONSTRUCTEUR                                  
           10 RB01-LCONSTRUCTEUR   PIC X(25).                                   
      *    *************************************************************        
      *                       T.EQUIPEMENT                                      
           10 RB01-TEQUIPEMENT     PIC X(5).                                    
      *    *************************************************************        
      *                       FLG LIVRAISON                                     
           10 RB01-WLIVRAISON      PIC X(1).                                    
      *    *************************************************************        
      *                       FLG COMPLETEL                                     
           10 RB01-WCOMPLETEL      PIC X(1).                                    
      *    *************************************************************        
      *                       DATE SYSTEME DE M.A.J LIGNE                       
           10 RB01-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       FLG PRODUIT RENOVE                                
           10 RB01-WRETOURHS       PIC X(1).                                    
      *    *************************************************************        
      *                       DATE DE MAJ DE LA GEF                             
           10 RB01-DTRTGEF         PIC X(8).                                    
      *    *************************************************************        
      *                       ADR.MAC                                           
           10 RB01-ADRMAC2         PIC X(12).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0102-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0102-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NRECEPTION-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NRECEPTION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NLOTPROD-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NLOTPROD-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NEAN-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NEAN-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NSERIE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NSERIE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-LREFFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-LREFFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-LREFCOMM-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-LREFCOMM-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-ADRMAC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-ADRMAC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NHARDWARE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NHARDWARE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-NFIRMWARE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-NFIRMWARE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-DPRODUCTION-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-DPRODUCTION-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-LLGRECEPTION-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-LLGRECEPTION-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-CLOGIN-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-CLOGIN-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-CPASSE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-CPASSE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-LCONSTRUCTEUR-F PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-LCONSTRUCTEUR-F PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-TEQUIPEMENT-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-TEQUIPEMENT-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-WLIVRAISON-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-WLIVRAISON-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-WCOMPLETEL-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-WCOMPLETEL-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-WRETOURHS-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-WRETOURHS-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-DTRTGEF-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-DTRTGEF-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB01-ADRMAC2-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB01-ADRMAC2-F       PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 18      *        
      ******************************************************************        
                                                                                
