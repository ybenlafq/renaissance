      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBEQP TYPE EQPMENT REDBOX              *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBEQP.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBEQP.                                                             
      *}                                                                        
           05  RBEQP-CTABLEG2    PIC X(15).                                     
           05  RBEQP-CTABLEG2-REDEF REDEFINES RBEQP-CTABLEG2.                   
               10  RBEQP-TYEQP           PIC X(05).                             
           05  RBEQP-WTABLEG     PIC X(80).                                     
           05  RBEQP-WTABLEG-REDEF  REDEFINES RBEQP-WTABLEG.                    
               10  RBEQP-WCPLT           PIC X(01).                             
               10  RBEQP-LEQPM           PIC X(20).                             
               10  RBEQP-NEAN            PIC X(13).                             
               10  RBEQP-IMMO            PIC X(01).                             
               10  RBEQP-WDIVERS         PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBEQP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBEQP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBEQP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBEQP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBEQP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBEQP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
