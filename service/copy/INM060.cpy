      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM060 AU 19/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,08,BI,A,                          *        
      *                           32,08,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM060.                                                        
            05 NOMETAT-INM060           PIC X(6) VALUE 'INM060'.                
            05 RUPTURES-INM060.                                                 
           10 INM060-DATETR             PIC X(08).                      007  008
           10 INM060-NSOC               PIC X(03).                      015  003
           10 INM060-NLIEU              PIC X(03).                      018  003
           10 INM060-NCAISS             PIC X(03).                      021  003
           10 INM060-NTRANS             PIC X(08).                      024  008
           10 INM060-NVENTE             PIC X(08).                      032  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM060-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 INM060-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM060.                                                   
           10 INM060-CDEVAUT            PIC X(03).                      042  003
           10 INM060-CDEVRF             PIC X(03).                      045  003
           10 INM060-LDEVAUT            PIC X(05).                      048  005
           10 INM060-LDEVRF             PIC X(05).                      053  005
           10 INM060-CUMUL-AU-PTOTAL    PIC S9(10)V9(2) COMP-3.         058  007
           10 INM060-PVENTE             PIC S9(08)V9(2) COMP-3.         065  006
           10 INM060-DATETR2            PIC X(08).                      071  008
            05 FILLER                      PIC X(434).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM060-LONG           PIC S9(4)   COMP  VALUE +078.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM060-LONG           PIC S9(4) COMP-5  VALUE +078.           
                                                                                
      *}                                                                        
