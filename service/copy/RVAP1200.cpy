      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1200                         
      *---------------------------------------------------------                
      *                                                                         
            EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1200.                                                            
           02  AP12-NSEQID         PIC S9(18)  USAGE COMP-3.                    
           02  AP12-DCREATION      PIC X(0008).                                 
           02  AP12-NCRI           PIC X(0017).                                 
           02  AP12-NENCAISSE      PIC X(0014).                                 
           02  AP12-DENCAISSE      PIC X(0008).                                 
           02  AP12-PMONTCRI       PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP12-NLIEU          PIC X(0003).                                 
           02  AP12-NSOCA2I        PIC X(0003).                                 
           02  AP12-DATETRAIT      PIC X(0008).                                 
           02  AP12-DATERETRY      PIC X(0008).                                 
           02  AP12-NATTEST        PIC S9(18)  USAGE COMP-3.                    
           02  AP12-CODEANO        PIC X(0002).                                 
           02  AP12-NIDANO         PIC S9(18)  USAGE COMP-3.                    
           02  AP12-DATEANO        PIC X(0008).                                 
           02  AP12-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NSEQ-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NSEQ-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DCREATION-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-DCREATION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NCRI-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NCRI-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-DENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-PMONTCRI-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-PMONTCRI-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NSOCA2I-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NSOCA2I-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DATETRAIT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-DATETRAIT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DATERETRY-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-DATERETRY-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NATTEST-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NATTEST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-CODEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-CODEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-NIDANO-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-NIDANO-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DATEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP12-DATEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP12-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP12-DSYST-F        PIC S9(4) COMP-5.                            
            EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
