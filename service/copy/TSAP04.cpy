      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP04-RECORD.                                                      
           05 TS-AP04-TABLE            OCCURS 10.                               
              10 TS-AP04-DENCAISSE      PIC X(08).                              
              10 TS-AP04-NLIEU          PIC X(03).                              
              10 TS-AP04-NVENTE         PIC X(14).                              
              10 TS-AP04-CPRESTA        PIC X(05).                              
              10 TS-AP04-PPRESTA        PIC S9(7)V9(2) USAGE COMP-3.            
              10 TS-AP04-CTITRENOM      PIC X(05).                              
              10 TS-AP04-LNOM           PIC X(25).                              
              10 TS-AP04-LPRENOM        PIC X(15).                              
              10 TS-AP04-LADRESSE       PIC X(38).                              
              10 TS-AP04-NSEQID         PIC S9(18) COMP-3.                      
                                                                                
