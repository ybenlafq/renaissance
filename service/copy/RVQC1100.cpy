      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVQC1100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQC1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQC1100.                                                            
      *}                                                                        
      *                       NCISOC                                            
           10 QC11-NCISOC          PIC X(3).                                    
      *                       CTQUEST                                           
           10 QC11-CTQUEST         PIC S9(2)V USAGE COMP-3.                     
      *                       NCARTE                                            
           10 QC11-NCARTE          PIC X(15).                                   
      *                       CCONTEXTE                                         
           10 QC11-CCONTEXTE       PIC X(10).                                   
      *                       NSOCIETE                                          
           10 QC11-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 QC11-NLIEU           PIC X(3).                                    
      *                       LSOCIETE                                          
           10 QC11-LSOCIETE        PIC X(5).                                    
      *                       LLIEU                                             
           10 QC11-LLIEU           PIC X(20).                                   
      *                       FILIALEALT                                        
           10 QC11-FILIALEALT      PIC X(3).                                    
      *                       LIEUALT                                           
           10 QC11-LIEUALT         PIC X(3).                                    
      *                       LSOCIETEALT                                       
           10 QC11-LSOCIETEALT     PIC X(5).                                    
      *                       LLIEUALT                                          
           10 QC11-LLIEUALT        PIC X(20).                                   
      *                       DEVENT                                            
           10 QC11-DEVENT          PIC X(8).                                    
      *                       NREFEVENT                                         
           10 QC11-NREFEVENT       PIC X(30).                                   
      *                       CINT                                              
           10 QC11-CINT            PIC X(10).                                   
      *                       CINT2                                             
           10 QC11-CINT2           PIC X(10).                                   
      *                       CINT3                                             
           10 QC11-CINT3           PIC X(10).                                   
      *                       NCLIENT                                           
           10 QC11-NCLIENT         PIC X(8).                                    
      *                       CTITRE                                            
           10 QC11-CTITRE          PIC X(5).                                    
      *                       LNOM                                              
           10 QC11-LNOM            PIC X(25).                                   
      *                       LPRENOM                                           
           10 QC11-LPRENOM         PIC X(15).                                   
      *                       EMAIL                                             
           10 QC11-EMAIL           PIC X(50).                                   
      *                       NTELCLIENT                                        
           10 QC11-NTELCLIENT      PIC X(10).                                   
      *                       NGSM                                              
           10 QC11-NGSM            PIC X(10).                                   
      *                       NADDR                                             
           10 QC11-NADDR           PIC X(8).                                    
      *                       CPOSTAL                                           
           10 QC11-CPOSTAL         PIC X(5).                                    
      *                       LCOMMUNE                                          
           10 QC11-LCOMMUNE        PIC X(32).                                   
      *                       LPAYS                                             
           10 QC11-LPAYS           PIC X(32).                                   
      *                       CTYPE                                             
           10 QC11-CTYPE           PIC X(1).                                    
      *                       NCODIC                                            
           10 QC11-NCODIC          PIC X(7).                                    
      *                       CENREG                                            
           10 QC11-CENREG          PIC X(5).                                    
      *                       CFAM                                              
           10 QC11-CFAM            PIC X(5).                                    
      *                       LFAM                                              
           10 QC11-LFAM            PIC X(20).                                   
      *                       CMARQ                                             
           10 QC11-CMARQ           PIC X(5).                                    
      *                       LMARQ                                             
           10 QC11-LMARQ           PIC X(20).                                   
      *                       LREFPRODUIT                                       
           10 QC11-LREFPRODUIT     PIC X(20).                                   
      *                       QVENDUE                                           
           10 QC11-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
      *                       CMODDEL                                           
           10 QC11-CMODDEL         PIC X(3).                                    
      *                       WEMPORTE                                          
           10 QC11-WEMPORTE        PIC X(1).                                    
      *                       DDELIV                                            
           10 QC11-DDELIV          PIC X(8).                                    
      *                       PVTOTAL                                           
           10 QC11-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       REMISE                                            
           10 QC11-REMISE          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       DGARANTIE                                         
           10 QC11-DGARANTIE       PIC X(8).                                    
      *                       CPSE                                              
           10 QC11-CPSE            PIC X(5).                                    
      *                       LPSE                                              
           10 QC11-LPSE            PIC X(20).                                   
      *                       REGIME                                            
           10 QC11-REGIME          PIC X(3).                                    
      *                       NSEQNQ                                            
           10 QC11-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
      *                       DALT                                              
           10 QC11-DALT            PIC X(8).                                    
      *                       CTLIVREUR                                         
           10 QC11-CTLIVREUR       PIC X(5).                                    
      *                       CPERIM                                            
           10 QC11-CPERIM          PIC X(5).                                    
      *                       WCREDIT                                           
           10 QC11-WCREDIT         PIC X(1).                                    
      *                       CPLAGE                                            
           10 QC11-CPLAGE          PIC X(2).                                    
      *                       CDINTT                                            
           10 QC11-CDINTT          PIC X(3).                                    
      *                       CPANNE                                            
           10 QC11-CPANNE          PIC X(2).                                    
      *                       DELAI                                             
           10 QC11-DELAI           PIC X(3).                                    
      *                       CVAR1                                             
           10 QC11-CVAR1           PIC X(50).                                   
      *                       CVAR2                                             
           10 QC11-CVAR2           PIC X(50).                                   
      *                       CVAR3                                             
           10 QC11-CVAR3           PIC X(50).                                   
      *                       CVAR4                                             
           10 QC11-CVAR4           PIC X(50).                                   
      *                       CVAR5                                             
           10 QC11-CVAR5           PIC X(50).                                   
      *                       DCREATION                                         
           10 QC11-DCREATION       PIC X(8).                                    
      *                       DTESSI                                            
           10 QC11-DTESSI          PIC X(8).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 62      *        
      ******************************************************************        
                                                                                
