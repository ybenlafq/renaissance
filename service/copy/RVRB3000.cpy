      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTRB30                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3000.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUM SERIE                                         
           10 RB30-NSERIE          PIC X(15).                                   
      *    *************************************************************        
      *                       NCODIC                                            
           10 RB30-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       NUM IMMOBILISATION                                
           10 RB30-NIMMO           PIC X(10).                                   
      *    *************************************************************        
      *                       TYPE EQP                                          
           10 RB30-TEQUIPEMENT     PIC X(5).                                    
      *    *************************************************************        
      *                       NOM FOURNISSEUR                                   
           10 RB30-NFOURNISSEUR    PIC X(25).                                   
      *    *************************************************************        
      *                       DATE SYSTEME DE M.A.J LIGNE                       
           10 RB30-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-NSERIE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-NSERIE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-NIMMO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-NIMMO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-TEQUIPEMENT-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-TEQUIPEMENT-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-NFOURNISSEUR-F  PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-NFOURNISSEUR-F  PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB30-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB30-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
