      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC0101.                                                            
      *}                                                                        
      *                       RANDOMNUM                                         
           10 PC01-RANDOMNUM       PIC X(19).                                   
      *                       SERIALNUM                                         
           10 PC01-SERIALNUM       PIC X(13).                                   
      *                       CARDTYPECODE                                      
           10 PC01-CARDTYPECODE    PIC X(20).                                   
      *                       CDEV                                              
           10 PC01-CDEV            PIC X(4).                                    
      *                       DVALIDF                                           
           10 PC01-DVALIDF         PIC X(8).                                    
      *                       DVALIDR                                           
           10 PC01-DVALIDR         PIC X(8).                                    
      *                       STATUT                                            
           10 PC01-STATUT          PIC X(5).                                    
      *                       CMAP                                              
           10 PC01-CMAP            PIC S9(7)V9(4) USAGE COMP-3.                 
      *                       SOLDED                                            
           10 PC01-SOLDED          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       SOLDEP                                            
           10 PC01-SOLDEP          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       NSOCVTE                                           
           10 PC01-NSOCVTE         PIC X(3).                                    
      *                       NLIEUVTE                                          
           10 PC01-NLIEUVTE        PIC X(3).                                    
      *                       DVENTE                                            
           10 PC01-DVENTE          PIC X(8).                                    
      *                       CPREST                                            
           10 PC01-CPREST          PIC X(5).                                    
      *                       NSOCNET                                           
           10 PC01-NSOCNET         PIC X(3).                                    
      *                       WDEVCCP                                           
           10 PC01-WDEVCCP         PIC X(1).                                    
      *                       COMMENT                                           
           10 PC01-COMMENT         PIC X(80).                                   
      *                       DSYST                                             
           10 PC01-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DTSOLDE                                           
           10 PC01-DTSOLDE         PIC X(08).                                   
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
