      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHL1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHL1500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL1500.                                                            
           02  HL15-NOMMAP                                                      
               PIC X(0008).                                                     
           02  HL15-ZECRAN                                                      
               PIC X(0008).                                                     
           02  HL15-CCRITERE                                                    
               PIC X(0018).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-NOCCUR                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-NOCCUR                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  HL15-NSEQCRIT                                                    
               PIC S9(5) COMP-3.                                                
           02  HL15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHL1500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL1500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-NOMMAP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-NOMMAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-ZECRAN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-ZECRAN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-CCRITERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-CCRITERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-NOCCUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-NOCCUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-NSEQCRIT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-NSEQCRIT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
