      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    COPIE DE LA TS UTILIISEE DANS LE PROGRAMME TRB72 : TSRB72   *        
      *----------------------------------------------------------------*        
       01  TS-RB72-IDENT.                                                       
           05  TS-RB72-TERMID              PIC X(04).                           
           05  TS-RB72-TRANID              PIC X(04).                           
       01  FILLER                    REDEFINES TS-RB72-IDENT.                   
           05  TS-RB72-NAME                PIC X(08).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RANG-TS-RB72                    PIC S9(4) COMP.                      
      *--                                                                       
       01  RANG-TS-RB72                    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-RB72-LONG                    PIC S9(4) COMP VALUE +594.           
      *--                                                                       
       01  TS-RB72-LONG                    PIC S9(4) COMP-5 VALUE +594.         
      *}                                                                        
       01  TS-RB72-DATA.                                                        
           05  TS-RB72-LIGNE               OCCURS 06.                           
               10  TS-RB72-NENVOI          PIC X(07).                           
               10  TS-RB72-NSOC            PIC X(03).                           
               10  TS-RB72-CTRAIT          PIC X(05).                           
               10  TS-RB72-NSERIEE         PIC X(16).                           
               10  TS-RB72-NSERIER         PIC X(16).                           
               10  TS-RB72-NDOSSIER        PIC X(20).                           
               10  TS-RB72-WLREGUL         PIC X(30).                           
               10  TS-RB72-STATUT          PIC X(01).                           
               10  TS-RB72-MODIF           PIC X(01).                           
      *                                                                         
      *--- FIN DE LA TS TSRB72 ----------------------------------------*        
                                                                                
