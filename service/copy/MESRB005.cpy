      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ REPONSE INTERRO BASE PARC                             
      *   du S.I. REDBOX. (RBxRIntBParc)                                        
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESRB005-DATA.                                                        
      *                                                                         
      *                                                                         
      *                                                                         
      *   N� D'INTERROGATION                                                    
          05 MESRB05-NINTER        PIC    X(10).                                
      *   R�PONSE BASE PARC                                                     
          05 MESRB05-REPBPARC      PIC    X(01).                                
      *{ remove-comma-in-dde 1.5                                                
      *      88 MESRB05-TRUE       VALUE  '0' , 't' , 'T'.                      
      *--                                                                       
             88 MESRB05-TRUE       VALUE  '0'   't'   'T'.                      
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *      88 MESRB05-FALSE      VALUE  '1' , 'f' , 'F'.                      
      *--                                                                       
             88 MESRB05-FALSE      VALUE  '1'   'f'   'F'.                      
      *}                                                                        
      *   STATUS                                                                
          05 MESRB05-STATUS        PIC    X(25).                                
      *   erreur                                                                
          05 MESRB05-ERREUR        PIC    X(25).                                
                                                                                
