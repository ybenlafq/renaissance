      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVBB1100                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  DCLRVBB1100.                                                         
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  DCLRVBB1100.                                                         
      *}                                                                        
      *                       NSOCIETE                                          
           10 BB11-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 BB11-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 BB11-NVENTE          PIC X(7).                                    
      *                       CTYPENREG                                         
           10 BB11-CTYPENREG       PIC X(1).                                    
      *                       NCODICGRP                                         
           10 BB11-NCODICGRP       PIC X(7).                                    
      *                       NCODIC                                            
           10 BB11-NCODIC          PIC X(7).                                    
      *                       NSEQ                                              
           10 BB11-NSEQ            PIC X(2).                                    
      *                       CMODDEL                                           
           10 BB11-CMODDEL         PIC X(3).                                    
      *                       DDELIV                                            
           10 BB11-DDELIV          PIC X(8).                                    
      *                       NORDRE                                            
           10 BB11-NORDRE          PIC X(5).                                    
      *                       CENREG                                            
           10 BB11-CENREG          PIC X(5).                                    
      *                       QVENDUE                                           
           10 BB11-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
      *                       PVUNIT                                            
           10 BB11-PVUNIT          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVUNITF                                           
           10 BB11-PVUNITF         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVTOTAL                                           
           10 BB11-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CEQUIPE                                           
           10 BB11-CEQUIPE         PIC X(5).                                    
      *                       NLIGNE                                            
           10 BB11-NLIGNE          PIC X(2).                                    
      *                       TAUXTVA                                           
           10 BB11-TAUXTVA         PIC S9(3)V9(2) USAGE COMP-3.                 
      *                       QCONDT                                            
           10 BB11-QCONDT          PIC S9(5)V USAGE COMP-3.                     
      *                       PRMP                                              
           10 BB11-PRMP            PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       WEMPORTE                                          
           10 BB11-WEMPORTE        PIC X(1).                                    
      *                       CPLAGE                                            
           10 BB11-CPLAGE          PIC X(2).                                    
      *                       CPROTOUR                                          
           10 BB11-CPROTOUR        PIC X(5).                                    
      *                       CADRTOUR                                          
           10 BB11-CADRTOUR        PIC X(1).                                    
      *                       CPOSTAL                                           
           10 BB11-CPOSTAL         PIC X(5).                                    
      *                       LCOMMENT                                          
           10 BB11-LCOMMENT        PIC X(35).                                   
      *                       CVENDEUR                                          
           10 BB11-CVENDEUR        PIC X(6).                                    
      *                       NSOCLIVR                                          
           10 BB11-NSOCLIVR        PIC X(3).                                    
      *                       NDEPOT                                            
           10 BB11-NDEPOT          PIC X(3).                                    
      *                       NAUTORM                                           
           10 BB11-NAUTORM         PIC X(5).                                    
      *                       WARTINEX                                          
           10 BB11-WARTINEX        PIC X(1).                                    
      *                       WEDITBL                                           
           10 BB11-WEDITBL         PIC X(1).                                    
      *                       WACOMMUTER                                        
           10 BB11-WACOMMUTER      PIC X(1).                                    
      *                       WCQERESF                                          
           10 BB11-WCQERESF        PIC X(1).                                    
      *                       NMUTATION                                         
           10 BB11-NMUTATION       PIC X(7).                                    
      *                       CTOURNEE                                          
           10 BB11-CTOURNEE        PIC X(8).                                    
      *                       WTOPELIVRE                                        
           10 BB11-WTOPELIVRE      PIC X(1).                                    
      *                       DCOMPTA                                           
           10 BB11-DCOMPTA         PIC X(8).                                    
      *                       DCREATION                                         
           10 BB11-DCREATION       PIC X(8).                                    
      *                       HCREATION                                         
           10 BB11-HCREATION       PIC X(4).                                    
      *                       DANNULATION                                       
           10 BB11-DANNULATION     PIC X(8).                                    
      *                       NLIEUORIG                                         
           10 BB11-NLIEUORIG       PIC X(3).                                    
      *                       WINTMAJ                                           
           10 BB11-WINTMAJ         PIC X(1).                                    
      *                       DSYST                                             
           10 BB11-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DSTAT                                             
           10 BB11-DSTAT           PIC X(4).                                    
      *                       CPRESTGRP                                         
           10 BB11-CPRESTGRP       PIC X(5).                                    
      *                       NSOCMODIF                                         
           10 BB11-NSOCMODIF       PIC X(3).                                    
      *                       NLIEUMODIF                                        
           10 BB11-NLIEUMODIF      PIC X(3).                                    
      *                       DATENC                                            
           10 BB11-DATENC          PIC X(8).                                    
      *                       CDEV                                              
           10 BB11-CDEV            PIC X(3).                                    
      *                       WUNITR                                            
           10 BB11-WUNITR          PIC X(20).                                   
      *                       LRCMMT                                            
           10 BB11-LRCMMT          PIC X(10).                                   
      *                       NSOCP                                             
           10 BB11-NSOCP           PIC X(3).                                    
      *                       NLIEUP                                            
           10 BB11-NLIEUP          PIC X(3).                                    
      *                       NTRANS                                            
           10 BB11-NTRANS          PIC S9(8)V USAGE COMP-3.                     
      *                       NSEQFV                                            
           10 BB11-NSEQFV          PIC X(2).                                    
      *                       NSEQNQ                                            
           10 BB11-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
      *                       NSEQREF                                           
           10 BB11-NSEQREF         PIC S9(5)V USAGE COMP-3.                     
      *                       NMODIF                                            
           10 BB11-NMODIF          PIC S9(5)V USAGE COMP-3.                     
      *                       NSOCORIG                                          
           10 BB11-NSOCORIG        PIC X(3).                                    
      *                       DTOPE                                             
           10 BB11-DTOPE           PIC X(8).                                    
      *                       NSOCLIVR1                                         
           10 BB11-NSOCLIVR1       PIC X(3).                                    
      *                       NDEPOT1                                           
           10 BB11-NDEPOT1         PIC X(3).                                    
      *                       NSOCGEST                                          
           10 BB11-NSOCGEST        PIC X(3).                                    
      *                       NLIEUGEST                                         
           10 BB11-NLIEUGEST       PIC X(3).                                    
      *                       NSOCDEPLIV                                        
           10 BB11-NSOCDEPLIV      PIC X(3).                                    
      *                       NLIEUDEPLIV                                       
           10 BB11-NLIEUDEPLIV     PIC X(3).                                    
      *                       TYPVTE                                            
           10 BB11-TYPVTE          PIC X(1).                                    
      *                       CTYPENT                                           
           10 BB11-CTYPENT         PIC X(2).                                    
      *                       NLIEN                                             
           10 BB11-NLIEN           PIC S9(5)V USAGE COMP-3.                     
      *                       NACTVTE                                           
           10 BB11-NACTVTE         PIC S9(5)V USAGE COMP-3.                     
      *                       PVCODIG                                           
           10 BB11-PVCODIG         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       QTCODIG                                           
           10 BB11-QTCODIG         PIC S9(5)V USAGE COMP-3.                     
      *                       DPRLG                                             
           10 BB11-DPRLG           PIC X(8).                                    
      *                       CALERTE                                           
           10 BB11-CALERTE         PIC X(5).                                    
      *                       CREPRISE                                          
           10 BB11-CREPRISE        PIC X(5).                                    
      *                       NSEQENS                                           
           10 BB11-NSEQENS         PIC S9(5)V USAGE COMP-3.                     
      *                       MPRIMECLI                                         
           10 BB11-MPRIMECLI       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XNSOCIETE                                         
           10 BB11-XNSOCIETE       PIC X(3).                                    
      *                       XNLIEU                                            
           10 BB11-XNLIEU          PIC X(3).                                    
      *                       XNVENTE                                           
           10 BB11-XNVENTE         PIC X(7).                                    
      *                       XCTYPENREG                                        
           10 BB11-XCTYPENREG      PIC X(1).                                    
      *                       XNCODICGRP                                        
           10 BB11-XNCODICGRP      PIC X(7).                                    
      *                       XNCODIC                                           
           10 BB11-XNCODIC         PIC X(7).                                    
      *                       XNSEQ                                             
           10 BB11-XNSEQ           PIC X(2).                                    
      *                       XCMODDEL                                          
           10 BB11-XCMODDEL        PIC X(3).                                    
      *                       XDDELIV                                           
           10 BB11-XDDELIV         PIC X(8).                                    
      *                       XNORDRE                                           
           10 BB11-XNORDRE         PIC X(5).                                    
      *                       XCENREG                                           
           10 BB11-XCENREG         PIC X(5).                                    
      *                       XQVENDUE                                          
           10 BB11-XQVENDUE        PIC S9(5)V USAGE COMP-3.                     
      *                       XPVUNIT                                           
           10 BB11-XPVUNIT         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XPVUNITF                                          
           10 BB11-XPVUNITF        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XPVTOTAL                                          
           10 BB11-XPVTOTAL        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XCEQUIPE                                          
           10 BB11-XCEQUIPE        PIC X(5).                                    
      *                       XNLIGNE                                           
           10 BB11-XNLIGNE         PIC X(2).                                    
      *                       XTAUXTVA                                          
           10 BB11-XTAUXTVA        PIC S9(3)V9(2) USAGE COMP-3.                 
      *                       XQCONDT                                           
           10 BB11-XQCONDT         PIC S9(5)V USAGE COMP-3.                     
      *                       XPRMP                                             
           10 BB11-XPRMP           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XWEMPORTE                                         
           10 BB11-XWEMPORTE       PIC X(1).                                    
      *                       XCPLAGE                                           
           10 BB11-XCPLAGE         PIC X(2).                                    
      *                       XCPROTOUR                                         
           10 BB11-XCPROTOUR       PIC X(5).                                    
      *                       XCADRTOUR                                         
           10 BB11-XCADRTOUR       PIC X(1).                                    
      *                       XCPOSTAL                                          
           10 BB11-XCPOSTAL        PIC X(5).                                    
      *                       XLCOMMENT                                         
           10 BB11-XLCOMMENT       PIC X(35).                                   
      *                       XCVENDEUR                                         
           10 BB11-XCVENDEUR       PIC X(6).                                    
      *                       XNSOCLIVR                                         
           10 BB11-XNSOCLIVR       PIC X(3).                                    
      *                       XNDEPOT                                           
           10 BB11-XNDEPOT         PIC X(3).                                    
      *                       XNAUTORM                                          
           10 BB11-XNAUTORM        PIC X(5).                                    
      *                       XWARTINEX                                         
           10 BB11-XWARTINEX       PIC X(1).                                    
      *                       XWEDITBL                                          
           10 BB11-XWEDITBL        PIC X(1).                                    
      *                       XWACOMMUTER                                       
           10 BB11-XWACOMMUTER     PIC X(1).                                    
      *                       XWCQERESF                                         
           10 BB11-XWCQERESF       PIC X(1).                                    
      *                       XNMUTATION                                        
           10 BB11-XNMUTATION      PIC X(7).                                    
      *                       XCTOURNEE                                         
           10 BB11-XCTOURNEE       PIC X(8).                                    
      *                       XWTOPELIVRE                                       
           10 BB11-XWTOPELIVRE     PIC X(1).                                    
      *                       XDCOMPTA                                          
           10 BB11-XDCOMPTA        PIC X(8).                                    
      *                       XDCREATION                                        
           10 BB11-XDCREATION      PIC X(8).                                    
      *                       XHCREATION                                        
           10 BB11-XHCREATION      PIC X(4).                                    
      *                       XDANNULATION                                      
           10 BB11-XDANNULATION    PIC X(8).                                    
      *                       XNLIEUORIG                                        
           10 BB11-XNLIEUORIG      PIC X(3).                                    
      *                       XWINTMAJ                                          
           10 BB11-XWINTMAJ        PIC X(1).                                    
      *                       XDSYST                                            
           10 BB11-XDSYST          PIC S9(13)V USAGE COMP-3.                    
      *                       XDSTAT                                            
           10 BB11-XDSTAT          PIC X(4).                                    
      *                       XCPRESTGRP                                        
           10 BB11-XCPRESTGRP      PIC X(5).                                    
      *                       XNSOCMODIF                                        
           10 BB11-XNSOCMODIF      PIC X(3).                                    
      *                       XNLIEUMODIF                                       
           10 BB11-XNLIEUMODIF     PIC X(3).                                    
      *                       XDATENC                                           
           10 BB11-XDATENC         PIC X(8).                                    
      *                       XCDEV                                             
           10 BB11-XCDEV           PIC X(3).                                    
      *                       XWUNITR                                           
           10 BB11-XWUNITR         PIC X(20).                                   
      *                       XLRCMMT                                           
           10 BB11-XLRCMMT         PIC X(10).                                   
      *                       XNSOCP                                            
           10 BB11-XNSOCP          PIC X(3).                                    
      *                       XNLIEUP                                           
           10 BB11-XNLIEUP         PIC X(3).                                    
      *                       XNTRANS                                           
           10 BB11-XNTRANS         PIC S9(8)V USAGE COMP-3.                     
      *                       XNSEQFV                                           
           10 BB11-XNSEQFV         PIC X(2).                                    
      *                       XNSEQNQ                                           
           10 BB11-XNSEQNQ         PIC S9(5)V USAGE COMP-3.                     
      *                       XNSEQREF                                          
           10 BB11-XNSEQREF        PIC S9(5)V USAGE COMP-3.                     
      *                       XNMODIF                                           
           10 BB11-XNMODIF         PIC S9(5)V USAGE COMP-3.                     
      *                       XNSOCORIG                                         
           10 BB11-XNSOCORIG       PIC X(3).                                    
      *                       XDTOPE                                            
           10 BB11-XDTOPE          PIC X(8).                                    
      *                       XNSOCLIVR1                                        
           10 BB11-XNSOCLIVR1      PIC X(3).                                    
      *                       XNDEPOT1                                          
           10 BB11-XNDEPOT1        PIC X(3).                                    
      *                       XNSOCGEST                                         
           10 BB11-XNSOCGEST       PIC X(3).                                    
      *                       XNLIEUGEST                                        
           10 BB11-XNLIEUGEST      PIC X(3).                                    
      *                       XNSOCDEPLIV                                       
           10 BB11-XNSOCDEPLIV     PIC X(3).                                    
      *                       XNLIEUDEPLIV                                      
           10 BB11-XNLIEUDEPLIV    PIC X(3).                                    
      *                       XTYPVTE                                           
           10 BB11-XTYPVTE         PIC X(1).                                    
      *                       XCTYPENT                                          
           10 BB11-XCTYPENT        PIC X(2).                                    
      *                       XNLIEN                                            
           10 BB11-XNLIEN          PIC S9(5)V USAGE COMP-3.                     
      *                       XNACTVTE                                          
           10 BB11-XNACTVTE        PIC S9(5)V USAGE COMP-3.                     
      *                       XPVCODIG                                          
           10 BB11-XPVCODIG        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       XQTCODIG                                          
           10 BB11-XQTCODIG        PIC S9(5)V USAGE COMP-3.                     
      *                       XDPRLG                                            
           10 BB11-XDPRLG          PIC X(8).                                    
      *                       XCALERTE                                          
           10 BB11-XCALERTE        PIC X(5).                                    
      *                       XCREPRISE                                         
           10 BB11-XCREPRISE       PIC X(5).                                    
      *                       XNSEQENS                                          
           10 BB11-XNSEQENS        PIC S9(5)V USAGE COMP-3.                     
      *                       XMPRIMECLI                                        
           10 BB11-XMPRIMECLI      PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       IBMSNAP_COMMITSEQ                                 
           10 BB11-IBMSNAP-COMMITSEQ                                            
              PIC X(10).                                                        
      *                       IBMSNAP_INTENTSEQ                                 
           10 BB11-IBMSNAP-INTENTSEQ                                            
              PIC X(10).                                                        
      *                       IBMSNAP_OPERATION                                 
           10 BB11-IBMSNAP-OPERATION                                            
              PIC X(1).                                                         
      *                       IBMSNAP_LOGMARKER                                 
           10 BB11-IBMSNAP-LOGMARKER                                            
              PIC X(26).                                                        
      *                       DSYSTUPD                                          
           10 BB11-DSYSTUPD        PIC S9(13)V USAGE COMP-3.                    
      *                       CTYPEUPD                                          
           10 BB11-CTYPEUPD        PIC X(1).                                    
      *                       DCREA                                             
           10 BB11-DCREA           PIC X(10).                                   
      *                       COMMENT                                           
           10 BB11-COMMENT         PIC X(70).                                   
      *                       CSTATUT                                           
           10 BB11-CSTATUT         PIC X(5).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 165     *        
      ******************************************************************        
                                                                                
