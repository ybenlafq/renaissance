      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM055 AU 05/11/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,03,BI,A,                          *        
      *                           26,03,BI,A,                          *        
      *                           29,03,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM055.                                                        
            05 NOMETAT-INM055           PIC X(6) VALUE 'INM055'.                
            05 RUPTURES-INM055.                                                 
           10 INM055-DATETR             PIC X(08).                      007  008
           10 INM055-NSOC               PIC X(03).                      015  003
           10 INM055-CMOPAI             PIC X(05).                      018  005
           10 INM055-NSOCEMIT           PIC X(03).                      023  003
           10 INM055-NLIEU              PIC X(03).                      026  003
           10 INM055-NCAISSE            PIC X(03).                      029  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM055-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 INM055-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM055.                                                   
           10 INM055-CDEVAUT            PIC X(03).                      034  003
           10 INM055-CDEVRF             PIC X(03).                      037  003
           10 INM055-CIMPAI             PIC X(16).                      040  016
           10 INM055-DATED              PIC X(10).                      056  010
           10 INM055-LDEVAUT            PIC X(05).                      066  005
           10 INM055-LDEVRF             PIC X(05).                      071  005
           10 INM055-LMOPAI             PIC X(20).                      076  020
           10 INM055-NTRANS             PIC X(08).                      096  008
           10 INM055-CUMUL-AU-PDPAIE    PIC S9(10)V9(2) COMP-3.         104  007
           10 INM055-PDPAIE             PIC S9(08)V9(2) COMP-3.         111  006
            05 FILLER                      PIC X(396).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM055-LONG           PIC S9(4)   COMP  VALUE +116.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM055-LONG           PIC S9(4) COMP-5  VALUE +116.           
                                                                                
      *}                                                                        
