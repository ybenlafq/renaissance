      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0300                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQC0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQC0300.                                                            
      *}                                                                        
           02  QC03-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC03-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC03-NCARTE                                                      
               PIC X(0007).                                                     
           02  QC03-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  QC03-NLIEU                                                       
               PIC X(0003).                                                     
           02  QC03-CFAM                                                        
               PIC X(0005).                                                     
           02  QC03-CMARQ                                                       
               PIC X(0005).                                                     
           02  QC03-CDINTH                                                      
               PIC X(0003).                                                     
           02  QC03-CDINTT                                                      
               PIC X(0003).                                                     
           02  QC03-CREGGAR                                                     
               PIC X(0003).                                                     
           02  QC03-CPANNE                                                      
               PIC X(0002).                                                     
           02  QC03-DELAI                                                       
               PIC X(0003).                                                     
           02  QC03-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  QC03-DTERM                                                       
               PIC X(0008).                                                     
           02  QC03-CCOMPTOIR                                                   
               PIC X(0006).                                                     
           02  QC03-DSAISIE                                                     
               PIC X(0008).                                                     
           02  QC03-DCREATION                                                   
               PIC X(0008).                                                     
           02  QC03-DDESCENTE                                                   
               PIC X(0008).                                                     
           02  QC03-FREPONSE                                                    
               PIC X(0040).                                                     
           02  QC03-DEDIT                                                       
               PIC X(0008).                                                     
           02  QC03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQC0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQC0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-NCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-NCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CDINTH-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CDINTH-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CDINTT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CDINTT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CREGGAR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CREGGAR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CPANNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CPANNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DELAI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DELAI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DTERM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DTERM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-CCOMPTOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-CCOMPTOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DDESCENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DDESCENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-FREPONSE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-FREPONSE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DEDIT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DEDIT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
