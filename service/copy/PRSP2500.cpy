      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE SP2500                       
      ******************************************************************        
      *                                                                         
       CLEF-SP2500             SECTION.                                         
      *                                                                         
           MOVE 'RVSP2500          '       TO   TABLE-NAME.                     
           MOVE 'SP2500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-SP2500. EXIT.                                                   
                EJECT                                                           
                                                                                
