      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVHE1003                    *        
      ******************************************************************        
       01  RVHE1003.                                                            
      *                       NLIEUHED                                          
           10 HE10-NLIEUHED        PIC X(3).                                    
      *                       NGHE                                              
           10 HE10-NGHE            PIC X(7).                                    
      *                       NCODIC                                            
           10 HE10-NCODIC          PIC X(7).                                    
      *                       DVENTE                                            
           10 HE10-DVENTE          PIC X(8).                                    
      *                       NLIEUVTE                                          
           10 HE10-NLIEUVTE        PIC X(3).                                    
      *                       NVENTE                                            
           10 HE10-NVENTE          PIC X(7).                                    
      *                       DDEPOT                                            
           10 HE10-DDEPOT          PIC X(8).                                    
      *                       CTRAIT                                            
           10 HE10-CTRAIT          PIC X(5).                                    
      *                       CTYPHS                                            
           10 HE10-CTYPHS          PIC X(5).                                    
      *                       LPANNE                                            
           10 HE10-LPANNE          PIC X(20).                                   
      *                       CTITRENOM                                         
           10 HE10-CTITRENOM       PIC X(5).                                    
      *                       LNOM                                              
           10 HE10-LNOM            PIC X(25).                                   
      *                       LPRENOM                                           
           10 HE10-LPRENOM         PIC X(15).                                   
      *                       LADR1                                             
           10 HE10-LADR1           PIC X(32).                                   
      *                       LADR2                                             
           10 HE10-LADR2           PIC X(32).                                   
      *                       CPOSTAL                                           
           10 HE10-CPOSTAL         PIC X(5).                                    
      *                       LCOMMUNE                                          
           10 HE10-LCOMMUNE        PIC X(26).                                   
      *                       NTEL                                              
           10 HE10-NTEL            PIC X(8).                                    
      *                       CLIEUHET                                          
           10 HE10-CLIEUHET        PIC X(5).                                    
      *                       NRETOUR                                           
           10 HE10-NRETOUR         PIC X(7).                                    
      *                       CREFUS                                            
           10 HE10-CREFUS          PIC X(5).                                    
      *                       CPANNE                                            
           10 HE10-CPANNE          PIC X(5).                                    
      *                       CLIEUHEI                                          
           10 HE10-CLIEUHEI        PIC X(5).                                    
      *                       NLOTDIAG                                          
           10 HE10-NLOTDIAG        PIC X(7).                                    
      *                       NENVOI                                            
           10 HE10-NENVOI          PIC X(7).                                    
      *                       CTIERS                                            
           10 HE10-CTIERS          PIC X(5).                                    
      *                       NTYOPE                                            
           10 HE10-NTYOPE          PIC X(3).                                    
      *                       NSOCIETE                                          
           10 HE10-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 HE10-NLIEU           PIC X(3).                                    
      *                       NSSLIEU                                           
           10 HE10-NSSLIEU         PIC X(3).                                    
      *                       CLIEUTRT                                          
           10 HE10-CLIEUTRT        PIC X(5).                                    
      *                       WSOLDE                                            
           10 HE10-WSOLDE          PIC X(1).                                    
      *                       NSERIE                                            
           10 HE10-NSERIE          PIC X(16).                                   
      *                       NACCORD                                           
           10 HE10-NACCORD         PIC X(12).                                   
      *                       LNOMACCORD                                        
           10 HE10-LNOMACCORD      PIC X(10).                                   
      *                       DSYST                                             
           10 HE10-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       ADRESSE                                           
           10 HE10-ADRESSE         PIC X(7).                                    
      *                       NFA                                               
           10 HE10-NFA             PIC X(7).                                    
      *                       NLIEURET                                          
           10 HE10-NLIEURET        PIC X(3).                                    
      *                       PDOSNASC                                          
           10 HE10-PDOSNASC        PIC X(3).                                    
      *                       DOSNASC                                           
           10 HE10-DOSNASC         PIC X(9).                                    
      *                       CTYINASC                                          
           10 HE10-CTYINASC        PIC X(5).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 42      *        
      ******************************************************************        
                                                                                
