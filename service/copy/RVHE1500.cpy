      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHE1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE1500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHE1500.                                                            
           02  HE15-CLIEUHET                                                    
               PIC X(0005).                                                     
           02  HE15-NLIEUHED                                                    
               PIC X(0003).                                                     
           02  HE15-NGHE                                                        
               PIC X(0007).                                                     
           02  HE15-NSEQ                                                        
               PIC X(0003).                                                     
           02  HE15-DMVT                                                        
               PIC X(0008).                                                     
           02  HE15-NCODICAV                                                    
               PIC X(0007).                                                     
           02  HE15-NCODICAP                                                    
               PIC X(0007).                                                     
           02  HE15-CTYPMVTAV                                                   
               PIC X(0014).                                                     
           02  HE15-CTYPMVTAP                                                   
               PIC X(0014).                                                     
           02  HE15-CTRAITAV                                                    
               PIC X(0005).                                                     
           02  HE15-CTRAITAP                                                    
               PIC X(0005).                                                     
           02  HE15-NTYOPEAV                                                    
               PIC X(0003).                                                     
           02  HE15-NTYOPEAP                                                    
               PIC X(0003).                                                     
           02  HE15-CLIEUHETAP                                                  
               PIC X(0005).                                                     
           02  HE15-WSOLDEAV                                                    
               PIC X(0001).                                                     
           02  HE15-WSOLDEAP                                                    
               PIC X(0001).                                                     
           02  HE15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHE1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CLIEUHET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CLIEUHET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NLIEUHED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NLIEUHED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NGHE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NGHE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NCODICAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NCODICAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NCODICAP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NCODICAP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CTYPMVTAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CTYPMVTAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CTYPMVTAP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CTYPMVTAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CTRAITAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CTRAITAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CTRAITAP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CTRAITAP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NTYOPEAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NTYOPEAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-NTYOPEAP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-NTYOPEAP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-CLIEUHETAP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-CLIEUHETAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-WSOLDEAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-WSOLDEAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-WSOLDEAP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-WSOLDEAP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
