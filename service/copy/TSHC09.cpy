      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE RTHC09                                       *            
      *       TR : HC00  GESTION CENTRE HS                         *            
      *       PG : THC09 TRANSFERTS DE PRODUITS VERS 020           *            
      **************************************************************            
      *                                                                         
      *------------------------------ LONGUEUR                                  
       01  TS-LONG              PIC S9(4) COMP-3 VALUE 1728.                    
       01  TS-DONNEES.                                                          
      *------------------------------                                           
           05 TS-MAP.                                                           
             07 TS-OCCURS       OCCURS 12.                                      
      *------------------------------ NUMERO DE HS                              
              10 TS-NCHS        PIC X(07).                                      
      *------------------------------ CODIC                                     
              10 TS-NCODIC      PIC X(07).                                      
      *------------------------------ FAMILLE                                   
              10 TS-CFAM        PIC X(05).                                      
      *------------------------------ MARQUE                                    
              10 TS-CMARQ       PIC X(05).                                      
      *------------------------------ REFERENCE                                 
              10 TS-LREF        PIC X(20).                                      
      *------------------------------ NUMERO ENVOI                              
              10 TS-NENVOI      PIC X(06).                                      
      *------------------------------ CODE ORIGINE                              
              10 TS-CORIGINE    PIC X(07).                                      
      *------------------------------ DATE DE SOLDE                             
              10 TS-DSOLDE      PIC X(08).                                      
      *------------------------------ DATE DE DEPOT                             
              10 TS-DDEPOT      PIC X(08).                                      
      *------------------------------ PROVENANCE ORIGINE                        
              10 TS-CPROVORIG   PIC X(01).                                      
      *------------------------------ NUMERO DE SERIE                           
              10 TS-CSERIE      PIC X(16).                                      
      *------------------------------ DATE D ACCORD FOURNISSEUR                 
              10 TS-DACCFOUR    PIC X(08).                                      
      *------------------------------ NUMERO D'ACCORD                           
              10 TS-NACCORD     PIC X(12).                                      
      *------------------------------ INTERLOCUTEUR                             
              10 TS-INTERLOC    PIC X(10).                                      
      *------------------------------ CLIENT                                    
              10 TS-CLIENT      PIC X(20).                                      
      *------------------------------ NUMERO DE SAV                             
              10 TS-NSAV        PIC X(03).                                      
      *------------------------------ ANOMALIE MVT DE STOCK                     
              10 TS-TOPANO      PIC X(01).                                      
                                                                                
