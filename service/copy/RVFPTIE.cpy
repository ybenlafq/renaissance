      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FPTIE ASSOCIATION CENTRES DE GESTION   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFPTIE .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFPTIE .                                                            
      *}                                                                        
           05  FPTIE-CTABLEG2    PIC X(15).                                     
           05  FPTIE-CTABLEG2-REDEF REDEFINES FPTIE-CTABLEG2.                   
               10  FPTIE-CUGS            PIC X(03).                             
           05  FPTIE-WTABLEG     PIC X(80).                                     
           05  FPTIE-WTABLEG-REDEF  REDEFINES FPTIE-WTABLEG.                    
               10  FPTIE-WACTIF          PIC X(01).                             
               10  FPTIE-NTIERS          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFPTIE-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFPTIE-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FPTIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FPTIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FPTIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FPTIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
