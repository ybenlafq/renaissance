      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-PC10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-PC10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-PC10-ENTETE.                                         00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION  .                               00000540
                    10 COMM-CODE-FONCTION      OCCURS 3.                   00000
                       15  COMM-CFONC     PIC  X(03).                           
                 05 COMM-CFONCTION-OPTION   PIC X(03).                          
                 05 COMM-COPTION            PIC X(02).                          
      *    02 COMM-PC10-APPLI.                                          00000500
           02 COMM-PC10-FILLER              PIC X(3854).                00000620
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3855 00000020
                                                                        00000390
      *-- PARAMETRAGE DES FORMATS PHOTOS                                00000390
           02 COMM-PC11-APPLI REDEFINES COMM-PC10-FILLER.               00000040
              03 COMM-11.                                               00000550
                 05 COMM-11-MODE-INTERO     PIC X(01).                          
                     88  11-MODE-INTERO   VALUE '1'.                            
                 05 COMM-11-JOUR-1          PIC X(08).                  00000130
               04 COMM-11-DATA.                                         00000550
                 05 COMM-11-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-11-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-11-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-11-VAL-FORMAT.                                 00000090
                    10 COMM-11-CPREST-OLD      PIC X(01).                  00000
                    10 COMM-11-CFORMAT         PIC X(05).               00000100
                    10 COMM-11-CCEWE           PIC X(04).               00000110
                    10 COMM-11-LFORMAT         PIC X(20).               00000120
                    10 COMM-11-CPHOTO          PIC X(01).               00000130
                    10 COMM-11-PRESTAP         PIC X(05).               00000130
                    10 COMM-11-PRESTAG         PIC X(05).               00000130
                    10 COMM-11-DDEB            PIC X(08).               00000130
                    10 COMM-11-DFIN            PIC X(08).               00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-11-TYPE-TRT             PIC X .                       
                     88 11-CONSULT-ECRAN     VALUE ' '.                         
                     88 11-CREATION-FORMAT   VALUE '1'.                         
                     88 11-MAJ-FORMAT        VALUE '2'.                         
                 05  COMM-11-MAJ-EN-COURS      PIC X(01).                       
                     88 11-MAJ-EN-COURS     VALUE '1'.                          
                 05  COMM-11-CONFIRM-SUP       PIC X(01).                       
                     88 11-CONFIRM-SUP      VALUE '1'.                          
                 05  COMM-11-DEMANDE-CONFIRM   PIC X(01).                       
                     88 11-DEMANDE-CONFIRM  VALUE '1'.                          
                 05  COMM-12-FILLER            PIC X(80).               00000550
                                                                        00000550
              03 COMM-12.                                               00000550
                 05 COMM-12-MODE-INTERO     PIC X(01).                          
                     88  12-MODE-INTERO   VALUE '1'.                            
               04 COMM-12-DATA.                                         00000550
                 05 COMM-12-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-12-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-12-VAL-ENTETE.                                 00000090
                    10 COMM-12-CFORMAT         PIC X(05).               00000100
                    10 COMM-12-CCEWE           PIC X(04).               00000100
                    10 COMM-12-LFORMAT         PIC X(20).               00000100
                 05 COMM-12-VAL-LIGNE.                                  00000090
                    10 COMM-12-CPREST-OLD            PIC X(01).            00000
                    10 COMM-12-CPREST-NW             PIC X(05).            00000
                    10 COMM-12-LPREST-NW             PIC X(20).            00000
                    10 COMM-12-DDEB                  PIC X(08).            00000
                    10 COMM-12-DFIN                  PIC X(08).            00000
                 05 COMM-12-IND-TS                PIC 9(04).            00000130
      *--  GESTION MODE INTEROGATION / CONSULTATION                     00000390
                 05  COMM-12-TYPE-TRT             PIC X .                       
                     88 12-CONSULT-ECRAN     VALUE ' '.                         
                     88 12-CREATION-CPRESTA  VALUE '1'.                         
                     88 12-MAJ-CPRESTA       VALUE '2'.                         
                 05  COMM-12-MAJ-EN-COURS      PIC X(01).                       
                     88 12-MAJ-EN-COURS     VALUE '1'.                          
                 05  COMM-12-CONFIRM-SUP       PIC X(01).                       
                     88 12-CONFIRM-SUP      VALUE '1'.                          
                 05  COMM-12-DEMANDE-CONFIRM   PIC X(01).                       
                     88 12-DEMANDE-CONFIRM  VALUE '1'.                          
              03 COMM-12-FILLER PIC X(3400).                            00000550
      *-- CONSULTATION DES CARTES                                       00000390
           02 COMM-PC13-APPLI REDEFINES COMM-PC10-FILLER.               00000040
              03 COMM-13-LEVEL-MAX       PIC X(06).                             
              03 COMM-13.                                               00000050
                 05 COMM-13-DEM-CHARGEMENT  PIC X(01).                          
                     88 13-DEMANDE-CHARGEMENT VALUE '1'.                        
                 05 COMM-13-MODE-INTERO     PIC X(01).                          
                     88 13-MODE-INTERO        VALUE '1'.                        
                 05  COMM-13-DEMANDE-MONTANT PIC X(01).                         
                     88 13-DEMANDE-MONTANT    VALUE '1'.                        
                 05  COMM-13-CDEV            PIC X(01).                         
                     88 13-CDEV-978-EURO      VALUE '1'.                        
                 05 COMM-13-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-13-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-13-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-13-VAL-EDITION.                                00000090
                    10 COMM-13-RANDOMNUM       PIC X(16).               00000   
                    10 COMM-13-SERIALNUM       PIC X(13).               00000   
                    10 COMM-13-PRTYP           PIC X(20).               00000   
                    10 COMM-13-STATUT          PIC X(05).               00000   
                    10 COMM-13-COMMENT1        PIC X(52).               00000110
      *             10 COMM-13-COMMENT2        PIC X(40).               00000110
                    10 COMM-13-DATA-CARTE.                                      
                      13 COMM-13-LIGNE-CARTE OCCURS 10.                         
                        15 COMM-13-OPTYPE      PIC X(10).                       
                        15 COMM-13-LMVT        PIC X(20).                       
                        15 COMM-13-ETAT        PIC X(02).                       
                        15 COMM-13-DATEMVT     PIC X(08).                       
                        15 COMM-13-NSOC        PIC X(03).                       
                        15 COMM-13-NLIEU       PIC X(03).                       
                        15 COMM-13-NVENTE      PIC X(08).                       
                        15 COMM-13-MNTDARTY    PIC X(9).                        
                        15 COMM-13-SOLDDAR     PIC X(9).                        
                        15 COMM-13-SOLDEUR     PIC X(9).                        
                        15 COMM-13-SOLDPAR     PIC X(9).                        
                    10 COMM-14.                                                 
                       15 COMM-14-DATA-CARTE.                                   
                          20 COMM-14-OPTYPE      PIC X(10).                     
                          20 COMM-14-LMVT        PIC X(20).                     
                          20 COMM-14-ETAT        PIC X(02).                     
                          20 COMM-14-DATEMVT     PIC X(10).                     
                          20 COMM-14-NSOC        PIC X(03).                     
                          20 COMM-14-NLIEU       PIC X(03).                     
                          20 COMM-14-NVENTE      PIC X(08).                     
                          20 COMM-14-MNTDARTY    PIC X(9).                      
                          20 COMM-14-MNTEUROS    PIC X(9).                      
                          20 COMM-14-SOLDDAR     PIC X(9).                      
                          20 COMM-14-SOLDEUR     PIC X(9).                      
                          20 COMM-14-SOLDPAR     PIC X(9).                      
                          20 COMM-14-CDEV        PIC X(04).                     
                          20 COMM-14-ADDARTY     PIC X(10).                     
                          20 COMM-14-AHDARTY     PIC X(8).                      
                          20 COMM-14-ANSOCVTE    PIC X(3).                      
                          20 COMM-14-ANLIEUVTE   PIC X(3).                      
                          20 COMM-14-ANVENTE     PIC X(7).                      
                          20 COMM-14-AMNTDARTY   PIC ----9,99                   
                                         BLANK WHEN ZERO.                       
                          20 COMM-14-AMNTEUROS   PIC ----9,99                   
                                         BLANK WHEN ZERO.                       
                          20 COMM-14-STATUT      PIC X(05).                     
                          20 COMM-14-DVALIDR     PIC X(10).                     
                          20 COMM-14-DVALIDF     PIC X(10).                     
                          20 COMM-14-LPRESTATION PIC X(20).                     
                          20 COMM-14-CMAP        PIC ----9,99                   
                                         BLANK WHEN ZERO.                       
                          20 COMM-14-CANAL        PIC X(05).                    
                          20 COMM-14-CUTIL        PIC X(20).                    
                          20 COMM-14-NACCORD      PIC X(10).                    
                          20 COMM-14-DDARTY       PIC X(10).                    
                          20 COMM-14-HDARTY       PIC X(06).                    
                          20 COMM-14-DACCORD      PIC X(10).                    
                          20 COMM-14-HACCORD      PIC X(06).                    
                          20 COMM-14-NTRANS       PIC X(10).                    
                          20 COMM-14-NCAISSE      PIC X(03).                    
                05 COMM-13-LETTRAGE             PIC X(10).                      
              03 COMM-13-FILLER PIC X(2471).                            00000550
                                                                        00000550
      *-- EDITION DES ANOMALIES CARTES                                  00000390
           02 COMM-PC15-APPLI REDEFINES COMM-PC10-FILLER.               00000040
              03 COMM-15.                                               00000050
                 05 COMM-15-DEM-CHARGEMENT  PIC X(01).                          
                     88     15-DEMANDE-CHARGEMENT VALUE '1'.                    
                 05 COMM-15-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-15-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-15-VAL-EDITION.                                00000090
                    10 COMM-15-DATDEB          PIC X(10).               00000   
                    10 COMM-15-DATFIN          PIC X(10).               00000   
                    10 COMM-15-NSOC            PIC X(03).               00000110
      *--                                                               00000390
                 05  COMM-15-DEMANDE-CONFIRM PIC X(01).                         
                     88 15-DEMANDE-CONFIRM       VALUE '1'.                     
      *--            VALIDATION EDITION DE LA TOTALIT� DES TYPES DE CA  00000390
                 05  COMM-15-DEMAND-EDIT     PIC X(01).                         
                     88 15-DEMANDE-EDIT VALUE '1'.                              
                     88 15-EDITION-TOUT VALUE '2'.                              
                 05 COMM-15-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-15-MODE-INTERO     PIC X(01).                          
                     88     15-MODE-INTERO    VALUE '1'.                        
                 05 COMM-15-ANOMALIE        PIC  X(01) .                00000080
                 05 COMM-15-FILLER PIC X(96).                           00000550
              03 COMM-15-FILLER PIC X(3720).                            00000550
                                                                        00000550
      *-- EDITION DES RAPPROCHEMENT GCT/CARTE                           00000390
           02 COMM-PC16-APPLI REDEFINES COMM-PC10-FILLER.               00000040
              03 COMM-16.                                               00000050
                 05 COMM-16-DEM-CHARGEMENT  PIC X(01).                          
                     88     16-DEMANDE-CHARGEMENT VALUE '1'.                    
                 05 COMM-16-INDTS           PIC S9(5) COMP-3.           00000070
                 05 COMM-16-INDMAX          PIC S9(5) COMP-3.           00000080
                 05 COMM-16-VAL-EDITION.                                00000090
                    10 COMM-16-DARRETE   .                              00000100
                       15 COMM-16-SSAA            PIC X(04).               00000
                       15 COMM-16-MM              PIC X(02).               00000
                    10 COMM-16-TYPEDIT         PIC X(01).               00000110
      *--                                                               00000390
                 05  COMM-16-DEMANDE-CONFIRM PIC X(01).                         
                     88 16-DEMANDE-CONFIRM       VALUE '1'.                     
      *--            VALIDATION EDITION DE LA TOTALIT� DES TYPES DE CA  00000390
                 05  COMM-16-DEMAND-EDIT     PIC X(01).                         
                     88 16-DEMANDE-EDIT VALUE '1'.                              
                     88 16-EDITION-TOUT VALUE '2'.                              
                 05 COMM-16-NLIGNE          PIC S9(5) COMP-3.           00000070
                 05 COMM-16-MODE-INTERO     PIC X(01).                          
                     88     16-MODE-INTERO    VALUE '1'.                        
                 05 COMM-16-DCTRL     .                                         
                    15 COMM-16-SSAA-CTRL       PIC X(04).                       
                    15 COMM-16-MM-CTRL         PIC X(02).                       
                    15 COMM-16-JJ-CTRL         PIC X(02).                       
                 05 COMM-16-FILLER PIC X(89).                           00000550
              03 COMM-16-FILLER PIC X(3724).                            00000550
                                                                                
                                                                        00000030
