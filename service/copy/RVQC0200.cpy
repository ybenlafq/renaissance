      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0200.                                                            
           02  QC02-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC02-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC02-NCARTE                                                      
               PIC X(0007).                                                     
           02  QC02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  QC02-NLIEU                                                       
               PIC X(0003).                                                     
           02  QC02-DVENTE                                                      
               PIC X(0008).                                                     
           02  QC02-HVENTE                                                      
               PIC X(0002).                                                     
           02  QC02-CFAM                                                        
               PIC X(0005).                                                     
           02  QC02-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  QC02-CCAISSE                                                     
               PIC X(0006).                                                     
           02  QC02-DSAISIE                                                     
               PIC X(0008).                                                     
           02  QC02-DCREATION                                                   
               PIC X(0008).                                                     
           02  QC02-DDESCENTE                                                   
               PIC X(0008).                                                     
           02  QC02-FREPONSE                                                    
               PIC X(0040).                                                     
           02  QC02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-NCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-NCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-HVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-HVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-CCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-CCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-DDESCENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-DDESCENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-FREPONSE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-FREPONSE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
