      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE41   EHE41                                              00000020
      ***************************************************************** 00000030
       01   EHE41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEHETI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEHETI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHEIL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCLIEHEIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHEIF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCLIEHEII      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHEIL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLLIEHEIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHEIF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEHEII      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTIERSI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNOMI    PIC X(20).                                      00000450
           02 TABLEI OCCURS   12 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSELI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLHEDL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNLHEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLHEDF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNLHEDI      PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNGHEL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MNGHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNGHEF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNGHEI  PIC X(7).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFAMI  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMARQI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREFI  PIC X(20).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(78).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EHE41   EHE41                                              00000960
      ***************************************************************** 00000970
       01   EHE41O REDEFINES EHE41I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNUMPAGEA      PIC X.                                     00001150
           02 MNUMPAGEC PIC X.                                          00001160
           02 MNUMPAGEP PIC X.                                          00001170
           02 MNUMPAGEH PIC X.                                          00001180
           02 MNUMPAGEV PIC X.                                          00001190
           02 MNUMPAGEO      PIC X(2).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC X(2).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MCLIEHETA      PIC X.                                     00001290
           02 MCLIEHETC PIC X.                                          00001300
           02 MCLIEHETP PIC X.                                          00001310
           02 MCLIEHETH PIC X.                                          00001320
           02 MCLIEHETV PIC X.                                          00001330
           02 MCLIEHETO      PIC X(5).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLLIEHETA      PIC X.                                     00001360
           02 MLLIEHETC PIC X.                                          00001370
           02 MLLIEHETP PIC X.                                          00001380
           02 MLLIEHETH PIC X.                                          00001390
           02 MLLIEHETV PIC X.                                          00001400
           02 MLLIEHETO      PIC X(20).                                 00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCLIEHEIA      PIC X.                                     00001430
           02 MCLIEHEIC PIC X.                                          00001440
           02 MCLIEHEIP PIC X.                                          00001450
           02 MCLIEHEIH PIC X.                                          00001460
           02 MCLIEHEIV PIC X.                                          00001470
           02 MCLIEHEIO      PIC X(5).                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLLIEHEIA      PIC X.                                     00001500
           02 MLLIEHEIC PIC X.                                          00001510
           02 MLLIEHEIP PIC X.                                          00001520
           02 MLLIEHEIH PIC X.                                          00001530
           02 MLLIEHEIV PIC X.                                          00001540
           02 MLLIEHEIO      PIC X(20).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCTIERSA  PIC X.                                          00001570
           02 MCTIERSC  PIC X.                                          00001580
           02 MCTIERSP  PIC X.                                          00001590
           02 MCTIERSH  PIC X.                                          00001600
           02 MCTIERSV  PIC X.                                          00001610
           02 MCTIERSO  PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLNOMA    PIC X.                                          00001640
           02 MLNOMC    PIC X.                                          00001650
           02 MLNOMP    PIC X.                                          00001660
           02 MLNOMH    PIC X.                                          00001670
           02 MLNOMV    PIC X.                                          00001680
           02 MLNOMO    PIC X(20).                                      00001690
           02 TABLEO OCCURS   12 TIMES .                                00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MSELA   PIC X.                                          00001720
             03 MSELC   PIC X.                                          00001730
             03 MSELP   PIC X.                                          00001740
             03 MSELH   PIC X.                                          00001750
             03 MSELV   PIC X.                                          00001760
             03 MSELO   PIC X.                                          00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MNLHEDA      PIC X.                                     00001790
             03 MNLHEDC PIC X.                                          00001800
             03 MNLHEDP PIC X.                                          00001810
             03 MNLHEDH PIC X.                                          00001820
             03 MNLHEDV PIC X.                                          00001830
             03 MNLHEDO      PIC X(3).                                  00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MNGHEA  PIC X.                                          00001860
             03 MNGHEC  PIC X.                                          00001870
             03 MNGHEP  PIC X.                                          00001880
             03 MNGHEH  PIC X.                                          00001890
             03 MNGHEV  PIC X.                                          00001900
             03 MNGHEO  PIC X(7).                                       00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MNCODICA     PIC X.                                     00001930
             03 MNCODICC     PIC X.                                     00001940
             03 MNCODICP     PIC X.                                     00001950
             03 MNCODICH     PIC X.                                     00001960
             03 MNCODICV     PIC X.                                     00001970
             03 MNCODICO     PIC X(7).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MCFAMA  PIC X.                                          00002000
             03 MCFAMC  PIC X.                                          00002010
             03 MCFAMP  PIC X.                                          00002020
             03 MCFAMH  PIC X.                                          00002030
             03 MCFAMV  PIC X.                                          00002040
             03 MCFAMO  PIC X(5).                                       00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MCMARQA      PIC X.                                     00002070
             03 MCMARQC PIC X.                                          00002080
             03 MCMARQP PIC X.                                          00002090
             03 MCMARQH PIC X.                                          00002100
             03 MCMARQV PIC X.                                          00002110
             03 MCMARQO      PIC X(5).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MLREFA  PIC X.                                          00002140
             03 MLREFC  PIC X.                                          00002150
             03 MLREFP  PIC X.                                          00002160
             03 MLREFH  PIC X.                                          00002170
             03 MLREFV  PIC X.                                          00002180
             03 MLREFO  PIC X(20).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(78).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
