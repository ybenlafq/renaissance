      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE SP2000                       
      ******************************************************************        
      *                                                                         
       CLEF-SP2000             SECTION.                                         
      *                                                                         
           MOVE 'RVSP2000          '       TO   TABLE-NAME.                     
           MOVE 'SP2000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-SP2000. EXIT.                                                   
                EJECT                                                           
                                                                                
