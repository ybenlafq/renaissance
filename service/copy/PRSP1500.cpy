      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE SP1500                       
      ******************************************************************        
      *                                                                         
       CLEF-SP1500             SECTION.                                         
      *                                                                         
           MOVE 'RVSP1500          '       TO   TABLE-NAME.                     
           MOVE 'SP1500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-SP1500. EXIT.                                                   
                EJECT                                                           
                                                                                
