      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IQC003 AU 12/11/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IQC003.                                                        
            05 NOMETAT-IQC003           PIC X(6) VALUE 'IQC003'.                
            05 RUPTURES-IQC003.                                                 
           10 IQC003-NCISOC             PIC X(03).                      007  003
           10 IQC003-CTCARTE            PIC X(01).                      010  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IQC003-SEQUENCE           PIC S9(04) COMP.                011  002
      *--                                                                       
           10 IQC003-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IQC003.                                                   
           10 IQC003-JOURJ              PIC X(05).                      013  005
           10 IQC003-JOURJ-1            PIC X(05).                      018  005
           10 IQC003-JOURJ-2            PIC X(05).                      023  005
           10 IQC003-JOURJ-3            PIC X(05).                      028  005
           10 IQC003-JOURJ-4            PIC X(05).                      033  005
           10 IQC003-JOURJ-5            PIC X(05).                      038  005
           10 IQC003-JOURJ-6            PIC X(05).                      043  005
           10 IQC003-CPTJ               PIC S9(05)      COMP-3.         048  003
           10 IQC003-CPTJ-1             PIC S9(05)      COMP-3.         051  003
           10 IQC003-CPTJ-2             PIC S9(05)      COMP-3.         054  003
           10 IQC003-CPTJ-3             PIC S9(05)      COMP-3.         057  003
           10 IQC003-CPTJ-4             PIC S9(05)      COMP-3.         060  003
           10 IQC003-CPTJ-5             PIC S9(05)      COMP-3.         063  003
           10 IQC003-CPTJ-6             PIC S9(05)      COMP-3.         066  003
           10 IQC003-SOMME-MOISJ        PIC S9(06)      COMP-3.         069  004
           10 IQC003-SOMME-MOISJ-1      PIC S9(06)      COMP-3.         073  004
            05 FILLER                      PIC X(436).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IQC003-LONG           PIC S9(4)   COMP  VALUE +076.           
      *                                                                         
      *--                                                                       
        01  DSECT-IQC003-LONG           PIC S9(4) COMP-5  VALUE +076.           
                                                                                
      *}                                                                        
