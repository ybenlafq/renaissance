      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSBC70 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSBC70-IDENTIFICATEUR.                                       00000050
           05  TSBC70-TRANSID               PIC X(04) VALUE 'BC70'.     00000060
           05  TSBC70-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSBC70-ITEM.                                                 00000090
           05  TSBC70-DATAS.                                            00000110
               10  TSBC70-LIGNE             OCCURS 11.                  00000120
                  15  TSBC70-DONNES-VENTE.                              00000130
                   20 TSBC70-VENTE             PIC X.                           
                      88 TSBC70-VENTE-ARCHIVE           VALUE 'A'.              
                      88 TSBC70-VENTE-ENCOURS           VALUE 'E'.              
                      88 TSBC70-VENTE-INEXIST           VALUE 'I'.              
                   20  TSBC70-NSOCIETE         PIC X(03).               00000130
                   20  TSBC70-NLIEU            PIC X(03).               00000130
                   20  TSBC70-NVENTE           PIC X(07).               00000140
                   20  TSBC70-DVENTE           PIC X(08).               00000140
                   20  TSBC70-NCLUSTER         PIC X(13).               00000140
                   20  TSBC70-WEXPTAXE         PIC X(01).               00000140
                   20  TSBC70-LLIEU            PIC X(20).               00000140
JC0806             20  TSBC70-CFAM             PIC X(05).               00000140
JC0806             20  TSBC70-CMARQ            PIC X(05).               00000140
JC0107             20  TSBC70-NFLAG            PIC X(01).               00000140
                  15  TSBC70-DONNES-CLIENT.                             00000150
                   20  TSBC70-LNOM             PIC X(25).               00000150
                   20  TSBC70-NTEL             PIC X(10).               00000170
                   20  TSBC70-LPRENOM          PIC X(15).               00160   
                   20  TSBC70-CPOSTAL          PIC X(05).               00180   
                   20  TSBC70-LCOMMUNE         PIC X(32).               00190   
                   20  TSBC70-WTYPEADR         PIC X(1).                00190   
                   20  TSBC70-TYPVTE           PIC X(01).               00190   
                   20  TSBC70-IDCLIENT         PIC X(08).               00190   
                   20  TSBC70-TCLIENT          PIC X(01).               00190   
                   20  TSBC70-FILLEUR          PIC X(10).               00190   
               10  TSBC70-NUM-LIGNE         PIC 9(2).                   00190   
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
