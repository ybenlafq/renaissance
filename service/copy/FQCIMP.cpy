      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *   COPY DU FICHIER D'IMPRESSION DES CARTES T                             
       FD        FQCIMP     LABEL RECORD STANDARD   RECORDING F.                
       01        FQCIMP-ENR.                                                    
          3      FQCIMP-CENTITE              PIC X(05).                         
          3      FQCIMP-CODBARRE.                                               
              5  FQCIMP-NCISOC               PIC X(03).                         
              5  FQCIMP-CTCARTE              PIC X(01).                         
              5  FQCIMP-NCARTE               PIC X(07).                         
          3      FQCIMP-LIGNE1               PIC X(44).                         
          3      FQCIMP-LIGNE2               PIC X(44).                         
          3      FQCIMP-ADRES1               PIC X(32).                         
          3      FQCIMP-ADRES2               PIC X(32).                         
          3      FQCIMP-ADRES3               PIC X(32).                         
          3      FQCIMP-ADRES4               PIC X(32).                         
          3      FQCIMP-LFAM                 PIC X(20).                         
          3      FQCIMP-LLIEUVENTE           PIC X(20).                         
                                                                                
