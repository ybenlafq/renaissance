      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(PNMD.RVCM0000)                                    *        
      *        LIBRARY(DSA001.DEVL.SOURCE(RVCM0000))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        APOST                                                   *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVCM0000                      *        
      ******************************************************************        
       01  RVCM0000.                                                            
           10 CM00-NSOCIETE             PIC X(3).                               
           10 CM00-NLIEU                PIC X(3).                               
           10 CM00-DATEJOUR             PIC X(8).                               
           10 CM00-HOUV                 PIC X(4).                               
           10 CM00-HFER                 PIC X(4).                               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVCM0000-FLAG.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CM00-NSOCIETE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 CM00-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CM00-NLIEU-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 CM00-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CM00-DATE-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 CM00-DATE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CM00-HOUV-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 CM00-HOUV-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CM00-HFER-F               PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 CM00-HFER-F               PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
