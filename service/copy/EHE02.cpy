      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE02   EHE02                                              00000020
      ***************************************************************** 00000030
       01   EHE02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTIERSF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNTIERSI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLTIERSI  PIC X(20).                                      00000290
           02 MLIGNEI OCCURS   12 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MMRQI   PIC X(5).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MFAMI   PIC X(5).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCODI   PIC X(7).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRIL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MTRIL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTRIF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MTRII   PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRDUL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MRDUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MRDUF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MRDUI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MWACL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWACF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWACI   PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSRL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MWSRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWSRF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWSRI   PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(78).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUPPRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLSUPPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSUPPRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLSUPPRI  PIC X(60).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EHE02   EHE02                                              00000840
      ***************************************************************** 00000850
       01   EHE02O REDEFINES EHE02I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNUMPAGEA      PIC X.                                     00001030
           02 MNUMPAGEC PIC X.                                          00001040
           02 MNUMPAGEP PIC X.                                          00001050
           02 MNUMPAGEH PIC X.                                          00001060
           02 MNUMPAGEV PIC X.                                          00001070
           02 MNUMPAGEO      PIC 999.                                   00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MPAGEMAXA      PIC X.                                     00001100
           02 MPAGEMAXC PIC X.                                          00001110
           02 MPAGEMAXP PIC X.                                          00001120
           02 MPAGEMAXH PIC X.                                          00001130
           02 MPAGEMAXV PIC X.                                          00001140
           02 MPAGEMAXO      PIC 999.                                   00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNTIERSA  PIC X.                                          00001170
           02 MNTIERSC  PIC X.                                          00001180
           02 MNTIERSP  PIC X.                                          00001190
           02 MNTIERSH  PIC X.                                          00001200
           02 MNTIERSV  PIC X.                                          00001210
           02 MNTIERSO  PIC X(5).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLTIERSA  PIC X.                                          00001240
           02 MLTIERSC  PIC X.                                          00001250
           02 MLTIERSP  PIC X.                                          00001260
           02 MLTIERSH  PIC X.                                          00001270
           02 MLTIERSV  PIC X.                                          00001280
           02 MLTIERSO  PIC X(20).                                      00001290
           02 MLIGNEO OCCURS   12 TIMES .                               00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MMRQA   PIC X.                                          00001320
             03 MMRQC   PIC X.                                          00001330
             03 MMRQP   PIC X.                                          00001340
             03 MMRQH   PIC X.                                          00001350
             03 MMRQV   PIC X.                                          00001360
             03 MMRQO   PIC X(5).                                       00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MFAMA   PIC X.                                          00001390
             03 MFAMC   PIC X.                                          00001400
             03 MFAMP   PIC X.                                          00001410
             03 MFAMH   PIC X.                                          00001420
             03 MFAMV   PIC X.                                          00001430
             03 MFAMO   PIC X(5).                                       00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MCODA   PIC X.                                          00001460
             03 MCODC   PIC X.                                          00001470
             03 MCODP   PIC X.                                          00001480
             03 MCODH   PIC X.                                          00001490
             03 MCODV   PIC X.                                          00001500
             03 MCODO   PIC X(7).                                       00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MTRIA   PIC X.                                          00001530
             03 MTRIC   PIC X.                                          00001540
             03 MTRIP   PIC X.                                          00001550
             03 MTRIH   PIC X.                                          00001560
             03 MTRIV   PIC X.                                          00001570
             03 MTRIO   PIC X.                                          00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MRDUA   PIC X.                                          00001600
             03 MRDUC   PIC X.                                          00001610
             03 MRDUP   PIC X.                                          00001620
             03 MRDUH   PIC X.                                          00001630
             03 MRDUV   PIC X.                                          00001640
             03 MRDUO   PIC X(5).                                       00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MWACA   PIC X.                                          00001670
             03 MWACC   PIC X.                                          00001680
             03 MWACP   PIC X.                                          00001690
             03 MWACH   PIC X.                                          00001700
             03 MWACV   PIC X.                                          00001710
             03 MWACO   PIC X.                                          00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MWSRA   PIC X.                                          00001740
             03 MWSRC   PIC X.                                          00001750
             03 MWSRP   PIC X.                                          00001760
             03 MWSRH   PIC X.                                          00001770
             03 MWSRV   PIC X.                                          00001780
             03 MWSRO   PIC X.                                          00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLIBERRA  PIC X.                                          00001810
           02 MLIBERRC  PIC X.                                          00001820
           02 MLIBERRP  PIC X.                                          00001830
           02 MLIBERRH  PIC X.                                          00001840
           02 MLIBERRV  PIC X.                                          00001850
           02 MLIBERRO  PIC X(78).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLSUPPRA  PIC X.                                          00001880
           02 MLSUPPRC  PIC X.                                          00001890
           02 MLSUPPRP  PIC X.                                          00001900
           02 MLSUPPRH  PIC X.                                          00001910
           02 MLSUPPRV  PIC X.                                          00001920
           02 MLSUPPRO  PIC X(60).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
