      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE17   EHE17                                              00000020
      ***************************************************************** 00000030
       01   EHE17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(2).                                       00000090
           02 MDATJOUI  PIC X(9).                                       00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(2).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000170
           02 FILLER    PIC X(2).                                       00000180
           02 MNLIEUHSI      PIC X(3).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000210
           02 FILLER    PIC X(2).                                       00000220
           02 MNHSI     PIC X(7).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METATECL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 METATECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 METATECF  PIC X.                                          00000250
           02 FILLER    PIC X(2).                                       00000260
           02 METATECI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIVL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNSOCIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCIVF  PIC X.                                          00000290
           02 FILLER    PIC X(2).                                       00000300
           02 MNSOCIVI  PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCIVL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MLSTOCIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCIVF      PIC X.                                     00000330
           02 FILLER    PIC X(2).                                       00000340
           02 MLSTOCIVI      PIC X(3).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCIVL     COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MSLSTOCIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCIVF     PIC X.                                     00000370
           02 FILLER    PIC X(2).                                       00000380
           02 MSLSTOCIVI     PIC X(3).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCECL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNSOCECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCECF  PIC X.                                          00000410
           02 FILLER    PIC X(2).                                       00000420
           02 MNSOCECI  PIC X(3).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTOCECL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLSTOCECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTOCECF      PIC X.                                     00000450
           02 FILLER    PIC X(2).                                       00000460
           02 MLSTOCECI      PIC X(3).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLSTOCECL     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MSLSTOCECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLSTOCECF     PIC X.                                     00000490
           02 FILLER    PIC X(2).                                       00000500
           02 MSLSTOCECI     PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITIVL     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MLTRAITIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITIVF     PIC X.                                     00000530
           02 FILLER    PIC X(2).                                       00000540
           02 MLTRAITIVI     PIC X(5).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITECL     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MLTRAITECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTRAITECF     PIC X.                                     00000570
           02 FILLER    PIC X(2).                                       00000580
           02 MLTRAITECI     PIC X(5).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICIVL     COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MNCODICIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICIVF     PIC X.                                     00000610
           02 FILLER    PIC X(2).                                       00000620
           02 MNCODICIVI     PIC X(7).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICECL     COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MNCODICECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCODICECF     PIC X.                                     00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MNCODICECI     PIC X(7).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MCTRAITI  PIC X(5).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCTRAITL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MLCTRAITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCTRAITF      PIC X.                                     00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MLCTRAITI      PIC X(20).                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUHECL     COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MCLIEUHECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUHECF     PIC X.                                     00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MCLIEUHECI     PIC X(5).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRAITIVL     COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MTTRAITIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTTRAITIVF     PIC X.                                     00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTTRAITIVI     PIC X(5).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTTRAITL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MLTTRAITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTTRAITF      PIC X.                                     00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MLTTRAITI      PIC X(20).                                 00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRAITECL     COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MTTRAITECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTTRAITECF     PIC X.                                     00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTTRAITECI     PIC X(5).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOSERIEL      COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MNOSERIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNOSERIEF      PIC X.                                     00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MNOSERIEI      PIC X(16).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOSERIECL     COMP PIC S9(4).                            00000960
      *--                                                                       
           02 MNOSERIECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNOSERIECF     PIC X.                                     00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNOSERIECI     PIC X(16).                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATUTL      COMP PIC S9(4).                            00001000
      *--                                                                       
           02 MCSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTATUTF      PIC X.                                     00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MCSTATUTI      PIC X(3).                                  00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSTATUTL      COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MLSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLSTATUTF      PIC X.                                     00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MLSTATUTI      PIC X(20).                                 00001070
      * MESSAGE ERREUR                                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLIBERRI  PIC X(78).                                      00001120
      * CODE TRANSACTION                                                00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MZONCMDI  PIC X(15).                                      00001210
      * CICS DE TRAVAIL                                                 00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      * NETNAME                                                         00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNETNAMI  PIC X(8).                                       00001310
      * CODE TERMINAL                                                   00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MSCREENI  PIC X(4).                                       00001360
      ***************************************************************** 00001370
      * SDF: EHE17   EHE17                                              00001380
      ***************************************************************** 00001390
       01   EHE17O REDEFINES EHE17I.                                    00001400
           02 FILLER    PIC X(12).                                      00001410
      * DATE DU JOUR                                                    00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDATJOUA  PIC X.                                          00001440
           02 MDATJOUC  PIC X.                                          00001450
           02 MDATJOUH  PIC X.                                          00001460
           02 MDATJOUO  PIC X(9).                                       00001470
      * HEURE                                                           00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MTIMJOUA  PIC X.                                          00001500
           02 MTIMJOUC  PIC X.                                          00001510
           02 MTIMJOUH  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNLIEUHSA      PIC X.                                     00001550
           02 MNLIEUHSC PIC X.                                          00001560
           02 MNLIEUHSH PIC X.                                          00001570
           02 MNLIEUHSO      PIC X(3).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNHSA     PIC X.                                          00001600
           02 MNHSC     PIC X.                                          00001610
           02 MNHSH     PIC X.                                          00001620
           02 MNHSO     PIC X(7).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 METATECA  PIC X.                                          00001650
           02 METATECC  PIC X.                                          00001660
           02 METATECH  PIC X.                                          00001670
           02 METATECO  PIC X(10).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSOCIVA  PIC X.                                          00001700
           02 MNSOCIVC  PIC X.                                          00001710
           02 MNSOCIVH  PIC X.                                          00001720
           02 MNSOCIVO  PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLSTOCIVA      PIC X.                                     00001750
           02 MLSTOCIVC PIC X.                                          00001760
           02 MLSTOCIVH PIC X.                                          00001770
           02 MLSTOCIVO      PIC X(3).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MSLSTOCIVA     PIC X.                                     00001800
           02 MSLSTOCIVC     PIC X.                                     00001810
           02 MSLSTOCIVH     PIC X.                                     00001820
           02 MSLSTOCIVO     PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNSOCECA  PIC X.                                          00001850
           02 MNSOCECC  PIC X.                                          00001860
           02 MNSOCECH  PIC X.                                          00001870
           02 MNSOCECO  PIC X(3).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLSTOCECA      PIC X.                                     00001900
           02 MLSTOCECC PIC X.                                          00001910
           02 MLSTOCECH PIC X.                                          00001920
           02 MLSTOCECO      PIC X(3).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MSLSTOCECA     PIC X.                                     00001950
           02 MSLSTOCECC     PIC X.                                     00001960
           02 MSLSTOCECH     PIC X.                                     00001970
           02 MSLSTOCECO     PIC X(3).                                  00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLTRAITIVA     PIC X.                                     00002000
           02 MLTRAITIVC     PIC X.                                     00002010
           02 MLTRAITIVH     PIC X.                                     00002020
           02 MLTRAITIVO     PIC X(5).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLTRAITECA     PIC X.                                     00002050
           02 MLTRAITECC     PIC X.                                     00002060
           02 MLTRAITECH     PIC X.                                     00002070
           02 MLTRAITECO     PIC X(5).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNCODICIVA     PIC X.                                     00002100
           02 MNCODICIVC     PIC X.                                     00002110
           02 MNCODICIVH     PIC X.                                     00002120
           02 MNCODICIVO     PIC X(7).                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNCODICECA     PIC X.                                     00002150
           02 MNCODICECC     PIC X.                                     00002160
           02 MNCODICECH     PIC X.                                     00002170
           02 MNCODICECO     PIC X(7).                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCTRAITA  PIC X.                                          00002200
           02 MCTRAITC  PIC X.                                          00002210
           02 MCTRAITH  PIC X.                                          00002220
           02 MCTRAITO  PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLCTRAITA      PIC X.                                     00002250
           02 MLCTRAITC PIC X.                                          00002260
           02 MLCTRAITH PIC X.                                          00002270
           02 MLCTRAITO      PIC X(20).                                 00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCLIEUHECA     PIC X.                                     00002300
           02 MCLIEUHECC     PIC X.                                     00002310
           02 MCLIEUHECH     PIC X.                                     00002320
           02 MCLIEUHECO     PIC X(5).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTTRAITIVA     PIC X.                                     00002350
           02 MTTRAITIVC     PIC X.                                     00002360
           02 MTTRAITIVH     PIC X.                                     00002370
           02 MTTRAITIVO     PIC X(5).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLTTRAITA      PIC X.                                     00002400
           02 MLTTRAITC PIC X.                                          00002410
           02 MLTTRAITH PIC X.                                          00002420
           02 MLTTRAITO      PIC X(20).                                 00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MTTRAITECA     PIC X.                                     00002450
           02 MTTRAITECC     PIC X.                                     00002460
           02 MTTRAITECH     PIC X.                                     00002470
           02 MTTRAITECO     PIC X(5).                                  00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MNOSERIEA      PIC X.                                     00002500
           02 MNOSERIEC PIC X.                                          00002510
           02 MNOSERIEH PIC X.                                          00002520
           02 MNOSERIEO      PIC X(16).                                 00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNOSERIECA     PIC X.                                     00002550
           02 MNOSERIECC     PIC X.                                     00002560
           02 MNOSERIECH     PIC X.                                     00002570
           02 MNOSERIECO     PIC X(16).                                 00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MCSTATUTA      PIC X.                                     00002600
           02 MCSTATUTC PIC X.                                          00002610
           02 MCSTATUTH PIC X.                                          00002620
           02 MCSTATUTO      PIC X(3).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLSTATUTA      PIC X.                                     00002650
           02 MLSTATUTC PIC X.                                          00002660
           02 MLSTATUTH PIC X.                                          00002670
           02 MLSTATUTO      PIC X(20).                                 00002680
      * MESSAGE ERREUR                                                  00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MLIBERRA  PIC X.                                          00002710
           02 MLIBERRC  PIC X.                                          00002720
           02 MLIBERRH  PIC X.                                          00002730
           02 MLIBERRO  PIC X(78).                                      00002740
      * CODE TRANSACTION                                                00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MCODTRAA  PIC X.                                          00002770
           02 MCODTRAC  PIC X.                                          00002780
           02 MCODTRAH  PIC X.                                          00002790
           02 MCODTRAO  PIC X(4).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MZONCMDA  PIC X.                                          00002820
           02 MZONCMDC  PIC X.                                          00002830
           02 MZONCMDH  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
      * CICS DE TRAVAIL                                                 00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MCICSA    PIC X.                                          00002880
           02 MCICSC    PIC X.                                          00002890
           02 MCICSH    PIC X.                                          00002900
           02 MCICSO    PIC X(5).                                       00002910
      * NETNAME                                                         00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNETNAMA  PIC X.                                          00002940
           02 MNETNAMC  PIC X.                                          00002950
           02 MNETNAMH  PIC X.                                          00002960
           02 MNETNAMO  PIC X(8).                                       00002970
      * CODE TERMINAL                                                   00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MSCREENA  PIC X.                                          00003000
           02 MSCREENC  PIC X.                                          00003010
           02 MSCREENH  PIC X.                                          00003020
           02 MSCREENO  PIC X(4).                                       00003030
                                                                                
