      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ENM02                                                      00000020
      ***************************************************************** 00000030
       01   ENM02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
           02 MNDETCPTI OCCURS   14 TIMES .                             00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCPTL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MCCPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCCPTF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCCPTI  PIC X(2).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIBL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MCLIBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCLIBF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCLIBI  PIC X(30).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCRIL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MCCRIL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCCRIF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCCRII  PIC X(10).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAUTL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCAUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAUTF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCAUTI  PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXCL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCEXCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCEXCF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCEXCI  PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSNTL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCSNTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCSNTF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCSNTI  PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEMPL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCEMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCEMPF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCEMPI  PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCHEL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCCHEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCCHEF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCCHEI  PIC X.                                          00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWICL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MWICL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWICF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MWICI   PIC X.                                          00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRCRIL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCTRCRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTRCRIF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCTRCRII     PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: ENM02                                                      00000880
      ***************************************************************** 00000890
       01   ENM02O REDEFINES ENM02I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MWFONCA   PIC X.                                          00001140
           02 MWFONCC   PIC X.                                          00001150
           02 MWFONCP   PIC X.                                          00001160
           02 MWFONCH   PIC X.                                          00001170
           02 MWFONCV   PIC X.                                          00001180
           02 MWFONCO   PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNSOCA    PIC X.                                          00001210
           02 MNSOCC    PIC X.                                          00001220
           02 MNSOCP    PIC X.                                          00001230
           02 MNSOCH    PIC X.                                          00001240
           02 MNSOCV    PIC X.                                          00001250
           02 MNSOCO    PIC X(3).                                       00001260
           02 MNDETCPTO OCCURS   14 TIMES .                             00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MCCPTA  PIC X.                                          00001290
             03 MCCPTC  PIC X.                                          00001300
             03 MCCPTP  PIC X.                                          00001310
             03 MCCPTH  PIC X.                                          00001320
             03 MCCPTV  PIC X.                                          00001330
             03 MCCPTO  PIC X(2).                                       00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MCLIBA  PIC X.                                          00001360
             03 MCLIBC  PIC X.                                          00001370
             03 MCLIBP  PIC X.                                          00001380
             03 MCLIBH  PIC X.                                          00001390
             03 MCLIBV  PIC X.                                          00001400
             03 MCLIBO  PIC X(30).                                      00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MCCRIA  PIC X.                                          00001430
             03 MCCRIC  PIC X.                                          00001440
             03 MCCRIP  PIC X.                                          00001450
             03 MCCRIH  PIC X.                                          00001460
             03 MCCRIV  PIC X.                                          00001470
             03 MCCRIO  PIC X(10).                                      00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MCAUTA  PIC X.                                          00001500
             03 MCAUTC  PIC X.                                          00001510
             03 MCAUTP  PIC X.                                          00001520
             03 MCAUTH  PIC X.                                          00001530
             03 MCAUTV  PIC X.                                          00001540
             03 MCAUTO  PIC X.                                          00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MCEXCA  PIC X.                                          00001570
             03 MCEXCC  PIC X.                                          00001580
             03 MCEXCP  PIC X.                                          00001590
             03 MCEXCH  PIC X.                                          00001600
             03 MCEXCV  PIC X.                                          00001610
             03 MCEXCO  PIC X.                                          00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MCSNTA  PIC X.                                          00001640
             03 MCSNTC  PIC X.                                          00001650
             03 MCSNTP  PIC X.                                          00001660
             03 MCSNTH  PIC X.                                          00001670
             03 MCSNTV  PIC X.                                          00001680
             03 MCSNTO  PIC X.                                          00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MCEMPA  PIC X.                                          00001710
             03 MCEMPC  PIC X.                                          00001720
             03 MCEMPP  PIC X.                                          00001730
             03 MCEMPH  PIC X.                                          00001740
             03 MCEMPV  PIC X.                                          00001750
             03 MCEMPO  PIC X.                                          00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCCHEA  PIC X.                                          00001780
             03 MCCHEC  PIC X.                                          00001790
             03 MCCHEP  PIC X.                                          00001800
             03 MCCHEH  PIC X.                                          00001810
             03 MCCHEV  PIC X.                                          00001820
             03 MCCHEO  PIC X.                                          00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MWICA   PIC X.                                          00001850
             03 MWICC   PIC X.                                          00001860
             03 MWICP   PIC X.                                          00001870
             03 MWICH   PIC X.                                          00001880
             03 MWICV   PIC X.                                          00001890
             03 MWICO   PIC X.                                          00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MCTRCRIA     PIC X.                                     00001920
             03 MCTRCRIC     PIC X.                                     00001930
             03 MCTRCRIP     PIC X.                                     00001940
             03 MCTRCRIH     PIC X.                                     00001950
             03 MCTRCRIV     PIC X.                                     00001960
             03 MCTRCRIO     PIC X.                                     00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(78).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
