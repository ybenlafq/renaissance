      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTRB02                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0200.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUM.ENVOI                                         
           10 RB02-NENVOI          PIC X(07).                                   
      *    *************************************************************        
      *                       CENTRE DE TRAITEMENT                              
           10 RB02-CTRAIT          PIC X(05).                                   
      *    *************************************************************        
      *                       SOCIETE EXPEDITRICE INITIALE                      
           10 RB02-NSOCORIG        PIC X(03).                                   
      *    *************************************************************        
      *                       CODE TIERS                                        
           10 RB02-CTIERS          PIC X(05).                                   
      *    *************************************************************        
      *                       DATE ENVOI EF00                                   
           10 RB02-DENVOI          PIC X(08).                                   
      *    *************************************************************        
      *                       DATE ENVOI PROGRAMME BRB110                       
           10 RB02-DEXT            PIC X(08).                                   
      *    *************************************************************        
      *                       DATE DE PREMI�RE RECEPTION FLUX 020               
           10 RB02-DRECEP          PIC X(08).                                   
      *    *************************************************************        
      *                       NOMBRE EQP ENVOYES FLUX 010                       
           10 RB02-NBENV           PIC S9(5) COMP-3.                            
      *    *************************************************************        
      *                       NOMBRE EQP RECUS FLUX 020                         
           10 RB02-NBRECU          PIC S9(5) COMP-3.                            
      *    *************************************************************        
      *                       STATUT DE L'ENVOI                                 
           10 RB02-WOK             PIC X(01).                                   
      *    *************************************************************        
      *                       DATE SYSTEME                                      
           10 RB02-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-NENVOI-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-NENVOI-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-CTRAIT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-CTRAIT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-NSOCORIG-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-NSOCORIG-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-DENVOI-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-DENVOI-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-DEXT-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-DEXT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-DRECEP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-DRECEP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-NBENV-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-NBENV-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-NBRECU-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-NBRECU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-WOK-F           PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-WOK-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB02-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB02-DSYST-F         PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
