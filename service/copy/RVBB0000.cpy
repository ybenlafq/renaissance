      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVBB0000                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBB0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBB0000.                                                            
      *}                                                                        
      *                       IDORGAN                                           
           10 BB00-IDORGAN         PIC X(2).                                    
      *                       LORGAN                                            
           10 BB00-LORGAN          PIC X(30).                                   
      *                       OFFRE                                             
           10 BB00-OFFRE           PIC X(2).                                    
      *                       NCOMMANDE                                         
           10 BB00-NCOMMANDE       PIC X(12).                                   
      *                       DCOMPTA                                           
           10 BB00-DCOMPTA         PIC X(10).                                   
      *                       NDOSSIER                                          
           10 BB00-NDOSSIER        PIC X(12).                                   
      *                       NSEQ                                              
           10 BB00-NSEQ            PIC S9(3)V USAGE COMP-3.                     
      *                       ADR_LIVR1                                         
           10 BB00-ADR-LIVR1       PIC X(38).                                   
      *                       ADR_LIVR2                                         
           10 BB00-ADR-LIVR2       PIC X(38).                                   
      *                       ADR_LIVR3                                         
           10 BB00-ADR-LIVR3       PIC X(38).                                   
      *                       ADR_LIVR4                                         
           10 BB00-ADR-LIVR4       PIC X(38).                                   
      *                       ADR_LIVR5                                         
           10 BB00-ADR-LIVR5       PIC X(38).                                   
      *                       ADR_LIVR6                                         
           10 BB00-ADR-LIVR6       PIC X(38).                                   
      *                       CPOST                                             
           10 BB00-CPOST           PIC X(5).                                    
      *                       CPAYS                                             
           10 BB00-CPAYS           PIC X(2).                                    
      *                       CPRIME                                            
           10 BB00-CPRIME          PIC S9(9)V USAGE COMP-3.                     
      *                       QTE_COMMANDEE                                     
           10 BB00-QTE-COMMANDEE   PIC S9(4)V USAGE COMP-3.                     
      *                       RFOURN                                            
           10 BB00-RFOURN          PIC X(20).                                   
      *                       TCADEAU                                           
           10 BB00-TCADEAU         PIC X(3).                                    
      *                       RCADEAU                                           
           10 BB00-RCADEAU         PIC X(3).                                    
      *                       LCADEAU                                           
           10 BB00-LCADEAU         PIC X(30).                                   
      *                       CCOLIS                                            
           10 BB00-CCOLIS          PIC X(5).                                    
      *                       NB_POINTS                                         
           10 BB00-NB-POINTS       PIC S9(9)V USAGE COMP-3.                     
      *                       MT_PRIME                                          
           10 BB00-MT-PRIME        PIC S9(14)V9(2) USAGE COMP-3.                
      *                       MT_MURCEF                                         
           10 BB00-MT-MURCEF       PIC S9(14)V9(2) USAGE COMP-3.                
      *                       CDEV                                              
           10 BB00-CDEV            PIC X(3).                                    
      *                       NB_DEC                                            
           10 BB00-NB-DEC          PIC S9(4)V USAGE COMP-3.                     
      *                       NADH                                              
           10 BB00-NADH            PIC X(16).                                   
      *                       TEL_DOM                                           
           10 BB00-TEL-DOM         PIC X(10).                                   
      *                       TEL_BUR                                           
           10 BB00-TEL-BUR         PIC X(10).                                   
      *                       TEL_PORT                                          
           10 BB00-TEL-PORT        PIC X(10).                                   
      *                       CANAL_COMMANDE                                    
           10 BB00-CANAL-COMMANDE  PIC X(3).                                    
      *                       ADR_MAIL                                          
           10 BB00-ADR-MAIL        PIC X(100).                                  
      *                       DCREA                                             
           10 BB00-DCREA           PIC X(8).                                    
      *                       CERR                                              
           10 BB00-CERR            PIC X(5).                                    
      *                       DUPD                                              
           10 BB00-DUPD            PIC X(8).                                    
      *                       COMMENT                                           
           10 BB00-COMMENT         PIC X(70).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 37      *        
      ******************************************************************        
                                                                                
