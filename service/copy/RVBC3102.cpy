      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVBC3102                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBC3100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVBC3102.                                                            
           02  BC31-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  BC31-NLIEU                                                       
               PIC X(0003).                                                     
           02  BC31-NVENTE                                                      
               PIC X(0008).                                                     
           02  BC31-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  BC31-LNOM                                                        
               PIC X(0025).                                                     
           02  BC31-LPRENOM                                                     
               PIC X(0015).                                                     
           02  BC31-LBATIMENT                                                   
               PIC X(0003).                                                     
           02  BC31-LESCALIER                                                   
               PIC X(0003).                                                     
           02  BC31-LETAGE                                                      
               PIC X(0003).                                                     
           02  BC31-LPORTE                                                      
               PIC X(0003).                                                     
           02  BC31-LCMPAD1                                                     
               PIC X(0032).                                                     
           02  BC31-CVOIE                                                       
               PIC X(0005).                                                     
           02  BC31-CTVOIE                                                      
               PIC X(0004).                                                     
           02  BC31-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  BC31-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  BC31-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  BC31-LBUREAU                                                     
               PIC X(0026).                                                     
           02  BC31-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  BC31-DVENTE                                                      
               PIC X(0008).                                                     
           02  BC31-TELDOM                                                      
               PIC X(0010).                                                     
           02  BC31-TELBUR                                                      
               PIC X(0010).                                                     
           02  BC31-LPOSTEBUR                                                   
               PIC X(0005).                                                     
           02  BC31-WPRESENTEE                                                  
               PIC X(0001).                                                     
           02  BC31-WCHARGEE                                                    
               PIC X(0001).                                                     
           02  BC31-DVERSION                                                    
               PIC X(0008).                                                     
           02  BC31-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  BC31-IDCLIENT                                                    
               PIC X(0008).                                                     
           02  BC31-CPAYS                                                       
               PIC X(0003).                                                     
           02  BC31-NGSM                                                        
               PIC X(0015).                                                     
           02  BC31-NSOCVTE                                                     
               PIC X(0014).                                                     
           02  BC31-PRFDOS                                                      
               PIC X(0003).                                                     
           02  BC31-NUDOS                                                       
               PIC X(0009).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBC3101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBC3102-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LBATIMENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LESCALIER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LETAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LPORTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LCMPAD1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-TELBUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-LPOSTEBUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-LPOSTEBUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-DVERSION-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-DVERSION-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-IDCLIENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-IDCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-CPAYS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-CPAYS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NGSM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NGSM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NSOCVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NSOCVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-PRFDOS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-PRFDOS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC31-NUDOS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC31-NUDOS-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
