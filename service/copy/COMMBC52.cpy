      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MBC52                                                 
      * APPELLE PAR LINK DANS TMBC52                                            
      *                                                                         
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MBC52.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
             10 COMM-MBC52-NCARTE    PIC X(09).                                 
             10 COMM-MBC52-LNOM      PIC X(60).                                 
             10 COMM-MBC52-NCLIENTADR PIC X(08).                                
             10 COMM-MBC52-NSIRET    PIC X(14).                                 
             10 COMM-MBC52-CPOST     PIC X(05).                                 
             10 COMM-MBC52-LCOMN     PIC X(32).                                 
             10 COMM-MBC52-CPAYS     PIC X(03).                                 
             10 COMM-MBC52-TPAYS     PIC X(03).                                 
             10 COMM-MBC52-LPAYS     PIC X(15).                                 
             10 COMM-MBC52-NTDOM     PIC X(15).                                 
             10 COMM-MBC52-NSOCCICS  PIC X(03).                                 
             10 COMM-MBC52-NSOC      PIC X(3).                                  
             10 COMM-MBC52-NLIEU     PIC X(3).                                  
      *              FILLEUR                                                    
             10 COMM-MBC52-CFILLEUR       PIC X(200).                           
      *              CONTROLES  + DONNES  EN ENTREE   O/N                       
        05 COMM-MBC52-CTR.                                                      
      *   BC52 :    NBR MAXI DE LIGNE QUI PEUVENT ETRE TRAITES = 90             
             10 COMM-MBC52-NBROCC         PIC 9(05).                            
             10 COMM-MBC52-SSAAMMJJ       PIC X(08).                            
      *      MESSAGE ERREUR DE LA CONTROLE LOGIIQUE + TS DE RETOUR              
         05 COMM-BC52-RETOUR-TS.                                                
             10 COMM-MBC52-NOM-TS        PIC X(08).                             
             10 COMM-MBC52-NBPAGE        PIC 9(03).                             
             10 COMM-MBC52-NBLIG         PIC 9(07).                             
             10 COMM-MBC52-INDMAX        PIC 9(07).                             
             10 COMM-MBC52-CONTROLE      PIC X(01) .                            
                88 COMM-MBC52-CONTROLE-OK     VALUE ' '.                        
                88 COMM-MBC52-CONTROLE-PAS-OK VALUE '1'.                        
             10 COMM-MBC52-MESS          PIC X(80).                             
      *      MESSAGE ERREUR MESSAGE BC                                          
         05 COMM-BC52-RETOUR-MQ.                                                
             10 COMM-MBC52-CODRETMQ      PIC X(01).                             
                  88  COMM-MBC52-MQ-OK     VALUE ' '.                           
                  88  COMM-MBC52-MQ-ERREUR VALUE '1'.                           
             10 COMM-MBC52-LIBERRMQ      PIC X(60).                             
      *      MESSAGE ERREUR MESSAGE MQ ENVOI/ TRANSMISSION.                     
         05 COMM-BC52-RETOUR-BC.                                                
             10 COMM-MBC52-CODRETBC      PIC X(01).                             
                  88  COMM-MBC52-BC-OK     VALUE ' '.                           
                  88  COMM-MBC52-BC-ERREUR VALUE '1'.                           
             10 COMM-MBC52-LIBERRBC      PIC X(60).                             
         05 FILLER                       PIC X(34).                             
                                                                                
