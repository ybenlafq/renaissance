      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTRB02                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0300.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUM.ENVOI                                         
           10 RB03-NENVOI          PIC X(07).                                   
      *    *************************************************************        
      *                       DATE ENVOI FLUX 010                               
           10 RB03-CTRAIT          PIC X(05).                                   
      *    *************************************************************        
      *                       SOCIETE EXPEDITRICE INITIALE                      
           10 RB03-NSOCORIG        PIC X(03).                                   
      *    *************************************************************        
      *                       LIEU EXPEDITION INITIAL                           
           10 RB03-DRECU           PIC X(08).                                   
      *    *************************************************************        
      *                       NUMERO DE HS                                      
           10 RB03-NDOSSIER        PIC X(20).                                   
      *    *************************************************************        
      *                       NUMERO CODIC                                      
           10 RB03-NCODIC          PIC X(07).                                   
      *    *************************************************************        
      *                       NUMERO SERIE ENVOYE                               
           10 RB03-NSERIEE         PIC X(16).                                   
      *    *************************************************************        
      *                       NUMERO SERIE RECU                                 
           10 RB03-NSERIER         PIC X(16).                                   
      *    *************************************************************        
      *                       NUMERO EAN                                        
           10 RB03-WSTATUT         PIC X(01).                                   
      *    *************************************************************        
      *                       CODE TRAITEMENT                                   
           10 RB03-WREGUL          PIC X(01).                                   
      *    *************************************************************        
      *                       STOCK EQUIPEMENT SOLDE O/N                        
           10 RB03-WLREGUL         PIC X(30).                                   
      *    *************************************************************        
      *                       DATE SYSTEME                                      
           10 RB03-DSYST           PIC S9(13) COMP-3.                           
      *                       GESTION DU SOLDE DE L ENVOI                       
           10 RB03-WSOLDE          PIC X(01).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NENVOI-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NENVOI-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-CTRAIT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-CTRAIT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NSOCORIG-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NSOCORIG-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-DRECU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-DRECU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NDOSSIER-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NDOSSIER-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NSERIEE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NSERIEE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-NSERIER-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-NSERIER-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-WSTATUT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-WSTATUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-WREGUL-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-WREGUL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-WLREGUL-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-WLREGUL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB03-WSOLDE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 RB03-WSOLDE-F        PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      * THE NUMBER OF RECORDS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
