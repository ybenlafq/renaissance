      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - DETAIL ANOMALIE                                          00000020
      ***************************************************************** 00000030
       01   EAP1AI.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MFONCI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODEANOL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MCODEANOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODEANOF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCODEANOI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATUTL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MCSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTATUTF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MCSTATUTI      PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEANOL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MDATEANOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEANOF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MDATEANOI      PIC X(10).                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATETRTL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MDATETRTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATETRTF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDATETRTI      PIC X(10).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCLIENTL      COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MNCLIENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCLIENTF      PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNCLIENTI      PIC X(8).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAVL      COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MNSOCSAVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCSAVF      PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNSOCSAVI      PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAVL     COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MNLIEUSAVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUSAVF     PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNLIEUSAVI     PIC X(3).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCRIL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNCRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCRIF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNCRII    PIC X(11).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNVENTEI  PIC X(14).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOSSIERL     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MNDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDOSSIERF     PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNDOSSIERI     PIC X(9).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLGVTEL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MNLGVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLGVTEF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNLGVTEI  PIC X(3).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTOPAGEL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MDTOPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDTOPAGEF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MDTOPAGEI      PIC X(10).                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPRESTAL      COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MCPRESTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPRESTAF      PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCPRESTAI      PIC X(9).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPPRESTAL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MPPRESTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPPRESTAF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MPPRESTAI      PIC X(10).                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREGULL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MWREGULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWREGULF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MWREGULI  PIC X.                                          00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSOLDERL      COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MWSOLDERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWSOLDERF      PIC X.                                     00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MWSOLDERI      PIC X.                                     00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIMPL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MNSOCIMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCIMPF      PIC X.                                     00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNSOCIMPI      PIC X(3).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUIMPL     COMP PIC S9(4).                            00000880
      *--                                                                       
           02 MNLIEUIMPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUIMPF     PIC X.                                     00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MNLIEUIMPI     PIC X(3).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVTEIMPL      COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MNVTEIMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNVTEIMPF      PIC X.                                     00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNVTEIMPI      PIC X(7).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLGIMPL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MNLGIMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLGIMPF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MNLGIMPI  PIC X(3).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEMISL   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MWEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWEMISF   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MWEMISI   PIC X.                                          00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMENTL      COMP PIC S9(4).                            00001040
      *--                                                                       
           02 MCOMMENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMMENTF      PIC X.                                     00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCOMMENTI      PIC X(50).                                 00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCONFIRML     COMP PIC S9(4).                            00001080
      *--                                                                       
           02 MLCONFIRML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCONFIRMF     PIC X.                                     00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MLCONFIRMI     PIC X(47).                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCONFIRML     COMP PIC S9(4).                            00001120
      *--                                                                       
           02 MCCONFIRML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCONFIRMF     PIC X.                                     00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCCONFIRMI     PIC X.                                     00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCONFIRML     COMP PIC S9(4).                            00001160
      *--                                                                       
           02 MFCONFIRML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFCONFIRMF     PIC X.                                     00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MFCONFIRMI     PIC X(5).                                  00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MLIBERRI  PIC X(77).                                      00001230
      * CODE TRANSACTION                                                00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MCODTRAI  PIC X(4).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MZONCMDI  PIC X(14).                                      00001320
      * CICS DE TRAVAIL                                                 00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCICSI    PIC X(5).                                       00001370
      * NETNAME                                                         00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNETNAMI  PIC X(8).                                       00001420
      * CODE TERMINAL                                                   00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MSCREENI  PIC X(5).                                       00001470
      ***************************************************************** 00001480
      * DA2I - DETAIL ANOMALIE                                          00001490
      ***************************************************************** 00001500
       01   EAP1AO REDEFINES EAP1AI.                                    00001510
           02 FILLER    PIC X(12).                                      00001520
      * DATE DU JOUR                                                    00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MDATJOUA  PIC X.                                          00001550
           02 MDATJOUC  PIC X.                                          00001560
           02 MDATJOUP  PIC X.                                          00001570
           02 MDATJOUH  PIC X.                                          00001580
           02 MDATJOUV  PIC X.                                          00001590
           02 MDATJOUO  PIC X(10).                                      00001600
      * HEURE                                                           00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTIMJOUA  PIC X.                                          00001630
           02 MTIMJOUC  PIC X.                                          00001640
           02 MTIMJOUP  PIC X.                                          00001650
           02 MTIMJOUH  PIC X.                                          00001660
           02 MTIMJOUV  PIC X.                                          00001670
           02 MTIMJOUO  PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MFONCA    PIC X.                                          00001700
           02 MFONCC    PIC X.                                          00001710
           02 MFONCP    PIC X.                                          00001720
           02 MFONCH    PIC X.                                          00001730
           02 MFONCV    PIC X.                                          00001740
           02 MFONCO    PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCODEANOA      PIC X.                                     00001770
           02 MCODEANOC PIC X.                                          00001780
           02 MCODEANOP PIC X.                                          00001790
           02 MCODEANOH PIC X.                                          00001800
           02 MCODEANOV PIC X.                                          00001810
           02 MCODEANOO      PIC X(2).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCSTATUTA      PIC X.                                     00001840
           02 MCSTATUTC PIC X.                                          00001850
           02 MCSTATUTP PIC X.                                          00001860
           02 MCSTATUTH PIC X.                                          00001870
           02 MCSTATUTV PIC X.                                          00001880
           02 MCSTATUTO      PIC X(3).                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDATEANOA      PIC X.                                     00001910
           02 MDATEANOC PIC X.                                          00001920
           02 MDATEANOP PIC X.                                          00001930
           02 MDATEANOH PIC X.                                          00001940
           02 MDATEANOV PIC X.                                          00001950
           02 MDATEANOO      PIC X(10).                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MDATETRTA      PIC X.                                     00001980
           02 MDATETRTC PIC X.                                          00001990
           02 MDATETRTP PIC X.                                          00002000
           02 MDATETRTH PIC X.                                          00002010
           02 MDATETRTV PIC X.                                          00002020
           02 MDATETRTO      PIC X(10).                                 00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNCLIENTA      PIC X.                                     00002050
           02 MNCLIENTC PIC X.                                          00002060
           02 MNCLIENTP PIC X.                                          00002070
           02 MNCLIENTH PIC X.                                          00002080
           02 MNCLIENTV PIC X.                                          00002090
           02 MNCLIENTO      PIC X(8).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNSOCSAVA      PIC X.                                     00002120
           02 MNSOCSAVC PIC X.                                          00002130
           02 MNSOCSAVP PIC X.                                          00002140
           02 MNSOCSAVH PIC X.                                          00002150
           02 MNSOCSAVV PIC X.                                          00002160
           02 MNSOCSAVO      PIC X(3).                                  00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNLIEUSAVA     PIC X.                                     00002190
           02 MNLIEUSAVC     PIC X.                                     00002200
           02 MNLIEUSAVP     PIC X.                                     00002210
           02 MNLIEUSAVH     PIC X.                                     00002220
           02 MNLIEUSAVV     PIC X.                                     00002230
           02 MNLIEUSAVO     PIC X(3).                                  00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNCRIA    PIC X.                                          00002260
           02 MNCRIC    PIC X.                                          00002270
           02 MNCRIP    PIC X.                                          00002280
           02 MNCRIH    PIC X.                                          00002290
           02 MNCRIV    PIC X.                                          00002300
           02 MNCRIO    PIC X(11).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNVENTEA  PIC X.                                          00002330
           02 MNVENTEC  PIC X.                                          00002340
           02 MNVENTEP  PIC X.                                          00002350
           02 MNVENTEH  PIC X.                                          00002360
           02 MNVENTEV  PIC X.                                          00002370
           02 MNVENTEO  PIC X(14).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNDOSSIERA     PIC X.                                     00002400
           02 MNDOSSIERC     PIC X.                                     00002410
           02 MNDOSSIERP     PIC X.                                     00002420
           02 MNDOSSIERH     PIC X.                                     00002430
           02 MNDOSSIERV     PIC X.                                     00002440
           02 MNDOSSIERO     PIC X(9).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MNLGVTEA  PIC X.                                          00002470
           02 MNLGVTEC  PIC X.                                          00002480
           02 MNLGVTEP  PIC X.                                          00002490
           02 MNLGVTEH  PIC X.                                          00002500
           02 MNLGVTEV  PIC X.                                          00002510
           02 MNLGVTEO  PIC X(3).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MDTOPAGEA      PIC X.                                     00002540
           02 MDTOPAGEC PIC X.                                          00002550
           02 MDTOPAGEP PIC X.                                          00002560
           02 MDTOPAGEH PIC X.                                          00002570
           02 MDTOPAGEV PIC X.                                          00002580
           02 MDTOPAGEO      PIC X(10).                                 00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCPRESTAA      PIC X.                                     00002610
           02 MCPRESTAC PIC X.                                          00002620
           02 MCPRESTAP PIC X.                                          00002630
           02 MCPRESTAH PIC X.                                          00002640
           02 MCPRESTAV PIC X.                                          00002650
           02 MCPRESTAO      PIC X(9).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MPPRESTAA      PIC X.                                     00002680
           02 MPPRESTAC PIC X.                                          00002690
           02 MPPRESTAP PIC X.                                          00002700
           02 MPPRESTAH PIC X.                                          00002710
           02 MPPRESTAV PIC X.                                          00002720
           02 MPPRESTAO      PIC X(10).                                 00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MWREGULA  PIC X.                                          00002750
           02 MWREGULC  PIC X.                                          00002760
           02 MWREGULP  PIC X.                                          00002770
           02 MWREGULH  PIC X.                                          00002780
           02 MWREGULV  PIC X.                                          00002790
           02 MWREGULO  PIC X.                                          00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MWSOLDERA      PIC X.                                     00002820
           02 MWSOLDERC PIC X.                                          00002830
           02 MWSOLDERP PIC X.                                          00002840
           02 MWSOLDERH PIC X.                                          00002850
           02 MWSOLDERV PIC X.                                          00002860
           02 MWSOLDERO      PIC X.                                     00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MNSOCIMPA      PIC X.                                     00002890
           02 MNSOCIMPC PIC X.                                          00002900
           02 MNSOCIMPP PIC X.                                          00002910
           02 MNSOCIMPH PIC X.                                          00002920
           02 MNSOCIMPV PIC X.                                          00002930
           02 MNSOCIMPO      PIC X(3).                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MNLIEUIMPA     PIC X.                                     00002960
           02 MNLIEUIMPC     PIC X.                                     00002970
           02 MNLIEUIMPP     PIC X.                                     00002980
           02 MNLIEUIMPH     PIC X.                                     00002990
           02 MNLIEUIMPV     PIC X.                                     00003000
           02 MNLIEUIMPO     PIC X(3).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MNVTEIMPA      PIC X.                                     00003030
           02 MNVTEIMPC PIC X.                                          00003040
           02 MNVTEIMPP PIC X.                                          00003050
           02 MNVTEIMPH PIC X.                                          00003060
           02 MNVTEIMPV PIC X.                                          00003070
           02 MNVTEIMPO      PIC X(7).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MNLGIMPA  PIC X.                                          00003100
           02 MNLGIMPC  PIC X.                                          00003110
           02 MNLGIMPP  PIC X.                                          00003120
           02 MNLGIMPH  PIC X.                                          00003130
           02 MNLGIMPV  PIC X.                                          00003140
           02 MNLGIMPO  PIC X(3).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MWEMISA   PIC X.                                          00003170
           02 MWEMISC   PIC X.                                          00003180
           02 MWEMISP   PIC X.                                          00003190
           02 MWEMISH   PIC X.                                          00003200
           02 MWEMISV   PIC X.                                          00003210
           02 MWEMISO   PIC X.                                          00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MCOMMENTA      PIC X.                                     00003240
           02 MCOMMENTC PIC X.                                          00003250
           02 MCOMMENTP PIC X.                                          00003260
           02 MCOMMENTH PIC X.                                          00003270
           02 MCOMMENTV PIC X.                                          00003280
           02 MCOMMENTO      PIC X(50).                                 00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLCONFIRMA     PIC X.                                     00003310
           02 MLCONFIRMC     PIC X.                                     00003320
           02 MLCONFIRMP     PIC X.                                     00003330
           02 MLCONFIRMH     PIC X.                                     00003340
           02 MLCONFIRMV     PIC X.                                     00003350
           02 MLCONFIRMO     PIC X(47).                                 00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCCONFIRMA     PIC X.                                     00003380
           02 MCCONFIRMC     PIC X.                                     00003390
           02 MCCONFIRMP     PIC X.                                     00003400
           02 MCCONFIRMH     PIC X.                                     00003410
           02 MCCONFIRMV     PIC X.                                     00003420
           02 MCCONFIRMO     PIC X.                                     00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MFCONFIRMA     PIC X.                                     00003450
           02 MFCONFIRMC     PIC X.                                     00003460
           02 MFCONFIRMP     PIC X.                                     00003470
           02 MFCONFIRMH     PIC X.                                     00003480
           02 MFCONFIRMV     PIC X.                                     00003490
           02 MFCONFIRMO     PIC X(5).                                  00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MLIBERRA  PIC X.                                          00003520
           02 MLIBERRC  PIC X.                                          00003530
           02 MLIBERRP  PIC X.                                          00003540
           02 MLIBERRH  PIC X.                                          00003550
           02 MLIBERRV  PIC X.                                          00003560
           02 MLIBERRO  PIC X(77).                                      00003570
      * CODE TRANSACTION                                                00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MZONCMDA  PIC X.                                          00003670
           02 MZONCMDC  PIC X.                                          00003680
           02 MZONCMDP  PIC X.                                          00003690
           02 MZONCMDH  PIC X.                                          00003700
           02 MZONCMDV  PIC X.                                          00003710
           02 MZONCMDO  PIC X(14).                                      00003720
      * CICS DE TRAVAIL                                                 00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MCICSA    PIC X.                                          00003750
           02 MCICSC    PIC X.                                          00003760
           02 MCICSP    PIC X.                                          00003770
           02 MCICSH    PIC X.                                          00003780
           02 MCICSV    PIC X.                                          00003790
           02 MCICSO    PIC X(5).                                       00003800
      * NETNAME                                                         00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MNETNAMA  PIC X.                                          00003830
           02 MNETNAMC  PIC X.                                          00003840
           02 MNETNAMP  PIC X.                                          00003850
           02 MNETNAMH  PIC X.                                          00003860
           02 MNETNAMV  PIC X.                                          00003870
           02 MNETNAMO  PIC X(8).                                       00003880
      * CODE TERMINAL                                                   00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MSCREENA  PIC X.                                          00003910
           02 MSCREENC  PIC X.                                          00003920
           02 MSCREENP  PIC X.                                          00003930
           02 MSCREENH  PIC X.                                          00003940
           02 MSCREENV  PIC X.                                          00003950
           02 MSCREENO  PIC X(5).                                       00003960
                                                                                
