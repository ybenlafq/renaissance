      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBELV ENCAISSEMENT AUTO                *        
      *----------------------------------------------------------------*        
       01  RVRBELV .                                                            
           05  RBELV-CTABLEG2    PIC X(15).                                     
           05  RBELV-CTABLEG2-REDEF REDEFINES RBELV-CTABLEG2.                   
               10  RBELV-TENLV           PIC X(01).                             
           05  RBELV-WTABLEG     PIC X(80).                                     
           05  RBELV-WTABLEG-REDEF  REDEFINES RBELV-WTABLEG.                    
               10  RBELV-MODPAI          PIC X(01).                             
               10  RBELV-NA              PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBELV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBELV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBELV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBELV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBELV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
