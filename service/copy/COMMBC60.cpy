      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *--------------------------------------------------------                 
      * DONNE CLIENT                                                            
      * COMM DE PROGRAMME MBC60                                                 
      * APPELLE PAR LINK DANS TBC60                                             
      *                                LONGUEUR 1000 CARAC.                     
      *--------------------------------------------------------                 
       01 Z-COMMAREA-MBC60.                                                     
      *DONNES COMPLEMENTAIRES EN ENTREE DE LA MODULE                            
         05 MBC60-DONNES-ENTREE.                                                
      *                               CODE TRAITEMENT (MAJ CRE)                 
             10 COMM-MBC60-CODETR         PIC X(03).                            
      *                               TYPE DE L'ADRESSE A/B/C           00550009
             10 COMM-MBC60-ADRESSE        PIC X(01).                            
      *                               CONTROLE EXISTANCE COMMUNE                
             10 COMM-MBC60-COMN-ERREUR    PIC X(01).                            
      *                               COMMUNE FORCEE                            
             10 COMM-MBC60-COMN-FORCEE    PIC X(01).                            
      *                               MODE DEGRADE (O/N)                        
             10 COMM-MBC60-DEGRADE        PIC X(01).                            
      *                               CONTROLES EN ENTREE   O/N                 
             10 COMM-MBC60-CTR-SYNT-OK    PIC X(01).                            
             10 COMM-MBC60-CTR-LOG-OK     PIC X(01).                            
             10 COMM-MBC60-SSAAMMJJ       PIC X(08).                            
             10 COMM-MBC60-NSOCCICS       PIC X(03).                            
             10 COMM-MBC60-NSOC           PIC X(03).                            
             10 COMM-MBC60-NLIEU          PIC X(03).                            
             10 COMM-MBC60-POMAD          PIC X(01).                            
      *   SI 'N' ON N'ATTEND PAS LA REPONSE MQ                                  
             10 COMM-MBC60-REPONSE        PIC X(01).                            
         05 FILLER                        PIC X(09).                            
      *                                                                         
      *      DONNES CLIENT                                                      
      *                                                                         
         05 MBC60-DONNES-CLIENT.                                                
             10 COMM-MBC60-NCARTE       PIC X(09).                              
             10 COMM-MBC60-LNOM         PIC X(25).                              
             10 COMM-MBC60-LPRENOM      PIC X(15).                              
             10 COMM-MBC60-CTITRENOM    PIC X(05).                              
             10 COMM-MBC60-TORG         PIC X(05).                              
             10 COMM-MBC60-LORG         PIC X(32).                              
             10 COMM-MBC60-NCLIENTADR   PIC X(08).                              
             10 COMM-MBC60-NFOYERADR    PIC X(08).                              
             10 COMM-MBC60-DNAISS       PIC X(08).                              
             10 COMM-MBC60-CVOIE        PIC X(05).                              
             10 COMM-MBC60-CTVOIE       PIC X(04).                              
             10 COMM-MBC60-LNOMVOIE     PIC X(25).                              
             10 COMM-MBC60-CPOST        PIC X(05).                              
             10 COMM-MBC60-LCOMN        PIC X(32).                              
             10 COMM-MBC60-NTDOM        PIC X(15).                              
             10 COMM-MBC60-NGSM         PIC X(15).                              
             10 COMM-MBC60-NTBUR        PIC X(15).                              
             10 COMM-MBC60-POSTEBUR     PIC X(05).                              
             10 COMM-MBC60-EMAIL        PIC X(65).                              
             10 COMM-MBC60-LBATIMENT    PIC X(03).                              
             10 COMM-MBC60-LESCALIER    PIC X(03).                              
             10 COMM-MBC60-LETAGE       PIC X(03).                              
             10 COMM-MBC60-LPORTE       PIC X(03).                              
             10 COMM-MBC60-CMPAD1       PIC X(32).                              
             10 COMM-MBC60-LBUREAU      PIC X(26).                              
             10 COMM-MBC60-CPAYS        PIC X(03).                              
             10 COMM-MBC60-TPAYS        PIC X(03).                              
             10 COMM-MBC60-RESSEC       PIC X(01).                              
             10 COMM-MBC60-ORGMODIF     PIC X(01).                              
             10 FILLER                  PIC X(02).                              
             10 COMM-MBC60-EQUEST       PIC X(03).                              
         05 COMM-MBC60-INDIN            PIC X(03).                              
         05 COMM-MBC60-NCLIENTGRP       PIC X(08).                              
      *                                                                         
      *      FLAGS MODIFICATION         25 CHAR                                 
      *                                                                         
         05 MBC60-FLAG-CLI.                                                     
             10 COMM-MBC60-F-NCARTE     PIC X(01).                              
             10 COMM-MBC60-F-LNOM       PIC X(01).                              
             10 COMM-MBC60-F-LPRENOM    PIC X(01).                              
             10 COMM-MBC60-F-CTITRENOM  PIC X(01).                              
             10 COMM-MBC60-F-TORG       PIC X(01).                              
             10 COMM-MBC60-F-LORG       PIC X(01).                              
             10 COMM-MBC60-F-DNAISS     PIC X(01).                              
             10 COMM-MBC60-F-CVOIE      PIC X(01).                              
             10 COMM-MBC60-F-CTVOIE     PIC X(01).                              
             10 COMM-MBC60-F-LNOMVOIE   PIC X(01).                              
             10 COMM-MBC60-F-CPOST      PIC X(01).                              
             10 COMM-MBC60-F-LCOMN      PIC X(01).                              
             10 COMM-MBC60-F-NTDOM      PIC X(01).                              
             10 COMM-MBC60-F-NGSM       PIC X(01).                              
             10 COMM-MBC60-F-NTBUR      PIC X(01).                              
             10 COMM-MBC60-F-POSTEBUR   PIC X(01).                              
             10 COMM-MBC60-F-EMAIL      PIC X(01).                              
             10 COMM-MBC60-F-LBATIMENT  PIC X(01).                              
             10 COMM-MBC60-F-LESCALIER  PIC X(01).                              
             10 COMM-MBC60-F-LETAGE     PIC X(01).                              
             10 COMM-MBC60-F-LPORTE     PIC X(01).                              
             10 COMM-MBC60-F-CMPAD1     PIC X(01).                              
             10 COMM-MBC60-F-LBUREAU    PIC X(01).                              
             10 COMM-MBC60-F-CPAYS      PIC X(01).                              
             10 COMM-MBC60-F-RESSEC     PIC X(01).                              
             10 COMM-MBC60-F-FILLEUR    PIC X(05).                              
         05 FILLER                      PIC X(10).                              
      * ADDIFIF � L'APPLICATION                                                 
         05 COMM-MBC60-CPNAISS      PIC X(05).                                  
         05 COMM-MBC60-LNAISS       PIC X(32).                                  
         05 COMM-MBC60-F-LNAISS     PIC X(01).                                  
      *                                                                         
      *      MESSAGE ERREUR DE LA CONTROLE SYNT +LOG+ MESSAGE MQ                
         05 MBC60-RETOUR-CTR.                                                   
             10 COMM-MBC60-CONTROLE      PIC X(01).                             
                88 COMM-MBC60-CONTROLE-OK     VALUE ' '.                        
                88 COMM-MBC60-CONTROLE-PAS-OK VALUE '1'.                        
             10 COMM-MBC60-MESS          PIC X(80).                             
         05 COMM-BC50-RETOUR-MQ.                                                
             10 COMM-MBC60-CODRETMQ      PIC X(01).                             
                  88  COMM-MBC60-MQ-OK     VALUE ' '.                           
                  88  COMM-MBC60-MQ-ERREUR VALUE '1'.                           
             10 COMM-MBC60-LIBERRMQ      PIC X(60).                             
         05 COMM-BC50-RETOUR-BC.                                                
             10 COMM-MBC60-CODRETBC      PIC X(01).                             
                  88  COMM-MBC60-BC-OK     VALUE ' '.                           
                  88  COMM-MBC60-BC-ERREUR VALUE '1'.                           
             10 COMM-MBC60-LIBERRBC      PIC X(60).                             
      *ZONE D'ECHANGE POUR L'APPLICATION APPELANTE                              
         05 COMM-MBC60-CHANGE            PIC X(20).                             
         05 COMM-MBC60-ACID              PIC X(08).                             
         05 FILLER                       PIC X(02).                             
JC    *      DONNES CARTE                                                       
JC    *                                                                         
JC       05 MBC60-DONNES-CARTE.                                                 
JC           10 COMM-MBC60-NSOCIETEM     PIC X(03).                             
JC           10 COMM-MBC60-NLIEUM        PIC X(03).                             
JC           10 COMM-MBC60-CVENDEUR      PIC X(06).                             
JC           10 COMM-MBC60-NORDRE        PIC X(05).                             
JC           10 COMM-MBC60-NVENTE        PIC X(07).                             
JC           10 COMM-MBC60-FCOURR        PIC X(01).                             
JC                                                                              
JC       05 MBC60-FILLER                 PIC X(231).                            
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
