      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE050 AU 20/03/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,09,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE050.                                                        
            05 NOMETAT-IHE050           PIC X(6) VALUE 'IHE050'.                
            05 RUPTURES-IHE050.                                                 
           10 IHE050-CHEFPROD           PIC X(09).                      007  009
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE050-SEQUENCE           PIC S9(04) COMP.                016  002
      *--                                                                       
           10 IHE050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE050.                                                   
           10 IHE050-APPLI              PIC X(05).                      018  005
           10 IHE050-CODETRAIT          PIC X(11).                      023  011
           10 IHE050-CODIC              PIC X(07).                      034  007
           10 IHE050-DATEMVT            PIC X(08).                      041  008
           10 IHE050-DEPOT              PIC X(05).                      049  005
           10 IHE050-STOCK              PIC X(05).                      054  005
           10 IHE050-WTABLEG            PIC X(20).                      059  020
           10 IHE050-PRMP               PIC 9(12)V9(2).                 079  014
           10 IHE050-QTE                PIC 9(05)     .                 093  005
           10 IHE050-VALO               PIC 9(12)V9(2).                 098  014
            05 FILLER                      PIC X(401).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE050-LONG           PIC S9(4)   COMP  VALUE +111.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE050-LONG           PIC S9(4) COMP-5  VALUE +111.           
                                                                                
      *}                                                                        
