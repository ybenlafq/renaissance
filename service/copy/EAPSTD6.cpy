      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE - CONDITION DE PR�PARATION     *
      * NOM FICHIER.: EAPSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 280                                             *
      *****************************************************************
      *
       01  EAPSTD6.
      * TYPE ENREGISTREMENT : RECEPTION ARTICLE CONDITION DE PR�PARATION
           05      EAPSTD6-TYP-ENREG      PIC  X(0006).
      * CODE ETAT
           05      EAPSTD6-CETAT          PIC  X(0001).
      * CODE SOCIETE
           05      EAPSTD6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EAPSTD6-CARTICLE       PIC  X(0018).
      * CONDITION DE PREPARATION
           05      EAPSTD6-COND-PREP      PIC  X(0005).
      * LIBELLE CONDITION DE PREPARATION
           05      EAPSTD6-LIB-COND-PREP  PIC  X(0035).
      * QUANTITE
           05      EAPSTD6-QTE            PIC  9(0009).
      * LONGUEUR
           05      EAPSTD6-LONGUEUR       PIC  9(0009).
      * LARGEUR
           05      EAPSTD6-LARGEUR        PIC  9(0009).
      * HAUTEUR
           05      EAPSTD6-HAUTEUR        PIC  9(0009).
      * POIDS
           05      EAPSTD6-POIDS          PIC  9(0009).
      * TYPE DE PREPARATION
           05      EAPSTD6-TYPE-PREPA     PIC  9(0003).
      * CODE EMBALLAGE
           05      EAPSTD6-CODE-EMBALLAGE PIC  X(0010).
      * COEFFICIENT DE FOISONNEMENT
           05      EAPSTD6-COEF-FOISONNEM PIC  9(0003).
      * COEFFICIENT DE VIDE
           05      EAPSTD6-COEF-VIDE      PIC  9(0003).
      * NUMERO TYPE SUPPORT
           05      EAPSTD6-NTYPE-SUPPORT  PIC  9(0009).
      * CODE � BARRES
           05      EAPSTD6-NEAN           PIC  X(0020).
      * ADRESSE
           05      EAPSTD6-ADRESSE        PIC  X(0020).
      * NBRE D �TIQUETTES
           05      EAPSTD6-NB-ETIQUETTES  PIC  9(0003).
      * DISPONIBLE 0/1
           05      EAPSTD6-DISPONIBLE     PIC  9(0001).
      * ZONE G�N�RIQUE
           05      EAPSTD6-ZONE-GENERIQUE PIC  9(0003).
      * QTE R�APPRO
           05      EAPSTD6-QTE-REAPPRO    PIC  9(0009).
      * SEUIL MINI
           05      EAPSTD6-SEUIL-MINI     PIC  9(0009).
      * SEUIL MAXI
           05      EAPSTD6-SEUIL-MAXI     PIC  9(0009).
      * TYPE R�APPRO
           05      EAPSTD6-TYPE-REAPPRO   PIC  9(0003).
      * MODE PREPARATION
           05      EAPSTD6-MODE-PREPARATION PIC 9(003).
      * PREPARATION MASSE
           05      EAPSTD6-PREPA-MASSE    PIC  9(0001).
      * SEUIL PREPA MASSE
           05      EAPSTD6-SEUIL-PREP-MASSE PIC 9(009).
      * SEUIL PREPA PALETTE
           05      EAPSTD6-SEUIL-PREP-PAL PIC  9(0009).
      * PROFIL PREPA PALETTE
           05      EAPSTD6-PROF-PREP-PAL  PIC  9(0003).
      * ROTATION A/B/C/BC
           05      EAPSTD6-ROTATION       PIC  X(0001).
      * PROFIL DE R�APPRO
           05      EAPSTD6-PROFIL-REAPPRO PIC  9(0003).
      * FILLER 30
           05      EAPSTD6-FILLER         PIC  X(0030).
      * FIN
           05      EAPSTD6-FIN            PIC  X(0001).
      
