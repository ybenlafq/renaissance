      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBSTA STATUT CODE RETOUR COMPLETEL     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBSTA.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBSTA.                                                             
      *}                                                                        
           05  RBSTA-CTABLEG2    PIC X(15).                                     
           05  RBSTA-CTABLEG2-REDEF REDEFINES RBSTA-CTABLEG2.                   
               10  RBSTA-WCMPLT          PIC X(01).                             
           05  RBSTA-WTABLEG     PIC X(80).                                     
           05  RBSTA-WTABLEG-REDEF  REDEFINES RBSTA-WTABLEG.                    
               10  RBSTA-LWCMPLT         PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRBSTA-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRBSTA-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBSTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBSTA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
