      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFX02   EFX02                                              00000020
      ***************************************************************** 00000030
       01   EFX06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELEMENTL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MELEMENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MELEMENTF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MELEMENTI      PIC X(8).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAFFL     COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MAFFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MAFFF     PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MAFFI     PIC X(6).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITGLL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MTITGLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITGLF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTITGLI   PIC X(50).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITFDCL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MTITFDCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTITFDCF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MTITFDCI  PIC X(23).                                      00000390
           02 FILLER  OCCURS   12 TIMES .                               00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSUPL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MSUPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSUPF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MSUPI   PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MSOCI   PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METABLL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 METABLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 METABLF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 METABLI      PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLEGLL      COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MCLEGLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLEGLF      PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MCLEGLI      PIC X(6).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSECTTABL    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MSECTTABL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSECTTABF    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MSECTTABI    PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MLIBELLEI    PIC X(29).                                 00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFDC6L  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MFDC6L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFDC6F  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MFDC6I  PIC X(6).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFDC15L      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MFDC15L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MFDC15F      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MFDC15I      PIC X(15).                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLIBERRI  PIC X(78).                                      00000760
      * CODE TRANSACTION                                                00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MZONCMDI  PIC X(15).                                      00000850
      * NOM DU CICS                                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      * NOM LIGNE VTAM                                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      * CODE TERMINAL                                                   00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(4).                                       00001000
      ***************************************************************** 00001010
      * SDF: EFX02   EFX02                                              00001020
      ***************************************************************** 00001030
       01   EFX06O REDEFINES EFX06I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
      * DATE JOUR TRAITEMENT                                            00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
      * HEURE TRAITEMENT                                                00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC Z99.                                        00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNBPA     PIC X.                                          00001300
           02 MNBPC     PIC X.                                          00001310
           02 MNBPP     PIC X.                                          00001320
           02 MNBPH     PIC X.                                          00001330
           02 MNBPV     PIC X.                                          00001340
           02 MNBPO     PIC Z99.                                        00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MELEMENTA      PIC X.                                     00001370
           02 MELEMENTC PIC X.                                          00001380
           02 MELEMENTP PIC X.                                          00001390
           02 MELEMENTH PIC X.                                          00001400
           02 MELEMENTV PIC X.                                          00001410
           02 MELEMENTO      PIC X(8).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MAFFA     PIC X.                                          00001440
           02 MAFFC     PIC X.                                          00001450
           02 MAFFP     PIC X.                                          00001460
           02 MAFFH     PIC X.                                          00001470
           02 MAFFV     PIC X.                                          00001480
           02 MAFFO     PIC X(6).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MTITGLA   PIC X.                                          00001510
           02 MTITGLC   PIC X.                                          00001520
           02 MTITGLP   PIC X.                                          00001530
           02 MTITGLH   PIC X.                                          00001540
           02 MTITGLV   PIC X.                                          00001550
           02 MTITGLO   PIC X(50).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MTITFDCA  PIC X.                                          00001580
           02 MTITFDCC  PIC X.                                          00001590
           02 MTITFDCP  PIC X.                                          00001600
           02 MTITFDCH  PIC X.                                          00001610
           02 MTITFDCV  PIC X.                                          00001620
           02 MTITFDCO  PIC X(23).                                      00001630
           02 FILLER  OCCURS   12 TIMES .                               00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MSUPA   PIC X.                                          00001660
             03 MSUPC   PIC X.                                          00001670
             03 MSUPP   PIC X.                                          00001680
             03 MSUPH   PIC X.                                          00001690
             03 MSUPV   PIC X.                                          00001700
             03 MSUPO   PIC X.                                          00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MSOCA   PIC X.                                          00001730
             03 MSOCC   PIC X.                                          00001740
             03 MSOCP   PIC X.                                          00001750
             03 MSOCH   PIC X.                                          00001760
             03 MSOCV   PIC X.                                          00001770
             03 MSOCO   PIC X(3).                                       00001780
             03 FILLER       PIC X(2).                                  00001790
             03 METABLA      PIC X.                                     00001800
             03 METABLC PIC X.                                          00001810
             03 METABLP PIC X.                                          00001820
             03 METABLH PIC X.                                          00001830
             03 METABLV PIC X.                                          00001840
             03 METABLO      PIC X(3).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MCLEGLA      PIC X.                                     00001870
             03 MCLEGLC PIC X.                                          00001880
             03 MCLEGLP PIC X.                                          00001890
             03 MCLEGLH PIC X.                                          00001900
             03 MCLEGLV PIC X.                                          00001910
             03 MCLEGLO      PIC X(6).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MSECTTABA    PIC X.                                     00001940
             03 MSECTTABC    PIC X.                                     00001950
             03 MSECTTABP    PIC X.                                     00001960
             03 MSECTTABH    PIC X.                                     00001970
             03 MSECTTABV    PIC X.                                     00001980
             03 MSECTTABO    PIC X.                                     00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MLIBELLEA    PIC X.                                     00002010
             03 MLIBELLEC    PIC X.                                     00002020
             03 MLIBELLEP    PIC X.                                     00002030
             03 MLIBELLEH    PIC X.                                     00002040
             03 MLIBELLEV    PIC X.                                     00002050
             03 MLIBELLEO    PIC X(29).                                 00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MFDC6A  PIC X.                                          00002080
             03 MFDC6C  PIC X.                                          00002090
             03 MFDC6P  PIC X.                                          00002100
             03 MFDC6H  PIC X.                                          00002110
             03 MFDC6V  PIC X.                                          00002120
             03 MFDC6O  PIC X(6).                                       00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MFDC15A      PIC X.                                     00002150
             03 MFDC15C PIC X.                                          00002160
             03 MFDC15P PIC X.                                          00002170
             03 MFDC15H PIC X.                                          00002180
             03 MFDC15V PIC X.                                          00002190
             03 MFDC15O      PIC X(15).                                 00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLIBERRA  PIC X.                                          00002220
           02 MLIBERRC  PIC X.                                          00002230
           02 MLIBERRP  PIC X.                                          00002240
           02 MLIBERRH  PIC X.                                          00002250
           02 MLIBERRV  PIC X.                                          00002260
           02 MLIBERRO  PIC X(78).                                      00002270
      * CODE TRANSACTION                                                00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCODTRAA  PIC X.                                          00002300
           02 MCODTRAC  PIC X.                                          00002310
           02 MCODTRAP  PIC X.                                          00002320
           02 MCODTRAH  PIC X.                                          00002330
           02 MCODTRAV  PIC X.                                          00002340
           02 MCODTRAO  PIC X(4).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MZONCMDA  PIC X.                                          00002370
           02 MZONCMDC  PIC X.                                          00002380
           02 MZONCMDP  PIC X.                                          00002390
           02 MZONCMDH  PIC X.                                          00002400
           02 MZONCMDV  PIC X.                                          00002410
           02 MZONCMDO  PIC X(15).                                      00002420
      * NOM DU CICS                                                     00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCICSA    PIC X.                                          00002450
           02 MCICSC    PIC X.                                          00002460
           02 MCICSP    PIC X.                                          00002470
           02 MCICSH    PIC X.                                          00002480
           02 MCICSV    PIC X.                                          00002490
           02 MCICSO    PIC X(5).                                       00002500
      * NOM LIGNE VTAM                                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
      * CODE TERMINAL                                                   00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSCREENA  PIC X.                                          00002610
           02 MSCREENC  PIC X.                                          00002620
           02 MSCREENP  PIC X.                                          00002630
           02 MSCREENH  PIC X.                                          00002640
           02 MSCREENV  PIC X.                                          00002650
           02 MSCREENO  PIC X(4).                                       00002660
                                                                                
