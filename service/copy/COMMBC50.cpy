      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TBC50 (TBC50 -> TBC51)   TR: BC50  *    00020000
      *                 CONSULTATION CLIENTS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 5000  00050000
      *                                                                 00060000
      *            TRANSACTION BC50 : CONSULTATIONS CLIENTS        *    00070000
      *                                                                 00080000
      **************************************************************    00090000
      * LY07 : INTERFACE AVEC SIEBEL B2B : REDEFINITION DE LA ZONE *    00100000
      *        POUR LE TRANSFERT DES DONN�ES AUX VENTES            *    00110000
      *                                                            *    00120000
      **************************************************************    00130000
      * COMMAREA SPECIFIQUE PRG TBC50 (MENU)             TR: BC50  *    00140000
      *                          TBC51                             *    00150000
      *                           TBC52                            *    00160000
      * ---------------------------------------------------------- *    00170000
      *  MAINTENANCE : 21/07/10 MARQUE INNO32                      *    00180000
      *  AJOUT DU DIGICODE DANS LE TRANSFERT VERS LES ECRANS       *    00190000
      *  DE VENTE. MODIFICATION DE LA LONGEUR DE COMM              *    00200000
      * ---------------------------------------------------------- *    00210000
      *  MAINTENANCE : 04/05/12 MARQUE M16117                      *    00220000
      *  AJOUT DE LA ZONE  ASC DANS LE TRANSFERT VERS LES ECRANS   *    00230000
      *  DE VENTE. MODIFICATION DE LA LONGEUR DE COMM              *    00240000
      **************************************************************    00250000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00260000
      **************************************************************    00270000
      *                                                                 00280000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00290000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00300000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00310000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00320000
      *                                                                 00330000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +5372    00340000
      * COMPRENANT :                                                    00350000
      * 1 - LES ZONES RESERVEES A AIDA                                  00360000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00370000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00380000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00390000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00400000
      *                                                                 00410000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00420000
      * PAR AIDA                                                        00430000
      *                                                                 00440000
      *---------------------------------372 + 5000  ----------------    00450000
      *                                                                 00460000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-BC50-LONG-COMMAREA PIC S9(4) COMP VALUE +5382.           00470000
      *--                                                                       
       01  COM-BC50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +5382.                 
      *}                                                                        
      *                                                                 00480000
       01  Z-COMMAREA.                                                  00490000
      *                                                                 00500000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00510000
          02 FILLER-COM-AIDA      PIC X(100).                           00520000
      *                                                                 00530000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00540000
          02 COMM-CICS-APPLID     PIC X(8).                             00550000
          02 COMM-CICS-NETNAM     PIC X(8).                             00560000
          02 COMM-CICS-TRANSA     PIC X(4).                             00570000
      *                                                                 00580000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00590000
          02 COMM-DATE-SIECLE     PIC XX.                               00600000
          02 COMM-DATE-ANNEE      PIC XX.                               00610000
          02 COMM-DATE-MOIS       PIC XX.                               00620000
          02 COMM-DATE-JOUR       PIC XX.                               00630000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00640000
          02 COMM-DATE-QNTA       PIC 999.                              00650000
          02 COMM-DATE-QNT0       PIC 99999.                            00660000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00670000
          02 COMM-DATE-BISX       PIC 9.                                00680000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00690000
          02 COMM-DATE-JSM        PIC 9.                                00700000
      *   LIBELLES DU JOUR COURT - LONG                                 00710000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00720000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00730000
      *   LIBELLES DU MOIS COURT - LONG                                 00740000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00750000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00760000
      *   DIFFERENTES FORMES DE DATE                                    00770000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00780000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00790000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00800000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00810000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00820000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00830000
      *   DIFFERENTES FORMES DE DATE                                    00840000
          02 COMM-DATE-FILLER     PIC X(14).                            00850000
      *                                                                 00860000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00870000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 BC50-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00890000
      *                                                                 00900000
      ***************************************************************** 00910000
      *                                                                 00920000
      * ZONES RESERVEES APPLICATIVES ---------------------4000--- 3724  00930000
      *                                                                 00940000
      *            TRANSACTION BC50 : CONSULTATION    CLIENTS         * 00950000
      *                                                                 00960000
      ***************************************************************** 00970000
          02 COMM-BC50-APPLI .                                          00980000
      *------------------------------ ZONE DONNEES TBC50                00990000
            05 COMM-BC50-DONNEES-TBC50.                                 01000000
      *------CRITERES DE SELECTION-----------------           283       01010000
      *-                              NUMERO  CARTE             0       01020000
             10 COMM-BC50-NCARTE  PIC X(09).                            01030000
      *-                              LIBELLE NOM                       01040000
             10 COMM-BC50-LNOM    PIC X(25).                            01050000
      *-                              NOM GENERIQUE                     01060000
             10 COMM-BC50-LNOM-G  PIC X(01).                            01070000
      *-                              LIBELLE PRENOM                    01080000
             10 COMM-BC50-LPRENOM PIC X(15).                            01090000
      *-                              CODE POSTAL                       01100000
             10 COMM-BC50-CPOST   PIC X(05).                            01110000
      *-                              LIBELLE VILLE                     01120000
             10 COMM-BC50-LCOMN   PIC X(32).                            01130000
      *-                              LIBELLE BUREAU                    01140000
             10 COMM-BC50-LBUREAU PIC X(26).                            01150000
      *-                                 CODE PAYS PLAQUES NUMEROLOGIQUE01160000
             10 COMM-BC50-CPAYS   PIC X(03).                            01170000
      *-                                 CODE PAYS AFNOR                01180000
             10 COMM-BC50-TPAYS   PIC X(03).                            01190000
      *-                              LIBELLE PAYS                      01200000
             10 COMM-BC50-LPAYS   PIC X(15).                            01210000
      *                               LONGEUR NUM TEL (SSTABLE PAYS)    01220000
             10 COMM-BC50-LONGTEL PIC X(02).                            01230000
      *-                              NUM  DE TELEPHONE                 01240000
             10 COMM-BC50-NTDOM   PIC X(15).                            01250000
      *-                              NUM  DE GSM                       01260000
             10 COMM-BC50-NGSM    PIC X(15).                            01270000
      *-                              NUM  DE BUREAU                    01280000
             10 COMM-BC50-NTBUR   PIC X(15).                            01290000
      *-                              DATE DE NAISSANCE                 01300000
             10 COMM-BC50-DNAISS  PIC X(08).                            01310000
      *-                              NUM  DE SOCIETE                   01320000
             10 COMM-BC50-NSOC    PIC X(3).                             01330000
      *-                              NUM  DE LIEU                      01340000
             10 COMM-BC50-NLIEU   PIC X(3).                             01350000
      *-                              NUM  DE VENTE                     01360000
             10 COMM-BC50-NVENTE  PIC X(7).                             01370000
      *-                              NUM  DE EMAIL                     01380000
             10 COMM-BC50-EMAIL   PIC X(65).                            01390000
      *------ZONES  GESTION ECRAN - PAGINATION                 16       01400000
            05 COMM-BC50-EBC50.                                         01410000
      *-                              NUMERO PAGE COURANTE              01420000
             10 COMM-BC50-NUMPAG    PIC 9(03).                          01430000
      *-                              NUMERO PAGE MAXI                  01440000
             10 COMM-BC50-PAGE-MAX  PIC 9(03).                          01450000
      *-                              NUMERO PAGE ECRAN                 01460000
             10 COMM-BC50-ECRAN     PIC 9(03).                          01470000
      *-                              NUMERO PAGE ECRAN MAXI            01480000
             10 COMM-BC50-ECRAN-MAX PIC 9(03).                          01490000
      *-                              NB LIGNES                         01500000
             10 COMM-BC50-I-CHOIX-MAX  PIC 9(02).                       01510000
      *-                              NUMERO PAGE ECRAN MAXI            01520000
             10 COMM-BC50-I-CHOIX   PIC 9(02).                          01530000
      *                                                                 01540000
      *      ZONE MESSAGE D'ERREUR POUR LA TRANSACT 'APPELANTE' -80-    01550000
          05 COMM-BC50-MLIBERR      PIC X(80).                          01560000
      *                                                                 01570000
      *      SELECTION D'UNE ORGANISATION                               01580000
          05 COMM-BC50-CORG          PIC X(01).                         01590000
      *      ZONE FLAG POUR INDIQUER LA VENTE AVEC LA REDEVENCE         01600000
          05 COMM-BC50-SELECTION     PIC X.                             01610000
             88 COMM-BC50-NON-SELECT         VALUE SPACE.               01620000
             88 COMM-BC50-OUI-REDEV          VALUE 'O'.                 01630000
      *      ZONE FILLEUR POUR LA SELECTION                             01640000
          05 COMM-BC50-APPEL-BDCLI   PIC X.                             01650000
             88 COMM-BC50-NON-APPEL          VALUE SPACE.               01660000
             88 COMM-BC50-OUI-APPEL          VALUE 'O'.                 01670000
      *      ZONE FILLEUR POUR LA SELECTION                             01680000
PM    *   05 COMM-BC50-SELFILLEUR    PIC X(17).                         01690000
PM        05 COMM-BC50-SELFILLEUR.                                      01700000
PM           10 COMM-BC50-NSOCIETEM     PIC X(03).                      01710000
PM           10 COMM-BC50-NLIEUM        PIC X(03).                      01720000
PM           10 COMM-BC50-CVENDEUR      PIC X(06).                      01730000
PM           10 COMM-BC50-NORDRE        PIC X(05).                      01740000
      *------GESTION ECRAN - FICHES---------------------------3620      01750000
      *----------------------------------LONGUEUR 374 X 10              01760000
          05 COMM-BC50-DONNEE-CLI.                                      01770000
           08 COMM-BC50-LIGNE  OCCURS 10.                               01780000
             10 COMM-BC50-ENCARTE        PIC X(09).                     01790000
             10 COMM-BC50-ELNOM          PIC X(25).                     01800000
             10 COMM-BC50-ELPRENOM       PIC X(15).                     01810000
             10 COMM-BC50-ECTITRENOM     PIC X(05).                     01820000
             10 COMM-BC50-ETORG          PIC X(05).                     01830000
             10 COMM-BC50-ELORG          PIC X(32).                     01840000
             10 COMM-BC50-ENCLIENTADR    PIC X(08).                     01850000
             10 COMM-BC50-ENFOYERADR     PIC X(08).                     01860000
             10 COMM-BC50-EDNAISS        PIC X(08).                     01870000
             10 COMM-BC50-ECRAN1.                                       01880000
                 15 COMM-BC50-ECVOIE     PIC X(05).                     01890000
                 15 COMM-BC50-ECTVOIE    PIC X(04).                     01900000
                 15 COMM-BC50-ELNOMVOIE  PIC X(25).                     01910000
             10 COMM-BC50-ECRAN2.                                       01920000
                 15 COMM-BC50-ECPOST     PIC X(05).                     01930000
                 15 COMM-BC50-ELCOMN     PIC X(32).                     01940000
                 15 COMM-BC50-ENTDOM     PIC X(15).                     01950000
             10 COMM-BC50-ECRAN3.                                       01960000
                 15 COMM-BC50-ENGSM      PIC X(15).                     01970000
                 15 COMM-BC50-ENTBUR     PIC X(15).                     01980000
                 15 COMM-BC50-EPOSTEBUR  PIC X(05).                     01990000
             10 COMM-BC50-ECRAN4.                                       02000000
                 15 COMM-BC50-EEMAIL     PIC X(65).                     02010000
             10 COMM-BC50-ADRESSE-COMPLETE.                             02020000
                 15 COMM-BC50-ELBATIMENT PIC X(03).                     02030000
                 15 COMM-BC50-ELESCALIER PIC X(03).                     02040000
                 15 COMM-BC50-ELETAGE    PIC X(03).                     02050000
                 15 COMM-BC50-ELPORTE    PIC X(03).                     02060000
                 15 COMM-BC50-ECMPAD1    PIC X(32).                     02070000
                 15 COMM-BC50-ELBUREAU   PIC X(26).                     02080000
      *-                                 CODE PAYS PLAQUES NUMEROLOGIQUE02090000
                 15 COMM-BC50-ECPAYS     PIC X(03).                     02100000
      *-                                 CODE PAYS AFNOR                02110000
                 15 COMM-BC50-ETPAYS     PIC X(03).                     02120000
      *           RESIDENCE SECONDAIRE (O/N) CONTIENT RES PRINCIPALE    02130000
                 15 COMM-BC50-ERESSEC    PIC X(01).                     02140000
      *                               DROIT DE MODIFIER L'ORGANISATION  02150000
                 15 COMM-BC50-EORGMODIF PIC X(01).                      02160000
      *                               FLAG DETENTION DONNE REDEVENCE    02170000
                 15 COMM-BC50-EQUEST    PIC X(03).                      02180000
      *                               TYPE CLIENT GENRE PROMOSTIM       02190000
                 15 COMM-BC50-CTYPCLIENT PIC X(01).                     02200000
      *                               ZONE FILLER POUR LA LISTE CLIENT  02210000
JC0807           15 COMM-BC50-GEST-B2B  PIC X(01).                      02220000
LY07  *----------------------------------LONGUEUR 38400                 02230000
LY07  *-- LES DONNEES ISSUES DE B2B UCM/SIEBEL                          02240000
LY07  *---TRA BC51 BC61                                                 02250000
LY07  *----------------------------------                               02260000
:         05 COMM-BCBB  REDEFINES COMM-BC50-DONNEE-CLI.                 02270000
:            10 COMM-BCBB-CLIENT-B2B.                                   02280000
:               15 COMM-BCBB-LIGNE  OCCURS 2.                           02290000
:                  20 COMM-BCBB-NCARTE            PIC X(09).            02300000
:                  20 COMM-BCBB-TORG              PIC X(05).            02310000
:                  20 COMM-BCBB-LNOM              PIC X(60).            02320000
:                  20 COMM-BCBB-NCLIENTADR        PIC X(08).            02330000
:                  20 COMM-BCBB-NSIRET            PIC X(14).            02340000
:                  20 COMM-BCBB-NTDOM             PIC X(15).            02350000
:                  20 COMM-BCBB-CPOST             PIC X(05).            02360000
:                  20 COMM-BCBB-LCOMN             PIC X(32).            02370000
:                  20 COMM-BCBB-CVOIE             PIC X(05).            02380000
:                  20 COMM-BCBB-CTVOIE            PIC X(04).            02390000
:                  20 COMM-BCBB-LNOMVOIE          PIC X(25).            02400000
:                  20 COMM-BCBB-CMPAD1            PIC X(32).            02410000
:                  20 COMM-BCBB-LBUREAU           PIC X(26).            02420000
:                  20 COMM-BCBB-CPAYS             PIC X(03).            02430000
:                  20 COMM-BCBB-TPAYS             PIC X(03).            02440000
:                  20 COMM-BCBB-ORGMODIF          PIC X(01).            02450000
:                  20 COMM-BCBB-TFACTU            PIC X(01).            02460000
:                  20 COMM-BCBB-ARF               PIC X(01).            02470000
:                  20 COMM-BCBB-MONTANT           PIC Z(10).            02480000
:                  20 COMM-BCBB-NIERRARCH         PIC X(01).            02490000
:                  20 COMM-BCBB-FACT              PIC X(01).            02500000
:                  20 COMM-BCBB-CPTEPARENT        PIC X(08).            02510000
:                  20 COMM-BCBB-CTYPCLIENT        PIC X(01).            02520000
:                  20 COMM-BCBB-NCOMPTE           PIC X(08).            02530000
:                  20 COMM-BCBB-EFILLER           PIC X(50).            02540000
:                  20 COMM-BCBB-CINSEE            PIC X(05).            02550000
:                  20 COMM-BCBB-CILOT             PIC X(08).            02560000
LY07          10 COMM-BCBB-EFILLER1          PIC X(3158).               02570000
          05 COMM-BC50-DONNE-NAISS.                                     02580000
            08 COMM-BC50-LIGNE-NAISS  OCCURS 10.                        02590000
             10 COMM-BC50-ECPNAISS       PIC X(05).                     02600000
             10 COMM-BC50-ELNAISS        PIC X(32).                     02610000
      *----------------------------------LONGUEUR 374                   02620000
          05 COMM-BC60-DONNE-CLI.                                       02630000
             10 COMM-BC60-ENCARTE        PIC X(09).                     02640000
             10 COMM-BC60-ELNOM          PIC X(25).                     02650000
             10 COMM-BC60-ELPRENOM       PIC X(15).                     02660000
             10 COMM-BC60-ECTITRENOM     PIC X(05).                     02670000
             10 COMM-BC60-ETORG          PIC X(05).                     02680000
             10 COMM-BC60-ELORG          PIC X(32).                     02690000
             10 COMM-BC60-ENCLIENTADR    PIC X(08).                     02700000
             10 COMM-BC60-ENFOYERADR     PIC X(08).                     02710000
             10 COMM-BC60-EDNAISS        PIC X(08).                     02720000
             10 COMM-BC60-ECVOIE         PIC X(05).                     02730000
             10 COMM-BC60-ECTVOIE        PIC X(04).                     02740000
             10 COMM-BC60-ELNOMVOIE      PIC X(25).                     02750000
             10 COMM-BC60-ECPOST         PIC X(05).                     02760000
             10 COMM-BC60-ELCOMN         PIC X(32).                     02770000
             10 COMM-BC60-ENTDOM         PIC X(15).                     02780000
             10 COMM-BC60-ENGSM          PIC X(15).                     02790000
             10 COMM-BC60-ENTBUR         PIC X(15).                     02800000
             10 COMM-BC60-EPOSTEBUR      PIC X(05).                     02810000
             10 COMM-BC60-EEMAIL         PIC X(65).                     02820000
             10 COMM-BC60-ELBATIMENT     PIC X(03).                     02830000
             10 COMM-BC60-ELESCALIER     PIC X(03).                     02840000
             10 COMM-BC60-ELETAGE        PIC X(03).                     02850000
             10 COMM-BC60-ELPORTE        PIC X(03).                     02860000
             10 COMM-BC60-ECMPAD1        PIC X(32).                     02870000
             10 COMM-BC60-ELBUREAU       PIC X(26).                     02880000
      *-                                 CODE PAYS PLAQUES NUMEROLOGIQUE02890000
             10 COMM-BC60-ECPAYS         PIC X(03).                     02900000
      *-                                 CODE PAYS AFNOR                02910000
             10 COMM-BC60-ETPAYS         PIC X(03).                     02920000
      *      RESIDENCE SECONDAIRE (O/N) CONTIENT RES PRINCIPALE         02930000
             10 COMM-BC60-ERESSEC        PIC X(01).                     02940000
      *                                  DROIT DE MODIFIER L"ORG        02950000
             10 COMM-BC60-EORGMODIF      PIC X(01).                     02960000
      *                                  DONNEE POUR LA REDEVANCE       02970000
             10 COMM-BC60-EQUEST         PIC X(03).                     02980000
      *                                  TYPE CLIENT                    02990000
             10 COMM-BC60-CTYPCLIENT     PIC X(01).                     03000000
      *                                  ZONE FILLER POUR FICHE CLIENT  03010000
JC0807       10 COMM-BC60-GEST-B2B       PIC X(01).                     03020000
JC0807*      10 COMM-BC60-EFILLER        PIC X(01).                     03030000
          05 COMM-BC60-ELPAYS            PIC X(15).                     03040000
          05 COMM-BC60-LONGTEL           PIC X(02).                     03050000
          05 COMM-BC60-INDIN             PIC X(03).                     03060000
          05 COMM-BC60-CILOT             PIC X(08).                     03070000
          05 COMM-BC60-CINSEE            PIC X(05).                     03080000
          05 COMM-BC60-LIBVOIE           PIC X(12).                     03090000
          05 COMM-BC60-DNAISS10          PIC X(10).                     03100000
INNO32    05 COMM-BC60-DIGICOD           PIC X(10).                     03110000
M16117    05 COMM-BC60-MCASC             PIC X(01).                     03120000
      *                                  ZONE FILLER POUR FICHE CLIENT  03130000
M16117**  05 COMM-BC60-EFILLEUR1         PIC X(07).                     03140000
M16117    05 COMM-BC60-EFILLEUR1         PIC X(06).                     03150000
                                                                        03160000
                                                                        03170000
      *        NOM    PROGRAMME    RETOUR LEVEL-MAX             42      03180000
          05 COMM-BC50-LEVEL-MAX      PIC X(06).                        03190000
      *        PARAMETRAGES DIVERS                                      03200000
      *                               ACCES BDCLI AUTORISE O/N          03210000
          05 COMM-BC50-ACCES          PIC X(01).                        03220000
      *                               CARTE CLIENT O/N                  03230000
          05 COMM-BC50-BD-CARTE       PIC X(01).                        03240000
      *                               TELEPHONE    O/N                  03250000
          05 COMM-BC50-BD-TEL         PIC X(01).                        03260000
      *                               DATE NAISSANCE O/N                03270000
          05 COMM-BC50-BD-DNAISS      PIC X(01).                        03280000
      *                               SELECTION PAR NVENTE  O/N         03290000
          05 COMM-BC50-BD-NVENTE      PIC X(01).                        03300000
      *                               SELECTION PAR EMAIL   O/N         03310000
          05 COMM-BC50-BD-EMAIL       PIC X(01).                        03320000
      *                               CODE PAYS AFNOR DE MAGASIN        03330000
          05 COMM-BC50-BD-PAYS        PIC X(03).                        03340000
      *                               CODE POUR OBTENIR L'INTERNATIONAL 03350000
          05 COMM-BC50-BD-INDOUT      PIC X(03).                        03360000
      *                               CURSEUR SUR LE TELEPHONE          03370000
          05 COMM-BC50-BD-CURSEUR     PIC X(01).                        03380000
      *                               CODE TRAITEMENT BDCLI (MAJ CRE)   03390000
          05 COMM-BC50-BD-CODETR      PIC X(03).                        03400000
      *                               CODE TRAITEMENT (MAJ CRE)         03410000
      *                               'ARF' POUR DEBRANCEMENT B2B       03420000
          05 COMM-BC50-CODETR         PIC X(03).                        03430000
      *                               CODE NSOC DU CICS                 03440000
          05 COMM-BC50-NSOCCICS       PIC X(03).                        03450000
      *                               TYPE DE L'ADRESSE A/B/C           03460000
          05 COMM-BC50-ADRESSE        PIC X(01).                        03470000
      *                               LIBELLE TYPE DE L'ADRESSE A/B/C   03480000
          05 COMM-BC50-LADRESSE       PIC X(10).                        03490000
      *                               CONTROLE EXISTANCE COMMUNE        03500000
          05 COMM-BC50-COMN-ERREUR    PIC X(01).                        03510000
      *                               AFICHAGE COMMUNE SUR L'ECRAN      03520000
          05 COMM-BC50-COMN-MODIF     PIC X(01).                        03530000
      *                               COMMUNE FORCEE                    03540000
          05 COMM-BC50-COMN-FORCEE    PIC X(01).                        03550000
      *                               MODE DEGRADE (O/N)                03560000
          05 COMM-BC50-DEGRADE        PIC X(01).                        03570000
      *                                                                 03580000
      *                                                                 03590000
      *      ZONES TRAITEMENT TRANSACTION BC60                   15     03600000
      *                                                                 03610000
      *                               CONTROLE EXISTENCE COMMUNE        03620000
          05 COMM-BC60-COMN-ERREUR    PIC X(01).                        03630000
      *                               COMMUNE FORCEE                    03640000
          05 COMM-BC60-COMN-FORCEE    PIC X(01).                        03650000
          05 COMM-BC60-VOIE           PIC X(01).                        03660000
          05 COMM-BC60-TEL            PIC X(01).                        03670000
          05 COMM-BC60-DNAISS         PIC X(01).                        03680000
      *                               CONTROLE EXISTENCE DE LA RUE      03690000
          05 COMM-BC60-CTL-CILOT      PIC X.                            03700000
             88 COMM-BC60-CTL-EFFECTUE         VALUE 'O'.               03710000
             88 COMM-BC60-CTL-PAS-EFFECTUE     VALUE SPACE.             03720000
      *                               SAISIE FORCEE DE LA VOIE          03730000
          05 COMM-BC60-SAISIE-FORCEE  PIC X.                            03740000
             88 COMM-BC60-PF12-ACTIVEE         VALUE 'O'.               03750000
             88 COMM-BC60-PF12-NON-ACTIVEE     VALUE SPACE.             03760000
          05 COMM-BC60-HISTO-PF12     PIC X.                            03770000
             88 COMM-BC60-PF12-EFFECTUE        VALUE 'O'.               03780000
             88 COMM-BC60-PF12-PAS-EFFECTUE    VALUE SPACE.             03790000
          05 COMM-BC60-ERREUR-CTL-ADR PIC X.                            03800000
             88 COMM-BC60-ERREUR-CTL           VALUE 'O'.               03810000
             88 COMM-BC60-PAS-ERREUR-CTL       VALUE SPACE.             03820000
      *                               MODE DEGRADE (O/N)                03830000
          05 COMM-BC60-DEGRADE        PIC X(01).                        03840000
      *                               MAJ CLIENT VENU GV00 (O/N)        03850000
          05 COMM-BC50-MAJNCLI        PIC X(01).                        03860000
      *                               CP EXISTE DANS POMAD (O/' ')      03870000
          05 COMM-BC50-POMAD          PIC X(01).                        03880000
      *                               CREATION GROUPE CLIENT            03890000
          05 COMM-BC60-CREGRP         PIC X(01).                        03900000
      *                               CREATION GROUPE CLIENT            03910000
          05 COMM-BC50-ACID           PIC X(08).                        03920000
      *   SSTABLE BDCLI SANS ATTANDRE REPONSE MQ                        03930000
          05 COMM-BC50-BD-REPONSE     PIC X(01).                        03940000
      * DEBRANCHEMENT AUTOMATIQUE DE RECHERCHE VOIE                     03950000
          05 COMM-BC60-DEBRANCH-TBC64 PIC X.                            03960000
             88 COMM-BC50-OUI-AUTOMATIQUE         VALUE 'O'.            03970000
             88 COMM-BC50-NON-AUTOMATIQUE         VALUE SPACE.          03980000
      * AUTORISER FORCAGE COMMUNE                                       03990000
          05 COMM-BC60-FORCAGE-COMM  PIC X.                             04000000
             88 COMM-BC50-PAS-FORCAGE     VALUE 'O'.                    04010000
             88 COMM-BC50-FORCAGE         VALUE SPACE.                  04020000
      * PROTECTION PAYS SI LIGNE UNIQUE                                 04030000
          05 COMM-BC60-LIGNES-PAYS   PIC X.                             04040000
             88 COMM-BC50-PROT-PAYS       VALUE 'O'.                    04050000
             88 COMM-BC50-NO-PROT-PAYS    VALUE SPACE.                  04060000
      * FORCAGE DONNER LIEU NAISSANCE                                   04070000
          05 COMM-BC60-LNAISS-FORCEE PIC X.                             04080000
             88 COMM-BC50-LNAISS-FORC     VALUE 'F'.                    04090000
             88 COMM-BC50-LNAISS-MAJ      VALUE 'M'.                    04100000
             88 COMM-BC50-NO-LNAISS-FORC  VALUE SPACE.                  04110000
080812*      ZONES SUPPRESSION UCM LIEU NAISSANCE                       04120000
          05 COMM-BC60-INFO-NAISSANCE.                                  04130000
             10 COMM-BC60-ECPNAISS       PIC X(05).                     04140000
             10 COMM-BC60-ELNAISS        PIC X(32).                     04150000
      *      ZONES POUR LA REDEVACE                                     04160000
          05 COMM-BC60-CPNAISS        PIC X(05).                        04170000
          05 COMM-BC60-CLNAISS        PIC X(32).                        04180000
          05 COMM-BC60-POMAD-LN       PIC X(01).                        04190000
JC    * RENDRE LA SAISIE CARTE FACULTATIVE                              04200000
JC        05 COMM-BC60-CARTE         PIC X.                             04210000
JC           88 COMM-BC50-CARTE-OBL       VALUE ' '.                    04220000
JC           88 COMM-BC50-CARTE-FAC       VALUE '1'.                    04230000
JC    * PRESENCE OU NON DU N� DE CARTE CLIENT EN BASE                   04240000
JC        05 COMM-BC60-PCARTE         PIC X.                            04250000
JC           88 COMM-BC50-PAS-DE-CARTE    VALUE ' '.                    04260000
JC           88 COMM-BC50-PRESENCE-CARTE  VALUE '1'.                    04270000
JC    * PRESENCE OU NON DE LA DATE DE NAISSANCE CLIENT EN BASE          04280000
JC        05 COMM-BC60-PDNAISS        PIC X.                            04290000
JC           88 COMM-BC50-PAS-DNAISS      VALUE ' '.                    04300000
JC           88 COMM-BC50-PRESENCE-DNAISS VALUE '1'.                    04310000
JC        05 COMM-BC60-NCARTE-S       PIC X(09).                        04320000
JC        05 COMM-BC60-FCOURR         PIC X.                            04330000
JC           88 COMM-BC50-FCOURR          VALUE ' '.                    04340000
JC           88 COMM-BC50-FCOURR-MAJ      VALUE 'M'.                    04350000
PM        05 COMM-BC50-DROIT-S.                                         04360000
PM           10 COMM-BC50-CARTE-SUP   PIC X.                            04370000
PM           10 COMM-BC50-DROIT-AUT   PIC X(9).                         04380000
      *                               ACCES BDB2B VALEUR '' OU 'B'      04390000
JC0807    05 COMM-BC50-BD-B2B         PIC X(01).                        04400000
LY1207    05 COMM-BC50-B2B            PIC X(01).                        04410000
      *       FLAG PRESENCE CARTE                                       04420000
          05 COMM-BC60-CARTE-PRES     PIC X(01).                        04430000
          05 COMM-BC60-IDCLIENT       PIC X(08).                        04440000
          05 COMM-BC60-IDQUESTION     PIC X(05).                        04450000
          05 COMM-BC60-SOURCE         PIC X(05).                        04460000
JC0807*   05 COMM-BC50-LIBRE          PIC X(018).                       04470001
INTER     05 COMM-BC50-INTERFIL       PIC X(001).                       04471001
INTER     05 COMM-BC50-LIBRE          PIC X(017).                       04472001
      *                                                                 04480000
          05 COMM-BC50-REDEFINES      PIC X(160).                       04490000
      *                                                                 04500000
      ***    ZONES TRAITEMENT TRANSACTION BC62                   74     04510000
          05 COMM-BC62-APPLI  REDEFINES COMM-BC50-REDEFINES.            04520000
               10 COMM-BC62-CINSEE         PIC X(05).                   04530000
               10 COMM-BC62-LCOMN          PIC X(32).                   04540000
               10 COMM-BC62-CPOST          PIC X(05).                   04550000
               10 COMM-BC62-LBUREAU        PIC X(26).                   04560000
               10 COMM-BC62-NPAGE          PIC 999.                     04570000
               10 COMM-BC62-NBRPAG         PIC 999.                     04580000
      * TYPE DE LIEU DE NAISSANCE LIEU HABITATION / NAISSANCE           04590000
               10 COMM-BC62-TYPE-CP       PIC X.                        04600000
                  88 COMM-BC62-CP-HABITA       VALUE SPACE.             04610000
                  88 COMM-BC62-CP-NAISS        VALUE 'O'.               04620000
               10 COMM-BC62-POMAD          PIC X(01).                   04630000
               10 COMM-BC62-FILLER         PIC X(84).                   04640000
      ****          ZONES APPLICATIVES BC64 - SELECTION ADRESSES *** 14504650000
          05 COMM-BC64-APPLI  REDEFINES COMM-BC50-REDEFINES.            04660000
               10 COMM-BC64-CINSEE          PIC X(05).                  04670000
               10 COMM-BC64-MAX-PAGE        PIC 9(2).                   04680000
               10 COMM-BC64-NB-PAGE         PIC 9(2).                   04690000
               10 COMM-BC64-NO-PAGE         PIC 9(2).                   04700000
               10 COMM-BC64-WCRITERE        PIC X(41).                  04710000
      *-        NOM DE LA VOIE DEMANDE EN RECHERCHE                     04720000
               10 COMM-BC64-LNOMPVOIE       PIC X(41).                  04730000
      *-        NOM ET TYPE DE LA VOIE DE LA DERNIERE RECHERCHE         04740000
               10 COMM-BC64-CTVOIE-DEB      PIC X(05).                  04750000
               10 COMM-BC64-LNOMVOIE-DEB    PIC X(21).                  04760000
               10 COMM-BC64-SEL-CTVOIE      PIC X(05).                  04770000
               10 COMM-BC64-SEL-LNOMVOIE    PIC X(21).                  04780000
               10 COMM-BC64-SAIS-ERREUR     PIC X(01).                  04790000
               10 COMM-BC64-FILLER          PIC X(14).                  04800000
      *                                                                 04810000
                                                                                
