      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHN01   EHN01                                              00000020
      ***************************************************************** 00000030
       01   EHN01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMPGML  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOMPGML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMPGMF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNOMPGMI  PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCTIERSI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLNOMI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MWTYPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWTYPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWTYPI    PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFOUNCGL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCFOUNCGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCFOUNCGF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFOUNCGI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOUNCGL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLFOUNCGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLFOUNCGF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFOUNCGI      PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIESAVL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCTIESAVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTIESAVF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCTIESAVI      PIC X(4).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIESAVL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLTIESAVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTIESAVF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLTIESAVI      PIC X(25).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDESTL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTYPDESTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPDESTF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTYPDESTI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR1L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLADR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR1F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLADR1I   PIC X(32).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR2L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLADR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR2F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLADR2I   PIC X(32).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCPOSTALI      PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLCOMMUNL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLCOMMUNF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLCOMMUNI      PIC X(26).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPAYSL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPAYSF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCPAYSI   PIC X(2).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAYSL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLPAYSF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLPAYSI   PIC X(20).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTELL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNTELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNTELF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNTELI    PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAXL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MNFAXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAXF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MNFAXI    PIC X(15).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERLL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLINTERLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLINTERLF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLINTERLI      PIC X(20).                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPTRAL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCTYPTRAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPTRAF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCTYPTRAI      PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSPL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MLTRANSPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTRANSPF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLTRANSPI      PIC X(20).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLIBERRI  PIC X(78).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUPPRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLSUPPRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLSUPPRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLSUPPRI  PIC X(27).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCODTRAI  PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCICSI    PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * SDF: EHN01   EHN01                                              00001190
      ***************************************************************** 00001200
       01   EHN01O REDEFINES EHN01I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNOMPGMA  PIC X.                                          00001380
           02 MNOMPGMC  PIC X.                                          00001390
           02 MNOMPGMP  PIC X.                                          00001400
           02 MNOMPGMH  PIC X.                                          00001410
           02 MNOMPGMV  PIC X.                                          00001420
           02 MNOMPGMO  PIC X(5).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MCTIERSA  PIC X.                                          00001450
           02 MCTIERSC  PIC X.                                          00001460
           02 MCTIERSP  PIC X.                                          00001470
           02 MCTIERSH  PIC X.                                          00001480
           02 MCTIERSV  PIC X.                                          00001490
           02 MCTIERSO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLNOMA    PIC X.                                          00001520
           02 MLNOMC    PIC X.                                          00001530
           02 MLNOMP    PIC X.                                          00001540
           02 MLNOMH    PIC X.                                          00001550
           02 MLNOMV    PIC X.                                          00001560
           02 MLNOMO    PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MWTYPA    PIC X.                                          00001590
           02 MWTYPC    PIC X.                                          00001600
           02 MWTYPP    PIC X.                                          00001610
           02 MWTYPH    PIC X.                                          00001620
           02 MWTYPV    PIC X.                                          00001630
           02 MWTYPO    PIC X.                                          00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCFOUNCGA      PIC X.                                     00001660
           02 MCFOUNCGC PIC X.                                          00001670
           02 MCFOUNCGP PIC X.                                          00001680
           02 MCFOUNCGH PIC X.                                          00001690
           02 MCFOUNCGV PIC X.                                          00001700
           02 MCFOUNCGO      PIC 9(5).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLFOUNCGA      PIC X.                                     00001730
           02 MLFOUNCGC PIC X.                                          00001740
           02 MLFOUNCGP PIC X.                                          00001750
           02 MLFOUNCGH PIC X.                                          00001760
           02 MLFOUNCGV PIC X.                                          00001770
           02 MLFOUNCGO      PIC X(20).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCTIESAVA      PIC X.                                     00001800
           02 MCTIESAVC PIC X.                                          00001810
           02 MCTIESAVP PIC X.                                          00001820
           02 MCTIESAVH PIC X.                                          00001830
           02 MCTIESAVV PIC X.                                          00001840
           02 MCTIESAVO      PIC X(4).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLTIESAVA      PIC X.                                     00001870
           02 MLTIESAVC PIC X.                                          00001880
           02 MLTIESAVP PIC X.                                          00001890
           02 MLTIESAVH PIC X.                                          00001900
           02 MLTIESAVV PIC X.                                          00001910
           02 MLTIESAVO      PIC X(25).                                 00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MTYPDESTA      PIC X.                                     00001940
           02 MTYPDESTC PIC X.                                          00001950
           02 MTYPDESTP PIC X.                                          00001960
           02 MTYPDESTH PIC X.                                          00001970
           02 MTYPDESTV PIC X.                                          00001980
           02 MTYPDESTO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLADR1A   PIC X.                                          00002010
           02 MLADR1C   PIC X.                                          00002020
           02 MLADR1P   PIC X.                                          00002030
           02 MLADR1H   PIC X.                                          00002040
           02 MLADR1V   PIC X.                                          00002050
           02 MLADR1O   PIC X(32).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLADR2A   PIC X.                                          00002080
           02 MLADR2C   PIC X.                                          00002090
           02 MLADR2P   PIC X.                                          00002100
           02 MLADR2H   PIC X.                                          00002110
           02 MLADR2V   PIC X.                                          00002120
           02 MLADR2O   PIC X(32).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCPOSTALA      PIC X.                                     00002150
           02 MCPOSTALC PIC X.                                          00002160
           02 MCPOSTALP PIC X.                                          00002170
           02 MCPOSTALH PIC X.                                          00002180
           02 MCPOSTALV PIC X.                                          00002190
           02 MCPOSTALO      PIC X(5).                                  00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLCOMMUNA      PIC X.                                     00002220
           02 MLCOMMUNC PIC X.                                          00002230
           02 MLCOMMUNP PIC X.                                          00002240
           02 MLCOMMUNH PIC X.                                          00002250
           02 MLCOMMUNV PIC X.                                          00002260
           02 MLCOMMUNO      PIC X(26).                                 00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCPAYSA   PIC X.                                          00002290
           02 MCPAYSC   PIC X.                                          00002300
           02 MCPAYSP   PIC X.                                          00002310
           02 MCPAYSH   PIC X.                                          00002320
           02 MCPAYSV   PIC X.                                          00002330
           02 MCPAYSO   PIC X(2).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLPAYSA   PIC X.                                          00002360
           02 MLPAYSC   PIC X.                                          00002370
           02 MLPAYSP   PIC X.                                          00002380
           02 MLPAYSH   PIC X.                                          00002390
           02 MLPAYSV   PIC X.                                          00002400
           02 MLPAYSO   PIC X(20).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MNTELA    PIC X.                                          00002430
           02 MNTELC    PIC X.                                          00002440
           02 MNTELP    PIC X.                                          00002450
           02 MNTELH    PIC X.                                          00002460
           02 MNTELV    PIC X.                                          00002470
           02 MNTELO    PIC X(8).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MNFAXA    PIC X.                                          00002500
           02 MNFAXC    PIC X.                                          00002510
           02 MNFAXP    PIC X.                                          00002520
           02 MNFAXH    PIC X.                                          00002530
           02 MNFAXV    PIC X.                                          00002540
           02 MNFAXO    PIC X(15).                                      00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLINTERLA      PIC X.                                     00002570
           02 MLINTERLC PIC X.                                          00002580
           02 MLINTERLP PIC X.                                          00002590
           02 MLINTERLH PIC X.                                          00002600
           02 MLINTERLV PIC X.                                          00002610
           02 MLINTERLO      PIC X(20).                                 00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MCTYPTRAA      PIC X.                                     00002640
           02 MCTYPTRAC PIC X.                                          00002650
           02 MCTYPTRAP PIC X.                                          00002660
           02 MCTYPTRAH PIC X.                                          00002670
           02 MCTYPTRAV PIC X.                                          00002680
           02 MCTYPTRAO      PIC X(5).                                  00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MLTRANSPA      PIC X.                                     00002710
           02 MLTRANSPC PIC X.                                          00002720
           02 MLTRANSPP PIC X.                                          00002730
           02 MLTRANSPH PIC X.                                          00002740
           02 MLTRANSPV PIC X.                                          00002750
           02 MLTRANSPO      PIC X(20).                                 00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MLIBERRA  PIC X.                                          00002780
           02 MLIBERRC  PIC X.                                          00002790
           02 MLIBERRP  PIC X.                                          00002800
           02 MLIBERRH  PIC X.                                          00002810
           02 MLIBERRV  PIC X.                                          00002820
           02 MLIBERRO  PIC X(78).                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLSUPPRA  PIC X.                                          00002850
           02 MLSUPPRC  PIC X.                                          00002860
           02 MLSUPPRP  PIC X.                                          00002870
           02 MLSUPPRH  PIC X.                                          00002880
           02 MLSUPPRV  PIC X.                                          00002890
           02 MLSUPPRO  PIC X(27).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCODTRAA  PIC X.                                          00002920
           02 MCODTRAC  PIC X.                                          00002930
           02 MCODTRAP  PIC X.                                          00002940
           02 MCODTRAH  PIC X.                                          00002950
           02 MCODTRAV  PIC X.                                          00002960
           02 MCODTRAO  PIC X(4).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCICSA    PIC X.                                          00002990
           02 MCICSC    PIC X.                                          00003000
           02 MCICSP    PIC X.                                          00003010
           02 MCICSH    PIC X.                                          00003020
           02 MCICSV    PIC X.                                          00003030
           02 MCICSO    PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNETNAMA  PIC X.                                          00003060
           02 MNETNAMC  PIC X.                                          00003070
           02 MNETNAMP  PIC X.                                          00003080
           02 MNETNAMH  PIC X.                                          00003090
           02 MNETNAMV  PIC X.                                          00003100
           02 MNETNAMO  PIC X(8).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSCREENA  PIC X.                                          00003130
           02 MSCREENC  PIC X.                                          00003140
           02 MSCREENP  PIC X.                                          00003150
           02 MSCREENH  PIC X.                                          00003160
           02 MSCREENV  PIC X.                                          00003170
           02 MSCREENO  PIC X(4).                                       00003180
                                                                                
