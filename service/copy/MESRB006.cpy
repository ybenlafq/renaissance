      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ R�PONSE si vente prise par la caisse                  
      *   verS si REDBOX.                                                       
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESRB006-DATA.                                                        
      *                                                                         
      * R�PONSE                                                                 
      *                                                                         
      *   N� DE TRANSACTION REDBOX                                              
          05 MESRB06-NTRRBX        PIC    X(10).                                
      *   CODE SOCI�T� VENTE "GV"                                               
          05 MESRB06-NSOC          PIC    X(03).                                
      *   CODE LIEU VENTE "GV"                                                  
          05 MESRB06-NLIEU         PIC    X(03).                                
      *   N� VENTE "GV"                                                         
          05 MESRB06-NVENTE        PIC    X(07).                                
      *   code erreur msb49                                                     
          05 MESRB06-Nerreur       PIC    X(04).                                
      *   libelle erreur mbs49                                                  
          05 MESRB06-lerreur       PIC    X(58).                                
      *   filler                                                                
          05 MESRB06-filler        PIC    X(30).                                
      *                                                                         
                                                                                
