      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IDS010 AU 04/11/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IDS010.                                                        
            05 NOMETAT-IDS010           PIC X(6) VALUE 'IDS010'.                
            05 RUPTURES-IDS010.                                                 
           10 IDS010-NSOCIETE           PIC X(03).                      007  003
           10 IDS010-WSEQED             PIC S9(05)      COMP-3.         010  003
           10 IDS010-WSEQFAM            PIC S9(05)      COMP-3.         013  003
           10 IDS010-CMARQ              PIC X(05).                      016  005
           10 IDS010-NCODIC             PIC X(07).                      021  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IDS010-SEQUENCE           PIC S9(04) COMP.                028  002
      *--                                                                       
           10 IDS010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IDS010.                                                   
           10 IDS010-CFAM               PIC X(05).                      030  005
           10 IDS010-CRAYON             PIC X(05).                      035  005
           10 IDS010-LLIEU              PIC X(20).                      040  020
           10 IDS010-LREFFOURN          PIC X(20).                      060  020
           10 IDS010-LSTATUT            PIC X(03).                      080  003
           10 IDS010-NLIEUVALO          PIC X(03).                      083  003
           10 IDS010-PEXCLUS            PIC S9(09)V9(6) COMP-3.         086  008
           10 IDS010-PVALINV            PIC S9(09)V9(6) COMP-3.         094  008
           10 IDS010-QEXCLUS            PIC S9(05)      COMP-3.         102  003
           10 IDS010-QTEINV             PIC S9(05)      COMP-3.         105  003
            05 FILLER                      PIC X(405).                          
                                                                                
