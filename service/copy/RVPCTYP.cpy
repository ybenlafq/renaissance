      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PCTYP LISTE DES TYPES DE CARTES        *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPCTYP .                                                            
           05  PCTYP-CTABLEG2    PIC X(15).                                     
           05  PCTYP-CTABLEG2-REDEF REDEFINES PCTYP-CTABLEG2.                   
               10  PCTYP-CTYPE           PIC X(07).                             
           05  PCTYP-WTABLEG     PIC X(80).                                     
           05  PCTYP-WTABLEG-REDEF  REDEFINES PCTYP-WTABLEG.                    
               10  PCTYP-WACTIF          PIC X(01).                             
               10  PCTYP-TYPE            PIC X(03).                             
               10  PCTYP-LIBELLE         PIC X(49).                             
               10  PCTYP-TXTVA           PIC X(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPCTYP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PCTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PCTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PCTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
