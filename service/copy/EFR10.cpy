      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Franchise - Protection de stocks                                00000020
      ***************************************************************** 00000030
       01   EFR10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNCODICI  PIC X(7).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFDECLIL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MFDECLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFDECLIF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MFDECLII  PIC X.                                          00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMIJJL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDEMIJJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEMIJJF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDEMIJJI  PIC X(2).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMIMML  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MDEMIMML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEMIMMF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MDEMIMMI  PIC X(2).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISAL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MDEMISAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEMISAF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDEMISAI  PIC X(4).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPSL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MNUMPSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUMPSF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MNUMPSI   PIC X(10).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFEMIJJL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MFEMIJJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFEMIJJF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MFEMIJJI  PIC X(2).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFEMIMML  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MFEMIMML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFEMIMMF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MFEMIMMI  PIC X(2).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFEMISAL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MFEMISAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFEMISAF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MFEMISAI  PIC X(4).                                       00000590
           02 MTABI OCCURS   10 TIMES .                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSL    COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MNSL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MNSF    PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MNSI    PIC X(10).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MDATEI  PIC X(5).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFRANL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MFRANL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFRANF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MFRANI  PIC X.                                          00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MCODICI      PIC X(7).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MFAMI   PIC X(5).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQL  COMP PIC S9(4).                                 00000810
      *--                                                                       
             03 MMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMARQF  PIC X.                                          00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MMARQI  PIC X(5).                                       00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000850
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MREFI   PIC X(20).                                      00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTOCKL      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MSTOCKL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTOCKF      PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MSTOCKI      PIC X(5).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNTL   COMP PIC S9(4).                                 00000930
      *--                                                                       
             03 MMNTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMNTF   PIC X.                                          00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MMNTI   PIC X(8).                                       00000960
      * ZONE CMD AIDA                                                   00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBERRI  PIC X(79).                                      00001010
      * CODE TRANSACTION                                                00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      * CICS DE TRAVAIL                                                 00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MCICSI    PIC X(5).                                       00001110
      * NETNAME                                                         00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MNETNAMI  PIC X(8).                                       00001160
      * CODE TERMINAL                                                   00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MSCREENI  PIC X(4).                                       00001210
      ***************************************************************** 00001220
      * Franchise - Protection de stocks                                00001230
      ***************************************************************** 00001240
       01   EFR10O REDEFINES EFR10I.                                    00001250
           02 FILLER    PIC X(12).                                      00001260
      * DATE DU JOUR                                                    00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
      * HEURE                                                           00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MTIMJOUA  PIC X.                                          00001370
           02 MTIMJOUC  PIC X.                                          00001380
           02 MTIMJOUP  PIC X.                                          00001390
           02 MTIMJOUH  PIC X.                                          00001400
           02 MTIMJOUV  PIC X.                                          00001410
           02 MTIMJOUO  PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MPAGEA    PIC X.                                          00001440
           02 MPAGEC    PIC X.                                          00001450
           02 MPAGEP    PIC X.                                          00001460
           02 MPAGEH    PIC X.                                          00001470
           02 MPAGEV    PIC X.                                          00001480
           02 MPAGEO    PIC Z9.                                         00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNBPA     PIC X.                                          00001510
           02 MNBPC     PIC X.                                          00001520
           02 MNBPP     PIC X.                                          00001530
           02 MNBPH     PIC X.                                          00001540
           02 MNBPV     PIC X.                                          00001550
           02 MNBPO     PIC Z9.                                         00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNCODICA  PIC X.                                          00001580
           02 MNCODICC  PIC X.                                          00001590
           02 MNCODICP  PIC X.                                          00001600
           02 MNCODICH  PIC X.                                          00001610
           02 MNCODICV  PIC X.                                          00001620
           02 MNCODICO  PIC X(7).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MFDECLIA  PIC X.                                          00001650
           02 MFDECLIC  PIC X.                                          00001660
           02 MFDECLIP  PIC X.                                          00001670
           02 MFDECLIH  PIC X.                                          00001680
           02 MFDECLIV  PIC X.                                          00001690
           02 MFDECLIO  PIC X.                                          00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MDEMIJJA  PIC X.                                          00001720
           02 MDEMIJJC  PIC X.                                          00001730
           02 MDEMIJJP  PIC X.                                          00001740
           02 MDEMIJJH  PIC X.                                          00001750
           02 MDEMIJJV  PIC X.                                          00001760
           02 MDEMIJJO  PIC X(2).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MDEMIMMA  PIC X.                                          00001790
           02 MDEMIMMC  PIC X.                                          00001800
           02 MDEMIMMP  PIC X.                                          00001810
           02 MDEMIMMH  PIC X.                                          00001820
           02 MDEMIMMV  PIC X.                                          00001830
           02 MDEMIMMO  PIC X(2).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MDEMISAA  PIC X.                                          00001860
           02 MDEMISAC  PIC X.                                          00001870
           02 MDEMISAP  PIC X.                                          00001880
           02 MDEMISAH  PIC X.                                          00001890
           02 MDEMISAV  PIC X.                                          00001900
           02 MDEMISAO  PIC X(4).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNUMPSA   PIC X.                                          00001930
           02 MNUMPSC   PIC X.                                          00001940
           02 MNUMPSP   PIC X.                                          00001950
           02 MNUMPSH   PIC X.                                          00001960
           02 MNUMPSV   PIC X.                                          00001970
           02 MNUMPSO   PIC X(10).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MFEMIJJA  PIC X.                                          00002000
           02 MFEMIJJC  PIC X.                                          00002010
           02 MFEMIJJP  PIC X.                                          00002020
           02 MFEMIJJH  PIC X.                                          00002030
           02 MFEMIJJV  PIC X.                                          00002040
           02 MFEMIJJO  PIC X(2).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MFEMIMMA  PIC X.                                          00002070
           02 MFEMIMMC  PIC X.                                          00002080
           02 MFEMIMMP  PIC X.                                          00002090
           02 MFEMIMMH  PIC X.                                          00002100
           02 MFEMIMMV  PIC X.                                          00002110
           02 MFEMIMMO  PIC X(2).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MFEMISAA  PIC X.                                          00002140
           02 MFEMISAC  PIC X.                                          00002150
           02 MFEMISAP  PIC X.                                          00002160
           02 MFEMISAH  PIC X.                                          00002170
           02 MFEMISAV  PIC X.                                          00002180
           02 MFEMISAO  PIC X(4).                                       00002190
           02 MTABO OCCURS   10 TIMES .                                 00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MNSA    PIC X.                                          00002220
             03 MNSC    PIC X.                                          00002230
             03 MNSP    PIC X.                                          00002240
             03 MNSH    PIC X.                                          00002250
             03 MNSV    PIC X.                                          00002260
             03 MNSO    PIC X(10).                                      00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MDATEA  PIC X.                                          00002290
             03 MDATEC  PIC X.                                          00002300
             03 MDATEP  PIC X.                                          00002310
             03 MDATEH  PIC X.                                          00002320
             03 MDATEV  PIC X.                                          00002330
             03 MDATEO  PIC X(5).                                       00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MFRANA  PIC X.                                          00002360
             03 MFRANC  PIC X.                                          00002370
             03 MFRANP  PIC X.                                          00002380
             03 MFRANH  PIC X.                                          00002390
             03 MFRANV  PIC X.                                          00002400
             03 MFRANO  PIC X.                                          00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MCODICA      PIC X.                                     00002430
             03 MCODICC PIC X.                                          00002440
             03 MCODICP PIC X.                                          00002450
             03 MCODICH PIC X.                                          00002460
             03 MCODICV PIC X.                                          00002470
             03 MCODICO      PIC X(7).                                  00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MFAMA   PIC X.                                          00002500
             03 MFAMC   PIC X.                                          00002510
             03 MFAMP   PIC X.                                          00002520
             03 MFAMH   PIC X.                                          00002530
             03 MFAMV   PIC X.                                          00002540
             03 MFAMO   PIC X(5).                                       00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MMARQA  PIC X.                                          00002570
             03 MMARQC  PIC X.                                          00002580
             03 MMARQP  PIC X.                                          00002590
             03 MMARQH  PIC X.                                          00002600
             03 MMARQV  PIC X.                                          00002610
             03 MMARQO  PIC X(5).                                       00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MREFA   PIC X.                                          00002640
             03 MREFC   PIC X.                                          00002650
             03 MREFP   PIC X.                                          00002660
             03 MREFH   PIC X.                                          00002670
             03 MREFV   PIC X.                                          00002680
             03 MREFO   PIC X(20).                                      00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MSTOCKA      PIC X.                                     00002710
             03 MSTOCKC PIC X.                                          00002720
             03 MSTOCKP PIC X.                                          00002730
             03 MSTOCKH PIC X.                                          00002740
             03 MSTOCKV PIC X.                                          00002750
             03 MSTOCKO      PIC X(5).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MMNTA   PIC X.                                          00002780
             03 MMNTC   PIC X.                                          00002790
             03 MMNTP   PIC X.                                          00002800
             03 MMNTH   PIC X.                                          00002810
             03 MMNTV   PIC X.                                          00002820
             03 MMNTO   PIC X(8).                                       00002830
      * ZONE CMD AIDA                                                   00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLIBERRA  PIC X.                                          00002860
           02 MLIBERRC  PIC X.                                          00002870
           02 MLIBERRP  PIC X.                                          00002880
           02 MLIBERRH  PIC X.                                          00002890
           02 MLIBERRV  PIC X.                                          00002900
           02 MLIBERRO  PIC X(79).                                      00002910
      * CODE TRANSACTION                                                00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
      * CICS DE TRAVAIL                                                 00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MCICSA    PIC X.                                          00003020
           02 MCICSC    PIC X.                                          00003030
           02 MCICSP    PIC X.                                          00003040
           02 MCICSH    PIC X.                                          00003050
           02 MCICSV    PIC X.                                          00003060
           02 MCICSO    PIC X(5).                                       00003070
      * NETNAME                                                         00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MNETNAMA  PIC X.                                          00003100
           02 MNETNAMC  PIC X.                                          00003110
           02 MNETNAMP  PIC X.                                          00003120
           02 MNETNAMH  PIC X.                                          00003130
           02 MNETNAMV  PIC X.                                          00003140
           02 MNETNAMO  PIC X(8).                                       00003150
      * CODE TERMINAL                                                   00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MSCREENA  PIC X.                                          00003180
           02 MSCREENC  PIC X.                                          00003190
           02 MSCREENP  PIC X.                                          00003200
           02 MSCREENH  PIC X.                                          00003210
           02 MSCREENV  PIC X.                                          00003220
           02 MSCREENO  PIC X(4).                                       00003230
                                                                                
