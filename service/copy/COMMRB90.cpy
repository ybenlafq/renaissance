      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TRB90                    TR: RB90  *    00002209
      * DARTY BOX : SAISIE DES PARAMETRES D'ELIGIBILITE A LA THD   *    00002309
      **************************************************************            
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +5372            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *--------------------------------- 4096 CARAC. ---------                  
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-RB90-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-RB90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-FILLER     PIC X(14).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                                
      *                                                                         
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-RB90-ENTETE.                                         00000500
              03 COMM-ENTETE.                                           00000510
                 05 COMM-CODLANG            PIC X(02).                  00000520
                 05 COMM-CODPIC             PIC X(02).                  00000530
                 05 COMM-CODESFONCTION  .                               00000540
                    10 COMM-CODE-FONCTION      OCCURS 3.                   00000
                       15  COMM-CFONC       PIC X(03).                          
                 05 COMM-CFONCTION-OPTION   PIC X(03).                          
                 05 COMM-COPTION            PIC X(02).                          
                 05 COMM-ACID               PIC X(08).                  00000520
      *                                                                         
      *****************************************************************         
      * ZONES RESERVEES APPLICATIVES ---------------------4000--- 3724*         
      *            TRANSACTION RB90                                   *         
      *****************************************************************         
      *                                                                         
      *---------- ZONE DONNEES TRB90 --------------                             
          02 COMM-RB90-APPLI .                                                  
      *---------- CRITERES DE SELECTION ----------- (40 CARAC.)                 
            05 COMM-RB90-DONNEES-TRB90.                                         
             10 COMM-RB90-NCONTREDBOX             PIC X(18).                    
             10 COMM-RB90-NVENTENCG               PIC X(13).                    
             10 COMM-RB90-CSST                    PIC X(20).                    
             10 COMM-RB90-CTYPINTER               PIC X(05).                    
             10 COMM-RB90-NINTER                  PIC X(20).                    
             10 COMM-RB90-DATENASC1               PIC X(10).                    
             10 COMM-RB90-DATENASC2               PIC X(10).                    
             10 COMM-RB90-CSTATUT                 PIC X(05).                    
            05 COMM-RB90-DONNEES-TRB901.                                        
             10 COMM-RB90-NINTERP                 PIC X(20).                    
             10 COMM-RB90-DATENASCP               PIC X(08).                    
             10 COMM-RB90-NINTERS                 PIC X(20).                    
             10 COMM-RB90-DATENASCS               PIC X(08).                    
            05 COMM-RB90-DONNEES-TRB902 OCCURS 5.                               
             10 COMM-RB90-MONTANT                 PIC X(10).                    
             10 COMM-RB90-STATUT                  PIC X(05).                    
             10 COMM-RB90-VENTEL                  PIC X(13).                    
      *---------- ZONES GESTION ECRAN ------------- (6 CARAC.)                  
            05 COMM-RB90-ERB90.                                                 
      *---------- NUMERO DE PAGE COURANTE ---------                             
             10 COMM-RB90-NPAGE     PIC 9(03).                                  
      *---------- NUMERO DE PAGE MAXI -------------                             
             10 COMM-RB90-NPAGE-MAX PIC 9(03).                                  
      *                                                                         
      *----- ZONE MESSAGE D'ERREUR POUR -----------                             
      *----- LA TRANSACT 'APPELANTE' -80- ---------                             
          05 COMM-RB90-MLIBERR      PIC X(80).                                  
      *                                                                         
          05 COMM-RB90-TS-MODIFICATION   PIC X VALUE SPACE.                     
             88 COMM-RB90-MODIF-TS             VALUE '1'.                       
             88 COMM-RB90-NON-MODIF-TS         VALUE '0'.                       
      *                                                                         
          05 COMM-RB90-FILLER       PIC X(15).                                  
      *****************************************************************         
                                                                                
