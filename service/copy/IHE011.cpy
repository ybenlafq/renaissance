      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE011 AU 31/01/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,07,BI,A,                          *        
      *                           24,08,BI,A,                          *        
      *                           32,03,BI,A,                          *        
      *                           35,07,BI,A,                          *        
      *                           42,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE011.                                                        
            05 NOMETAT-IHE011           PIC X(6) VALUE 'IHE011'.                
            05 RUPTURES-IHE011.                                                 
           10 IHE011-CMARQ              PIC X(05).                      007  005
           10 IHE011-CFAM               PIC X(05).                      012  005
           10 IHE011-NCODIC             PIC X(07).                      017  007
           10 IHE011-DDEPOT             PIC X(08).                      024  008
           10 IHE011-NLIEUHED           PIC X(03).                      032  003
           10 IHE011-NGHE               PIC X(07).                      035  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE011-SEQUENCE           PIC S9(04) COMP.                042  002
      *--                                                                       
           10 IHE011-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE011.                                                   
           10 IHE011-CLIEUHET           PIC X(05).                      044  005
           10 IHE011-CTRAIT             PIC X(05).                      049  005
           10 IHE011-LREFFOURN          PIC X(20).                      054  020
           10 IHE011-WQTE               PIC S9(05)      COMP-3.         074  003
           10 IHE011-WVALEUR            PIC S9(07)V9(2) COMP-3.         077  005
            05 FILLER                      PIC X(431).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE011-LONG           PIC S9(4)   COMP  VALUE +081.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE011-LONG           PIC S9(4) COMP-5  VALUE +081.           
                                                                                
      *}                                                                        
