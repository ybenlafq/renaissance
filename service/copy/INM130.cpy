      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM130 AU 24/07/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,08,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM130.                                                        
            05 NOMETAT-INM130           PIC X(6) VALUE 'INM130'.                
            05 RUPTURES-INM130.                                                 
           10 INM130-NSOC               PIC X(03).                      007  003
           10 INM130-NSAV               PIC X(03).                      010  003
           10 INM130-DATETR             PIC X(08).                      013  008
           10 INM130-NLIEU              PIC X(03).                      021  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM130-SEQUENCE           PIC S9(04) COMP.                024  002
      *--                                                                       
           10 INM130-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM130.                                                   
           10 INM130-CDEVRF             PIC X(03).                      026  003
           10 INM130-LDEVAUT            PIC X(05).                      029  005
           10 INM130-LDEVRF             PIC X(05).                      034  005
           10 INM130-NBON               PIC X(06).                      039  006
           10 INM130-NTRANS             PIC X(08).                      045  008
           10 INM130-CONV-MDVRF         PIC S9(08)V9(2) COMP-3.         053  006
           10 INM130-CONV-MONTHT        PIC S9(08)V9(2) COMP-3.         059  006
           10 INM130-CONV-MONTVA        PIC S9(08)V9(2) COMP-3.         065  006
           10 INM130-PMDVRF             PIC S9(08)V9(2) COMP-3.         071  006
           10 INM130-PMONTHT            PIC S9(08)V9(2) COMP-3.         077  006
           10 INM130-PMONTVA            PIC S9(08)V9(2) COMP-3.         083  006
            05 FILLER                      PIC X(424).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM130-LONG           PIC S9(4)   COMP  VALUE +088.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM130-LONG           PIC S9(4) COMP-5  VALUE +088.           
                                                                                
      *}                                                                        
