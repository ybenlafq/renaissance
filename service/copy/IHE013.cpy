      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE013 AU 22/02/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE013.                                                        
            05 NOMETAT-IHE013           PIC X(6) VALUE 'IHE013'.                
            05 RUPTURES-IHE013.                                                 
           10 IHE013-CLIEUHET           PIC X(05).                      007  005
           10 IHE013-CTRAIT             PIC X(05).                      012  005
           10 IHE013-CMARQ              PIC X(05).                      017  005
           10 IHE013-CFAM               PIC X(05).                      022  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE013-SEQUENCE           PIC S9(04) COMP.                027  002
      *--                                                                       
           10 IHE013-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE013.                                                   
           10 IHE013-DEV-ENCOURS        PIC X(03).                      029  003
           10 IHE013-DEV-EQU            PIC X(03).                      032  003
           10 IHE013-LTRAIT             PIC X(20).                      035  020
           10 IHE013-TAUX               PIC S9(01)V9(6) COMP-3.         055  004
           10 IHE013-WQTE-CFAM          PIC S9(05)     .                059  005
           10 IHE013-WVALEUR-CFAM       PIC S9(09)V9(2).                064  011
            05 FILLER                      PIC X(438).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE013-LONG           PIC S9(4)   COMP  VALUE +074.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE013-LONG           PIC S9(4) COMP-5  VALUE +074.           
                                                                                
      *}                                                                        
