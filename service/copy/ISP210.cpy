      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP210 AU 07/04/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,20,BI,A,                          *        
      *                           48,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP210.                                                        
            05 NOMETAT-ISP210           PIC X(6) VALUE 'ISP210'.                
            05 RUPTURES-ISP210.                                                 
           10 ISP210-NSOCIETE           PIC X(03).                      007  003
           10 ISP210-CHEFPROD           PIC X(05).                      010  005
           10 ISP210-CAPPRO             PIC X(05).                      015  005
           10 ISP210-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 ISP210-CMARQ              PIC X(05).                      023  005
           10 ISP210-LREFFOURN          PIC X(20).                      028  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP210-SEQUENCE           PIC S9(04) COMP.                048  002
      *--                                                                       
           10 ISP210-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP210.                                                   
           10 ISP210-CAPPRO-PAR         PIC X(05).                      050  005
           10 ISP210-CASSORT            PIC X(05).                      055  005
           10 ISP210-CFAM               PIC X(05).                      060  005
           10 ISP210-NCODIC             PIC X(07).                      065  007
           10 ISP210-STOCKDEP           PIC 9(10)     .                 072  010
           10 ISP210-STOCKMAG           PIC 9(10)     .                 082  010
            05 FILLER                      PIC X(421).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP210-LONG           PIC S9(4)   COMP  VALUE +091.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP210-LONG           PIC S9(4) COMP-5  VALUE +091.           
                                                                                
      *}                                                                        
