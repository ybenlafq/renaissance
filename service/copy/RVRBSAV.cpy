      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBSAV PROFIL SAV                       *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBSAV.                                                             
           05  RBSAV-CTABLEG2    PIC X(15).                                     
           05  RBSAV-CTABLEG2-REDEF REDEFINES RBSAV-CTABLEG2.                   
               10  RBSAV-CSAV            PIC X(06).                             
           05  RBSAV-WTABLEG     PIC X(80).                                     
           05  RBSAV-WTABLEG-REDEF  REDEFINES RBSAV-WTABLEG.                    
               10  RBSAV-CPROFIL         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBSAV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSAV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBSAV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSAV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBSAV-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
