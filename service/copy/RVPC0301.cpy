      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTPC03                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPC0301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC0301.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       EXERCICE COMPTABLE                                
           10 PC03-EXERCICE        PIC X(4).                                    
      *    *************************************************************        
      *                       PERIODE COMPTABLE                                 
           10 PC03-PERIODE         PIC X(2).                                    
      *    *************************************************************        
      *                       DATE DE FIN DE PERIODE                            
           10 PC03-DATEFIN         PIC X(8).                                    
      *    *************************************************************        
      *                       COMPTE                                            
           10 PC03-COMPTE          PIC X(6).                                    
      *    *************************************************************        
      *                       SOCIETE DE VENTE (TIERS 1:3)                      
           10 PC03-NSOCVTE         PIC X(3).                                    
      *    *************************************************************        
      *                       LIEU DE VENTE    (TIERS 4:3)                      
           10 PC03-NLIEUVTE        PIC X(3).                                    
      *    *************************************************************        
      *                       LETTRAGE (00 DATE DE VENTE)                       
           10 PC03-LETTRAGE        PIC X(10).                                   
      *    *************************************************************        
      *                       SOLDE                                             
           10 PC03-SOLDE           PIC S9(13)V9(2) USAGE COMP-3.                
      *    *************************************************************        
      *                       DATE SYSTEME M.A.J.                               
           10 PC03-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       COMPTE SAP                                        
           10 PC03-CPTSAP          PIC X(8).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPC0301-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPC0301-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *                       EXERCICE COMPTABLE                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-EXERCICE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-EXERCICE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-PERIODE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-PERIODE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-DATEFIN-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-DATEFIN-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-COMPTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-COMPTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-NSOCVTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-NSOCVTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-NLIEUVTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-NLIEUVTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-LETTRAGE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-LETTRAGE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-SOLDE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-SOLDE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC03-CPTSAP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 PC03-CPTSAP-F        PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
