      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTRB31                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3101.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3101.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NUM SERIE                                         
           10 RB31-NSERIE          PIC X(15).                                   
      *    *************************************************************        
      *                       NCODIC                                            
           10 RB31-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       TYPE MOUVEMNT                                     
           10 RB31-TMVMENT         PIC X(3).                                    
      *    *************************************************************        
      *                       DATE MVT                                          
           10 RB31-DMVMENT         PIC X(8).                                    
      *    *************************************************************        
      *                       HEURE MVT                                         
           10 RB31-HMVMENT         PIC X(6).                                    
      *    *************************************************************        
      *                       FLAG ABEL                                         
           10 RB31-WABEL           PIC X(1).                                    
      *    *************************************************************        
      *                       IDENTIFIANT MVT                                   
           10 RB31-NIDENT          PIC X(20).                                   
      *    *************************************************************        
      *                       DATE SYSTEME DE M.A.J LIGNE                       
           10 RB31-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       MONTANT FACTURE HT                                
           10 RB31-MFACTUREHT      PIC S9(05)V99 USAGE COMP-3.                  
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3101-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3101-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-NSERIE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-NSERIE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-TMVMENT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-TMVMENT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-DMVMENT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-DMVMENT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-HMVMENT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-HMVMENT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-WABEL-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-WABEL-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-NIDENT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-NIDENT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-DSYST-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-DSYST-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB31-MFACTUREHT-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 RB31-MFACTUREHT-F         PIC S9(4) COMP-5.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
