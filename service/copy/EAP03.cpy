      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - LISTE CLIENTS A2I                                        00000020
      ***************************************************************** 00000030
       01   EAP03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCLL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNOMCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOMCLF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNOMCLI   PIC X(25).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIXL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCHOIXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHOIXF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCHOIXI   PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDPOSTALL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCDPOSTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCDPOSTALF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCDPOSTALI     PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEEL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MANNEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MANNEEF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MANNEEI   PIC X(4).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMOISL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MMOISL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMOISF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MMOISI    PIC X(2).                                       00000450
           02 MLIGNEI OCCURS   5 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MACTI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMPRNML     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNMPRNML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNMPRNMF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNMPRNMI     PIC X(44).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR1L  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MADR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR1F  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MADR1I  PIC X(25).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR2L  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MADR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR2F  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MADR2I  PIC X(32).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADR3L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MADR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MADR3F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MADR3I  PIC X(30).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(79).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      * NETNAME                                                         00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNETNAMI  PIC X(8).                                       00000870
      * CODE TERMINAL                                                   00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MSCREENI  PIC X(4).                                       00000920
      ***************************************************************** 00000930
      * DA2I - LISTE CLIENTS A2I                                        00000940
      ***************************************************************** 00000950
       01   EAP03O REDEFINES EAP03I.                                    00000960
           02 FILLER    PIC X(12).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MDATJOUA  PIC X.                                          00000990
           02 MDATJOUC  PIC X.                                          00001000
           02 MDATJOUP  PIC X.                                          00001010
           02 MDATJOUH  PIC X.                                          00001020
           02 MDATJOUV  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUP  PIC X.                                          00001080
           02 MTIMJOUH  PIC X.                                          00001090
           02 MTIMJOUV  PIC X.                                          00001100
           02 MTIMJOUO  PIC X(5).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MFONCA    PIC X.                                          00001130
           02 MFONCC    PIC X.                                          00001140
           02 MFONCP    PIC X.                                          00001150
           02 MFONCH    PIC X.                                          00001160
           02 MFONCV    PIC X.                                          00001170
           02 MFONCO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEA    PIC X.                                          00001200
           02 MPAGEC    PIC X.                                          00001210
           02 MPAGEP    PIC X.                                          00001220
           02 MPAGEH    PIC X.                                          00001230
           02 MPAGEV    PIC X.                                          00001240
           02 MPAGEO    PIC 99.                                         00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNBPA     PIC X.                                          00001270
           02 MNBPC     PIC X.                                          00001280
           02 MNBPP     PIC X.                                          00001290
           02 MNBPH     PIC X.                                          00001300
           02 MNBPV     PIC X.                                          00001310
           02 MNBPO     PIC 99.                                         00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNOMCLA   PIC X.                                          00001340
           02 MNOMCLC   PIC X.                                          00001350
           02 MNOMCLP   PIC X.                                          00001360
           02 MNOMCLH   PIC X.                                          00001370
           02 MNOMCLV   PIC X.                                          00001380
           02 MNOMCLO   PIC X(25).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCHOIXA   PIC X.                                          00001410
           02 MCHOIXC   PIC X.                                          00001420
           02 MCHOIXP   PIC X.                                          00001430
           02 MCHOIXH   PIC X.                                          00001440
           02 MCHOIXV   PIC X.                                          00001450
           02 MCHOIXO   PIC X.                                          00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCDPOSTALA     PIC X.                                     00001480
           02 MCDPOSTALC     PIC X.                                     00001490
           02 MCDPOSTALP     PIC X.                                     00001500
           02 MCDPOSTALH     PIC X.                                     00001510
           02 MCDPOSTALV     PIC X.                                     00001520
           02 MCDPOSTALO     PIC X(5).                                  00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MANNEEA   PIC X.                                          00001550
           02 MANNEEC   PIC X.                                          00001560
           02 MANNEEP   PIC X.                                          00001570
           02 MANNEEH   PIC X.                                          00001580
           02 MANNEEV   PIC X.                                          00001590
           02 MANNEEO   PIC X(4).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MMOISA    PIC X.                                          00001620
           02 MMOISC    PIC X.                                          00001630
           02 MMOISP    PIC X.                                          00001640
           02 MMOISH    PIC X.                                          00001650
           02 MMOISV    PIC X.                                          00001660
           02 MMOISO    PIC X(2).                                       00001670
           02 MLIGNEO OCCURS   5 TIMES .                                00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MACTA   PIC X.                                          00001700
             03 MACTC   PIC X.                                          00001710
             03 MACTP   PIC X.                                          00001720
             03 MACTH   PIC X.                                          00001730
             03 MACTV   PIC X.                                          00001740
             03 MACTO   PIC X.                                          00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MNMPRNMA     PIC X.                                     00001770
             03 MNMPRNMC     PIC X.                                     00001780
             03 MNMPRNMP     PIC X.                                     00001790
             03 MNMPRNMH     PIC X.                                     00001800
             03 MNMPRNMV     PIC X.                                     00001810
             03 MNMPRNMO     PIC X(44).                                 00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MADR1A  PIC X.                                          00001840
             03 MADR1C  PIC X.                                          00001850
             03 MADR1P  PIC X.                                          00001860
             03 MADR1H  PIC X.                                          00001870
             03 MADR1V  PIC X.                                          00001880
             03 MADR1O  PIC X(25).                                      00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MADR2A  PIC X.                                          00001910
             03 MADR2C  PIC X.                                          00001920
             03 MADR2P  PIC X.                                          00001930
             03 MADR2H  PIC X.                                          00001940
             03 MADR2V  PIC X.                                          00001950
             03 MADR2O  PIC X(32).                                      00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MADR3A  PIC X.                                          00001980
             03 MADR3C  PIC X.                                          00001990
             03 MADR3P  PIC X.                                          00002000
             03 MADR3H  PIC X.                                          00002010
             03 MADR3V  PIC X.                                          00002020
             03 MADR3O  PIC X(30).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLIBERRA  PIC X.                                          00002050
           02 MLIBERRC  PIC X.                                          00002060
           02 MLIBERRP  PIC X.                                          00002070
           02 MLIBERRH  PIC X.                                          00002080
           02 MLIBERRV  PIC X.                                          00002090
           02 MLIBERRO  PIC X(79).                                      00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCODTRAA  PIC X.                                          00002120
           02 MCODTRAC  PIC X.                                          00002130
           02 MCODTRAP  PIC X.                                          00002140
           02 MCODTRAH  PIC X.                                          00002150
           02 MCODTRAV  PIC X.                                          00002160
           02 MCODTRAO  PIC X(4).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MZONCMDA  PIC X.                                          00002190
           02 MZONCMDC  PIC X.                                          00002200
           02 MZONCMDP  PIC X.                                          00002210
           02 MZONCMDH  PIC X.                                          00002220
           02 MZONCMDV  PIC X.                                          00002230
           02 MZONCMDO  PIC X(15).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCICSA    PIC X.                                          00002260
           02 MCICSC    PIC X.                                          00002270
           02 MCICSP    PIC X.                                          00002280
           02 MCICSH    PIC X.                                          00002290
           02 MCICSV    PIC X.                                          00002300
           02 MCICSO    PIC X(5).                                       00002310
      * NETNAME                                                         00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNETNAMA  PIC X.                                          00002340
           02 MNETNAMC  PIC X.                                          00002350
           02 MNETNAMP  PIC X.                                          00002360
           02 MNETNAMH  PIC X.                                          00002370
           02 MNETNAMV  PIC X.                                          00002380
           02 MNETNAMO  PIC X(8).                                       00002390
      * CODE TERMINAL                                                   00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MSCREENA  PIC X.                                          00002420
           02 MSCREENC  PIC X.                                          00002430
           02 MSCREENP  PIC X.                                          00002440
           02 MSCREENH  PIC X.                                          00002450
           02 MSCREENV  PIC X.                                          00002460
           02 MSCREENO  PIC X(4).                                       00002470
                                                                                
