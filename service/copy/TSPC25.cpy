      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-PC25-RECORD.                                                      
           05 TS-PC25-TABLE            OCCURS 6.                                
              10 TS-PC25-CPRESTA               PIC X(07).                       
              10 TS-PC25-LPRESTA               PIC X(20).                       
              10 TS-PC25-PUNIT                 PIC X(09).                       
              10 TS-PC25-QTE                   PIC X(05).                       
              10 TS-PC25-MT-HT                 PIC X(09).                       
              10 TS-PC25-MT-TVA                PIC X(09).                       
              10 TS-PC25-MT-TTC                PIC X(09).                       
              10 TS-PC25-TX-TVA                PIC X(01).                       
                                                                                
