      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: client pro selection                                       00000020
      ***************************************************************** 00000030
       01   EBC61I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLNOMI    PIC X(60).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRESSEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MADRESSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRESSEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MADRESSEI      PIC X(60).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPOSTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPOSTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPOSTI   PIC X(60).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTDOML   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNTDOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNTDOMF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNTDOMI   PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSIRETL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSIRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSIRETF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSIRETI  PIC X(14).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCLIENTADRL   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNCLIENTADRL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNCLIENTADRF   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCLIENTADRI   PIC X(8).                                  00000450
           02 MLCLIENTI OCCURS   10 TIMES .                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLCHOIXI     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOML   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOMF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNOMI   PIC X(25).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFOL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINFOF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MINFOI  PIC X(50).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(73).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: client pro selection                                       00000800
      ***************************************************************** 00000810
       01   EBC61O REDEFINES EBC61I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEP    PIC X.                                          00001010
           02 MPAGEH    PIC X.                                          00001020
           02 MPAGEV    PIC X.                                          00001030
           02 MPAGEO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MPAGEMAXA      PIC X.                                     00001060
           02 MPAGEMAXC PIC X.                                          00001070
           02 MPAGEMAXP PIC X.                                          00001080
           02 MPAGEMAXH PIC X.                                          00001090
           02 MPAGEMAXV PIC X.                                          00001100
           02 MPAGEMAXO      PIC X(3).                                  00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLNOMA    PIC X.                                          00001130
           02 MLNOMC    PIC X.                                          00001140
           02 MLNOMP    PIC X.                                          00001150
           02 MLNOMH    PIC X.                                          00001160
           02 MLNOMV    PIC X.                                          00001170
           02 MLNOMO    PIC X(60).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MADRESSEA      PIC X.                                     00001200
           02 MADRESSEC PIC X.                                          00001210
           02 MADRESSEP PIC X.                                          00001220
           02 MADRESSEH PIC X.                                          00001230
           02 MADRESSEV PIC X.                                          00001240
           02 MADRESSEO      PIC X(60).                                 00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCPOSTA   PIC X.                                          00001270
           02 MCPOSTC   PIC X.                                          00001280
           02 MCPOSTP   PIC X.                                          00001290
           02 MCPOSTH   PIC X.                                          00001300
           02 MCPOSTV   PIC X.                                          00001310
           02 MCPOSTO   PIC X(60).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNTDOMA   PIC X.                                          00001340
           02 MNTDOMC   PIC X.                                          00001350
           02 MNTDOMP   PIC X.                                          00001360
           02 MNTDOMH   PIC X.                                          00001370
           02 MNTDOMV   PIC X.                                          00001380
           02 MNTDOMO   PIC X(15).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNSIRETA  PIC X.                                          00001410
           02 MNSIRETC  PIC X.                                          00001420
           02 MNSIRETP  PIC X.                                          00001430
           02 MNSIRETH  PIC X.                                          00001440
           02 MNSIRETV  PIC X.                                          00001450
           02 MNSIRETO  PIC X(14).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNCLIENTADRA   PIC X.                                     00001480
           02 MNCLIENTADRC   PIC X.                                     00001490
           02 MNCLIENTADRP   PIC X.                                     00001500
           02 MNCLIENTADRH   PIC X.                                     00001510
           02 MNCLIENTADRV   PIC X.                                     00001520
           02 MNCLIENTADRO   PIC X(8).                                  00001530
           02 MLCLIENTO OCCURS   10 TIMES .                             00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MLCHOIXA     PIC X.                                     00001560
             03 MLCHOIXC     PIC X.                                     00001570
             03 MLCHOIXP     PIC X.                                     00001580
             03 MLCHOIXH     PIC X.                                     00001590
             03 MLCHOIXV     PIC X.                                     00001600
             03 MLCHOIXO     PIC X.                                     00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNOMA   PIC X.                                          00001630
             03 MNOMC   PIC X.                                          00001640
             03 MNOMP   PIC X.                                          00001650
             03 MNOMH   PIC X.                                          00001660
             03 MNOMV   PIC X.                                          00001670
             03 MNOMO   PIC X(25).                                      00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MINFOA  PIC X.                                          00001700
             03 MINFOC  PIC X.                                          00001710
             03 MINFOP  PIC X.                                          00001720
             03 MINFOH  PIC X.                                          00001730
             03 MINFOV  PIC X.                                          00001740
             03 MINFOO  PIC X(50).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(73).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
