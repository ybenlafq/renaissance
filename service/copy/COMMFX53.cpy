      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  Z-COMMFX53.                                                          
           03  COMM-FX53-GV11-DCOMPTA    PIC X(8).                              
           03  COMM-FX53-OPEN-CLOSE      PIC X(1).                              
              88  COMM-FX53-A-OUVRIR VALUE 'O'.                                 
              88  COMM-FX53-A-FERMER VALUE 'C'.                                 
              88  COMM-FX53-OUVERTS VALUE 'R'.                                  
           03  COMM-FX53-I.                                                     
              05  COMM-FX53-NSOCIETE     PIC X(3).                              
              05  COMM-FX53-NLIEU        PIC X(3).                              
              05  COMM-FX53-NVENTE       PIC X(7).                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-FX53-I-FFX052     PIC S9(4) COMP.                        
      *--                                                                       
              05  COMM-FX53-I-FFX052     PIC S9(4) COMP-5.                      
      *}                                                                        
              05  COMM-FX53-OCCURS      OCCURS 50.                              
                 10  COMM-FX53-CGCPLT    PIC X(5).                              
                 10  COMM-FX53-NGCPLT    PIC X(8).                              
                 10  COMM-FX53-NCODIC    PIC X(7).                              
                 10  COMM-FX53-DDELIV    PIC X(8).                              
                 10  COMM-FX53-DCOMPTA   PIC X(8).                              
                 10  COMM-FX53-PVTOTAL   PIC S9(7)V99 COMP-3.                   
                 10  COMM-FX53-LNOM      PIC X(25).                             
                 10  COMM-FX53-CINSEE    PIC X(5).                              
           03  COMM-FX53-O.                                                     
              05  COMM-FX53-CODE-RETOUR  PIC X(1) VALUE '0'.                    
                 88  COMM-FX53-OK           VALUE '0'.                          
                 88  COMM-FX53-KO           VALUE '1'.                          
                 88  COMM-FX53-KK           VALUE '2'.                          
              05  COMM-FX53-MSG          PIC X(50).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05  COMM-FX53-I-KO         PIC S9(4) COMP.                        
      *                                                                         
      *--                                                                       
              05  COMM-FX53-I-KO         PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
