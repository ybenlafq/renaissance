      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(RDPP.RVPC0800)                                    *        
      *        LIBRARY(DSA024.DEVL.SOURCE(RVPC0800))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        APOST                                                   *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RDPP.RVPC0800                      *        
      ******************************************************************        
       01  RVPC0800.                                                            
           10 PC08-RANDOMNUM       PIC X(19).                                   
           10 PC08-NSOCIETE        PIC X(3).                                    
           10 PC08-NLIEU           PIC X(3).                                    
           10 PC08-NVENTE          PIC X(7).                                    
           10 PC08-CPRESTATION     PIC X(5).                                    
           10 PC08-CTITRENOM       PIC X(5).                                    
           10 PC08-LNOM            PIC X(25).                                   
           10 PC08-LPRENOM         PIC X(15).                                   
           10 PC08-NSEQ            PIC S9(5)V USAGE COMP-3.                     
           10 PC08-NSOCIETER       PIC X(3).                                    
           10 PC08-NLIEUR          PIC X(3).                                    
           10 PC08-NVENTER         PIC X(7).                                    
           10 PC08-TEOP            PIC X(3).                                    
           10 PC08-MEOP            PIC S9(7)V9(2) USAGE COMP-3.                 
           10 PC08-DACTIV          PIC X(8).                                    
           10 PC08-DCREATION       PIC X(8).                                    
           10 PC08-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRVPC0800.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 17 TIMES.          
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 17 TIMES.              
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 17      *        
      ******************************************************************        
                                                                                
