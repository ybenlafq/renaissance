      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP071 AU 03/05/2007  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,08,BI,A,                          *        
      *                           20,07,BI,A,                          *        
      *                           27,03,PD,A,                          *        
      *                           30,20,BI,A,                          *        
      *                           50,07,BI,A,                          *        
      *                           57,03,BI,A,                          *        
      *                           60,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP071.                                                        
            05 NOMETAT-ISP071           PIC X(6) VALUE 'ISP071'.                
            05 RUPTURES-ISP071.                                                 
           10 ISP071-COULEUR            PIC X(05).                      007  005
           10 ISP071-NCDE               PIC X(08).                      012  008
           10 ISP071-NREC               PIC X(07).                      020  007
           10 ISP071-WSEQFAM            PIC S9(05)      COMP-3.         027  003
           10 ISP071-LREFFOURN          PIC X(20).                      030  020
           10 ISP071-NCODIC             PIC X(07).                      050  007
           10 ISP071-CDEVISE            PIC X(03).                      057  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP071-SEQUENCE           PIC S9(04) COMP.                060  002
      *--                                                                       
           10 ISP071-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP071.                                                   
           10 ISP071-CFAM               PIC X(05).                      062  005
           10 ISP071-CMARQ              PIC X(05).                      067  005
           10 ISP071-DTRAIT             PIC X(10).                      072  010
           10 ISP071-LCOMMENT           PIC X(20).                      082  020
           10 ISP071-NSOC               PIC X(03).                      102  003
           10 ISP071-PBF                PIC S9(07)V9(2) COMP-3.         105  005
           10 ISP071-PSF                PIC S9(07)V9(2) COMP-3.         110  005
           10 ISP071-SRP                PIC S9(07)V9(2) COMP-3.         115  005
            05 FILLER                      PIC X(393).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP071-LONG           PIC S9(4)   COMP  VALUE +119.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP071-LONG           PIC S9(4) COMP-5  VALUE +119.           
                                                                                
      *}                                                                        
