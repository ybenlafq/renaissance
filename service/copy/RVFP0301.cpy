      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVFP0301                    *        
      ******************************************************************        
       01  RVFP0301.                                                            
      *                       NSINISTRE                                         
           10 FP03-NSINISTRE       PIC X(20).                                   
      *                       CTYPFAC2                                          
           10 FP03-CTYPFAC2        PIC X(1).                                    
      *                       DSYST                                             
           10 FP03-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       MTFRANCH                                          
           10 FP03-MTFRANCH        PIC S9(11)V9(2) USAGE COMP-3.                
      *                       MTCLIENT                                          
           10 FP03-MTCLIENT        PIC S9(11)V9(2) USAGE COMP-3.                
      *                       DCHARGE                                           
           10 FP03-DCHARGE         PIC X(8).                                    
      *                       DTRAIT                                            
           10 FP03-DTRAIT          PIC X(8).                                    
      *                       NFACT_PAC                                         
           10 FP03-NFACT-PAC       PIC X(12).                                   
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
