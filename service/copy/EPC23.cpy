      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ACC: DONNEES CLIENT B2B                                         00000020
      ***************************************************************** 00000030
       01   EPC23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSTATUTI  PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCDEI    PIC X(12).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDCDEI    PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHRONOL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCHRONOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCHRONOF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCHRONOI      PIC X(15).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTREMISEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MTREMISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTREMISEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTREMISEI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSL      COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MRSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MRSF      PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MRSI      PIC X(25).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFRSL     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MFRSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFRSF     PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MFRSI     PIC X(25).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCPTEL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCPTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCPTEF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCPTEI   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFNCPTEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MFNCPTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFNCPTEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MFNCPTEI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVOIEL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVOIEF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCVOIEI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCVOIEL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MFCVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFCVOIEF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MFCVOIEI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVOIEL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MTVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTVOIEF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTVOIEI   PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFTVOIEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MFTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFTVOIEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MFTVOIEI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIEL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNVOIEF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNVOIEI   PIC X(21).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFNVOIEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MFNVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MFNVOIEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MFNVOIEI  PIC X(21).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCADRL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCADRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCADRF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCADRI    PIC X(25).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCADRL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MFCADRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFCADRF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MFCADRI   PIC X(25).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCPOSTALI      PIC X(5).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCPOSTALL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MFCPOSTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFCPOSTALF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MFCPOSTALI     PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMUNEL      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCOMMUNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMMUNEF      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCOMMUNEI      PIC X(25).                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCOMMUNEL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MFCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFCOMMUNEF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MFCOMMUNEI     PIC X(25).                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBUREAUL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MBUREAUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MBUREAUF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MBUREAUI  PIC X(25).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFBUREAUL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MFBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFBUREAUF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MFBUREAUI      PIC X(25).                                 00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAYSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MPAYSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAYSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MPAYSI    PIC X(20).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFPAYSL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MFPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFPAYSF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MFPAYSI   PIC X(20).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(74).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCODTRAI  PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCICSI    PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNETNAMI  PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MSCREENI  PIC X(4).                                       00001330
      ***************************************************************** 00001340
      * ACC: DONNEES CLIENT B2B                                         00001350
      ***************************************************************** 00001360
       01   EPC23O REDEFINES EPC23I.                                    00001370
           02 FILLER    PIC X(12).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MTIMJOUA  PIC X.                                          00001470
           02 MTIMJOUC  PIC X.                                          00001480
           02 MTIMJOUP  PIC X.                                          00001490
           02 MTIMJOUH  PIC X.                                          00001500
           02 MTIMJOUV  PIC X.                                          00001510
           02 MTIMJOUO  PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MSTATUTA  PIC X.                                          00001540
           02 MSTATUTC  PIC X.                                          00001550
           02 MSTATUTP  PIC X.                                          00001560
           02 MSTATUTH  PIC X.                                          00001570
           02 MSTATUTV  PIC X.                                          00001580
           02 MSTATUTO  PIC X(10).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNCDEA    PIC X.                                          00001610
           02 MNCDEC    PIC X.                                          00001620
           02 MNCDEP    PIC X.                                          00001630
           02 MNCDEH    PIC X.                                          00001640
           02 MNCDEV    PIC X.                                          00001650
           02 MNCDEO    PIC X(12).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MDCDEA    PIC X.                                          00001680
           02 MDCDEC    PIC X.                                          00001690
           02 MDCDEP    PIC X.                                          00001700
           02 MDCDEH    PIC X.                                          00001710
           02 MDCDEV    PIC X.                                          00001720
           02 MDCDEO    PIC X(10).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNCHRONOA      PIC X.                                     00001750
           02 MNCHRONOC PIC X.                                          00001760
           02 MNCHRONOP PIC X.                                          00001770
           02 MNCHRONOH PIC X.                                          00001780
           02 MNCHRONOV PIC X.                                          00001790
           02 MNCHRONOO      PIC X(15).                                 00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MTREMISEA      PIC X.                                     00001820
           02 MTREMISEC PIC X.                                          00001830
           02 MTREMISEP PIC X.                                          00001840
           02 MTREMISEH PIC X.                                          00001850
           02 MTREMISEV PIC X.                                          00001860
           02 MTREMISEO      PIC X(5).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MRSA      PIC X.                                          00001890
           02 MRSC      PIC X.                                          00001900
           02 MRSP      PIC X.                                          00001910
           02 MRSH      PIC X.                                          00001920
           02 MRSV      PIC X.                                          00001930
           02 MRSO      PIC X(25).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MFRSA     PIC X.                                          00001960
           02 MFRSC     PIC X.                                          00001970
           02 MFRSP     PIC X.                                          00001980
           02 MFRSH     PIC X.                                          00001990
           02 MFRSV     PIC X.                                          00002000
           02 MFRSO     PIC X(25).                                      00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNCPTEA   PIC X.                                          00002030
           02 MNCPTEC   PIC X.                                          00002040
           02 MNCPTEP   PIC X.                                          00002050
           02 MNCPTEH   PIC X.                                          00002060
           02 MNCPTEV   PIC X.                                          00002070
           02 MNCPTEO   PIC X(8).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MFNCPTEA  PIC X.                                          00002100
           02 MFNCPTEC  PIC X.                                          00002110
           02 MFNCPTEP  PIC X.                                          00002120
           02 MFNCPTEH  PIC X.                                          00002130
           02 MFNCPTEV  PIC X.                                          00002140
           02 MFNCPTEO  PIC X(8).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCVOIEA   PIC X.                                          00002170
           02 MCVOIEC   PIC X.                                          00002180
           02 MCVOIEP   PIC X.                                          00002190
           02 MCVOIEH   PIC X.                                          00002200
           02 MCVOIEV   PIC X.                                          00002210
           02 MCVOIEO   PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MFCVOIEA  PIC X.                                          00002240
           02 MFCVOIEC  PIC X.                                          00002250
           02 MFCVOIEP  PIC X.                                          00002260
           02 MFCVOIEH  PIC X.                                          00002270
           02 MFCVOIEV  PIC X.                                          00002280
           02 MFCVOIEO  PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MTVOIEA   PIC X.                                          00002310
           02 MTVOIEC   PIC X.                                          00002320
           02 MTVOIEP   PIC X.                                          00002330
           02 MTVOIEH   PIC X.                                          00002340
           02 MTVOIEV   PIC X.                                          00002350
           02 MTVOIEO   PIC X(4).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MFTVOIEA  PIC X.                                          00002380
           02 MFTVOIEC  PIC X.                                          00002390
           02 MFTVOIEP  PIC X.                                          00002400
           02 MFTVOIEH  PIC X.                                          00002410
           02 MFTVOIEV  PIC X.                                          00002420
           02 MFTVOIEO  PIC X(4).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNVOIEA   PIC X.                                          00002450
           02 MNVOIEC   PIC X.                                          00002460
           02 MNVOIEP   PIC X.                                          00002470
           02 MNVOIEH   PIC X.                                          00002480
           02 MNVOIEV   PIC X.                                          00002490
           02 MNVOIEO   PIC X(21).                                      00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MFNVOIEA  PIC X.                                          00002520
           02 MFNVOIEC  PIC X.                                          00002530
           02 MFNVOIEP  PIC X.                                          00002540
           02 MFNVOIEH  PIC X.                                          00002550
           02 MFNVOIEV  PIC X.                                          00002560
           02 MFNVOIEO  PIC X(21).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCADRA    PIC X.                                          00002590
           02 MCADRC    PIC X.                                          00002600
           02 MCADRP    PIC X.                                          00002610
           02 MCADRH    PIC X.                                          00002620
           02 MCADRV    PIC X.                                          00002630
           02 MCADRO    PIC X(25).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MFCADRA   PIC X.                                          00002660
           02 MFCADRC   PIC X.                                          00002670
           02 MFCADRP   PIC X.                                          00002680
           02 MFCADRH   PIC X.                                          00002690
           02 MFCADRV   PIC X.                                          00002700
           02 MFCADRO   PIC X(25).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCPOSTALA      PIC X.                                     00002730
           02 MCPOSTALC PIC X.                                          00002740
           02 MCPOSTALP PIC X.                                          00002750
           02 MCPOSTALH PIC X.                                          00002760
           02 MCPOSTALV PIC X.                                          00002770
           02 MCPOSTALO      PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MFCPOSTALA     PIC X.                                     00002800
           02 MFCPOSTALC     PIC X.                                     00002810
           02 MFCPOSTALP     PIC X.                                     00002820
           02 MFCPOSTALH     PIC X.                                     00002830
           02 MFCPOSTALV     PIC X.                                     00002840
           02 MFCPOSTALO     PIC X(5).                                  00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCOMMUNEA      PIC X.                                     00002870
           02 MCOMMUNEC PIC X.                                          00002880
           02 MCOMMUNEP PIC X.                                          00002890
           02 MCOMMUNEH PIC X.                                          00002900
           02 MCOMMUNEV PIC X.                                          00002910
           02 MCOMMUNEO      PIC X(25).                                 00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MFCOMMUNEA     PIC X.                                     00002940
           02 MFCOMMUNEC     PIC X.                                     00002950
           02 MFCOMMUNEP     PIC X.                                     00002960
           02 MFCOMMUNEH     PIC X.                                     00002970
           02 MFCOMMUNEV     PIC X.                                     00002980
           02 MFCOMMUNEO     PIC X(25).                                 00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MBUREAUA  PIC X.                                          00003010
           02 MBUREAUC  PIC X.                                          00003020
           02 MBUREAUP  PIC X.                                          00003030
           02 MBUREAUH  PIC X.                                          00003040
           02 MBUREAUV  PIC X.                                          00003050
           02 MBUREAUO  PIC X(25).                                      00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MFBUREAUA      PIC X.                                     00003080
           02 MFBUREAUC PIC X.                                          00003090
           02 MFBUREAUP PIC X.                                          00003100
           02 MFBUREAUH PIC X.                                          00003110
           02 MFBUREAUV PIC X.                                          00003120
           02 MFBUREAUO      PIC X(25).                                 00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MPAYSA    PIC X.                                          00003150
           02 MPAYSC    PIC X.                                          00003160
           02 MPAYSP    PIC X.                                          00003170
           02 MPAYSH    PIC X.                                          00003180
           02 MPAYSV    PIC X.                                          00003190
           02 MPAYSO    PIC X(20).                                      00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MFPAYSA   PIC X.                                          00003220
           02 MFPAYSC   PIC X.                                          00003230
           02 MFPAYSP   PIC X.                                          00003240
           02 MFPAYSH   PIC X.                                          00003250
           02 MFPAYSV   PIC X.                                          00003260
           02 MFPAYSO   PIC X(20).                                      00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MLIBERRA  PIC X.                                          00003290
           02 MLIBERRC  PIC X.                                          00003300
           02 MLIBERRP  PIC X.                                          00003310
           02 MLIBERRH  PIC X.                                          00003320
           02 MLIBERRV  PIC X.                                          00003330
           02 MLIBERRO  PIC X(74).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCODTRAA  PIC X.                                          00003360
           02 MCODTRAC  PIC X.                                          00003370
           02 MCODTRAP  PIC X.                                          00003380
           02 MCODTRAH  PIC X.                                          00003390
           02 MCODTRAV  PIC X.                                          00003400
           02 MCODTRAO  PIC X(4).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCICSA    PIC X.                                          00003430
           02 MCICSC    PIC X.                                          00003440
           02 MCICSP    PIC X.                                          00003450
           02 MCICSH    PIC X.                                          00003460
           02 MCICSV    PIC X.                                          00003470
           02 MCICSO    PIC X(5).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MNETNAMA  PIC X.                                          00003500
           02 MNETNAMC  PIC X.                                          00003510
           02 MNETNAMP  PIC X.                                          00003520
           02 MNETNAMH  PIC X.                                          00003530
           02 MNETNAMV  PIC X.                                          00003540
           02 MNETNAMO  PIC X(8).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MSCREENA  PIC X.                                          00003570
           02 MSCREENC  PIC X.                                          00003580
           02 MSCREENP  PIC X.                                          00003590
           02 MSCREENH  PIC X.                                          00003600
           02 MSCREENV  PIC X.                                          00003610
           02 MSCREENO  PIC X(4).                                       00003620
                                                                                
