      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   TABLE RTRB50                                                          
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVRB5001                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVRB5001.                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           02  RB50-TYPINTER                                                    
               PIC X(0005).                                                     
           02  RB50-LIBFAS                                                      
               PIC X(0030).                                                     
           02  RB50-DCREATION                                                   
               PIC X(0008).                                                     
           02  RB50-DMAJ                                                        
               PIC X(0008).                                                     
           02  RB50-LIBOPE                                                      
               PIC X(0015).                                                     
           02  RB50-DEFFET                                                      
               PIC X(0008).                                                     
           02  RB50-WRESIL                                                      
               PIC X(0001).                                                     
       EXEC SQL END DECLARE SECTION END-EXEC.
           02  RB50-MONTANT                                                     
               PIC 9(0003)V99 COMP-3.                                           
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           02  RB50-CTVA                                                        
               PIC X(0001).                                                     
           02  RB50-TYPCOMPTA                                                   
               PIC X(0001).                                                     
           02  RB50-CPTIMMO                                                     
               PIC X(0006).                                                     
           02  RB50-WMODAMORT                                                   
               PIC X(0001).                                                     
           02  RB50-DURECO                                                      
               PIC S9(0002)V99 COMP-3.                                          
           02  RB50-DURFISC                                                     
               PIC S9(0002)V99 COMP-3.                                          
           02  RB50-WCDETRTABEL                                                 
               PIC X(0001).                                                     
           02  RB50-RUBRIQUE                                                    
               PIC X(0006).                                                     
           02  RB50-SECTION                                                     
               PIC X(0006).                                                     
       EXEC SQL END DECLARE SECTION END-EXEC.
