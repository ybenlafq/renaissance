      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-AP3A-RECORD.                                                      
           05 TS-AP3A-TABLE            OCCURS 10.                               
              10 TS-AP3A-LIGNE                 PIC X(80).                       
              10 TS-AP3A-ACT                   PIC X(01).                       
              10 TS-AP3A-A-F                   PIC X(02).                       
              10 TS-AP3A-NSEQID                PIC S9(18) COMP-3.               
                                                                                
