      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * B2B : Menu selection commandes                                  00000020
      ***************************************************************** 00000030
       01   EBB00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOSSIERL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MDOSSIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDOSSIERF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MDOSSIERI      PIC X(12).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNOMI     PIC X(25).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELDOML  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTELDOML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELDOMF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTELDOMI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCDEI    PIC X(12).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEB1L      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDATDEB1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATDEB1F      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATDEB1I      PIC X(10).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFIN1L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDATFIN1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATFIN1F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATFIN1I      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEB2L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDATDEB2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATDEB2F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATDEB2I      PIC X(10).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFIN2L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDATFIN2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATFIN2F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATFIN2I      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNBB01L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MWNBB01L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWNBB01F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWNBB01I  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNBB00L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MWNBB00L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWNBB00F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWNBB00I  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCERRL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCERRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCERRF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCERRI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBERRI  PIC X(73).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODTRAI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCICSI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNETNAMI  PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSCREENI  PIC X(4).                                       00000810
      ***************************************************************** 00000820
      * B2B : Menu selection commandes                                  00000830
      ***************************************************************** 00000840
       01   EBB00O REDEFINES EBB00I.                                    00000850
           02 FILLER    PIC X(12).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MDATJOUA  PIC X.                                          00000880
           02 MDATJOUC  PIC X.                                          00000890
           02 MDATJOUP  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUV  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MTIMJOUA  PIC X.                                          00000950
           02 MTIMJOUC  PIC X.                                          00000960
           02 MTIMJOUP  PIC X.                                          00000970
           02 MTIMJOUH  PIC X.                                          00000980
           02 MTIMJOUV  PIC X.                                          00000990
           02 MTIMJOUO  PIC X(5).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MDOSSIERA      PIC X.                                     00001020
           02 MDOSSIERC PIC X.                                          00001030
           02 MDOSSIERP PIC X.                                          00001040
           02 MDOSSIERH PIC X.                                          00001050
           02 MDOSSIERV PIC X.                                          00001060
           02 MDOSSIERO      PIC X(12).                                 00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNOMA     PIC X.                                          00001090
           02 MNOMC     PIC X.                                          00001100
           02 MNOMP     PIC X.                                          00001110
           02 MNOMH     PIC X.                                          00001120
           02 MNOMV     PIC X.                                          00001130
           02 MNOMO     PIC X(25).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MNVENTEA  PIC X.                                          00001160
           02 MNVENTEC  PIC X.                                          00001170
           02 MNVENTEP  PIC X.                                          00001180
           02 MNVENTEH  PIC X.                                          00001190
           02 MNVENTEV  PIC X.                                          00001200
           02 MNVENTEO  PIC X(7).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MTELDOMA  PIC X.                                          00001230
           02 MTELDOMC  PIC X.                                          00001240
           02 MTELDOMP  PIC X.                                          00001250
           02 MTELDOMH  PIC X.                                          00001260
           02 MTELDOMV  PIC X.                                          00001270
           02 MTELDOMO  PIC X(10).                                      00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNCDEA    PIC X.                                          00001300
           02 MNCDEC    PIC X.                                          00001310
           02 MNCDEP    PIC X.                                          00001320
           02 MNCDEH    PIC X.                                          00001330
           02 MNCDEV    PIC X.                                          00001340
           02 MNCDEO    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATDEB1A      PIC X.                                     00001370
           02 MDATDEB1C PIC X.                                          00001380
           02 MDATDEB1P PIC X.                                          00001390
           02 MDATDEB1H PIC X.                                          00001400
           02 MDATDEB1V PIC X.                                          00001410
           02 MDATDEB1O      PIC X(10).                                 00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDATFIN1A      PIC X.                                     00001440
           02 MDATFIN1C PIC X.                                          00001450
           02 MDATFIN1P PIC X.                                          00001460
           02 MDATFIN1H PIC X.                                          00001470
           02 MDATFIN1V PIC X.                                          00001480
           02 MDATFIN1O      PIC X(10).                                 00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MDATDEB2A      PIC X.                                     00001510
           02 MDATDEB2C PIC X.                                          00001520
           02 MDATDEB2P PIC X.                                          00001530
           02 MDATDEB2H PIC X.                                          00001540
           02 MDATDEB2V PIC X.                                          00001550
           02 MDATDEB2O      PIC X(10).                                 00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDATFIN2A      PIC X.                                     00001580
           02 MDATFIN2C PIC X.                                          00001590
           02 MDATFIN2P PIC X.                                          00001600
           02 MDATFIN2H PIC X.                                          00001610
           02 MDATFIN2V PIC X.                                          00001620
           02 MDATFIN2O      PIC X(10).                                 00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MWNBB01A  PIC X.                                          00001650
           02 MWNBB01C  PIC X.                                          00001660
           02 MWNBB01P  PIC X.                                          00001670
           02 MWNBB01H  PIC X.                                          00001680
           02 MWNBB01V  PIC X.                                          00001690
           02 MWNBB01O  PIC X.                                          00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MWNBB00A  PIC X.                                          00001720
           02 MWNBB00C  PIC X.                                          00001730
           02 MWNBB00P  PIC X.                                          00001740
           02 MWNBB00H  PIC X.                                          00001750
           02 MWNBB00V  PIC X.                                          00001760
           02 MWNBB00O  PIC X.                                          00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCERRA    PIC X.                                          00001790
           02 MCERRC    PIC X.                                          00001800
           02 MCERRP    PIC X.                                          00001810
           02 MCERRH    PIC X.                                          00001820
           02 MCERRV    PIC X.                                          00001830
           02 MCERRO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MLIBERRA  PIC X.                                          00001860
           02 MLIBERRC  PIC X.                                          00001870
           02 MLIBERRP  PIC X.                                          00001880
           02 MLIBERRH  PIC X.                                          00001890
           02 MLIBERRV  PIC X.                                          00001900
           02 MLIBERRO  PIC X(73).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCODTRAA  PIC X.                                          00001930
           02 MCODTRAC  PIC X.                                          00001940
           02 MCODTRAP  PIC X.                                          00001950
           02 MCODTRAH  PIC X.                                          00001960
           02 MCODTRAV  PIC X.                                          00001970
           02 MCODTRAO  PIC X(4).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCICSA    PIC X.                                          00002000
           02 MCICSC    PIC X.                                          00002010
           02 MCICSP    PIC X.                                          00002020
           02 MCICSH    PIC X.                                          00002030
           02 MCICSV    PIC X.                                          00002040
           02 MCICSO    PIC X(5).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNETNAMA  PIC X.                                          00002070
           02 MNETNAMC  PIC X.                                          00002080
           02 MNETNAMP  PIC X.                                          00002090
           02 MNETNAMH  PIC X.                                          00002100
           02 MNETNAMV  PIC X.                                          00002110
           02 MNETNAMO  PIC X(8).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MSCREENA  PIC X.                                          00002140
           02 MSCREENC  PIC X.                                          00002150
           02 MSCREENP  PIC X.                                          00002160
           02 MSCREENH  PIC X.                                          00002170
           02 MSCREENV  PIC X.                                          00002180
           02 MSCREENO  PIC X(4).                                       00002190
                                                                                
