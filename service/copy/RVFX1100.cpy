      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFX1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX1100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX1100.                                                            
           02  FX11-CZONE                                                       
               PIC X(0008).                                                     
           02  FX11-ZONEGL                                                      
               PIC X(0012).                                                     
           02  FX11-ZONECOSYS                                                   
               PIC X(0016).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFX1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFX1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX11-CZONE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX11-CZONE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX11-ZONEGL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX11-ZONEGL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX11-ZONECOSYS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FX11-ZONECOSYS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
