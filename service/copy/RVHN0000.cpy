      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RTHN00)                                    *        
      *        LIBRARY(DSA043.DEVL.SOURCE(RVHN0000))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(HN00-)                                            *        
      *        STRUCTURE(RVHN0000)                                     *        
      *        APOST                                                   *        
      *        LABEL(YES)                                              *        
      *        DBCSDELIM(NO)                                           *        
      *        COLSUFFIX(YES)                                          *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RTHN00 TABLE                                 
           ( CTIERS                         CHAR(05) NOT NULL,                  
             LNOM                           CHAR(20) NOT NULL,                  
             LADR1                          CHAR(32) NOT NULL,                  
             LADR2                          CHAR(32) NOT NULL,                  
             CPOSTAL                        CHAR(05) NOT NULL,                  
             LCOMMUNE                       CHAR(26) NOT NULL,                  
             NTEL                           CHAR(08) NOT NULL,                  
             NFAX                           CHAR(15) NOT NULL,                  
             LINTERLOCUT                    CHAR(20) NOT NULL,                  
             CTYPTRAN                       CHAR(05) NOT NULL,                  
             WTYPTIERS                      CHAR(01) NOT NULL,                  
             NENTCDE                        CHAR(05) NOT NULL,                  
             CTIERSELA                      CHAR(04) NOT NULL,                  
             DSYST                          DEC (13, 0) NOT NULL,               
             CTYPDEST                       CHAR(03) NOT NULL,                  
             CPAYS                          CHAR(02) NOT NULL,                  
             DTRAIT                         CHAR(08) NOT NULL,                  
             NSOCORIG                       CHAR(03) NOT NULL,                  
             WSUPP                          CHAR(01) NOT NULL                   
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTHN00                      *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVHN0000.                                                            
      *    *************************************************************        
      *                       CTIERS                                            
           10 HN00-CTIERS          PIC X(5).                                    
      *    *************************************************************        
      *                       LNOM                                              
           10 HN00-LNOM            PIC X(20).                                   
      *    *************************************************************        
      *                       LADR1                                             
           10 HN00-LADR1           PIC X(32).                                   
      *    *************************************************************        
      *                       LADR2                                             
           10 HN00-LADR2           PIC X(32).                                   
      *    *************************************************************        
      *                       CPOSTAL                                           
           10 HN00-CPOSTAL         PIC X(5).                                    
      *    *************************************************************        
      *                       LCOMMUNE                                          
           10 HN00-LCOMMUNE        PIC X(26).                                   
      *    *************************************************************        
      *                       NTEL                                              
           10 HN00-NTEL            PIC X(8).                                    
      *    *************************************************************        
      *                       NFAX                                              
           10 HN00-NFAX            PIC X(15).                                   
      *    *************************************************************        
      *                       LINTERLOCUT                                       
           10 HN00-LINTERLOCUT     PIC X(20).                                   
      *    *************************************************************        
      *                       CTYPTRAN                                          
           10 HN00-CTYPTRAN        PIC X(5).                                    
      *    *************************************************************        
      *                       WTYPTIERS                                         
           10 HN00-WTYPTIERS       PIC X(1).                                    
      *    *************************************************************        
      *                       NENTCDE                                           
           10 HN00-NENTCDE         PIC X(5).                                    
      *    *************************************************************        
      *                       CTIERSELA                                         
           10 HN00-CTIERSELA       PIC X(4).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 HN00-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       CTYPDEST                                          
           10 HN00-CTYPDEST        PIC X(3).                                    
      *    *************************************************************        
      *                       CPAYS                                             
           10 HN00-CPAYS           PIC X(2).                                    
      *    *************************************************************        
      *                       DTRAIT                                            
           10 HN00-DTRAIT          PIC X(8).                                    
      *    *************************************************************        
      *                       NSOCORIG                                          
           10 HN00-NSOCORIG        PIC X(3).                                    
      *    *************************************************************        
      *                       WSUPP                                             
           10 HN00-WSUPP           PIC X(1).                                    
       EXEC SQL END DECLARE SECTION END-EXEC.
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTHN00.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 19 TIMES.          
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 19 TIMES.              
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
                                                                                
