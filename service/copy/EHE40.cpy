      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE40   EHE40                                              00000020
      ***************************************************************** 00000030
       01   EHE40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(41).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNUMPAGEI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMAXI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEHETL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEHETF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIEHETI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEHETI      PIC X(20).                                 00000330
           02 FILLER  OCCURS   12 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEL1L  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MSEL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEL1F  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MSEL1I  PIC X.                                          00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIERS1L     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MTIERS1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIERS1F     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MTIERS1I     PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPDTS1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNPDTS1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPDTS1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNPDTS1I     PIC X(4).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDENV1L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDDENV1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDENV1F     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDDENV1I     PIC X(10).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEL2L  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSEL2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSEL2F  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSEL2I  PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIERS2L     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MTIERS2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIERS2F     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTIERS2I     PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNPDTS2L     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNPDTS2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNPDTS2F     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNPDTS2I     PIC X(4).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDENV2L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDDENV2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDENV2F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDDENV2I     PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(78).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMMAPL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNOMMAPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOMMAPF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNOMMAPI  PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EHE40   EHE40                                              00000920
      ***************************************************************** 00000930
       01   EHE40O REDEFINES EHE40I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTITREA   PIC X.                                          00001110
           02 MTITREC   PIC X.                                          00001120
           02 MTITREP   PIC X.                                          00001130
           02 MTITREH   PIC X.                                          00001140
           02 MTITREV   PIC X.                                          00001150
           02 MTITREO   PIC X(41).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MNUMPAGEA      PIC X.                                     00001180
           02 MNUMPAGEC PIC X.                                          00001190
           02 MNUMPAGEP PIC X.                                          00001200
           02 MNUMPAGEH PIC X.                                          00001210
           02 MNUMPAGEV PIC X.                                          00001220
           02 MNUMPAGEO      PIC X(2).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MPAGEMAXA      PIC X.                                     00001250
           02 MPAGEMAXC PIC X.                                          00001260
           02 MPAGEMAXP PIC X.                                          00001270
           02 MPAGEMAXH PIC X.                                          00001280
           02 MPAGEMAXV PIC X.                                          00001290
           02 MPAGEMAXO      PIC X(2).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCLIEHETA      PIC X.                                     00001320
           02 MCLIEHETC PIC X.                                          00001330
           02 MCLIEHETP PIC X.                                          00001340
           02 MCLIEHETH PIC X.                                          00001350
           02 MCLIEHETV PIC X.                                          00001360
           02 MCLIEHETO      PIC X(5).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLLIEHETA      PIC X.                                     00001390
           02 MLLIEHETC PIC X.                                          00001400
           02 MLLIEHETP PIC X.                                          00001410
           02 MLLIEHETH PIC X.                                          00001420
           02 MLLIEHETV PIC X.                                          00001430
           02 MLLIEHETO      PIC X(20).                                 00001440
           02 FILLER  OCCURS   12 TIMES .                               00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MSEL1A  PIC X.                                          00001470
             03 MSEL1C  PIC X.                                          00001480
             03 MSEL1P  PIC X.                                          00001490
             03 MSEL1H  PIC X.                                          00001500
             03 MSEL1V  PIC X.                                          00001510
             03 MSEL1O  PIC X.                                          00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MTIERS1A     PIC X.                                     00001540
             03 MTIERS1C     PIC X.                                     00001550
             03 MTIERS1P     PIC X.                                     00001560
             03 MTIERS1H     PIC X.                                     00001570
             03 MTIERS1V     PIC X.                                     00001580
             03 MTIERS1O     PIC X(5).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MNPDTS1A     PIC X.                                     00001610
             03 MNPDTS1C     PIC X.                                     00001620
             03 MNPDTS1P     PIC X.                                     00001630
             03 MNPDTS1H     PIC X.                                     00001640
             03 MNPDTS1V     PIC X.                                     00001650
             03 MNPDTS1O     PIC X(4).                                  00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MDDENV1A     PIC X.                                     00001680
             03 MDDENV1C     PIC X.                                     00001690
             03 MDDENV1P     PIC X.                                     00001700
             03 MDDENV1H     PIC X.                                     00001710
             03 MDDENV1V     PIC X.                                     00001720
             03 MDDENV1O     PIC X(10).                                 00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MSEL2A  PIC X.                                          00001750
             03 MSEL2C  PIC X.                                          00001760
             03 MSEL2P  PIC X.                                          00001770
             03 MSEL2H  PIC X.                                          00001780
             03 MSEL2V  PIC X.                                          00001790
             03 MSEL2O  PIC X.                                          00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MTIERS2A     PIC X.                                     00001820
             03 MTIERS2C     PIC X.                                     00001830
             03 MTIERS2P     PIC X.                                     00001840
             03 MTIERS2H     PIC X.                                     00001850
             03 MTIERS2V     PIC X.                                     00001860
             03 MTIERS2O     PIC X(5).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNPDTS2A     PIC X.                                     00001890
             03 MNPDTS2C     PIC X.                                     00001900
             03 MNPDTS2P     PIC X.                                     00001910
             03 MNPDTS2H     PIC X.                                     00001920
             03 MNPDTS2V     PIC X.                                     00001930
             03 MNPDTS2O     PIC X(4).                                  00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MDDENV2A     PIC X.                                     00001960
             03 MDDENV2C     PIC X.                                     00001970
             03 MDDENV2P     PIC X.                                     00001980
             03 MDDENV2H     PIC X.                                     00001990
             03 MDDENV2V     PIC X.                                     00002000
             03 MDDENV2O     PIC X(10).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLIBERRA  PIC X.                                          00002030
           02 MLIBERRC  PIC X.                                          00002040
           02 MLIBERRP  PIC X.                                          00002050
           02 MLIBERRH  PIC X.                                          00002060
           02 MLIBERRV  PIC X.                                          00002070
           02 MLIBERRO  PIC X(78).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCODTRAA  PIC X.                                          00002100
           02 MCODTRAC  PIC X.                                          00002110
           02 MCODTRAP  PIC X.                                          00002120
           02 MCODTRAH  PIC X.                                          00002130
           02 MCODTRAV  PIC X.                                          00002140
           02 MCODTRAO  PIC X(4).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNOMMAPA  PIC X.                                          00002170
           02 MNOMMAPC  PIC X.                                          00002180
           02 MNOMMAPP  PIC X.                                          00002190
           02 MNOMMAPH  PIC X.                                          00002200
           02 MNOMMAPV  PIC X.                                          00002210
           02 MNOMMAPO  PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
