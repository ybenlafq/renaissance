      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ACC: COMPOSITION DE LA COMMANDE                                 00000020
      ***************************************************************** 00000030
       01   EPC22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSTATUTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEI    PIC X(12).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCDEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCDEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDCDEI    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOMPTEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCOMPTEI      PIC X(8).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSL      COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MRSL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MRSF      PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MRSI      PIC X(25).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCHRONOL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNCHRONOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCHRONOF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCHRONOI      PIC X(15).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTREMISEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MTREMISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTREMISEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTREMISEI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDEURL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCVENDEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCVENDEURF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCVENDEURI     PIC X(6).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBONCDEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNBONCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBONCDEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNBONCDEI      PIC X(24).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSINISTREL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSINISTREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSINISTREF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSINISTREI     PIC X(18).                                 00000610
           02 MLIGNEI OCCURS   4 TIMES .                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLIBELLEI    PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFLOGOL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MREFLOGOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MREFLOGOF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MREFLOGOI    PIC X(10).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPUNITL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MPUNITL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPUNITF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MPUNITI      PIC X(9).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQTEI   PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTHTL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MMTHTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMTHTF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MMTHTI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTVAL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MMTTVAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTVAF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MMTTVAI      PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTTCL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MMTTTCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTTCF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MMTTTCI      PIC X(9).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTCL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MTOTCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTOTCF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MTOTCI    PIC X(9).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREMISEL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MREMISEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREMISEF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MREMISEI  PIC X(9).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTPL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MTOTPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTOTPF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MTOTPI    PIC X(9).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMTTOTALL      COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MMTTOTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMTTOTALF      PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MMTTOTALI      PIC X(9).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(74).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * ACC: COMPOSITION DE LA COMMANDE                                 00001280
      ***************************************************************** 00001290
       01   EPC22O REDEFINES EPC22I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MPAGEA    PIC X.                                          00001470
           02 MPAGEC    PIC X.                                          00001480
           02 MPAGEP    PIC X.                                          00001490
           02 MPAGEH    PIC X.                                          00001500
           02 MPAGEV    PIC X.                                          00001510
           02 MPAGEO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNBPA     PIC X.                                          00001540
           02 MNBPC     PIC X.                                          00001550
           02 MNBPP     PIC X.                                          00001560
           02 MNBPH     PIC X.                                          00001570
           02 MNBPV     PIC X.                                          00001580
           02 MNBPO     PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSTATUTA  PIC X.                                          00001610
           02 MSTATUTC  PIC X.                                          00001620
           02 MSTATUTP  PIC X.                                          00001630
           02 MSTATUTH  PIC X.                                          00001640
           02 MSTATUTV  PIC X.                                          00001650
           02 MSTATUTO  PIC X(10).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNCDEA    PIC X.                                          00001680
           02 MNCDEC    PIC X.                                          00001690
           02 MNCDEP    PIC X.                                          00001700
           02 MNCDEH    PIC X.                                          00001710
           02 MNCDEV    PIC X.                                          00001720
           02 MNCDEO    PIC X(12).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MDCDEA    PIC X.                                          00001750
           02 MDCDEC    PIC X.                                          00001760
           02 MDCDEP    PIC X.                                          00001770
           02 MDCDEH    PIC X.                                          00001780
           02 MDCDEV    PIC X.                                          00001790
           02 MDCDEO    PIC X(10).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNCOMPTEA      PIC X.                                     00001820
           02 MNCOMPTEC PIC X.                                          00001830
           02 MNCOMPTEP PIC X.                                          00001840
           02 MNCOMPTEH PIC X.                                          00001850
           02 MNCOMPTEV PIC X.                                          00001860
           02 MNCOMPTEO      PIC X(8).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MRSA      PIC X.                                          00001890
           02 MRSC      PIC X.                                          00001900
           02 MRSP      PIC X.                                          00001910
           02 MRSH      PIC X.                                          00001920
           02 MRSV      PIC X.                                          00001930
           02 MRSO      PIC X(25).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNCHRONOA      PIC X.                                     00001960
           02 MNCHRONOC PIC X.                                          00001970
           02 MNCHRONOP PIC X.                                          00001980
           02 MNCHRONOH PIC X.                                          00001990
           02 MNCHRONOV PIC X.                                          00002000
           02 MNCHRONOO      PIC X(15).                                 00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTREMISEA      PIC X.                                     00002030
           02 MTREMISEC PIC X.                                          00002040
           02 MTREMISEP PIC X.                                          00002050
           02 MTREMISEH PIC X.                                          00002060
           02 MTREMISEV PIC X.                                          00002070
           02 MTREMISEO      PIC X(5).                                  00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCVENDEURA     PIC X.                                     00002100
           02 MCVENDEURC     PIC X.                                     00002110
           02 MCVENDEURP     PIC X.                                     00002120
           02 MCVENDEURH     PIC X.                                     00002130
           02 MCVENDEURV     PIC X.                                     00002140
           02 MCVENDEURO     PIC X(6).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNBONCDEA      PIC X.                                     00002170
           02 MNBONCDEC PIC X.                                          00002180
           02 MNBONCDEP PIC X.                                          00002190
           02 MNBONCDEH PIC X.                                          00002200
           02 MNBONCDEV PIC X.                                          00002210
           02 MNBONCDEO      PIC X(24).                                 00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MSINISTREA     PIC X.                                     00002240
           02 MSINISTREC     PIC X.                                     00002250
           02 MSINISTREP     PIC X.                                     00002260
           02 MSINISTREH     PIC X.                                     00002270
           02 MSINISTREV     PIC X.                                     00002280
           02 MSINISTREO     PIC X(18).                                 00002290
           02 MLIGNEO OCCURS   4 TIMES .                                00002300
             03 FILLER       PIC X(2).                                  00002310
             03 MLIBELLEA    PIC X.                                     00002320
             03 MLIBELLEC    PIC X.                                     00002330
             03 MLIBELLEP    PIC X.                                     00002340
             03 MLIBELLEH    PIC X.                                     00002350
             03 MLIBELLEV    PIC X.                                     00002360
             03 MLIBELLEO    PIC X(20).                                 00002370
             03 FILLER       PIC X(2).                                  00002380
             03 MREFLOGOA    PIC X.                                     00002390
             03 MREFLOGOC    PIC X.                                     00002400
             03 MREFLOGOP    PIC X.                                     00002410
             03 MREFLOGOH    PIC X.                                     00002420
             03 MREFLOGOV    PIC X.                                     00002430
             03 MREFLOGOO    PIC X(10).                                 00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MPUNITA      PIC X.                                     00002460
             03 MPUNITC PIC X.                                          00002470
             03 MPUNITP PIC X.                                          00002480
             03 MPUNITH PIC X.                                          00002490
             03 MPUNITV PIC X.                                          00002500
             03 MPUNITO      PIC X(9).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MQTEA   PIC X.                                          00002530
             03 MQTEC   PIC X.                                          00002540
             03 MQTEP   PIC X.                                          00002550
             03 MQTEH   PIC X.                                          00002560
             03 MQTEV   PIC X.                                          00002570
             03 MQTEO   PIC X(5).                                       00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MMTHTA  PIC X.                                          00002600
             03 MMTHTC  PIC X.                                          00002610
             03 MMTHTP  PIC X.                                          00002620
             03 MMTHTH  PIC X.                                          00002630
             03 MMTHTV  PIC X.                                          00002640
             03 MMTHTO  PIC X(8).                                       00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MMTTVAA      PIC X.                                     00002670
             03 MMTTVAC PIC X.                                          00002680
             03 MMTTVAP PIC X.                                          00002690
             03 MMTTVAH PIC X.                                          00002700
             03 MMTTVAV PIC X.                                          00002710
             03 MMTTVAO      PIC X(8).                                  00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MMTTTCA      PIC X.                                     00002740
             03 MMTTTCC PIC X.                                          00002750
             03 MMTTTCP PIC X.                                          00002760
             03 MMTTTCH PIC X.                                          00002770
             03 MMTTTCV PIC X.                                          00002780
             03 MMTTTCO      PIC X(9).                                  00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MTOTCA    PIC X.                                          00002810
           02 MTOTCC    PIC X.                                          00002820
           02 MTOTCP    PIC X.                                          00002830
           02 MTOTCH    PIC X.                                          00002840
           02 MTOTCV    PIC X.                                          00002850
           02 MTOTCO    PIC X(9).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MREMISEA  PIC X.                                          00002880
           02 MREMISEC  PIC X.                                          00002890
           02 MREMISEP  PIC X.                                          00002900
           02 MREMISEH  PIC X.                                          00002910
           02 MREMISEV  PIC X.                                          00002920
           02 MREMISEO  PIC X(9).                                       00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MTOTPA    PIC X.                                          00002950
           02 MTOTPC    PIC X.                                          00002960
           02 MTOTPP    PIC X.                                          00002970
           02 MTOTPH    PIC X.                                          00002980
           02 MTOTPV    PIC X.                                          00002990
           02 MTOTPO    PIC X(9).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MMTTOTALA      PIC X.                                     00003020
           02 MMTTOTALC PIC X.                                          00003030
           02 MMTTOTALP PIC X.                                          00003040
           02 MMTTOTALH PIC X.                                          00003050
           02 MMTTOTALV PIC X.                                          00003060
           02 MMTTOTALO      PIC X(9).                                  00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(74).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
