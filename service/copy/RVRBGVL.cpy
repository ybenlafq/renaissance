      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBGVL AJOUT DE LIGNES DANS GV          *        
      *----------------------------------------------------------------*        
       01  RVRBGVL.                                                             
           05  RBGVL-CTABLEG2    PIC X(15).                                     
           05  RBGVL-CTABLEG2-REDEF REDEFINES RBGVL-CTABLEG2.                   
               10  RBGVL-CDECL           PIC X(07).                             
               10  RBGVL-SEQ             PIC X(02).                             
           05  RBGVL-WTABLEG     PIC X(80).                                     
           05  RBGVL-WTABLEG-REDEF  REDEFINES RBGVL-WTABLEG.                    
               10  RBGVL-FLAG            PIC X(01).                             
               10  RBGVL-AJOUTT OCCURS 6.                                       
                  15  RBGVL-AJOUT           PIC X(07).                          
                  15  RBGVL-AJOUTQ          PIC 9(02).                          
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBGVL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBGVL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBGVL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBGVL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBGVL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
