      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE010 AU 01/02/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,05,BI,A,                          *        
      *                           27,07,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE010.                                                        
            05 NOMETAT-IHE010           PIC X(6) VALUE 'IHE010'.                
            05 RUPTURES-IHE010.                                                 
           10 IHE010-CLIEUHET           PIC X(05).                      007  005
           10 IHE010-CTRAIT             PIC X(05).                      012  005
           10 IHE010-CMARQ              PIC X(05).                      017  005
           10 IHE010-CFAM               PIC X(05).                      022  005
           10 IHE010-NCODIC             PIC X(07).                      027  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE010-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 IHE010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE010.                                                   
           10 IHE010-LREFFOURN          PIC X(20).                      036  020
           10 IHE010-LTRAIT             PIC X(20).                      056  020
           10 IHE010-WVALEUR            PIC S9(07)V9(2) COMP-3.         076  005
           10 IHE010-WQTE               PIC S9(05)     .                081  005
            05 FILLER                      PIC X(427).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE010-LONG           PIC S9(4)   COMP  VALUE +085.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE010-LONG           PIC S9(4) COMP-5  VALUE +085.           
                                                                                
      *}                                                                        
