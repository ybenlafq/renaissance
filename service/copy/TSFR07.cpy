      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FR07-RECORD.                                                      
           05 TS-FR07-TABLE            OCCURS 09.                               
              10 TS-FR07-CPROG             PIC X(05).                           
              10 TS-FR07-SOCORI            PIC X(03).                           
              10 TS-FR07-LIEUORI           PIC X(03).                           
              10 TS-FR07-SOCDST            PIC X(03).                           
              10 TS-FR07-LIEUDST           PIC X(03).                           
              10 TS-FR07-SOCVTE            PIC X(03).                           
              10 TS-FR07-LIEUVTE           PIC X(03).                           
              10 TS-FR07-INTITULE          PIC X(30).                           
                                                                                
