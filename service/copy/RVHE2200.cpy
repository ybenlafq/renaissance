      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHE2200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHE2200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE2200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE2200.                                                            
      *}                                                                        
           02  HE22-CTIERS                                                      
               PIC X(0005).                                                     
           02  HE22-CLIEUHET                                                    
               PIC X(0005).                                                     
           02  HE22-NENVOI                                                      
               PIC X(0007).                                                     
           02  HE22-DENVOI                                                      
               PIC X(0008).                                                     
           02  HE22-QENVOI                                                      
               PIC S9(7) COMP-3.                                                
           02  HE22-WFLAG                                                       
               PIC X(0001).                                                     
           02  HE22-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHE2200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHE2200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHE2200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-CTIERS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-CTIERS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-CLIEUHET-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-CLIEUHET-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-NENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-NENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-DENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-DENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-QENVOI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-QENVOI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HE22-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HE22-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
