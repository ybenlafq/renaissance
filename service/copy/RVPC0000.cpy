      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTPC00                        *        
      ******************************************************************        
       01  RVPC0000.                                                            
      *    *************************************************************        
      *                       CODE APPLICATION                                  
           10 PC00-CAPPLI          PIC X(10).                                   
      *    *************************************************************        
      *                       CODE FONCTION                                     
           10 PC00-CFONCTION       PIC X(10).                                   
      *    *************************************************************        
      *                       DATE DE DEBUT DE TRAITEMENT                       
           10 PC00-DDEBUT          PIC X(8).                                    
      *    *************************************************************        
      *                       HEURE DE DEBUT DE TRAITEMENT                      
           10 PC00-HDEBUT          PIC X(6).                                    
      *    *************************************************************        
      *                       SOCIETE                                           
           10 PC00-NSOC            PIC X(3).                                    
      *    *************************************************************        
      *                       LIEU                                              
           10 PC00-NLIEU           PIC X(3).                                    
      *    *************************************************************        
      *                       LIBELLE                                           
           10 PC00-LIBELLE         PIC X(30).                                   
      *    *************************************************************        
      *                       NBRE ENREGISTREMENTS LUS                          
           10 PC00-NBENREGL        PIC S9(7)V USAGE COMP-3.                     
      *    *************************************************************        
      *                       NBRE ENREGISTREMENTS TRAITES                      
           10 PC00-NBENREGT        PIC S9(7)V USAGE COMP-3.                     
      *    *************************************************************        
      *                       STATUT DE TRAITEMENT                              
           10 PC00-STATUT          PIC X(2).                                    
      *    *************************************************************        
      *                       DATE SYSTEME M.A.J.                               
           10 PC00-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
       01  RVSV0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-CAPPLI-F            PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-CAPPLI-F            PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-CFONCTION-F         PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-CFONCTION-F         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-DDEBUT-F            PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-DDEBUT-F            PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-HDEBUT-F            PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-HDEBUT-F            PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-NSOC-F              PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-NSOC-F              PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-NLIEU-F             PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-NLIEU-F             PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-LIBELLE-F          PIC S9(4) COMP.                           
      *--                                                                       
           10 PC00-LIBELLE-F          PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-NBENREGL-F          PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-NBENREGL-F          PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-NBENREGT-F          PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-NBENREGT-F          PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-STATUT-F            PIC S9(4) COMP.                          
      *--                                                                       
           10 PC00-STATUT-F            PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PC00-DSYST-F             PIC S9(4) COMP.                          
      *                                                                         
      *--                                                                       
           10 PC00-DSYST-F             PIC S9(4) COMP-5.                        
                                                                                
      *}                                                                        
