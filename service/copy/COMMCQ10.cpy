      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *  TRANSACTION: CQ10                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION CQ10                  *        
      *  LONGUEUR   : 4096                                             *        
      ******************************************************************        
      *                                                                         
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CQ10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-CQ10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      ******* ZONES APPLICATIVES CQ10   --------------------------- 95          
           02 COMM-CQ10-ZONES.                                                  
                 05 COMM-CQ10-PROGPREC        PIC X(05).                00742002
                 05 COMM-CQ10-NSOCIETE        PIC X(03).                00742002
                 05 COMM-CQ10-NLIEU           PIC X(03).                00743002
                 05 COMM-CQ10-LLIEU           PIC X(20).                00744002
                 05 COMM-CQ10-NSOCPTF         PIC X(03).                00745002
                 05 COMM-CQ10-NPTF            PIC X(03).                00748002
                 05 COMM-CQ10-LPTF            PIC X(20).                00748002
                 05 COMM-CQ10-PAGE            PIC 9(04).                        
                 05 COMM-CQ10-NBPAGES         PIC 9(04).                        
                 05 COMM-MAJ-NCODIC           PIC X(07).                        
                 05 COMM-MAJ-NVENTE           PIC X(07).                        
                 05 COMM-MAJ-STVTE            PIC X.                            
                 05 COMM-MAJ-HD               PIC X.                            
                 05 COMM-MAJ-CUIS             PIC X.                            
                 05 COMM-MAJ-SOCMAG           PIC XXX.                          
                 05 COMM-MAJ-MAG              PIC XXX.                          
                 05 COMM-MAJ-SOCPTF           PIC XXX.                          
                 05 COMM-MAJ-PTF              PIC XXX.                          
      *****      INDICATEUR SI TS TROP LONGUE                                   
                 05 COMM-CQ10-TSLONG          PIC 9(01).                        
                    88 TS-PAS-TROP-LONGUE                       VALUE 0.        
                    88 TS-TROP-LONGUE                           VALUE 1.        
      *                                                                         
           02 COMM-FILLER                     PIC X(3629).              00670000
      *                                                                         
                                                                                
