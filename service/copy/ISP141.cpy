      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP141 AU 21/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,PD,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,07,BI,A,                          *        
      *                           47,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP141.                                                        
            05 NOMETAT-ISP141           PIC X(6) VALUE 'ISP141'.                
            05 RUPTURES-ISP141.                                                 
           10 ISP141-COULEUR            PIC X(05).                      007  005
           10 ISP141-WSEQFAM            PIC S9(05)      COMP-3.         012  003
           10 ISP141-CMARQ              PIC X(05).                      015  005
           10 ISP141-LREFFOURN          PIC X(20).                      020  020
           10 ISP141-NCODIC             PIC X(07).                      040  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP141-SEQUENCE           PIC S9(04) COMP.                047  002
      *--                                                                       
           10 ISP141-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP141.                                                   
           10 ISP141-CFAM               PIC X(05).                      049  005
           10 ISP141-DTRAIT             PIC X(10).                      054  010
           10 ISP141-LLIEU              PIC X(20).                      064  020
           10 ISP141-NLIEU              PIC X(03).                      084  003
           10 ISP141-NSOCIETE           PIC X(03).                      087  003
           10 ISP141-NZONPRIX           PIC X(02).                      090  002
           10 ISP141-DEFFET             PIC X(08).                      092  008
           10 ISP141-DFINEFFET          PIC X(08).                      100  008
            05 FILLER                      PIC X(405).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP141-LONG           PIC S9(4)   COMP  VALUE +107.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP141-LONG           PIC S9(4) COMP-5  VALUE +107.           
                                                                                
      *}                                                                        
