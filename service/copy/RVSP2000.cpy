      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSP2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSP2000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSP2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSP2000.                                                            
      *}                                                                        
           02  SP20-NCODIC                                                      
               PIC X(0007).                                                     
           02  SP20-DTRAITEMENT                                                 
               PIC X(0008).                                                     
           02  SP20-CPROG                                                       
               PIC X(0006).                                                     
           02  SP20-CACID                                                       
               PIC X(0008).                                                     
           02  SP20-DEFFET                                                      
               PIC X(0008).                                                     
           02  SP20-PSRPTTC                                                     
               PIC S9(7)V9(0006) COMP-3.                                        
           02  SP20-PVTTC                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  SP20-LCOMMENT                                                    
               PIC X(0005).                                                     
           02  SP20-NZONPRIX                                                    
               PIC X(0002).                                                     
           02  SP20-NCONC                                                       
               PIC X(0004).                                                     
           02  SP20-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  SP20-NLIEU                                                       
               PIC X(0003).                                                     
           02  SP20-NCDE                                                        
               PIC X(0007).                                                     
           02  SP20-NREC                                                        
               PIC X(0007).                                                     
           02  SP20-CAPPRO                                                      
               PIC X(0005).                                                     
           02  SP20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSP2000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSP2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSP2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-DTRAITEMENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-DTRAITEMENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-CPROG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-PSRPTTC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-PSRPTTC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-PVTTC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-PVTTC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NCONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NCONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SP20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SP20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
