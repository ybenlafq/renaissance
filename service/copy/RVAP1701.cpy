      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1700                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1700.                                                            
           02  AP17-NSEQID         PIC S9(18) COMP-3.                           
           02  AP17-CODEANO        PIC X(0002).                                 
           02  AP17-DATEANO        PIC X(0008).                                 
           02  AP17-CSTATUT        PIC X(0003).                                 
           02  AP17-DATETRAIT      PIC X(0008).                                 
           02  AP17-LCOMMENT       PIC X(0050).                                 
           02  AP17-NIDRELANCE     PIC S9(18) COMP-3.                           
           02  AP17-NIDFACT        PIC S9(18) COMP-3.                           
           02  AP17-NCLIENT        PIC X(0008).                                 
           02  AP17-NVTEFACT       PIC X(0014).                                 
           02  AP17-NLVTEFACT      PIC X(0003).                                 
           02  AP17-CPRESTA        PIC X(0005).                                 
           02  AP17-PPRESTA        PIC S9(7)V9(2) COMP-3.                       
           02  AP17-NIDVTEA2I      PIC S9(18) COMP-3.                           
           02  AP17-NVTEA2I        PIC X(0014).                                 
           02  AP17-NLVTEA2I       PIC X(0003).                                 
           02  AP17-NIDCRI         PIC S9(18) COMP-3.                           
           02  AP17-NCRI           PIC X(0017).                                 
           02  AP17-DSYST          PIC S9(13)V USAGE COMP-3.                    
           02  AP17-NSOCA2I        PIC X(03).                                   
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-CODEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-CODEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-DATEANO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-DATEANO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-CSTATUT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-CSTATUT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-DATETRAIT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-DATETRAIT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-LCOMMENT-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-LCOMMENT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NIDRELANCE-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NIDRELANCE-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NIDFACT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NIDFACT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NCLIENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NCLIENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NVTEFACT-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NVTEFACT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NLVTEFACT-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NLVTEFACT-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-CPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-CPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-PPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-PPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NIDVTEA2I-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NIDVTEA2I-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NVTEA2I-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NVTEA2I-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NLVTEA2I-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NLVTEA2I-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NIDCRI-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NIDCRI-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NCRI-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-NCRI-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-DSYST-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP17-DSYST-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP17-NSOCA2I-F      PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP17-NSOCA2I-F      PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
