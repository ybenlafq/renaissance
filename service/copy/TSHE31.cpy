      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *            TS DES PROGRAMMES THE31 ET MHE31                             
       01                 :X:-NOM.                                              
            02            FILLER           PIC X(04) VALUE 'HE31'.              
            02            :X:-TERM         PIC X(04) VALUE  SPACE.              
      *            TAILLE DE LA COMMAREA                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01               TS-LONG           PIC S9(4) COMP VALUE +1800.           
      *--                                                                       
       01               :X:-LONG           PIC S9(4) COMP-5 VALUE +1800.        
      *}                                                                        
      *            DONNEES DE LA COMMAREA                                       
       01                 :X:-DATA.                                             
                 03       :X:-LIGNE                  OCCURS    12.              
                    04    :X:-NLIEUHED     PIC X(03).                           
                    04    :X:-NGHE         PIC X(07).                           
                    04    :X:-NCODIC       PIC X(07).                           
                    04    :X:-CFAM         PIC X(05).                           
                    04    :X:-CMARQ        PIC X(05).                           
                    04    :X:-LREFFOURN    PIC X(15).                           
                    04    :X:-LPANNE       PIC X(20).                           
                    04    :X:-CPANNE       PIC X(05).                           
                    04    :X:-CREFUS       PIC X(05).                           
                    04    :X:-CTRAIT       PIC X(05).                           
                    04    :X:-CCTRT        PIC X(05).                           
                    04    :X:-NFA          PIC X(07).                           
                    04    :X:-CTYPHS       PIC X(05).                           
                    04    :X:-DDEPOT       PIC X(08).                           
                    04    :X:-ORIG.                                             
                      05  :X:-NSOCIETE     PIC X(03).                           
                      05  :X:-NLIEU        PIC X(03).                           
                      05  :X:-NSSLIEU      PIC X(03).                           
                      05  :X:-CLIEUTRT     PIC X(05).                           
                    04    :X:-TOPANO       PIC X(01).                           
      *               GESTION DE LA MODIF. DE CODIC                             
                    04    :X:-MAJ-NCODIC   PIC X(01).                           
                    04    :X:-NCODIC-NEW   PIC X(07).                           
                    04    :X:-CFAM-NEW     PIC X(05).                           
                    04    :X:-CMARQ-NEW    PIC X(05).                           
                    04    :X:-LREFFOURN-NEW PIC X(15).                          
      *                                                                         
      ********** F I N  D E  L A  C O M M A R E A  "T S H E 3 1" *******        
                                                                                
