      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00002000
      *                                                            *    00008900
      * COMMAREA SPECIFIQUE PRG MBC6B                              *    00002209
      *                                                            *    00008900
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BC6B-LONG-COMMAREA PIC S9(4) COMP VALUE +400.                   
      *--                                                                       
       01  COMM-BC6B-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +400.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA-MBC6B.                                                    
      *                                                                         
      *---   ZONE APPLICATIVE                                                   
          02 COMM-DONNEES-APPLI.                                                
      *---      INFO CICS                                                       
             05 COMM-MBC6B-APPLID        PIC X(08).                             
             05 COMM-MBC6B-NSOCCICS      PIC X(03).                             
             05 COMM-MBC6B-SSAAMMJJ      PIC X(08).                             
      *------------------------------ ZONE DONNEES MBC6B                        
          02 COMM-DONNEES-MBC6B.                                                
      *---     NUMERO DE SOCIETE                                                
             05 COMM-MBC6B-NSOC          PIC X(03).                             
      *---     NUMERO DE LIEU                                                   
             05 COMM-MBC6B-NLIEU         PIC X(03).                             
      *---     NUMERO D ORDRE                                                   
             05 COMM-MBC6B-NORDRE        PIC X(05).                             
      *---     DATE DE VENTE (DATE DE JOUR)                                     
             05 COMM-MBC6B-DVENTE        PIC X(08).                             
      *---     CODE VENDEUR                                                     
             05 COMM-MBC6B-CVENDEUR      PIC X(06).                             
      *---     NUMERO D ADRESSE                                                 
             05 COMM-MBC6B-NADR          PIC X(08).                             
      *---      QUESTION                                                        
             05 COMM-MBC6B-QUESTION      PIC X(75).                             
      *---      IDCLIENT                                                        
             05 COMM-MBC6B-IDCLIENT      PIC X(08).                             
      *---      IDQUESTION                                                      
             05 COMM-MBC6B-IDQUESTION    PIC X(05).                             
      *---      SOURCE                                                          
             05 COMM-MBC6B-SOURCE        PIC X(05).                             
      *---      TELEPHONE FIXE                                                  
             05 COMM-MBC6B-TEL           PIC X(15).                             
      *---      TELEPHONE GSM                                                   
             05 COMM-MBC6B-GSM           PIC X(15).                             
      *---      EMAIL                                                           
             05 COMM-MBC6B-EMAIL         PIC X(65).                             
      *------------------------------ ZONE RETOUR MBC6B                         
          02 COMM-MBC6B-RETOUR.                                                 
             05 COMM-MBC6B-RETOUR-MQ.                                           
                 10 COMM-MBC6B-CODRETMQ      PIC X(01).                         
                      88  COMM-MBC6B-MQ-OK     VALUE ' '.                       
                      88  COMM-MBC6B-MQ-ERREUR VALUE '1'.                       
                 10 COMM-MBC6B-LIBERRMQ      PIC X(60).                         
             05 COMM-BC6B-RETOUR-BC.                                            
                 10 COMM-MBC6B-CODRETBC      PIC X(01).                         
                      88  COMM-MBC6B-BC-OK     VALUE ' '.                       
                      88  COMM-MBC6B-BC-ERREUR VALUE '1'.                       
                 10 COMM-MBC6B-LIBERRBC      PIC X(60).                         
      *---      FILLER                                                          
          02 FILLER                     PIC X(038).                             
      *                                                                         
                                                                                
