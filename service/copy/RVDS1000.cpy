      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVDS1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVDS1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVDS1000.                                                            
           02  DS10-CFAM                                                        
               PIC X(0005).                                                     
           02  DS10-QTAUX1                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  DS10-QTAUX2                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  DS10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVDS1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVDS1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS10-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS10-QTAUX1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS10-QTAUX1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS10-QTAUX2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS10-QTAUX2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
