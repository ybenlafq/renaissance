      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    COPIE DE LA TS UTILIISEE DANS LE PROGRAMME TRB81 : TSRB81   *        
      *----------------------------------------------------------------*        
       01  TS-RB81-IDENT.                                                       
           05  TS-RB81-TERMID              PIC X(04).                           
           05  TS-RB81-TRANID              PIC X(04).                           
       01  FILLER                    REDEFINES TS-RB81-IDENT.                   
           05  TS-RB81-NAME                PIC X(08).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RANG-TS-RB81                    PIC S9(4) COMP.                      
      *--                                                                       
       01  RANG-TS-RB81                    PIC S9(4) COMP-5.                    
      *}                                                                        
       01  TS-RB81-DATA.                                                        
           05  TS-RB81-LIGNE               OCCURS 14.                           
               10  TS-RB81-NSERIE          PIC X(16).                           
               10  TS-RB81-NIMMOB          PIC X(10).                           
               10  TS-RB81-NCODIC          PIC X(07).                           
               10  TS-RB81-DATMVT.                                              
                   15  TS-RB81-MVTJOUR     PIC X(02).                           
                   15  TS-RB81-FILLE1      PIC X(01).                           
                   15  TS-RB81-MVTMOIS     PIC X(02).                           
                   15  TS-RB81-FILLE2      PIC X(01).                           
MBEN00*            15  TS-RB81-MVTSSAA     PIC X(04).                           
MBEN00             15  TS-RB81-MVTAA       PIC X(02).                           
MBEN00*        10  TS-RB81-NIDENT          PIC X(07).                           
MBEN00         10  TS-RB81-NIDENT          PIC X(14).                           
               10  TS-RB81-CODFER          PIC X(01).                           
               10  TS-RB81-REFERE          PIC X(20).                           
               10  TS-RB81-MODIF           PIC X(01).                           
      *                                                                         
      *--- FIN DE LA TS TSRB81 ----------------------------------------*        
                                                                                
