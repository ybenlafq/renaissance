      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM065 AU 26/08/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,08,BI,A,                          *        
      *                           29,08,BI,A,                          *        
      *                           37,07,BI,A,                          *        
      *                           44,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM065.                                                        
            05 NOMETAT-INM065           PIC X(6) VALUE 'INM065'.                
            05 RUPTURES-INM065.                                                 
           10 INM065-DATETR             PIC X(08).                      007  008
           10 INM065-NSOC               PIC X(03).                      015  003
           10 INM065-NLIEU              PIC X(03).                      018  003
           10 INM065-NTRANS             PIC X(08).                      021  008
           10 INM065-NVENTE             PIC X(08).                      029  008
           10 INM065-NCODIC             PIC X(07).                      037  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM065-SEQUENCE           PIC S9(04) COMP.                044  002
      *--                                                                       
           10 INM065-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM065.                                                   
           10 INM065-DEVREF             PIC X(15).                      046  015
           10 INM065-QTE                PIC X(05).                      061  005
           10 INM065-PMONTANT           PIC S9(07)V9(2) COMP-3.         066  005
           10 INM065-PTVA               PIC S9(03)V9(2) COMP-3.         071  003
            05 FILLER                      PIC X(439).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM065-LONG           PIC S9(4)   COMP  VALUE +073.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM065-LONG           PIC S9(4) COMP-5  VALUE +073.           
                                                                                
      *}                                                                        
