      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IABTSL AU 08/03/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,08,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IABTSL.                                                        
            05 NOMETAT-IABTSL           PIC X(6) VALUE 'IABTSL'.                
            05 RUPTURES-IABTSL.                                                 
           10 IABTSL-LIBELLE            PIC X(05).                      007  005
           10 IABTSL-DOPER              PIC X(08).                      012  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IABTSL-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IABTSL-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IABTSL.                                                   
           10 IABTSL-LIEU               PIC X(03).                      022  003
           10 IABTSL-SOCIETE            PIC X(03).                      025  003
           10 IABTSL-TEMPS              PIC X(13).                      028  013
           10 IABTSL-QMVT               PIC S9(05)      COMP-3.         041  003
            05 FILLER                      PIC X(469).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IABTSL-LONG           PIC S9(4)   COMP  VALUE +043.           
      *                                                                         
      *--                                                                       
        01  DSECT-IABTSL-LONG           PIC S9(4) COMP-5  VALUE +043.           
                                                                                
      *}                                                                        
