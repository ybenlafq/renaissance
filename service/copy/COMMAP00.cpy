      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *                      COMMAREA DA2I                                      
      **************************************************************            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-AP00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-AP00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES AIDA ------------------------------------- 100          
     1    02 FILLER-COM-AIDA      PIC  X(100).                                  
      * ZONES RESERVEES CICS ------------------------------------- 020          
   101    02 COMM-CICS-APPLID     PIC  X(8).                                    
   109    02 COMM-CICS-NETNAM     PIC  X(8).                                    
   117    02 COMM-CICS-TRANSA     PIC  X(4).                                    
      * DATE DU JOUR --------------------------------------------- 100          
   121    02 COMM-DATE-SIECLE     PIC  99.                                      
   123    02 COMM-DATE-ANNEE      PIC  99.                                      
   125    02 COMM-DATE-MOIS       PIC  99.                                      
   127    02 COMM-DATE-JOUR       PIC  99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
   129    02 COMM-DATE-QNTA       PIC  999.                                     
   132    02 COMM-DATE-QNT0       PIC  99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
   137    02 COMM-DATE-BISX       PIC  9.                                       
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
   138    02 COMM-DATE-JSM        PIC  9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
   139    02 COMM-DATE-JSM-LC     PIC  XXX.                                     
   142    02 COMM-DATE-JSM-LL     PIC  X(8).                                    
      *   LIBELLES DU MOIS COURT - LONG                                         
   150    02 COMM-DATE-MOIS-LC    PIC  XXX.                                     
   153    02 COMM-DATE-MOIS-LL    PIC  X(8).                                    
      *   DIFFERENTES FORMES DE DATE                                            
   161    02 COMM-DATE-SSAAMMJJ   PIC  X(8).                                    
   169    02 COMM-DATE-AAMMJJ     PIC  X(6).                                    
   175    02 COMM-DATE-JJMMSSAA   PIC  X(8).                                    
   183    02 COMM-DATE-JJMMAA     PIC  X(6).                                    
   189    02 COMM-DATE-JJ-MM-AA   PIC  X(8).                                    
   197    02 COMM-DATE-JJ-MM-SSAA PIC  X(10).                                   
      *   TRAITEMENT NUMERO DE SEMAINE                                          
          02 COMM-DATE-WEEK.                                                    
   207       05 COMM-DATE-SEMSS   PIC  99.                                      
   209       05 COMM-DATE-SEMAA   PIC  99.                                      
   211       05 COMM-DATE-SEMNU   PIC  99.                                      
   213    02 COMM-DATE-FILLER     PIC  X(08).                                   
      * ATTRIBUTS BMS======================================== 4 + AAAA          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
   221*   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
   223*   02 COMM-AP00-ZMAP       PIC  X.                                       
          02 COMM-AP00-ZMAP       PIC  X(2400).                                 
      * ZONES APPLICATIVES AP00 GENERALES  ====================== BBBB          
          02 COMM-AP00.                                                         
      ******* NIVEAU D'ACCES *******************************************        
             03 COMM-AP00-NOM-PROG  PIC X(5).                                   
      * ZONES APPLICATIVES FT00 ****************************************        
      *   02 COMM-AP00-REDEFINES        PIC X(3000).                            
      *----------------------------------------------------------------*        
      * ZONES APPLICATIVES AP01                                        *        
      *----------------------------------------------------------------*        
      *   02 COMM-AP01 REDEFINES COMM-AP00-REDEFINES.                           
          02 COMM-AP01.                                                         
             03 COMM-AP01-INDMAX           PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP01-PAGE             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP01-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP01-NBP              PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP01-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-AP01-NSOCSAV          PIC X(03).                           
             03 COMM-AP01-NLIEUSAV         PIC X(03).                           
             03 COMM-AP01-NDOSSIER         PIC X(09).                           
             03 COMM-AP01-NSOCVTE          PIC X(03).                           
             03 COMM-AP01-NLIEUVTE         PIC X(03).                           
             03 COMM-AP01-NVTESEL          PIC X(08).                           
             03 COMM-AP01-NCLTSEL          PIC X(08).                           
             03 COMM-AP01-DATESEL          PIC X(10).                           
             03 COMM-AP01-CSTATSEL         PIC X(03).                           
             03 COMM-AP01-CANOSEL          PIC X(02).                           
JC0107       03 COMM-AP01-TRISEL           PIC X(01).                           
      *----------------------------------------------------------------*        
      * ZONES APPLICATIVES AP02                                        *        
      *----------------------------------------------------------------*        
      *   02 COMM-AP02 REDEFINES COMM-AP00-REDEFINES.                           
          02 COMM-AP02.                                                         
             03 COMM-AP02-RPROG            PIC X(05).                           
             03 COMM-AP02-NSEQANO          PIC S9(18) COMP-3.                   
             03 COMM-AP02-CODEANO          PIC X(02).                           
             03 COMM-AP02-CSTATUT          PIC X(03).                           
             03 COMM-AP02-DATEANO          PIC X(10).                           
             03 COMM-AP02-DATETRAIT        PIC X(10).                           
             03 COMM-AP02-NIDFACT          PIC S9(18) COMP-3.                   
             03 COMM-AP02-NCLIENT          PIC X(08).                           
             03 COMM-AP02-NSOCSAV          PIC X(03).                           
             03 COMM-AP02-NLIEUSAV         PIC X(03).                           
             03 COMM-AP02-NIDCRI           PIC S9(18) COMP-3.                   
             03 COMM-AP02-NCRI             PIC X(11).                           
             03 COMM-AP02-NVENTE           PIC X(14).                           
             03 COMM-AP02-NLGVENTE         PIC X(03).                           
             03 COMM-AP02-NDOSSIER         PIC X(09).                           
             03 COMM-AP02-DTOPAGE          PIC X(10).                           
             03 COMM-AP02-CPRESTA          PIC X(05).                           
             03 COMM-AP02-PPRESTA          PIC X(10).                           
             03 COMM-AP02-WREGUL           PIC X(01).                           
             03 COMM-AP02-WSOLDER          PIC X(01).                           
             03 COMM-AP02-NSOCIMP          PIC X(03).                           
             03 COMM-AP02-NLIEUIMP         PIC X(03).                           
             03 COMM-AP02-NVTEIMP          PIC X(08).                           
             03 COMM-AP02-NLGVTEIMP        PIC X(03).                           
             03 COMM-AP02-COMMENT          PIC X(50).                           
             03 COMM-AP02-WEMIS            PIC X(01).                           
             03 COMM-AP02-MESS             PIC X(45).                           
          02 COMM-AP03.                                                         
             03 COMM-AP03-RPROG            PIC X(05).                           
             03 COMM-AP03-INDMAX           PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP03-PAGE             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP03-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP03-NBP              PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP03-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-AP03-NUMCL            PIC X(08).                           
             03 COMM-AP03-NOMCL            PIC X(44).                           
PV0508       03 COMM-AP03-CDPOSTAL         PIC X(05).                           
             03 COMM-AP03-NVENTE           PIC X(14).                           
             03 COMM-AP03-NCRI             PIC X(17).                           
             03 COMM-AP03-NFACTURE         PIC X(06).                           
             03 COMM-AP03-CHOIX            PIC X(01).                           
          02 COMM-AP2A.                                                         
             03 COMM-AP2A-RPROG            PIC X(05).                           
             03 COMM-AP2A-INDMAX           PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP2A-PAGE             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP2A-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP2A-NBP              PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP2A-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-AP2A-GAUCHE-DROITE    PIC X VALUE 'O'.                     
              88 COMM-AP2A-DROITE                VALUE 'N'.                     
              88 COMM-AP2A-GAUCHE                VALUE 'O'.                     
          02 COMM-AP3A.                                                         
             03 COMM-AP3A-RPROG            PIC X(05).                           
             03 COMM-AP3A-INDMAX           PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP3A-PAGE             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP3A-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP3A-NBP              PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP3A-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-AP3A-GAUCHE-DROITE    PIC X VALUE 'O'.                     
              88 COMM-AP3A-DROITE                VALUE 'N'.                     
              88 COMM-AP3A-GAUCHE                VALUE 'O'.                     
          02 COMM-AP04.                                                         
             03 COMM-AP04-RPROG            PIC X(05).                           
             03 COMM-AP04-INDMAX           PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP04-PAGE             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP04-PAGE             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AP04-NBP              PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP04-NBP              PIC S9(4) COMP-5.                    
      *}                                                                        
             03 COMM-AP04-NCLIENT          PIC X(08).                           
             03 COMM-AP04-NOMCL            PIC X(44).                           
             03 COMM-AP04-CDPOSTAL         PIC X(05).                           
             03 COMM-AP04-ANNEE            PIC X(04).                           
             03 COMM-AP04-MOIS             PIC X(02).                           
             03 COMM-AP04-CHOIX            PIC X(01).                           
          02 COMM-AP2A-BIS.                                                     
JC1206       03 COMM-AP2A-LIGNES           OCCURS 10 TIMES.                     
JC1206         04 COMM-AP2A-LIG            PIC X(78).                           
JC1206    02 COMM-AP3A-BIS.                                                     
JC1206       03 COMM-AP3A-SELECT           OCCURS 10 TIMES.                     
JC1206         04 COMM-AP3A-SEL            PIC X(01).                           
PV0508*----------------------------------------------------------------*        
PV0508* ZONES APPLICATIVES AP04                                        *        
PV0508*----------------------------------------------------------------*        
PV0508*   02 COMM-AP04B REDEFINES COMM-AP00-REDEFINES.                          
PV0508    02 COMM-AP04B.                                                        
PV0508       03 COMM-AP04B-RPROG           PIC X(05).                           
PV0508       03 COMM-AP04B-INDMAX          PIC S9(5) COMP-3.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
PV0508*      03 COMM-AP04B-PAGE            PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP04B-PAGE            PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
PV0508*      03 COMM-AP04B-NBP             PIC S9(4) COMP.                      
      *--                                                                       
             03 COMM-AP04B-NBP             PIC S9(4) COMP-5.                    
      *}                                                                        
PV0508       03 COMM-AP04B-NSOCSEL         PIC X(03).                           
PV0508       03 COMM-AP04B-NLIEUSEL        PIC X(03).                           
PV0508       03 COMM-AP04B-DDATESEL        PIC X(08).                           
PV0508       03 COMM-AP04B-FDATESEL        PIC X(08).                           
                                                                                
