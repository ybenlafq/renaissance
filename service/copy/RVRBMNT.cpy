      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBMNT TARIF PAR SAV ET PRESTATION      *        
      *----------------------------------------------------------------*        
       01  RVRBMNT .                                                            
           05  RBMNT-CTABLEG2    PIC X(15).                                     
           05  RBMNT-CTABLEG2-REDEF REDEFINES RBMNT-CTABLEG2.                   
               10  RBMNT-SAV             PIC X(06).                             
               10  RBMNT-PRESTA          PIC X(05).                             
               10  RBMNT-SEQ             PIC X(03).                             
               10  RBMNT-SEQ-N          REDEFINES RBMNT-SEQ                     
                                         PIC 9(03).                             
           05  RBMNT-WTABLEG     PIC X(80).                                     
           05  RBMNT-WTABLEG-REDEF  REDEFINES RBMNT-WTABLEG.                    
               10  RBMNT-MONTANT         PIC X(09).                             
               10  RBMNT-MONTANT-N      REDEFINES RBMNT-MONTANT                 
                                         PIC 9(07)V9(02).                       
               10  RBMNT-DATEF           PIC X(08).                             
               10  RBMNT-DATEF-N        REDEFINES RBMNT-DATEF                   
                                         PIC 9(08).                             
               10  RBMNT-COMMENT         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBMNT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBMNT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBMNT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBMNT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBMNT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
