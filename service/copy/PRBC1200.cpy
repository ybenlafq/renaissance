      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE BC1200                       
      ******************************************************************        
      *                                                                         
       CLEF-BC1200             SECTION.                                         
      *                                                                         
           MOVE 'RVBC1200          '       TO   TABLE-NAME.                     
           MOVE 'BC1200'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-BC1200. EXIT.                                                   
                EJECT                                                           
                                                                                
