      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHC110 AU 29/08/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,09,BI,A,                          *        
      *                           16,03,PD,A,                          *        
      *                           19,05,BI,A,                          *        
      *                           24,07,BI,A,                          *        
      *                           31,07,BI,A,                          *        
      *                           38,07,BI,A,                          *        
      *                           45,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHC110.                                                        
            05 NOMETAT-IHC110           PIC X(6) VALUE 'IHC110'.                
            05 RUPTURES-IHC110.                                                 
           10 IHC110-TEBRB              PIC X(09).                      007  009
           10 IHC110-WSEQFAM            PIC S9(05)      COMP-3.         016  003
           10 IHC110-CMARQ              PIC X(05).                      019  005
           10 IHC110-NCODIC             PIC X(07).                      024  007
           10 IHC110-LLIEU              PIC X(07).                      031  007
           10 IHC110-NCHS               PIC X(07).                      038  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHC110-SEQUENCE           PIC S9(04) COMP.                045  002
      *--                                                                       
           10 IHC110-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHC110.                                                   
           10 IHC110-ADRESSE            PIC X(08).                      047  008
           10 IHC110-CFAM               PIC X(05).                      055  005
           10 IHC110-LREFFOURN          PIC X(20).                      060  020
           10 IHC110-NLIEUHC            PIC X(03).                      080  003
           10 IHC110-NLIEUHC-E          PIC X(03).                      083  003
           10 IHC110-QMAG               PIC S9(04)      COMP-3.         086  003
           10 IHC110-QTE                PIC S9(03)      COMP-3.         089  002
           10 IHC110-QTOTAL             PIC S9(04)      COMP-3.         091  003
            05 FILLER                      PIC X(419).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHC110-LONG           PIC S9(4)   COMP  VALUE +093.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHC110-LONG           PIC S9(4) COMP-5  VALUE +093.           
                                                                                
      *}                                                                        
