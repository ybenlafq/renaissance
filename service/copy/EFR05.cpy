      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Franchise - REGLES d'EXTRACTION                                 00000020
      ***************************************************************** 00000030
       01   EFR05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
           02 MTABI OCCURS   12 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROGL      COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MCPROGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPROGF      PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MCPROGI      PIC X(5).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCORIL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MSOCORIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCORIF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MSOCORII     PIC X(3).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUORIL    COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLIEUORIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUORIF    PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLIEUORII    PIC X(3).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCDSTL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MSOCDSTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCDSTF     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MSOCDSTI     PIC X(3).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUDSTL    COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MLIEUDSTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUDSTF    PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MLIEUDSTI    PIC X(3).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCVTEL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MSOCVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCVTEF     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MSOCVTEI     PIC X(3).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUVTEL    COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MLIEUVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIEUVTEF    PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MLIEUVTEI    PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCPROGL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCCPROGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCPROGF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCCPROGI  PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCORIL      COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MCSOCORIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCORIF      PIC X.                                     00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCSOCORII      PIC X(3).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUORIL     COMP PIC S9(4).                            00000610
      *--                                                                       
           02 MCLIEUORIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUORIF     PIC X.                                     00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MCLIEUORII     PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCDSTL      COMP PIC S9(4).                            00000650
      *--                                                                       
           02 MCSOCDSTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCDSTF      PIC X.                                     00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCSOCDSTI      PIC X(3).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUDSTL     COMP PIC S9(4).                            00000690
      *--                                                                       
           02 MCLIEUDSTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUDSTF     PIC X.                                     00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCLIEUDSTI     PIC X(3).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCVTEL      COMP PIC S9(4).                            00000730
      *--                                                                       
           02 MCSOCVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSOCVTEF      PIC X.                                     00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCSOCVTEI      PIC X(3).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUVTEL     COMP PIC S9(4).                            00000770
      *--                                                                       
           02 MCLIEUVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUVTEF     PIC X.                                     00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCLIEUVTEI     PIC X(3).                                  00000800
      * ZONE CMD AIDA                                                   00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(79).                                      00000850
      * CODE TRANSACTION                                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      * CICS DE TRAVAIL                                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      * NETNAME                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      * CODE TERMINAL                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(4).                                       00001050
      ***************************************************************** 00001060
      * Franchise - REGLES d'EXTRACTION                                 00001070
      ***************************************************************** 00001080
       01   EFR05O REDEFINES EFR05I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
      * DATE DU JOUR                                                    00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
      * HEURE                                                           00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MPAGEA    PIC X.                                          00001280
           02 MPAGEC    PIC X.                                          00001290
           02 MPAGEP    PIC X.                                          00001300
           02 MPAGEH    PIC X.                                          00001310
           02 MPAGEV    PIC X.                                          00001320
           02 MPAGEO    PIC Z9.                                         00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNBPA     PIC X.                                          00001350
           02 MNBPC     PIC X.                                          00001360
           02 MNBPP     PIC X.                                          00001370
           02 MNBPH     PIC X.                                          00001380
           02 MNBPV     PIC X.                                          00001390
           02 MNBPO     PIC Z9.                                         00001400
           02 MTABO OCCURS   12 TIMES .                                 00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MCPROGA      PIC X.                                     00001430
             03 MCPROGC PIC X.                                          00001440
             03 MCPROGP PIC X.                                          00001450
             03 MCPROGH PIC X.                                          00001460
             03 MCPROGV PIC X.                                          00001470
             03 MCPROGO      PIC X(5).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MSOCORIA     PIC X.                                     00001500
             03 MSOCORIC     PIC X.                                     00001510
             03 MSOCORIP     PIC X.                                     00001520
             03 MSOCORIH     PIC X.                                     00001530
             03 MSOCORIV     PIC X.                                     00001540
             03 MSOCORIO     PIC X(3).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MLIEUORIA    PIC X.                                     00001570
             03 MLIEUORIC    PIC X.                                     00001580
             03 MLIEUORIP    PIC X.                                     00001590
             03 MLIEUORIH    PIC X.                                     00001600
             03 MLIEUORIV    PIC X.                                     00001610
             03 MLIEUORIO    PIC X(3).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MSOCDSTA     PIC X.                                     00001640
             03 MSOCDSTC     PIC X.                                     00001650
             03 MSOCDSTP     PIC X.                                     00001660
             03 MSOCDSTH     PIC X.                                     00001670
             03 MSOCDSTV     PIC X.                                     00001680
             03 MSOCDSTO     PIC X(3).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MLIEUDSTA    PIC X.                                     00001710
             03 MLIEUDSTC    PIC X.                                     00001720
             03 MLIEUDSTP    PIC X.                                     00001730
             03 MLIEUDSTH    PIC X.                                     00001740
             03 MLIEUDSTV    PIC X.                                     00001750
             03 MLIEUDSTO    PIC X(3).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MSOCVTEA     PIC X.                                     00001780
             03 MSOCVTEC     PIC X.                                     00001790
             03 MSOCVTEP     PIC X.                                     00001800
             03 MSOCVTEH     PIC X.                                     00001810
             03 MSOCVTEV     PIC X.                                     00001820
             03 MSOCVTEO     PIC X(3).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MLIEUVTEA    PIC X.                                     00001850
             03 MLIEUVTEC    PIC X.                                     00001860
             03 MLIEUVTEP    PIC X.                                     00001870
             03 MLIEUVTEH    PIC X.                                     00001880
             03 MLIEUVTEV    PIC X.                                     00001890
             03 MLIEUVTEO    PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCCPROGA  PIC X.                                          00001920
           02 MCCPROGC  PIC X.                                          00001930
           02 MCCPROGP  PIC X.                                          00001940
           02 MCCPROGH  PIC X.                                          00001950
           02 MCCPROGV  PIC X.                                          00001960
           02 MCCPROGO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCSOCORIA      PIC X.                                     00001990
           02 MCSOCORIC PIC X.                                          00002000
           02 MCSOCORIP PIC X.                                          00002010
           02 MCSOCORIH PIC X.                                          00002020
           02 MCSOCORIV PIC X.                                          00002030
           02 MCSOCORIO      PIC X(3).                                  00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCLIEUORIA     PIC X.                                     00002060
           02 MCLIEUORIC     PIC X.                                     00002070
           02 MCLIEUORIP     PIC X.                                     00002080
           02 MCLIEUORIH     PIC X.                                     00002090
           02 MCLIEUORIV     PIC X.                                     00002100
           02 MCLIEUORIO     PIC X(3).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCSOCDSTA      PIC X.                                     00002130
           02 MCSOCDSTC PIC X.                                          00002140
           02 MCSOCDSTP PIC X.                                          00002150
           02 MCSOCDSTH PIC X.                                          00002160
           02 MCSOCDSTV PIC X.                                          00002170
           02 MCSOCDSTO      PIC X(3).                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCLIEUDSTA     PIC X.                                     00002200
           02 MCLIEUDSTC     PIC X.                                     00002210
           02 MCLIEUDSTP     PIC X.                                     00002220
           02 MCLIEUDSTH     PIC X.                                     00002230
           02 MCLIEUDSTV     PIC X.                                     00002240
           02 MCLIEUDSTO     PIC X(3).                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCSOCVTEA      PIC X.                                     00002270
           02 MCSOCVTEC PIC X.                                          00002280
           02 MCSOCVTEP PIC X.                                          00002290
           02 MCSOCVTEH PIC X.                                          00002300
           02 MCSOCVTEV PIC X.                                          00002310
           02 MCSOCVTEO      PIC X(3).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCLIEUVTEA     PIC X.                                     00002340
           02 MCLIEUVTEC     PIC X.                                     00002350
           02 MCLIEUVTEP     PIC X.                                     00002360
           02 MCLIEUVTEH     PIC X.                                     00002370
           02 MCLIEUVTEV     PIC X.                                     00002380
           02 MCLIEUVTEO     PIC X(3).                                  00002390
      * ZONE CMD AIDA                                                   00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIBERRA  PIC X.                                          00002420
           02 MLIBERRC  PIC X.                                          00002430
           02 MLIBERRP  PIC X.                                          00002440
           02 MLIBERRH  PIC X.                                          00002450
           02 MLIBERRV  PIC X.                                          00002460
           02 MLIBERRO  PIC X(79).                                      00002470
      * CODE TRANSACTION                                                00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
      * CICS DE TRAVAIL                                                 00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCICSA    PIC X.                                          00002580
           02 MCICSC    PIC X.                                          00002590
           02 MCICSP    PIC X.                                          00002600
           02 MCICSH    PIC X.                                          00002610
           02 MCICSV    PIC X.                                          00002620
           02 MCICSO    PIC X(5).                                       00002630
      * NETNAME                                                         00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MNETNAMA  PIC X.                                          00002660
           02 MNETNAMC  PIC X.                                          00002670
           02 MNETNAMP  PIC X.                                          00002680
           02 MNETNAMH  PIC X.                                          00002690
           02 MNETNAMV  PIC X.                                          00002700
           02 MNETNAMO  PIC X(8).                                       00002710
      * CODE TERMINAL                                                   00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MSCREENA  PIC X.                                          00002740
           02 MSCREENC  PIC X.                                          00002750
           02 MSCREENP  PIC X.                                          00002760
           02 MSCREENH  PIC X.                                          00002770
           02 MSCREENV  PIC X.                                          00002780
           02 MSCREENO  PIC X(4).                                       00002790
                                                                                
