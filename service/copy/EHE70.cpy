      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE70                                                      00000020
      ***************************************************************** 00000030
       01   EHE70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHDL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNLIEUHDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHDF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNLIEUHDI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGHEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNGHEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNGHEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNGHEI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEPOTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEPOTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDDEPOTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTRAITI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPDOSL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPDOSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPDOSF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPDOSI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOSL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNDOSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNDOSF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNDOSI    PIC X(9).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYINASCL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTYINASCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYINASCF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYINASCI      PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODICI  PIC X(7).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAMI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLREFI    PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPHSL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCTYPHSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPHSF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTYPHSI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNSERIEI  PIC X(16).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITCLIL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTITCLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTITCLIF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTITCLII  PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLNOMI    PIC X(25).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLPRENOMI      PIC X(15).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR1L   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLADR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR1F   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLADR1I   PIC X(32).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADR2L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLADR2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLADR2F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLADR2I   PIC X(32).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCPOSTALI      PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLCOMMUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMUF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLCOMMUI  PIC X(26).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTELL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNTELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNTELF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNTELI    PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPANNEL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLPANNEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPANNEF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLPANNEI  PIC X(20).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUHTL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MCLIEUHTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEUHTF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCLIEUHTI      PIC X(5).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRETOURL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MNRETOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRETOURF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNRETOURI      PIC X(7).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRETOURL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MDRETOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRETOURF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDRETOURI      PIC X(10).                                 00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPANNEL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCPANNEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPANNEF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCPANNEI  PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREFUSL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCREFUSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCREFUSF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCREFUSI  PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCTIERSI  PIC X(5).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNACCORDL      COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MNACCORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNACCORDF      PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNACCORDI      PIC X(12).                                 00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMACORL      COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MNOMACORL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNOMACORF      PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNOMACORI      PIC X(10).                                 00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUHIL      COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MCLIEUHIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEUHIF      PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCLIEUHII      PIC X(5).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLOTDIAL      COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MNLOTDIAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLOTDIAF      PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNLOTDIAI      PIC X(7).                                  00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLOTDIAL      COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MDLOTDIAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDLOTDIAF      PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MDLOTDIAI      PIC X(10).                                 00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MNENVOII  PIC X(7).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MDENVOII  PIC X(10).                                      00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINRDUL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MLINRDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINRDUF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MLINRDUI  PIC X(15).                                      00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRENDUL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MNRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRENDUF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MNRENDUI  PIC X(20).                                      00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIDRDUL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MLIDRDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIDRDUF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MLIDRDUI  PIC X(6).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRENDUL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MDRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRENDUF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MDRENDUI  PIC X(10).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNADRL    COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MNADRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNADRF    PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MNADRI    PIC X(7).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAL     COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MNFAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNFAF     PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MNFAI     PIC X(7).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTYOPEL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MNTYOPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTYOPEF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MNTYOPEI  PIC X(3).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSTEL    COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MNSTEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSTEF    PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MNSTEI    PIC X(3).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MNLIEUI   PIC X(3).                                       00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUL      COMP PIC S9(4).                            00001900
      *--                                                                       
           02 MNSSLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSSLIEUF      PIC X.                                     00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MNSSLIEUI      PIC X(3).                                  00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUTRL      COMP PIC S9(4).                            00001940
      *--                                                                       
           02 MCLIEUTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEUTRF      PIC X.                                     00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MCLIEUTRI      PIC X(5).                                  00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSOLDEL  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MWSOLDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSOLDEF  PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MWSOLDEI  PIC X.                                          00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002030
           02 FILLER    PIC X(4).                                       00002040
           02 MLIBERRI  PIC X(80).                                      00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MCODTRAI  PIC X(4).                                       00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002100
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002110
           02 FILLER    PIC X(4).                                       00002120
           02 MCICSI    PIC X(5).                                       00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MNETNAMI  PIC X(8).                                       00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MSCREENI  PIC X(4).                                       00002210
      ***************************************************************** 00002220
      * SDF: EHE70                                                      00002230
      ***************************************************************** 00002240
       01   EHE70O REDEFINES EHE70I.                                    00002250
           02 FILLER    PIC X(12).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDATJOUA  PIC X.                                          00002280
           02 MDATJOUC  PIC X.                                          00002290
           02 MDATJOUP  PIC X.                                          00002300
           02 MDATJOUH  PIC X.                                          00002310
           02 MDATJOUV  PIC X.                                          00002320
           02 MDATJOUO  PIC X(10).                                      00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTIMJOUA  PIC X.                                          00002350
           02 MTIMJOUC  PIC X.                                          00002360
           02 MTIMJOUP  PIC X.                                          00002370
           02 MTIMJOUH  PIC X.                                          00002380
           02 MTIMJOUV  PIC X.                                          00002390
           02 MTIMJOUO  PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNLIEUHDA      PIC X.                                     00002420
           02 MNLIEUHDC PIC X.                                          00002430
           02 MNLIEUHDP PIC X.                                          00002440
           02 MNLIEUHDH PIC X.                                          00002450
           02 MNLIEUHDV PIC X.                                          00002460
           02 MNLIEUHDO      PIC 9(3).                                  00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNGHEA    PIC X.                                          00002490
           02 MNGHEC    PIC X.                                          00002500
           02 MNGHEP    PIC X.                                          00002510
           02 MNGHEH    PIC X.                                          00002520
           02 MNGHEV    PIC X.                                          00002530
           02 MNGHEO    PIC 9(7).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MDDEPOTA  PIC X.                                          00002560
           02 MDDEPOTC  PIC X.                                          00002570
           02 MDDEPOTP  PIC X.                                          00002580
           02 MDDEPOTH  PIC X.                                          00002590
           02 MDDEPOTV  PIC X.                                          00002600
           02 MDDEPOTO  PIC X(10).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCTRAITA  PIC X.                                          00002630
           02 MCTRAITC  PIC X.                                          00002640
           02 MCTRAITP  PIC X.                                          00002650
           02 MCTRAITH  PIC X.                                          00002660
           02 MCTRAITV  PIC X.                                          00002670
           02 MCTRAITO  PIC X(5).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MPDOSA    PIC X.                                          00002700
           02 MPDOSC    PIC X.                                          00002710
           02 MPDOSP    PIC X.                                          00002720
           02 MPDOSH    PIC X.                                          00002730
           02 MPDOSV    PIC X.                                          00002740
           02 MPDOSO    PIC 9(3).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNDOSA    PIC X.                                          00002770
           02 MNDOSC    PIC X.                                          00002780
           02 MNDOSP    PIC X.                                          00002790
           02 MNDOSH    PIC X.                                          00002800
           02 MNDOSV    PIC X.                                          00002810
           02 MNDOSO    PIC 9(9).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MTYINASCA      PIC X.                                     00002840
           02 MTYINASCC PIC X.                                          00002850
           02 MTYINASCP PIC X.                                          00002860
           02 MTYINASCH PIC X.                                          00002870
           02 MTYINASCV PIC X.                                          00002880
           02 MTYINASCO      PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MNCODICA  PIC X.                                          00002910
           02 MNCODICC  PIC X.                                          00002920
           02 MNCODICP  PIC X.                                          00002930
           02 MNCODICH  PIC X.                                          00002940
           02 MNCODICV  PIC X.                                          00002950
           02 MNCODICO  PIC X(7).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MCFAMA    PIC X.                                          00002980
           02 MCFAMC    PIC X.                                          00002990
           02 MCFAMP    PIC X.                                          00003000
           02 MCFAMH    PIC X.                                          00003010
           02 MCFAMV    PIC X.                                          00003020
           02 MCFAMO    PIC X(5).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCMARQA   PIC X.                                          00003050
           02 MCMARQC   PIC X.                                          00003060
           02 MCMARQP   PIC X.                                          00003070
           02 MCMARQH   PIC X.                                          00003080
           02 MCMARQV   PIC X.                                          00003090
           02 MCMARQO   PIC X(5).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLREFA    PIC X.                                          00003120
           02 MLREFC    PIC X.                                          00003130
           02 MLREFP    PIC X.                                          00003140
           02 MLREFH    PIC X.                                          00003150
           02 MLREFV    PIC X.                                          00003160
           02 MLREFO    PIC X(20).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCTYPHSA  PIC X.                                          00003190
           02 MCTYPHSC  PIC X.                                          00003200
           02 MCTYPHSP  PIC X.                                          00003210
           02 MCTYPHSH  PIC X.                                          00003220
           02 MCTYPHSV  PIC X.                                          00003230
           02 MCTYPHSO  PIC X(5).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MNSERIEA  PIC X.                                          00003260
           02 MNSERIEC  PIC X.                                          00003270
           02 MNSERIEP  PIC X.                                          00003280
           02 MNSERIEH  PIC X.                                          00003290
           02 MNSERIEV  PIC X.                                          00003300
           02 MNSERIEO  PIC X(16).                                      00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MTITCLIA  PIC X.                                          00003330
           02 MTITCLIC  PIC X.                                          00003340
           02 MTITCLIP  PIC X.                                          00003350
           02 MTITCLIH  PIC X.                                          00003360
           02 MTITCLIV  PIC X.                                          00003370
           02 MTITCLIO  PIC X(5).                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MLNOMA    PIC X.                                          00003400
           02 MLNOMC    PIC X.                                          00003410
           02 MLNOMP    PIC X.                                          00003420
           02 MLNOMH    PIC X.                                          00003430
           02 MLNOMV    PIC X.                                          00003440
           02 MLNOMO    PIC X(25).                                      00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MLPRENOMA      PIC X.                                     00003470
           02 MLPRENOMC PIC X.                                          00003480
           02 MLPRENOMP PIC X.                                          00003490
           02 MLPRENOMH PIC X.                                          00003500
           02 MLPRENOMV PIC X.                                          00003510
           02 MLPRENOMO      PIC X(15).                                 00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MLADR1A   PIC X.                                          00003540
           02 MLADR1C   PIC X.                                          00003550
           02 MLADR1P   PIC X.                                          00003560
           02 MLADR1H   PIC X.                                          00003570
           02 MLADR1V   PIC X.                                          00003580
           02 MLADR1O   PIC X(32).                                      00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MLADR2A   PIC X.                                          00003610
           02 MLADR2C   PIC X.                                          00003620
           02 MLADR2P   PIC X.                                          00003630
           02 MLADR2H   PIC X.                                          00003640
           02 MLADR2V   PIC X.                                          00003650
           02 MLADR2O   PIC X(32).                                      00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MCPOSTALA      PIC X.                                     00003680
           02 MCPOSTALC PIC X.                                          00003690
           02 MCPOSTALP PIC X.                                          00003700
           02 MCPOSTALH PIC X.                                          00003710
           02 MCPOSTALV PIC X.                                          00003720
           02 MCPOSTALO      PIC 9(5).                                  00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLCOMMUA  PIC X.                                          00003750
           02 MLCOMMUC  PIC X.                                          00003760
           02 MLCOMMUP  PIC X.                                          00003770
           02 MLCOMMUH  PIC X.                                          00003780
           02 MLCOMMUV  PIC X.                                          00003790
           02 MLCOMMUO  PIC X(26).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MNTELA    PIC X.                                          00003820
           02 MNTELC    PIC X.                                          00003830
           02 MNTELP    PIC X.                                          00003840
           02 MNTELH    PIC X.                                          00003850
           02 MNTELV    PIC X.                                          00003860
           02 MNTELO    PIC X(8).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MLPANNEA  PIC X.                                          00003890
           02 MLPANNEC  PIC X.                                          00003900
           02 MLPANNEP  PIC X.                                          00003910
           02 MLPANNEH  PIC X.                                          00003920
           02 MLPANNEV  PIC X.                                          00003930
           02 MLPANNEO  PIC X(20).                                      00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MCLIEUHTA      PIC X.                                     00003960
           02 MCLIEUHTC PIC X.                                          00003970
           02 MCLIEUHTP PIC X.                                          00003980
           02 MCLIEUHTH PIC X.                                          00003990
           02 MCLIEUHTV PIC X.                                          00004000
           02 MCLIEUHTO      PIC X(5).                                  00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MNRETOURA      PIC X.                                     00004030
           02 MNRETOURC PIC X.                                          00004040
           02 MNRETOURP PIC X.                                          00004050
           02 MNRETOURH PIC X.                                          00004060
           02 MNRETOURV PIC X.                                          00004070
           02 MNRETOURO      PIC 9(7).                                  00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MDRETOURA      PIC X.                                     00004100
           02 MDRETOURC PIC X.                                          00004110
           02 MDRETOURP PIC X.                                          00004120
           02 MDRETOURH PIC X.                                          00004130
           02 MDRETOURV PIC X.                                          00004140
           02 MDRETOURO      PIC X(10).                                 00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MCPANNEA  PIC X.                                          00004170
           02 MCPANNEC  PIC X.                                          00004180
           02 MCPANNEP  PIC X.                                          00004190
           02 MCPANNEH  PIC X.                                          00004200
           02 MCPANNEV  PIC X.                                          00004210
           02 MCPANNEO  PIC X(5).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MCREFUSA  PIC X.                                          00004240
           02 MCREFUSC  PIC X.                                          00004250
           02 MCREFUSP  PIC X.                                          00004260
           02 MCREFUSH  PIC X.                                          00004270
           02 MCREFUSV  PIC X.                                          00004280
           02 MCREFUSO  PIC X(5).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MCTIERSA  PIC X.                                          00004310
           02 MCTIERSC  PIC X.                                          00004320
           02 MCTIERSP  PIC X.                                          00004330
           02 MCTIERSH  PIC X.                                          00004340
           02 MCTIERSV  PIC X.                                          00004350
           02 MCTIERSO  PIC X(5).                                       00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MNACCORDA      PIC X.                                     00004380
           02 MNACCORDC PIC X.                                          00004390
           02 MNACCORDP PIC X.                                          00004400
           02 MNACCORDH PIC X.                                          00004410
           02 MNACCORDV PIC X.                                          00004420
           02 MNACCORDO      PIC X(12).                                 00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MNOMACORA      PIC X.                                     00004450
           02 MNOMACORC PIC X.                                          00004460
           02 MNOMACORP PIC X.                                          00004470
           02 MNOMACORH PIC X.                                          00004480
           02 MNOMACORV PIC X.                                          00004490
           02 MNOMACORO      PIC X(10).                                 00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MCLIEUHIA      PIC X.                                     00004520
           02 MCLIEUHIC PIC X.                                          00004530
           02 MCLIEUHIP PIC X.                                          00004540
           02 MCLIEUHIH PIC X.                                          00004550
           02 MCLIEUHIV PIC X.                                          00004560
           02 MCLIEUHIO      PIC X(5).                                  00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MNLOTDIAA      PIC X.                                     00004590
           02 MNLOTDIAC PIC X.                                          00004600
           02 MNLOTDIAP PIC X.                                          00004610
           02 MNLOTDIAH PIC X.                                          00004620
           02 MNLOTDIAV PIC X.                                          00004630
           02 MNLOTDIAO      PIC 9(7).                                  00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MDLOTDIAA      PIC X.                                     00004660
           02 MDLOTDIAC PIC X.                                          00004670
           02 MDLOTDIAP PIC X.                                          00004680
           02 MDLOTDIAH PIC X.                                          00004690
           02 MDLOTDIAV PIC X.                                          00004700
           02 MDLOTDIAO      PIC X(10).                                 00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MNENVOIA  PIC X.                                          00004730
           02 MNENVOIC  PIC X.                                          00004740
           02 MNENVOIP  PIC X.                                          00004750
           02 MNENVOIH  PIC X.                                          00004760
           02 MNENVOIV  PIC X.                                          00004770
           02 MNENVOIO  PIC 9(7).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MDENVOIA  PIC X.                                          00004800
           02 MDENVOIC  PIC X.                                          00004810
           02 MDENVOIP  PIC X.                                          00004820
           02 MDENVOIH  PIC X.                                          00004830
           02 MDENVOIV  PIC X.                                          00004840
           02 MDENVOIO  PIC X(10).                                      00004850
           02 FILLER    PIC X(2).                                       00004860
           02 MLINRDUA  PIC X.                                          00004870
           02 MLINRDUC  PIC X.                                          00004880
           02 MLINRDUP  PIC X.                                          00004890
           02 MLINRDUH  PIC X.                                          00004900
           02 MLINRDUV  PIC X.                                          00004910
           02 MLINRDUO  PIC X(15).                                      00004920
           02 FILLER    PIC X(2).                                       00004930
           02 MNRENDUA  PIC X.                                          00004940
           02 MNRENDUC  PIC X.                                          00004950
           02 MNRENDUP  PIC X.                                          00004960
           02 MNRENDUH  PIC X.                                          00004970
           02 MNRENDUV  PIC X.                                          00004980
           02 MNRENDUO  PIC X(20).                                      00004990
           02 FILLER    PIC X(2).                                       00005000
           02 MLIDRDUA  PIC X.                                          00005010
           02 MLIDRDUC  PIC X.                                          00005020
           02 MLIDRDUP  PIC X.                                          00005030
           02 MLIDRDUH  PIC X.                                          00005040
           02 MLIDRDUV  PIC X.                                          00005050
           02 MLIDRDUO  PIC X(6).                                       00005060
           02 FILLER    PIC X(2).                                       00005070
           02 MDRENDUA  PIC X.                                          00005080
           02 MDRENDUC  PIC X.                                          00005090
           02 MDRENDUP  PIC X.                                          00005100
           02 MDRENDUH  PIC X.                                          00005110
           02 MDRENDUV  PIC X.                                          00005120
           02 MDRENDUO  PIC X(10).                                      00005130
           02 FILLER    PIC X(2).                                       00005140
           02 MNADRA    PIC X.                                          00005150
           02 MNADRC    PIC X.                                          00005160
           02 MNADRP    PIC X.                                          00005170
           02 MNADRH    PIC X.                                          00005180
           02 MNADRV    PIC X.                                          00005190
           02 MNADRO    PIC X(7).                                       00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MNFAA     PIC X.                                          00005220
           02 MNFAC     PIC X.                                          00005230
           02 MNFAP     PIC X.                                          00005240
           02 MNFAH     PIC X.                                          00005250
           02 MNFAV     PIC X.                                          00005260
           02 MNFAO     PIC X(7).                                       00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MNTYOPEA  PIC X.                                          00005290
           02 MNTYOPEC  PIC X.                                          00005300
           02 MNTYOPEP  PIC X.                                          00005310
           02 MNTYOPEH  PIC X.                                          00005320
           02 MNTYOPEV  PIC X.                                          00005330
           02 MNTYOPEO  PIC X(3).                                       00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MNSTEA    PIC X.                                          00005360
           02 MNSTEC    PIC X.                                          00005370
           02 MNSTEP    PIC X.                                          00005380
           02 MNSTEH    PIC X.                                          00005390
           02 MNSTEV    PIC X.                                          00005400
           02 MNSTEO    PIC X(3).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MNLIEUA   PIC X.                                          00005430
           02 MNLIEUC   PIC X.                                          00005440
           02 MNLIEUP   PIC X.                                          00005450
           02 MNLIEUH   PIC X.                                          00005460
           02 MNLIEUV   PIC X.                                          00005470
           02 MNLIEUO   PIC X(3).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MNSSLIEUA      PIC X.                                     00005500
           02 MNSSLIEUC PIC X.                                          00005510
           02 MNSSLIEUP PIC X.                                          00005520
           02 MNSSLIEUH PIC X.                                          00005530
           02 MNSSLIEUV PIC X.                                          00005540
           02 MNSSLIEUO      PIC X(3).                                  00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MCLIEUTRA      PIC X.                                     00005570
           02 MCLIEUTRC PIC X.                                          00005580
           02 MCLIEUTRP PIC X.                                          00005590
           02 MCLIEUTRH PIC X.                                          00005600
           02 MCLIEUTRV PIC X.                                          00005610
           02 MCLIEUTRO      PIC X(5).                                  00005620
           02 FILLER    PIC X(2).                                       00005630
           02 MWSOLDEA  PIC X.                                          00005640
           02 MWSOLDEC  PIC X.                                          00005650
           02 MWSOLDEP  PIC X.                                          00005660
           02 MWSOLDEH  PIC X.                                          00005670
           02 MWSOLDEV  PIC X.                                          00005680
           02 MWSOLDEO  PIC X.                                          00005690
           02 FILLER    PIC X(2).                                       00005700
           02 MLIBERRA  PIC X.                                          00005710
           02 MLIBERRC  PIC X.                                          00005720
           02 MLIBERRP  PIC X.                                          00005730
           02 MLIBERRH  PIC X.                                          00005740
           02 MLIBERRV  PIC X.                                          00005750
           02 MLIBERRO  PIC X(80).                                      00005760
           02 FILLER    PIC X(2).                                       00005770
           02 MCODTRAA  PIC X.                                          00005780
           02 MCODTRAC  PIC X.                                          00005790
           02 MCODTRAP  PIC X.                                          00005800
           02 MCODTRAH  PIC X.                                          00005810
           02 MCODTRAV  PIC X.                                          00005820
           02 MCODTRAO  PIC X(4).                                       00005830
           02 FILLER    PIC X(2).                                       00005840
           02 MCICSA    PIC X.                                          00005850
           02 MCICSC    PIC X.                                          00005860
           02 MCICSP    PIC X.                                          00005870
           02 MCICSH    PIC X.                                          00005880
           02 MCICSV    PIC X.                                          00005890
           02 MCICSO    PIC X(5).                                       00005900
           02 FILLER    PIC X(2).                                       00005910
           02 MNETNAMA  PIC X.                                          00005920
           02 MNETNAMC  PIC X.                                          00005930
           02 MNETNAMP  PIC X.                                          00005940
           02 MNETNAMH  PIC X.                                          00005950
           02 MNETNAMV  PIC X.                                          00005960
           02 MNETNAMO  PIC X(8).                                       00005970
           02 FILLER    PIC X(2).                                       00005980
           02 MSCREENA  PIC X.                                          00005990
           02 MSCREENC  PIC X.                                          00006000
           02 MSCREENP  PIC X.                                          00006010
           02 MSCREENH  PIC X.                                          00006020
           02 MSCREENV  PIC X.                                          00006030
           02 MSCREENO  PIC X(4).                                       00006040
                                                                                
