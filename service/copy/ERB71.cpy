      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * RBX : Liste des réceptions                                      00000020
      ***************************************************************** 00000030
       01   ERB71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE1L   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE1F   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGE1I   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGE2L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGE2F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGE2I   PIC X(3).                                       00000210
           02 MLIGNEI OCCURS   16 TIMES .                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNEXPEDL     COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MNEXPEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNEXPEDF     PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNEXPEDI     PIC X(7).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATENVL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MDATENVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATENVF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MDATENVI     PIC X(10).                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATRECL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MDATRECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATRECF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MDATRECI     PIC X(10).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTENVL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MQTENVL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQTENVF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MQTENVI      PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTRECL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MQTRECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQTRECF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQTRECI      PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNSOCIETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCIETF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNSOCIETI    PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSTATUTI     PIC X(3).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHOIXL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCHOIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCHOIXF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCHOIXI      PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(73).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * RBX : Liste des réceptions                                      00000760
      ***************************************************************** 00000770
       01   ERB71O REDEFINES ERB71I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MPAGE1A   PIC X.                                          00000950
           02 MPAGE1C   PIC X.                                          00000960
           02 MPAGE1P   PIC X.                                          00000970
           02 MPAGE1H   PIC X.                                          00000980
           02 MPAGE1V   PIC X.                                          00000990
           02 MPAGE1O   PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MPAGE2A   PIC X.                                          00001020
           02 MPAGE2C   PIC X.                                          00001030
           02 MPAGE2P   PIC X.                                          00001040
           02 MPAGE2H   PIC X.                                          00001050
           02 MPAGE2V   PIC X.                                          00001060
           02 MPAGE2O   PIC X(3).                                       00001070
           02 MLIGNEO OCCURS   16 TIMES .                               00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MNEXPEDA     PIC X.                                     00001100
             03 MNEXPEDC     PIC X.                                     00001110
             03 MNEXPEDP     PIC X.                                     00001120
             03 MNEXPEDH     PIC X.                                     00001130
             03 MNEXPEDV     PIC X.                                     00001140
             03 MNEXPEDO     PIC X(7).                                  00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MDATENVA     PIC X.                                     00001170
             03 MDATENVC     PIC X.                                     00001180
             03 MDATENVP     PIC X.                                     00001190
             03 MDATENVH     PIC X.                                     00001200
             03 MDATENVV     PIC X.                                     00001210
             03 MDATENVO     PIC X(10).                                 00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MDATRECA     PIC X.                                     00001240
             03 MDATRECC     PIC X.                                     00001250
             03 MDATRECP     PIC X.                                     00001260
             03 MDATRECH     PIC X.                                     00001270
             03 MDATRECV     PIC X.                                     00001280
             03 MDATRECO     PIC X(10).                                 00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MQTENVA      PIC X.                                     00001310
             03 MQTENVC PIC X.                                          00001320
             03 MQTENVP PIC X.                                          00001330
             03 MQTENVH PIC X.                                          00001340
             03 MQTENVV PIC X.                                          00001350
             03 MQTENVO      PIC ZZZZZ.                                 00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MQTRECA      PIC X.                                     00001380
             03 MQTRECC PIC X.                                          00001390
             03 MQTRECP PIC X.                                          00001400
             03 MQTRECH PIC X.                                          00001410
             03 MQTRECV PIC X.                                          00001420
             03 MQTRECO      PIC ZZZZZ.                                 00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MNSOCIETA    PIC X.                                     00001450
             03 MNSOCIETC    PIC X.                                     00001460
             03 MNSOCIETP    PIC X.                                     00001470
             03 MNSOCIETH    PIC X.                                     00001480
             03 MNSOCIETV    PIC X.                                     00001490
             03 MNSOCIETO    PIC X(3).                                  00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MSTATUTA     PIC X.                                     00001520
             03 MSTATUTC     PIC X.                                     00001530
             03 MSTATUTP     PIC X.                                     00001540
             03 MSTATUTH     PIC X.                                     00001550
             03 MSTATUTV     PIC X.                                     00001560
             03 MSTATUTO     PIC X(3).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MCHOIXA      PIC X.                                     00001590
             03 MCHOIXC PIC X.                                          00001600
             03 MCHOIXP PIC X.                                          00001610
             03 MCHOIXH PIC X.                                          00001620
             03 MCHOIXV PIC X.                                          00001630
             03 MCHOIXO      PIC X.                                     00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(73).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
