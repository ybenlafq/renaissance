      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00002000
      *                                                            *    00008900
      * COMMAREA SPECIFIQUE PRG MBC6A                              *    00002209
      *                                                            *    00008900
      **************************************************************            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-BC6A-LONG-COMMAREA PIC S9(4) COMP VALUE +600.                   
      *--                                                                       
       01  COMM-BC6A-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +600.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA-MBC6A.                                                    
      *                                                                         
      *---   ZONE APPLICATIVE                                                   
          02 COMM-DONNEES-APPLI.                                                
      *---      INFO CICS                                                       
             05 COMM-MBC6A-APPLID        PIC X(08).                             
             05 COMM-MBC6A-NSOCCICS      PIC X(03).                             
             05 COMM-MBC6A-SSAAMMJJ      PIC X(08).                             
      *------------------------------ ZONE DONNEES MBC6A                        
          02 COMM-DONNEES-MBC6A.                                                
      *---      NUMERO D ADRESSE                                                
             05 COMM-MBC6A-NADR          PIC X(08).                             
      *---      VERSION                                                         
             05 COMM-MBC6A-VERSION       PIC X(01).                             
          02 COMM-DONNEES-REPONSE.                                              
      *---      LIGNE INFO 1                                                    
             05 COMM-MBC6A-INFO1         PIC X(79).                             
      *---      LIGNE INFO 2                                                    
             05 COMM-MBC6A-INFO2         PIC X(79).                             
      *---      LIGNE INFO 3                                                    
             05 COMM-MBC6A-INFO3         PIC X(79).                             
      *---      QUESTION                                                        
             05 COMM-MBC6A-QUESTION      PIC X(75).                             
      *---      FLAG MAIL OBLIGATOIRE                                           
             05 COMM-MBC6A-FMAIL         PIC X(01).                             
      *---      FLAG GSM  OBLIGATOIRE                                           
             05 COMM-MBC6A-FGSM          PIC X(01).                             
      *---      FLAG TELEPHONE FIXE OBLIGATOIRE                                 
             05 COMM-MBC6A-FTEL          PIC X(01).                             
      *---      IDCLIENT                                                        
             05 COMM-MBC6A-IDCLIENT      PIC X(08).                             
      *---      IDQUESTION                                                      
             05 COMM-MBC6A-IDQUESTION    PIC X(05).                             
      *---      SOURCE                                                          
             05 COMM-MBC6A-SOURCE        PIC X(05).                             
      *------------------------------ ZONE RETOUR MBC6A                         
          02 COMM-MBC6A-RETOUR.                                                 
             05 COMM-MBC6A-RETOUR-MQ.                                           
                 10 COMM-MBC6A-CODRETMQ      PIC X(01).                         
                      88  COMM-MBC6A-MQ-OK     VALUE ' '.                       
                      88  COMM-MBC6A-MQ-ERREUR VALUE '1'.                       
                 10 COMM-MBC6A-LIBERRMQ      PIC X(60).                         
             05 COMM-BC6A-RETOUR-BC.                                            
                 10 COMM-MBC6A-CODRETBC      PIC X(01).                         
                      88  COMM-MBC6A-BC-OK     VALUE ' '.                       
                      88  COMM-MBC6A-BC-ERREUR VALUE '1'.                       
                 10 COMM-MBC6A-LIBERRBC      PIC X(60).                         
      *---      FILLER                                                          
          02 FILLER                     PIC X(117).                             
      *                                                                         
                                                                                
