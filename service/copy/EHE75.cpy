      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE65   EHE65                                              00000020
      ***************************************************************** 00000030
       01   EHE75I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEHETL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLLIEHETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEHETF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLLIEHETI      PIC X(19).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLNOMI    PIC X(19).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNUMPAGEI      PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPAGEMAXI      PIC X(2).                                  00000290
           02 MCODBARD OCCURS   9 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODBARL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCODBARL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODBARF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCODBARI     PIC X(17).                                 00000340
           02 MANNULD OCCURS   9 TIMES .                                00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MANNULL      COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MANNULL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MANNULF      PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MANNULI      PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLIBERRI  PIC X(19).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MSCREENI  PIC X(4).                                       00000470
      ***************************************************************** 00000480
      * SDF: EHE65   EHE65                                              00000490
      ***************************************************************** 00000500
       01   EHE75O REDEFINES EHE75I.                                    00000510
           02 FILLER    PIC X(12).                                      00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MDATJOUA  PIC X.                                          00000540
           02 MDATJOUC  PIC X.                                          00000550
           02 MDATJOUP  PIC X.                                          00000560
           02 MDATJOUH  PIC X.                                          00000570
           02 MDATJOUV  PIC X.                                          00000580
           02 MDATJOUO  PIC X(10).                                      00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MTIMJOUA  PIC X.                                          00000610
           02 MTIMJOUC  PIC X.                                          00000620
           02 MTIMJOUP  PIC X.                                          00000630
           02 MTIMJOUH  PIC X.                                          00000640
           02 MTIMJOUV  PIC X.                                          00000650
           02 MTIMJOUO  PIC X(5).                                       00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MLLIEHETA      PIC X.                                     00000680
           02 MLLIEHETC PIC X.                                          00000690
           02 MLLIEHETP PIC X.                                          00000700
           02 MLLIEHETH PIC X.                                          00000710
           02 MLLIEHETV PIC X.                                          00000720
           02 MLLIEHETO      PIC X(19).                                 00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MLNOMA    PIC X.                                          00000750
           02 MLNOMC    PIC X.                                          00000760
           02 MLNOMP    PIC X.                                          00000770
           02 MLNOMH    PIC X.                                          00000780
           02 MLNOMV    PIC X.                                          00000790
           02 MLNOMO    PIC X(19).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MNUMPAGEA      PIC X.                                     00000820
           02 MNUMPAGEC PIC X.                                          00000830
           02 MNUMPAGEP PIC X.                                          00000840
           02 MNUMPAGEH PIC X.                                          00000850
           02 MNUMPAGEV PIC X.                                          00000860
           02 MNUMPAGEO      PIC X(2).                                  00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MPAGEMAXA      PIC X.                                     00000890
           02 MPAGEMAXC PIC X.                                          00000900
           02 MPAGEMAXP PIC X.                                          00000910
           02 MPAGEMAXH PIC X.                                          00000920
           02 MPAGEMAXV PIC X.                                          00000930
           02 MPAGEMAXO      PIC X(2).                                  00000940
           02 DFHMS1 OCCURS   9 TIMES .                                 00000950
             03 FILLER       PIC X(2).                                  00000960
             03 MCODBARA     PIC X.                                     00000970
             03 MCODBARC     PIC X.                                     00000980
             03 MCODBARP     PIC X.                                     00000990
             03 MCODBARH     PIC X.                                     00001000
             03 MCODBARV     PIC X.                                     00001010
             03 MCODBARO     PIC X(17).                                 00001020
           02 DFHMS2 OCCURS   9 TIMES .                                 00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MANNULA      PIC X.                                     00001050
             03 MANNULC PIC X.                                          00001060
             03 MANNULP PIC X.                                          00001070
             03 MANNULH PIC X.                                          00001080
             03 MANNULV PIC X.                                          00001090
             03 MANNULO      PIC X.                                     00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLIBERRA  PIC X.                                          00001120
           02 MLIBERRC  PIC X.                                          00001130
           02 MLIBERRP  PIC X.                                          00001140
           02 MLIBERRH  PIC X.                                          00001150
           02 MLIBERRV  PIC X.                                          00001160
           02 MLIBERRO  PIC X(19).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MSCREENA  PIC X.                                          00001190
           02 MSCREENC  PIC X.                                          00001200
           02 MSCREENP  PIC X.                                          00001210
           02 MSCREENH  PIC X.                                          00001220
           02 MSCREENV  PIC X.                                          00001230
           02 MSCREENO  PIC X(4).                                       00001240
                                                                                
