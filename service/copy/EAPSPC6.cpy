      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE - CONDITION DE PR�PARATION     *
      * NOM FICHIER.: EAPSPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 079                                             *
      *****************************************************************
      *
       01  EAPSPC6.
      * TYPE ENREGISTREMENT : RECEPTION ARTICLE CONDITION DE PR�PARATION
           05      EAPSPC6-TYP-ENREG      PIC  X(0006).
      * CODE SOCIETE
           05      EAPSPC6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EAPSPC6-CARTICLE       PIC  X(0018).
      * CONDITIONNEMENT STOCKAGE
           05      EAPSPC6-COND-STOCKAGE  PIC  X(0005).
      * DATE DERNIERE MODIF
           05      EAPSPC6-DMODIF         PIC  X(0008).
      * HEURE DERNIERE MODIF
           05      EAPSPC6-HMODIF         PIC  X(0006).
      * FILLER  30
           05      EAPSPC6-FILLER         PIC  X(0030).
      * FIN
           05      EAPSPC6-FIN            PIC  X(0001).
      
