      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * ACC - MENU GENERAL                                              00000020
      ***************************************************************** 00000030
       01   EPC20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNCDEI    PIC X(12).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MDATDEBI  PIC X(10).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDATFINI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMPTEL      COMP PIC S9(4).                            00000280
      *--                                                                       
           02 MNCOMPTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCOMPTEF      PIC X.                                     00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNCOMPTEI      PIC X(8).                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MZONCMDI  PIC X.                                          00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MSTATUTI  PIC X(10).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNSERIEI  PIC X(13).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIBERRI  PIC X(79).                                      00000470
      * CODE TRANSACTION                                                00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCODTRAI  PIC X(4).                                       00000520
      * CICS DE TRAVAIL                                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCICSI    PIC X(5).                                       00000570
      * NETNAME                                                         00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      * CODE TERMINAL                                                   00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MSCREENI  PIC X(4).                                       00000670
      ***************************************************************** 00000680
      * ACC - MENU GENERAL                                              00000690
      ***************************************************************** 00000700
       01   EPC20O REDEFINES EPC20I.                                    00000710
           02 FILLER    PIC X(12).                                      00000720
      * DATE DU JOUR                                                    00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MDATJOUA  PIC X.                                          00000750
           02 MDATJOUC  PIC X.                                          00000760
           02 MDATJOUP  PIC X.                                          00000770
           02 MDATJOUH  PIC X.                                          00000780
           02 MDATJOUV  PIC X.                                          00000790
           02 MDATJOUO  PIC X(10).                                      00000800
      * HEURE                                                           00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTIMJOUA  PIC X.                                          00000830
           02 MTIMJOUC  PIC X.                                          00000840
           02 MTIMJOUP  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUV  PIC X.                                          00000870
           02 MTIMJOUO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MNCDEA    PIC X.                                          00000900
           02 MNCDEC    PIC X.                                          00000910
           02 MNCDEP    PIC X.                                          00000920
           02 MNCDEH    PIC X.                                          00000930
           02 MNCDEV    PIC X.                                          00000940
           02 MNCDEO    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATDEBA  PIC X.                                          00000970
           02 MDATDEBC  PIC X.                                          00000980
           02 MDATDEBP  PIC X.                                          00000990
           02 MDATDEBH  PIC X.                                          00001000
           02 MDATDEBV  PIC X.                                          00001010
           02 MDATDEBO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATFINA  PIC X.                                          00001040
           02 MDATFINC  PIC X.                                          00001050
           02 MDATFINP  PIC X.                                          00001060
           02 MDATFINH  PIC X.                                          00001070
           02 MDATFINV  PIC X.                                          00001080
           02 MDATFINO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNCOMPTEA      PIC X.                                     00001110
           02 MNCOMPTEC PIC X.                                          00001120
           02 MNCOMPTEP PIC X.                                          00001130
           02 MNCOMPTEH PIC X.                                          00001140
           02 MNCOMPTEV PIC X.                                          00001150
           02 MNCOMPTEO      PIC X(8).                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MZONCMDA  PIC X.                                          00001180
           02 MZONCMDC  PIC X.                                          00001190
           02 MZONCMDP  PIC X.                                          00001200
           02 MZONCMDH  PIC X.                                          00001210
           02 MZONCMDV  PIC X.                                          00001220
           02 MZONCMDO  PIC X.                                          00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MSTATUTA  PIC X.                                          00001250
           02 MSTATUTC  PIC X.                                          00001260
           02 MSTATUTP  PIC X.                                          00001270
           02 MSTATUTH  PIC X.                                          00001280
           02 MSTATUTV  PIC X.                                          00001290
           02 MSTATUTO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNSERIEA  PIC X.                                          00001320
           02 MNSERIEC  PIC X.                                          00001330
           02 MNSERIEP  PIC X.                                          00001340
           02 MNSERIEH  PIC X.                                          00001350
           02 MNSERIEV  PIC X.                                          00001360
           02 MNSERIEO  PIC X(13).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLIBERRA  PIC X.                                          00001390
           02 MLIBERRC  PIC X.                                          00001400
           02 MLIBERRP  PIC X.                                          00001410
           02 MLIBERRH  PIC X.                                          00001420
           02 MLIBERRV  PIC X.                                          00001430
           02 MLIBERRO  PIC X(79).                                      00001440
      * CODE TRANSACTION                                                00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCODTRAA  PIC X.                                          00001470
           02 MCODTRAC  PIC X.                                          00001480
           02 MCODTRAP  PIC X.                                          00001490
           02 MCODTRAH  PIC X.                                          00001500
           02 MCODTRAV  PIC X.                                          00001510
           02 MCODTRAO  PIC X(4).                                       00001520
      * CICS DE TRAVAIL                                                 00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCICSA    PIC X.                                          00001550
           02 MCICSC    PIC X.                                          00001560
           02 MCICSP    PIC X.                                          00001570
           02 MCICSH    PIC X.                                          00001580
           02 MCICSV    PIC X.                                          00001590
           02 MCICSO    PIC X(5).                                       00001600
      * NETNAME                                                         00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNETNAMA  PIC X.                                          00001630
           02 MNETNAMC  PIC X.                                          00001640
           02 MNETNAMP  PIC X.                                          00001650
           02 MNETNAMH  PIC X.                                          00001660
           02 MNETNAMV  PIC X.                                          00001670
           02 MNETNAMO  PIC X(8).                                       00001680
      * CODE TERMINAL                                                   00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MSCREENA  PIC X.                                          00001710
           02 MSCREENC  PIC X.                                          00001720
           02 MSCREENP  PIC X.                                          00001730
           02 MSCREENH  PIC X.                                          00001740
           02 MSCREENV  PIC X.                                          00001750
           02 MSCREENO  PIC X(4).                                       00001760
                                                                                
