      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: consult d"une reception                                    00000020
      ***************************************************************** 00000030
       01   ERB24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNBLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBLF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNBLI     PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNFOURNI  PIC X(25).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDRECEPTI      PIC X(10).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCOMMANDEL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCOMMANDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCOMMANDEF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCOMMANDEI    PIC X(7).                                  00000290
      * entite fourn darty                                              00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBENTITEL    COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MLIBENTITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLIBENTITEF    PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MLIBENTITEI    PIC X(17).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPRDATTL     COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MNBPRDATTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBPRDATTF     PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MNBPRDATTI     PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTATTL     COMP PIC S9(4).                            00000390
      *--                                                                       
           02 MNBLOTATTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBLOTATTF     PIC X.                                     00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MNBLOTATTI     PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTRECEPL   COMP PIC S9(4).                            00000430
      *--                                                                       
           02 MNBLOTRECEPL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNBLOTRECEPF   PIC X.                                     00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MNBLOTRECEPI   PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTACCL     COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MNBLOTACCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBLOTACCF     PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNBLOTACCI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTREFL     COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MNBLOTREFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBLOTREFF     PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNBLOTREFI     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLOTRESTL    COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MNBLOTRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNBLOTRESTF    PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNBLOTRESTI    PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMESSAGEL      COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MMESSAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMESSAGEF      PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MMESSAGEI      PIC X(30).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(78).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * Rbx: consult d"une reception                                    00000840
      ***************************************************************** 00000850
       01   ERB24O REDEFINES ERB24I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNBLA     PIC X.                                          00001030
           02 MNBLC     PIC X.                                          00001040
           02 MNBLP     PIC X.                                          00001050
           02 MNBLH     PIC X.                                          00001060
           02 MNBLV     PIC X.                                          00001070
           02 MNBLO     PIC X(10).                                      00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNFOURNA  PIC X.                                          00001100
           02 MNFOURNC  PIC X.                                          00001110
           02 MNFOURNP  PIC X.                                          00001120
           02 MNFOURNH  PIC X.                                          00001130
           02 MNFOURNV  PIC X.                                          00001140
           02 MNFOURNO  PIC X(25).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDRECEPTA      PIC X.                                     00001170
           02 MDRECEPTC PIC X.                                          00001180
           02 MDRECEPTP PIC X.                                          00001190
           02 MDRECEPTH PIC X.                                          00001200
           02 MDRECEPTV PIC X.                                          00001210
           02 MDRECEPTO      PIC X(10).                                 00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNCOMMANDEA    PIC X.                                     00001240
           02 MNCOMMANDEC    PIC X.                                     00001250
           02 MNCOMMANDEP    PIC X.                                     00001260
           02 MNCOMMANDEH    PIC X.                                     00001270
           02 MNCOMMANDEV    PIC X.                                     00001280
           02 MNCOMMANDEO    PIC X(7).                                  00001290
      * entite fourn darty                                              00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLIBENTITEA    PIC X.                                     00001320
           02 MLIBENTITEC    PIC X.                                     00001330
           02 MLIBENTITEP    PIC X.                                     00001340
           02 MLIBENTITEH    PIC X.                                     00001350
           02 MLIBENTITEV    PIC X.                                     00001360
           02 MLIBENTITEO    PIC X(17).                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNBPRDATTA     PIC X.                                     00001390
           02 MNBPRDATTC     PIC X.                                     00001400
           02 MNBPRDATTP     PIC X.                                     00001410
           02 MNBPRDATTH     PIC X.                                     00001420
           02 MNBPRDATTV     PIC X.                                     00001430
           02 MNBPRDATTO     PIC X(5).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNBLOTATTA     PIC X.                                     00001460
           02 MNBLOTATTC     PIC X.                                     00001470
           02 MNBLOTATTP     PIC X.                                     00001480
           02 MNBLOTATTH     PIC X.                                     00001490
           02 MNBLOTATTV     PIC X.                                     00001500
           02 MNBLOTATTO     PIC X(5).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNBLOTRECEPA   PIC X.                                     00001530
           02 MNBLOTRECEPC   PIC X.                                     00001540
           02 MNBLOTRECEPP   PIC X.                                     00001550
           02 MNBLOTRECEPH   PIC X.                                     00001560
           02 MNBLOTRECEPV   PIC X.                                     00001570
           02 MNBLOTRECEPO   PIC X(5).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNBLOTACCA     PIC X.                                     00001600
           02 MNBLOTACCC     PIC X.                                     00001610
           02 MNBLOTACCP     PIC X.                                     00001620
           02 MNBLOTACCH     PIC X.                                     00001630
           02 MNBLOTACCV     PIC X.                                     00001640
           02 MNBLOTACCO     PIC X(5).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNBLOTREFA     PIC X.                                     00001670
           02 MNBLOTREFC     PIC X.                                     00001680
           02 MNBLOTREFP     PIC X.                                     00001690
           02 MNBLOTREFH     PIC X.                                     00001700
           02 MNBLOTREFV     PIC X.                                     00001710
           02 MNBLOTREFO     PIC X(5).                                  00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNBLOTRESTA    PIC X.                                     00001740
           02 MNBLOTRESTC    PIC X.                                     00001750
           02 MNBLOTRESTP    PIC X.                                     00001760
           02 MNBLOTRESTH    PIC X.                                     00001770
           02 MNBLOTRESTV    PIC X.                                     00001780
           02 MNBLOTRESTO    PIC X(5).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MMESSAGEA      PIC X.                                     00001810
           02 MMESSAGEC PIC X.                                          00001820
           02 MMESSAGEP PIC X.                                          00001830
           02 MMESSAGEH PIC X.                                          00001840
           02 MMESSAGEV PIC X.                                          00001850
           02 MMESSAGEO      PIC X(30).                                 00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(78).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
