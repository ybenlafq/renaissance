      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV00   EAV00                                              00000020
      ***************************************************************** 00000030
       01   ERB62I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * soc de modif                                                    00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCML    COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MSOCML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCMF    PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MSOCMI    PIC X(3).                                       00000180
      * lieu                                                            00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUML   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLIEUML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUMF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIEUMI   PIC X(3).                                       00000230
      * libelle lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUML  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MLLIEUML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUMF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUMI  PIC X(20).                                      00000280
      * soc de vente                                                    00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCVL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSOCVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCVF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSOCVI    PIC X(3).                                       00000330
      * lieu                                                            00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUVL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLIEUVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUVF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLIEUVI   PIC X(3).                                       00000380
      * vente                                                           00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVENTEVL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MVENTEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVENTEVF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MVENTEVI  PIC X(7).                                       00000430
      * contrat                                                         00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCONTRAL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MCONTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCONTRAF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCONTRAI  PIC X(18).                                      00000480
      * nom                                                             00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNOMI     PIC X(25).                                      00000530
      * adresse                                                         00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRESSL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MADRESSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRESSF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MADRESSI  PIC X(41).                                      00000580
      * prenom                                                          00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRENOML  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MPRENOML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRENOMF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MPRENOMI  PIC X(15).                                      00000630
      * ref postale                                                     00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTALL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MPOSTALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPOSTALF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MPOSTALI  PIC X(41).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDL   COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENDF   PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCVENDI   PIC X(6).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENDL   COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVENDF   PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLVENDI   PIC X(15).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOFFREL   COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MOFFREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOFFREF   PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MOFFREI   PIC X(20).                                      00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOFFRE2L  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MOFFRE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOFFRE2F  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MOFFRE2I  PIC X(20).                                      00000840
           02 MOPTIONI OCCURS   12 TIMES .                              00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPT1L  COMP PIC S9(4).                                 00000860
      *--                                                                       
             03 MOPT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MOPT1F  PIC X.                                          00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MOPT1I  PIC X(20).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHOIXL      COMP PIC S9(4).                            00000900
      *--                                                                       
             03 MCHOIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCHOIXF      PIC X.                                     00000910
             03 FILLER  PIC X(4).                                       00000920
             03 MCHOIXI      PIC X.                                     00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPT2L  COMP PIC S9(4).                                 00000940
      *--                                                                       
             03 MOPT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MOPT2F  PIC X.                                          00000950
             03 FILLER  PIC X(4).                                       00000960
             03 MOPT2I  PIC X(20).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBERRI  PIC X(78).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCODTRAI  PIC X(4).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCICSI    PIC X(5).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * SDF: EAV00   EAV00                                              00001190
      ***************************************************************** 00001200
       01   ERB62O REDEFINES ERB62I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MTIMJOUA  PIC X.                                          00001310
           02 MTIMJOUC  PIC X.                                          00001320
           02 MTIMJOUP  PIC X.                                          00001330
           02 MTIMJOUH  PIC X.                                          00001340
           02 MTIMJOUV  PIC X.                                          00001350
           02 MTIMJOUO  PIC X(5).                                       00001360
      * soc de modif                                                    00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MSOCMA    PIC X.                                          00001390
           02 MSOCMC    PIC X.                                          00001400
           02 MSOCMP    PIC X.                                          00001410
           02 MSOCMH    PIC X.                                          00001420
           02 MSOCMV    PIC X.                                          00001430
           02 MSOCMO    PIC X(3).                                       00001440
      * lieu                                                            00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLIEUMA   PIC X.                                          00001470
           02 MLIEUMC   PIC X.                                          00001480
           02 MLIEUMP   PIC X.                                          00001490
           02 MLIEUMH   PIC X.                                          00001500
           02 MLIEUMV   PIC X.                                          00001510
           02 MLIEUMO   PIC X(3).                                       00001520
      * libelle lieu                                                    00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLLIEUMA  PIC X.                                          00001550
           02 MLLIEUMC  PIC X.                                          00001560
           02 MLLIEUMP  PIC X.                                          00001570
           02 MLLIEUMH  PIC X.                                          00001580
           02 MLLIEUMV  PIC X.                                          00001590
           02 MLLIEUMO  PIC X(20).                                      00001600
      * soc de vente                                                    00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MSOCVA    PIC X.                                          00001630
           02 MSOCVC    PIC X.                                          00001640
           02 MSOCVP    PIC X.                                          00001650
           02 MSOCVH    PIC X.                                          00001660
           02 MSOCVV    PIC X.                                          00001670
           02 MSOCVO    PIC X(3).                                       00001680
      * lieu                                                            00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLIEUVA   PIC X.                                          00001710
           02 MLIEUVC   PIC X.                                          00001720
           02 MLIEUVP   PIC X.                                          00001730
           02 MLIEUVH   PIC X.                                          00001740
           02 MLIEUVV   PIC X.                                          00001750
           02 MLIEUVO   PIC X(3).                                       00001760
      * vente                                                           00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MVENTEVA  PIC X.                                          00001790
           02 MVENTEVC  PIC X.                                          00001800
           02 MVENTEVP  PIC X.                                          00001810
           02 MVENTEVH  PIC X.                                          00001820
           02 MVENTEVV  PIC X.                                          00001830
           02 MVENTEVO  PIC X(7).                                       00001840
      * contrat                                                         00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MCONTRAA  PIC X.                                          00001870
           02 MCONTRAC  PIC X.                                          00001880
           02 MCONTRAP  PIC X.                                          00001890
           02 MCONTRAH  PIC X.                                          00001900
           02 MCONTRAV  PIC X.                                          00001910
           02 MCONTRAO  PIC X(18).                                      00001920
      * nom                                                             00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNOMA     PIC X.                                          00001950
           02 MNOMC     PIC X.                                          00001960
           02 MNOMP     PIC X.                                          00001970
           02 MNOMH     PIC X.                                          00001980
           02 MNOMV     PIC X.                                          00001990
           02 MNOMO     PIC X(25).                                      00002000
      * adresse                                                         00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MADRESSA  PIC X.                                          00002030
           02 MADRESSC  PIC X.                                          00002040
           02 MADRESSP  PIC X.                                          00002050
           02 MADRESSH  PIC X.                                          00002060
           02 MADRESSV  PIC X.                                          00002070
           02 MADRESSO  PIC X(41).                                      00002080
      * prenom                                                          00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MPRENOMA  PIC X.                                          00002110
           02 MPRENOMC  PIC X.                                          00002120
           02 MPRENOMP  PIC X.                                          00002130
           02 MPRENOMH  PIC X.                                          00002140
           02 MPRENOMV  PIC X.                                          00002150
           02 MPRENOMO  PIC X(15).                                      00002160
      * ref postale                                                     00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MPOSTALA  PIC X.                                          00002190
           02 MPOSTALC  PIC X.                                          00002200
           02 MPOSTALP  PIC X.                                          00002210
           02 MPOSTALH  PIC X.                                          00002220
           02 MPOSTALV  PIC X.                                          00002230
           02 MPOSTALO  PIC X(41).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCVENDA   PIC X.                                          00002260
           02 MCVENDC   PIC X.                                          00002270
           02 MCVENDP   PIC X.                                          00002280
           02 MCVENDH   PIC X.                                          00002290
           02 MCVENDV   PIC X.                                          00002300
           02 MCVENDO   PIC X(6).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLVENDA   PIC X.                                          00002330
           02 MLVENDC   PIC X.                                          00002340
           02 MLVENDP   PIC X.                                          00002350
           02 MLVENDH   PIC X.                                          00002360
           02 MLVENDV   PIC X.                                          00002370
           02 MLVENDO   PIC X(15).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MOFFREA   PIC X.                                          00002400
           02 MOFFREC   PIC X.                                          00002410
           02 MOFFREP   PIC X.                                          00002420
           02 MOFFREH   PIC X.                                          00002430
           02 MOFFREV   PIC X.                                          00002440
           02 MOFFREO   PIC X(20).                                      00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MOFFRE2A  PIC X.                                          00002470
           02 MOFFRE2C  PIC X.                                          00002480
           02 MOFFRE2P  PIC X.                                          00002490
           02 MOFFRE2H  PIC X.                                          00002500
           02 MOFFRE2V  PIC X.                                          00002510
           02 MOFFRE2O  PIC X(20).                                      00002520
           02 MOPTIONO OCCURS   12 TIMES .                              00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MOPT1A  PIC X.                                          00002550
             03 MOPT1C  PIC X.                                          00002560
             03 MOPT1P  PIC X.                                          00002570
             03 MOPT1H  PIC X.                                          00002580
             03 MOPT1V  PIC X.                                          00002590
             03 MOPT1O  PIC X(20).                                      00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MCHOIXA      PIC X.                                     00002620
             03 MCHOIXC PIC X.                                          00002630
             03 MCHOIXP PIC X.                                          00002640
             03 MCHOIXH PIC X.                                          00002650
             03 MCHOIXV PIC X.                                          00002660
             03 MCHOIXO      PIC X.                                     00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MOPT2A  PIC X.                                          00002690
             03 MOPT2C  PIC X.                                          00002700
             03 MOPT2P  PIC X.                                          00002710
             03 MOPT2H  PIC X.                                          00002720
             03 MOPT2V  PIC X.                                          00002730
             03 MOPT2O  PIC X(20).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(78).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
