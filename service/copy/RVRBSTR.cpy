      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBSTR CORRESPONDANCE SST/PROFIL        *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBSTR.                                                             
           05  RBSTR-CTABLEG2    PIC X(15).                                     
           05  RBSTR-CTABLEG2-REDEF REDEFINES RBSTR-CTABLEG2.                   
               10  RBSTR-CSTR            PIC X(06).                             
           05  RBSTR-WTABLEG     PIC X(80).                                     
           05  RBSTR-WTABLEG-REDEF  REDEFINES RBSTR-WTABLEG.                    
               10  RBSTR-CPROFIL         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBSTR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSTR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBSTR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBSTR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBSTR-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
