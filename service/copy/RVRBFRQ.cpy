      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE RBFRQ TABLE DES FREQUENCES             *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVRBFRQ.                                                             
           05  RBFRQ-CTABLEG2    PIC X(15).                                     
           05  RBFRQ-CTABLEG2-REDEF REDEFINES RBFRQ-CTABLEG2.                   
               10  RBFRQ-FREQ            PIC X(06).                             
           05  RBFRQ-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVRBFRQ-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBFRQ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  RBFRQ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RBFRQ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  RBFRQ-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
