      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB1001.                                                            
      *}                                                                        
           10 RB10-NSOCIETE             PIC X(3).                               
           10 RB10-NLIEU                PIC X(3).                               
           10 RB10-NSOCVTE              PIC X(3).                               
           10 RB10-NLIEUVTE             PIC X(3).                               
           10 RB10-NVENTE               PIC X(7).                               
           10 RB10-NSERIEREPRIS         PIC X(15).                              
           10 RB10-NCODICREPRIS         PIC X(7).                               
           10 RB10-NEANREPRIS           PIC X(13).                              
           10 RB10-WTYPMVT              PIC X(1).                               
           10 RB10-WCOMPLET             PIC X(1).                               
           10 RB10-WHS                  PIC X(1).                               
           10 RB10-DMVT                 PIC X(8).                               
           10 RB10-NSERIEREMIS          PIC X(15).                              
           10 RB10-NCODICREMIS          PIC X(7).                               
           10 RB10-NEANREMIS            PIC X(13).                              
           10 RB10-WACTIVATION          PIC X(01).                              
           10 RB10-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 RB10-LCOMMENT             PIC X(70).                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NSOCIETE-F           PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NLIEU-F              PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NSOCVTE-F            PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NSOCVTE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NLIEUVTE-F           PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NLIEUVTE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NSERIEREPRIS-F       PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NSERIEREPRIS-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NCODICREPRIS-F       PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NCODICREPRIS-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NEANREPRIS-F         PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NEANREPRIS-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-WTYPMVT-F            PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-WTYPMVT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-WCOMPLET-F           PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-WCOMPLET-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-WHS-F                PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-WHS-F                PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-DMVT-F               PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-DMVT-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NSERIEREMIS-F        PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NSERIEREMIS-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NCODICREMIS-F        PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NCODICREMIS-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-NEANREMIS-F          PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-NEANREMIS-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-WACTIVATION-F        PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-WACTIVATION-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-DSYST-F              PIC S9(4) USAGE COMP.                   
      *--                                                                       
           10 RB10-DSYST-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB10-LCOMMENT-F           PIC S9(4) USAGE COMP.                   
      *                                                                         
      *--                                                                       
           10 RB10-LCOMMENT-F           PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
