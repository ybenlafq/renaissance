      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IDS005 AU 10/12/1993  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,03,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IDS005.                                                        
            05 NOMETAT-IDS005           PIC X(6) VALUE 'IDS005'.                
            05 RUPTURES-IDS005.                                                 
           10 IDS005-NSOCIETE           PIC X(03).                      007  003
           10 IDS005-WSEQED             PIC S9(05)      COMP-3.         010  003
           10 IDS005-WSEQFAM            PIC S9(05)      COMP-3.         013  003
           10 IDS005-CMARQ              PIC X(05).                      016  005
           10 IDS005-NCODIC             PIC X(07).                      021  007
           10 IDS005-NLIEUVALO          PIC X(03).                      028  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IDS005-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 IDS005-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IDS005.                                                   
           10 IDS005-CFAM               PIC X(05).                      033  005
           10 IDS005-CRAYON             PIC X(05).                      038  005
           10 IDS005-LLIEU              PIC X(20).                      043  020
           10 IDS005-LREFFOURN          PIC X(20).                      063  020
           10 IDS005-PEXCLUS            PIC S9(09)V9(6) COMP-3.         083  008
           10 IDS005-PNET               PIC S9(09)V9(6) COMP-3.         091  008
           10 IDS005-PVALINV            PIC S9(09)V9(6) COMP-3.         099  008
           10 IDS005-QEXCLUS            PIC S9(05)      COMP-3.         107  003
           10 IDS005-QTAUX              PIC S9(03)V9(2) COMP-3.         110  003
           10 IDS005-QTEINV             PIC S9(05)      COMP-3.         113  003
           10 IDS005-QTENET             PIC S9(05)      COMP-3.         116  003
            05 FILLER                      PIC X(394).                          
                                                                                
