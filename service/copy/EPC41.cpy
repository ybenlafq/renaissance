      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CC - PARAMETRAGE ICS                                            00000020
      ***************************************************************** 00000030
       01   EPC41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREGLEL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNREGLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNREGLEF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNREGLEI  PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBELLEI      PIC X(25).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGNEL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNLIGNEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIGNEF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNLIGNEI  PIC X(2).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERFL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MINTERFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINTERFF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MINTERFI  PIC X(5).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVAL     COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MTVAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTVAF     PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTVAI     PIC X(3).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEOPL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MTYPEOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPEOPF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MTYPEOPI  PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNATUREI  PIC X(5).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT1L   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCRIT1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT1F   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCRIT1I   PIC X(5).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT2L   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCRIT2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT2F   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCRIT2I   PIC X(5).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT3L   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCRIT3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT3F   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCRIT3I   PIC X(5).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT4L   COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCRIT4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT4F   PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCRIT4I   PIC X(5).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT5L   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCRIT5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT5F   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCRIT5I   PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT6L   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCRIT6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT6F   PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCRIT6I   PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSENSL    COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MSENSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSENSF    PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MSENSI    PIC X.                                          00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIL     COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MCLIL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCLIF     PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCLII     PIC X(15).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCEL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MSOCEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCEF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MSOCEI    PIC X(3).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUEL   COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MLIEUEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUEF   PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MLIEUEI   PIC X(3).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCRL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MSOCRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCRF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MSOCRI    PIC X(3).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEURL   COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLIEURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEURF   PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLIEURI   PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMOPL   COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNUMOPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNUMOPF   PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNUMOPI   PIC X(15).                                      00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATOPL   COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MDATOPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATOPF   PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MDATOPI   PIC X(8).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTR4L  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MLETTR4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTR4F  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MLETTR4I  PIC X(10).                                      00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTR5L  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MLETTR5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTR5F  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MLETTR5I  PIC X(10).                                      00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMONTANTL      COMP PIC S9(4).                            00001080
      *--                                                                       
           02 MMONTANTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MMONTANTF      PIC X.                                     00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MMONTANTI      PIC X(5).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00001120
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCDEVISEI      PIC X(3).                                  00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVT1L   COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MLMVT1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMVT1F   PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MLMVT1I   PIC X(25).                                      00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVT2L   COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MLMVT2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMVT2F   PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MLMVT2I   PIC X(25).                                      00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVT3L   COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MLMVT3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMVT3F   PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MLMVT3I   PIC X(25).                                      00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVT4L   COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MLMVT4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMVT4F   PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MLMVT4I   PIC X(25).                                      00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMVT5L   COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MLMVT5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMVT5F   PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MLMVT5I   PIC X(25).                                      00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPAYSL   COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCPAYSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPAYSF   PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCPAYSI   PIC X(3).                                       00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTVAL    COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MWTVAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWTVAF    PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MWTVAI    PIC X.                                          00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFDOCL  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MREFDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREFDOCF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MREFDOCI  PIC X(14).                                      00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURL   COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MNFOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNFOURF   PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MNFOURI   PIC X(15).                                      00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPC02L    COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MPC02L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPC02F    PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MPC02I    PIC X.                                          00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERL     COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MPERL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPERF     PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MPERI     PIC X.                                          00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MJOURI    PIC X(2).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRAITSL  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MTRAITSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTRAITSF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MTRAITSI  PIC X(5).                                       00001670
      * ZONE CMD AIDA                                                   00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001700
           02 FILLER    PIC X(4).                                       00001710
           02 MLIBERRI  PIC X(79).                                      00001720
      * CODE TRANSACTION                                                00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCODTRAI  PIC X(4).                                       00001770
      * CICS DE TRAVAIL                                                 00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MCICSI    PIC X(5).                                       00001820
      * NETNAME                                                         00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001840
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001850
           02 FILLER    PIC X(4).                                       00001860
           02 MNETNAMI  PIC X(8).                                       00001870
      * CODE TERMINAL                                                   00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001900
           02 FILLER    PIC X(4).                                       00001910
           02 MSCREENI  PIC X(4).                                       00001920
      ***************************************************************** 00001930
      * CC - PARAMETRAGE ICS                                            00001940
      ***************************************************************** 00001950
       01   EPC41O REDEFINES EPC41I.                                    00001960
           02 FILLER    PIC X(12).                                      00001970
      * DATE DU JOUR                                                    00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MDATJOUA  PIC X.                                          00002000
           02 MDATJOUC  PIC X.                                          00002010
           02 MDATJOUP  PIC X.                                          00002020
           02 MDATJOUH  PIC X.                                          00002030
           02 MDATJOUV  PIC X.                                          00002040
           02 MDATJOUO  PIC X(10).                                      00002050
      * HEURE                                                           00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MTIMJOUA  PIC X.                                          00002080
           02 MTIMJOUC  PIC X.                                          00002090
           02 MTIMJOUP  PIC X.                                          00002100
           02 MTIMJOUH  PIC X.                                          00002110
           02 MTIMJOUV  PIC X.                                          00002120
           02 MTIMJOUO  PIC X(5).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MNREGLEA  PIC X.                                          00002150
           02 MNREGLEC  PIC X.                                          00002160
           02 MNREGLEP  PIC X.                                          00002170
           02 MNREGLEH  PIC X.                                          00002180
           02 MNREGLEV  PIC X.                                          00002190
           02 MNREGLEO  PIC X(10).                                      00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MLIBELLEA      PIC X.                                     00002220
           02 MLIBELLEC PIC X.                                          00002230
           02 MLIBELLEP PIC X.                                          00002240
           02 MLIBELLEH PIC X.                                          00002250
           02 MLIBELLEV PIC X.                                          00002260
           02 MLIBELLEO      PIC X(25).                                 00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNLIGNEA  PIC X.                                          00002290
           02 MNLIGNEC  PIC X.                                          00002300
           02 MNLIGNEP  PIC X.                                          00002310
           02 MNLIGNEH  PIC X.                                          00002320
           02 MNLIGNEV  PIC X.                                          00002330
           02 MNLIGNEO  PIC X(2).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MINTERFA  PIC X.                                          00002360
           02 MINTERFC  PIC X.                                          00002370
           02 MINTERFP  PIC X.                                          00002380
           02 MINTERFH  PIC X.                                          00002390
           02 MINTERFV  PIC X.                                          00002400
           02 MINTERFO  PIC X(5).                                       00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MTVAA     PIC X.                                          00002430
           02 MTVAC     PIC X.                                          00002440
           02 MTVAP     PIC X.                                          00002450
           02 MTVAH     PIC X.                                          00002460
           02 MTVAV     PIC X.                                          00002470
           02 MTVAO     PIC X(3).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MTYPEOPA  PIC X.                                          00002500
           02 MTYPEOPC  PIC X.                                          00002510
           02 MTYPEOPP  PIC X.                                          00002520
           02 MTYPEOPH  PIC X.                                          00002530
           02 MTYPEOPV  PIC X.                                          00002540
           02 MTYPEOPO  PIC X(5).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNATUREA  PIC X.                                          00002570
           02 MNATUREC  PIC X.                                          00002580
           02 MNATUREP  PIC X.                                          00002590
           02 MNATUREH  PIC X.                                          00002600
           02 MNATUREV  PIC X.                                          00002610
           02 MNATUREO  PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MCRIT1A   PIC X.                                          00002640
           02 MCRIT1C   PIC X.                                          00002650
           02 MCRIT1P   PIC X.                                          00002660
           02 MCRIT1H   PIC X.                                          00002670
           02 MCRIT1V   PIC X.                                          00002680
           02 MCRIT1O   PIC X(5).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MCRIT2A   PIC X.                                          00002710
           02 MCRIT2C   PIC X.                                          00002720
           02 MCRIT2P   PIC X.                                          00002730
           02 MCRIT2H   PIC X.                                          00002740
           02 MCRIT2V   PIC X.                                          00002750
           02 MCRIT2O   PIC X(5).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCRIT3A   PIC X.                                          00002780
           02 MCRIT3C   PIC X.                                          00002790
           02 MCRIT3P   PIC X.                                          00002800
           02 MCRIT3H   PIC X.                                          00002810
           02 MCRIT3V   PIC X.                                          00002820
           02 MCRIT3O   PIC X(5).                                       00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCRIT4A   PIC X.                                          00002850
           02 MCRIT4C   PIC X.                                          00002860
           02 MCRIT4P   PIC X.                                          00002870
           02 MCRIT4H   PIC X.                                          00002880
           02 MCRIT4V   PIC X.                                          00002890
           02 MCRIT4O   PIC X(5).                                       00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCRIT5A   PIC X.                                          00002920
           02 MCRIT5C   PIC X.                                          00002930
           02 MCRIT5P   PIC X.                                          00002940
           02 MCRIT5H   PIC X.                                          00002950
           02 MCRIT5V   PIC X.                                          00002960
           02 MCRIT5O   PIC X(5).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCRIT6A   PIC X.                                          00002990
           02 MCRIT6C   PIC X.                                          00003000
           02 MCRIT6P   PIC X.                                          00003010
           02 MCRIT6H   PIC X.                                          00003020
           02 MCRIT6V   PIC X.                                          00003030
           02 MCRIT6O   PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MSENSA    PIC X.                                          00003060
           02 MSENSC    PIC X.                                          00003070
           02 MSENSP    PIC X.                                          00003080
           02 MSENSH    PIC X.                                          00003090
           02 MSENSV    PIC X.                                          00003100
           02 MSENSO    PIC X.                                          00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MCLIA     PIC X.                                          00003130
           02 MCLIC     PIC X.                                          00003140
           02 MCLIP     PIC X.                                          00003150
           02 MCLIH     PIC X.                                          00003160
           02 MCLIV     PIC X.                                          00003170
           02 MCLIO     PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MSOCEA    PIC X.                                          00003200
           02 MSOCEC    PIC X.                                          00003210
           02 MSOCEP    PIC X.                                          00003220
           02 MSOCEH    PIC X.                                          00003230
           02 MSOCEV    PIC X.                                          00003240
           02 MSOCEO    PIC X(3).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MLIEUEA   PIC X.                                          00003270
           02 MLIEUEC   PIC X.                                          00003280
           02 MLIEUEP   PIC X.                                          00003290
           02 MLIEUEH   PIC X.                                          00003300
           02 MLIEUEV   PIC X.                                          00003310
           02 MLIEUEO   PIC X(3).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MSOCRA    PIC X.                                          00003340
           02 MSOCRC    PIC X.                                          00003350
           02 MSOCRP    PIC X.                                          00003360
           02 MSOCRH    PIC X.                                          00003370
           02 MSOCRV    PIC X.                                          00003380
           02 MSOCRO    PIC X(3).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MLIEURA   PIC X.                                          00003410
           02 MLIEURC   PIC X.                                          00003420
           02 MLIEURP   PIC X.                                          00003430
           02 MLIEURH   PIC X.                                          00003440
           02 MLIEURV   PIC X.                                          00003450
           02 MLIEURO   PIC X(3).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MNUMOPA   PIC X.                                          00003480
           02 MNUMOPC   PIC X.                                          00003490
           02 MNUMOPP   PIC X.                                          00003500
           02 MNUMOPH   PIC X.                                          00003510
           02 MNUMOPV   PIC X.                                          00003520
           02 MNUMOPO   PIC X(15).                                      00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MDATOPA   PIC X.                                          00003550
           02 MDATOPC   PIC X.                                          00003560
           02 MDATOPP   PIC X.                                          00003570
           02 MDATOPH   PIC X.                                          00003580
           02 MDATOPV   PIC X.                                          00003590
           02 MDATOPO   PIC X(8).                                       00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MLETTR4A  PIC X.                                          00003620
           02 MLETTR4C  PIC X.                                          00003630
           02 MLETTR4P  PIC X.                                          00003640
           02 MLETTR4H  PIC X.                                          00003650
           02 MLETTR4V  PIC X.                                          00003660
           02 MLETTR4O  PIC X(10).                                      00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MLETTR5A  PIC X.                                          00003690
           02 MLETTR5C  PIC X.                                          00003700
           02 MLETTR5P  PIC X.                                          00003710
           02 MLETTR5H  PIC X.                                          00003720
           02 MLETTR5V  PIC X.                                          00003730
           02 MLETTR5O  PIC X(10).                                      00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MMONTANTA      PIC X.                                     00003760
           02 MMONTANTC PIC X.                                          00003770
           02 MMONTANTP PIC X.                                          00003780
           02 MMONTANTH PIC X.                                          00003790
           02 MMONTANTV PIC X.                                          00003800
           02 MMONTANTO      PIC X(5).                                  00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MCDEVISEA      PIC X.                                     00003830
           02 MCDEVISEC PIC X.                                          00003840
           02 MCDEVISEP PIC X.                                          00003850
           02 MCDEVISEH PIC X.                                          00003860
           02 MCDEVISEV PIC X.                                          00003870
           02 MCDEVISEO      PIC X(3).                                  00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MLMVT1A   PIC X.                                          00003900
           02 MLMVT1C   PIC X.                                          00003910
           02 MLMVT1P   PIC X.                                          00003920
           02 MLMVT1H   PIC X.                                          00003930
           02 MLMVT1V   PIC X.                                          00003940
           02 MLMVT1O   PIC X(25).                                      00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLMVT2A   PIC X.                                          00003970
           02 MLMVT2C   PIC X.                                          00003980
           02 MLMVT2P   PIC X.                                          00003990
           02 MLMVT2H   PIC X.                                          00004000
           02 MLMVT2V   PIC X.                                          00004010
           02 MLMVT2O   PIC X(25).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MLMVT3A   PIC X.                                          00004040
           02 MLMVT3C   PIC X.                                          00004050
           02 MLMVT3P   PIC X.                                          00004060
           02 MLMVT3H   PIC X.                                          00004070
           02 MLMVT3V   PIC X.                                          00004080
           02 MLMVT3O   PIC X(25).                                      00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MLMVT4A   PIC X.                                          00004110
           02 MLMVT4C   PIC X.                                          00004120
           02 MLMVT4P   PIC X.                                          00004130
           02 MLMVT4H   PIC X.                                          00004140
           02 MLMVT4V   PIC X.                                          00004150
           02 MLMVT4O   PIC X(25).                                      00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MLMVT5A   PIC X.                                          00004180
           02 MLMVT5C   PIC X.                                          00004190
           02 MLMVT5P   PIC X.                                          00004200
           02 MLMVT5H   PIC X.                                          00004210
           02 MLMVT5V   PIC X.                                          00004220
           02 MLMVT5O   PIC X(25).                                      00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MCPAYSA   PIC X.                                          00004250
           02 MCPAYSC   PIC X.                                          00004260
           02 MCPAYSP   PIC X.                                          00004270
           02 MCPAYSH   PIC X.                                          00004280
           02 MCPAYSV   PIC X.                                          00004290
           02 MCPAYSO   PIC X(3).                                       00004300
           02 FILLER    PIC X(2).                                       00004310
           02 MWTVAA    PIC X.                                          00004320
           02 MWTVAC    PIC X.                                          00004330
           02 MWTVAP    PIC X.                                          00004340
           02 MWTVAH    PIC X.                                          00004350
           02 MWTVAV    PIC X.                                          00004360
           02 MWTVAO    PIC X.                                          00004370
           02 FILLER    PIC X(2).                                       00004380
           02 MREFDOCA  PIC X.                                          00004390
           02 MREFDOCC  PIC X.                                          00004400
           02 MREFDOCP  PIC X.                                          00004410
           02 MREFDOCH  PIC X.                                          00004420
           02 MREFDOCV  PIC X.                                          00004430
           02 MREFDOCO  PIC X(14).                                      00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MNFOURA   PIC X.                                          00004460
           02 MNFOURC   PIC X.                                          00004470
           02 MNFOURP   PIC X.                                          00004480
           02 MNFOURH   PIC X.                                          00004490
           02 MNFOURV   PIC X.                                          00004500
           02 MNFOURO   PIC X(15).                                      00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MPC02A    PIC X.                                          00004530
           02 MPC02C    PIC X.                                          00004540
           02 MPC02P    PIC X.                                          00004550
           02 MPC02H    PIC X.                                          00004560
           02 MPC02V    PIC X.                                          00004570
           02 MPC02O    PIC X.                                          00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MPERA     PIC X.                                          00004600
           02 MPERC     PIC X.                                          00004610
           02 MPERP     PIC X.                                          00004620
           02 MPERH     PIC X.                                          00004630
           02 MPERV     PIC X.                                          00004640
           02 MPERO     PIC X.                                          00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MJOURA    PIC X.                                          00004670
           02 MJOURC    PIC X.                                          00004680
           02 MJOURP    PIC X.                                          00004690
           02 MJOURH    PIC X.                                          00004700
           02 MJOURV    PIC X.                                          00004710
           02 MJOURO    PIC X(2).                                       00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MTRAITSA  PIC X.                                          00004740
           02 MTRAITSC  PIC X.                                          00004750
           02 MTRAITSP  PIC X.                                          00004760
           02 MTRAITSH  PIC X.                                          00004770
           02 MTRAITSV  PIC X.                                          00004780
           02 MTRAITSO  PIC X(5).                                       00004790
      * ZONE CMD AIDA                                                   00004800
           02 FILLER    PIC X(2).                                       00004810
           02 MLIBERRA  PIC X.                                          00004820
           02 MLIBERRC  PIC X.                                          00004830
           02 MLIBERRP  PIC X.                                          00004840
           02 MLIBERRH  PIC X.                                          00004850
           02 MLIBERRV  PIC X.                                          00004860
           02 MLIBERRO  PIC X(79).                                      00004870
      * CODE TRANSACTION                                                00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCODTRAA  PIC X.                                          00004900
           02 MCODTRAC  PIC X.                                          00004910
           02 MCODTRAP  PIC X.                                          00004920
           02 MCODTRAH  PIC X.                                          00004930
           02 MCODTRAV  PIC X.                                          00004940
           02 MCODTRAO  PIC X(4).                                       00004950
      * CICS DE TRAVAIL                                                 00004960
           02 FILLER    PIC X(2).                                       00004970
           02 MCICSA    PIC X.                                          00004980
           02 MCICSC    PIC X.                                          00004990
           02 MCICSP    PIC X.                                          00005000
           02 MCICSH    PIC X.                                          00005010
           02 MCICSV    PIC X.                                          00005020
           02 MCICSO    PIC X(5).                                       00005030
      * NETNAME                                                         00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MNETNAMA  PIC X.                                          00005060
           02 MNETNAMC  PIC X.                                          00005070
           02 MNETNAMP  PIC X.                                          00005080
           02 MNETNAMH  PIC X.                                          00005090
           02 MNETNAMV  PIC X.                                          00005100
           02 MNETNAMO  PIC X(8).                                       00005110
      * CODE TERMINAL                                                   00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MSCREENA  PIC X.                                          00005140
           02 MSCREENC  PIC X.                                          00005150
           02 MSCREENP  PIC X.                                          00005160
           02 MSCREENH  PIC X.                                          00005170
           02 MSCREENV  PIC X.                                          00005180
           02 MSCREENO  PIC X(4).                                       00005190
                                                                                
