      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHL1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHL1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL1000.                                                            
           02  HL10-NOMMAP                                                      
               PIC X(0008).                                                     
           02  HL10-ZECRAN                                                      
               PIC X(0008).                                                     
           02  HL10-CTABLE                                                      
               PIC X(0018).                                                     
           02  HL10-CCHAMPR                                                     
               PIC X(0018).                                                     
           02  HL10-CCHAMPL                                                     
               PIC X(0018).                                                     
           02  HL10-NREQUETE                                                    
               PIC S9(5) COMP-3.                                                
           02  HL10-NSEQAFF                                                     
               PIC S9(3) COMP-3.                                                
           02  HL10-LONGAFF                                                     
               PIC S9(3) COMP-3.                                                
           02  HL10-LONGTABLE                                                   
               PIC S9(3) COMP-3.                                                
           02  HL10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHL1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHL1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-NOMMAP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-NOMMAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-ZECRAN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-ZECRAN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-CTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-CTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-CCHAMPR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-CCHAMPR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-CCHAMPL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-CCHAMPL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-NREQUETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-NREQUETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-NSEQAFF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-NSEQAFF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-LONGAFF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-LONGAFF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-LONGTABLE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-LONGTABLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HL10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HL10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
