      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SP90-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-SP90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *-------------------------------------------------------3694-----*        
      * MENU GENERAL (PROGRAMME TSP90)                                 *        
      *---------------------------------------------------------10-----*        
           02 COMM-SP90-APPLI.                                                  
              05 COMM-SP90-CFONCTION      PIC X(3).                             
              05 COMM-SP90-NSOC           PIC X(3).                             
              05 COMM-SP90-PAGE           PIC 99.                               
              05 COMM-SP90-PAGE-MAX       PIC 99.                               
      *----------------------------------------------------------------*        
      * CREATION/MODIFICATION DEROGATIONS (PROGRAMME TSP91)            *        
      *---------------------------------------------------------101----*        
           02 COMM-SP91-APPLI.                                          00420001
              05 COMM-SP91-NCODIC         PIC 9(7).                             
              05 COMM-SP91-LREFFOURN      PIC X(20).                            
              05 COMM-SP91-CFAM           PIC X(5).                             
              05 COMM-SP91-LFAM           PIC X(20).                            
              05 COMM-SP91-CMARQ          PIC X(5).                             
              05 COMM-SP91-LMARQ          PIC X(20).                            
              05 COMM-SP91-DEMAIN         PIC X(10).                            
              05 COMM-SP91-LIGNES  OCCURS 14.                                   
                 10 COMM-SP91-ETAT PIC X.                                       
      *----------------------------------------------------------------*        
      * CONSULTATION DEROGATIONS (PROGRAMME TSP92)                     *        
      *-----------------------------------------------------------46---*        
           02 COMM-SP92-APPLI.                                          00420001
              05 COMM-SP92-NCODIC         PIC 9(7).                             
              05 COMM-SP92-NSOCIETE       PIC 9(3).                             
              05 COMM-SP92-NLIEU          PIC 9(3).                             
              05 COMM-SP92-NZONPRIX       PIC 9(2).                             
              05 COMM-SP92-CFAM           PIC X(5).                             
              05 COMM-SP92-CMARQ          PIC X(5).                             
              05 COMM-SP92-WSEQFAM        PIC 9(5).                             
              05 COMM-SP92-WDACEM         PIC X.                                
              05 COMM-SP92-TAILLE-TS      PIC 9(5).                             
CL1209        05 COMM-SP92-DATEFIN        PIC X(10).                            
      *-------------------------------------------------------          00743701
           02 COMM-SP90-MESSAGE           PIC X(79).                            
CL1209     02 COMM-SP90-FILLER            PIC X(458).                   01720001
CL1209*    02 COMM-SP90-FILLER            PIC X(468).                   01720001
           02 COMM-SP90-PROG              PIC X(3000).                  01720001
      ***************************************************************** 02170001
                                                                                
