      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVRB2000                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRB2000                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVRB2001.                                                    00090001
           02  RB20-NCONTREDBOX                                         00100001
               PIC X(0018).                                             00110001
           02  RB20-NSOCIETE                                            00111001
               PIC X(0003).                                             00112001
           02  RB20-NLIEU                                               00120001
               PIC X(0003).                                             00130001
           02  RB20-NVENTE                                              00140001
               PIC X(0007).                                             00150001
           02  RB20-NDOSSIER                                            00160001
               PIC S9(3) COMP-3.                                        00170001
           02  RB20-NSEQ                                                00180001
               PIC S9(3) COMP-3.                                        00190001
           02  RB20-DMODIF                                              00300001
               PIC X(0008).                                             00310000
           02  RB20-DSYST                                               00320001
               PIC S9(13) COMP-3.                                       00330001
           02  RB20-CVDESCRIPT                                          00331001
               PIC X(0005).                                             00332001
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVRB2000                          00360001
      *---------------------------------------------------------        00370000
      *                                                                 00380000
       01  RVRB2001-FLAGS.                                              00390001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NCONTREDBOX-F                                       00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  RB20-NCONTREDBOX-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NSOCIETE-F                                          00411001
      *        PIC S9(4) COMP.                                          00412001
      *--                                                                       
           02  RB20-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NLIEU-F                                             00420001
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  RB20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NVENTE-F                                            00440001
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  RB20-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NDOSSIER-F                                          00460001
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  RB20-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-NSEQ-F                                              00480001
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  RB20-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-DMODIF-F                                            00600001
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  RB20-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-DSYST-F                                             00620001
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  RB20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RB20-CVDESCRIPT-F                                        00631001
      *        PIC S9(4) COMP.                                          00632001
      *--                                                                       
           02  RB20-CVDESCRIPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
