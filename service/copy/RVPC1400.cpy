      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPC1400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPC1400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC1400.                                                            
           02  PC14-RANDOMNUM                                                   
               PIC X(0019).                                                     
           02  PC14-SERIALNUM                                                   
               PIC X(0013).                                                     
           02  PC14-CARDTYPECODE                                                
               PIC X(0020).                                                     
           02  PC14-IDPROMO                                                     
               PIC X(0032).                                                     
           02  PC14-LPROMO                                                      
               PIC X(0132).                                                     
           02  PC14-NSOCEMIS                                                    
               PIC X(0003).                                                     
           02  PC14-NLIEUEMIS                                                   
               PIC X(0003).                                                     
           02  PC14-DACTSOUHAITE                                                
               PIC X(0008).                                                     
           02  PC14-MTTACT                                                      
               PIC S9(07)V9(02) COMP-3.                                         
           02  PC14-DCREATION                                                   
               PIC X(0008).                                                     
           02  PC14-DMAJ                                                        
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPC1400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC1400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-RANDOMNUM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-RANDOMNUM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-SERIALNUM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-SERIALNUM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-CARDTYPECODE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-CARDTYPECODE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-IDPROMO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-IDPROMO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-LPROMO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-LPROMO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-NSOCEMIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-NSOCEMIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-NLIEUEMIS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-NLIEUEMIS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-DACTSOUHAITE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-DACTSOUHAITE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-MTTACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-MTTACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC14-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PC14-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
