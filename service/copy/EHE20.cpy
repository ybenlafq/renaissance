      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE20   EHE20                                              00000020
      ***************************************************************** 00000030
       01   EHE20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRTL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCTRTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRTF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCTRTI    PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRTL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLTRTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTRTF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLTRTI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCIMPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCIMPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCIMPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCIMPI    PIC X(4).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERS1L      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTIERS1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTIERS1F      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTIERS1I      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRIL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRIF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTRII    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPTRIL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTYPTRIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPTRIF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYPTRII  PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELTL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSELTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSELTF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSELTI    PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERS3L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTIERS3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTIERS3F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTIERS3I      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPLOTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MTYPLOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPLOTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MTYPLOTI  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERS2L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MCTIERS2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTIERS2F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCTIERS2I      PIC X(5).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEMANDL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNDEMANDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEMANDF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNDEMANDI      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDENVOII  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNENVOII  PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEHEDL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MNLIEHEDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEHEDF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNLIEHEDI      PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGHEL    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNGHEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNGHEF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNGHEI    PIC X(7).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSTATUTI  PIC X(3).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEDEPL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCLIEDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEDEPF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCLIEDEPI      PIC X(3).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCTRAITI  PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERS4L      COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MCTIERS4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTIERS4F      PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCTIERS4I      PIC X(5).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRI2L   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCTRI2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTRI2F   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCTRI2I   PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRSELL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MADRSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRSELF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MADRSELI  PIC X.                                          00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRDEBL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MADRDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRDEBF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MADRDEBI  PIC X(7).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRFINL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MADRFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRFINF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MADRFINI  PIC X(7).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(78).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCICSI    PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNETNAMI  PIC X(8).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * SDF: EHE20   EHE20                                              00001310
      ***************************************************************** 00001320
       01   EHE20O REDEFINES EHE20I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUP  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUV  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTIMJOUA  PIC X.                                          00001430
           02 MTIMJOUC  PIC X.                                          00001440
           02 MTIMJOUP  PIC X.                                          00001450
           02 MTIMJOUH  PIC X.                                          00001460
           02 MTIMJOUV  PIC X.                                          00001470
           02 MTIMJOUO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MZONCMDA  PIC X.                                          00001500
           02 MZONCMDC  PIC X.                                          00001510
           02 MZONCMDP  PIC X.                                          00001520
           02 MZONCMDH  PIC X.                                          00001530
           02 MZONCMDV  PIC X.                                          00001540
           02 MZONCMDO  PIC X(15).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MCTRTA    PIC X.                                          00001570
           02 MCTRTC    PIC X.                                          00001580
           02 MCTRTP    PIC X.                                          00001590
           02 MCTRTH    PIC X.                                          00001600
           02 MCTRTV    PIC X.                                          00001610
           02 MCTRTO    PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLTRTA    PIC X.                                          00001640
           02 MLTRTC    PIC X.                                          00001650
           02 MLTRTP    PIC X.                                          00001660
           02 MLTRTH    PIC X.                                          00001670
           02 MLTRTV    PIC X.                                          00001680
           02 MLTRTO    PIC X(20).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCIMPA    PIC X.                                          00001710
           02 MCIMPC    PIC X.                                          00001720
           02 MCIMPP    PIC X.                                          00001730
           02 MCIMPH    PIC X.                                          00001740
           02 MCIMPV    PIC X.                                          00001750
           02 MCIMPO    PIC X(4).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCTIERS1A      PIC X.                                     00001780
           02 MCTIERS1C PIC X.                                          00001790
           02 MCTIERS1P PIC X.                                          00001800
           02 MCTIERS1H PIC X.                                          00001810
           02 MCTIERS1V PIC X.                                          00001820
           02 MCTIERS1O      PIC X(5).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MCTRIA    PIC X.                                          00001850
           02 MCTRIC    PIC X.                                          00001860
           02 MCTRIP    PIC X.                                          00001870
           02 MCTRIH    PIC X.                                          00001880
           02 MCTRIV    PIC X.                                          00001890
           02 MCTRIO    PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTYPTRIA  PIC X.                                          00001920
           02 MTYPTRIC  PIC X.                                          00001930
           02 MTYPTRIP  PIC X.                                          00001940
           02 MTYPTRIH  PIC X.                                          00001950
           02 MTYPTRIV  PIC X.                                          00001960
           02 MTYPTRIO  PIC X.                                          00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MSELTA    PIC X.                                          00001990
           02 MSELTC    PIC X.                                          00002000
           02 MSELTP    PIC X.                                          00002010
           02 MSELTH    PIC X.                                          00002020
           02 MSELTV    PIC X.                                          00002030
           02 MSELTO    PIC X.                                          00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCTIERS3A      PIC X.                                     00002060
           02 MCTIERS3C PIC X.                                          00002070
           02 MCTIERS3P PIC X.                                          00002080
           02 MCTIERS3H PIC X.                                          00002090
           02 MCTIERS3V PIC X.                                          00002100
           02 MCTIERS3O      PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MTYPLOTA  PIC X.                                          00002130
           02 MTYPLOTC  PIC X.                                          00002140
           02 MTYPLOTP  PIC X.                                          00002150
           02 MTYPLOTH  PIC X.                                          00002160
           02 MTYPLOTV  PIC X.                                          00002170
           02 MTYPLOTO  PIC X.                                          00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCTIERS2A      PIC X.                                     00002200
           02 MCTIERS2C PIC X.                                          00002210
           02 MCTIERS2P PIC X.                                          00002220
           02 MCTIERS2H PIC X.                                          00002230
           02 MCTIERS2V PIC X.                                          00002240
           02 MCTIERS2O      PIC X(5).                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNDEMANDA      PIC X.                                     00002270
           02 MNDEMANDC PIC X.                                          00002280
           02 MNDEMANDP PIC X.                                          00002290
           02 MNDEMANDH PIC X.                                          00002300
           02 MNDEMANDV PIC X.                                          00002310
           02 MNDEMANDO      PIC X(7).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDENVOIA  PIC X.                                          00002340
           02 MDENVOIC  PIC X.                                          00002350
           02 MDENVOIP  PIC X.                                          00002360
           02 MDENVOIH  PIC X.                                          00002370
           02 MDENVOIV  PIC X.                                          00002380
           02 MDENVOIO  PIC X(10).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNENVOIA  PIC X.                                          00002410
           02 MNENVOIC  PIC X.                                          00002420
           02 MNENVOIP  PIC X.                                          00002430
           02 MNENVOIH  PIC X.                                          00002440
           02 MNENVOIV  PIC X.                                          00002450
           02 MNENVOIO  PIC 9(7).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNLIEHEDA      PIC X.                                     00002480
           02 MNLIEHEDC PIC X.                                          00002490
           02 MNLIEHEDP PIC X.                                          00002500
           02 MNLIEHEDH PIC X.                                          00002510
           02 MNLIEHEDV PIC X.                                          00002520
           02 MNLIEHEDO      PIC X(3).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNGHEA    PIC X.                                          00002550
           02 MNGHEC    PIC X.                                          00002560
           02 MNGHEP    PIC X.                                          00002570
           02 MNGHEH    PIC X.                                          00002580
           02 MNGHEV    PIC X.                                          00002590
           02 MNGHEO    PIC X(7).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MSTATUTA  PIC X.                                          00002620
           02 MSTATUTC  PIC X.                                          00002630
           02 MSTATUTP  PIC X.                                          00002640
           02 MSTATUTH  PIC X.                                          00002650
           02 MSTATUTV  PIC X.                                          00002660
           02 MSTATUTO  PIC X(3).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCLIEDEPA      PIC X.                                     00002690
           02 MCLIEDEPC PIC X.                                          00002700
           02 MCLIEDEPP PIC X.                                          00002710
           02 MCLIEDEPH PIC X.                                          00002720
           02 MCLIEDEPV PIC X.                                          00002730
           02 MCLIEDEPO      PIC X(3).                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MCTRAITA  PIC X.                                          00002760
           02 MCTRAITC  PIC X.                                          00002770
           02 MCTRAITP  PIC X.                                          00002780
           02 MCTRAITH  PIC X.                                          00002790
           02 MCTRAITV  PIC X.                                          00002800
           02 MCTRAITO  PIC X(5).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCTIERS4A      PIC X.                                     00002830
           02 MCTIERS4C PIC X.                                          00002840
           02 MCTIERS4P PIC X.                                          00002850
           02 MCTIERS4H PIC X.                                          00002860
           02 MCTIERS4V PIC X.                                          00002870
           02 MCTIERS4O      PIC X(5).                                  00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCTRI2A   PIC X.                                          00002900
           02 MCTRI2C   PIC X.                                          00002910
           02 MCTRI2P   PIC X.                                          00002920
           02 MCTRI2H   PIC X.                                          00002930
           02 MCTRI2V   PIC X.                                          00002940
           02 MCTRI2O   PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MADRSELA  PIC X.                                          00002970
           02 MADRSELC  PIC X.                                          00002980
           02 MADRSELP  PIC X.                                          00002990
           02 MADRSELH  PIC X.                                          00003000
           02 MADRSELV  PIC X.                                          00003010
           02 MADRSELO  PIC X.                                          00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MADRDEBA  PIC X.                                          00003040
           02 MADRDEBC  PIC X.                                          00003050
           02 MADRDEBP  PIC X.                                          00003060
           02 MADRDEBH  PIC X.                                          00003070
           02 MADRDEBV  PIC X.                                          00003080
           02 MADRDEBO  PIC X(7).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MADRFINA  PIC X.                                          00003110
           02 MADRFINC  PIC X.                                          00003120
           02 MADRFINP  PIC X.                                          00003130
           02 MADRFINH  PIC X.                                          00003140
           02 MADRFINV  PIC X.                                          00003150
           02 MADRFINO  PIC X(7).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLIBERRA  PIC X.                                          00003180
           02 MLIBERRC  PIC X.                                          00003190
           02 MLIBERRP  PIC X.                                          00003200
           02 MLIBERRH  PIC X.                                          00003210
           02 MLIBERRV  PIC X.                                          00003220
           02 MLIBERRO  PIC X(78).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MCODTRAA  PIC X.                                          00003250
           02 MCODTRAC  PIC X.                                          00003260
           02 MCODTRAP  PIC X.                                          00003270
           02 MCODTRAH  PIC X.                                          00003280
           02 MCODTRAV  PIC X.                                          00003290
           02 MCODTRAO  PIC X(4).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCICSA    PIC X.                                          00003320
           02 MCICSC    PIC X.                                          00003330
           02 MCICSP    PIC X.                                          00003340
           02 MCICSH    PIC X.                                          00003350
           02 MCICSV    PIC X.                                          00003360
           02 MCICSO    PIC X(5).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MNETNAMA  PIC X.                                          00003390
           02 MNETNAMC  PIC X.                                          00003400
           02 MNETNAMP  PIC X.                                          00003410
           02 MNETNAMH  PIC X.                                          00003420
           02 MNETNAMV  PIC X.                                          00003430
           02 MNETNAMO  PIC X(8).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MSCREENA  PIC X.                                          00003460
           02 MSCREENC  PIC X.                                          00003470
           02 MSCREENP  PIC X.                                          00003480
           02 MSCREENH  PIC X.                                          00003490
           02 MSCREENV  PIC X.                                          00003500
           02 MSCREENO  PIC X(4).                                       00003510
                                                                                
