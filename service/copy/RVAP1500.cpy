      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAP1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAP1500                         
      *---------------------------------------------------------                
      *                                                                         
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAP1500.                                                            
           02  AP15-NSEQID         PIC S9(18) USAGE COMP-3.                     
           02  AP15-DCREATION      PIC X(0008).                                 
           02  AP15-CORIGINE       PIC X(0005).                                 
           02  AP15-NVENTE         PIC X(0014).                                 
           02  AP15-NLIGNE         PIC X(0003).                                 
           02  AP15-DLGVTE         PIC X(0008).                                 
           02  AP15-NCRI           PIC X(0017).                                 
           02  AP15-NENCAISSE      PIC X(0014).                                 
           02  AP15-DENCAISSE      PIC X(0008).                                 
           02  AP15-NCLIENT        PIC X(0008).                                 
           02  AP15-CPRESTVTE      PIC X(0005).                                 
           02  AP15-CPRESTCRI      PIC X(0005).                                 
           02  AP15-PPRESTA        PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP15-NLIEU          PIC X(0003).                                 
           02  AP15-NSOCA2I        PIC X(0003).                                 
           02  AP15-DANNUL         PIC X(0008).                                 
           02  AP15-NVTEANNUL      PIC X(0014).                                 
           02  AP15-NLGANNUL       PIC X(0003).                                 
           02  AP15-NENCAISANN     PIC X(0014).                                 
           02  AP15-NIDFACTURE     PIC S9(18) USAGE COMP-3.                     
           02  AP15-DRAPPRO        PIC X(0008).                                 
           02  AP15-DIMPUTE        PIC X(0008).                                 
           02  AP15-PMONTCESU      PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP15-CMODEAUTR      PIC X(0005).                                 
           02  AP15-PMONTAUTR      PIC S9(7)V9(2) USAGE COMP-3.                 
           02  AP15-DATTEST        PIC X(0008).                                 
           02  AP15-CTITRENOM1     PIC X(0005).                                 
           02  AP15-LNOM1          PIC X(0025).                                 
           02  AP15-LPRENOM1       PIC X(0015).                                 
           02  AP15-LADR1A         PIC X(0038).                                 
           02  AP15-LADR1B         PIC X(0038).                                 
           02  AP15-LADR1C         PIC X(0038).                                 
           02  AP15-LADR1D         PIC X(0038).                                 
           02  AP15-CTITRENOM2     PIC X(0005).                                 
           02  AP15-LNOM2          PIC X(0025).                                 
           02  AP15-LPRENOM2       PIC X(0015).                                 
           02  AP15-LADR2A         PIC X(0038).                                 
           02  AP15-LADR2B         PIC X(0038).                                 
           02  AP15-LADR2C         PIC X(0038).                                 
           02  AP15-LADR2D         PIC X(0038).                                 
           02  AP15-DSYST          PIC S9(13) USAGE COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAP1500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAP1500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NSEQID-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NSEQID-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DCREATION-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DCREATION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CORIGINE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CORIGINE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NLIGNE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NLIGNE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DLGVTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DLGVTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NCRI-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NCRI-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DENCAISSE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DENCAISSE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NCLIENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NCLIENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CPRESTVTE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CPRESTVTE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CPRESTCRI-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CPRESTCRI-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-PPRESTA-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-PPRESTA-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NSOCA2I-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NSOCA2I-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DANNUL-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DANNUL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NVTEANNUL-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NVTEANNUL-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NLGANNUL-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NLGANNUL-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NENCAISANN-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NENCAISANN-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-NIDFACTURE-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-NIDFACTURE-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DRAPPRO-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DRAPPRO-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DIMPUTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DIMPUTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-PMONTCESU-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-PMONTCESU-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CMODEAUTR-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CMODEAUTR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-PMONTAUTR-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-PMONTAUTR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DATTEST-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-DATTEST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CTITRENOM1-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CTITRENOM1-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LNOM1-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LNOM1-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LPRENOM1-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LPRENOM1-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR1A-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR1A-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR1B-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR1B-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR1C-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR1C-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR1D-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR1D-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-CTITRENOM2-F   PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-CTITRENOM2-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LNOM2-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LNOM2-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LPRENOM2-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LPRENOM2-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR2A-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR2A-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR2B-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR2B-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR2C-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR2C-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-LADR2D-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  AP15-LADR2D-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AP15-DSYST-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           02  AP15-DSYST-F        PIC S9(4) COMP-5.                            
           EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
