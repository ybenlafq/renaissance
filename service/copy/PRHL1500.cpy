      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE HL1500                       
      ******************************************************************        
      *                                                                         
       CLEF-HL1500             SECTION.                                         
      *                                                                         
           MOVE 'RVHL1500          '       TO   TABLE-NAME.                     
           MOVE 'HL1500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-HL1500. EXIT.                                                   
                EJECT                                                           
                                                                                
