      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                        00000080
      * ------------------------------------------------------------ *          
      *                                                              *          
      *                                                              *          
      *                                                              *          
      *                                                              *          
      *                                                              *          
      *                                                              *          
      * ------------------------------------------------------------ *          
       01  COMM-PC40-APPLI.                                                     
          05 ZONES-CTRL-APPEL                PIC 9(01).                         
             88 COMM-APPEL-MPC40-NON         VALUE 0.                           
             88 COMM-APPEL-MPC40-OUI         VALUE 1.                           
          05 COMM-PC40-ZIN.                                                     
             10 COMM-PC40-VENTE.                                                
                15 COMM-PC40-NSOCIETE           PIC X(03).                      
                15 COMM-PC40-NLIEU              PIC X(03).                      
                15 COMM-PC40-NVENTE             PIC X(07).                      
             10 COMM-PC40-ADRESSE.                                              
                15 COMM-PC40-CTITRENOM       PIC X(0005).                       
                15 COMM-PC40-LNOM            PIC X(0025).                       
                15 COMM-PC40-LPRENOM         PIC X(0015).                       
                15 COMM-PC40-LBATIMENT       PIC X(0003).                       
                15 COMM-PC40-LESCALIER       PIC X(0003).                       
                15 COMM-PC40-LETAGE          PIC X(0003).                       
                15 COMM-PC40-LPORTE          PIC X(0003).                       
                15 COMM-PC40-LCMPAD1         PIC X(0032).                       
                15 COMM-PC40-LCMPAD2         PIC X(0032).                       
                15 COMM-PC40-CVOIE           PIC X(0005).                       
                15 COMM-PC40-CTVOIE          PIC X(0004).                       
                15 COMM-PC40-LNOMVOIE        PIC X(0021).                       
                15 COMM-PC40-LCOMMUNE        PIC X(0032).                       
                15 COMM-PC40-CPOSTAL         PIC X(0005).                       
                15 COMM-PC40-LBUREAU         PIC X(0026).                       
                                                                        00000130
             10 COMM-PC40-MAX-T1             PIC S9(02) COMP-3 VALUE 50.        
             10 COMM-PC40-CPT-T1             PIC S9(02) COMP-3.                 
             10 COMM-PC40-TABLE-T1.                                             
               15 COMM-PC40-T1         OCCURS 50.                               
                 20 COMM-PC40-ORDERID         PIC X(11).                        
                 20 COMM-PC40-ORDERITID       PIC X(11).                        
                 20 COMM-PC40-TYPE            PIC X(03).                        
                 20 COMM-PC40-LEGACYID        PIC X(20).                        
                 20 COMM-PC40-MEOP            PIC S9(7)V99 COMP-3.              
                 20 COMM-PC40-MEOPDP          PIC S9(7)V99 COMP-3.              
                 20 COMM-PC40-CLOS            PIC X(01).                        
                 20 COMM-PC40-DSYST           PIC S9(13)V USAGE COMP-3.         
                                                                        00000130
             10 COMM-PC40-MAX-T2             PIC S9(02) COMP-3 VALUE 40.        
             10 COMM-PC40-CPT-T2             PIC S9(02) COMP-3.                 
             10 COMM-PC40-TABLE-T2.                                             
                15 COMM-PC40-T2         OCCURS 40.                              
                   20 COMM-PC40-TABLE-NUM        PIC X(19).                     
                   20 COMM-PC40-CPREST           PIC X(05).                     
                   20 COMM-PC40-DACTIV           PIC X(08).                     
          05 FILLER                              PIC X(2626).                   
          05 COMM-PC40-ZOUT.                                                    
             10 COMM-PC40-CODRET            PIC X(02)  VALUE '00'.              
                88 COMM-PC40-OK             VALUE '00'.                         
                88 COMM-PC40-KO             VALUE '01' THRU '99'.               
             10 COMM-PC40-LIBERR            PIC X(80).                          
          05 FILLER                              PIC X(2626).                   
                                                                        00000130
