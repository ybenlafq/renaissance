      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISP011 AU 10/04/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISP011.                                                        
            05 NOMETAT-ISP011           PIC X(6) VALUE 'ISP011'.                
            05 RUPTURES-ISP011.                                                 
           10 ISP011-NSOCEMET           PIC X(03).                      007  003
           10 ISP011-NSOCRECEP          PIC X(03).                      010  003
           10 ISP011-NUMFACT            PIC X(07).                      013  007
           10 ISP011-CMARQ              PIC X(05).                      020  005
           10 ISP011-CFAM               PIC X(05).                      025  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISP011-SEQUENCE           PIC S9(04) COMP.                030  002
      *--                                                                       
           10 ISP011-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISP011.                                                   
           10 ISP011-LSOCEMET           PIC X(25).                      032  025
           10 ISP011-LSOCRECEP          PIC X(25).                      057  025
           10 ISP011-MOISTRT            PIC X(05).                      082  005
           10 ISP011-NCODIC             PIC X(07).                      087  007
           10 ISP011-REFERENCE          PIC X(20).                      094  020
           10 ISP011-TYPFACT            PIC X(01).                      114  001
           10 ISP011-QTE                PIC S9(07)      COMP-3.         115  004
           10 ISP011-VALOPRMP           PIC S9(09)V9(2) COMP-3.         119  006
           10 ISP011-VALOSRP            PIC S9(09)V9(2) COMP-3.         125  006
           10 ISP011-DATEFACT           PIC X(08).                      131  008
            05 FILLER                      PIC X(374).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISP011-LONG           PIC S9(4)   COMP  VALUE +138.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISP011-LONG           PIC S9(4) COMP-5  VALUE +138.           
                                                                                
      *}                                                                        
