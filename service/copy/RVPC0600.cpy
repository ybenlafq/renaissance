      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPC0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPC0600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0600.                                                            
           02  PC06-CCARTE         PIC X(07).                                   
           02  PC06-LCARTE         PIC X(20).                                   
           02  PC06-NCOMMANDE      PIC X(12).                                   
           02  PC06-REF-LOGO       PIC X(20).                                   
           02  PC06-NSERIE-DEB     PIC X(13).                                   
           02  PC06-NSERIE-FIN     PIC X(13).                                   
           02  PC06-PRIX-UNIT      PIC S9(06)V9(0002) COMP-3.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-QTE            PIC S9(05) COMP.                             
      *--                                                                       
           02  PC06-QTE            PIC S9(05) COMP-5.                           
      *}                                                                        
           02  PC06-MT-TTC         PIC S9(06)V9(0002) COMP-3.                   
           02  PC06-NERR           PIC S9(05) COMP-3.                           
           02  PC06-DSYST          PIC S9(13) COMP-3.                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA RVPC0600                                        
      *---------------------------------------------------------                
      *                                                                         
       01  RVPC0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-CCARTE-F        PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-CCARTE-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-LCARTE-F        PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-LCARTE-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-NCOMMANDE-F     PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-NCOMMANDE-F     PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-REF-LOGO-F      PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-REF-LOGO-F      PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-NSERIE-DEB-F    PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-NSERIE-DEB-F    PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-NSERIE-FIN-F    PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-NSERIE-FIN-F    PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-PRIX-UNIT-F     PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-PRIX-UNIT-F     PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-QTE-F           PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-QTE-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-MT-TTC-F        PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-MT-TTC-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-NERR-F          PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-NERR-F          PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PC06-DSYST-F         PIC S9(4) COMP.                             
      *--                                                                       
           02  PC06-DSYST-F         PIC S9(4) COMP-5.                           
      *}                                                                        
       EJECT                                                                    
                                                                                
