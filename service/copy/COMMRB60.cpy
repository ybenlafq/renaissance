      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES CONTRATS DARTYBOX                     *        
      * TRANSACTION: RB60                                              *        
      * TITRE      : COMMAREA DU MENU DE MISE � JOUR DES MODIFICATION  *        
      *              DE CONTRAT REDBOX                                 *        
      * LONGUEUR   : ????                                              *        
      *                                                                *        
      *                                                                *        
      *                                                                *        
      * -------------------------------------------------------------- *        
           05 COMM-RB60-ZIN.                                                    
               10 COMM-RB60-CAPPEL           PIC X(01).                         
                  88 COMM-RB60-VTE           VALUE 'V'.                         
                  88 COMM-RB60-CONTRAT       VALUE 'C'.                         
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-RB60-SOCM             PIC X(03).                         
               10 COMM-RB60-LIEUM            PIC X(03).                         
               10 COMM-RB60-LLIEUM           PIC X(20).                         
               10 COMM-RB60-CTYPLIEUM        PIC X(01).                         
               10 COMM-RB60-CTYPSOCM         PIC X(03).                         
               10 COMM-RB60-SOCV             PIC X(03).                         
               10 COMM-RB60-LIEUV            PIC X(03).                         
               10 COMM-RB60-VENTEV           PIC X(07).                         
               10 COMM-RB60-CONTRATV         PIC X(50).                         
           05 COMM-RB60-ZOUT.                                                   
               10 COMM-RB60-CODRET           PIC X(04).                         
               10 COMM-RB60-WLIBRET          PIC X(80).                         
               10 COMM-RB60-CTITRENOM        PIC X(05).                         
               10 COMM-RB60-LNOM             PIC X(25).                         
               10 COMM-RB60-LPRENOM          PIC X(15).                         
               10 COMM-RB60-CVOIE            PIC X(05).                         
               10 COMM-RB60-CTVOIE           PIC X(05).                         
               10 COMM-RB60-LNOMVOIE         PIC X(20).                         
               10 COMM-RB60-LCOMMUNE         PIC X(32).                         
               10 COMM-RB60-CPOSTAL          PIC X(05).                         
               10 COMM-RB60-VENTE.                                              
                  15 COMM-RB60-VENTE-LIGNES OCCURS 40.                          
                     30 COMM-RB60-NCODIC PIC X(07).                             
                     30 COMM-RB60-CENREG PIC X(05).                             
                     30 COMM-RB60-CTYPENREG  PIC X(01).                         
                     30 COMM-RB60-NSEQNQ     PIC 9(05) COMP-3.                  
               10 COMM-RB60-VTE-SUP  OCCURS 40.                                 
                 15 COMM-RB60-TTRAITS   PIC    X(01).                           
                 15 COMM-RB60-COFFRES   PIC    X(07).                           
                 15 COMM-RB60-CPRESTS   PIC    X(07).                           
                 15 COMM-RB60-QUANTITES PIC    9(05).                           
                 15 COMM-RB60-PVENTES   PIC    9(07)V99.                        
                 15 COMM-RB60-CMODDELS  PIC    X(05).                           
                 15 COMM-RB60-DDELIVS   PIC    X(08).                           
                 15 COMM-RB60-FVENDS    PIC    X(03).                           
                 15 COMM-RB60-CVENDS    PIC    X(07).                           
                 15 COMM-RB60-NSEREQS   PIC    X(15).                           
               10 COMM-RB60-VTE-CRE  OCCURS 40.                                 
                 15 COMM-RB60-TTRAITC   PIC    X(01).                           
                 15 COMM-RB60-COFFREC   PIC    X(07).                           
                 15 COMM-RB60-CPRESTC   PIC    X(07).                           
                 15 COMM-RB60-QUANTITEC PIC    9(05).                           
                 15 COMM-RB60-PVENTEC   PIC    9(07)V99.                        
                 15 COMM-RB60-CMODDELC  PIC    X(05).                           
                 15 COMM-RB60-DDELIVC   PIC    X(08).                           
                 15 COMM-RB60-FVENDC    PIC    X(03).                           
                 15 COMM-RB60-CVENDC    PIC    X(07).                           
                 15 COMM-RB60-NSEREQC   PIC    X(15).                           
               10 COMM-RB60-OFFRE-AV.                                           
                 15 COMM-RB60-COFFRE1   PIC    X(05).                           
                 15 COMM-RB60-CENREG1   PIC    X(07).                           
                 15 COMM-RB60-DOFFRE1   PIC    X(08).                           
                 15 COMM-RB60-LOFFRE1   PIC    X(20).                           
               10 COMM-RB60-DB003-TAB.                                          
                12 COMM-RB60-DB003 OCCURS 10.                                   
                 15 COMM-RB60-CHANGE003 PIC    X(05).                           
                 15 COMM-RB60-LOFFRE003 PIC    X(20).                           
                 15 COMM-RB60-CHOIX003  PIC    X(01).                           
                 15 COMM-RB60-OFFRE003  PIC    X(05).                           
               10 COMM-RB60-VEND.                                               
                 15 COMM-RB60-CVEND     PIC    X(06).                           
                 15 COMM-RB60-LVEND     PIC    X(15).                           
               10 COMM-RB60-DB004-TAB.                                          
                12 COMM-RB60-DB004 OCCURS 12.                                   
                 15 COMM-RB60-COPTA004  PIC    X(05).                           
                 15 COMM-RB60-LOPTA004  PIC    X(20).                           
                 15 COMM-RB60-ANCIEN04  PIC    X(01).                           
                 15 COMM-RB60-CHOIX004  PIC    X(01).                           
                 15 COMM-RB60-COPTN004  PIC    X(05).                           
                 15 COMM-RB60-LOPTN004  PIC    X(20).                           
                 15 COMM-RB60-AJOUT004  PIC    X(01).                           
                 15 COMM-RB60-SUPRE004  PIC    X(01).                           
                 15 COMM-RB60-NOUVEL04  PIC    X(01).                           
               10 COMM-RB60-FILLER      PIC    X(0499).                         
      *                                                                         
                                                                                
