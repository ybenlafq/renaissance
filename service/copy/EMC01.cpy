      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Codification des groupes                                   00000020
      ***************************************************************** 00000030
       01   EMC01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
           02 MTABLGRPI OCCURS   10 TIMES .                             00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGRPL  COMP PIC S9(4).                                 00000150
      *--                                                                       
             03 MCGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCGRPF  PIC X.                                          00000160
             03 FILLER  PIC X(4).                                       00000170
             03 MCGRPI  PIC X(5).                                       00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGRPL  COMP PIC S9(4).                                 00000190
      *--                                                                       
             03 MLGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLGRPF  PIC X.                                          00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MLGRPI  PIC X(20).                                      00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYGRPL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MTYGRPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTYGRPF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MTYGRPI      PIC X(5).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQGRPL    COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MNSEQGRPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSEQGRPF    PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNSEQGRPI    PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFACTIFL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MFACTIFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MFACTIFF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MFACTIFI     PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELGRPL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MSELGRPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELGRPF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MSELGRPI     PIC X.                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGRPNWL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCGRPNWL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGRPNWF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCGRPNWI  PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGRPNWL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLGRPNWL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLGRPNWF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLGRPNWI  PIC X(20).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYGRPNWL      COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MTYGRPNWL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYGRPNWF      PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MTYGRPNWI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQGRPNWL    COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MNSEQGRPNWL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSEQGRPNWF    PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNSEQGRPNWI    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(78).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: Codification des groupes                                   00000760
      ***************************************************************** 00000770
       01   EMC01O REDEFINES EMC01I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 MTABLGRPO OCCURS   10 TIMES .                             00000940
             03 FILLER       PIC X(2).                                  00000950
             03 MCGRPA  PIC X.                                          00000960
             03 MCGRPC  PIC X.                                          00000970
             03 MCGRPP  PIC X.                                          00000980
             03 MCGRPH  PIC X.                                          00000990
             03 MCGRPV  PIC X.                                          00001000
             03 MCGRPO  PIC X(5).                                       00001010
             03 FILLER       PIC X(2).                                  00001020
             03 MLGRPA  PIC X.                                          00001030
             03 MLGRPC  PIC X.                                          00001040
             03 MLGRPP  PIC X.                                          00001050
             03 MLGRPH  PIC X.                                          00001060
             03 MLGRPV  PIC X.                                          00001070
             03 MLGRPO  PIC X(20).                                      00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MTYGRPA      PIC X.                                     00001100
             03 MTYGRPC PIC X.                                          00001110
             03 MTYGRPP PIC X.                                          00001120
             03 MTYGRPH PIC X.                                          00001130
             03 MTYGRPV PIC X.                                          00001140
             03 MTYGRPO      PIC X(5).                                  00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MNSEQGRPA    PIC X.                                     00001170
             03 MNSEQGRPC    PIC X.                                     00001180
             03 MNSEQGRPP    PIC X.                                     00001190
             03 MNSEQGRPH    PIC X.                                     00001200
             03 MNSEQGRPV    PIC X.                                     00001210
             03 MNSEQGRPO    PIC X(5).                                  00001220
             03 FILLER       PIC X(2).                                  00001230
             03 MFACTIFA     PIC X.                                     00001240
             03 MFACTIFC     PIC X.                                     00001250
             03 MFACTIFP     PIC X.                                     00001260
             03 MFACTIFH     PIC X.                                     00001270
             03 MFACTIFV     PIC X.                                     00001280
             03 MFACTIFO     PIC X.                                     00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MSELGRPA     PIC X.                                     00001310
             03 MSELGRPC     PIC X.                                     00001320
             03 MSELGRPP     PIC X.                                     00001330
             03 MSELGRPH     PIC X.                                     00001340
             03 MSELGRPV     PIC X.                                     00001350
             03 MSELGRPO     PIC X.                                     00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCGRPNWA  PIC X.                                          00001380
           02 MCGRPNWC  PIC X.                                          00001390
           02 MCGRPNWP  PIC X.                                          00001400
           02 MCGRPNWH  PIC X.                                          00001410
           02 MCGRPNWV  PIC X.                                          00001420
           02 MCGRPNWO  PIC X(5).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLGRPNWA  PIC X.                                          00001450
           02 MLGRPNWC  PIC X.                                          00001460
           02 MLGRPNWP  PIC X.                                          00001470
           02 MLGRPNWH  PIC X.                                          00001480
           02 MLGRPNWV  PIC X.                                          00001490
           02 MLGRPNWO  PIC X(20).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MTYGRPNWA      PIC X.                                     00001520
           02 MTYGRPNWC PIC X.                                          00001530
           02 MTYGRPNWP PIC X.                                          00001540
           02 MTYGRPNWH PIC X.                                          00001550
           02 MTYGRPNWV PIC X.                                          00001560
           02 MTYGRPNWO      PIC X(5).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNSEQGRPNWA    PIC X.                                     00001590
           02 MNSEQGRPNWC    PIC X.                                     00001600
           02 MNSEQGRPNWP    PIC X.                                     00001610
           02 MNSEQGRPNWH    PIC X.                                     00001620
           02 MNSEQGRPNWV    PIC X.                                     00001630
           02 MNSEQGRPNWO    PIC X(5).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(78).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
