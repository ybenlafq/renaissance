      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFX02   EFX02                                              00000020
      ***************************************************************** 00000030
       01   EFX02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTIONL     COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MFONCTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MFONCTIONF     PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MFONCTIONI     PIC X(3).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPTEGLL     COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MCOMPTEGLL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMPTEGLF     PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MCOMPTEGLI     PIC X(6).                                  00000230
           02 M109I OCCURS   12 TIMES .                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTEGLL     COMP PIC S9(4).                            00000250
      *--                                                                       
             03 MCPTEGLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCPTEGLF     PIC X.                                     00000260
             03 FILLER  PIC X(4).                                       00000270
             03 MCPTEGLI     PIC X(6).                                  00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTECOSDEBITL    COMP PIC S9(4).                       00000290
      *--                                                                       
             03 MCPTECOSDEBITL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MCPTECOSDEBITF    PIC X.                                00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MCPTECOSDEBITI    PIC X(6).                             00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPTECOSCREDITL   COMP PIC S9(4).                       00000330
      *--                                                                       
             03 MCPTECOSCREDITL COMP-5 PIC S9(4).                               
      *}                                                                        
             03 MCPTECOSCREDITF   PIC X.                                00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MCPTECOSCREDITI   PIC X(6).                             00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MZONCMDI  PIC X(15).                                      00000400
      * ZONE MESSAGE                                                    00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBERRI  PIC X(55).                                      00000450
      * CODE TRANSACTION                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      * NOM DU CICS                                                     00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      * NOM LIGNE VTAM                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNETNAMI  PIC X(8).                                       00000600
      * CODE TERMINAL                                                   00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCREENI  PIC X(4).                                       00000650
      ***************************************************************** 00000660
      * SDF: EFX02   EFX02                                              00000670
      ***************************************************************** 00000680
       01   EFX02O REDEFINES EFX02I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
      * DATE JOUR TRAITEMENT                                            00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
      * HEURE TRAITEMENT                                                00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MTIMJOUA  PIC X.                                          00000810
           02 MTIMJOUC  PIC X.                                          00000820
           02 MTIMJOUP  PIC X.                                          00000830
           02 MTIMJOUH  PIC X.                                          00000840
           02 MTIMJOUV  PIC X.                                          00000850
           02 MTIMJOUO  PIC X(5).                                       00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MFONCTIONA     PIC X.                                     00000880
           02 MFONCTIONC     PIC X.                                     00000890
           02 MFONCTIONP     PIC X.                                     00000900
           02 MFONCTIONH     PIC X.                                     00000910
           02 MFONCTIONV     PIC X.                                     00000920
           02 MFONCTIONO     PIC X(3).                                  00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MCOMPTEGLA     PIC X.                                     00000950
           02 MCOMPTEGLC     PIC X.                                     00000960
           02 MCOMPTEGLP     PIC X.                                     00000970
           02 MCOMPTEGLH     PIC X.                                     00000980
           02 MCOMPTEGLV     PIC X.                                     00000990
           02 MCOMPTEGLO     PIC X(6).                                  00001000
           02 M109O OCCURS   12 TIMES .                                 00001010
             03 FILLER       PIC X(2).                                  00001020
             03 MCPTEGLA     PIC X.                                     00001030
             03 MCPTEGLC     PIC X.                                     00001040
             03 MCPTEGLP     PIC X.                                     00001050
             03 MCPTEGLH     PIC X.                                     00001060
             03 MCPTEGLV     PIC X.                                     00001070
             03 MCPTEGLO     PIC X(6).                                  00001080
             03 FILLER       PIC X(2).                                  00001090
             03 MCPTECOSDEBITA    PIC X.                                00001100
             03 MCPTECOSDEBITC    PIC X.                                00001110
             03 MCPTECOSDEBITP    PIC X.                                00001120
             03 MCPTECOSDEBITH    PIC X.                                00001130
             03 MCPTECOSDEBITV    PIC X.                                00001140
             03 MCPTECOSDEBITO    PIC X(6).                             00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MCPTECOSCREDITA   PIC X.                                00001170
             03 MCPTECOSCREDITC   PIC X.                                00001180
             03 MCPTECOSCREDITP   PIC X.                                00001190
             03 MCPTECOSCREDITH   PIC X.                                00001200
             03 MCPTECOSCREDITV   PIC X.                                00001210
             03 MCPTECOSCREDITO   PIC X(6).                             00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MZONCMDA  PIC X.                                          00001240
           02 MZONCMDC  PIC X.                                          00001250
           02 MZONCMDP  PIC X.                                          00001260
           02 MZONCMDH  PIC X.                                          00001270
           02 MZONCMDV  PIC X.                                          00001280
           02 MZONCMDO  PIC X(15).                                      00001290
      * ZONE MESSAGE                                                    00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLIBERRA  PIC X.                                          00001320
           02 MLIBERRC  PIC X.                                          00001330
           02 MLIBERRP  PIC X.                                          00001340
           02 MLIBERRH  PIC X.                                          00001350
           02 MLIBERRV  PIC X.                                          00001360
           02 MLIBERRO  PIC X(55).                                      00001370
      * CODE TRANSACTION                                                00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
      * NOM DU CICS                                                     00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCICSA    PIC X.                                          00001480
           02 MCICSC    PIC X.                                          00001490
           02 MCICSP    PIC X.                                          00001500
           02 MCICSH    PIC X.                                          00001510
           02 MCICSV    PIC X.                                          00001520
           02 MCICSO    PIC X(5).                                       00001530
      * NOM LIGNE VTAM                                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNETNAMA  PIC X.                                          00001560
           02 MNETNAMC  PIC X.                                          00001570
           02 MNETNAMP  PIC X.                                          00001580
           02 MNETNAMH  PIC X.                                          00001590
           02 MNETNAMV  PIC X.                                          00001600
           02 MNETNAMO  PIC X(8).                                       00001610
      * CODE TERMINAL                                                   00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MSCREENA  PIC X.                                          00001640
           02 MSCREENC  PIC X.                                          00001650
           02 MSCREENP  PIC X.                                          00001660
           02 MSCREENH  PIC X.                                          00001670
           02 MSCREENV  PIC X.                                          00001680
           02 MSCREENO  PIC X(4).                                       00001690
                                                                                
