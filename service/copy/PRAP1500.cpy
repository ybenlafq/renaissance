      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE AP1500                       
      ******************************************************************        
      *                                                                         
       CLEF-AP1500             SECTION.                                         
      *                                                                         
           MOVE 'RVAP1500          '       TO   TABLE-NAME.                     
           MOVE 'AP1500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-AP1500. EXIT.                                                   
                EJECT                                                           
                                                                                
