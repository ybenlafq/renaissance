      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHE51   EHE51                                              00000020
      ***************************************************************** 00000030
       01   EHE55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUHTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEUHTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCLIEUHTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEUHTI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUHTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUHTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLLIEUHTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUHTI      PIC X(20).                                 00000290
           02 MLIGNEI OCCURS   12 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLDL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNLDL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNLDF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNLDI   PIC X(3).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHSL   COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNHSF   PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNHSI   PIC X(7).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODL   COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCODF   PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCODI   PIC X(7).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFAML   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFAMF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MFAMI   PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMRQL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MMRQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMRQF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MMRQI   PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MREFI   PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRIL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MTRIL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTRIF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MTRII   PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLTL   COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNLTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNLTF   PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNLTI   PIC X(7).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPANL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MPANL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPANF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MPANI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRFUL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MRFUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MRFUF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MRFUI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EHE51   EHE51                                              00000920
      ***************************************************************** 00000930
       01   EHE55O REDEFINES EHE55I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNUMPAGEA      PIC X.                                     00001110
           02 MNUMPAGEC PIC X.                                          00001120
           02 MNUMPAGEP PIC X.                                          00001130
           02 MNUMPAGEH PIC X.                                          00001140
           02 MNUMPAGEV PIC X.                                          00001150
           02 MNUMPAGEO      PIC 99.                                    00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEMAXA      PIC X.                                     00001180
           02 MPAGEMAXC PIC X.                                          00001190
           02 MPAGEMAXP PIC X.                                          00001200
           02 MPAGEMAXH PIC X.                                          00001210
           02 MPAGEMAXV PIC X.                                          00001220
           02 MPAGEMAXO      PIC 99.                                    00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCLIEUHTA      PIC X.                                     00001250
           02 MCLIEUHTC PIC X.                                          00001260
           02 MCLIEUHTP PIC X.                                          00001270
           02 MCLIEUHTH PIC X.                                          00001280
           02 MCLIEUHTV PIC X.                                          00001290
           02 MCLIEUHTO      PIC X(5).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLLIEUHTA      PIC X.                                     00001320
           02 MLLIEUHTC PIC X.                                          00001330
           02 MLLIEUHTP PIC X.                                          00001340
           02 MLLIEUHTH PIC X.                                          00001350
           02 MLLIEUHTV PIC X.                                          00001360
           02 MLLIEUHTO      PIC X(20).                                 00001370
           02 MLIGNEO OCCURS   12 TIMES .                               00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MNLDA   PIC X.                                          00001400
             03 MNLDC   PIC X.                                          00001410
             03 MNLDP   PIC X.                                          00001420
             03 MNLDH   PIC X.                                          00001430
             03 MNLDV   PIC X.                                          00001440
             03 MNLDO   PIC 9(3).                                       00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MNHSA   PIC X.                                          00001470
             03 MNHSC   PIC X.                                          00001480
             03 MNHSP   PIC X.                                          00001490
             03 MNHSH   PIC X.                                          00001500
             03 MNHSV   PIC X.                                          00001510
             03 MNHSO   PIC 9(7).                                       00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MCODA   PIC X.                                          00001540
             03 MCODC   PIC X.                                          00001550
             03 MCODP   PIC X.                                          00001560
             03 MCODH   PIC X.                                          00001570
             03 MCODV   PIC X.                                          00001580
             03 MCODO   PIC X(7).                                       00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MFAMA   PIC X.                                          00001610
             03 MFAMC   PIC X.                                          00001620
             03 MFAMP   PIC X.                                          00001630
             03 MFAMH   PIC X.                                          00001640
             03 MFAMV   PIC X.                                          00001650
             03 MFAMO   PIC X(5).                                       00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MMRQA   PIC X.                                          00001680
             03 MMRQC   PIC X.                                          00001690
             03 MMRQP   PIC X.                                          00001700
             03 MMRQH   PIC X.                                          00001710
             03 MMRQV   PIC X.                                          00001720
             03 MMRQO   PIC X(5).                                       00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MREFA   PIC X.                                          00001750
             03 MREFC   PIC X.                                          00001760
             03 MREFP   PIC X.                                          00001770
             03 MREFH   PIC X.                                          00001780
             03 MREFV   PIC X.                                          00001790
             03 MREFO   PIC X(20).                                      00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MTRIA   PIC X.                                          00001820
             03 MTRIC   PIC X.                                          00001830
             03 MTRIP   PIC X.                                          00001840
             03 MTRIH   PIC X.                                          00001850
             03 MTRIV   PIC X.                                          00001860
             03 MTRIO   PIC X(5).                                       00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MNLTA   PIC X.                                          00001890
             03 MNLTC   PIC X.                                          00001900
             03 MNLTP   PIC X.                                          00001910
             03 MNLTH   PIC X.                                          00001920
             03 MNLTV   PIC X.                                          00001930
             03 MNLTO   PIC X(7).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MPANA   PIC X.                                          00001960
             03 MPANC   PIC X.                                          00001970
             03 MPANP   PIC X.                                          00001980
             03 MPANH   PIC X.                                          00001990
             03 MPANV   PIC X.                                          00002000
             03 MPANO   PIC X(5).                                       00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MRFUA   PIC X.                                          00002030
             03 MRFUC   PIC X.                                          00002040
             03 MRFUP   PIC X.                                          00002050
             03 MRFUH   PIC X.                                          00002060
             03 MRFUV   PIC X.                                          00002070
             03 MRFUO   PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(78).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
