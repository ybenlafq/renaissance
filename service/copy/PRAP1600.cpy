      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE AP1600                       
      ******************************************************************        
      *                                                                         
       CLEF-AP1600             SECTION.                                         
      *                                                                         
           MOVE 'RVAP1600          '       TO   TABLE-NAME.                     
           MOVE 'AP1600'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-AP1600. EXIT.                                                   
                EJECT                                                           
                                                                                
