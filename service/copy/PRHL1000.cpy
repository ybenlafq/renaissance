      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE HL1000                       
      ******************************************************************        
      *                                                                         
       CLEF-HL1000             SECTION.                                         
      *                                                                         
           MOVE 'RVHL1000          '       TO   TABLE-NAME.                     
           MOVE 'HL1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-HL1000. EXIT.                                                   
                EJECT                                                           
                                                                                
