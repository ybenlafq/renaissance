      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHE012 AU 12/03/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,07,BI,A,                          *        
      *                           29,03,BI,A,                          *        
      *                           32,07,BI,A,                          *        
      *                           39,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHE012.                                                        
            05 NOMETAT-IHE012           PIC X(6) VALUE 'IHE012'.                
            05 RUPTURES-IHE012.                                                 
           10 IHE012-CLIEUHET           PIC X(05).                      007  005
           10 IHE012-CMARQ              PIC X(05).                      012  005
           10 IHE012-CFAM               PIC X(05).                      017  005
           10 IHE012-NCODIC             PIC X(07).                      022  007
           10 IHE012-NLIEUHED           PIC X(03).                      029  003
           10 IHE012-NGHE               PIC X(07).                      032  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHE012-SEQUENCE           PIC S9(04) COMP.                039  002
      *--                                                                       
           10 IHE012-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHE012.                                                   
           10 IHE012-CTRAIT             PIC X(05).                      041  005
           10 IHE012-LREFFOURN          PIC X(20).                      046  020
           10 IHE012-NTYOPE             PIC X(03).                      066  003
           10 IHE012-WQTE               PIC S9(05)      COMP-3.         069  003
            05 FILLER                      PIC X(441).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHE012-LONG           PIC S9(4)   COMP  VALUE +071.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHE012-LONG           PIC S9(4) COMP-5  VALUE +071.           
                                                                                
      *}                                                                        
