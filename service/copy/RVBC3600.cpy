      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVBC3600                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBC3600                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBC3600.                                                            
           02  BC36-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  BC36-NLIEU                                                       
               PIC X(0003).                                                     
           02  BC36-NORDRE                                                      
               PIC X(0005).                                                     
           02  BC36-NVENTE                                                      
               PIC X(0008).                                                     
           02  BC36-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  BC36-EMAIL                                                       
               PIC X(0063).                                                     
           02  BC36-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVBC3600                                  
      **********************************************************                
       01  RVBC3600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-EMAIL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BC36-EMAIL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BC36-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  BC36-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
