      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ QUESTION SOUSCRIPTION/R�SILIATION REDBOX              
      *   VERS LE S.I. REDBOX. (RBxQSouRes)                                     
      *****************************************************************         
      *                                                                         
      *                                                                         
       05 MESRB001-DATA.                                                        
      *                                                                         
      * DONN�ES CLIENT                                                          
      *                                                                         
      *   CODE CLIENT                                                           
          10 MESRB01-CCLIENT       PIC    X(08).                                
      *   CIVILIT�                                                              
          10 MESRB01-CIVILITE      PIC    X(05).                                
      *   NOM CLIENT                                                            
          10 MESRB01-NOMCLI        PIC    X(25).                                
      *   PR�NOM CLIENT                                                         
          10 MESRB01-PRENOM        PIC    X(15).                                
      *   T�L�PHONE FIXE                                                        
          10 MESRB01-TELFIXE       PIC    X(10).                                
      *   T�L�PHONE PORTABLE                                                    
          10 MESRB01-TELPORT       PIC    X(10).                                
      *   T�L�PHONE BUREAU                                                      
          10 MESRB01-TELBUR        PIC    X(10).                                
      *   POSTE BUREAU                                                          
          10 MESRB01-PSTBUR        PIC    X(05).                                
      *   E-MAIL                                                                
          10 MESRB01-EMAIL         PIC    X(63).                                
      *   ADRESSES FACTURATION ET LIVRAISON                                     
          10 MESRB01-ADRESSE OCCURS 2.                                          
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)                   
             15 MESRB01-TYPADR        PIC    X(01).                             
      *      NUM�RO DE VOIE                                                     
             15 MESRB01-NUMVOIE       PIC    X(05).                             
      *      TYPE DE VOIE                                                       
             15 MESRB01-TYPVOIE       PIC    X(04).                             
      *      NOM DE VOIE                                                        
             15 MESRB01-NOMVOIE       PIC    X(21).                             
      *      COMPL�MENT ADRESSE DE LIVRAISON 1                                  
             15 MESRB01-CADRFAC1      PIC    X(32).                             
      *      COMPL�MENT ADRESSE DE LIVRAISON2                                   
             15 MESRB01-CADRFAC2      PIC    X(32).                             
      *      B�TIMENT                                                           
             15 MESRB01-BATIMENT      PIC    X(03).                             
      *      ESCALIER                                                           
             15 MESRB01-ESCALIER      PIC    X(03).                             
      *      �TAGE                                                              
             15 MESRB01-ETAGE         PIC    X(03).                             
      *      PORTE                                                              
             15 MESRB01-PORTE         PIC    X(03).                             
      *      CODE PAYS                                                          
             15 MESRB01-CPAYS         PIC    X(03).                             
      *      CODE INSEE                                                         
             15 MESRB01-CINSEE        PIC    X(05).                             
      *      COMMUNE                                                            
             15 MESRB01-COMMUNE       PIC    X(32).                             
      *      CODE POSTAL                                                        
             15 MESRB01-CPOSTAL       PIC    X(05).                             
      *      CODE client ucm                                                    
             15 MESRB01-idcliucm      PIC    X(08).                             
      *      CODE adresse ucm                                                   
             15 MESRB01-idadrucm      PIC    X(08).                             
      *                                                                         
      *      DONN�ES REDBOX                                                     
      *                                                                         
      *   N� TRANSACTION REDBOX                                                 
          10 MESRB01-TREDBOX       PIC    X(10).                                
      *   N� VENTE GV                                                           
          10 MESRB01-NVENTEGV      PIC    X(13).                                
      *   N� DE CONTRAT                                                         
          10 MESRB01-NCONTRAT      PIC    X(18).                                
      *   ST� DE TRANSACTION                                                    
          10 MESRB01-TSOC          PIC    X(03).                                
      *   LIEU DE TRANSACTION                                                   
          10 MESRB01-TLIEU         PIC    X(03).                                
      *   DATE DE TRANSACTION                                                   
          10 MESRB01-TDATE         PIC    X(08).                                
      *   HEURE DE TRANSACTION                                                  
          10 MESRB01-THEURE        PIC    X(06).                                
      *   TYPE D'ENLEVEMENT ('M' / 'E')                                         
          10 MESRB01-TENLV         PIC    X(01).                                
      *                                                                         
      *      DONN�ES LIGNE DE VENTE                                             
      *                                                                         
      *   *                                                                     
          10 MESRB01-DETVENTE OCCURS 80.                                        
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
             15 MESRB01-TTRAIT     PIC    X(01).                                
      *      CODE OFFRE                                                         
             15 MESRB01-COFFRE     PIC    X(07).                                
      *      CODIC OU CODE PRESTATION                                           
             15 MESRB01-CPREST     PIC    X(07).                                
      *      QUANTIT�                                                           
             15 MESRB01-QUANTITE   PIC    9(05).                                
      *      PRIX DE VENTE                                                      
             15 MESRB01-PVENTE     PIC   S9(07)V99.                             
      *      MODE DE D�LIVRANCE                                                 
             15 MESRB01-CMODDEL    PIC    X(05).                                
      *      DATE DE D�LIVRANCE                                                 
             15 MESRB01-DDELIV     PIC    X(08).                                
      *      FILIALE DU VENDEUR                                                 
             15 MESRB01-FVEND      PIC    X(03).                                
      *      MATRICULE DU VENDEUR                                               
             15 MESRB01-CVEND      PIC    X(07).                                
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
             15 MESRB01-NSEREQ     PIC    X(15).                                
      *                                                                         
                                                                                
