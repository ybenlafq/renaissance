      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC0201                           *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC0201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC0201.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NADRESSE                                          
           10 BC02-NADRESSE        PIC X(8).                                    
      *    *************************************************************        
      *                       WTYPEADR                                          
           10 BC02-WTYPEADR        PIC X(1).                                    
      *    *************************************************************        
      *                       WTYPEADR2                                         
           10 BC02-WTYPEADR2       PIC X(1).                                    
      *    *************************************************************        
      *                       LIG2                                              
           10 BC02-LIG2            PIC X(38).                                   
      *    *************************************************************        
      *                       LIG3                                              
           10 BC02-LIG3            PIC X(38).                                   
      *    *************************************************************        
      *                       LIG4                                              
           10 BC02-LIG4            PIC X(38).                                   
      *    *************************************************************        
      *                       LIG5                                              
           10 BC02-LIG5            PIC X(38).                                   
      *    *************************************************************        
      *                       LIG6                                              
           10 BC02-LIG6            PIC X(38).                                   
      *    *************************************************************        
      *                       CITY_CODE                                         
           10 BC02-CITY-CODE       PIC X(5).                                    
      *    *************************************************************        
      *                       CVOIE                                             
           10 BC02-CVOIE           PIC X(5).                                    
      *    *************************************************************        
      *                       FINDADR                                           
           10 BC02-FINDADR         PIC X(1).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC02-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       CODE PAYS UNISERV                                 
           10 BC02-CPAYS           PIC X(3).                                    
      ******************************************************************        
           10 BC02-CPOSTAL         PIC X(5).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBC0201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBC0201-FLAGS.                                                      
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-NADRESSE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-NADRESSE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-WTYPEADR-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-WTYPEADR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-WTYPEADR2-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-WTYPEADR2-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-LIG2-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-LIG2-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-LIG3-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-LIG3-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-LIG4-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-LIG4-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-LIG5-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-LIG5-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-LIG6-F          PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-LIG6-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-CITY-CODE-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-CITY-CODE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-CVOIE-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-CVOIE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-FINDADR-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-FINDADR-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-CPAYS-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-CPAYS-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC02-CPOSTAL-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC02-CPOSTAL-F       PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *        
      ******************************************************************        
                                                                                
