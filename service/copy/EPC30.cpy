      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPC30   EPC30                                              00000020
      ***************************************************************** 00000030
       01   EPC30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEVL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEVF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEVI   PIC 999.                                        00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEHL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEHF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEHI   PIC 999.                                        00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCFL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCFF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCFI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUFL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUFF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUFI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMFL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLNOMFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMFF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLNOMFI   PIC X(25).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACTIVDL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDACTIVDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACTIVDF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDACTIVDI      PIC X(10).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDACTIVFL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDACTIVFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDACTIVFF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDACTIVFI      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMCARDFL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNUMCARDFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNUMCARDFF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNUMCARDFI     PIC X(16).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRFL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNSOCRFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCRFF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNSOCRFI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEURFL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNLIEURFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEURFF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNLIEURFI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTERFL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNVENTERFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNVENTERFF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNVENTERFI     PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTETEL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MENTETEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTETEF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MENTETEI  PIC X(77).                                      00000610
           02 MTABI OCCURS   15 TIMES .                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSELI   PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIGNEI      PIC X(75).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(5).                                       00000940
      ***************************************************************** 00000950
      * SDF: EPC30   EPC30                                              00000960
      ***************************************************************** 00000970
       01   EPC30O REDEFINES EPC30I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEVA   PIC X.                                          00001150
           02 MPAGEVC   PIC X.                                          00001160
           02 MPAGEVP   PIC X.                                          00001170
           02 MPAGEVH   PIC X.                                          00001180
           02 MPAGEVV   PIC X.                                          00001190
           02 MPAGEVO   PIC 999.                                        00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEHA   PIC X.                                          00001220
           02 MPAGEHC   PIC X.                                          00001230
           02 MPAGEHP   PIC X.                                          00001240
           02 MPAGEHH   PIC X.                                          00001250
           02 MPAGEHV   PIC X.                                          00001260
           02 MPAGEHO   PIC 999.                                        00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNSOCFA   PIC X.                                          00001290
           02 MNSOCFC   PIC X.                                          00001300
           02 MNSOCFP   PIC X.                                          00001310
           02 MNSOCFH   PIC X.                                          00001320
           02 MNSOCFV   PIC X.                                          00001330
           02 MNSOCFO   PIC X(3).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNLIEUFA  PIC X.                                          00001360
           02 MNLIEUFC  PIC X.                                          00001370
           02 MNLIEUFP  PIC X.                                          00001380
           02 MNLIEUFH  PIC X.                                          00001390
           02 MNLIEUFV  PIC X.                                          00001400
           02 MNLIEUFO  PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLNOMFA   PIC X.                                          00001430
           02 MLNOMFC   PIC X.                                          00001440
           02 MLNOMFP   PIC X.                                          00001450
           02 MLNOMFH   PIC X.                                          00001460
           02 MLNOMFV   PIC X.                                          00001470
           02 MLNOMFO   PIC X(25).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDACTIVDA      PIC X.                                     00001500
           02 MDACTIVDC PIC X.                                          00001510
           02 MDACTIVDP PIC X.                                          00001520
           02 MDACTIVDH PIC X.                                          00001530
           02 MDACTIVDV PIC X.                                          00001540
           02 MDACTIVDO      PIC X(10).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDACTIVFA      PIC X.                                     00001570
           02 MDACTIVFC PIC X.                                          00001580
           02 MDACTIVFP PIC X.                                          00001590
           02 MDACTIVFH PIC X.                                          00001600
           02 MDACTIVFV PIC X.                                          00001610
           02 MDACTIVFO      PIC X(10).                                 00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNUMCARDFA     PIC X.                                     00001640
           02 MNUMCARDFC     PIC X.                                     00001650
           02 MNUMCARDFP     PIC X.                                     00001660
           02 MNUMCARDFH     PIC X.                                     00001670
           02 MNUMCARDFV     PIC X.                                     00001680
           02 MNUMCARDFO     PIC X(16).                                 00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNSOCRFA  PIC X.                                          00001710
           02 MNSOCRFC  PIC X.                                          00001720
           02 MNSOCRFP  PIC X.                                          00001730
           02 MNSOCRFH  PIC X.                                          00001740
           02 MNSOCRFV  PIC X.                                          00001750
           02 MNSOCRFO  PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNLIEURFA      PIC X.                                     00001780
           02 MNLIEURFC PIC X.                                          00001790
           02 MNLIEURFP PIC X.                                          00001800
           02 MNLIEURFH PIC X.                                          00001810
           02 MNLIEURFV PIC X.                                          00001820
           02 MNLIEURFO      PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNVENTERFA     PIC X.                                     00001850
           02 MNVENTERFC     PIC X.                                     00001860
           02 MNVENTERFP     PIC X.                                     00001870
           02 MNVENTERFH     PIC X.                                     00001880
           02 MNVENTERFV     PIC X.                                     00001890
           02 MNVENTERFO     PIC X(7).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MENTETEA  PIC X.                                          00001920
           02 MENTETEC  PIC X.                                          00001930
           02 MENTETEP  PIC X.                                          00001940
           02 MENTETEH  PIC X.                                          00001950
           02 MENTETEV  PIC X.                                          00001960
           02 MENTETEO  PIC X(77).                                      00001970
           02 MTABO OCCURS   15 TIMES .                                 00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MSELA   PIC X.                                          00002000
             03 MSELC   PIC X.                                          00002010
             03 MSELP   PIC X.                                          00002020
             03 MSELH   PIC X.                                          00002030
             03 MSELV   PIC X.                                          00002040
             03 MSELO   PIC X.                                          00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MLIGNEA      PIC X.                                     00002070
             03 MLIGNEC PIC X.                                          00002080
             03 MLIGNEP PIC X.                                          00002090
             03 MLIGNEH PIC X.                                          00002100
             03 MLIGNEV PIC X.                                          00002110
             03 MLIGNEO      PIC X(75).                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIBERRA  PIC X.                                          00002140
           02 MLIBERRC  PIC X.                                          00002150
           02 MLIBERRP  PIC X.                                          00002160
           02 MLIBERRH  PIC X.                                          00002170
           02 MLIBERRV  PIC X.                                          00002180
           02 MLIBERRO  PIC X(78).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCODTRAA  PIC X.                                          00002210
           02 MCODTRAC  PIC X.                                          00002220
           02 MCODTRAP  PIC X.                                          00002230
           02 MCODTRAH  PIC X.                                          00002240
           02 MCODTRAV  PIC X.                                          00002250
           02 MCODTRAO  PIC X(4).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MZONCMDA  PIC X.                                          00002280
           02 MZONCMDC  PIC X.                                          00002290
           02 MZONCMDP  PIC X.                                          00002300
           02 MZONCMDH  PIC X.                                          00002310
           02 MZONCMDV  PIC X.                                          00002320
           02 MZONCMDO  PIC X(15).                                      00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(5).                                       00002540
                                                                                
