      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBC1601                           *        
      ******************************************************************        
       01  RVBC1601.                                                            
      *    *************************************************************        
      *                       NORG                                              
           10 BC16-NORG            PIC X(6).                                    
      *    *************************************************************        
      *                       TORG                                              
           10 BC16-TORG            PIC X(5).                                    
      *    *************************************************************        
      *                       LORG                                              
           10 BC16-LORG            PIC X(25).                                   
      *    *************************************************************        
      *                       NADRESSE                                          
           10 BC16-NADRESSE        PIC X(8).                                    
      *    *************************************************************        
      *                       CTYPORG                                           
           10 BC16-CTYPORG         PIC X(1).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 BC16-DSYST           PIC S9(13)  USAGE COMP-3.                    
      *    *************************************************************        
      *                       NCLIENTADR                                        
           10 BC16-NCLIENTADR      PIC X(8).                                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBC1600-FLAGS.                                                      
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-NORG-F    PIC S9(4) USAGE COMP.                              
      *--                                                                       
           10 BC16-NORG-F    PIC S9(4) COMP-5.                                  
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-TORG-F     PIC S9(4) USAGE COMP.                             
      *--                                                                       
           10 BC16-TORG-F     PIC S9(4) COMP-5.                                 
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-LORG-F    PIC S9(4) USAGE COMP.                              
      *--                                                                       
           10 BC16-LORG-F    PIC S9(4) COMP-5.                                  
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-NADRESSE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC16-NADRESSE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-CTYPORG-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC16-CTYPORG-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC16-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *    *************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 BC16-NCLIENTADR-F    PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 BC16-NCLIENTADR-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
