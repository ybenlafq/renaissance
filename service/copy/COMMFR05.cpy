      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
      *    ZONES RESERVEES A AIDA -------------------------------- 100  00000060
           05  FILLER-COM-AIDA                 PIC X(100).              00000070
                                                                        00000080
      *    ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020  00000090
           05  COMM-CICS-APPLID                PIC X(008).              00000100
           05  COMM-CICS-NETNAM                PIC X(008).              00000110
           05  COMM-CICS-TRANSA                PIC X(004).              00000120
                                                                        00000130
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00000140
           05  COMM-DATE-SIECLE                PIC X(002).              00000150
           05  COMM-DATE-ANNEE                 PIC X(002).              00000160
           05  COMM-DATE-MOIS                  PIC X(002).              00000170
           05  COMM-DATE-JOUR                  PIC 9(002).              00000180
                                                                        00000130
      *    QUANTIEMES CALENDAIRE ET STANDARD                            00000190
           05  COMM-DATE-QNTA                  PIC 9(03).               00000200
           05  COMM-DATE-QNT0                  PIC 9(05).               00000210
                                                                        00000130
      *    ANNEE BISSEXTILE 1=OUI 0=NON                                 00000220
           05  COMM-DATE-BISX                  PIC 9(01).               00000230
                                                                        00000130
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           05  COMM-DATE-JSM                   PIC 9(01).               00000250
                                                                        00000130
      *    LIBELLES DU JOUR COURT - LONG                                00000260
           05  COMM-DATE-JSM-LC                PIC X(03).               00000270
           05  COMM-DATE-JSM-LL                PIC X(08).               00000280
                                                                        00000130
      *    LIBELLES DU MOIS COURT - LONG                                00000290
           05  COMM-DATE-MOIS-LC               PIC X(03).               00000300
           05  COMM-DATE-MOIS-LL               PIC X(08).               00000310
                                                                        00000130
      *    DIFFERENTES FORMES DE DATE                                   00000320
           05  COMM-DATE-SSAAMMJJ              PIC X(08).               00000330
           05  COMM-DATE-AAMMJJ                PIC X(06).               00000340
           05  COMM-DATE-JJMMSSAA              PIC X(08).               00000350
           05  COMM-DATE-JJMMAA                PIC X(06).               00000360
           05  COMM-DATE-JJ-MM-AA              PIC X(08).               00000370
           05  COMM-DATE-JJ-MM-SSAA            PIC X(10).               00000380
                                                                        00000130
      *    TRAITEMENT DU NUMERO DE SEMAINE                              00000390
           05  COMM-DATE-WEEK.                                          00000400
               10  COMM-DATE-SEMSS             PIC 9(02).               00000410
               10  COMM-DATE-SEMAA             PIC 9(02).               00000420
               10  COMM-DATE-SEMNU             PIC 9(02).               00000430
           05  COMM-DATE-FILLER                PIC X(08).               00000440
                                                                        00000130
      *    ZONES RESERVEES TRAITEMENT DU SWAP                           00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-SWAP-CURS                  PIC S9(4) COMP VALUE -1. 00000460
      *                                                                         
      *--                                                                       
           05  COMM-SWAP-CURS                  PIC S9(4) COMP-5 VALUE           
                                                                     -1.        
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
           05  COMM-FR05-ENTETE.                                        00000500
               10  COMM-ENTETE.                                         00000510
                   15  COMM-CODLANG            PIC X(02).               00000520
                   15  COMM-CODPIC             PIC X(02).               00000530
                   15  COMM-LEVEL-SUP          PIC X(05).               00000540
                   15  COMM-LEVEL-MAX          PIC X(05).               00000540
                   15  COMM-ACID               PIC X(08).                       
               10  COMM-MES-ERREUR.                                             
                   15  COMM-COD-ERREUR         PIC X(01).                       
                   15  COMM-LIB-MESSAG         PIC X(58).                       
               10  COMM-DAT-TIMJOU             PIC X(05).                       
               10  COMM-DAT-DEBUT              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-DEBUT.              
                   15  COMM-DAT-DEBJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-DEBSA          PIC X(04).                       
               10  COMM-DAT-FINAL              PIC X(10).                       
               10  FILLER                REDEFINES COMM-DAT-FINAL.              
                   15  COMM-DAT-FINJJ          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINMM          PIC X(02).                       
                   15  FILLER                  PIC X(01).                       
                   15  COMM-DAT-FINSA          PIC X(04).                       
                                                                        00000130
      *--- MENU APPLICATION CARTES CADEAUX B2B (ACC) - TFR05            00000390
           05  COMM-FR05-APPLI.                                         00000500
               10  COMM-FR05-PAGEMAX           PIC 9(03).                       
               10  COMM-FR05-PAGE              PIC 9(03).                       
               10  COMM-FR05-NBP               PIC 9(03).                       
           05  COMM-FR06-APPLI.                                         00000500
               10  COMM-FR06-CPROG             PIC X(05).                       
               10  COMM-FR06-SOCORI            PIC X(03).                       
               10  COMM-FR06-LIEUORI           PIC X(03).                       
               10  COMM-FR06-SOCDST            PIC X(03).                       
               10  COMM-FR06-LIEUDST           PIC X(03).                       
               10  COMM-FR06-SOCVTE            PIC X(03).                       
               10  COMM-FR06-LIEUVTE           PIC X(03).                       
               10 COMM-FR06-FONC           PIC X(1).                            
                  88 COMM-FR06-CRE         VALUE '0'.                           
                  88 COMM-FR06-MAJ         VALUE '1'.                           
      *--- FILLER                                                       00000390
           05  COMM-FR06-FILLER                PIC X(3529).             00000620
      *---                                                              00000390
                                                                                
