      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *    MISE A JOUR DES TABLES STOCKS (GS10, 30, 40, 43, 60, 70)   *         
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-HE90-LONG-COMMAREA PIC S9(4) COMP VALUE +200.                   
      *--                                                                       
       01  COMM-HE90-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
       01    Z-COMMAREA-HE90.                                                   
                                                                        00000830
      *      ZONES PASSEES A MHE90                                              
           02      COMM-HE90-CFONCTION     PIC X(01).                           
           02      COMM-HE90-QTE           PIC 9(05).                           
           02      COMM-HE90-CUTIL         PIC X(05).                           
           02      COMM-HE90-DOPER         PIC X(08).                           
           02      COMM-HE90-NORIGINE      PIC X(07).                           
           02      COMM-HE90-NHS           PIC X(07).                           
           02      COMM-HE90-NCODIC        PIC X(07).                           
           02      COMM-HE90-CMOTIF        PIC X(10).                           
           02      COMM-HE90-NLIEUENT      PIC X(03).                           
           02      COMM-HE90-ORIG.                                              
               03  COMM-HE90-NSOCORIG      PIC X(03).                           
               03  COMM-HE90-NLIEUORIG     PIC X(03).                           
               03  COMM-HE90-NSSLIEUORIG   PIC X(03).                           
               03  COMM-HE90-CLIEUTRTORIG  PIC X(05).                           
                                                                            0000
           02      COMM-HE90-DEST.                                              
               03  COMM-HE90-NSOCDEST      PIC X(03).                           
               03  COMM-HE90-NLIEUDEST     PIC X(03).                           
               03  COMM-HE90-NSSLIEUDEST   PIC X(03).                           
               03  COMM-HE90-CLIEUTRTDEST  PIC X(05).                           
      * ZONES RETOURNEES PAR MHE90                                              
      *                                                                         
      * REPONSE  OK = 0   SINON = 1 : ERREUR NON BLOQUANTE                      
      *                           2 : ERREUR     BLOQUANTE                      
           02      COMM-HE90-CRETOUR       PIC 9.                               
           02      COMM-HE90-MESS-ERR      PIC X(78).                           
           02      COMM-HE90-GS43-CACTION  PIC X.                               
               88  COMM-HE90-GS43-INACTION     VALUE SPACE.                     
               88  COMM-HE90-GS43-CREATION     VALUE 'C'.                       
                                                                                
