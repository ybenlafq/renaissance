      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF:redbox ctrl livraison                                       00000020
      ***************************************************************** 00000030
       01   ERB10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBLL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNBLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBLF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNBLI     PIC X(10).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLOTL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLOTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNLOTF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLOTI    PIC X(15).                                      00000210
      * nb couple ean+nserie                                            00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBCOUPLEL     COMP PIC S9(4).                            00000230
      *--                                                                       
           02 MNBCOUPLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBCOUPLEF     PIC X.                                     00000240
           02 FILLER    PIC X(4).                                       00000250
           02 MNBCOUPLEI     PIC X(5).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNEANL   COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLNEANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNEANF   PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLNEANI   PIC X(8).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNEANL    COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MNEANL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNEANF    PIC X.                                          00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MNEANI    PIC X(13).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNSERIEL      COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MLNSERIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLNSERIEF      PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLNSERIEI      PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSERIEL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MNSERIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSERIEF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MNSERIEI  PIC X(15).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLIBERRI  PIC X(19).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCICSI    PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNETNAMI  PIC X(8).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MSCREENI  PIC X(4).                                       00000580
      ***************************************************************** 00000590
      * SDF:redbox ctrl livraison                                       00000600
      ***************************************************************** 00000610
       01   ERB10O REDEFINES ERB10I.                                    00000620
           02 FILLER    PIC X(12).                                      00000630
           02 FILLER    PIC X(2).                                       00000640
           02 MDATJOUA  PIC X.                                          00000650
           02 MDATJOUC  PIC X.                                          00000660
           02 MDATJOUP  PIC X.                                          00000670
           02 MDATJOUH  PIC X.                                          00000680
           02 MDATJOUV  PIC X.                                          00000690
           02 MDATJOUO  PIC X(10).                                      00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MTIMJOUA  PIC X.                                          00000720
           02 MTIMJOUC  PIC X.                                          00000730
           02 MTIMJOUP  PIC X.                                          00000740
           02 MTIMJOUH  PIC X.                                          00000750
           02 MTIMJOUV  PIC X.                                          00000760
           02 MTIMJOUO  PIC X(5).                                       00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MNBLA     PIC X.                                          00000790
           02 MNBLC     PIC X.                                          00000800
           02 MNBLP     PIC X.                                          00000810
           02 MNBLH     PIC X.                                          00000820
           02 MNBLV     PIC X.                                          00000830
           02 MNBLO     PIC X(10).                                      00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MNLOTA    PIC X.                                          00000860
           02 MNLOTC    PIC X.                                          00000870
           02 MNLOTP    PIC X.                                          00000880
           02 MNLOTH    PIC X.                                          00000890
           02 MNLOTV    PIC X.                                          00000900
           02 MNLOTO    PIC X(15).                                      00000910
      * nb couple ean+nserie                                            00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MNBCOUPLEA     PIC X.                                     00000940
           02 MNBCOUPLEC     PIC X.                                     00000950
           02 MNBCOUPLEP     PIC X.                                     00000960
           02 MNBCOUPLEH     PIC X.                                     00000970
           02 MNBCOUPLEV     PIC X.                                     00000980
           02 MNBCOUPLEO     PIC X(5).                                  00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MLNEANA   PIC X.                                          00001010
           02 MLNEANC   PIC X.                                          00001020
           02 MLNEANP   PIC X.                                          00001030
           02 MLNEANH   PIC X.                                          00001040
           02 MLNEANV   PIC X.                                          00001050
           02 MLNEANO   PIC X(8).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNEANA    PIC X.                                          00001080
           02 MNEANC    PIC X.                                          00001090
           02 MNEANP    PIC X.                                          00001100
           02 MNEANH    PIC X.                                          00001110
           02 MNEANV    PIC X.                                          00001120
           02 MNEANO    PIC X(13).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MLNSERIEA      PIC X.                                     00001150
           02 MLNSERIEC PIC X.                                          00001160
           02 MLNSERIEP PIC X.                                          00001170
           02 MLNSERIEH PIC X.                                          00001180
           02 MLNSERIEV PIC X.                                          00001190
           02 MLNSERIEO      PIC X(7).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNSERIEA  PIC X.                                          00001220
           02 MNSERIEC  PIC X.                                          00001230
           02 MNSERIEP  PIC X.                                          00001240
           02 MNSERIEH  PIC X.                                          00001250
           02 MNSERIEV  PIC X.                                          00001260
           02 MNSERIEO  PIC X(15).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MLIBERRA  PIC X.                                          00001290
           02 MLIBERRC  PIC X.                                          00001300
           02 MLIBERRP  PIC X.                                          00001310
           02 MLIBERRH  PIC X.                                          00001320
           02 MLIBERRV  PIC X.                                          00001330
           02 MLIBERRO  PIC X(19).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCICSA    PIC X.                                          00001360
           02 MCICSC    PIC X.                                          00001370
           02 MCICSP    PIC X.                                          00001380
           02 MCICSH    PIC X.                                          00001390
           02 MCICSV    PIC X.                                          00001400
           02 MCICSO    PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNETNAMA  PIC X.                                          00001430
           02 MNETNAMC  PIC X.                                          00001440
           02 MNETNAMP  PIC X.                                          00001450
           02 MNETNAMH  PIC X.                                          00001460
           02 MNETNAMV  PIC X.                                          00001470
           02 MNETNAMO  PIC X(8).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MSCREENA  PIC X.                                          00001500
           02 MSCREENC  PIC X.                                          00001510
           02 MSCREENP  PIC X.                                          00001520
           02 MSCREENH  PIC X.                                          00001530
           02 MSCREENV  PIC X.                                          00001540
           02 MSCREENO  PIC X(4).                                       00001550
                                                                                
