      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010**************************************************************    00002000
000020* COMMAREA SPECIFIQUE PRG TFX00 (MENU)             TR: FX00  *    00002200
000090*                                                            *    00002900
000091*           POUR L'ADMINISTATION DES DONNEES                 *    00003000
000092**************************************************************    00003100
000093*        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00003200
000094**************************************************************    00003300
000095*                                                                 00003400
000096* XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00003500
000097*      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00003600
000098*      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00003700
000099*      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00003800
000100*                                                                 00003900
000110* COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00004000
000120* COMPRENANT :                                                    00004100
000130* 1 - LES ZONES RESERVEES A AIDA                                  00004200
000140* 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00004300
000150* 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00004400
000160* 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00004500
000170* 5 - LES ZONES RESERVEES APPLICATIVES                            00004600
000180*                                                                 00004700
000190* COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00004800
000191* PAR AIDA                                                        00004900
000192*                                                                 00005000
000193*-------------------------------------------------------------    00005100
000194*                                                                 00005200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000195*01  COM-FX00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00005300
      *--                                                                       
       01  COM-FX00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
000196*                                                                 00005400
000197 01  Z-COMMAREA.                                                  00005500
000198*                                                                 00005600
000199* ZONES RESERVEES A AIDA ----------------------------------- 100  00005700
000200    02 FILLER-COM-AIDA      PIC X(100).                           00005800
000210*                                                                 00005900
000220* ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00006000
000230    02 COMM-CICS-APPLID     PIC X(8).                             00006100
000240    02 COMM-CICS-NETNAM     PIC X(8).                             00006200
000250    02 COMM-CICS-TRANSA     PIC X(4).                             00006300
000260*                                                                 00006400
000270* ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00006500
000280    02 COMM-DATE-SIECLE     PIC XX.                               00006600
000290    02 COMM-DATE-ANNEE      PIC XX.                               00006700
000291    02 COMM-DATE-MOIS       PIC XX.                               00006800
000292    02 COMM-DATE-JOUR       PIC XX.                               00006900
000293*   QUANTIEMES CALENDAIRE ET STANDARD                             00007000
000294    02 COMM-DATE-QNTA       PIC 999.                              00007100
000295    02 COMM-DATE-QNT0       PIC 99999.                            00007200
000296*   ANNEE BISSEXTILE 1=OUI 0=NON                                  00007300
000297    02 COMM-DATE-BISX       PIC 9.                                00007400
000298*   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00007500
000299    02 COMM-DATE-JSM        PIC 9.                                00007600
000300*   LIBELLES DU JOUR COURT - LONG                                 00007700
000310    02 COMM-DATE-JSM-LC     PIC XXX.                              00007800
000320    02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00007900
000330*   LIBELLES DU MOIS COURT - LONG                                 00008000
000340    02 COMM-DATE-MOIS-LC    PIC XXX.                              00008100
000350    02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00008200
000360*   DIFFERENTES FORMES DE DATE                                    00008300
000370    02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00008400
000380    02 COMM-DATE-AAMMJJ     PIC X(6).                             00008500
000390    02 COMM-DATE-JJMMSSAA   PIC X(8).                             00008600
000391    02 COMM-DATE-JJMMAA     PIC X(6).                             00008700
000392    02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00008800
000393    02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00008900
000394*   DIFFERENTES FORMES DE DATE                                    00009000
000395    02 COMM-DATE-SEMSS      PIC X(02).                            00009100
000395    02 COMM-DATE-SEMAA      PIC X(02).                            00009100
000395    02 COMM-DATE-SEMNU      PIC X(02).                            00009100
000395    02 COMM-DATE-FILLER     PIC X(08).                            00009100
000396*                                                                 00009200
000397* ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00009300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000398*   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00009400
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
000399    02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00009500
000400*                                                                 00009600
000410* ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00010000
000420*                                                                 00410100
000430*            TRANSACTION FX00 : ADMINISTRATION DES DONNEES      * 00411000
000440*                                                                 00412000
000442    02 COMM-FX00-NOECS           PIC X(5).                                
000442    02 COMM-FX00-CRIT1           PIC X(5).                                
000442    02 COMM-FX00-CRIT2           PIC X(5).                                
000442    02 COMM-FX00-CRIT3           PIC X(5).                                
000442    02 COMM-FX00-DATEF           PIC X(8).                                
000442    02 COMM-FX00-MESS            PIC X(80).                               
000442    02 COMM-FX00-FONCTION        PIC X(3).                                
000442    02 COMM-FX00-COMPTEGL        PIC X(6).                                
000443*                                                                         
000450    02 COMM-FX00-APPLI.                                           00420000
000460*------------------------------ ZONE COMMUNE                      00510000
000470       03 COMM-FX00-FILLER         PIC X(3601).                   00520000
             03 COMM-FX06 REDEFINES COMM-FX00-FILLER.                           
                04 COMM-FX06-ELEMENT   PIC X(8).                                
                04 COMM-FX06-AFF       PIC X(3).                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FX06-PAGE      PIC S9(4) COMP.                          
      *--                                                                       
                04 COMM-FX06-PAGE      PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-FX06-NBP       PIC S9(4) COMP.                          
      *--                                                                       
                04 COMM-FX06-NBP       PIC S9(4) COMP-5.                        
      *}                                                                        
000480***************************************************************** 00740000
                                                                                
