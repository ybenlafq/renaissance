      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSB0300                      *        
      ******************************************************************        
       01  RVSB0300.                                                            
      *                       REFCMDEXT                                         
           10 SB03-REFCMDEXT       PIC X(30).                                   
      *                       TYPE                                              
           10 SB03-TYPE            PIC X(10).                                   
      *                       NLIGNE                                            
           10 SB03-NLIGNE          PIC S9(2)V USAGE COMP-3.                     
      *                       TLIGNE                                            
           10 SB03-TLIGNE          PIC X(1).                                    
      *                       NSOCIETE                                          
           10 SB03-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 SB03-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 SB03-NVENTE          PIC X(7).                                    
      *                       ARTICLE_EXT                                       
           10 SB03-ARTICLE-EXT     PIC X(16).                                   
      *                       REFERENCE_EXT                                     
           10 SB03-REFERENCE-EXT   PIC X(30).                                   
      *                       CFAM_EXT                                          
           10 SB03-CFAM-EXT        PIC X(3).                                    
      *                       LFAM_EXT                                          
           10 SB03-LFAM-EXT        PIC X(30).                                   
      *                       LMARQ_EXT                                         
           10 SB03-LMARQ-EXT       PIC X(15).                                   
      *                       CMODDEL_EXT                                       
           10 SB03-CMODDEL-EXT     PIC X(5).                                    
      *                       PVENTE_EXT                                        
           10 SB03-PVENTE-EXT      PIC S9(8)V9(2) USAGE COMP-3.                 
      *                       DSYST                                             
           10 SB03-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 15      *        
      ******************************************************************        
                                                                                
