      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0700                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQC0700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQC0700.                                                            
      *}                                                                        
           02  QC07-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC07-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC07-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  QC07-NLIEU                                                       
               PIC X(0003).                                                     
           02  QC07-DEDIT                                                       
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           02  QC07-CPTENVOIS                                                   
               PIC 9(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0700                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVQC0700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVQC0700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-DEDIT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-DEDIT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC07-CPTENVOIS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC07-CPTENVOIS-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
