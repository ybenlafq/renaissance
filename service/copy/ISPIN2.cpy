      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISPIN2 AU 09/03/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISPIN2.                                                        
            05 NOMETAT-ISPIN2           PIC X(6) VALUE 'ISPIN2'.                
            05 RUPTURES-ISPIN2.                                                 
           10 ISPIN2-NSOCIETE           PIC X(03).                      007  003
           10 ISPIN2-TEBRB              PIC X(05).                      010  005
           10 ISPIN2-CHEFPROD           PIC X(05).                      015  005
           10 ISPIN2-WSEQFAM            PIC S9(05)      COMP-3.         020  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISPIN2-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 ISPIN2-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISPIN2.                                                   
           10 ISPIN2-CAPPRO             PIC X(03).                      025  003
           10 ISPIN2-CASSORT            PIC X(02).                      028  002
           10 ISPIN2-CFAM               PIC X(05).                      030  005
           10 ISPIN2-CMARQ              PIC X(05).                      035  005
           10 ISPIN2-DCREATION          PIC X(08).                      040  008
           10 ISPIN2-DEFFETPV           PIC X(08).                      048  008
           10 ISPIN2-DERO               PIC X(01).                      056  001
           10 ISPIN2-DJOUR              PIC X(08).                      057  008
           10 ISPIN2-LREFFOURN          PIC X(20).                      065  020
           10 ISPIN2-NCODIC             PIC X(07).                      085  007
           10 ISPIN2-NZONPRIX           PIC X(02).                      092  002
           10 ISPIN2-PSTDTTC            PIC S9(07)V9(2) COMP-3.         094  005
           10 ISPIN2-SRP                PIC S9(07)V9(2) COMP-3.         099  005
            05 FILLER                      PIC X(409).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISPIN2-LONG           PIC S9(4)   COMP  VALUE +103.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISPIN2-LONG           PIC S9(4) COMP-5  VALUE +103.           
                                                                                
      *}                                                                        
