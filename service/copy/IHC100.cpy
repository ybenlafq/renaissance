      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHC100 AU 20/02/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,PD,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,07,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHC100.                                                        
            05 NOMETAT-IHC100           PIC X(6) VALUE 'IHC100'.                
            05 RUPTURES-IHC100.                                                 
           10 IHC100-WSEQED             PIC S9(05)      COMP-3.         007  003
           10 IHC100-WSEQFAM            PIC S9(05)      COMP-3.         010  003
           10 IHC100-CMARQ              PIC X(05).                      013  005
           10 IHC100-NCODIC             PIC X(07).                      018  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHC100-SEQUENCE           PIC S9(04) COMP.                025  002
      *--                                                                       
           10 IHC100-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHC100.                                                   
           10 IHC100-CFAM               PIC X(05).                      027  005
           10 IHC100-CLIEUTRT           PIC X(05).                      032  005
           10 IHC100-CRAYON             PIC X(05).                      037  005
           10 IHC100-LFAM               PIC X(20).                      042  020
           10 IHC100-LRAYON             PIC X(20).                      062  020
           10 IHC100-LSTA1              PIC X(09).                      082  009
           10 IHC100-LSTA2              PIC X(09).                      091  009
           10 IHC100-LSTA3              PIC X(09).                      100  009
           10 IHC100-LSTA4              PIC X(09).                      109  009
           10 IHC100-LSTA5              PIC X(09).                      118  009
           10 IHC100-LSTA6              PIC X(09).                      127  009
           10 IHC100-LSTA7              PIC X(09).                      136  009
           10 IHC100-LSTA8              PIC X(09).                      145  009
           10 IHC100-NLIEU              PIC X(03).                      154  003
           10 IHC100-NSOCIETE           PIC X(03).                      157  003
           10 IHC100-NSSLIEU            PIC X(03).                      160  003
           10 IHC100-COEFF1             PIC S9(03)V9(2) COMP-3.         163  003
           10 IHC100-COEFF2             PIC S9(03)V9(2) COMP-3.         166  003
           10 IHC100-COEFF3             PIC S9(03)V9(2) COMP-3.         169  003
           10 IHC100-COEFF4             PIC S9(03)V9(2) COMP-3.         172  003
           10 IHC100-COEFF5             PIC S9(03)V9(2) COMP-3.         175  003
           10 IHC100-COEFF6             PIC S9(03)V9(2) COMP-3.         178  003
           10 IHC100-COEFF7             PIC S9(03)V9(2) COMP-3.         181  003
           10 IHC100-COEFF8             PIC S9(03)V9(2) COMP-3.         184  003
           10 IHC100-PSTA               PIC S9(09)      COMP-3.         187  005
           10 IHC100-PSTA1              PIC S9(07)      COMP-3.         192  004
           10 IHC100-PSTA2              PIC S9(07)      COMP-3.         196  004
           10 IHC100-PSTA3              PIC S9(07)      COMP-3.         200  004
           10 IHC100-PSTA4              PIC S9(07)      COMP-3.         204  004
           10 IHC100-PSTA5              PIC S9(07)      COMP-3.         208  004
           10 IHC100-PSTA6              PIC S9(07)      COMP-3.         212  004
           10 IHC100-PSTA7              PIC S9(07)      COMP-3.         216  004
           10 IHC100-PSTA8              PIC S9(07)      COMP-3.         220  004
           10 IHC100-QTE1               PIC S9(03)      COMP-3.         224  002
           10 IHC100-QTE2               PIC S9(03)      COMP-3.         226  002
           10 IHC100-QTE3               PIC S9(03)      COMP-3.         228  002
           10 IHC100-QTE4               PIC S9(03)      COMP-3.         230  002
           10 IHC100-QTE5               PIC S9(03)      COMP-3.         232  002
           10 IHC100-QTE6               PIC S9(03)      COMP-3.         234  002
           10 IHC100-QTE7               PIC S9(03)      COMP-3.         236  002
           10 IHC100-QTE8               PIC S9(03)      COMP-3.         238  002
           10 IHC100-DINVENT            PIC X(08).                      240  008
            05 FILLER                      PIC X(265).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IHC100-LONG           PIC S9(4)   COMP  VALUE +247.           
      *                                                                         
      *--                                                                       
        01  DSECT-IHC100-LONG           PIC S9(4) COMP-5  VALUE +247.           
                                                                                
      *}                                                                        
