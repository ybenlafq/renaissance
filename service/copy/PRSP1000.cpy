      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE SP1000                       
      ******************************************************************        
      *                                                                         
       CLEF-SP1000             SECTION.                                         
      *                                                                         
           MOVE 'RVSP1000          '       TO   TABLE-NAME.                     
           MOVE 'SP1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-SP1000. EXIT.                                                   
                EJECT                                                           
                                                                                
