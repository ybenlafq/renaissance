      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ R�PONSE SOUSCRIPTION/R�SILIATION REDBOX               
      *   DU S.I. REDBOX. (RBxRSouRes)                                          
      *   copy du message mesrb002 avec ajout de zones                          
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESRB002-DATA.                                                        
      *                                                                         
      * R�PONSE                                                                 
      *                                                                         
      *   N� DE TRANSACTION REDBOX                                              
          05 MESRB02-NTRRBX        PIC    X(10).                                
      *   CODE SOCI�T� VENTE "GV"                                               
          05 MESRB02-NSOC          PIC    X(03).                                
      *   CODE LIEU VENTE "GV"                                                  
          05 MESRB02-NLIEU         PIC    X(03).                                
      *   N� VENTE "GV"                                                         
          05 MESRB02-NVENTE        PIC    X(07).                                
      *   CR permet de se diff�rencier du message d'encaissement                
          05 MESRB02-CR            PIC    X(01).                                
      *   Statut du CR 'OK' ou 'KO'                                             
          05 MESRB02-Statut        PIC    X(02).                                
      *                                                                         
                                                                                
