      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0500.                                                            
           02  QC05-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC05-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC05-NCARTE                                                      
               PIC X(0007).                                                     
           02  QC05-NDOSSIER                                                    
               PIC X(0010).                                                     
           02  QC05-CTITRE                                                      
               PIC X(0005).                                                     
           02  QC05-LNOM                                                        
               PIC X(0025).                                                     
           02  QC05-LPRENOM                                                     
               PIC X(0015).                                                     
           02  QC05-LADR1                                                       
               PIC X(0032).                                                     
           02  QC05-LADR2                                                       
               PIC X(0032).                                                     
           02  QC05-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  QC05-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  QC05-TELDOM                                                      
               PIC X(0010).                                                     
           02  QC05-LREF                                                        
               PIC X(0010).                                                     
           02  QC05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-NCARTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-NCARTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-NDOSSIER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-CTITRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-CTITRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LADR1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LADR1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LADR2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LADR2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-LREF-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-LREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
