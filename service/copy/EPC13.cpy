      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: consult mvts sur les cartes                                00000020
      ***************************************************************** 00000030
       01   EPC13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRANDOMNUML    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MRANDOMNUML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MRANDOMNUMF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MRANDOMNUMI    PIC X(16).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRTYPL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MPRTYPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRTYPF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MPRTYPI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSERIALNUML    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MSERIALNUML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSERIALNUMF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSERIALNUMI    PIC X(13).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSTATUTI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTRAGEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLETTRAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLETTRAGEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLETTRAGEI     PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMENT1L     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCOMMENT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMMENT1F     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCOMMENT1I     PIC X(52).                                 00000450
           02 MTABLGRPI OCCURS   10 TIMES .                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSELI   PIC X.                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMVTL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLMVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMVTF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLMVTI  PIC X(18).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METATL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 METATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 METATF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 METATI  PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATEI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNSOCI  PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNLIEUI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNVENTEI     PIC X(8).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTDARTYL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNTDARTYL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNTDARTYF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNTDARTYI    PIC X(9).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOLDEDL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MSOLDEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOLDEDF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSOLDEDI     PIC X(9).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOLDEPL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MSOLDEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOLDEPF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSOLDEPI     PIC X(9).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: consult mvts sur les cartes                                00001080
      ***************************************************************** 00001090
       01   EPC13O REDEFINES EPC13I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 NPAGEA    PIC X.                                          00001270
           02 NPAGEC    PIC X.                                          00001280
           02 NPAGEP    PIC X.                                          00001290
           02 NPAGEH    PIC X.                                          00001300
           02 NPAGEV    PIC X.                                          00001310
           02 NPAGEO    PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 NBPAGEA   PIC X.                                          00001340
           02 NBPAGEC   PIC X.                                          00001350
           02 NBPAGEP   PIC X.                                          00001360
           02 NBPAGEH   PIC X.                                          00001370
           02 NBPAGEV   PIC X.                                          00001380
           02 NBPAGEO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MRANDOMNUMA    PIC X.                                     00001410
           02 MRANDOMNUMC    PIC X.                                     00001420
           02 MRANDOMNUMP    PIC X.                                     00001430
           02 MRANDOMNUMH    PIC X.                                     00001440
           02 MRANDOMNUMV    PIC X.                                     00001450
           02 MRANDOMNUMO    PIC X(16).                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MPRTYPA   PIC X.                                          00001480
           02 MPRTYPC   PIC X.                                          00001490
           02 MPRTYPP   PIC X.                                          00001500
           02 MPRTYPH   PIC X.                                          00001510
           02 MPRTYPV   PIC X.                                          00001520
           02 MPRTYPO   PIC X(20).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MSERIALNUMA    PIC X.                                     00001550
           02 MSERIALNUMC    PIC X.                                     00001560
           02 MSERIALNUMP    PIC X.                                     00001570
           02 MSERIALNUMH    PIC X.                                     00001580
           02 MSERIALNUMV    PIC X.                                     00001590
           02 MSERIALNUMO    PIC X(13).                                 00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSTATUTA  PIC X.                                          00001620
           02 MSTATUTC  PIC X.                                          00001630
           02 MSTATUTP  PIC X.                                          00001640
           02 MSTATUTH  PIC X.                                          00001650
           02 MSTATUTV  PIC X.                                          00001660
           02 MSTATUTO  PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLETTRAGEA     PIC X.                                     00001690
           02 MLETTRAGEC     PIC X.                                     00001700
           02 MLETTRAGEP     PIC X.                                     00001710
           02 MLETTRAGEH     PIC X.                                     00001720
           02 MLETTRAGEV     PIC X.                                     00001730
           02 MLETTRAGEO     PIC X(10).                                 00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCOMMENT1A     PIC X.                                     00001760
           02 MCOMMENT1C     PIC X.                                     00001770
           02 MCOMMENT1P     PIC X.                                     00001780
           02 MCOMMENT1H     PIC X.                                     00001790
           02 MCOMMENT1V     PIC X.                                     00001800
           02 MCOMMENT1O     PIC X(52).                                 00001810
           02 MTABLGRPO OCCURS   10 TIMES .                             00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MSELA   PIC X.                                          00001840
             03 MSELC   PIC X.                                          00001850
             03 MSELP   PIC X.                                          00001860
             03 MSELH   PIC X.                                          00001870
             03 MSELV   PIC X.                                          00001880
             03 MSELO   PIC X.                                          00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MLMVTA  PIC X.                                          00001910
             03 MLMVTC  PIC X.                                          00001920
             03 MLMVTP  PIC X.                                          00001930
             03 MLMVTH  PIC X.                                          00001940
             03 MLMVTV  PIC X.                                          00001950
             03 MLMVTO  PIC X(18).                                      00001960
             03 FILLER       PIC X(2).                                  00001970
             03 METATA  PIC X.                                          00001980
             03 METATC  PIC X.                                          00001990
             03 METATP  PIC X.                                          00002000
             03 METATH  PIC X.                                          00002010
             03 METATV  PIC X.                                          00002020
             03 METATO  PIC X(2).                                       00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MDATEA  PIC X.                                          00002050
             03 MDATEC  PIC X.                                          00002060
             03 MDATEP  PIC X.                                          00002070
             03 MDATEH  PIC X.                                          00002080
             03 MDATEV  PIC X.                                          00002090
             03 MDATEO  PIC X(8).                                       00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MNSOCA  PIC X.                                          00002120
             03 MNSOCC  PIC X.                                          00002130
             03 MNSOCP  PIC X.                                          00002140
             03 MNSOCH  PIC X.                                          00002150
             03 MNSOCV  PIC X.                                          00002160
             03 MNSOCO  PIC X(3).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MNLIEUA      PIC X.                                     00002190
             03 MNLIEUC PIC X.                                          00002200
             03 MNLIEUP PIC X.                                          00002210
             03 MNLIEUH PIC X.                                          00002220
             03 MNLIEUV PIC X.                                          00002230
             03 MNLIEUO      PIC X(3).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MNVENTEA     PIC X.                                     00002260
             03 MNVENTEC     PIC X.                                     00002270
             03 MNVENTEP     PIC X.                                     00002280
             03 MNVENTEH     PIC X.                                     00002290
             03 MNVENTEV     PIC X.                                     00002300
             03 MNVENTEO     PIC X(8).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MNTDARTYA    PIC X.                                     00002330
             03 MNTDARTYC    PIC X.                                     00002340
             03 MNTDARTYP    PIC X.                                     00002350
             03 MNTDARTYH    PIC X.                                     00002360
             03 MNTDARTYV    PIC X.                                     00002370
             03 MNTDARTYO    PIC X(9).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MSOLDEDA     PIC X.                                     00002400
             03 MSOLDEDC     PIC X.                                     00002410
             03 MSOLDEDP     PIC X.                                     00002420
             03 MSOLDEDH     PIC X.                                     00002430
             03 MSOLDEDV     PIC X.                                     00002440
             03 MSOLDEDO     PIC X(9).                                  00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MSOLDEPA     PIC X.                                     00002470
             03 MSOLDEPC     PIC X.                                     00002480
             03 MSOLDEPP     PIC X.                                     00002490
             03 MSOLDEPH     PIC X.                                     00002500
             03 MSOLDEPV     PIC X.                                     00002510
             03 MSOLDEPO     PIC X(9).                                  00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
