      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CC - PARAMETRAGE ICS                                            00000020
      ***************************************************************** 00000030
       01   EPC42I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREGLEL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MNREGLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNREGLEF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNREGLEI  PIC X(10).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLIBELLEI      PIC X(25).                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGNEL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNLIGNEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIGNEF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNLIGNEI  PIC X(2).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAPPLIL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MAPPLIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MAPPLIF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MAPPLII   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPFACTL      COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MTYPFACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPFACTF      PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MTYPFACTI      PIC X(2).                                  00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNATUREL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MNATUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNATUREF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MNATUREI  PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOL    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MSOCOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCOF    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MSOCOI    PIC X(3).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOL   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIEUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUOF   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIEUOI   PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MSOCDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCDF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MSOCDI    PIC X(3).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIEUDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUDF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIEUDI   PIC X(3).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENTL   COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCVENTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENTF   PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCVENTI   PIC X(5).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSEL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MTIERSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSEF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MTIERSEI  PIC X(8).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTREL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLETTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTREF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLETTREI  PIC X(10).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIERSRL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MTIERSRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIERSRF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MTIERSRI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTRRL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLETTRRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTRRF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLETTRRI  PIC X(10).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPC02L    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MPC02L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPC02F    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MPC02I    PIC X.                                          00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERL     COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MPERL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPERF     PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MPERI     PIC X.                                          00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MJOURI    PIC X(2).                                       00000870
      * ZONE CMD AIDA                                                   00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MLIBERRI  PIC X(79).                                      00000920
      * CODE TRANSACTION                                                00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCODTRAI  PIC X(4).                                       00000970
      * CICS DE TRAVAIL                                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      * NETNAME                                                         00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MNETNAMI  PIC X(8).                                       00001070
      * CODE TERMINAL                                                   00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MSCREENI  PIC X(4).                                       00001120
      ***************************************************************** 00001130
      * CC - PARAMETRAGE ICS                                            00001140
      ***************************************************************** 00001150
       01   EPC42O REDEFINES EPC42I.                                    00001160
           02 FILLER    PIC X(12).                                      00001170
      * DATE DU JOUR                                                    00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MDATJOUA  PIC X.                                          00001200
           02 MDATJOUC  PIC X.                                          00001210
           02 MDATJOUP  PIC X.                                          00001220
           02 MDATJOUH  PIC X.                                          00001230
           02 MDATJOUV  PIC X.                                          00001240
           02 MDATJOUO  PIC X(10).                                      00001250
      * HEURE                                                           00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNREGLEA  PIC X.                                          00001350
           02 MNREGLEC  PIC X.                                          00001360
           02 MNREGLEP  PIC X.                                          00001370
           02 MNREGLEH  PIC X.                                          00001380
           02 MNREGLEV  PIC X.                                          00001390
           02 MNREGLEO  PIC X(10).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLIBELLEA      PIC X.                                     00001420
           02 MLIBELLEC PIC X.                                          00001430
           02 MLIBELLEP PIC X.                                          00001440
           02 MLIBELLEH PIC X.                                          00001450
           02 MLIBELLEV PIC X.                                          00001460
           02 MLIBELLEO      PIC X(25).                                 00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNLIGNEA  PIC X.                                          00001490
           02 MNLIGNEC  PIC X.                                          00001500
           02 MNLIGNEP  PIC X.                                          00001510
           02 MNLIGNEH  PIC X.                                          00001520
           02 MNLIGNEV  PIC X.                                          00001530
           02 MNLIGNEO  PIC X(2).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MAPPLIA   PIC X.                                          00001560
           02 MAPPLIC   PIC X.                                          00001570
           02 MAPPLIP   PIC X.                                          00001580
           02 MAPPLIH   PIC X.                                          00001590
           02 MAPPLIV   PIC X.                                          00001600
           02 MAPPLIO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MTYPFACTA      PIC X.                                     00001630
           02 MTYPFACTC PIC X.                                          00001640
           02 MTYPFACTP PIC X.                                          00001650
           02 MTYPFACTH PIC X.                                          00001660
           02 MTYPFACTV PIC X.                                          00001670
           02 MTYPFACTO      PIC X(2).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNATUREA  PIC X.                                          00001700
           02 MNATUREC  PIC X.                                          00001710
           02 MNATUREP  PIC X.                                          00001720
           02 MNATUREH  PIC X.                                          00001730
           02 MNATUREV  PIC X.                                          00001740
           02 MNATUREO  PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSOCOA    PIC X.                                          00001770
           02 MSOCOC    PIC X.                                          00001780
           02 MSOCOP    PIC X.                                          00001790
           02 MSOCOH    PIC X.                                          00001800
           02 MSOCOV    PIC X.                                          00001810
           02 MSOCOO    PIC X(3).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLIEUOA   PIC X.                                          00001840
           02 MLIEUOC   PIC X.                                          00001850
           02 MLIEUOP   PIC X.                                          00001860
           02 MLIEUOH   PIC X.                                          00001870
           02 MLIEUOV   PIC X.                                          00001880
           02 MLIEUOO   PIC X(3).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MSOCDA    PIC X.                                          00001910
           02 MSOCDC    PIC X.                                          00001920
           02 MSOCDP    PIC X.                                          00001930
           02 MSOCDH    PIC X.                                          00001940
           02 MSOCDV    PIC X.                                          00001950
           02 MSOCDO    PIC X(3).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLIEUDA   PIC X.                                          00001980
           02 MLIEUDC   PIC X.                                          00001990
           02 MLIEUDP   PIC X.                                          00002000
           02 MLIEUDH   PIC X.                                          00002010
           02 MLIEUDV   PIC X.                                          00002020
           02 MLIEUDO   PIC X(3).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCVENTA   PIC X.                                          00002050
           02 MCVENTC   PIC X.                                          00002060
           02 MCVENTP   PIC X.                                          00002070
           02 MCVENTH   PIC X.                                          00002080
           02 MCVENTV   PIC X.                                          00002090
           02 MCVENTO   PIC X(5).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MTIERSEA  PIC X.                                          00002120
           02 MTIERSEC  PIC X.                                          00002130
           02 MTIERSEP  PIC X.                                          00002140
           02 MTIERSEH  PIC X.                                          00002150
           02 MTIERSEV  PIC X.                                          00002160
           02 MTIERSEO  PIC X(8).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLETTREA  PIC X.                                          00002190
           02 MLETTREC  PIC X.                                          00002200
           02 MLETTREP  PIC X.                                          00002210
           02 MLETTREH  PIC X.                                          00002220
           02 MLETTREV  PIC X.                                          00002230
           02 MLETTREO  PIC X(10).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MTIERSRA  PIC X.                                          00002260
           02 MTIERSRC  PIC X.                                          00002270
           02 MTIERSRP  PIC X.                                          00002280
           02 MTIERSRH  PIC X.                                          00002290
           02 MTIERSRV  PIC X.                                          00002300
           02 MTIERSRO  PIC X(8).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLETTRRA  PIC X.                                          00002330
           02 MLETTRRC  PIC X.                                          00002340
           02 MLETTRRP  PIC X.                                          00002350
           02 MLETTRRH  PIC X.                                          00002360
           02 MLETTRRV  PIC X.                                          00002370
           02 MLETTRRO  PIC X(10).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MPC02A    PIC X.                                          00002400
           02 MPC02C    PIC X.                                          00002410
           02 MPC02P    PIC X.                                          00002420
           02 MPC02H    PIC X.                                          00002430
           02 MPC02V    PIC X.                                          00002440
           02 MPC02O    PIC X.                                          00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MPERA     PIC X.                                          00002470
           02 MPERC     PIC X.                                          00002480
           02 MPERP     PIC X.                                          00002490
           02 MPERH     PIC X.                                          00002500
           02 MPERV     PIC X.                                          00002510
           02 MPERO     PIC X.                                          00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MJOURA    PIC X.                                          00002540
           02 MJOURC    PIC X.                                          00002550
           02 MJOURP    PIC X.                                          00002560
           02 MJOURH    PIC X.                                          00002570
           02 MJOURV    PIC X.                                          00002580
           02 MJOURO    PIC X(2).                                       00002590
      * ZONE CMD AIDA                                                   00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLIBERRA  PIC X.                                          00002620
           02 MLIBERRC  PIC X.                                          00002630
           02 MLIBERRP  PIC X.                                          00002640
           02 MLIBERRH  PIC X.                                          00002650
           02 MLIBERRV  PIC X.                                          00002660
           02 MLIBERRO  PIC X(79).                                      00002670
      * CODE TRANSACTION                                                00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCODTRAA  PIC X.                                          00002700
           02 MCODTRAC  PIC X.                                          00002710
           02 MCODTRAP  PIC X.                                          00002720
           02 MCODTRAH  PIC X.                                          00002730
           02 MCODTRAV  PIC X.                                          00002740
           02 MCODTRAO  PIC X(4).                                       00002750
      * CICS DE TRAVAIL                                                 00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCICSA    PIC X.                                          00002780
           02 MCICSC    PIC X.                                          00002790
           02 MCICSP    PIC X.                                          00002800
           02 MCICSH    PIC X.                                          00002810
           02 MCICSV    PIC X.                                          00002820
           02 MCICSO    PIC X(5).                                       00002830
      * NETNAME                                                         00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
      * CODE TERMINAL                                                   00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MSCREENA  PIC X.                                          00002940
           02 MSCREENC  PIC X.                                          00002950
           02 MSCREENP  PIC X.                                          00002960
           02 MSCREENH  PIC X.                                          00002970
           02 MSCREENV  PIC X.                                          00002980
           02 MSCREENO  PIC X(4).                                       00002990
                                                                                
