      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Rbx: maj prix facturation                                       00000020
      ***************************************************************** 00000030
       01   ERB31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 NPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NBPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 NBPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NBPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 NBPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICFL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNCODICFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICFF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICFI      PIC X(7).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATDEBI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATFINI  PIC X(10).                                      00000330
           02 MTABLGRPI OCCURS   10 TIMES .                             00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNCDEI  PIC X(7).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNRECF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNRECI  PIC X(7).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECADML    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDRECADML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDRECADMF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDRECADMI    PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFAMI  PIC X(5).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCMARQI      PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLREFI  PIC X(18).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCODICI     PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQTEI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVL    COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MPVL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MPVF    PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPVI    PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEMAJL      COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MNCDEMAJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCDEMAJF      PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNCDEMAJI      PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECMAJL      COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MNRECMAJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRECMAJF      PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNRECMAJI      PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICMAJL    COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MNCODICMAJL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCODICMAJF    PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNCODICMAJI    PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPVMAJL   COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MPVMAJL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPVMAJF   PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MPVMAJI   PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(79).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * Rbx: maj prix facturation                                       00001080
      ***************************************************************** 00001090
       01   ERB31O REDEFINES ERB31I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 NPAGEA    PIC X.                                          00001270
           02 NPAGEC    PIC X.                                          00001280
           02 NPAGEP    PIC X.                                          00001290
           02 NPAGEH    PIC X.                                          00001300
           02 NPAGEV    PIC X.                                          00001310
           02 NPAGEO    PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 NBPAGEA   PIC X.                                          00001340
           02 NBPAGEC   PIC X.                                          00001350
           02 NBPAGEP   PIC X.                                          00001360
           02 NBPAGEH   PIC X.                                          00001370
           02 NBPAGEV   PIC X.                                          00001380
           02 NBPAGEO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNCODICFA      PIC X.                                     00001410
           02 MNCODICFC PIC X.                                          00001420
           02 MNCODICFP PIC X.                                          00001430
           02 MNCODICFH PIC X.                                          00001440
           02 MNCODICFV PIC X.                                          00001450
           02 MNCODICFO      PIC X(7).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MDATDEBA  PIC X.                                          00001480
           02 MDATDEBC  PIC X.                                          00001490
           02 MDATDEBP  PIC X.                                          00001500
           02 MDATDEBH  PIC X.                                          00001510
           02 MDATDEBV  PIC X.                                          00001520
           02 MDATDEBO  PIC X(10).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MDATFINA  PIC X.                                          00001550
           02 MDATFINC  PIC X.                                          00001560
           02 MDATFINP  PIC X.                                          00001570
           02 MDATFINH  PIC X.                                          00001580
           02 MDATFINV  PIC X.                                          00001590
           02 MDATFINO  PIC X(10).                                      00001600
           02 MTABLGRPO OCCURS   10 TIMES .                             00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNCDEA  PIC X.                                          00001630
             03 MNCDEC  PIC X.                                          00001640
             03 MNCDEP  PIC X.                                          00001650
             03 MNCDEH  PIC X.                                          00001660
             03 MNCDEV  PIC X.                                          00001670
             03 MNCDEO  PIC X(7).                                       00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MNRECA  PIC X.                                          00001700
             03 MNRECC  PIC X.                                          00001710
             03 MNRECP  PIC X.                                          00001720
             03 MNRECH  PIC X.                                          00001730
             03 MNRECV  PIC X.                                          00001740
             03 MNRECO  PIC X(7).                                       00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MDRECADMA    PIC X.                                     00001770
             03 MDRECADMC    PIC X.                                     00001780
             03 MDRECADMP    PIC X.                                     00001790
             03 MDRECADMH    PIC X.                                     00001800
             03 MDRECADMV    PIC X.                                     00001810
             03 MDRECADMO    PIC X(8).                                  00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MCFAMA  PIC X.                                          00001840
             03 MCFAMC  PIC X.                                          00001850
             03 MCFAMP  PIC X.                                          00001860
             03 MCFAMH  PIC X.                                          00001870
             03 MCFAMV  PIC X.                                          00001880
             03 MCFAMO  PIC X(5).                                       00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MCMARQA      PIC X.                                     00001910
             03 MCMARQC PIC X.                                          00001920
             03 MCMARQP PIC X.                                          00001930
             03 MCMARQH PIC X.                                          00001940
             03 MCMARQV PIC X.                                          00001950
             03 MCMARQO      PIC X(5).                                  00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MLREFA  PIC X.                                          00001980
             03 MLREFC  PIC X.                                          00001990
             03 MLREFP  PIC X.                                          00002000
             03 MLREFH  PIC X.                                          00002010
             03 MLREFV  PIC X.                                          00002020
             03 MLREFO  PIC X(18).                                      00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MNCODICA     PIC X.                                     00002050
             03 MNCODICC     PIC X.                                     00002060
             03 MNCODICP     PIC X.                                     00002070
             03 MNCODICH     PIC X.                                     00002080
             03 MNCODICV     PIC X.                                     00002090
             03 MNCODICO     PIC X(7).                                  00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MQTEA   PIC X.                                          00002120
             03 MQTEC   PIC X.                                          00002130
             03 MQTEP   PIC X.                                          00002140
             03 MQTEH   PIC X.                                          00002150
             03 MQTEV   PIC X.                                          00002160
             03 MQTEO   PIC X(5).                                       00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MPVA    PIC X.                                          00002190
             03 MPVC    PIC X.                                          00002200
             03 MPVP    PIC X.                                          00002210
             03 MPVH    PIC X.                                          00002220
             03 MPVV    PIC X.                                          00002230
             03 MPVO    PIC X(8).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MNCDEMAJA      PIC X.                                     00002260
           02 MNCDEMAJC PIC X.                                          00002270
           02 MNCDEMAJP PIC X.                                          00002280
           02 MNCDEMAJH PIC X.                                          00002290
           02 MNCDEMAJV PIC X.                                          00002300
           02 MNCDEMAJO      PIC X(7).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MNRECMAJA      PIC X.                                     00002330
           02 MNRECMAJC PIC X.                                          00002340
           02 MNRECMAJP PIC X.                                          00002350
           02 MNRECMAJH PIC X.                                          00002360
           02 MNRECMAJV PIC X.                                          00002370
           02 MNRECMAJO      PIC X(7).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNCODICMAJA    PIC X.                                     00002400
           02 MNCODICMAJC    PIC X.                                     00002410
           02 MNCODICMAJP    PIC X.                                     00002420
           02 MNCODICMAJH    PIC X.                                     00002430
           02 MNCODICMAJV    PIC X.                                     00002440
           02 MNCODICMAJO    PIC X(7).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MPVMAJA   PIC X.                                          00002470
           02 MPVMAJC   PIC X.                                          00002480
           02 MPVMAJP   PIC X.                                          00002490
           02 MPVMAJH   PIC X.                                          00002500
           02 MPVMAJV   PIC X.                                          00002510
           02 MPVMAJO   PIC X(8).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(79).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
