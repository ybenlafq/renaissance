      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVQC0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVQC0600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0600.                                                            
           02  QC06-NCISOC                                                      
               PIC X(0003).                                                     
           02  QC06-CTCARTE                                                     
               PIC X(0001).                                                     
           02  QC06-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  QC06-NLIEU                                                       
               PIC X(0003).                                                     
           02  QC06-NQUOTAR                                                     
               PIC S9(5) COMP-3.                                                
           02  QC06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVQC0600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVQC0600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-NCISOC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-NCISOC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-CTCARTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-CTCARTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-NQUOTAR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-NQUOTAR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  QC06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  QC06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
