      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * DA2I - LISTE ANOMALIES EN COURS                                 00000020
      ***************************************************************** 00000030
       01   EAP01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAVL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCSAVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCSAVF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCSAVI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAVL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNLIEUSAVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUSAVF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUSAVI     PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOSSIERL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDOSSIERF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNDOSSIERI     PIC X(9).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVTEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNSOCVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCVTEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOCVTEI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVTEL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNLIEUVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUVTEF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNLIEUVTEI     PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVTESELL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNVTESELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNVTESELF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNVTESELI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCLTSELL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNCLTSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCLTSELF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCLTSELI      PIC X(8).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATESELL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MDATESELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATESELF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATESELI      PIC X(10).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATSELL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCSTATSELL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCSTATSELF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCSTATSELI     PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCANOSELL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCANOSELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCANOSELF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCANOSELI      PIC X(2).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRISELL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTRISELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTRISELF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTRISELI  PIC X.                                          00000690
           02 MLIGNEI OCCURS   10 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MACTI   PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCRIL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNCRIL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCRIF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCRII  PIC X(17).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVTEL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNVTEF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNVTEI  PIC X(14).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCLIENTL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNCLIENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCLIENTF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNCLIENTI    PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEANOL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDATEANOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEANOF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDATEANOI    PIC X(10).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTATUTL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MCSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSTATUTF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MCSTATUTI    PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEANOL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCODEANOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODEANOF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCODEANOI    PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCOMMENTI    PIC X(15).                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(79).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      * NETNAME                                                         00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MNETNAMI  PIC X(8).                                       00001230
      * CODE TERMINAL                                                   00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MSCREENI  PIC X(4).                                       00001280
      ***************************************************************** 00001290
      * DA2I - LISTE ANOMALIES EN COURS                                 00001300
      ***************************************************************** 00001310
       01   EAP01O REDEFINES EAP01I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDATJOUA  PIC X.                                          00001350
           02 MDATJOUC  PIC X.                                          00001360
           02 MDATJOUP  PIC X.                                          00001370
           02 MDATJOUH  PIC X.                                          00001380
           02 MDATJOUV  PIC X.                                          00001390
           02 MDATJOUO  PIC X(10).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUP  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUV  PIC X.                                          00001460
           02 MTIMJOUO  PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MFONCA    PIC X.                                          00001490
           02 MFONCC    PIC X.                                          00001500
           02 MFONCP    PIC X.                                          00001510
           02 MFONCH    PIC X.                                          00001520
           02 MFONCV    PIC X.                                          00001530
           02 MFONCO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MPAGEA    PIC X.                                          00001560
           02 MPAGEC    PIC X.                                          00001570
           02 MPAGEP    PIC X.                                          00001580
           02 MPAGEH    PIC X.                                          00001590
           02 MPAGEV    PIC X.                                          00001600
           02 MPAGEO    PIC 99.                                         00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNBPA     PIC X.                                          00001630
           02 MNBPC     PIC X.                                          00001640
           02 MNBPP     PIC X.                                          00001650
           02 MNBPH     PIC X.                                          00001660
           02 MNBPV     PIC X.                                          00001670
           02 MNBPO     PIC 99.                                         00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSOCSAVA      PIC X.                                     00001700
           02 MNSOCSAVC PIC X.                                          00001710
           02 MNSOCSAVP PIC X.                                          00001720
           02 MNSOCSAVH PIC X.                                          00001730
           02 MNSOCSAVV PIC X.                                          00001740
           02 MNSOCSAVO      PIC X(3).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNLIEUSAVA     PIC X.                                     00001770
           02 MNLIEUSAVC     PIC X.                                     00001780
           02 MNLIEUSAVP     PIC X.                                     00001790
           02 MNLIEUSAVH     PIC X.                                     00001800
           02 MNLIEUSAVV     PIC X.                                     00001810
           02 MNLIEUSAVO     PIC X(3).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNDOSSIERA     PIC X.                                     00001840
           02 MNDOSSIERC     PIC X.                                     00001850
           02 MNDOSSIERP     PIC X.                                     00001860
           02 MNDOSSIERH     PIC X.                                     00001870
           02 MNDOSSIERV     PIC X.                                     00001880
           02 MNDOSSIERO     PIC X(9).                                  00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNSOCVTEA      PIC X.                                     00001910
           02 MNSOCVTEC PIC X.                                          00001920
           02 MNSOCVTEP PIC X.                                          00001930
           02 MNSOCVTEH PIC X.                                          00001940
           02 MNSOCVTEV PIC X.                                          00001950
           02 MNSOCVTEO      PIC X(3).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNLIEUVTEA     PIC X.                                     00001980
           02 MNLIEUVTEC     PIC X.                                     00001990
           02 MNLIEUVTEP     PIC X.                                     00002000
           02 MNLIEUVTEH     PIC X.                                     00002010
           02 MNLIEUVTEV     PIC X.                                     00002020
           02 MNLIEUVTEO     PIC X(3).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNVTESELA      PIC X.                                     00002050
           02 MNVTESELC PIC X.                                          00002060
           02 MNVTESELP PIC X.                                          00002070
           02 MNVTESELH PIC X.                                          00002080
           02 MNVTESELV PIC X.                                          00002090
           02 MNVTESELO      PIC X(7).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNCLTSELA      PIC X.                                     00002120
           02 MNCLTSELC PIC X.                                          00002130
           02 MNCLTSELP PIC X.                                          00002140
           02 MNCLTSELH PIC X.                                          00002150
           02 MNCLTSELV PIC X.                                          00002160
           02 MNCLTSELO      PIC X(8).                                  00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MDATESELA      PIC X.                                     00002190
           02 MDATESELC PIC X.                                          00002200
           02 MDATESELP PIC X.                                          00002210
           02 MDATESELH PIC X.                                          00002220
           02 MDATESELV PIC X.                                          00002230
           02 MDATESELO      PIC X(10).                                 00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCSTATSELA     PIC X.                                     00002260
           02 MCSTATSELC     PIC X.                                     00002270
           02 MCSTATSELP     PIC X.                                     00002280
           02 MCSTATSELH     PIC X.                                     00002290
           02 MCSTATSELV     PIC X.                                     00002300
           02 MCSTATSELO     PIC X(3).                                  00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCANOSELA      PIC X.                                     00002330
           02 MCANOSELC PIC X.                                          00002340
           02 MCANOSELP PIC X.                                          00002350
           02 MCANOSELH PIC X.                                          00002360
           02 MCANOSELV PIC X.                                          00002370
           02 MCANOSELO      PIC X(2).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MTRISELA  PIC X.                                          00002400
           02 MTRISELC  PIC X.                                          00002410
           02 MTRISELP  PIC X.                                          00002420
           02 MTRISELH  PIC X.                                          00002430
           02 MTRISELV  PIC X.                                          00002440
           02 MTRISELO  PIC X.                                          00002450
           02 MLIGNEO OCCURS   10 TIMES .                               00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MACTA   PIC X.                                          00002480
             03 MACTC   PIC X.                                          00002490
             03 MACTP   PIC X.                                          00002500
             03 MACTH   PIC X.                                          00002510
             03 MACTV   PIC X.                                          00002520
             03 MACTO   PIC X.                                          00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNCRIA  PIC X.                                          00002550
             03 MNCRIC  PIC X.                                          00002560
             03 MNCRIP  PIC X.                                          00002570
             03 MNCRIH  PIC X.                                          00002580
             03 MNCRIV  PIC X.                                          00002590
             03 MNCRIO  PIC X(17).                                      00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MNVTEA  PIC X.                                          00002620
             03 MNVTEC  PIC X.                                          00002630
             03 MNVTEP  PIC X.                                          00002640
             03 MNVTEH  PIC X.                                          00002650
             03 MNVTEV  PIC X.                                          00002660
             03 MNVTEO  PIC X(14).                                      00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MNCLIENTA    PIC X.                                     00002690
             03 MNCLIENTC    PIC X.                                     00002700
             03 MNCLIENTP    PIC X.                                     00002710
             03 MNCLIENTH    PIC X.                                     00002720
             03 MNCLIENTV    PIC X.                                     00002730
             03 MNCLIENTO    PIC X(8).                                  00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MDATEANOA    PIC X.                                     00002760
             03 MDATEANOC    PIC X.                                     00002770
             03 MDATEANOP    PIC X.                                     00002780
             03 MDATEANOH    PIC X.                                     00002790
             03 MDATEANOV    PIC X.                                     00002800
             03 MDATEANOO    PIC X(10).                                 00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MCSTATUTA    PIC X.                                     00002830
             03 MCSTATUTC    PIC X.                                     00002840
             03 MCSTATUTP    PIC X.                                     00002850
             03 MCSTATUTH    PIC X.                                     00002860
             03 MCSTATUTV    PIC X.                                     00002870
             03 MCSTATUTO    PIC X(3).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MCODEANOA    PIC X.                                     00002900
             03 MCODEANOC    PIC X.                                     00002910
             03 MCODEANOP    PIC X.                                     00002920
             03 MCODEANOH    PIC X.                                     00002930
             03 MCODEANOV    PIC X.                                     00002940
             03 MCODEANOO    PIC X(2).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MCOMMENTA    PIC X.                                     00002970
             03 MCOMMENTC    PIC X.                                     00002980
             03 MCOMMENTP    PIC X.                                     00002990
             03 MCOMMENTH    PIC X.                                     00003000
             03 MCOMMENTV    PIC X.                                     00003010
             03 MCOMMENTO    PIC X(15).                                 00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MLIBERRA  PIC X.                                          00003040
           02 MLIBERRC  PIC X.                                          00003050
           02 MLIBERRP  PIC X.                                          00003060
           02 MLIBERRH  PIC X.                                          00003070
           02 MLIBERRV  PIC X.                                          00003080
           02 MLIBERRO  PIC X(79).                                      00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MCODTRAA  PIC X.                                          00003110
           02 MCODTRAC  PIC X.                                          00003120
           02 MCODTRAP  PIC X.                                          00003130
           02 MCODTRAH  PIC X.                                          00003140
           02 MCODTRAV  PIC X.                                          00003150
           02 MCODTRAO  PIC X(4).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MZONCMDA  PIC X.                                          00003180
           02 MZONCMDC  PIC X.                                          00003190
           02 MZONCMDP  PIC X.                                          00003200
           02 MZONCMDH  PIC X.                                          00003210
           02 MZONCMDV  PIC X.                                          00003220
           02 MZONCMDO  PIC X(15).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MCICSA    PIC X.                                          00003250
           02 MCICSC    PIC X.                                          00003260
           02 MCICSP    PIC X.                                          00003270
           02 MCICSH    PIC X.                                          00003280
           02 MCICSV    PIC X.                                          00003290
           02 MCICSO    PIC X(5).                                       00003300
      * NETNAME                                                         00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MNETNAMA  PIC X.                                          00003330
           02 MNETNAMC  PIC X.                                          00003340
           02 MNETNAMP  PIC X.                                          00003350
           02 MNETNAMH  PIC X.                                          00003360
           02 MNETNAMV  PIC X.                                          00003370
           02 MNETNAMO  PIC X(8).                                       00003380
      * CODE TERMINAL                                                   00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MSCREENA  PIC X.                                          00003410
           02 MSCREENC  PIC X.                                          00003420
           02 MSCREENP  PIC X.                                          00003430
           02 MSCREENH  PIC X.                                          00003440
           02 MSCREENV  PIC X.                                          00003450
           02 MSCREENO  PIC X(4).                                       00003460
                                                                                
