      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FRVET PARAMETRAGE CODE VETUSTE         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFRVET .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFRVET .                                                            
      *}                                                                        
           05  FRVET-CTABLEG2    PIC X(15).                                     
           05  FRVET-CTABLEG2-REDEF REDEFINES FRVET-CTABLEG2.                   
               10  FRVET-IDVETUST        PIC X(10).                             
               10  FRVET-CFAM            PIC X(05).                             
           05  FRVET-WTABLEG     PIC X(80).                                     
           05  FRVET-WTABLEG-REDEF  REDEFINES FRVET-WTABLEG.                    
               10  FRVET-PARAM           PIC X(10).                             
               10  FRVET-COMMENT         PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFRVET-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFRVET-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FRVET-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FRVET-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FRVET-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FRVET-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
