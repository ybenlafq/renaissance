      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVFX1000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFX1000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVFX1000.                                                    00000090
           02  FX10-COMPTEGL                                            00000100
               PIC X(0006).                                             00000110
           02  FX10-COMPTECOSYS1                                        00000120
               PIC X(0006).                                             00000130
           02  FX10-COMPTECOSYS2                                        00000140
               PIC X(0006).                                             00000150
           02  FX10-DSYST                                               00000160
               PIC S9(13) COMP-3.                                       00000170
      *                                                                 00000180
      *---------------------------------------------------------        00000190
      *   LISTE DES FLAGS DE LA TABLE RVFX1000                          00000200
      *---------------------------------------------------------        00000210
      *                                                                 00000220
       01  RVFX1000-FLAGS.                                              00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX10-COMPTEGL-F                                          00000240
      *        PIC S9(4) COMP.                                          00000250
      *--                                                                       
           02  FX10-COMPTEGL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX10-COMPTECOSYS1-F                                      00000260
      *        PIC S9(4) COMP.                                          00000270
      *--                                                                       
           02  FX10-COMPTECOSYS1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX10-COMPTECOSYS2-F                                      00000280
      *        PIC S9(4) COMP.                                          00000290
      *--                                                                       
           02  FX10-COMPTECOSYS2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FX10-DSYST-F                                             00000300
      *        PIC S9(4) COMP.                                          00000310
      *--                                                                       
           02  FX10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000320
                                                                                
