      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:55 >
      
      *
       01  Z-COMMAREA.
      *    ZONES RESERVEES A AIDA -------------------------------- 100
           05  FILLER-COM-AIDA                 PIC X(100).
      
      *    ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020
           05  COMM-CICS-APPLID                PIC X(008).
           05  COMM-CICS-NETNAM                PIC X(008).
           05  COMM-CICS-TRANSA                PIC X(004).
      
      *    ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100
           05  COMM-DATE-SIECLE                PIC X(002).
           05  COMM-DATE-ANNEE                 PIC X(002).
           05  COMM-DATE-MOIS                  PIC X(002).
           05  COMM-DATE-JOUR                  PIC 9(002).
      
      *    QUANTIEMES CALENDAIRE ET STANDARD
           05  COMM-DATE-QNTA                  PIC 9(03).
           05  COMM-DATE-QNT0                  PIC 9(05).
      
      *    ANNEE BISSEXTILE 1=OUI 0=NON
           05  COMM-DATE-BISX                  PIC 9(01).
      
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE
           05  COMM-DATE-JSM                   PIC 9(01).
      
      *    LIBELLES DU JOUR COURT - LONG
           05  COMM-DATE-JSM-LC                PIC X(03).
           05  COMM-DATE-JSM-LL                PIC X(08).
      
      *    LIBELLES DU MOIS COURT - LONG
           05  COMM-DATE-MOIS-LC               PIC X(03).
           05  COMM-DATE-MOIS-LL               PIC X(08).
      
      *    DIFFERENTES FORMES DE DATE
           05  COMM-DATE-SSAAMMJJ              PIC X(08).
           05  COMM-DATE-AAMMJJ                PIC X(06).
           05  COMM-DATE-JJMMSSAA              PIC X(08).
           05  COMM-DATE-JJMMAA                PIC X(06).
           05  COMM-DATE-JJ-MM-AA              PIC X(08).
           05  COMM-DATE-JJ-MM-SSAA            PIC X(10).
      
      *    TRAITEMENT DU NUMERO DE SEMAINE
           05  COMM-DATE-WEEK.
               10  COMM-DATE-SEMSS             PIC 9(02).
               10  COMM-DATE-SEMAA             PIC 9(02).
               10  COMM-DATE-SEMNU             PIC 9(02).
           05  COMM-DATE-FILLER                PIC X(08).
      
      *    ZONES RESERVEES TRAITEMENT DU SWAP
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  COMM-SWAP-CURS                  PIC S9(4) COMP VALUE -1.
      *
      *--
           05  COMM-SWAP-CURS              PIC S9(4) COMP-5 VALUE -1.
      
      *}
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874
           05  COMM-PC20-ENTETE.
               10  COMM-ENTETE.
                   15  COMM-CODLANG            PIC X(02).
                   15  COMM-CODPIC             PIC X(02).
                   15  COMM-LEVEL-SUP          PIC X(05).
                   15  COMM-LEVEL-MAX          PIC X(05).
                   15  COMM-ACID               PIC X(08).
               10  COMM-MES-ERREUR.
                   15  COMM-COD-ERREUR         PIC X(01).
                   15  COMM-LIB-MESSAG         PIC X(58).
               10  COMM-DAT-TIMJOU             PIC X(05).
               10  COMM-DAT-DEBUT              PIC X(10).
               10  FILLER                REDEFINES COMM-DAT-DEBUT.
                   15  COMM-DAT-DEBJJ          PIC X(02).
                   15  FILLER                  PIC X(01).
                   15  COMM-DAT-DEBMM          PIC X(02).
                   15  FILLER                  PIC X(01).
                   15  COMM-DAT-DEBSA          PIC X(04).
               10  COMM-DAT-FINAL              PIC X(10).
               10  FILLER                REDEFINES COMM-DAT-FINAL.
                   15  COMM-DAT-FINJJ          PIC X(02).
                   15  FILLER                  PIC X(01).
                   15  COMM-DAT-FINMM          PIC X(02).
                   15  FILLER                  PIC X(01).
                   15  COMM-DAT-FINSA          PIC X(04).
      
      *--- MENU APPLICATION CARTES CADEAUX B2B (ACC) - TPC40
           05  COMM-PC40-APPLI.
               10  COMM-PC40-PAGEMAX           PIC 9(03).
               10  COMM-PC40-PAGE              PIC 9(03).
               10  COMM-PC40-NBP               PIC 9(03).
           05  COMM-PC41-APPLI.
               10  COMM-PC41-NREGLE            PIC X(10).
               10  COMM-PC41-NLIGNE            PIC X(02).
               10 COMM-PC41-FONC           PIC X(1).
                  88 COMM-PC41-CRE         VALUE '0'.
                  88 COMM-PC41-MAJ         VALUE '1'.
           05  COMM-PC42-APPLI.
               10  COMM-PC42-NREGLE            PIC X(10).
               10  COMM-PC42-NLIGNE            PIC X(02).
               10 COMM-PC42-FONC           PIC X(1).
                  88 COMM-PC42-CRE         VALUE '0'.
                  88 COMM-PC42-MAJ         VALUE '1'.
      *--- FILLER
           05  COMM-PC40-FILLER                PIC X(3516).
      *---
      
