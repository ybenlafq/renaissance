      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IQC002 AU 12/11/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IQC002.                                                        
            05 NOMETAT-IQC002           PIC X(6) VALUE 'IQC002'.                
            05 RUPTURES-IQC002.                                                 
           10 IQC002-NCISOC             PIC X(03).                      007  003
           10 IQC002-CTCARTE            PIC X(01).                      010  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IQC002-SEQUENCE           PIC S9(04) COMP.                011  002
      *--                                                                       
           10 IQC002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IQC002.                                                   
           10 IQC002-JOURJ              PIC X(05).                      013  005
           10 IQC002-JOURJ-1            PIC X(05).                      018  005
           10 IQC002-JOURJ-2            PIC X(05).                      023  005
           10 IQC002-JOURJ-3            PIC X(05).                      028  005
           10 IQC002-JOURJ-4            PIC X(05).                      033  005
           10 IQC002-JOURJ-5            PIC X(05).                      038  005
           10 IQC002-JOURJ-6            PIC X(05).                      043  005
           10 IQC002-NLIEU              PIC X(03).                      048  003
           10 IQC002-NSOCIETE           PIC X(03).                      051  003
           10 IQC002-CPTJ               PIC S9(05)      COMP-3.         054  003
           10 IQC002-CPTJ-1             PIC S9(05)      COMP-3.         057  003
           10 IQC002-CPTJ-2             PIC S9(05)      COMP-3.         060  003
           10 IQC002-CPTJ-3             PIC S9(05)      COMP-3.         063  003
           10 IQC002-CPTJ-4             PIC S9(05)      COMP-3.         066  003
           10 IQC002-CPTJ-5             PIC S9(05)      COMP-3.         069  003
           10 IQC002-CPTJ-6             PIC S9(05)      COMP-3.         072  003
           10 IQC002-SOMME-MOISJ        PIC S9(06)      COMP-3.         075  004
           10 IQC002-SOMME-MOISJ-1      PIC S9(06)      COMP-3.         079  004
            05 FILLER                      PIC X(430).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IQC002-LONG           PIC S9(4)   COMP  VALUE +082.           
      *                                                                         
      *--                                                                       
        01  DSECT-IQC002-LONG           PIC S9(4) COMP-5  VALUE +082.           
                                                                                
      *}                                                                        
