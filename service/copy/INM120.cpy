      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM120 AU 01/07/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,07,BI,A,                          *        
      *                           23,08,BI,A,                          *        
      *                           31,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM120.                                                        
            05 NOMETAT-INM120           PIC X(6) VALUE 'INM120'.                
            05 RUPTURES-INM120.                                                 
           10 INM120-SOCIETE            PIC X(03).                      007  003
           10 INM120-MAG                PIC X(03).                      010  003
           10 INM120-NCAISS             PIC X(03).                      013  003
           10 INM120-CODOPE             PIC X(07).                      016  007
           10 INM120-NTRANS             PIC X(08).                      023  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM120-SEQUENCE           PIC S9(04) COMP.                031  002
      *--                                                                       
           10 INM120-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM120.                                                   
           10 INM120-CMOPAC             PIC X(05).                      033  005
           10 INM120-CMOPAI             PIC X(05).                      038  005
           10 INM120-DEV-REF            PIC X(01).                      043  001
           10 INM120-DEV-TRANS          PIC X(01).                      044  001
           10 INM120-LMAG               PIC X(20).                      045  020
           10 INM120-MONT-DEVRF         PIC S9(08)V9(2) COMP-3.         065  006
           10 INM120-MONT-ENCAISS       PIC S9(08)V9(2) COMP-3.         071  006
           10 INM120-DATETR             PIC X(08).                      077  008
            05 FILLER                      PIC X(428).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM120-LONG           PIC S9(4)   COMP  VALUE +084.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM120-LONG           PIC S9(4) COMP-5  VALUE +084.           
                                                                                
      *}                                                                        
