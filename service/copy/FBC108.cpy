      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC108  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST DEJA CHARGEE      *         
      *                                                               *         
      *    FBC108       : FICHIER DES REGLEMENTS                      *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   86 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC108-ENREG.                                                        
           03  FBC108-CLE.                                                      
               05  FBC108-NSOCIETE            PIC  X(03).                       
               05  FBC108-NLIEU               PIC  X(03).                       
               05  FBC108-NVENTE              PIC  X(08).                       
           03  FBC108-DSAISIE                 PIC  X(08).                       
           03  FBC108-NSEQ                    PIC  X(02).                       
           03  FBC108-CMODPAIMT               PIC  X(05).                       
           03  FBC108-PREGLTVTE               PIC  S9(07)V9(02).                
           03  FBC108-NREGLTVTE               PIC  X(07).                       
           03  FBC108-DREGLTVTE               PIC  X(08).                       
           03  FBC108-WREGLTVTE               PIC  X(01).                       
           03  FBC108-DCOMPTA                 PIC  X(08).                       
           03  FBC108-CDEVISE                 PIC  X(03).                       
           03  FBC108-DVERSION                PIC  X(08).                       
           03  FBC108-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
