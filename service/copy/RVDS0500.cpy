      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVDS0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVDS0500                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVDS0500.                                                            
           02  DS05-NCODIC                                                      
               PIC X(0007).                                                     
           02  DS05-DSEMESTRE                                                   
               PIC X(0006).                                                     
           02  DS05-QTAUX1                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  DS05-QTAUX2                                                      
               PIC S9(3)V9(0002) COMP-3.                                        
           02  DS05-WTPBT                                                       
               PIC X(0001).                                                     
           02  DS05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVDS0500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVDS0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-DSEMESTRE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-DSEMESTRE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-QTAUX1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-QTAUX1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-QTAUX2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-QTAUX2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-WTPBT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-WTPBT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DS05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  DS05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
