      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    COPIE DE LA TS UTILIISEE DANS LE PROGRAMME TRB71 : TSRB71   *        
      *----------------------------------------------------------------*        
       01  TS-RB71-IDENT.                                                       
           05  TS-RB71-TERMID              PIC X(04).                           
           05  TS-RB71-TRANID              PIC X(04).                           
       01  FILLER                    REDEFINES TS-RB71-IDENT.                   
           05  TS-RB71-NAME                PIC X(08).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RANG-TS-RB71                    PIC S9(4) COMP.                      
      *--                                                                       
       01  RANG-TS-RB71                    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-RB71-LONG                    PIC S9(4) COMP VALUE +816.           
      *--                                                                       
       01  TS-RB71-LONG                    PIC S9(4) COMP-5 VALUE +816.         
      *}                                                                        
       01  TS-RB71-DATA.                                                        
           05  TS-RB71-LIGNE               OCCURS 16.                           
               10  TS-RB71-NENVOI          PIC X(07).                           
               10  TS-RB71-CTRAIT          PIC X(05).                           
               10  TS-RB71-DATENV.                                              
                   15  TS-RB71-ENVJOU      PIC X(02).                           
                   15  TS-RB71-FILLE1      PIC X(01).                           
                   15  TS-RB71-ENVMOI      PIC X(02).                           
                   15  TS-RB71-FILLE2      PIC X(01).                           
                   15  TS-RB71-ENVSAN      PIC X(04).                           
               10  TS-RB71-DATREC.                                              
                   15  TS-RB71-RECJOU      PIC X(02).                           
                   15  TS-RB71-FILLE3      PIC X(01).                           
                   15  TS-RB71-RECMOI      PIC X(02).                           
                   15  TS-RB71-FILLE4      PIC X(01).                           
                   15  TS-RB71-RECSAN      PIC X(04).                           
               10  TS-RB71-NBENV           PIC S9(5) COMP-3.                    
               10  TS-RB71-NBRECU          PIC S9(5) COMP-3.                    
               10  TS-RB71-NSOC            PIC X(03).                           
               10  TS-RB71-CTIERS          PIC X(05).                           
               10  TS-RB71-STATUT          PIC X(03).                           
               10  TS-RB71-LDET            PIC X(01).                           
               10  TS-RB71-MODIF           PIC X(01).                           
      *                                                                         
      *--- FIN DE LA TS TSRB71 ----------------------------------------*        
                                                                                
