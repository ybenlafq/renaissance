      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMHE20.                                              00000101
      *                                                                 00000200
      **************************************************************    00000300
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000400
      **************************************************************    00000500
      *                                                                 00000600
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000700
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000800
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000900
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00001000
      *                                                                 00001100
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00001200
      * COMPRENANT :                                                    00001300
      * 1 - LES ZONES RESERVEES A AIDA                                  00001400
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00001500
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00001600
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00001700
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00001800
      *                                                                 00001900
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00002000
      * PAR AIDA                                                        00002100
      *                                                                 00002200
      *-------------------------------------------------------------    00002300
                                                                        00002400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-HE20-LONG-COMMAREA PIC S9(5) COMP VALUE +4096.           00002501
      *                                                                         
      *--                                                                       
       01  COM-HE20-LONG-COMMAREA PIC S9(5) COMP-5 VALUE +4096.                 
                                                                        00002600
      *}                                                                        
       01  Z-COMMAREA.                                                  00002700
                                                                        00002800
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00002900
           02 FILLER-COM-AIDA      PIC X(100).                          00003000
                                                                        00003100
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00003200
           02 COMM-CICS-APPLID     PIC X(8).                            00003300
           02 COMM-CICS-NETNAM     PIC X(8).                            00003400
           02 COMM-CICS-TRANSA     PIC X(4).                            00003500
                                                                        00003600
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00003700
           02 COMM-DATE-SIECLE     PIC XX.                              00003800
           02 COMM-DATE-ANNEE      PIC XX.                              00003900
           02 COMM-DATE-MOIS       PIC XX.                              00004000
           02 COMM-DATE-JOUR       PIC 99.                              00004100
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00004200
           02 COMM-DATE-QNTA       PIC 999.                             00004300
           02 COMM-DATE-QNT0       PIC 99999.                           00004400
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00004500
           02 COMM-DATE-BISX       PIC 9.                               00004600
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00004700
           02 COMM-DATE-JSM        PIC 9.                               00004800
      *   LIBELLES DU JOUR COURT - LONG                                 00004900
           02 COMM-DATE-JSM-LC     PIC XXX.                             00005000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00005100
      *   LIBELLES DU MOIS COURT - LONG                                 00005200
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00005300
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00005400
      *   DIFFERENTES FORMES DE DATE                                    00005500
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00005600
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00005700
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00005800
           02 COMM-DATE-JJMMAA     PIC X(6).                            00005900
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00006000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00006100
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00006200
           02 COMM-DATE-WEEK.                                           00006300
              05 COMM-DATE-SEMSS   PIC 99.                              00006400
              05 COMM-DATE-SEMAA   PIC 99.                              00006500
              05 COMM-DATE-SEMNU   PIC 99.                              00006600
                                                                        00006700
           02 COMM-DATE-FILLER     PIC X(08).                           00006800
                                                                        00006900
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------152   00007000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00007100
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR       OCCURS 150 PIC X(01).                00007200
                                                                        00007300
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00007400
           2    FILLER   PIC X(22) VALUE ' *** COMMAREA HE20 ***'.      00007501
           2    COMM-HE20-APPLI.                                        00007601
                                                                        00007700
      *--------INCHANGEEES                                              00007800
             3      COMM-HE10-CACID           PIC X(8).                 00008001
             3      COMM-HE20-CACID           REDEFINES                 00008001
                    COMM-HE10-CACID           PIC X(8).                 00008001
             3      COMM-HE10-NSOCIETE        PIC X(3).                 00008001
             3      COMM-HE20-NSOCIETE        REDEFINES                 00008001
                    COMM-HE10-NSOCIETE        PIC X(3).                 00008001
             3      COMM-HE10-CLIEUHET        PIC X(5).                         
             3      COMM-HE20-CLIEUHET        REDEFINES                         
                    COMM-HE10-CLIEUHET        PIC X(5).                         
             3      COMM-HE10-LLIEUHET        PIC X(20).                        
             3      COMM-HE20-LLIEUHET        REDEFINES                         
                    COMM-HE10-LLIEUHET        PIC X(20).                        
             3      COMM-HE20-CTIERS          PIC X(5).                 00008001
             3      COMM-HE20-CTRI            PIC X(3).                 00008001
             3      COMM-HE20-DENVOI          PIC X(10).                00008001
             3      COMM-HE20-NENVOI          PIC X(7).                 00008001
             3      COMM-HE10-NLIEUHED        PIC X(3).                 00008001
             3      COMM-HE20-NLIEUHED        REDEFINES                 00008001
                    COMM-HE10-NLIEUHED        PIC X(3).                 00008001
             3      COMM-HE20-NGHE            PIC X(7).                 00008001
             3      COMM-HE20-SAISIE-HET      PIC X(1).                 00008200
             3      COMM-HE20-PROG            PIC X(04).                20     1
             3      COMM-HE20-TOP-PF5         PIC X(01).                20     1
             3      COMM-HE20-NSOCENV         PIC X(03).                20     1
             3      COMM-HE20-NLIEUENV        PIC X(03).                20     1
      *             CODE IMPRIMANTE                                             
             3      COMM-HE10-CIMP            PIC X(04).                20     1
             3      COMM-HE20-CIMP            REDEFINES                 20     1
                    COMM-HE10-CIMP            PIC X(04).                20     1
             3      COMM-HE10-LLIEUHED        PIC X(20).                20     1
             3      COMM-HE10-CTYPLIEU        PIC X(1).                 20     1
             3      COMM-HE10-CTYPLIEUACID    PIC X(1).                 20     1
             3      COMM-HE10-DRETOUR         PIC X(08).                20     1
             3      COMM-HE10-CTRAIT          PIC X(05).                20     1
             3      COMM-HE10-LTRAIT          PIC X(20).                20     1
             3      COMM-HE10-MESS            PIC X(50).                20     1
             3      COMM-HE10-RC-MHE10        PIC X(04).                20     1
             3      COMM-HE10-WCTRLNHS        PIC X(01).                20     1
             3      COMM-HE10-HEADR           PIC X(01).                20     1
             3      COMM-HE10-CISOC-NSOC      PIC X(3).                 00008001
             3      COMM-HE20-CISOC-NSOC      REDEFINES                 00008001
                    COMM-HE10-CISOC-NSOC      PIC X(3).                 00008001
             3      COMM-HE70-HESOC-WPANNE    PIC X(1).                         
             3      COMM-HE70-HEFLU           PIC X(1).                         
             3      COMM-HE70-NTYOPE-THE31    PIC X(5).                         
             3      COMM-HE70-NTYOPE-THE61    PIC X(5).                         
             3      COMM-HE20-FILLER1         PIC X(9).                 20     1
                                                                        20     1
      *              GESTION DES CODES TYPES D'OPERATIONS (NTYOPE)              
                                                                        20     1
             3       COMM-HEGOP.                                        20     1
               5     COMM-HE-I               PIC  9(02).                        
               5     COMM-HE-MAX             PIC  9(02).                        
               5     COMM-HE-LIGNES          OCCURS 12 .                        
                 7   COMM-HE-FLAGAVAR.                                          
                   9 COMM-HE-FLAV            PIC  X(04).                        
                   9 COMM-HE-FLAR            PIC  X(04).                        
                 7   COMM-HE-OPAV.                                              
                   9 COMM-HE-OPAVDE          PIC  X(03).                        
                   9 COMM-HE-OPAVFI          PIC  X(03).                        
                 7   COMM-HE-OPAR.                                              
                   9 COMM-HE-OPARDE          PIC  X(03).                        
                   9 COMM-HE-OPARFI          PIC  X(03).                        
             3      COMM-HE99-APPLI.                                    00007601
                                                                        00007700
              5     COMM-HE99-GESTION-TS.                                       
               9    COMM-HE99-PAGE       PIC 9(02).                             
               9    COMM-HE99-NBP        PIC 9(02).                             
               9    COMM-HE99-NBL        PIC 9(02).                             
              5     COMM-HE99-CSTABLE    PIC  X(05).                    20     1
              5     COMM-HE99-CFAM       PIC  X(05).                    20     1
              5     COMM-HE99-SEL        PIC  X(05).                    20     1
              5     COMM-HE99-TSUP       PIC  X(05).                    20     1
              5     COMM-HE99-TMAX       PIC  X(05).                    20     1
              5     COMM-HE99-LIB        PIC  X(20).                    20     1
             3      COMM-HE20-NDEMANDE   PIC  X(07).                            
             3      COMM-HE20-FILLER2    PIC  X(13).                    20     1
      *--------INITIALISER A CHAQUE OPTION                              00008400
             3       COMM-HE20-ZONES.                                   00008501
               5     COMM-HE20-CODRET     PIC X.                        00009101
                 88  COMM-HE20-PAS-MESS              VALUE ' '.         00009201
                 88  COMM-HE20-MESS-RET              VALUE '1'.         00009301
               5     COMM-HE20-MESSAGE    PIC X(50).                    00009401
               5     FILLER               PIC X(360).                   00009500
               5     COMM-HE20-SSPRGS     PIC X(2754).                  00009601
      *                                                                 00009700
      *--------ZONES PROPRES A THE11                                    00009801
                 05 COMM-HE11-SSPRG-11    REDEFINES COMM-HE20-SSPRGS .  00009901
                    10  COMM-HE11-MODETRAIT     PIC  X(3).                      
                    10  COMM-HE11-NGHE          PIC  X(7).                      
                    10  COMM-HE11-NCODIC        PIC  X(7).                      
                    10  COMM-HE11-CFAM          PIC  X(5).                      
                    10  COMM-HE11-CMARQ         PIC  X(5).                      
                    10  COMM-HE11-LREF          PIC  X(20).                     
                    10  COMM-HE11-NLIEUVTE      PIC  X(3).                      
                    10  COMM-HE11-NVENTE        PIC  X(7).                      
                    10  COMM-HE11-DVENTE        PIC  X(8).                      
                    10  COMM-HE11-DECHANG       PIC  X(8).                      
                    10  COMM-HE11-CTRAIT        PIC  X(5).                      
                    10  COMM-HE11-LTRAIT        PIC  X(20).                     
                    10  COMM-HE11-CTYPHS        PIC  X(5).                      
                    10  COMM-HE11-LTYPHS        PIC  X(20).                     
                    10  COMM-HE11-CPANNE        PIC  X(5).                      
                    10  COMM-HE11-LPANNE        PIC  X(20).                     
                    10  COMM-HE11-CTITRENOM     PIC  X(5).                      
                    10  COMM-HE11-LNOM          PIC  X(25).                     
                    10  COMM-HE11-LPRENOM       PIC  X(15).                     
                    10  COMM-HE11-LADR1         PIC  X(32).                     
                    10  COMM-HE11-LADR2         PIC  X(32).                     
                    10  COMM-HE11-CPOSTAL       PIC  X(05).                     
                    10  COMM-HE11-LCOMMUNE      PIC  X(26).                     
                    10  COMM-HE11-NTEL          PIC  X(08).                     
                    10  COMM-HE11-NSOCIETE      PIC  X(03).                     
                    10  COMM-HE11-NLIEU         PIC  X(03).                     
                    10  COMM-HE11-NSSLIEU       PIC  X(03).                     
                    10  COMM-HE11-CLIEUTRT      PIC  X(05).                     
                    10  COMM-HE11-NSERIE        PIC  X(16).                     
                    10  COMM-HE11-NACCORD       PIC  X(12).                     
                    10  COMM-HE11-LNOMACCORD    PIC  X(10).                     
      *        ZONES POUR GESTION MHE90                                         
                    10  COMM-HE11-NCODIC-ANC    PIC  X(07).                     
                    10  COMM-HE11-CLIEUHET-ANC  PIC  X(05).                     
                    10  COMM-HE11-CTRAIT-ANC    PIC  X(05).                     
                    10  COMM-HE11-TOPANO-AR     PIC  X(01).             00011201
                    10  COMM-HE11-TOPANO-AV     PIC  X(01).             00011201
                    10  COMM-HE11-SAISIE-NHS    PIC  X(01).                     
                    10  COMM-HE11-CODE-MAJ      PIC  X(01).                     
                    10  COMM-HE11-FILLER        PIC  X(2380).           00011201
                                                                        00014100
      *--------ZONES PROPRES A THE15                                    00014201
                 05 COMM-HE15-SSPRG-15    REDEFINES COMM-HE20-SSPRGS .  00014301
                    10  COMM-HE15-NOPERA        PIC  X(03).             00011201
                    10  COMM-HE15-GESTION-TS.                                   
                     15 COMM-HE15-ITEM          PIC  9(2).                      
                     15 COMM-HE15-POS-MAX       PIC  9(2).                      
                     15 COMM-HE15-CPT-LIGNE     PIC  9(2).                      
                    10  COMM-HE15-NRETOUR       PIC  9(7).                      
                    10  COMM-HE15-MESS          PIC  X(50).             00011201
                    10  COMM-HE15-RC-MHE15      PIC  X(04).             00011201
                    10  COMM-HE15-FILLER        PIC  X(2684).           00011201
                                                                        00009700
      *--------ZONES PROPRES A THE30, THE31 ET MHE31                    00009801
               5     COMM-HE30-SSPRG-30   REDEFINES COMM-HE20-SSPRGS.   00009901
                 7   COMM-HE30-GESTION-TS.                                      
                   9 COMM-HE30-ITEM       PIC 9(02).                            
                   9 COMM-HE30-POS-MAX    PIC 9(02).                            
                   9 COMM-HE30-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE30-J1         PIC 9(02).                            
                 7   COMM-HE30-J2         PIC 9(02).                            
                 7   COMM-HE30-PAGE       PIC 9(02).                            
                 7   COMM-HE30-FILLER     PIC X(1467).                          
                 7   COMM-HE31-GESTION-TS.                                      
                   9 COMM-HE31-ITEM       PIC 9(02).                            
                   9 COMM-HE31-POS-MAX    PIC 9(02).                            
                   9 COMM-HE31-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE31-SSPRG-31.                                        
                   9 COMM-HE31-DRETOUR    PIC X(10).                            
                   9 COMM-HE31-NRETOUR    PIC X(11).                            
                 7   COMM-HE31-RC-MHE31   PIC X(01).                            
                 7   COMM-HE31-I          PIC 9(02).                            
                 7   COMM-HE31-PAGE       PIC 9(02).                            
                 7   COMM-HE31-K          PIC S9(05) COMP-3.                    
                 7   COMM-HE31-EIBCPOSN   PIC  9(04).                           
                 7   COMM-HE31-PF         PIC  9(01).                           
                 7   COMM-HE31-RC-MHE32   PIC X(01).                            
                 7   COMM-HE31-FILLER     PIC X(1234).                          
                                                                        00014100
      *--------ZONES PROPRES A THE40 ET MHE40                           00014201
               5     COMM-HE40-SSPRG-40   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE40-GESTION-TS.                                      
                   9 COMM-HE40-ITEM       PIC 9(02).                            
                   9 COMM-HE40-POS-MAX    PIC 9(02).                            
                   9 COMM-HE40-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE40-MESS       PIC X(50).                            
                 7   COMM-HE40-RC-MHE40   PIC X(04).                            
                 7   COMM-HE40-FILLER     PIC  X(2694).                 00011201
      *--------ZONES PROPRES A THE41 ET MHE41                           00014201
               5     COMM-HE41-SSPRG-41   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE41-GESTION-TS.                                      
                   9 COMM-HE41-ITEM       PIC 9(02).                            
                   9 COMM-HE41-POS-MAX    PIC 9(02).                            
                   9 COMM-HE41-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE41-LNOM       PIC X(20).                            
                 7   COMM-HE41-CLIEUHEI   PIC X(05).                            
                 7   COMM-HE41-LLIEUHEI   PIC X(20).                            
                 7   COMM-HE41-NLOTDIAG   PIC X(07).                            
                 7   COMM-HE41-MESS       PIC X(50).                            
                 7   COMM-HE41-RC-MHE41   PIC X(04).                            
                 7   COMM-HE41-FILLER     PIC  X(2642).                 00011201
                                                                        00014100
      *--------ZONES PROPRES A THE50 ET THE51                           00014201
               5     COMM-HE50-SSPRG-50   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE50-GESTION-TS.                                      
                   9 COMM-HE50-ITEM       PIC 9(02).                            
                   9 COMM-HE50-POS-MAX    PIC 9(02).                            
                   9 COMM-HE50-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE50-J          PIC  9(02).                   00011201
                 7   COMM-HE50-PAGE       PIC  9(02).                   00011201
                 7   COMM-HE50-FILLER     PIC  X(1235).                 00011201
                 7   COMM-HE51-GESTION-TS.                                      
                   9 COMM-HE51-ITEM       PIC 9(02).                            
                   9 COMM-HE51-POS-MAX    PIC 9(02).                            
                   9 COMM-HE51-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE51-DRETOUR    PIC X(11).                            
                 7   COMM-HE51-CLIEUHEI   PIC X(05).                            
                 7   COMM-HE51-LLIEUHEI   PIC X(20).                            
                 7   COMM-HE51-DLOTDIAG   PIC X(10).                            
                 7   COMM-HE51-NLOTDIAG   PIC X(07).                            
                 7   COMM-HE51-LNOM       PIC X(20).                            
                 7   COMM-HE51-RC-MHE51   PIC X(04).                            
                 7   COMM-HE51-I          PIC 9(02).                            
                 7   COMM-HE51-PAGE       PIC 9(02).                            
                 7   COMM-HE51-K          PIC S9(05) COMP-3.                    
                 7   COMM-HE51-EIBCPOSN   PIC  9(04).                           
                 7   COMM-HE51-PF         PIC  9(01).                           
                 7   COMM-HE51-FILLER     PIC  X(1190).                 00011201
                                                                        00014100
      *--------ZONES PROPRES A THE55                                    00014201
               5     COMM-HE55-SSPRG-55   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE55-GESTION-TS.                                      
                   9 COMM-HE55-ITEM       PIC 9(02).                            
                   9 COMM-HE55-POS-MAX    PIC 9(02).                            
                   9 COMM-HE55-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE55-I          PIC 9(02).                            
                 7   COMM-HE55-EIBCPOSN   PIC 9(04).                            
                 7   COMM-HE51-FILLER     PIC X(2742).                  00011201
                                                                        00014100
      *--------ZONES PROPRES A THE60 ET MHE60                           00014201
               5     COMM-HE60-SSPRG-60   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE60-GESTION-TS.                                      
                   9 COMM-HE60-ITEM       PIC 9(02).                            
                   9 COMM-HE60-POS-MAX    PIC 9(02).                            
                   9 COMM-HE60-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE60-MESS       PIC X(50).                            
                 7   COMM-HE60-RC-MHE60   PIC X(04).                            
                 7   COMM-HE60-NDEM-X.                                          
                   9 COMM-HE60-NDEMANDE   PIC 9(07).                            
      *---           'A' AUTOMATIQUE, 'F' FORCE                                 
                 7   COMM-HE60-CHOIX      PIC X.                                
                 7   COMM-HE60-FILLER     PIC  X(2686).                 00011201
                                                                        00014100
      *--------ZONES PROPRES A THE61, MHE61                             00014201
               5     COMM-HE61-SSPRG-61   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE61-SSPRG.                                           
                  8  COMM-HE61-GESTION-TS.                                      
                   9 COMM-HE61-ITEM       PIC S9(03) COMP-3.                    
                   9 COMM-HE61-POS-MAX    PIC S9(03) COMP-3.                    
                   9 COMM-HE61-CPT-LIGNE  PIC S9(03) COMP-3.                    
                  8  COMM-HE61-LNOM       PIC X(20).                            
                  8  COMM-HE61-LADR1      PIC X(32).                            
                  8  COMM-HE61-LADR2      PIC X(32).                            
                  8  COMM-HE61-CPOSTAL    PIC X(05).                            
                  8  COMM-HE61-LCOMMUNE   PIC X(26).                            
                  8  COMM-HE61-CTYPTRAN   PIC X(05).                            
                  8  COMM-HE61-RC-MHE61   PIC X(04).                            
                  8  COMM-HE61-NENV-X.                                          
                   9 COMM-HE61-NENVOI     PIC 9(07).                            
                  8  COMM-HE61-FILLER     PIC X(100).                   00011201
                 7   COMM-HE65-SSPRG      REDEFINES COMM-HE61-SSPRG.            
                  8  COMM-HE65-GESTION-TS.                                      
                   9 COMM-HE65-ITEM       PIC 9(02).                            
                   9 COMM-HE65-POS-MAX    PIC 9(02).                            
                   9 COMM-HE65-CPT-LIGNE  PIC 9(02).                            
                  8  COMM-HE65-LNOM       PIC X(20).                            
                  8  COMM-HE65-LADR1      PIC X(32).                            
                  8  COMM-HE65-LADR2      PIC X(32).                            
                  8  COMM-HE65-CPOSTAL    PIC X(05).                            
                  8  COMM-HE65-LCOMMUNE   PIC X(26).                            
                  8  COMM-HE65-CTYPTRAN   PIC X(05).                            
                  8  COMM-HE65-RC-MHE65   PIC X(04).                            
                  8  COMM-HE65-NENV-X.                                          
                   9 COMM-HE65-NENVOI     PIC 9(07).                            
                  8  COMM-HE65-FILLER     PIC X(100).                   00011201
                 7   COMM-EF10-GESTION-TS.                                      
                   9 COMM-EF10-ITEM       PIC 9(02).                            
                   9 COMM-EF10-POS-MAX    PIC 9(02).                            
                   9 COMM-EF10-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE61-FILLER     PIC  X(2511).                 00011201
      *--------ZONES PROPRES A THE62, MHE62                             00014201
               5     COMM-HE62-SSPRG-62   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE62-GESTION-TS.                                      
                   9 COMM-HE62-ITEM       PIC 9(02).                            
                   9 COMM-HE62-POS-MAX    PIC 9(02).                            
                   9 COMM-HE62-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE62-FILLER     PIC  X(2748).                 00011201
                                                                        00014100
      *--------ZONES PROPRES A THE70                                    00014201
               5     COMM-HE70-SSPRG-70   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE70-GESTION-TS.                                      
                   9 COMM-HE70-ITEM       PIC  9(02).                           
                   9 COMM-HE70-POS-MAX    PIC  9(02).                           
                   9 COMM-HE70-CPT-LIGNE  PIC  9(02).                           
                                                                        00011201
                 7   COMM-HE70-NCODIC     PIC  X(07).                   00011201
                 7   COMM-HE70-DDEPOT     PIC  X(08).                   00011201
                 7   COMM-HE70-CTRAIT     PIC  X(05).                   00011201
                 7   COMM-HE70-CTYPHS     PIC  X(05).                   00011201
                 7   COMM-HE70-LPANNE     PIC  X(20).                   00011201
                 7   COMM-HE70-CTITRENOM  PIC  X(05).                   00011201
                 7   COMM-HE70-LNOM       PIC  X(25).                   00011201
                 7   COMM-HE70-LPRENOM    PIC  X(15).                   00011201
                 7   COMM-HE70-LADR1      PIC  X(32).                   00011201
                 7   COMM-HE70-LADR2      PIC  X(32).                   00011201
                 7   COMM-HE70-CPOSTAL    PIC  X(05).                   00011201
                 7   COMM-HE70-LCOMMUNE   PIC  X(26).                   00011201
                 7   COMM-HE70-NTEL       PIC  X(08).                   00011201
                 7   COMM-HE70-NRETOUR    PIC  X(07).                   00011201
                 7   COMM-HE70-CREFUS     PIC  X(05).                   00011201
                 7   COMM-HE70-CPANNE     PIC  X(05).                   00011201
                 7   COMM-HE70-CLIEUHEI   PIC  X(05).                   00011201
                 7   COMM-HE70-NLOTDIAG   PIC  X(07).                   00011201
                 7   COMM-HE70-NTYOPE     PIC  X(03).                   00011201
                 7   COMM-HE70-NLIEU      PIC  X(03).                   00011201
                 7   COMM-HE70-NSSLIEU    PIC  X(03).                   00011201
                 7   COMM-HE70-CLIEUTRT   PIC  X(05).                   00011201
                 7   COMM-HE70-WSOLDE     PIC  X(01).                   00011201
                 7   COMM-HE70-CLIEUHET   PIC  X(05).                           
                 7   COMM-HE70-TOPANO     PIC  X(01).                           
                 7   COMM-HE70-NSERIE     PIC  X(16).                           
                 7   COMM-HE70-NACCORD    PIC  X(12).                           
                 7   COMM-HE70-LNOMACCORD PIC  X(10).                           
                 7   COMM-HE70-NHS-A-SAISIR PIC  X(01).                         
                 7   COMM-HE70-NRENDU     PIC  X(20).                           
                 7   COMM-HE70-DRENDU     PIC  X(08).                           
                 7   COMM-HE70-ADRESSE    PIC  X(07).                           
                 7   COMM-HE70-NFA        PIC  X(07).                           
                 7   COMM-HE70-PDOSNASC   PIC  X(03).                           
                 7   COMM-HE70-DOSNASC    PIC  X(09).                           
                 7   COMM-HE70-CTYINASC   PIC  X(05).                           
                 7   COMM-HE70-NLIEURET   PIC  X(03).                           
                                                                        00011201
                 7   COMM-HE70-FILLER     PIC  X(2404).                 00011201
      *--------ZONES PROPRES A THE80, MHE80                             00014201
               5     COMM-HE80-SSPRG-80   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE80-GESTION-TS.                                      
                   9 COMM-HE80-ITEM       PIC 9(02).                            
                   9 COMM-HE80-POS-MAX    PIC 9(02).                            
                   9 COMM-HE80-CPT-LIGNE  PIC 9(02).                            
                 7   COMM-HE80-STATUT     PIC  X(03).                   00011201
                 7   COMM-HE80-CLIEUHEI   PIC  X(05).                   00011201
                 7   COMM-HE80-LTYOPE     PIC  X(20).                   00011201
                 7   COMM-HE80-CTRAIT     PIC  X(05).                   00011201
                 7   COMM-HE80-LTRAIT     PIC  X(20).                   00011201
                 7   COMM-HE80-OPTION     PIC  9(02).                   00011201
                 7   COMM-HE80-RC-MHE80   PIC  X(04).                   00011201
                 7   COMM-HE80-FILLER     PIC  X(2689).                 00011201
      *--------ZONES PROPRES A THE12                                    00014201
               5     COMM-HE12-SSPRG-12   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE12-OPTION     PIC  X.                               
                 7   COMM-HE12-FILLER     PIC  X(2753).                 00011201
      *--------ZONES PROPRES A MHE13                                    00014201
               5     COMM-HE13-SSPRG-13   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE13-VALID      PIC  X.                               
                 7   COMM-HE13-DEBUT      PIC  X(7).                            
                 7   COMM-HE13-FIN        PIC  X(7).                    00011201
                 7   COMM-HE13-NRETOUR    PIC  9(7).                            
                 7   COMM-HE13-MESS       PIC  X(50).                           
                 7   COMM-HE13-RC-MHE13   PIC  X(04).                           
                 7   COMM-HE13-FILLER     PIC  X(2678).                 00011201
      *---        THE64                                                         
               5     COMM-HE64-SSPRG-61   REDEFINES COMM-HE20-SSPRGS.   00014301
                 7   COMM-HE64-SSPRG.                                           
                  8  COMM-HE64-GESTION-TS.                                      
                   9 COMM-HE64-ITEM       PIC 9(02).                            
                   9 COMM-HE64-POS-MAX    PIC 9(02).                            
                   9 COMM-HE64-CPT-LIGNE  PIC 9(02).                            
                  8  COMM-HE64-LNOM       PIC X(20).                            
                  8  COMM-HE64-LADR1      PIC X(32).                            
                  8  COMM-HE64-LADR2      PIC X(32).                            
                  8  COMM-HE64-CPOSTAL    PIC X(05).                            
                  8  COMM-HE64-LCOMMUNE   PIC X(26).                            
                  8  COMM-HE64-CTYPTRAN   PIC X(05).                            
                  8  COMM-HE64-RC-MHE64   PIC X(04).                            
                  8  COMM-HE64-NENV-X.                                          
                   9 COMM-HE64-NENVOI     PIC 9(07).                            
                  8  COMM-HE64-FILLER     PIC X(2617).                  00011201
                                                                                
