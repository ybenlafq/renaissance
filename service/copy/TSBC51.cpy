      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------                     
      * TS-LISTE DE CLIENTS DONNEURS D'ORDRE                                    
      * REPONSE  MQ-SERISE                                                      
      *----------------------------------LONGUEUR          X 90                 
       01 TSBC51-DATA.                                                          
          05 TSBC51-LIGNE .                                                     
             10 TSBC51-NCARTE                    PIC X(09).                     
             10 TSBC51-TORG                      PIC X(05).                     
             10 TSBC51-LNOM                      PIC X(60).                     
             10 TSBC51-NCLIENTADR                PIC X(08).                     
             10 TSBC51-NSIRET                    PIC X(14).                     
             10 TSBC51-NTDOM                     PIC X(15).                     
             10 TSBC51-CPOST                     PIC X(05).                     
             10 TSBC51-LCOMN                     PIC X(32).                     
             10 TSBC51-CVOIE                     PIC X(05).                     
             10 TSBC51-CTVOIE                    PIC X(04).                     
             10 TSBC51-LNOMVOIE                  PIC X(25).                     
             10 TSBC51-CMPAD1                    PIC X(32).                     
             10 TSBC51-LBUREAU                   PIC X(26).                     
             10 TSBC51-CPAYS                     PIC X(03).                     
             10 TSBC51-TPAYS                     PIC X(03).                     
             10 TSBC51-ORGMODIF                  PIC X(01).                     
             10 TSBC51-TFACTU                    PIC X(01).                     
             10 TSBC51-ARF                       PIC X(01).                     
             10 TSBC51-MONTANT                   PIC Z(10).                     
             10 TSBC51-NIERRARCH                 PIC X(01).                     
             10 TSBC51-FACT                      PIC X(01).                     
             10 TSBC51-CPTEPARENT                PIC X(08).                     
             10 TSBC51-CTYPCLIENT                PIC X(01).                     
             10 TSBC51-NCOMPTE                   PIC X(08).                     
             10 TSBC51-RANG-TSBC61               PIC S9(5) COMP-3.              
             10 TSBC51-INDTS-MAX                 PIC S9(5) COMP-3.              
             10 TSBC51-REMISE                    PIC X(5).                      
             10 TSBC51-DATA-FILLER               PIC X(40).                     
                                                                                
