      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTRB35                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3500.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       N COMMANDE                                        
           10 RB35-NCOMMANDE       PIC X(7).                                    
      *    *************************************************************        
      *                       N RECEPTION ADMINISTRATIVE                        
           10 RB35-NREC            PIC X(7).                                    
      *    *************************************************************        
      *                       N CODIC                                           
           10 RB35-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       DRECEPT ADMIN                                     
           10 RB35-DRECEPT         PIC X(8).                                    
      *    *************************************************************        
      *                       PRIX FACTURE                                      
           10 RB35-PVUNIT          PIC S9(5)V9(2) USAGE COMP-3.                 
      *    *************************************************************        
      *                       D.MAJ PRIX                                        
           10 RB35-DMAJPV          PIC X(8).                                    
      *    *************************************************************        
      *                       ACID                                              
      *                       ID.UTILISATEUR                                    
           10 RB35-ACID            PIC X(8).                                    
      *    *************************************************************        
      *                       DATE SYSTEME DE M.A.J LIGNE                       
           10 RB35-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRB3500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRB3500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-NCOMMANDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-NCOMMANDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-NREC-F            PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-NREC-F            PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-NCODIC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-NCODIC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-DRECEPT-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-DRECEPT-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-PVUNIT-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-PVUNIT-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-DMAJPV-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-DMAJPV-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-ACID-F            PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-ACID-F            PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 RB35-DSYST-F           PIC S9(4) COMP.                            
      *--                                                                       
           10 RB35-DSYST-F           PIC S9(4) COMP-5.                          
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
                                                                                
