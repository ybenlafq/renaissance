      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM010 AU 09/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,03,BI,A,                          *        
      *                           27,23,BI,A,                          *        
      *                           50,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM010.                                                        
            05 NOMETAT-INM010           PIC X(6) VALUE 'INM010'.                
            05 RUPTURES-INM010.                                                 
           10 INM010-DATETR             PIC X(08).                      007  008
           10 INM010-NSOC               PIC X(03).                      015  003
           10 INM010-DEVTRANS           PIC X(03).                      018  003
           10 INM010-NLIEU              PIC X(03).                      021  003
           10 INM010-NCAISS             PIC X(03).                      024  003
           10 INM010-LMOPAI             PIC X(23).                      027  023
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM010-SEQUENCE           PIC S9(04) COMP.                050  002
      *--                                                                       
           10 INM010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM010.                                                   
           10 INM010-DEVAUT             PIC X(03).                      052  003
           10 INM010-LDEVREF            PIC X(25).                      055  025
           10 INM010-LDEVTRANS          PIC X(25).                      080  025
           10 INM010-LLIEU              PIC X(20).                      105  020
           10 INM010-AP-AUT             PIC S9(08)V9(2) COMP-3.         125  006
           10 INM010-AP-TRANS           PIC S9(08)V9(2) COMP-3.         131  006
           10 INM010-CO-AUT             PIC S9(05)V9(2) COMP-3.         137  004
           10 INM010-CO-TRANS           PIC S9(05)V9(2) COMP-3.         141  004
           10 INM010-DE-AUT             PIC S9(08)V9(2) COMP-3.         145  006
           10 INM010-DE-TRANS           PIC S9(08)V9(2) COMP-3.         151  006
           10 INM010-FO-AUT             PIC S9(08)V9(2) COMP-3.         157  006
           10 INM010-FO-TRANS           PIC S9(08)V9(2) COMP-3.         163  006
           10 INM010-MVT-AUT            PIC S9(08)V9(2) COMP-3.         169  006
           10 INM010-MVT-TRANS          PIC S9(08)V9(2) COMP-3.         175  006
           10 INM010-RE-AUT             PIC S9(08)V9(2) COMP-3.         181  006
           10 INM010-RE-TRANS           PIC S9(08)V9(2) COMP-3.         187  006
           10 INM010-VE-AUT             PIC S9(09)V9(2) COMP-3.         193  006
           10 INM010-VE-TRANS           PIC S9(09)V9(2) COMP-3.         199  006
            05 FILLER                      PIC X(308).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM010-LONG           PIC S9(4)   COMP  VALUE +204.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM010-LONG           PIC S9(4) COMP-5  VALUE +204.           
                                                                                
      *}                                                                        
