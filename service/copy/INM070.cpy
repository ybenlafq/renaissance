      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT INM070 AU 14/06/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                           20,03,BI,A,                          *        
      *                           23,03,BI,A,                          *        
      *                           26,08,BI,A,                          *        
      *                           34,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-INM070.                                                        
            05 NOMETAT-INM070           PIC X(6) VALUE 'INM070'.                
            05 RUPTURES-INM070.                                                 
           10 INM070-DATETR             PIC X(08).                      007  008
           10 INM070-NSOCIETE           PIC X(03).                      015  003
           10 INM070-CCOMPTEUR          PIC X(02).                      018  002
           10 INM070-NLIEU              PIC X(03).                      020  003
           10 INM070-NCAISSE            PIC X(03).                      023  003
           10 INM070-NTRANS             PIC X(08).                      026  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INM070-SEQUENCE           PIC S9(04) COMP.                034  002
      *--                                                                       
           10 INM070-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-INM070.                                                   
           10 INM070-CDEVAUT            PIC X(03).                      036  003
           10 INM070-CDEVRF             PIC X(03).                      039  003
           10 INM070-DATED              PIC X(10).                      042  010
           10 INM070-LCOMPTEUR          PIC X(30).                      052  030
           10 INM070-LDEVAUT            PIC X(05).                      082  005
           10 INM070-LDEVRF             PIC X(05).                      087  005
           10 INM070-NCRI               PIC X(10).                      092  010
           10 INM070-NLIEUSAV           PIC X(03).                      102  003
           10 INM070-NSOCSAV            PIC X(03).                      105  003
           10 INM070-NTOURN             PIC X(06).                      108  006
           10 INM070-CUMUL-AU-PDPAIE    PIC 9(10)V9(2).                 114  012
           10 INM070-PDPAIE             PIC S9(08)V9(2) COMP-3.         126  006
            05 FILLER                      PIC X(381).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-INM070-LONG           PIC S9(4)   COMP  VALUE +131.           
      *                                                                         
      *--                                                                       
        01  DSECT-INM070-LONG           PIC S9(4) COMP-5  VALUE +131.           
                                                                                
      *}                                                                        
