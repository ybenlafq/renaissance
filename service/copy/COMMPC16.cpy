      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------                 
      *    CRITERES DE SELECTION                                                
      * COMM DE PROGRAMME MPC16                                                 
      * APPELLE PAR LINK DANS TMC00                                             
      *----------------------------------LONGUEUR                               
       01 Z-COMMAREA-MPC16.                                                     
      * CRITERES DE SELECTION CLILENT                                           
         05 ZONES-SELECTION.                                                    
              10 COMM-MPC16-AAMMJJ       PIC X(06).                     01560   
              10 COMM-MPC16-JJMMSSAA     PIC X(08).                     01560   
              10 COMM-MPC16-INDMAX       PIC S9(5) COMP-3.              01560   
              10 COMM-MPC16-HEURE        PIC 9(08).                     01560   
         05  COMM-MPC16-DEMAND-EDIT     PIC X(01) .                             
              88 COMM-MPC16-EDIT-TOUT     VALUE '1'.                            
         05  COMM-MPC16-DETAIL          PIC X(01) .                             
              88 COMM-MPC16-EDIT-ANO      VALUE '1'.                            
         05  COMM-MPC16-DARRETE         PIC X(06).                              
         05  COMM-MPC16-DCTRL           PIC X(08).                              
      *      MESSAGE ERREUR                                                     
         05 COMM-MPC16-RETOUR.                                                  
             10 COMM-MPC16-CODRET        PIC X(01).                             
                  88  COMM-MPC16-OK     VALUE ' '.                              
                  88  COMM-MPC16-ERREUR VALUE '1'.                              
             10 COMM-MPC16-LIBERR        PIC X(60).                             
         05 FILLER                       PIC X(22).                             
                                                                                
