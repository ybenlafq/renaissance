      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-FR05-RECORD.                                                      
           05 TS-FR05-TABLE            OCCURS 12.                               
              10 TS-FR05-CPROG             PIC X(05).                           
              10 TS-FR05-SOCORI            PIC X(03).                           
              10 TS-FR05-LIEUORI           PIC X(03).                           
              10 TS-FR05-SOCDST            PIC X(03).                           
              10 TS-FR05-LIEUDST           PIC X(03).                           
              10 TS-FR05-SOCVTE            PIC X(03).                           
              10 TS-FR05-LIEUVTE           PIC X(03).                           
                                                                                
