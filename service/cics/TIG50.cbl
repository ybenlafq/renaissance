      * @(#) MetaWare Technologies Cataloguer 0.9.18
      * Translated: 19/10/2016 18:44
      * Portfolio: IMAGE01
      *!!!!TRANSLATION-ISSUE!!!!
      *    2 translation issues shown below.
      *    0 additional translation issues not shown below.
      *    2 total translation issues.
      *!!!!TRANSLATION-ISSUE!!!!
      * FATAL ERROR: store original comments
      *!!!!TRANSLATION-ISSUE!!!!
      *E1--NYS: Untranslated ON statement: RE::*UNDEFINED*
       IDENTIFICATION DIVISION.
       PROGRAM-ID. TIG50.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01  S--PROGRAM                PIC X(8) VALUE 'TIG50'.
       01  ENDVAL--5                 PIC S9(5).
       01  MW-REPEAT-CHAR--131       PIC X(131).
       01  MW-REPEAT-CHAR--8         PIC X(8).
       01  TSSRL_PTR                 POINTER.
       01  TSSRL_LEN                 PIC S9(4) COMP-5.
       01  TSSCPLR.
      * AUTHORIZED      */
       05 TSSROK                     PIC S9(4) COMP-5 VALUE 0.
      * RES NOT DEFINED */
       05 TSSRND                     PIC S9(4) COMP-5 VALUE 4.
      * NOT-AUTHORIZED  */
       05 TSSRNA                     PIC S9(4) COMP-5 VALUE 8.
      * INVALID P-LIST  */
       05 TSSRIPL                    PIC S9(4) COMP-5 VALUE 12.
      * ENVIRON ERROR   */
       05 TSSRENV                    PIC S9(4) COMP-5 VALUE 16.
      * TSS NOT ACTIVE  */
       05 TSSEINAC                   PIC S9(4) COMP-5 VALUE 20.
      * P-LIST SHORT   */
       05 TSSPLEN                    PIC S9(4) COMP-5 VALUE 370.
      * P-LIST FACLIST */
       05 TSSPLEN2                   PIC S9(4) COMP-5 VALUE 1138.
       01  TSSCPLS.
      * DEFINED USER    */
       05 TSSSDEF                    PIC S9(4) COMP-5 VALUE 0.
      * UNDEFINED USER  */
       05 TSSSUND                    PIC S9(4) COMP-5 VALUE 4.
      * NOT SIGNED-ON   */
       05 TSSSNSO                    PIC S9(4) COMP-5 VALUE 8.
       05 TSSSIDT                    PIC S9(4) COMP-5 VALUE 12.
       COPY SP-VERIFY-COPY.
       LOCAL-STORAGE SECTION.
       01  MW--STG                   PIC S9(4) COMP-5.
       01  SYKWDIV0.
         05 FIL01_WDIV               PIC X(20) VALUE 'ZONES DECLAR.==>'.
         05 FIL02_WDIV               PIC X(13) VALUE 'DEBUG........'.
         05 DEBUGGIN                 PIC X(3) VALUE 'OUI'.
         05 FIL03_WDIV               PIC X(14) VALUE 'FONCTION......'.
         05 FONCTION                 PIC X(2) VALUE 'FF'.
         05 FIL04_WDIV               PIC X(15) VALUE 'KONTROL........'.
         05 KONTROL                  PIC X(1) VALUE '0'.
         05 FIL05_WDIV               PIC X(15) VALUE 'CODE_RETOUR....'.
         05 CODE_RETOUR              PIC X(1) VALUE '0'.
         05 FIL06_WDIV               PIC X(15) VALUE 'TYPE_CIRCUIT...'.
         05 TYPE_CIRCUIT             PIC X(1) VALUE '1'.
         05 FIL07_WDIV               PIC X(15) VALUE 'ETAT_ECRAN.....'.
         05 ETAT_ECRAN               PIC X(1) VALUE '0'.
         05 FIL08_WDIV               PIC X(15) VALUE 'ACTION.........'.
         05 ACTION                   PIC X(1) VALUE ' '.
      * ****************************************************************
      *                                                             ***/
      *         DECLARATIVES DES INDICES
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 FIL09_WDIV               PIC X(14) VALUE 'INDICE_IA.....'.
         05 IA                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL10_WDIV               PIC X(14) VALUE 'INDICE_AI.....'.
         05 AI                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL11_WDIV               PIC X(14) VALUE 'INDICE_IT.....'.
         05 IT                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL12_WDIV               PIC X(14) VALUE 'INDICE_LIGNE..'.
         05 IL                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL13_WDIV               PIC X(14) VALUE 'INDICE_PAGE...'.
         05 IP                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL14_WDIV               PIC X(15) VALUE 'ETAT_CONSULT...'.
         05 ETAT_CONSULTATION        PIC X(1) VALUE '0'.
         05 FIL15_WDIV               PIC X(11) VALUE 'NUM_5......'.
         05 NUM_5                    PIC 9(5) VALUE 0.
         05 FIL16_WDIV               PIC X(12) VALUE 'COMPTEUR....'.
         05 COMPTEUR                 PIC S9(7) COMP-3 VALUE 0.
         05 B1                       PIC X(1) VALUE ' '.
      * COMMENTAIRE */
      * ****************************************************************
      *                                                             ***/
      *         CODE ABANDON
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 FIL17_WDIV               PIC X(15) VALUE 'CODE_ABANDON...'.
         05 CODE_ABANDON             PIC X(1) VALUE ' '.
      * ****************************************************************
      *                                                             ***/
      *         DATE ET HEURE DE COMPILATION
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 Z_WHEN_COMPILED          PIC X(18) VALUE
                                                   '24.DEC.99 11.11.11'.
      * ****************************************************************
      *                                                             ***/
      *         DIFFERENTES ZONES POUR LES ORDRES CICS
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 LONG_COMMAREA            PIC S9(4) COMP-5 VALUE +100.
         05 LONG_COMMAREA_LINK       PIC S9(4) COMP-5 VALUE +4096.
         05 LONG_START               PIC S9(4) COMP-5 VALUE +4096.
         05 LONG_TD                  PIC S9(4) COMP-5 VALUE +0.
         05 LONG_TS                  PIC S9(4) COMP-5 VALUE +0.
         05 RANG_TS                  PIC S9(4) COMP-5 VALUE +0.
         05 FILE_NAME                PIC X(8).
         05 FILE_LONG                PIC S9(4) COMP-5 VALUE +0.
      * ****************************************************************
      *                                                             ***/
      *         ZONES   DIVERSES
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 NOM_MAP                  PIC X(7).
         05 NOM_MAPSET               PIC X(7).
         05 NOM_PROG                 PIC X(7).
         05 NOM_TACHE                PIC X(4).
         05 NOM_LEVEL_MAX            PIC X(4).
         05 NOM_PROG_XCTL            PIC X(7).
         05 NOM_PROG_LINK            PIC X(7).
         05 NOM_TACHE_RETOUR         PIC X(4).
         05 NOM_TACHE_START          PIC X(4).
         05 TERM_START               PIC X(4).
         05 NOM_TD                   PIC X(4).
         05 IDENT_TS                 PIC X(8).
         05 CODE_TRAITEMENT_NORMAL   PIC X(2) VALUE '01'.
         05 CODE_TRAITEMENT_CRITERES PIC X(2) VALUE '11'.
         05 CODE_TRAITEMENT_DATAS    PIC X(2) VALUE '22'.
         05 CODE_TRAITEMENT_AUTOMATIQUE PIC X(2) VALUE '10'.
         05 CODE_LEVEL_MAX           PIC X(2) VALUE 'KA'.
         05 CODE_HELP                PIC X(2) VALUE 'KB'.
         05 CODE_LEVEL_SUP           PIC X(2) VALUE 'KC'.
         05 CODE_SUSPENSION          PIC X(2) VALUE '03'.
         05 CODE_FIN                 PIC X(2) VALUE '04'.
         05 CODE_PREMIERE            PIC X(2) VALUE '05'.
         05 CODE_DERNIERE            PIC X(2) VALUE '06'.
         05 CODE_PRECEDENTE          PIC X(2) VALUE '07'.
         05 CODE_SUIVANTE            PIC X(2) VALUE '08'.
         05 CODE_ERREUR_MANIPULATION PIC X(2) VALUE 'FF'.
         05 CODE_SWAP                PIC X(2) VALUE 'KS'.
         05 CODE_CREATION            PIC X(1) VALUE 'C'.
         05 CODE_MODIFICATION        PIC X(1) VALUE 'M'.
         05 CODE_INTERROGATION       PIC X(1) VALUE 'I'.
         05 CODE_SUPPRESSION         PIC X(1) VALUE 'S'.
         05 CODE_FIN_BOUCLE_IA       PIC S9(3) COMP-3 VALUE +999.
         05 CODE_FIN_BOUCLE_AI       PIC S9(3) COMP-3 VALUE +999.
         05 CODE_FIN_TABLE           PIC S9(3) COMP-3 VALUE +999.
         05 CODE_CONSULTATION_EN_COURS PIC X(1) VALUE '0'.
         05 CODE_FIN_CONSULTATION    PIC X(1) VALUE '1'.
         05 APOSTROPHE               PIC X(1) VALUE ' '.
         05 BINAIRE                  PIC S9(4) COMP-5 VALUE 0.
         05 L_VAR                    PIC S9(9) COMP-5 VALUE 0.
         05 W_TRACE_CICS             PIC X(50).
       01  ZONES_SWAP.
         05 SWAP_LONG_TS             PIC S9(4) COMP-5 VALUE +6096.
         05 SWAP_FILLER_IDENT_TS.
           10 SWAP_IDENT_TS_A        PIC X(4).
           10 SWAP_IDENT_TS_B        PIC X(4).
         05 SWAP_RANG_TS             PIC S9(4) COMP-5 VALUE 0.
       01  VALEURS_INDIC.
         05 TRAITEMENT               PIC X(2) VALUE '01'.
         05 PAGINATION               PIC X(2) VALUE '05'.
         05 TRAITEMENT_NORMAL        PIC X(2) VALUE '01'.
         05 TRAITEMENT_CRITERES      PIC X(2) VALUE '11'.
         05 TRAITEMENT_DATAS         PIC X(2) VALUE '22'.
         05 TRAITEMENT_AUTOMATIQUE   PIC X(2) VALUE '10'.
         05 SUSPENSION               PIC X(2) VALUE '03'.
         05 FIN                      PIC X(2) VALUE '04'.
         05 PREMIERE                 PIC X(2) VALUE '05'.
         05 DERNIERE                 PIC X(2) VALUE '06'.
         05 PRECEDENTE               PIC X(2) VALUE '07'.
         05 SUIVANTE                 PIC X(2) VALUE '08'.
         05 LEVEL_MAX                PIC X(2) VALUE 'KA'.
         05 HELP                     PIC X(2) VALUE 'KB'.
         05 LEVEL_SUP                PIC X(2) VALUE 'KC'.
         05 ABANDON                  PIC X(2) VALUE 'KD'.
         05 ERREUR_MANIPULATION      PIC X(2) VALUE 'FF'.
         05 SWAP                     PIC X(2) VALUE 'KS'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR KONTROL
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ERREUR_AIDA              PIC X(1) VALUE '1'.
         05 ERREUR                   PIC X(1) VALUE '2'.
         05 OK                       PIC X(1) VALUE '0'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR CODE_RETOUR
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 TROUVE                   PIC X(1) VALUE '0'.
         05 NORMAL                   PIC X(1) VALUE '0'.
         05 DATE_OK                  PIC X(1) VALUE '0'.
         05 SELECTE                  PIC X(1) VALUE '0'.
         05 NON_TROUVE               PIC X(1) VALUE '1'.
         05 NON_SELECTE              PIC X(1) VALUE '1'.
         05 ANORMAL                  PIC X(1) VALUE '1'.
         05 EXISTE_DEJA              PIC X(1) VALUE '2'.
         05 FIN_FICHIER              PIC X(1) VALUE '3'.
         05 ERREUR_DATE              PIC X(1) VALUE '4'.
         05 ERREUR_FORMAT            PIC X(1) VALUE '5'.
         05 ERREUR_HEURE             PIC X(1) VALUE '6'.
         05 DOUBLE                   PIC X(1) VALUE '7'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR TYPE_CIRCUIT
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 CIRCUIT_COURT            PIC X(1) VALUE '7'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR ETAT_ECRAN
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ECRAN_MAPFAIL            PIC X(1) VALUE '1'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR ACTION
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 CREATION                 PIC X(1) VALUE 'C'.
         05 MODIFICATION             PIC X(1) VALUE 'M'.
         05 SUPPRESSION              PIC X(1) VALUE 'S'.
         05 INTERROGATION            PIC X(1) VALUE 'I'.
      * ****************************************************************
      *                                                               */
      *    VALEURS DES FIN DE BOUCLES
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 FIN_BOUCLE_IA            PIC S9(3) COMP-3 VALUE +999.
         05 FIN_BOUCLE_AI            PIC S9(3) COMP-3 VALUE +999.
         05 FIN_TABLE                PIC S9(3) COMP-3 VALUE +999.
         05 FIN_BOUCLE_IL            PIC S9(3) COMP-3 VALUE +999.
      * ****************************************************************
      *                                                               */
      *    VALEURS DE ETAT_CONSULTATION
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 FIN_CONSULTATION         PIC X(1) VALUE '1'.
      * ****************************************************************
      *                                                               */
      *    VALEURS DE CODE_ABANDON
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ABEND_ABANDON            PIC X(1) VALUE 'A'.
         05 AIDA_ABANDON             PIC X(1) VALUE 'K'.
         05 TACHE_ABANDON            PIC X(1) VALUE 'T'.
         05 CICS_ABANDON             PIC X(1) VALUE 'C'.
         05 DL1_ABANDON              PIC X(1) VALUE 'D'.
       01  FIL_LIGNE                 PIC X(16) VALUE '**** LIGNE *****'.
       01  LIGNE                     PIC X(131).
       01  ALPHABETIQUE              PIC X(26) VALUE
                                           'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
       01  KW--ALPHANUMERIC          PIC X(36) VALUE
                                 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.
       01  NUMERIQUE                 PIC X(10) VALUE '0123456789'.
       01  NETNAME                   PIC X(8).
       01  EIB_RCODE                 PIC X(6).
       01  FIL_EIB.
      * X'00'*/
         05 FIL_NORMAL               PIC 1(8) BIT VALUE B'00000000'.
      * X'81'*/
         05 FIL_NOTFND               PIC 1(8) BIT VALUE B'10000001'.
      * X'0F'*/
         05 FIL_ENDFILE              PIC 1(8) BIT VALUE B'00001111'.
      * X'04'*/
         05 FIL_MAPFAIL              PIC 1(8) BIT VALUE B'00000100'.
      * X'01'*/
         05 FIL_ENDDATA              PIC 1(8) BIT VALUE B'00000001'.
      * X'02'*/
         05 FIL_QIDERR               PIC 1(8) BIT VALUE B'00000010'.
      * X'01'*/
         05 FIL_QZERO                PIC 1(8) BIT VALUE B'00000001'.
      * X'01'*/
         05 FIL_ITEMERR              PIC 1(8) BIT VALUE B'00000001'.
      * X'82'*/
         05 FIL_DUPREC               PIC 1(8) BIT VALUE B'10000010'.
         05 FIL_DUPKEY               PIC 1(8) BIT VALUE B'10000100'.
       01  Z_TIMER_DATJOU_FILE       PIC X(8).
       01  Z_TIMER_DATJOU            PIC X(8) VALUE '00/00/00'.
       01  Z_TIMER_TIMJOU            PIC X(9) VALUE '00H00M00S'.
       01  Z_TIMER_0HHMMSS           PIC 9(7).
       01  Z_CWA                     PIC X(100).
       01  Z_TCTUA                   PIC X(255).
       01  GRP--INIT_BMS1.
      * WORKAID        X'00' */
      * BRT_ALP_FSET   X'C9' */
      * BRT_NUM_FSET   X'D9' */
      * BRT_PRO_FSET   X'F9' */
      * DRK_ALP_FSET   X'4D' */
      * DRK_PRO_FSET   X'7D' */
      * DRK_PRO_RSET   X'7C' */
      * NOR_ALP_FSET   X'C5' */
      * NOR_NUM_FSET   X'D5' */
      * NOR_PRO_FSET   X'F5' */
      * WCC_SCREEN     X'C3' */
      * WCC_PRINTER    X'C8' */
      * SAUT_PAGE_EOM X'0C19'*/
      *        X'0C'         */
      *        X'19'         */
      * TYPE_TERMINAL   X'40'*/
      * MODELE_TERMINAL X'40'*/
      * CURSEUR  = -1        */
      *        X'FF'         */
         05 INIT_BMS1 OCCURS 18      PIC 1(8) BIT.
       01  GRP--INIT_BMS2.
      * TOUCHE_NULL     X'00'  */
      * TOUCHE_ENTER    X'7D'  */
      * TOUCHE_CLEAR    X'6D'  */
      * TOUCHE_PEN      X'7E'  */
      * TOUCHE_OPID     X'E6'  */
      * TOUCHE_MSRE     X'E7'  */
      * TOUCHE_STRF     X'C8'  */
      * TOUCHE_TRIG     X'7F'  */
      * TOUCHE_PA1      X'6C'  */
      * TOUCHE_PA2      X'6E'  */
      * TOUCHE_PA3      X'6B'  */
      * TOUCHE_PF1      X'F1'  */
      * TOUCHE_PF2      X'F2'  */
      * TOUCHE_PF3      X'F3'  */
      * TOUCHE_PF4      X'F4'  */
      * TOUCHE_PF5      X'F5'  */
      * TOUCHE_PF6      X'F6'  */
      * TOUCHE_PF7      X'F7'  */
      * TOUCHE_PF8      X'F8'  */
      * TOUCHE_PF9      X'F9'  */
      * TOUCHE_PF10     X'7A'  */
      * TOUCHE_PF11     X'7B'  */
      * TOUCHE_PF12     X'7C'  */
      * TOUCHE_PF13     X'C1'  */
      * TOUCHE_PF14     X'C2'  */
      * TOUCHE_PF15     X'C3'  */
      * TOUCHE_PF16     X'C4'  */
      * TOUCHE_PF17     X'C5'  */
      * TOUCHE_PF18     X'C6'  */
      * TOUCHE_PF19     X'C7'  */
      * TOUCHE_PF20     X'C8'  */
      * TOUCHE_PF21     X'C9'  */
      * TOUCHE_PF22     X'4A'  */
      * TOUCHE_PF23     X'4B'  */
      * TOUCHE_PF24     X'4C'  */
      * TERMINAL_ECRAN_91 X'91'*/
      * TERMINAL_ECRAN_99 X'99'*/
      * TERMINAL_IMPRIMANTE_93 X'93' '*/
      * TERMINAL_IMPRIMANTE_94 X'94' '*/
      * TERMINAL_IMPRIMANTE_9B X'9B' '*/
      * TERMINAL_IMPRIMANTE_9C X'9C' '*/
         05 INIT_BMS2 OCCURS 42      PIC 1(8) BIT.
       01  FIL_MAP                   PIC X(16) VALUE '*** MAP IG50 ***'.
       01  Z_MAP.
         05 DFHMS1                   PIC X(12).
      * MAP BMS
      *                                                               */
         05 MECRANL                  PIC S9(4) COMP-5.
         05 MECRANF                  PIC X(1).
         05 MECRANI                  PIC X(5).
      * APPLID CICS
      *                                                               */
         05 MCICSL                   PIC S9(4) COMP-5.
         05 MCICSF                   PIC X(1).
         05 MCICSI                   PIC X(8).
      * DATE JOUR
      *                                                               */
         05 MDATEL                   PIC S9(4) COMP-5.
         05 MDATEF                   PIC X(1).
         05 MDATEI                   PIC X(8).
      * SYSTEME DE SECURITE
      *                                                               */
         05 MSECURL                  PIC S9(4) COMP-5.
         05 MSECURF                  PIC X(1).
         05 MSECURI                  PIC X(3).
      * NETNAME
      *                                                               */
         05 MLIGNEL                  PIC S9(4) COMP-5.
         05 MLIGNEF                  PIC X(1).
         05 MLIGNEI                  PIC X(8).
      * HEURE
      *                                                               */
         05 MHEUREL                  PIC S9(4) COMP-5.
         05 MHEUREF                  PIC X(1).
         05 MHEUREI                  PIC X(8).
      * TERMINAL
      *                                                               */
         05 MTERMIL                  PIC S9(4) COMP-5.
         05 MTERMIF                  PIC X(1).
         05 MTERMII                  PIC X(4).
      * CHOIX OPTION
      *                                                               */
         05 MCHOIXL                  PIC S9(4) COMP-5.
         05 MCHOIXF                  PIC X(1).
         05 MCHOIXI                  PIC X(1).
      * MESSAGE
      *                                                               */
         05 MLIBERRL                 PIC S9(4) COMP-5.
         05 MLIBERRF                 PIC X(1).
         05 MLIBERRI                 PIC X(78).
         05 FILL0094                 PIC X(1).
       01  COM_IG50_LONG_COMMAREA    PIC S9(4) COMP-5 VALUE +1350.
       01  Z_COMMAREA                PIC X(1350).
       01  COMMET00_LONG             PIC S9(4) COMP-5 VALUE +396.
       01  COMMET00_ABAN             PIC X(396).
       01  Z_COMMAREA_LINK           PIC X(370).
       01  TSSCPLP                   POINTER.
       01  TSSPTR                    POINTER.
       01  SSASECU.
         05 NM_CA_SECU               PIC X(7) VALUE 'SSASECU'.
         05 LG_CA_SECU               PIC S9(4) COMP-5 VALUE 193.
         05 SECU_AUTORISATIONS.
           10 SECU_ENTREE.
             15 SECU_ID_TERM         PIC X(4).
             15 SECU_CD_TU_DEM       PIC X(4).
             15 SECU_FONCTION        PIC X(4).
           10 SECU_SORTIE.
             15 SECU_CD_RETOUR       PIC X(1).
             15 SECU_MSG_RETOUR      PIC X(60).
             15 SECU_GROUPE          PIC X(8).
             15 SECU_SOUS_GROUPE     PIC X(8).
             15 SECU_NM_UTILIS       PIC X(8).
             15 GRP--SECU_TABLE_FONC.
               20 SECU_TABLE_FONC OCCURS 20.
                 25 SECU_CD_FONC     PIC X(4).
         05 DLM_CA_SECU              PIC X(7) VALUE 'UCESASS'.
       01  Z_ERREUR.
         05 FIL_ERR00                PIC X(256).
         05 Z_ERREUR_TRACE_MESS      PIC X(80).
         05 Z_ERREUR_EIBLK           PIC X(66).
       01  FIL_ERR01.
         05 TRACE_MESSAGE_LONG       PIC S9(4) COMP-5 VALUE +84.
         05 TRACE_MESSAGE.
           10 FIL_ERR02              PIC X(1) VALUE ''.
           10 FIL_ERR03              PIC X(2) VALUE '$-'.
           10 FIL_ERR04              PIC X(1) VALUE ''.
           10 FIL_ERR05              PIC X(1) VALUE 'Y'.
           10 TRACE_MESSX.
             15 MESS1                PIC X(15) VALUE ''.
             15 MESS                 PIC X(64) VALUE ''.
       01  PTR_COMM                  POINTER.
       01  PTR_CWA                   POINTER.
       01  PTR_TWA                   POINTER.
       01  PTR_TCTUA                 POINTER.
       01  PTR_USER                  POINTER.
       01  SWAP_PTR_TS               POINTER.
       01  CWA_LONG                  PIC S9(4) COMP-5.
       01  TWA_LONG                  PIC S9(4) COMP-5.
       01  TCTUA_LONG                PIC S9(4) COMP-5.
       01  GFDX.
         05 GFDATA                   PIC X(1).
         05 GFJOUR                   PIC X(2).
         05 GFJJ REDEFINES GFJOUR    PIC 99.
         05 GFMOIS                   PIC X(2).
         05 GFMM REDEFINES GFMOIS    PIC 99.
         05 GFSIECLE                 PIC X(2).
         05 GFSS REDEFINES GFSIECLE  PIC 99.
         05 GFANNEE                  PIC X(2).
         05 GFAA REDEFINES GFANNEE   PIC 99.
         05 GFQNTA                   PIC 999.
         05 GFQNT0                   PIC 9(5).
         05 GFSMN                    PIC 9.
         05 GFAMJ                    PIC X(6).
         05 GFAAMMJJ REDEFINES GFAMJ PIC 9(6).
         05 GFSAMJ                   PIC X(8).
         05 GFSSAAMMJJ REDEFINES GFSAMJ PIC 9(8).
         05 GFVDAT                   PIC 1(1) BIT.
         05 GFBISS                   PIC 1(1) BIT.
         05 GFJMA_ED                 PIC X(8).
         05 GFJMSA_ED                PIC X(10).
         05 GFLMOISL                 PIC X(9).
         05 GFLMOISC                 PIC X(3).
         05 GFLJOUR                  PIC X(8).
       01  GFMESS                    PIC X(60).
       01  GRP--GFTABJ.
         05 GFTABJ OCCURS 12         PIC S9(4) COMP-5.
       01  GRP--GFTABLML.
         05 GFTABLML OCCURS 12       PIC X(9).
       01  GRP--GFTABLMC.
         05 GFTABLMC OCCURS 12       PIC X(3).
       01  GRP--GFTABLJ.
         05 GFTABLJ OCCURS 7         PIC X(8).
       01  FLAG1                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG2                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG3                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG4                     PIC 1(1) BIT SYNCHRONIZED.
       01  GFX0                      PIC S9(9) COMP-5.
       01  GFX1                      PIC S9(9) COMP-5.
       01  GFX2                      PIC S9(9) COMP-5.
       01  GFBJ                      PIC S9(9) COMP-5.
       01  GFBM                      PIC S9(9) COMP-5.
       01  GFBA                      PIC S9(9) COMP-5.
       01  GFBS                      PIC S9(9) COMP-5.
       01  GFBQTA                    PIC S9(9) COMP-5.
       01  GFBQT0                    PIC S9(9) COMP-5.
       01  I                         PIC S9(4) COMP-5.
       LINKAGE SECTION.
       01  PTR_PREF                  POINTER.
       01  SWAP_IDENT_TS             PIC X(8).
       01  NUM_4                     PIC 9(4).
       01  NUM_3                     PIC 9(3).
       01  NUM_2                     PIC 9(2).
       01  NUM_1                     PIC 9(1).
       01  REDEF_BINAIRE             PIC X(2).
       01  KW--STRING                PIC X(2000).
       01  EIB_CODE                  PIC X(1).
       01  FIL_EIBX.
         05 EIB_NORMAL               PIC X(1).
         05 EIB_NOTFND               PIC X(1).
         05 EIB_ENDFILE              PIC X(1).
         05 EIB_MAPFAIL              PIC X(1).
         05 EIB_ENDDATA              PIC X(1).
         05 EIB_QIDERR               PIC X(1).
         05 EIB_QZERO                PIC X(1).
         05 EIB_ITEMERR              PIC X(1).
         05 EIB_DUPREC               PIC X(1).
         05 EIB_DUPKEY               PIC X(1).
       01  Z_DATH2.
         05 DATH2_SS                 PIC X(2) VALUE '00'.
         05 DATH2_AA                 PIC X(2) VALUE '00'.
         05 DATH2_MM                 PIC X(2) VALUE '00'.
         05 DATH2_JJ                 PIC X(2) VALUE '00'.
       01  Z_DATH3.
         05 DATH3_JJ                 PIC X(2) VALUE '00'.
         05 DATH3_FIL1               PIC X(1) VALUE '/'.
         05 DATH3_MM                 PIC X(2) VALUE '00'.
         05 DATH3_FIL2               PIC X(1) VALUE '/'.
         05 DATH3_AA                 PIC X(2) VALUE '00'.
       01  Z_TIMER.
         05 TIME_HH                  PIC X(2) VALUE '00'.
         05 TIME_FIL1                PIC X(1) VALUE 'H'.
         05 TIME_MM                  PIC X(2) VALUE '00'.
         05 TIME_FIL2                PIC X(1) VALUE 'M'.
         05 TIME_SS                  PIC X(2) VALUE '00'.
         05 TIME_FIL3                PIC X(1) VALUE 'S'.
       01  GRP--Z_TIMER_TIMJOU_CAR.
         05 Z_TIMER_TIMJOU_CAR OCCURS 7 PIC X(1).
       01  TIMJOU_DARTY.
         05 FILER_JJ                 PIC 99.
         05 EIB_ANNEE                PIC 99.
         05 EIB_QQQ                  PIC 999.
       01  Z_CWA_DATJOU.
         05 Z_CWA_AA                 PIC X(2).
         05 FIL_CWA1                 PIC X(1).
         05 Z_CWA_MM                 PIC X(2).
         05 FIL_CWA2                 PIC X(1).
         05 Z_CWA_JJ                 PIC X(2).
       01  TCT_DARTY.
      * SIGNATURE-SECURITE  */
         05 TCT_SEC_SIG.
      *   .GROUPE           */
           10 TCT_SEC_GRP            PIC X(8).
      *   .SOUS-GROUPE      */
           10 TCT_SEC_SGR            PIC X(8).
      *   .UTILISATEUR      */
           10 TCT_SEC_UTI            PIC X(8).
         05 GRP--TCT_SEC_TBL.
      * TABLE DES .../...   */
           10 TCT_SEC_TBL OCCURS 20.
      * FONCTIONS ACCORDEES */
             15 TCT_SEC_FON          PIC X(4).
         05 TCT_SEC_FLR              PIC X(151).
       01  FIL_BMS1.
         05 WORKAID                  PIC X(1).
      * ****************************/
      * DEFINITION DES ATTRIBUTS  */
      * ****************************/
         05 BRT_ALP_FSET             PIC X(1).
         05 BRT_NUM_FSET             PIC X(1).
         05 BRT_PRO_FSET             PIC X(1).
         05 DRK_ALP_FSET             PIC X(1).
         05 DRK_PRO_FSET             PIC X(1).
         05 DRK_PRO_RSET             PIC X(1).
         05 NOR_ALP_FSET             PIC X(1).
         05 NOR_NUM_FSET             PIC X(1).
         05 NOR_PRO_FSET             PIC X(1).
      * WCC_SCREEN */
         05 WCC_SCREEN               PIC X(1).
      * WCC_PRINTER*/
         05 WCC_PRINTER              PIC X(1).
      * SAUT_PAGE_EOM*/
         05 SAUT_PAGE_EOM            PIC X(2).
      * *********************************************/
      * ATTENTION , LES DEUX ZONES CI-DESSOUS:     */
      * TYPE_TERMINAL ET MODELE_TERMINAL           */
      * DOIVENT RESTER GROUPEES, ET DANS CET ORDRE */
      * *********************************************/
         05 TERMCODE.
           10 TYPE_TERMINAL          PIC X(1).
           10 MODELE_TERMINAL        PIC X(1).
         05 CURSEUR                  PIC S9(4) COMP-5.
       01  FIL_BMS2.
         05 TOUCHE_NULL              PIC X(1).
         05 TOUCHE_ENTER             PIC X(1).
         05 TOUCHE_CLEAR             PIC X(1).
         05 TOUCHE_PEN               PIC X(1).
         05 TOUCHE_OPID              PIC X(1).
         05 TOUCHE_MSRE              PIC X(1).
         05 TOUCHE_STRF              PIC X(1).
         05 TOUCHE_TRIG              PIC X(1).
         05 TOUCHE_PA1               PIC X(1).
         05 TOUCHE_PA2               PIC X(1).
         05 TOUCHE_PA3               PIC X(1).
         05 TOUCHE_PF1               PIC X(1).
         05 TOUCHE_PF2               PIC X(1).
         05 TOUCHE_PF3               PIC X(1).
         05 TOUCHE_PF4               PIC X(1).
         05 TOUCHE_PF5               PIC X(1).
         05 TOUCHE_PF6               PIC X(1).
         05 TOUCHE_PF7               PIC X(1).
         05 TOUCHE_PF8               PIC X(1).
         05 TOUCHE_PF9               PIC X(1).
         05 TOUCHE_PF10              PIC X(1).
         05 TOUCHE_PF11              PIC X(1).
         05 TOUCHE_PF12              PIC X(1).
         05 TOUCHE_PF13              PIC X(1).
         05 TOUCHE_PF14              PIC X(1).
         05 TOUCHE_PF15              PIC X(1).
         05 TOUCHE_PF16              PIC X(1).
         05 TOUCHE_PF17              PIC X(1).
         05 TOUCHE_PF18              PIC X(1).
         05 TOUCHE_PF19              PIC X(1).
         05 TOUCHE_PF20              PIC X(1).
         05 TOUCHE_PF21              PIC X(1).
         05 TOUCHE_PF22              PIC X(1).
         05 TOUCHE_PF23              PIC X(1).
         05 TOUCHE_PF24              PIC X(1).
         05 TERMINAL_ECRAN_91        PIC X(1).
         05 TERMINAL_ECRAN_99        PIC X(1).
         05 TERMINAL_IMPRIMANTE_93   PIC X(1).
         05 TERMINAL_IMPRIMANTE_94   PIC X(1).
         05 TERMINAL_IMPRIMANTE_9B   PIC X(1).
         05 TERMINAL_IMPRIMANTE_9C   PIC X(1).
         05 TERMINAL_IMPRIMANTE_B6   PIC X(1).
       01  Z_TWA_PRINTER.
      * NOM DU TERMINAL */
         05 Z_TWA_PRT                PIC X(4).
      * FILLER          */
         05 Z_TWA_TYP                PIC X(1).
         05 Z_TWA_RET                PIC X(1).
       01  EIG50O.
         05 DFHMS2                   PIC X(12).
      * MAP BMS
      *                                                               */
         05 DFHMS3                   PIC S9(4) COMP-5.
         05 MECRANA                  PIC X(1).
         05 MECRANO                  PIC X(5).
      * APPLID CICS
      *                                                               */
         05 DFHMS4                   PIC S9(4) COMP-5.
         05 MCICSA                   PIC X(1).
         05 MCICSO                   PIC X(8).
      * DATE JOUR
      *                                                               */
         05 DFHMS5                   PIC S9(4) COMP-5.
         05 MDATEA                   PIC X(1).
         05 MDATEO                   PIC X(8).
      * SYSTEME DE SECURITE
      *                                                               */
         05 DFHMS6                   PIC S9(4) COMP-5.
         05 MSECURA                  PIC X(1).
         05 MSECURO                  PIC X(3).
      * NETNAME
      *                                                               */
         05 DFHMS7                   PIC S9(4) COMP-5.
         05 MLIGNEA                  PIC X(1).
         05 MLIGNEO                  PIC X(8).
      * HEURE
      *                                                               */
         05 DFHMS8                   PIC S9(4) COMP-5.
         05 MHEUREA                  PIC X(1).
         05 MHEUREO                  PIC X(8).
      * TERMINAL
      *                                                               */
         05 DFHMS9                   PIC S9(4) COMP-5.
         05 MTERMIA                  PIC X(1).
         05 MTERMIO                  PIC X(4).
      * CHOIX OPTION
      *                                                               */
         05 DFHMS10                  PIC S9(4) COMP-5.
         05 MCHOIXA                  PIC X(1).
         05 MCHOIXO                  PIC X(1).
      * MESSAGE
      *                                                               */
         05 DFHMS11                  PIC S9(4) COMP-5.
         05 MLIBERRA                 PIC X(1).
         05 MLIBERRO                 PIC X(78).
         05 FILL0094                 PIC X(1).
       01  COM_AIDA.
         05 COM_CODERR               PIC X(6).
         05 COM_PGMPRC               PIC X(8).
         05 Z_COMMAREA_NOM_MAP       PIC X(8).
         05 Z_COMMAREA_NOM_MAPSET    PIC X(8).
         05 Z_COMMAREA_NOM_TACHE     PIC X(4).
         05 Z_COMMAREA_LONG          PIC S9(4) COMP-5.
         05 COM_FILLER               PIC X(64).
       01  Z_COMMAREA_PREF           PIC X(1350).
       01  Z_COMMAREA_IG50.
         05 PREFIX_AIDA              PIC X(100).
      * COMMUN A DARTY : RESTE A DEFINIR */
         05 PREFIX_DARTY.
      * CICS APPLID  */
           10 COM_CICS_APPLID        PIC X(8).
      * LIGNE VTAM   */
           10 COM_CICS_NETNAM        PIC X(8).
      * LIGNE VTAM   */
           10 COM_CICS_SECURI        PIC X(3).
           10 COM_FILLER_DARTY       PIC X(81).
      * A DEVELOPPER POUR LA TRANSACTION */
      * LONGUEUR 1150  */
         05 COMMAREA_USER.
           10 COM_ENTETE.
      * IMAGE ECRAN          */
             15 COM_IMAECR           PIC X(5).
      * NUMERO TERMINAL      */
             15 COM_TERMIN           PIC X(4).
      * DATE TRAITEMENT      */
             15 COM_DATE             PIC X(8).
      * PAGE ECRAN           */
             15 COM_PAGE             PIC X(3).
           10 COM_ECRAN.
      * CODE FONCTION        */
             15 COM_FONCTI           PIC X(3).
      * NOM ETAT             */
             15 COM_NOMETA           PIC X(6).
      * DATE CLE ETAT        */
             15 COM_DATETA           PIC X(6).
      * DESTINATION CLE ETAT */
             15 COM_DESETA           PIC X(9).
      * DOCUMENTAT. CLE ETAT */
             15 COM_DOCETA           PIC X(15).
      * TYPE RACINE          */
             15 COM_TYPRAC           PIC X(1).
      * TITRE ETAT           */
             15 COM_TITETA           PIC X(40).
      * CONFIGURATION        */
             15 COM_CONFIG           PIC X(1).
      * TYPE DE PAPIER       */
             15 COM_TYPPAP           PIC X(20).
      * TYPE IMPRESSION:U,M  */
             15 COM_TYPIMP           PIC X(1).
      * NOMBRE DE LIGNES/PAGE*/
             15 COM_LIGPAG           PIC X(2).
      * NOMB. LIGN. EDITABLES*/
             15 COM_LIGEDI           PIC X(2).
      * NOMB. COLONNES/LIGNE */
             15 COM_COLLIG           PIC X(3).
      * TYPE DE PURGE 1 OU 2 */
             15 COM_TYPPUR           PIC X(1).
      * TYPE RATTACH. S OU P */
             15 COM_TYPRAT           PIC X(1).
      * NOMB. JOURS APRES IMP*/
             15 COM_JOUAPIMP         PIC X(2).
      * NOMB. JOURS APRES EDI*/
             15 COM_JOUAPEDI         PIC X(2).
             15 GRP--COM_MOIDESIRE.
               20 COM_MOIDESIRE OCCURS 12.
      * NOMB. JOURS APRES IMP*/
                 25 COM_MOIJOUAPIMP  PIC X(2).
      * NOMB. JOURS APRES EDI*/
                 25 COM_MOIJOUAPEDI  PIC X(2).
      * DESTINATION          */
             15 COM_DESTIN           PIC X(9).
             15 COM_SIGNATURE.
      * GROUPE               */
               20 COM_GROUPE         PIC X(8).
      * SOUS GROUPE          */
               20 COM_SOUSGR         PIC X(8).
      * UTILISATEUR          */
               20 COM_UTILIS         PIC X(8).
      * ECRAN DEMANDEUR      */
             15 COM_ECRDEM           PIC X(4).
      * IMPRIMANTE ASSOCIEE 1*/
             15 COM_IMPASS1          PIC X(4).
      * IMPRIMANTE ASSOCIEE 2*/
             15 COM_IMPASS2          PIC X(4).
      * LIBELLE DESTINATION  */
             15 COM_LIBDES           PIC X(30).
      * ETAT REPRIS O OU N   */
             15 COM_REPRISE          PIC X(1).
      * ETAT ANNULE          */
             15 COM_ANNULAT          PIC X(1).
      * MESSAGE COMMUNICATION*/
             15 COM_MESSAGE          PIC X(78).
      * ETAPE DU TRAITEMENT  */
             15 COM_COUP             PIC X(1).
      * EXIST. SEG. DEPENDANT*/
             15 COM_DEPENDANT        PIC X(1).
      * PAGE COURANTE        */
             15 COM_PAGSAV           PIC S9(4) COMP-5.
      * PAGE MAXIMUM         */
             15 COM_PAGMAX           PIC S9(4) COMP-5.
      * PROTOCOLE T L A      */
             15 COM_PROTOC           PIC X(1).
             15 COM_ECRAN            PIC X(1).
      * ITEM TS              */
             15 COM_ITEMTS           PIC S9(4) COMP-5.
             15 COM_FILLER           PIC X(773).
       01  COMMET00_STRU.
      * 1 = EXEC CICS     */
         05 ORI_TYP_ERR              PIC X(1).
      * 2 = EXEC DL1      */
      * DATE              */
         05 EIB_DATE                 PIC X(10).
      * HEURE             */
         05 EIB_TIME                 PIC X(10).
      * TERMINAL          */
         05 EIB_TRMID                PIC X(4).
      * NOM DE LA TACHE   */
         05 EIB_TRNID                PIC X(4).
      * NOM DU PROGRAMME  */
         05 ORI_NOM_PGM              PIC X(7).
      * NOM LIGNE VTAM    */
         05 ORI_NOM_LIG              PIC X(8).
      * DATE COMPILATION  */
         05 ORI_DAT_CPL              PIC X(18).
      * EXEC CICS ==> INFORMATIONS BLOCK EIB */
         05 EIB_SEL_INF.
      * CODE FONCTION     */
           10 EIB_FN                 PIC X(2).
      * CODE REPONSE      */
           10 EIB_RCODE              PIC X(6).
      * DATA-SET NAME     */
           10 EIB_DS                 PIC X(8).
      * EXEC DL1 ==> RESULTATS DU SCHEDULING CALL */
         05 DL1_PCB_CAL.
           10 DL1_FCTR               PIC X(1).
           10 DL1_DLTR               PIC X(1).
      * EXEC DL1 ==> MASQUE PCB DL1 EN COURS */
         05 DL1_PCB_MAS.
      * NOM DU DBD        */
           10 DL1_DBD_NOM            PIC X(8).
      * NIVEAU DU SEGMENT */
           10 DL1_SEG_LEV            PIC X(2).
      * CODE RETOUR       */
           10 DL1_COD_RET            PIC X(2).
      * PROCESSING OPTION */
           10 DL1_PRO_OPT            PIC X(4).
      * NOM DU SEGMENT    */
           10 DL1_SEG_NAM            PIC X(8).
      * LONGUEUR CLE CONC */
           10 DL1_LON_CLE            PIC S9(9) COMP-5.
      * NOMBRE SEGMENTS   */
           10 DL1_NBR_SEG            PIC S9(9) COMP-5.
      * CLE CONCATENEE    */
           10 DL1_CLE_VAL            PIC X(264).
         05 COMMET00_STRU            PIC X(3).
      * EXEC DL1 ==> AUTRES INFORMATIONS DL1 */
         05 DL1_INF_SUP.
      * FONCTION DL1      */
           10 DL1_TYP_FUN            PIC X(4).
      * NUMERO PCB        */
           10 DL1_NUM_PCB            PIC 9.
      * NOM DU PSB        */
           10 DL1_PSB_NOM            PIC X(8).
           10 DL1_NBR_PAR            PIC S9(9) COMP-5.
       01  TSSCPL.
      * HEADER TCPLV4L4 */
         05 TSSHEAD                  PIC X(8).
      * CLASS NAME      */
         05 TSSCLASS                 PIC X(8).
      * RESOURCE NAME   */
         05 TSSRNAME                 PIC X(44).
      * PRIV. PGM. NAME */
         05 TSSPPGM                  PIC X(8).
      * ACCESS LEVEL    */
         05 TSSACC                   PIC X(8).
      * RETURN CODE     */
         05 TSSRC                    PIC S9(4) COMP-5.
      * STATUS CODE     */
         05 TSSSTAT                  PIC S9(4) COMP-5.
      * RET CODE IN CHAR*/
         05 TSSCRC                   PIC X(2).
      * STAT CODE CHAR  */
         05 TSSCSTAT                 PIC X(2).
      * ADDRESS OF ACEE */
         05 TSSACEE                  PIC S9(9) COMP-5.
      * VOLUME SERIAL  */
         05 TSSVOL                   PIC X(6).
      * LOG 'Y' OR 'N'  */
         05 TSSLOG                   PIC X(1).
      * RESERVED        */
         05 TSSRSVD                  PIC X(2).
      * TSSDRC      FIXED BIN (1),              DRC FOR DUF CALL*/
      * DRC FOR DUF CALL*/
         05 TSSDRC                   PIC X(1).
      * RESERVED        */
         05 TSSRSVD2                 PIC X(16).
         05 TSSRTN                   PIC X(1024).
      * TSSCLASS=FACLIST*/
       01  TSSRTN_F.
         05 GRP--TSSFACX.
      * FACILITY LIST   */
           10 TSSFACX OCCURS 128     PIC X(8).
       01  TSSRTN_A.
      * ACID ACIDNAME   */
         05 TSSACIDA                 PIC X(8).
      * FACILITY NAME   */
         05 TSSFAC                   PIC X(8).
      * CURRENT MODE    */
         05 TSSMODE                  PIC X(8).
      * ACID TYPE       */
         05 TSSTYPE                  PIC X(8).
      * TERMINAL NAME   */
         05 TSSTERM                  PIC X(8).
      * SYSTEM NAME     */
         05 TSSSYS                   PIC X(8).
      * ACID FULLNAME   */
         05 TSSACIDF                 PIC X(32).
      * DEPT ACIDNAME   */
         05 TSSDEPTA                 PIC X(8).
      * DEPT FULLNAME   */
         05 TSSDEPTF                 PIC X(32).
      * DIV  ACIDNAME   */
         05 TSSDIVA                  PIC X(8).
      * DIV  FULLNAME   */
         05 TSSDIVF                  PIC X(32).
      * ZONE ACIDNAME   */
         05 TSSZONEA                 PIC X(8).
      * ZONE FULLNAME   */
         05 TSSZONEF                 PIC X(32).
         05 TSSFILL                  PIC X(824).
      * T SSCLASS=RESLIST */
       01  TSSRTN_R.
      * RESLIST ENTRY COUNT*/
         05 TSSRL_COUNT              PIC S9(9) COMP-5.
         05 TSSRL_DATA               PIC X(1020).
      * VAR LEN STRUCTURE  */
       01  TSSRL_ENTRY.
      * LEN RESOURCE NAME  */
         05 TSSRL_RESLEN             PIC S9(4) COMP-5.
      * NONGENERIC = '00'X */
         05 TSSRL_GENERIC            PIC X(1).
      * GENERIC    = '80'X */
         05 TSSRL_RESNAME            PIC X(1).
       01  TSS_AREA_INST.
      * CODE PRINTER TCT */
         05 TSS_PTR1_COD             PIC X(4).
         05 TSS_PTR1_TYP             PIC X(1).
       01  MESS_ABCODE               PIC X(4).
       01  TRACE_MESS                PIC X(79).
       01  LINK_CWA                  PIC X(512).
       01  LINK_TWA                  PIC X(500).
       01  LINK_TCTUA                PIC X(255).
       01  LINK_USER                 PIC X(4000).
       01  LINK_TS.
         05 LINK_TS_MAP              PIC X(2000).
         05 LINK_TS_COMMAREA         PIC X(4096).
       01  GFJMSA                    PIC X(8).
       PROCEDURE DIVISION USING PTR_PREF.
       BEGIN--MAIN SECTION.
           SET ADDRESS OF Z_COMMAREA_PREF TO PTR_PREF
           SET ADDRESS OF SWAP_IDENT_TS TO ADDRESS OF
                                                    SWAP_FILLER_IDENT_TS
           SET ADDRESS OF NUM_4 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_3 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_2 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_1 TO ADDRESS OF NUM_5
           SET ADDRESS OF REDEF_BINAIRE TO ADDRESS OF BINAIRE
           MOVE ALL ' ' TO MW-REPEAT-CHAR--131
           MOVE MW-REPEAT-CHAR--131 TO LIGNE
           SET ADDRESS OF EIB_CODE TO ADDRESS OF EIB_RCODE
           SET ADDRESS OF FIL_EIBX TO ADDRESS OF FIL_EIB
           MOVE ALL '0' TO MW-REPEAT-CHAR--8
           MOVE MW-REPEAT-CHAR--8 TO Z_TIMER_DATJOU_FILE
           SET ADDRESS OF Z_DATH2 TO ADDRESS OF Z_TIMER_DATJOU_FILE
           SET ADDRESS OF Z_DATH3 TO ADDRESS OF Z_TIMER_DATJOU
           SET ADDRESS OF Z_TIMER TO ADDRESS OF Z_TIMER_TIMJOU
           SET ADDRESS OF Z_TIMER_TIMJOU_CAR TO ADDRESS OF
                                                         Z_TIMER_0HHMMSS
           SET ADDRESS OF TIMJOU_DARTY TO ADDRESS OF Z_TIMER_0HHMMSS
           SET ADDRESS OF Z_CWA_DATJOU TO ADDRESS OF Z_CWA
           SET ADDRESS OF TCT_DARTY TO ADDRESS OF Z_TCTUA
           MOVE B'00000000' TO INIT_BMS1(1)
           MOVE B'11001001' TO INIT_BMS1(2)
           MOVE B'11011001' TO INIT_BMS1(3)
           MOVE B'11111001' TO INIT_BMS1(4)
           MOVE B'01001101' TO INIT_BMS1(5)
           MOVE B'01111101' TO INIT_BMS1(6)
           MOVE B'01111100' TO INIT_BMS1(7)
           MOVE B'11000101' TO INIT_BMS1(8)
           MOVE B'11010101' TO INIT_BMS1(9)
           MOVE B'11110101' TO INIT_BMS1(10)
           MOVE B'11000011' TO INIT_BMS1(11)
           MOVE B'11001000' TO INIT_BMS1(12)
           MOVE B'00001100' TO INIT_BMS1(13)
           MOVE B'00011001' TO INIT_BMS1(14)
           MOVE B'01000000' TO INIT_BMS1(15)
           MOVE B'01000000' TO INIT_BMS1(16)
           MOVE B'11111111' TO INIT_BMS1(17)
           MOVE B'11111111' TO INIT_BMS1(18)
           SET ADDRESS OF FIL_BMS1 TO ADDRESS OF GRP--INIT_BMS1
           MOVE B'00000000' TO INIT_BMS2(1)
           MOVE B'01111101' TO INIT_BMS2(2)
           MOVE B'01101101' TO INIT_BMS2(3)
           MOVE B'01111110' TO INIT_BMS2(4)
           MOVE B'11100110' TO INIT_BMS2(5)
           MOVE B'11100111' TO INIT_BMS2(6)
           MOVE B'11001000' TO INIT_BMS2(7)
           MOVE B'01111111' TO INIT_BMS2(8)
           MOVE B'01101100' TO INIT_BMS2(9)
           MOVE B'01101110' TO INIT_BMS2(10)
           MOVE B'01101011' TO INIT_BMS2(11)
           MOVE B'11110001' TO INIT_BMS2(12)
           MOVE B'11110010' TO INIT_BMS2(13)
           MOVE B'11110011' TO INIT_BMS2(14)
           MOVE B'11110100' TO INIT_BMS2(15)
           MOVE B'11110101' TO INIT_BMS2(16)
           MOVE B'11110110' TO INIT_BMS2(17)
           MOVE B'11110111' TO INIT_BMS2(18)
           MOVE B'11111000' TO INIT_BMS2(19)
           MOVE B'11111001' TO INIT_BMS2(20)
           MOVE B'01111010' TO INIT_BMS2(21)
           MOVE B'01111011' TO INIT_BMS2(22)
           MOVE B'01111100' TO INIT_BMS2(23)
           MOVE B'11000001' TO INIT_BMS2(24)
           MOVE B'11000010' TO INIT_BMS2(25)
           MOVE B'11000011' TO INIT_BMS2(26)
           MOVE B'11000100' TO INIT_BMS2(27)
           MOVE B'11000101' TO INIT_BMS2(28)
           MOVE B'11000110' TO INIT_BMS2(29)
           MOVE B'11000111' TO INIT_BMS2(30)
           MOVE B'11001000' TO INIT_BMS2(31)
           MOVE B'11001001' TO INIT_BMS2(32)
           MOVE B'01001010' TO INIT_BMS2(33)
           MOVE B'01001011' TO INIT_BMS2(34)
           MOVE B'01001100' TO INIT_BMS2(35)
           MOVE B'10010001' TO INIT_BMS2(36)
           MOVE B'10011001' TO INIT_BMS2(37)
           MOVE B'10010011' TO INIT_BMS2(38)
           MOVE B'10010100' TO INIT_BMS2(39)
           MOVE B'10011011' TO INIT_BMS2(40)
           MOVE B'10011100' TO INIT_BMS2(41)
           MOVE B'10110110' TO INIT_BMS2(42)
           SET ADDRESS OF FIL_BMS2 TO ADDRESS OF GRP--INIT_BMS2
           SET ADDRESS OF Z_TWA_PRINTER TO ADDRESS OF LINK_TWA
           SET ADDRESS OF EIG50O TO ADDRESS OF Z_MAP
           SET ADDRESS OF COM_AIDA TO ADDRESS OF Z_COMMAREA
           SET ADDRESS OF Z_COMMAREA_IG50 TO ADDRESS OF Z_COMMAREA
           SET ADDRESS OF COMMET00_STRU TO ADDRESS OF COMMET00_ABAN
           SET ADDRESS OF TSSCPL TO TSSCPLP
           SET ADDRESS OF TSSRTN_F TO ADDRESS OF TSSRTN
           SET ADDRESS OF TSSRTN_A TO ADDRESS OF TSSRTN
           SET ADDRESS OF TSSRTN_R TO ADDRESS OF TSSRTN
           SET ADDRESS OF TSSRL_ENTRY TO TSSRL_PTR
           SET ADDRESS OF TSS_AREA_INST TO TSSPTR
           SET ADDRESS OF MESS_ABCODE TO ADDRESS OF MESS
           SET ADDRESS OF TRACE_MESS TO ADDRESS OF TRACE_MESSX
           SET ADDRESS OF LINK_CWA TO PTR_CWA
           SET ADDRESS OF LINK_TWA TO PTR_TWA
           SET ADDRESS OF LINK_TCTUA TO PTR_TCTUA
           SET ADDRESS OF LINK_USER TO PTR_USER
           SET ADDRESS OF LINK_TS TO SWAP_PTR_TS
           SET ADDRESS OF GFJMSA TO ADDRESS OF GFJOUR
           MOVE 31 TO GFTABJ(1)
           MOVE 28 TO GFTABJ(2)
           MOVE 31 TO GFTABJ(3)
           MOVE 30 TO GFTABJ(4)
           MOVE 31 TO GFTABJ(5)
           MOVE 30 TO GFTABJ(6)
           MOVE 31 TO GFTABJ(7)
           MOVE 31 TO GFTABJ(8)
           MOVE 30 TO GFTABJ(9)
           MOVE 31 TO GFTABJ(10)
           MOVE 30 TO GFTABJ(11)
           MOVE 31 TO GFTABJ(12)
           MOVE 'JANVIER  ' TO GFTABLML(1)
           MOVE 'FEVRIER  ' TO GFTABLML(2)
           MOVE 'MARS     ' TO GFTABLML(3)
           MOVE 'AVRIL    ' TO GFTABLML(4)
           MOVE 'MAI      ' TO GFTABLML(5)
           MOVE 'JUIN     ' TO GFTABLML(6)
           MOVE 'JUILLET  ' TO GFTABLML(7)
           MOVE 'AOUT     ' TO GFTABLML(8)
           MOVE 'SEPTEMBRE' TO GFTABLML(9)
           MOVE 'OCTOBRE  ' TO GFTABLML(10)
           MOVE 'NOVEMBRE ' TO GFTABLML(11)
           MOVE 'DECEMBRE ' TO GFTABLML(12)
           MOVE 'JAN' TO GFTABLMC(1)
           MOVE 'FEV' TO GFTABLMC(2)
           MOVE 'MAR' TO GFTABLMC(3)
           MOVE 'AVR' TO GFTABLMC(4)
           MOVE 'MAI' TO GFTABLMC(5)
           MOVE 'JUN' TO GFTABLMC(6)
           MOVE 'JUL' TO GFTABLMC(7)
           MOVE 'AOU' TO GFTABLMC(8)
           MOVE 'SEP' TO GFTABLMC(9)
           MOVE 'OCT' TO GFTABLMC(10)
           MOVE 'NOV' TO GFTABLMC(11)
           MOVE 'DEC' TO GFTABLMC(12)
           MOVE 'LUNDI   ' TO GFTABLJ(1)
           MOVE 'MARDI   ' TO GFTABLJ(2)
           MOVE 'MERCREDI' TO GFTABLJ(3)
           MOVE 'JEUDI   ' TO GFTABLJ(4)
           MOVE 'VENDREDI' TO GFTABLJ(5)
           MOVE 'SAMEDI  ' TO GFTABLJ(6)
           MOVE 'DIMANCHE' TO GFTABLJ(7)
           CONTINUE.
       BEGIN--PROGRAM SECTION.
           PERFORM MODULE_ENTREE THRU E--MODULE_ENTREE
           IF FONCTION > '00' AND FONCTION < '99' THEN
             PERFORM MODULE_TRAITEMENT THRU E--MODULE_TRAITEMENT
           END-IF
           PERFORM MODULE_SORTIE THRU E--MODULE_SORTIE
           CONTINUE.
           COPY CBGOBACK.
       MODULE_ENTREE.
           PERFORM INIT_USER THRU E--INIT_USER
           PERFORM INIT_ADDRESS THRU E--INIT_ADDRESS
           PERFORM TIG50_APPLID_CICS THRU E--TIG50_APPLID_CICS
           PERFORM RECEPTION_MESSAGE THRU E--RECEPTION_MESSAGE
           CONTINUE.
       E--MODULE_ENTREE.
           EXIT.
       INIT_USER.
           SET ADDRESS OF KW--STRING TO ADDRESS OF Z_MAP
           COMPUTE MW--STG = LENGTH OF Z_MAP
           COMPUTE MW--STG = LENGTH OF Z_MAP
           MOVE LOW-VALUE TO KW--STRING(1:MW--STG)
           MOVE 'EIG50' TO NOM_MAP
           MOVE 'EIG50' TO NOM_MAPSET
           MOVE 'TIG50' TO NOM_PROG
           MOVE 'IG50' TO NOM_TACHE
           MOVE 'NON' TO DEBUGGIN
           COMPUTE LONG_COMMAREA = COM_IG50_LONG_COMMAREA
           IF EIBCALEN NOT = 0 THEN
             MOVE Z_COMMAREA_PREF TO Z_COMMAREA
           ELSE
             INITIALIZE Z_COMMAREA
           END-IF
           CONTINUE.
       E--INIT_USER.
           EXIT.
       TIG50_APPLID_CICS.
           IF COM_CODERR = '' THEN
             PERFORM SORTIE_LEVEL_MAX THRU E--SORTIE_LEVEL_MAX
           END-IF
           EXEC CICS                  ASSIGN
                 APPLID  (       COM_CICS_APPLID)
                 NETNAME (       COM_CICS_NETNAM)
                 NOHANDLE END-EXEC
           MOVE COM_CICS_NETNAM TO NETNAME
           MOVE 'TSS' TO COM_CICS_SECURI
           CONTINUE.
       E--TIG50_APPLID_CICS.
           EXIT.
       INIT_ADDRESS.
           IF EIBTRMID NOT = ' ' AND EIBTRMID NOT = LOW-VALUE THEN
             EXEC CICS                                 ASSIGN TERMCODE (
                                                      TERMCODE) NOHANDLE
             END-EXEC
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           EXEC CICS                          ASSIGN CWALENG   (
                                                               CWA_LONG)
                                  TWALENG   (       TWA_LONG)
                                  TCTUALENG (       TCTUA_LONG)
                                  NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           IF CWA_LONG > 0 THEN
             EXEC CICS                          ADDRESS CWA    (
                                                       PTR_CWA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_CWA TO PTR_CWA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
             MOVE LINK_CWA TO Z_CWA
           ELSE
             MOVE 'CWA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           IF TWA_LONG > 0 THEN
             EXEC CICS                          ADDRESS TWA    (
                                                       PTR_TWA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_TWA TO PTR_TWA
             SET ADDRESS OF Z_TWA_PRINTER TO ADDRESS OF LINK_TWA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           ELSE
             MOVE 'TWA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           IF TCTUA_LONG > 0 THEN
             EXEC CICS                          ADDRESS TCTUA  (
                                                     PTR_TCTUA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_TCTUA TO PTR_TCTUA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
             MOVE LINK_TCTUA TO Z_TCTUA
           ELSE
             MOVE 'TCTUA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           MOVE EIBDATE TO Z_TIMER_0HHMMSS
           MOVE EIB_ANNEE TO GFANNEE
           COMPUTE GFQNTA = EIB_QQQ
           MOVE '2' TO GFDATA
           PERFORM METDATE THRU E--METDATE
           MOVE '20' TO DATH2_SS
           MOVE GFANNEE TO DATH2_AA
           IF DATH2_AA > '50' THEN
             MOVE '19' TO DATH2_SS
           END-IF
           MOVE GFMOIS TO DATH2_MM
           MOVE GFJOUR TO DATH2_JJ
           MOVE GFANNEE TO DATH3_AA
           MOVE GFMOIS TO DATH3_MM
           MOVE GFJOUR TO DATH3_JJ
           MOVE EIBTIME TO Z_TIMER_0HHMMSS
           STRING Z_TIMER_TIMJOU_CAR(2) Z_TIMER_TIMJOU_CAR(3) DELIMITED
                                                    BY SIZE INTO TIME_HH
           STRING Z_TIMER_TIMJOU_CAR(4) Z_TIMER_TIMJOU_CAR(5) DELIMITED
                                                    BY SIZE INTO TIME_MM
           STRING Z_TIMER_TIMJOU_CAR(6) Z_TIMER_TIMJOU_CAR(7) DELIMITED
                                                    BY SIZE INTO TIME_SS
           CONTINUE.
       E--INIT_ADDRESS.
           EXIT.
       RECEPTION_MESSAGE.
           IF EIBCALEN = 0 THEN
             MOVE CODE_TRAITEMENT_AUTOMATIQUE TO FONCTION
             GO TO E--RECEPTION_MESSAGE
           END-IF
           IF EIBTRNID NOT = NOM_TACHE THEN
             MOVE CODE_TRAITEMENT_AUTOMATIQUE TO FONCTION
             GO TO E--RECEPTION_MESSAGE
           END-IF
           PERFORM RECEIVE_MAP THRU E--RECEIVE_MAP
           MOVE EIBAID TO WORKAID
           IF WORKAID = TOUCHE_PF4 THEN
             MOVE CODE_LEVEL_MAX TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_PF3 THEN
             MOVE CODE_LEVEL_SUP TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_ENTER THEN
             MOVE CODE_TRAITEMENT_NORMAL TO FONCTION
           END-IF
           IF ETAT_ECRAN = ECRAN_MAPFAIL THEN
             MOVE CODE_LEVEL_SUP TO FONCTION
           END-IF
           CONTINUE.
       E--RECEPTION_MESSAGE.
           EXIT.
       RECEIVE_MAP.
           EXEC CICS                          RECEIVE
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    INTO   (       Z_MAP)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE = EIB_NORMAL THEN
             MOVE '0' TO CODE_RETOUR
           ELSE
             IF EIB_CODE = EIB_MAPFAIL THEN
               MOVE '1' TO ETAT_ECRAN
             ELSE
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           CONTINUE.
       E--RECEIVE_MAP.
           EXIT.
       MODULE_TRAITEMENT.
           IF FONCTION = TRAITEMENT_NORMAL THEN
             PERFORM MODULE_TRAITEMENT_NORMAL THRU
                                             E--MODULE_TRAITEMENT_NORMAL
           END-IF
           IF FONCTION = TRAITEMENT_AUTOMATIQUE THEN
             PERFORM MODULE_TRAITEMENT_AUTOMATIQUE THRU
                                        E--MODULE_TRAITEMENT_AUTOMATIQUE
           END-IF
           CONTINUE.
       E--MODULE_TRAITEMENT.
           EXIT.
       MODULE_TRAITEMENT_NORMAL.
           PERFORM CONTROLE_SYNTAXE THRU E--CONTROLE_SYNTAXE
           CONTINUE.
       E--MODULE_TRAITEMENT_NORMAL.
           EXIT.
       CONTROLE_SYNTAXE.
           EVALUATE MCHOIXI
             WHEN '1'
               CONTINUE
             WHEN '2'
               CONTINUE
             WHEN '3'
               CONTINUE
             WHEN '4'
               CONTINUE
             WHEN '5'
               CONTINUE
             WHEN '6'
               CONTINUE
             WHEN OTHER
               MOVE '1' TO KONTROL
               COMPUTE MCHOIXL = CURSEUR
               MOVE BRT_ALP_FSET TO MCHOIXA
               STRING NOM_TACHE ' * ' 'OPTION INVALIDE' DELIMITED BY
                                                      SIZE INTO MLIBERRI
           END-EVALUATE
           CONTINUE.
       E--CONTROLE_SYNTAXE.
           EXIT.
       MODULE_TRAITEMENT_AUTOMATIQUE.
           PERFORM REMPLISSAGE_FORMAT_ECRAN THRU
                                             E--REMPLISSAGE_FORMAT_ECRAN
           CONTINUE.
       E--MODULE_TRAITEMENT_AUTOMATIQUE.
           EXIT.
       REMPLISSAGE_FORMAT_ECRAN.
           PERFORM REMPLISSAGE_ZONES_OBLIGATOIRES THRU
                                       E--REMPLISSAGE_ZONES_OBLIGATOIRES
           CONTINUE.
       E--REMPLISSAGE_FORMAT_ECRAN.
           EXIT.
       REMPLISSAGE_ZONES_OBLIGATOIRES.
           MOVE NOM_MAP TO MECRANI
           MOVE COM_CICS_APPLID TO MCICSI
           MOVE EIBTRMID TO MTERMII
           MOVE COM_CICS_NETNAM TO MLIGNEI
           MOVE COM_CICS_SECURI TO MSECURI
           MOVE Z_TIMER_DATJOU TO MDATEI
           CONTINUE.
       E--REMPLISSAGE_ZONES_OBLIGATOIRES.
           EXIT.
       MODULE_SORTIE.
           STRING TIME_HH 'H ' TIME_MM 'MN' DELIMITED BY SIZE INTO
                                                                 MHEUREI
           IF FONCTION = TRAITEMENT_AUTOMATIQUE THEN
             PERFORM SORTIE_AFFICHAGE_FORMAT THRU
                                              E--SORTIE_AFFICHAGE_FORMAT
           END-IF
           IF KONTROL NOT = OK THEN
             PERFORM SORTIE_ERREUR THRU E--SORTIE_ERREUR
           END-IF
           IF FONCTION = TRAITEMENT_NORMAL THEN
             PERFORM SORTIE_SUITE THRU E--SORTIE_SUITE
           END-IF
           IF FONCTION = LEVEL_SUP THEN
             PERFORM SORTIE_LEVEL_SUPERIEUR THRU
                                               E--SORTIE_LEVEL_SUPERIEUR
           END-IF
           IF FONCTION = LEVEL_MAX THEN
             PERFORM SORTIE_LEVEL_MAX THRU E--SORTIE_LEVEL_MAX
           END-IF
           IF FONCTION = ERREUR_MANIPULATION THEN
             PERFORM SORTIE_ERREUR_MANIP THRU E--SORTIE_ERREUR_MANIP
           END-IF
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS
           PERFORM ABANDON_TACHE THRU E--ABANDON_TACHE
           CONTINUE.
       E--MODULE_SORTIE.
           EXIT.
       SORTIE_AFFICHAGE_FORMAT.
           COMPUTE MCHOIXL = CURSEUR
           PERFORM SEND_MAP THRU E--SEND_MAP
           MOVE NOM_TACHE TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_AFFICHAGE_FORMAT.
           EXIT.
       SORTIE_ERREUR.
           PERFORM SEND_MAP_ERREUR THRU E--SEND_MAP_ERREUR
           MOVE EIBTRNID TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_ERREUR.
           EXIT.
       SORTIE_SUITE.
           EVALUATE MCHOIXI
             WHEN '1'
               MOVE 'TIG51' TO NOM_PROG_XCTL
             WHEN '2'
               MOVE 'TIG52' TO NOM_PROG_XCTL
             WHEN '3'
               MOVE 'TIG53' TO NOM_PROG_XCTL
             WHEN '4'
               MOVE 'TIG5A' TO NOM_PROG_XCTL
             WHEN '5'
               MOVE 'TIG56' TO NOM_PROG_XCTL
             WHEN '6'
               GO TO SORTIE_TIGP0
             WHEN OTHER
               MOVE ' TIG50 * ERREUR OPTION SORTIE-SUITE' TO MESS
               PERFORM ABANDON_TACHE THRU E--ABANDON_TACHE
           END-EVALUATE
           MOVE NOM_PROG TO COM_PGMPRC
           PERFORM XCTL_PROG_COMMAREA THRU E--XCTL_PROG_COMMAREA
           CONTINUE.
       SORTIE_TIGP0.
           MOVE 'TIGP0' TO NOM_PROG_XCTL
           EXEC CICS                  SYNCPOINT  END-EXEC
           EXEC CICS                  XCTL
                 PROGRAM  (       NOM_PROG_XCTL)
                 NOHANDLE  END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SORTIE_SUITE.
           EXIT.
       SORTIE_LEVEL_SUPERIEUR.
           MOVE '====  FIN DE LA TRANSACTION IG50  ====' TO LIGNE
           PERFORM SEND_TEXT_ERASE THRU E--SEND_TEXT_ERASE
           PERFORM RETOUR THRU E--RETOUR
           CONTINUE.
       E--SORTIE_LEVEL_SUPERIEUR.
           EXIT.
       SORTIE_LEVEL_MAX.
           MOVE '====  FIN DE LA TRANSACTION IG50  ====' TO LIGNE
           PERFORM SEND_TEXT_ERASE THRU E--SEND_TEXT_ERASE
           PERFORM RETOUR THRU E--RETOUR
           CONTINUE.
       E--SORTIE_LEVEL_MAX.
           EXIT.
       SORTIE_ERREUR_MANIP.
           STRING NOM_TACHE ' * ' 'ERREUR MANIPULATION' DELIMITED BY
                                                      SIZE INTO MLIBERRI
           PERFORM SEND_MAP_NO_ERASE THRU E--SEND_MAP_NO_ERASE
           MOVE NOM_TACHE TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_ERREUR_MANIP.
           EXIT.
       RETOUR_COMMAREA.
           EXEC CICS                          RETURN
                                    TRANSID  (       NOM_TACHE_RETOUR)
                                    COMMAREA (       Z_COMMAREA)
                                    LENGTH   (       LONG_COMMAREA)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--RETOUR_COMMAREA.
           EXIT.
       RETOUR.
           MOVE LOW-VALUE TO LINK_TCTUA(1:255)
           EXEC CICS                          RETURN
                                    NOHANDLE
           END-EXEC
           CONTINUE.
       E--RETOUR.
           EXIT.
       SEND_MAP_ERREUR.
           EXEC CICS                          SEND
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    FROM   (       Z_MAP)
                                    ERASE
                                    CURSOR
                                    ALARM
                                    FRSET
                                    FREEKB
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_MAP_ERREUR.
           EXIT.
       SEND_MAP_NO_ERASE.
           EXEC CICS                          SEND
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    FROM   (       Z_MAP)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_MAP_NO_ERASE.
           EXIT.
       SEND_MAP.
           EXEC CICS                          SEND
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    FROM   (       Z_MAP)
                                    ERASE
                                    CURSOR
                                     NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_MAP.
           EXIT.
       SEND_TEXT_ERASE.
           EXEC CICS                          SEND TEXT
                                     ERASE
                                     FROM   (       LIGNE)
                                     LENGTH (       131)
                                     NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_TEXT_ERASE.
           EXIT.
       XCTL_PROG_COMMAREA.
           EXEC CICS                       SYNCPOINT
           END-EXEC
           EXEC CICS                          XCTL
                                    PROGRAM  (       NOM_PROG_XCTL)
                                    COMMAREA (       Z_COMMAREA)
                                    LENGTH   (       LONG_COMMAREA)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--XCTL_PROG_COMMAREA.
           EXIT.
       LINK_PROG.
           EXEC CICS                  LINK  PROGRAM  (
                                                          NOM_PROG_LINK)
                       COMMAREA (       Z_COMMAREA_LINK)
                       LENGTH   (       LONG_COMMAREA_LINK)
                       NOHANDLE END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           CONTINUE.
       E--LINK_PROG.
           EXIT.
       METDATE.
           INITIALIZE GFMESS
           MOVE B'0' TO FLAG1
           MOVE B'0' TO FLAG2
           MOVE B'0' TO FLAG3
           MOVE B'0' TO FLAG4
           MOVE '1234569' TO VERIFY-MASK
           CALL SP-VERIFY USING LENGTH OF GFDATA GFDATA LENGTH OF
                           '1234569' '1234569' VERIFY-MASK RETURN-VERIFY
           IF RETURN-VERIFY NOT = 0 THEN
             MOVE 'CODE APPEL INVALIDE' TO GFMESS
             GO TO GFERDAT
           END-IF
           IF GFDATA = '1' THEN
             MOVE B'1' TO FLAG1
           END-IF
           IF GFDATA = '2' THEN
             MOVE B'1' TO FLAG2
           END-IF
           IF GFDATA = '3' THEN
             MOVE B'1' TO FLAG3
           END-IF
           IF GFDATA = '4' THEN
             MOVE B'1' TO FLAG4
           END-IF
           IF GFDATA = '5' THEN
             MOVE GFJMA_ED(1:2) TO GFJOUR
             MOVE GFJMA_ED(4:2) TO GFMOIS
             MOVE GFJMA_ED(7:2) TO GFANNEE
             MOVE B'1' TO FLAG1
           END-IF
           IF GFDATA = '6' THEN
             MOVE GFJMSA_ED(1:2) TO GFJOUR
             MOVE GFJMSA_ED(4:2) TO GFMOIS
             MOVE GFJMSA_ED(7:2) TO GFSIECLE
             MOVE GFJMSA_ED(9:2) TO GFANNEE
             MOVE B'1' TO FLAG4
           END-IF
           IF GFDATA = '9' THEN
             MOVE FUNCTION CURRENT-DATE(7:2) TO GFJOUR
             MOVE FUNCTION CURRENT-DATE(5:2) TO GFMOIS
             MOVE FUNCTION CURRENT-DATE(3:2) TO GFANNEE
             MOVE FUNCTION CURRENT-DATE(1:2) TO GFSIECLE
             MOVE B'1' TO FLAG4
           END-IF
           IF FLAG1 OR FLAG2 THEN
             IF GFAA < 50 THEN
               MOVE 20 TO GFSS
             ELSE
               MOVE 19 TO GFSS
             END-IF
           END-IF
           IF FLAG1 THEN
             MOVE B'0' TO FLAG1
             MOVE B'1' TO FLAG4
           END-IF
           IF FLAG4 OR FLAG2 THEN
             IF GFSS < 19 OR GFSS > 20 THEN
               MOVE 'SIECLE INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFANNEE GFANNEE LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'ANNEE NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             COMPUTE GFBS = GFSS
             COMPUTE GFBA = GFAA
             PERFORM BISSEXTILE THRU E--BISSEXTILE
           END-IF
           IF FLAG4 THEN
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFMOIS GFMOIS LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'MOIS NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFJOUR GFJOUR LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'JOUR NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             COMPUTE GFBM = GFMM
             COMPUTE GFBJ = GFJJ
             IF GFBM < 1 OR GFBM > 12 THEN
               MOVE 'MOIS HORS LIMITES' TO GFMESS
               GO TO GFERDAT
             END-IF
             IF GFBJ < 1 OR GFBJ > GFTABJ(GFBM) THEN
               MOVE 'JOUR HORS LIMITES' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG2 THEN
             COMPUTE GFBQTA = GFQNTA
             IF GFBISS THEN
               MOVE 366 TO GFX1
             ELSE
               MOVE 365 TO GFX1
             END-IF
             IF GFBQTA < 1 OR GFBQTA > GFX1 THEN
               MOVE 'QUANTIEME CALENDAIRE INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG3 THEN
             COMPUTE GFBQT0 = GFQNT0
             IF GFBQT0 < 1 OR GFBQT0 > 73049 THEN
               MOVE 'QUANTIEME STANDARD INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG4 THEN
             COMPUTE GFBQTA = GFBJ
             COMPUTE ENDVAL--5 = GFBM - 1
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > ENDVAL--5
               COMPUTE GFBQTA = GFBQTA + GFTABJ(I)
             END-PERFORM
           END-IF
           IF FLAG2 OR FLAG4 THEN
             COMPUTE GFX0 = GFBA + (GFBS * 100) - 1900
             COMPUTE GFX1 = GFX0 * 365
             COMPUTE GFX2 = (GFX0 - 1) / 4
             COMPUTE GFBQT0 = GFX1 + GFX2 + GFBQTA
           END-IF
           IF FLAG3 THEN
             COMPUTE GFX1 = (GFBQT0 * 4) / 1461
             COMPUTE GFBS = (GFX1 / 100) + 19
             COMPUTE GFBA = GFX1 - ((GFBS - 19) * 100)
             COMPUTE GFX0 = (((GFX1 * 1461) + 3) / 4) - 1
             COMPUTE GFBQTA = GFBQT0 - GFX0
             PERFORM BISSEXTILE THRU E--BISSEXTILE
           END-IF
           IF FLAG2 OR FLAG3 THEN
             COMPUTE GFX1 = GFBQTA
             PERFORM VARYING GFBM FROM 1 BY 1 UNTIL NOT (GFX1 > 0)
               COMPUTE GFBJ = GFX1
               COMPUTE GFX1 = GFX1 - GFTABJ(GFBM)
             END-PERFORM
             COMPUTE GFBM = GFBM - 1
           END-IF
           COMPUTE GFSMN = FUNCTION MOD (GFBQT0 + 6 7) + 1
           MOVE B'1' TO GFVDAT
           COMPUTE GFJJ = GFBJ
           COMPUTE GFMM = GFBM
           COMPUTE GFAA = GFBA
           COMPUTE GFSS = GFBS
           COMPUTE GFQNTA = GFBQTA
           COMPUTE GFQNT0 = GFBQT0
           STRING GFANNEE GFMOIS GFJOUR DELIMITED BY SIZE INTO GFAMJ
           STRING GFSIECLE GFANNEE GFMOIS GFJOUR DELIMITED BY SIZE INTO
                                                                  GFSAMJ
           STRING GFJOUR '/' GFMOIS '/' GFANNEE DELIMITED BY SIZE INTO
                                                                GFJMA_ED
           STRING GFJOUR '/' GFMOIS '/' GFSIECLE GFANNEE DELIMITED BY
                                                     SIZE INTO GFJMSA_ED
           MOVE GFTABLML(GFBM) TO GFLMOISL
           MOVE GFTABLMC(GFBM) TO GFLMOISC
           MOVE GFTABLJ(GFBJ) TO GFLJOUR
           GO TO E--METDATE
           CONTINUE.
       GFERDAT.
           MOVE B'0' TO GFVDAT
           GO TO E--METDATE
           CONTINUE.
       E--METDATE.
           EXIT.
       BISSEXTILE.
           IF GFBA NOT = 0 OR FUNCTION MOD (GFBS 10) = 0 THEN
             IF FUNCTION MOD (GFBA 4) = 0 THEN
               MOVE B'1' TO GFBISS
             ELSE
               MOVE B'0' TO GFBISS
             END-IF
           END-IF
           IF GFBISS THEN
             MOVE 29 TO GFTABJ(2)
           ELSE
             MOVE 28 TO GFTABJ(2)
           END-IF
           CONTINUE.
       E--BISSEXTILE.
           EXIT.
       ABANDON_TACHE.
           PERFORM ABANDON_ABEND THRU E--ABANDON_ABEND
           CONTINUE.
       E--ABANDON_TACHE.
           EXIT.
       ABANDON_ABEND.
           PERFORM ABANDON_CAID THRU E--ABANDON_CAID
           CONTINUE.
       E--ABANDON_ABEND.
           EXIT.
       ABANDON_CAID.
           PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           CONTINUE.
       E--ABANDON_CAID.
           EXIT.
       ABANDON_AIDA.
           MOVE 'ABANDON-TACHE' TO MESS1
           GO TO ABANDON_ABANDON
           PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           CONTINUE.
       E--ABANDON_AIDA.
           EXIT.
       ABANDON_CICS.
           INITIALIZE COMMET00_ABAN
           MOVE 'ABANDON-CICS' TO MESS1
           MOVE '1' TO ORI_TYP_ERR
           MOVE Z_TIMER_DATJOU TO EIB_DATE
           MOVE Z_TIMER_TIMJOU TO EIB_TIME
           MOVE EIBTRMID TO EIB_TRMID
           MOVE EIBTRNID TO EIB_TRNID
           MOVE NOM_PROG TO ORI_NOM_PGM
           MOVE NETNAME TO ORI_NOM_LIG
           MOVE Z_WHEN_COMPILED TO ORI_DAT_CPL
           MOVE EIBFN TO EIB_FN
           MOVE EIBRCODE TO EIB_RCODE
           MOVE EIBDS TO EIB_DS
           EXEC CICS                     LINK PROGRAM  (       'TET00')
                           COMMAREA (       COMMET00_ABAN)
                           LENGTH   (       COMMET00_LONG)
                           NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             GO TO ABEND_AIDA
           END-IF
           GO TO ABANDON_ABANDON
           PERFORM SEND_MESSAGE_ABANDON THRU E--SEND_MESSAGE_ABANDON
           CONTINUE.
       E--ABANDON_CICS.
           EXIT.
       SEND_MESSAGE_ABANDON.
           CONTINUE.
       ABANDON_ABANDON.
           EVALUATE TYPE_TERMINAL WHEN TERMINAL_ECRAN_91
             WHEN TERMINAL_ECRAN_99
               EXEC CICS                                         SEND
                                FROM    (       TRACE_MESSAGE)
                                LENGTH  (       TRACE_MESSAGE_LONG)
                                CTLCHAR (       WCC_SCREEN)
                                NOHANDLE
               END-EXEC WHEN TERMINAL_IMPRIMANTE_93 WHEN
               TERMINAL_IMPRIMANTE_94 WHEN TERMINAL_IMPRIMANTE_9B WHEN
                                                  TERMINAL_IMPRIMANTE_9C
             WHEN TERMINAL_IMPRIMANTE_B6
               EXEC CICS                                         SEND
                                FROM    (       TRACE_MESSAGE)
                                LENGTH  (       TRACE_MESSAGE_LONG)
                                CTLCHAR (       WCC_PRINTER)
                                NOHANDLE
               END-EXEC
             WHEN OTHER
               CONTINUE
           END-EVALUATE
           IF DEBUGGIN = 'OUI' THEN
             MOVE 1 TO BINAIRE
             EXEC CICS                                         RECEIVE
                                          INTO     (       Z_MAP)
                                          LENGTH   (       BINAIRE)
                                          NOHANDLE
             END-EXEC
           END-IF
           CONTINUE.
       ABEND_AIDA.
           MOVE LOW-VALUE TO LINK_TCTUA(1:255)
           EXEC CICS                         ABEND
                          CANCEL
                          NOHANDLE
           END-EXEC
           CONTINUE.
       E--SEND_MESSAGE_ABANDON.
           EXIT.
       END--MAIN.
           EXIT.
       END PROGRAM TIG50.
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
