      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     THLP1.                                   
       AUTHOR. PROJET DSA0.                                                     
      *               HELP                                                      
      *---------------------------------------------------------------*         
      * MAQUETTE DU CODE PROJET AIDA : DSA0                           *         
E0452 ******************************************************************        
      * DE02005 26/09/08 SUPPORT EVOLUTION D004238                              
      *                  MESSAGE CORRECT SI AIDE NON SAISISSABLE                
      ******************************************************************        
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *==============================================================           
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
       01  WS.                                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  J                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  J                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  P                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  P                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  D                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  F                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  F                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  L                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L1                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  L1                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L2                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  L2                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  M                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  M                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  C                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  C                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IO                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  IO                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LO                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LO                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  JB                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  JB                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  DEB                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  DEB                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  SUR                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  SUR                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  IMAP                      PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  IMAP                      PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ADMAP                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  ADMAP                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  ADA.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ADE                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
               10  ADE                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD1                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD1                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD2                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD2                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  AD3                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD3                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  AD4                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD4                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  IB                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  IB                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JB    *    05  WZERO                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  WZERO                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  ZCOMPA.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ZCOMP                     PIC S9(4)    COMP.                 
      *--                                                                       
               10  ZCOMP                     PIC S9(4) COMP-5.                  
      *}                                                                        
JB         05  ZHWA.                                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ZHW                       PIC S9(4)    COMP.                 
      *--                                                                       
               10  ZHW                       PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LMAP                      PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LMAP                      PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LG-SEND                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LG-SEND                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ITEM-TS                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  ITEM-TS                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TS-LENGTH                 PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  TS-LENGTH                 PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-DEB                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-DEB                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-FIN                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-FIN                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-LIG                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-LIG                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-HAUT                    PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-HAUT                    PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-BAS                     PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-BAS                     PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LG-ECRAN                  PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LG-ECRAN                  PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-POSCHOIX                PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-POSCHOIX                PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SF-SET                    PIC X        VALUE X'1D'.              
      *--                                                                       
           05  SF-SET                    PIC X        VALUE X'1D'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SBA-SET                   PIC X        VALUE X'11'.              
      *--                                                                       
           05  SBA-SET                   PIC X        VALUE X'11'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  IC-SET                    PIC X        VALUE X'13'.              
      *--                                                                       
           05  IC-SET                    PIC X        VALUE X'13'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  PT-SET                    PIC X        VALUE X'05'.              
      *--                                                                       
           05  PT-SET                    PIC X        VALUE X'09'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  RA-SET                    PIC X        VALUE X'3C'.              
      *--                                                                       
           05  RA-SET                    PIC X        VALUE X'14'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  EAU-SET                   PIC X        VALUE X'12'.              
      *--                                                                       
           05  EAU-SET                   PIC X        VALUE X'12'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SFE-SET                   PIC X        VALUE X'29'.              
      *--                                                                       
           05  SFE-SET                   PIC X        VALUE X'89'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SA-SET                    PIC X        VALUE X'28'.              
      *--                                                                       
           05  SA-SET                    PIC X        VALUE X'88'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  MF-SET                    PIC X        VALUE X'2C'.              
      *--                                                                       
           05  MF-SET                    PIC X        VALUE X'8C'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  FATT-SET                  PIC X        VALUE X'C0'.              
      *--                                                                       
           05  FATT-SET                  PIC X        VALUE X'E9'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  HILI-SET                  PIC X        VALUE X'41'.              
      *--                                                                       
           05  HILI-SET                  PIC X        VALUE X'A0'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  COLOR-SET                 PIC X        VALUE X'42'.              
      *--                                                                       
           05  COLOR-SET                 PIC X        VALUE X'E2'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  PS-SET                    PIC X        VALUE X'43'.              
      *--                                                                       
           05  PS-SET                    PIC X        VALUE X'E4'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  APL-SET                   PIC X        VALUE X'08'.              
      *--                                                                       
           05  APL-SET                   PIC X        VALUE X'97'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CLH                       PIC X        VALUE X'A2'.              
      *--                                                                       
           05  CLH                       PIC X        VALUE X'73'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CLV                       PIC X        VALUE X'85'.              
      *--                                                                       
           05  CLV                       PIC X        VALUE X'65'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CHG                       PIC X        VALUE X'C5'.              
      *--                                                                       
           05  CHG                       PIC X        VALUE X'45'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CHD                       PIC X        VALUE X'D5'.              
      *--                                                                       
           05  CHD                       PIC X        VALUE X'4E'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CBG                       PIC X        VALUE X'C4'.              
      *--                                                                       
           05  CBG                       PIC X        VALUE X'44'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CBD                       PIC X        VALUE X'D4'.              
      *--                                                                       
           05  CBD                       PIC X        VALUE X'4D'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CFB                       PIC X        VALUE X'BA'.              
      *--                                                                       
           05  CFB                       PIC X        VALUE X'AC'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  CFH                       PIC X        VALUE X'BB'.              
      *--                                                                       
           05  CFH                       PIC X        VALUE X'7C'.              
      *}                                                                        
           05  W-COLOR                   PIC X        VALUE SPACES.             
           05  W-HILI                    PIC X        VALUE SPACES.             
           05  WS-COLOR                  PIC X        VALUE SPACES.             
           05  WS-HILI                   PIC X        VALUE SPACES.             
           05  WS-ATTR                   PIC X        VALUE SPACES.             
           05  DFH-UNP                   PIC X        VALUE ' '.                
           05  DFH-ASK                   PIC X        VALUE '0'.                
           05  DFH-ASK-BR                PIC X        VALUE '8'.                
           05  DFH-DEFAULT               PIC X        VALUE LOW-VALUE.          
      *                                                                         
           05  FLAG-POSIT-CURS       PIC X(1).                                  
             88  POSIT-CURS-OK                 VALUE 'O'.                       
             88  POSIT-CURS-KO                 VALUE 'N'.                       
           05  TAB-ADDR.                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'40C1C2C3C4C5C6C7C8C94A4B4C4D4E4F'.                         
      *--                                                                       
                   X'20414243444546474849B02E3C282B21'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'50D1D2D3D4D5D6D7D8D95A5B5C5D5E5F'.                         
      *--                                                                       
                   X'264A4B4C4D4E4F505152A7242A293B5E'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'6061E2E3E4E5E6E7E8E96A6B6C6D6E6F'.                         
      *--                                                                       
                   X'2D2F535455565758595AF92C255F3E3F'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'F0F1F2F3F4F5F6F7F8F97A7B7C7D7E7F'.                         
      *--                                                                       
                   X'303132333435363738393AA3E0273D22'.                         
      *}                                                                        
           05  FILLER      REDEFINES TAB-ADDR.                                  
               10  T-ADDR                PIC X        OCCURS 64.                
           05  NOM-TS-CMA.                                                      
               10  FILLER                PIC X(4)     VALUE 'HELP'.             
               10  TS-CMA-TRMID          PIC X(4)     VALUE SPACES.             
           05  NOM-TS-DATA.                                                     
               10  FILLER                PIC X(4)     VALUE 'HLP0'.             
               10  TS-DATA-TRMID         PIC X(4)     VALUE SPACES.             
           05  Z-MESSERR                 PIC X(79)    VALUE SPACES.             
       01  ZIO-ECRAN                PIC X(4096)       VALUE SPACES.             
JB... *01  TS-DATA.                                                             
      *    05  FILLER      OCCURS 1.                                            
      *        10  FILLER      OCCURS 20.                                       
      ****     10  FILLER      OCCURS 40.                                       
      *            15  TS-DATA-SAISISSABLE       PIC X.                         
      *            15  TS-DATA-KEY               PIC X(50).                     
      *            15  TS-DATA-DATA              PIC X(50).                     
      *            15  TS-DATA-DATA-LIB         OCCURS 8.                       
...JB *              20  TS-DATA-LIBELLE         PIC X(20).                     
JB*   *01  TS-DATA.                                                             
      *    05  FILLER      OCCURS 2.                                            
      *        10  FILLER      OCCURS 40.                                       
      *            15  TS-DATA-KEY               PIC X(50).                     
*JB   *            15  TS-DATA-DATA              PIC X(50).                     
JB...  01  TS-DATA.                                                             
           05  FILLER      OCCURS 1.                                            
               10  FILLER      OCCURS 10.                                       
                   15  TS-DATA-SAISISSABLE       PIC X.                         
                   15  TS-DATA-KEY               PIC X(50).                     
                   15  TS-DATA-DATA              PIC X(50).                     
       01  NOM-TS-LIB.                                                          
           05  FILLER                PIC X(4)     VALUE 'HLPL'.                 
           05  TS-LIB-TRMID          PIC X(4)     VALUE SPACES.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-LIB-LONG                 PIC S9(04) COMP VALUE +1600.             
      *--                                                                       
       01  TS-LIB-LONG                 PIC S9(04) COMP-5 VALUE +1600.           
      *}                                                                        
       01  TS-LIBELLE.                                                          
...JB      05  TS-DATA-LIBELLE           PIC X(1600).                           
           COPY TSHELP.                                                         
           COPY SYKWDIV0.                                                       
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION  DES ZONES D'INTERFACE DB2                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *    COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
           COPY SYKWZCMD.                                                       
           COPY SYKWECRA.                                                       
      * -AIDA *********************************************************         
      *   ZONES GENERALES OBLIGATOIRES                                *         
      *****************************************************************         
           COPY  SYKWEIB0.                                                      
      * -AIDA *********************************************************         
      *  ZONES DE COMMAREA                                                      
      *****************************************************************         
           COPY  COMMHELP.                                                      
           COPY  SYKWCOMM.                                                      
      * -AIDA *********************************************************         
      *       * ZONE BUFFER D'ENTREE-SORTIE                                     
      *****************************************************************         
           COPY  SYKWZINO.                                                      
      * -AIDA *********************************************************         
      *       * ZONE DE GESTION DES ERREURS                                     
      *****************************************************************         
           COPY  SYKWERRO.                                                      
      * -AIDA *********************************************************         
      *       * ZONES DE GESTION DES FICHIERS                                   
      *****************************************************************         
      * -AIDA *********************************************************         
      *                                                               *         
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   *         
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   *         
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          *         
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          *         
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          *         
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    *         
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    *         
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 4096 DEPENDING ON EIBCALEN.                  
           COPY  SYKLINKB.                                                      
       01  TS-HLP-COMMAREA  PIC X(4096).                                        
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           - MODULE DE BASE DE LA TRANSACTION HLP1 -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-HLP1                    SECTION.                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      *                                                                         
           IF TRAITEMENT                                                        
              PERFORM MODULE-TRAITEMENT                                         
           END-IF.                                                              
      *                                                                         
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-HLP1.    EXIT.                                                
       EJECT                                                                    
      * -AIDA *********************************************************         
      *                                                               *         
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        *         
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        *         
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                    -----------------------                    *         
      *                    -   MODULE D'ENTREE   -                    *         
      *                    -----------------------                    *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -      -            -                   -           *         
      *  ----------  --------  ----------  -----------------------    *         
      *  - HANDLE -  - USER -  - ADRESS -  - RECEPTION MESSAGE   -    *         
      *  ----------  --------  ----------  -----------------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       MODULE-ENTREE                   SECTION.                                 
      *---------------------------------------                                  
           PERFORM INIT-USER.                                                   
           PERFORM RECEPTION-MESSAGE.                                           
      *****************************************************************         
      *    INITIALISATION DES ZONES QUI NE PEUVENT PAS ETRE EN VALUE  *         
      *****************************************************************         
      *---------------------------------------                                  
       INIT-USER                       SECTION.                                 
      *---------------------------------------                                  
           MOVE 'THLP1'             TO NOM-PROG.                                
           MOVE 'HLP1'              TO NOM-TACHE.                               
      *                                                                         
           MOVE EIBTRMID            TO TS-MAP-TRMID.                            
           MOVE EIBTRMID            TO TS-CMA-TRMID.                            
           MOVE EIBTRMID            TO TS-DATA-TRMID.                           
           MOVE EIBTRMID            TO TS-LIB-TRMID.                            
           IF EIBCALEN NOT = ZERO                                               
              MOVE DFHCOMMAREA         TO Z-COMMAREA                            
           END-IF.                                                              
           MOVE LENGTH OF Z-COMMAREA   TO LONG-COMMAREA                         
           PERFORM READ-TS-MAP.                                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASSIGN COLOR(W-COLOR)                                      
      *                     HILIGHT(W-HILI)                                     
      *                     TERMCODE(TERMCODE)                                  
      *                     NOHANDLE                                            
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS ASSIGN COLOR(W-COLOR)                                      
                            HILIGHT(W-HILI)                                     
                            TERMCODE(TERMCODE)                                  
                            NOHANDLE                                            
           END-EXEC.                                                            
      *}                                                                        
      *****************************************************************         
      * DETERMINATION DU TRAITEMENT EN FONCTION DE L'ENVIRONNEMENT    *         
      *****************************************************************         
      *------------------------------------                                     
       RECEPTION-MESSAGE            SECTION.                                    
      *------------------------------------                                     
           IF EIBCALEN = ZERO                                                   
           OR EIBTRNID NOT = NOM-TACHE                                          
              PERFORM TRAITEMENT-INITIAL                                        
              IF OK                                                             
                 MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                   
              END-IF                                                            
           ELSE                                                                 
              MOVE EIBAID       TO WORKAID                                      
              IF COMM-HELP-NBITEM > ZERO                                        
                 PERFORM RECEIVE-MESSAGE                                        
              ELSE                                                              
                 IF TOUCHE-PF5                                                  
                    MOVE 'THLP2'   TO  NOM-PROG-XCTL                            
                    MOVE NOM-PROG  TO  COM-PGMPRC                               
                    PERFORM XCTL-PROG-COMMAREA                                  
                 ELSE                                                           
                    MOVE CODE-LEVEL-MAX               TO FONCTION               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *------------------------------------                                     
       RECEIVE-MESSAGE              SECTION.                                    
      *------------------------------------                                     
           MOVE 4096                TO LG-ECRAN.                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RECEIVE INTO(Z-INOUT)                                      
      *                      LENGTH(LG-ECRAN)                                   
      *                      NOHANDLE                                           
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS RECEIVE INTO(Z-INOUT)                                      
                             LENGTH(LG-ECRAN)                                   
                             NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
      *----LEVEL-MAX KA                                                         
           IF TOUCHE-PF3                                                        
           OR TOUCHE-PF4                                                        
              MOVE CODE-LEVEL-MAX      TO FONCTION                              
           END-IF.                                                              
      *                                                                         
      *----SUITE 01                                                             
           IF TOUCHE-ENTER                                                      
           OR TOUCHE-PF7                                                        
           OR TOUCHE-PF8                                                        
           OR TOUCHE-PF12                                                       
              MOVE CODE-TRAITEMENT-NORMAL TO FONCTION                           
           END-IF.                                                              
      *----MAPFAIL                                                              
           IF TOUCHE-CLEAR                                                      
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                      
           END-IF.                                                              
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE TRAITEMENT                                          *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT          SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
               PERFORM MODULE-TRAITEMENT-NORMAL                                 
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
               PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                            
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT.      EXIT.                                        
      *****************************************************************         
      *****************************************************************         
      ***************** TRAITEMENT AUTOMATIQUE ************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ......  * TRAITEMENT AUTOMATIQUE  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   TRAITEMENT AUTOMATIQUE    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *                               -                               *         
      *                               -                               *         
      *                    ---------------------                      *         
      *                    - REMPLISSAGE ECRAN -                      *         
      *                    ---------------------                      *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                                  
      *                                                                         
           PERFORM REMPLISSAGE-FORMAT-ECRAN.                                    
      *                                                                         
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
       EJECT                                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   REMPLISSAGE DE L'ECRAN    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * ----------------------   -------------   -----------------    *         
      * - ZONES OBLIGATOIRES -   - PROTEGEES -   - NON PROTEGEES -    *         
      * ----------------------   -------------   -----------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * REMPLISSAGE DE LA MAP     *  HLP1    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-FORMAT-ECRAN      SECTION.                                   
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                             
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                
      *                                                                         
           MOVE ZERO                TO P.                                       
           IF COMM-HELP-NBITEM = ZERO                                           
              MOVE                                                              
              'Aucun �l�ment trouv� pour la s�lection / No item found'          
                                                            TO Z-MESSERR        
              MOVE 1                   TO KONTROL                               
           ELSE                                                                 
              PERFORM REMPLISSAGE-ZONES-NO-PROTEGEES                            
           END-IF.                                                              
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES OBLIGATOIRES        *  HLP1    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       REMPLISSAGE-ZONES-OBLIGATOIRES  SECTION.                                 
      *---------------------------------------                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES PROTEGEES           *  HLP1    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       REMPLISSAGE-ZONES-PROTEGEES     SECTION.                                 
      *---------------------------------------                                  
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES NON PROTEGEES       *  HLP1    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       REMPLISSAGE-ZONES-NO-PROTEGEES  SECTION.                                 
      *---------------------------------------                                  
           INITIALIZE COMM-HELP-PCURSEUR.                                       
           PERFORM READ-TS-DATA.                                                
           PERFORM READ-TS-LIBELLE.                                             
           MOVE COMM-HELP-POS-DEBUT TO ADE.                                     
           MOVE ZERO                TO P.                                       
           IF TS-APLTXT = HIGH-VALUE                                            
              MOVE DFH-DEFAULT         TO WS-HILI                               
              MOVE TS-YELLOW           TO WS-COLOR                              
              MOVE DFH-ASK             TO WS-ATTR                               
              PERFORM POSITIONNEMENT-CHAMP                                      
              COMPUTE P = P + 1                                                 
              MOVE APL-SET        TO ZIO-ECRAN (P:1)                            
              COMPUTE P = P + 1                                                 
              MOVE CHG            TO ZIO-ECRAN (P:1)                            
              COMPUTE LO = COMM-HELP-LENGTH - 3                                 
              PERFORM VARYING IO FROM 1 BY 1                                    
                      UNTIL   IO > LO                                           
                 COMPUTE P = P + 1                                              
                 MOVE APL-SET        TO ZIO-ECRAN (P:1)                         
                 COMPUTE P = P + 1                                              
                 MOVE CLH            TO ZIO-ECRAN (P:1)                         
              END-PERFORM                                                       
              COMPUTE P = P + 1                                                 
              MOVE APL-SET        TO ZIO-ECRAN (P:1)                            
              COMPUTE P = P + 1                                                 
              MOVE CHD            TO ZIO-ECRAN (P:1)                            
           ELSE                                                                 
              MOVE TS-REVRS            TO WS-HILI                               
              MOVE DFH-ASK             TO WS-ATTR                               
              IF TS-REVRS NOT = LOW-VALUE                                       
                 MOVE TS-BLUE             TO WS-COLOR                           
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 COMPUTE LO = COMM-HELP-LENGTH - 1                              
                 PERFORM VARYING IO FROM 1 BY 1                                 
                         UNTIL   IO > LO                                        
                    COMPUTE P = P + 1                                           
                    MOVE ' '         TO ZIO-ECRAN (P:1)                         
                 END-PERFORM                                                    
              ELSE                                                              
                 MOVE TS-YELLOW           TO WS-COLOR                           
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 COMPUTE P = P + 1                                              
                 MOVE '+'         TO ZIO-ECRAN (P:1)                            
                 COMPUTE LO = COMM-HELP-LENGTH - 3                              
                 PERFORM VARYING IO FROM 1 BY 1                                 
                         UNTIL   IO > LO                                        
                    COMPUTE P = P + 1                                           
                    MOVE '-'         TO ZIO-ECRAN (P:1)                         
                 END-PERFORM                                                    
                 COMPUTE P = P + 1                                              
                 MOVE '+'         TO ZIO-ECRAN (P:1)                            
              END-IF                                                            
           END-IF.                                                              
           COMPUTE ADE = ADE + (COMM-HELP-LENGTH - 1).                          
           IF W-COLOR = HIGH-VALUE                                              
           OR W-HILI  = HIGH-VALUE                                              
              PERFORM RESET-ATTRIBUTS-ECRAN                                     
           END-IF.                                                              
JB         MOVE  1  TO  DEB.                                                    
JB         MOVE  0  TO  SUR.                                                    
JB         SET   POSIT-CURS-KO  TO  TRUE .                                      
           PERFORM VARYING J FROM 1 BY 1                                        
                   UNTIL   J > COMM-HELP-OCCV                                   
              COMPUTE ADE = ADE + (TS-SCRNWD - COMM-HELP-LENGTH)                
              IF TS-APLTXT = HIGH-VALUE                                         
                 MOVE DFH-DEFAULT         TO WS-HILI                            
                 MOVE TS-YELLOW           TO WS-COLOR                           
                 MOVE DFH-ASK             TO WS-ATTR                            
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 COMPUTE P = P + 1                                              
                 MOVE APL-SET             TO ZIO-ECRAN (P:1)                    
                 COMPUTE P = P + 1                                              
                 IF J = 1 AND COMM-HELP-NOITEM > 1                              
                    MOVE CFH                 TO ZIO-ECRAN (P:1)                 
                 ELSE                                                           
                    IF J = COMM-HELP-OCCV AND                                   
                       COMM-HELP-NOITEM < COMM-HELP-NBITEM                      
                       MOVE CFB                 TO ZIO-ECRAN (P:1)              
                    ELSE                                                        
                       MOVE CLV                 TO ZIO-ECRAN (P:1)              
                    END-IF                                                      
                 END-IF                                                         
              ELSE                                                              
                 MOVE TS-REVRS            TO WS-HILI                            
                 MOVE DFH-ASK             TO WS-ATTR                            
                 IF TS-REVRS NOT = LOW-VALUE                                    
                    MOVE TS-BLUE             TO WS-COLOR                        
                    PERFORM POSITIONNEMENT-CHAMP                                
                    COMPUTE P = P + 1                                           
                    MOVE ' '                 TO ZIO-ECRAN (P:1)                 
                 ELSE                                                           
                    MOVE TS-YELLOW           TO WS-COLOR                        
                    PERFORM POSITIONNEMENT-CHAMP                                
                    COMPUTE P = P + 1                                           
                    MOVE '!'                 TO ZIO-ECRAN (P:1)                 
                 END-IF                                                         
              END-IF                                                            
              COMPUTE ADE = ADE + 1                                             
              PERFORM VARYING I FROM 1 BY 1                                     
                      UNTIL   I > COMM-HELP-OCCH                                
                 IF TS-DATA-KEY (I J) = SPACES                                  
JB               OR TS-DATA-SAISISSABLE (I J) = 'N'                             
                    MOVE DFH-ASK             TO WS-ATTR                         
                    MOVE TS-TURQ             TO WS-COLOR                        
                    MOVE DFH-DEFAULT         TO WS-HILI                         
                 ELSE                                                           
                    MOVE DFH-UNP             TO WS-ATTR                         
                    MOVE TS-YELLOW           TO WS-COLOR                        
                    MOVE TS-UNDLN            TO WS-HILI                         
                 END-IF                                                         
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 IF I = 1 AND J = 1                                             
                 AND TS-DATA-SAISISSABLE (I J) NOT = 'N'                        
                    ADD  1                   TO P                               
                    MOVE IC-SET              TO ZIO-ECRAN (P:1)                 
                    SET  POSIT-CURS-OK       TO TRUE                            
                 ELSE                                                           
                    IF ( TS-DATA-SAISISSABLE (I J) NOT = 'N'                    
                    AND  POSIT-CURS-KO )                                        
                    OR ( J = COMM-HELP-OCCV AND POSIT-CURS-KO )                 
JB2706                 ADD  1                   TO P                            
                       MOVE IC-SET              TO ZIO-ECRAN (P:1)              
                       SET  POSIT-CURS-OK       TO TRUE                         
                    END-IF                                                      
                 END-IF                                                         
                 ADD  1                   TO P                                  
                 MOVE ' '                 TO ZIO-ECRAN (P:1)                    
                 IF  TS-DATA-KEY (I J) NOT = SPACES                             
                    MOVE ADE                 TO COMM-HELP-POSCUR (I J)          
                 END-IF                                                         
                 COMPUTE ADE = ADE + 1                                          
                 MOVE DFH-ASK             TO WS-ATTR                            
                 MOVE TS-TURQ             TO WS-COLOR                           
                 MOVE DFH-DEFAULT         TO WS-HILI                            
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 ADD  1                   TO P                                  
JB*   *          MOVE TS-DATA-KEY (I J) (1:COMM-HELP-LGKEY) TO                  
JB*   *               ZIO-ECRAN (P:COMM-HELP-LGKEY)                             
JB*   *          COMPUTE ADE = ADE + COMM-HELP-LGKEY                            
JB*   *          COMPUTE P = P + (COMM-HELP-LGKEY - 1)                          
JB               MOVE TS-DATA-KEY (I J) (1:COMM-HELP-LGLIB (1) ) TO             
JB                    ZIO-ECRAN (P:COMM-HELP-LGLIB (1) )                        
JB               COMPUTE ADE = ADE + COMM-HELP-LGLIB (1)                        
                 COMPUTE P = P + (COMM-HELP-LGLIB (1) - 1)                      
JB*   *          MOVE DFH-ASK             TO WS-ATTR                            
JB*   *          MOVE TS-TURQ             TO WS-COLOR                           
JB*   *          MOVE DFH-DEFAULT         TO WS-HILI                            
JB*   *          PERFORM POSITIONNEMENT-CHAMP                                   
JB*   *          IF COMM-HELP-LGDATA NOT = ZERO                                 
JB               IF COMM-HELP-NCHAMPL > 1                                       
                   MOVE DFH-ASK             TO WS-ATTR                          
                   MOVE TS-TURQ             TO WS-COLOR                         
                   MOVE DFH-DEFAULT         TO WS-HILI                          
                   PERFORM POSITIONNEMENT-CHAMP                                 
                   ADD  1                   TO P                                
                   MOVE TS-DATA-DATA (I J) (1:COMM-HELP-LGLIB(2)) TO            
                         ZIO-ECRAN (P:COMM-HELP-LGLIB(2))                       
                   COMPUTE ADE = ADE + COMM-HELP-LGLIB(2)                       
                   COMPUTE P = P + (COMM-HELP-LGLIB(2) - 1)                     
JB                 MOVE  2  TO  IB                                              
JB                 PERFORM VARYING IA FROM 1 BY 1                               
                                         UNTIL IA > 8                           
                                         OR IA > (COMM-HELP-NCHAMPL - 2)        
                                         OR COMM-HELP-NCHAMPL < 2               
                                         OR IB > 8                              
                       MOVE DFH-ASK             TO WS-ATTR                      
                       MOVE TS-TURQ             TO WS-COLOR                     
                       MOVE DFH-DEFAULT         TO WS-HILI                      
                       PERFORM POSITIONNEMENT-CHAMP                             
                       ADD  1                   TO P                            
JB*   *             MOVE TS-DATA-DATA (I J) (1:COMM-HELP-LGDATA) TO             
JB*   *                  ZIO-ECRAN (P:COMM-HELP-LGDATA)                         
                       ADD  1                   TO IB                           
      *                MOVE TS-DATA-LIBELLE (I J IA)                            
      *                       (1:COMM-HELP-LGLIB(IB) ) TO                       
      *                       ZIO-ECRAN (P:COMM-HELP-LGLIB (IB) )               
                       ADD  SUR                 TO DEB                          
                       MOVE COMM-HELP-LGLIB(IB) TO SUR                          
                       MOVE TS-DATA-LIBELLE                                     
                              (DEB:SUR) TO                                      
                              ZIO-ECRAN (P:SUR)                                 
                        COMPUTE ADE = ADE + SUR                                 
                        IF SUR > 1                                              
                           COMPUTE P = P + (SUR - 1)                            
                        ELSE                                                    
                           COMPUTE P = P + 1                                    
                        END-IF                                                  
                    END-PERFORM                                                 
JB               ELSE                                                           
                    MOVE DFH-ASK             TO WS-ATTR                         
                    MOVE TS-TURQ             TO WS-COLOR                        
                    MOVE DFH-DEFAULT         TO WS-HILI                         
                    PERFORM POSITIONNEMENT-CHAMP                                
                 END-IF                                                         
              END-PERFORM                                                       
              IF TS-APLTXT = HIGH-VALUE                                         
                 MOVE DFH-ASK             TO WS-ATTR                            
                 MOVE DFH-DEFAULT         TO WS-HILI                            
                 MOVE TS-YELLOW           TO WS-COLOR                           
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 ADD  1                   TO P                                  
                 MOVE APL-SET             TO ZIO-ECRAN (P:1)                    
                 COMPUTE P = P + 1                                              
                 MOVE CLV                 TO ZIO-ECRAN (P:1)                    
              ELSE                                                              
                 MOVE DFH-ASK             TO WS-ATTR                            
                 MOVE TS-REVRS            TO WS-HILI                            
                 IF TS-REVRS NOT = LOW-VALUE                                    
                    MOVE TS-BLUE             TO WS-COLOR                        
                    PERFORM POSITIONNEMENT-CHAMP                                
                    ADD  1                   TO P                               
                    MOVE ' '                 TO ZIO-ECRAN (P:1)                 
                 ELSE                                                           
                    MOVE TS-YELLOW           TO WS-COLOR                        
                    PERFORM POSITIONNEMENT-CHAMP                                
                    ADD  1                   TO P                               
                    MOVE '!'                 TO ZIO-ECRAN (P:1)                 
                 END-IF                                                         
              END-IF                                                            
              COMPUTE ADE = ADE + 1                                             
              IF W-COLOR = HIGH-VALUE                                           
              OR W-HILI  = HIGH-VALUE                                           
                 PERFORM RESET-ATTRIBUTS-ECRAN                                  
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE ADE = ADE + (TS-SCRNWD - COMM-HELP-LENGTH).                  
           IF TS-APLTXT = HIGH-VALUE                                            
              MOVE DFH-DEFAULT         TO WS-HILI                               
              MOVE TS-YELLOW           TO WS-COLOR                              
              MOVE DFH-ASK             TO WS-ATTR                               
              PERFORM POSITIONNEMENT-CHAMP                                      
              COMPUTE P = P + 1                                                 
              MOVE APL-SET        TO ZIO-ECRAN (P:1)                            
              COMPUTE P = P + 1                                                 
              MOVE CBG            TO ZIO-ECRAN (P:1)                            
              COMPUTE LO = COMM-HELP-LENGTH - 3                                 
              PERFORM VARYING IO FROM 1 BY 1                                    
                      UNTIL   IO > LO                                           
                 COMPUTE P = P + 1                                              
                 MOVE APL-SET        TO ZIO-ECRAN (P:1)                         
                 COMPUTE P = P + 1                                              
                 MOVE CLH            TO ZIO-ECRAN (P:1)                         
              END-PERFORM                                                       
              COMPUTE P = P + 1                                                 
              MOVE APL-SET        TO ZIO-ECRAN (P:1)                            
              COMPUTE P = P + 1                                                 
              MOVE CBD            TO ZIO-ECRAN (P:1)                            
           ELSE                                                                 
              MOVE TS-REVRS            TO WS-HILI                               
              MOVE DFH-ASK             TO WS-ATTR                               
              IF TS-REVRS NOT = LOW-VALUE                                       
                 MOVE TS-BLUE             TO WS-COLOR                           
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 COMPUTE LO = COMM-HELP-LENGTH - 1                              
                 PERFORM VARYING IO FROM 1 BY 1                                 
                         UNTIL   IO > LO                                        
                    COMPUTE P = P + 1                                           
                    MOVE ' '         TO ZIO-ECRAN (P:1)                         
                 END-PERFORM                                                    
              ELSE                                                              
                 MOVE TS-YELLOW           TO WS-COLOR                           
                 PERFORM POSITIONNEMENT-CHAMP                                   
                 COMPUTE P = P + 1                                              
                 MOVE '+'         TO ZIO-ECRAN (P:1)                            
                 COMPUTE LO = COMM-HELP-LENGTH - 3                              
                 PERFORM VARYING IO FROM 1 BY 1                                 
                         UNTIL   IO > LO                                        
                    COMPUTE P = P + 1                                           
                    MOVE '-'         TO ZIO-ECRAN (P:1)                         
                 END-PERFORM                                                    
                 COMPUTE P = P + 1                                              
                 MOVE '+'         TO ZIO-ECRAN (P:1)                            
              END-IF                                                            
           END-IF.                                                              
           COMPUTE ADE = ADE + (COMM-HELP-LENGTH - 1).                          
           IF W-COLOR = HIGH-VALUE                                              
           OR W-HILI  = HIGH-VALUE                                              
              PERFORM RESET-ATTRIBUTS-ECRAN                                     
           END-IF.                                                              
           MOVE P                   TO LG-SEND.                                 
      *---------------------------------------                                  
       TRAITEMENT-INITIAL              SECTION.                                 
      *---------------------------------------                                  
           PERFORM READ-TS-CMA.                                                 
           INITIALIZE COMMAREA-HELP.                                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LINK PROGRAM('MHLPI')                                      
      *                   COMMAREA(Z-COMMAREA)                                  
      *                   LENGTH(LONG-COMMAREA)                                 
      *                   NOHANDLE                                              
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS LINK PROGRAM('MHLPI')                                      
                          COMMAREA(Z-COMMAREA)                                  
                          LENGTH(LONG-COMMAREA)                                 
                          NOHANDLE                                              
           END-EXEC.                                                            
      *}                                                                        
           IF COMM-HELP-OCCV NOT = ZERO                                         
              PERFORM INITIALISER-DATAS                                         
           ELSE                                                                 
              IF (COMM-CICS-APPLID (1:4) NOT = 'CI05')                          
              AND (COMM-CICS-APPLID (1:2) NOT = 'C5')                           
JB            AND (COMM-CICS-APPLID (1:3) NOT = 'CI5')                          
                MOVE 'Non autoris� / Not authorised - Press PF3'                
                                     TO   Z-MESSERR                             
                    MOVE  1          TO   KONTROL                               
              ELSE                                                              
                 PERFORM ERREUR-POSITIONNEMENT                                  
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       INITIALISER-DATAS               SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LINK PROGRAM('MHLPQ')                                      
      *                   COMMAREA(Z-COMMAREA)                                  
      *                   LENGTH(LONG-COMMAREA)                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS LINK PROGRAM('MHLPQ')                                      
                          COMMAREA(Z-COMMAREA)                                  
                          LENGTH(LONG-COMMAREA)                                 
           END-EXEC.                                                            
      *}                                                                        
E0452-*    REGARDE SI UN CHOIX SERA SAISISSABLE                                 
           SET COMM-HELP-MODE-INT TO TRUE                                       
           PERFORM VARYING COMM-HELP-NOITEM FROM 1 BY 1                         
           UNTIL (COMM-HELP-NOITEM > COMM-HELP-NBITEM)                          
           OR COMM-HELP-MODE-MAJ                                                
               PERFORM READ-TS-DATA                                             
               PERFORM VARYING J FROM 1 BY 1                                    
               UNTIL (J > 10) OR COMM-HELP-MODE-MAJ                             
                   IF (TS-DATA-SAISISSABLE(1 J) NOT = 'N')                      
                   AND (TS-DATA-KEY (1 J)) NOT = SPACE                          
                       SET COMM-HELP-MODE-MAJ TO TRUE                           
                   END-IF                                                       
               END-PERFORM                                                      
           END-PERFORM                                                          
-E0452     INITIALIZE TS-DATA                                                   
           MOVE 1                   TO COMM-HELP-NOITEM.                        
           IF COMM-HELP-NBITEM = 1                                              
              PERFORM RECALCULER-POS-DEBUT                                      
           END-IF.                                                              
      *---------------------------------------                                  
       RECALCULER-POS-DEBUT            SECTION.                                 
      *---------------------------------------                                  
           PERFORM READ-TS-DATA.                                                
           COMPUTE AD1 = TS-CURPOS / TS-SCRNWD.                                 
           PERFORM VARYING J FROM COMM-HELP-OCCV BY -1                          
                   UNTIL   J < 1                                                
                   OR      TS-DATA-KEY (1 J) NOT = SPACES                       
           END-PERFORM.                                                         
           IF J NOT < 1                                                         
              COMPUTE AD2 = COMM-HELP-POS-DEBUT / TS-SCRNWD                     
              IF AD2 NOT > AD1                                                  
                 IF COMM-HELP-OCCV = (TS-SCRNHT - 1) - 2                        
                    COMPUTE I = ((COMM-HELP-OCCV - J) / 2)                      
                              * TS-SCRNWD                                       
                 ELSE                                                           
                    COMPUTE I = (COMM-HELP-OCCV - J) * TS-SCRNWD                
                 END-IF                                                         
                 COMPUTE COMM-HELP-POS-DEBUT = COMM-HELP-POS-DEBUT              
                                             + I                                
                 COMPUTE I = (AD1 - 1) * TS-SCRNWD                              
                 IF COMM-HELP-POS-DEBUT > I                                     
                    DIVIDE COMM-HELP-POS-DEBUT BY TS-SCRNWD                     
                           GIVING ADE REMAINDER AD2                             
                    COMPUTE COMM-HELP-POS-DEBUT = (AD1 * TS-SCRNWD)             
                                                + AD2                           
                 END-IF                                                         
              END-IF                                                            
              MOVE J                TO COMM-HELP-OCCV                           
           END-IF.                                                              
      *---------------------------------------                                  
       ERREUR-POSITIONNEMENT           SECTION.                                 
      *---------------------------------------                                  
           MOVE 'Position inconnue / unknown - PF5 pour la cr�er / to cr        
      -    'eate it'  TO Z-MESSERR.                                             
           MOVE 1                   TO KONTROL.                                 
      *****************************************************************         
      *****************************************************************         
      ***************                                ******************         
      *************** T R A I T E M T    N O R M A L ******************         
      ***************                                ******************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ....... * TRAITEMENT NORMAL       *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -      TRAITEMENT NORMAL      -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------- -------------------- ------------------- *         
      * - CONTROLE SYNTAXE - - CONTROLE LOGIQUE - - TRAITEMENT TACHE- *         
      * -------------------- -------------------- ------------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       MODULE-TRAITEMENT-NORMAL        SECTION.                                 
      *---------------------------------------                                  
           MOVE LG-ECRAN            TO P.                                       
           PERFORM CONTROLE-LOGIQUE.                                            
           IF OK                                                                
              PERFORM TRAITEMENT-TACHE                                          
           END-IF.                                                              
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES LOGIQUES ******************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  LOGIQUES      *  HLP1    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       CONTROLE-LOGIQUE                SECTION.                                 
      *---------------------------------------                                  
           IF LG-ECRAN NOT = ZERO                                               
              PERFORM RECHERCHER-DATA-SAISI                                     
              IF NORMAL                                                         
                 PERFORM RECHERCHER-CHAMP-SELECTE                               
                 IF NON-TROUVE  AND                                             
                    P NOT > LG-ECRAN                                            
                    MOVE 'Position invalide / Invalid position'                 
                                             TO Z-MESSERR                       
                    MOVE 1                   TO KONTROL                         
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              IF TOUCHE-PF12                                                    
                 IF NORMAL                                                      
                    SET  NON-TROUVE          TO TRUE                            
                    MOVE EIBCPOSN            TO W-POSCHOIX                      
                    PERFORM RECHERCHER-POSITION                                 
                    IF NON-TROUVE                                               
                       MOVE 'Position invalide / Invalid position'              
                                                TO Z-MESSERR                    
                       MOVE 1                   TO KONTROL                      
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       RECHERCHER-DATA-SAISI           SECTION.                                 
      *---------------------------------------                                  
           SET  NORMAL              TO TRUE.                                    
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL I > LG-ECRAN                                           
              IF Z-INOUT (I:1) = SBA-SET                                        
                 ADD  3                TO I                                     
                 IF Z-INOUT (I:1) NOT = ' '                                     
                              AND NOT = LOW-VALUE                               
                              AND NOT = SBA-SET                                 
                    COMPUTE I = I + 1                                           
                    PERFORM VARYING I FROM I BY 1                               
                            UNTIL   I > LG-ECRAN                                
                       IF Z-INOUT (I:1) = SBA-SET                               
                          ADD  3                TO I                            
                          IF Z-INOUT (I:1) NOT = ' '                            
                                       AND NOT = LOW-VALUE                      
                                       AND NOT = SBA-SET                        
                             SET  DOUBLE           TO TRUE                      
                          END-IF                                                
                          COMPUTE I = I - 1                                     
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-IF                                                         
                 COMPUTE I = I - 1                                              
              END-IF                                                            
           END-PERFORM.                                                         
           IF DOUBLE                                                            
             MOVE 'Ne s�lectionner qu''une ligne / Select one line only'        
                                       TO Z-MESSERR                             
              MOVE 1                   TO KONTROL                               
           END-IF.                                                              
      *****************************************************************         
      *****************************************************************         
      *************** TRAITEMENT TACHE ********************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  TRAITEMENTS DE LA TACHE  *  HLP1    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -  TRAITEMENT DE LA TACHE     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------  ----------------  ---------------------  *         
      * - GESTION FICHIER -  - GESTION  MAP -  - GESTION COMMAREA  -  *         
      * -------------------  ----------------  ---------------------  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       TRAITEMENT-TACHE                SECTION.                                 
      *---------------------------------------                                  
           PERFORM TRAITEMENT-COMMAREA.                                         
           IF LG-ECRAN NOT = ZERO                                               
              PERFORM RECHERCHER-CHAMP-SELECTE                                  
              IF P NOT > LG-ECRAN                                               
                 PERFORM READ-TS-DATA                                           
                 PERFORM READ-TS-MAP                                            
                 MOVE TS-DATA-KEY (I J) (1:TS-LGCHAMP)  TO                      
                      TS-MESSAGE (TS-ADRCHAMP:TS-LGCHAMP)                       
                 PERFORM REWRITE-TS-MAP                                         
                 MOVE CODE-LEVEL-MAX              TO FONCTION                   
              END-IF                                                            
           ELSE                                                                 
              IF TOUCHE-PF12                                                    
                 SET  NON-TROUVE          TO TRUE                               
                 MOVE EIBCPOSN            TO W-POSCHOIX                         
                 PERFORM RECHERCHER-POSITION                                    
                 IF TROUVE                                                      
                    PERFORM READ-TS-DATA                                        
                    PERFORM READ-TS-MAP                                         
                    MOVE TS-DATA-KEY (I J) (1:TS-LGCHAMP)  TO                   
                         TS-MESSAGE (TS-ADRCHAMP:TS-LGCHAMP)                    
                    PERFORM REWRITE-TS-MAP                                      
                    MOVE CODE-LEVEL-MAX              TO FONCTION                
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
      *****************************************************************         
      *************** GESTION DE LA COMMAREA **************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA COMMAREA    *  HLP1    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       TRAITEMENT-COMMAREA             SECTION.                                 
      *---------------------------------------                                  
           IF TOUCHE-PF7                                                        
              COMPUTE COMM-HELP-NOITEM = COMM-HELP-NOITEM - 1                   
              IF COMM-HELP-NOITEM < 1                                           
                 MOVE 1                           TO COMM-HELP-NOITEM           
                 MOVE 'Pas de page pr�c�dente / No previous page'               
                      TO Z-MESSERR                                              
              ELSE                                                              
                 MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                   
              END-IF                                                            
           END-IF.                                                              
           IF TOUCHE-PF8                                                        
              COMPUTE COMM-HELP-NOITEM = COMM-HELP-NOITEM + 1                   
              IF COMM-HELP-NOITEM > COMM-HELP-NBITEM                            
                 MOVE COMM-HELP-NBITEM            TO COMM-HELP-NOITEM           
                 MOVE 'Pas de page suivante / No next page' TO                  
                      Z-MESSERR                                                 
              ELSE                                                              
                 MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                   
              END-IF                                                            
           END-IF.                                                              
      * -AIDA *********************************************************         
      *                                                               *         
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  *         
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  *         
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        *         
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        *         
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *                                                               *         
      *****************************************************************         
       EJECT                                                                    
      *****************************************************************         
      *                 MODULE DE SORTIE GENERALISE                   *         
      *****************************************************************         
      *---------------------------------------                                  
       MODULE-SORTIE                   SECTION.                                 
      *---------------------------------------                                  
           IF TRAITEMENT-AUTOMATIQUE                                            
              PERFORM          SORTIE-AFFICHAGE-FORMAT                          
           END-IF.                                                              
      *                                                                         
           IF NOT OK                                                            
              PERFORM          SORTIE-ERREUR                                    
           END-IF.                                                              
      *                                                                         
           IF TRAITEMENT-NORMAL                                                 
              PERFORM          SORTIE-SUITE                                     
           END-IF.                                                              
      *                                                                         
           IF LEVEL-MAX                                                         
              PERFORM          SORTIE-LEVEL-MAX                                 
           END-IF.                                                              
           IF ERREUR-MANIPULATION                                               
              PERFORM          SORTIE-ERREUR-MANIP                              
           END-IF.                                                              
      *                                                                         
      * ABANDON * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS.              
           GO TO ABANDON-TACHE.                                                 
      *                                                                         
       FIN-MODULE-SORTIE.        EXIT.                                          
       EJECT                                                                    
      ****************************************************************          
      * SORTIE DU TRAITEMENT AUTOMATIQUE                             *          
      ****************************************************************          
      *---------------------------------------                                  
       SORTIE-AFFICHAGE-FORMAT         SECTION.                                 
      *---------------------------------------                                  
      *                                                                         
           PERFORM SEND-NOERASE                                                 
      *                                                                         
           MOVE NOM-TACHE  TO  NOM-TACHE-RETOUR.                                
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *****************************************************************         
      *  AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT  *         
      *****************************************************************         
      *---------------------------------------                                  
       SORTIE-SUITE                    SECTION.                                 
      *---------------------------------------                                  
           PERFORM REINIT-MESSAGE.                                              
           PERFORM SEND-NOERASE.                                                
      *                                                                         
           MOVE EIBTRNID TO NOM-TACHE-RETOUR.                                   
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *****************************************************************         
      *    RETOUR APRES PF3 AU LEVEL SUPERIEUR                      *           
      *****************************************************************         
      *---------------------------------------                                  
       SORTIE-LEVEL-MAX                SECTION.                                 
      *---------------------------------------                                  
           PERFORM READ-TS-CMA.                                                 
           MOVE TS-LENGTH              TO LONG-COMMAREA.                        
           PERFORM READ-TS-MAP.                                                 
           PERFORM SEND-MESSAGE-ORIGINE.                                        
           PERFORM DELETE-TS-ALL.                                               
           MOVE Z-COMMAREA-NOM-TACHE   TO NOM-TACHE-RETOUR.                     
           PERFORM RETOUR-COMMAREA.                                             
      *****************************************************************         
      *         SORTIE ERREUR MANIPULATION                            *         
      *****************************************************************         
      *---------------------------------------                                  
       SORTIE-ERREUR-MANIP             SECTION.                                 
      *---------------------------------------                                  
           MOVE 'Touche fonction non d�finie / Function key not defined'        
             to Z-MESSERR.                                                      
           PERFORM SORTIE-ERREUR.                                               
      *---------------------------------------                                  
       SORTIE-ERREUR                   SECTION.                                 
      *---------------------------------------                                  
           PERFORM REINIT-MESSAGE.                                              
           PERFORM SEND-NOERASE.                                                
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *---------------------------------------                                  
       SEND-NOERASE                    SECTION.                                 
      *---------------------------------------                                  
           PERFORM INIT-ZONE-MESSAGE.                                           
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(ZIO-ECRAN)                                       
      *                   LENGTH(LG-SEND)                                       
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(ZIO-ECRAN)                                       
                          LENGTH(LG-SEND)                                       
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       REINIT-MESSAGE                  SECTION.                                 
      *---------------------------------------                                  
           MOVE ZERO                TO P.                                       
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > LG-ECRAN                                         
              IF Z-INOUT (I:1) = SBA-SET                                        
                 ADD  1                TO P                                     
                 MOVE Z-INOUT (I:1)    TO ZIO-ECRAN (P:1)                       
                 ADD  1                TO I                                     
                 MOVE Z-INOUT (I:2)    TO ZCOMPA                                
JB*   *          CALL 'MHLPA' USING ZCOMPA                                      
JB               PERFORM MHLPA-SECTION                                          
                 ADD  1                TO I                                     
                 COMPUTE ZCOMP = ZCOMP - 1                                      
      *{ Tr-Hexa-Map 1.5                                                        
      *          IF TS-14BITS = X'01'                                           
      *--                                                                       
                 IF TS-14BITS = X'01'                                           
      *}                                                                        
                    ADD  1                TO P                                  
                    MOVE ZCOMPA           TO ZIO-ECRAN (P:2)                    
                    ADD  1                TO P                                  
                 ELSE                                                           
                    DIVIDE ZCOMP BY 64 GIVING AD1 REMAINDER AD2                 
                    ADD  1                TO AD1                                
                    ADD  1                TO AD2                                
                    ADD  1                TO P                                  
                    MOVE T-ADDR (AD1)     TO ZIO-ECRAN (P:1)                    
                    ADD  1                TO P                                  
                    MOVE T-ADDR (AD2)     TO ZIO-ECRAN (P:1)                    
                 END-IF                                                         
                 ADD  1                TO P                                     
                 MOVE MF-SET           TO ZIO-ECRAN (P:1)                       
                 ADD  1                TO P                                     
      *{ Tr-Hexa-Map 1.5                                                        
      *          MOVE X'01'            TO ZIO-ECRAN (P:1)                       
      *--                                                                       
                 MOVE X'01'            TO ZIO-ECRAN (P:1)                       
      *}                                                                        
                 ADD  1                TO P                                     
                 MOVE FATT-SET         TO ZIO-ECRAN (P:1)                       
                 ADD  1                TO P                                     
                 MOVE NOR-ALP-FSET     TO ZIO-ECRAN (P:1)                       
              ELSE                                                              
                 ADD  1                TO P                                     
                 MOVE Z-INOUT (I:1)    TO ZIO-ECRAN (P:1)                       
              END-IF                                                            
           END-PERFORM.                                                         
      *---------------------------------------                                  
       INIT-ZONE-MESSAGE               SECTION.                                 
      *---------------------------------------                                  
           IF Z-MESSERR = SPACES                                                
             IF COMM-HELP-NBENR > COMM-HELP-LIMITE                              
              STRING 'LIMITE ' COMM-HELP-LIMITE                                 
              ' enreg. / LIMIT ' COMM-HELP-LIMITE ' records  '                  
              DELIMITED BY SIZE INTO Z-MESSERR (1:41)                           
             ELSE                                                               
E0452         IF COMM-HELP-MODE-MAJ                                             
              MOVE '''X'' pour s�lectionner / ''X'' to select'                  
                            TO Z-MESSERR                                        
E0452-        ELSE                                                              
              MOVE 'F3 pour sortir / F3 to exit' TO Z-MESSERR                   
-E0452        END-IF                                                            
             END-IF                                                             
              MOVE 41               TO I                                        
              IF COMM-HELP-NOITEM > 1                                           
                 MOVE ', PF7=page pr�c�dente / previous page'                   
                 TO Z-MESSERR (I:37)                                            
                 ADD  37               TO I                                     
              END-IF                                                            
              IF COMM-HELP-NOITEM < COMM-HELP-NBITEM                            
                 MOVE ', PF8=page suivante / next page'                         
                  TO Z-MESSERR (I:31)                                           
              END-IF                                                            
              MOVE DFH-DEFAULT         TO WS-HILI                               
           ELSE                                                                 
              MOVE TS-REVRS            TO WS-HILI                               
           END-IF.                                                              
           MOVE TS-NEUTR            TO WS-COLOR.                                
           MOVE ZERO                TO ADE.                                     
           IF W-HILI  NOT = HIGH-VALUE AND                                      
              W-COLOR NOT = HIGH-VALUE                                          
              MOVE DFH-ASK-BR          TO WS-ATTR                               
           ELSE                                                                 
              MOVE DFH-ASK             TO WS-ATTR                               
           END-IF.                                                              
           PERFORM POSITIONNEMENT-CHAMP.                                        
           PERFORM VARYING I FROM LENGTH OF Z-MESSERR BY -1                     
                   UNTIL   I = 1                                                
                   OR Z-MESSERR (I:1) > ' '                                     
           END-PERFORM.                                                         
           COMPUTE L = ((TS-SCRNWD - 1) - I) / 2.                               
           PERFORM VARYING L FROM L BY -1                                       
                   UNTIL   L < 1                                                
              ADD  1                TO P                                        
              MOVE ' '              TO ZIO-ECRAN (P:1)                          
              ADD  1                TO ADE                                      
           END-PERFORM.                                                         
           ADD  1                   TO P.                                       
           MOVE Z-MESSERR (1:I)     TO ZIO-ECRAN(P:I)                           
           COMPUTE P = P + (I - 1).                                             
           COMPUTE ADE = ADE + I.                                               
           IF ADE < TS-SCRNWD                                                   
              PERFORM VARYING ADE FROM ADE BY 1                                 
                      UNTIL   ADE = TS-SCRNWD                                   
                 ADD  1                TO P                                     
                 MOVE ' '              TO ZIO-ECRAN (P:1)                       
              END-PERFORM                                                       
              PERFORM RESET-ATTRIBUTS-ECRAN                                     
           END-IF.                                                              
           MOVE P                   TO LG-SEND.                                 
      *---------------------------------------                                  
       SEND-MESSAGE-ORIGINE            SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(TS-MESSAGE)                                      
      *                   LENGTH(TS-LGMESS)                                     
      *                   ERASE                                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(TS-MESSAGE)                                      
                          LENGTH(TS-LGMESS)                                     
                          ERASE                                                 
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       POSITIONNEMENT-CHAMP            SECTION.                                 
      *---------------------------------------                                  
           PERFORM SET-BYTE-ADDRESS.                                            
           ADD  1                   TO P.                                       
           MOVE TS-FSET             TO ZIO-ECRAN (P:1).                         
           IF TS-FSET = SFE-SET                                                 
              MOVE 1                   TO ZCOMP                                 
              IF W-COLOR      = HIGH-VALUE                                      
                 ADD  1                   TO ZCOMP                              
              END-IF                                                            
              IF W-HILI       = HIGH-VALUE                                      
                 ADD  1                   TO ZCOMP                              
              END-IF                                                            
              ADD  1                   TO P                                     
              MOVE ZCOMPA (2:1)        TO ZIO-ECRAN (P:1)                       
              ADD  1                   TO P                                     
              MOVE FATT-SET            TO ZIO-ECRAN (P:1)                       
              ADD  1                   TO P                                     
              MOVE WS-ATTR             TO ZIO-ECRAN (P:1)                       
              IF W-COLOR      = HIGH-VALUE                                      
                 ADD  1                   TO P                                  
                 MOVE COLOR-SET           TO ZIO-ECRAN (P:1)                    
                 ADD  1                   TO P                                  
                 MOVE WS-COLOR            TO ZIO-ECRAN (P:1)                    
              END-IF                                                            
              IF W-HILI      = HIGH-VALUE                                       
                 ADD  1                   TO P                                  
                 MOVE HILI-SET            TO ZIO-ECRAN (P:1)                    
                 ADD  1                   TO P                                  
                 MOVE WS-HILI             TO ZIO-ECRAN (P:1)                    
              END-IF                                                            
           ELSE                                                                 
              ADD  1                   TO P                                     
              MOVE WS-ATTR             TO ZIO-ECRAN (P:1)                       
           END-IF.                                                              
           COMPUTE ADE = ADE + 1.                                               
      *---------------------------------------                                  
       SET-BYTE-ADDRESS                SECTION.                                 
      *---------------------------------------                                  
           ADD  1                   TO P.                                       
           MOVE SBA-SET             TO ZIO-ECRAN (P:1).                         
      *{ Tr-Hexa-Map 1.5                                                        
      *    IF TS-14BITS = X'01'                                                 
      *--                                                                       
           IF TS-14BITS = X'01'                                                 
      *}                                                                        
              ADD  1                   TO P                                     
              MOVE ADA                 TO ZIO-ECRAN (P:2)                       
              ADD  1                   TO P                                     
           ELSE                                                                 
              DIVIDE ADE BY 64 GIVING AD1 REMAINDER AD2                         
              ADD  1                   TO AD1                                   
              ADD  1                   TO AD2                                   
              ADD  1                   TO P                                     
              MOVE T-ADDR (AD1)        TO ZIO-ECRAN (P:1)                       
              ADD  1                   TO P                                     
              MOVE T-ADDR (AD2)        TO ZIO-ECRAN (P:1)                       
           END-IF.                                                              
      *---------------------------------------                                  
       RESET-ATTRIBUTS-ECRAN           SECTION.                                 
      *---------------------------------------                                  
           COMPUTE L = TS-LGMESS - (TS-LGCHAMP + 4).                            
           MOVE ZERO                TO M.                                       
           MOVE ZERO                TO ADMAP.                                   
           PERFORM VARYING IMAP FROM 1 BY 1                                     
                   UNTIL   IMAP > L                                             
                   OR      ADMAP > ADE                                          
              IF TS-MESSAGE (IMAP:1) = SBA-SET                                  
                 COMPUTE IMAP = IMAP + 1                                        
                 PERFORM CALCUL-ADRESSE-MESSAGE                                 
                 IF ADMAP NOT > ADE                                             
                    COMPUTE M = IMAP + 1                                        
                    IF ADMAP = ADE                                              
                       MOVE ZERO          TO M                                  
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           IF M > ZERO                AND                                       
              M < L                                                             
              IF TS-MESSAGE (M:1) = SFE-SET                                     
                 ADD  1                   TO P                                  
                 MOVE TS-MESSAGE (M:1)    TO ZIO-ECRAN (P:1)                    
                 COMPUTE M = M + 1                                              
                 ADD  1                   TO P                                  
                 MOVE TS-MESSAGE (M:1)    TO ZIO-ECRAN (P:1)                    
                 MOVE ZERO                TO ZCOMP                              
                 MOVE TS-MESSAGE (M:1)    TO ZCOMPA (2:1)                       
                 COMPUTE M = M + 1                                              
                 PERFORM UNTIL ZCOMP = ZERO                                     
                    ADD  1                   TO P                               
                    MOVE TS-MESSAGE (M:1)    TO ZIO-ECRAN (P:1)                 
                    IF TS-MESSAGE (M:1) = FATT-SET                              
                       COMPUTE M = M + 1                                        
                       ADD  1                   TO P                            
                       MOVE DFH-ASK             TO ZIO-ECRAN (P:1)              
                    ELSE                                                        
                       COMPUTE M = M + 1                                        
                       ADD  1                   TO P                            
                       MOVE TS-MESSAGE (M:1)    TO ZIO-ECRAN (P:1)              
                    END-IF                                                      
                    COMPUTE M = M + 1                                           
                    COMPUTE ZCOMP = ZCOMP - 1                                   
                 END-PERFORM                                                    
              ELSE                                                              
                 IF TS-MESSAGE (M:1) = SF-SET                                   
                    ADD  1                   TO P                               
                    MOVE TS-MESSAGE (M:1)    TO ZIO-ECRAN (P:1)                 
                    ADD  1                   TO P                               
                    MOVE DFH-ASK             TO ZIO-ECRAN (P:1)                 
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       CALCUL-ADRESSE-MESSAGE          SECTION.                                 
      *---------------------------------------                                  
           MOVE TS-MESSAGE (IMAP:2) TO ZCOMPA.                                  
JB*   *    CALL 'MHLPA' USING ZCOMPA.                                           
JB         PERFORM MHLPA-SECTION.                                               
           COMPUTE IMAP = IMAP + 1.                                             
           MOVE ZCOMP               TO ADMAP.                                   
      *---------------------------------------                                  
       RECHERCHER-CHAMP-SELECTE        SECTION.                                 
      *---------------------------------------                                  
           SET  NON-TROUVE          TO TRUE.                                    
           PERFORM VARYING P FROM 1 BY 1                                        
                   UNTIL   P > LG-ECRAN                                         
                   OR      TROUVE                                               
              IF Z-INOUT (P:1) = SBA-SET                                        
                 ADD  1                   TO P                                  
                 MOVE Z-INOUT (P:2)       TO ZCOMPA                             
JB*   *          CALL 'MHLPA' USING ZCOMPA                                      
JB               PERFORM MHLPA-SECTION                                          
                 ADD  1                   TO P                                  
                 MOVE ZCOMP               TO W-POSCHOIX                         
                 ADD  1                   TO P                                  
                 IF Z-INOUT (P:1) NOT = ' '                                     
                              AND NOT = LOW-VALUE                               
                              AND NOT = SBA-SET                                 
                    PERFORM RECHERCHER-POSITION                                 
                 END-IF                                                         
                 COMPUTE P = P - 1                                              
              END-IF                                                            
           END-PERFORM.                                                         
      *---------------------------------------                                  
       RECHERCHER-POSITION             SECTION.                                 
      *---------------------------------------                                  
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > COMM-HELP-OCCH                                   
                   OR      TROUVE                                               
              PERFORM VARYING J FROM 1 BY 1                                     
                      UNTIL   J > COMM-HELP-OCCV                                
                      OR      TROUVE                                            
                 IF COMM-HELP-POSCUR (I J) = W-POSCHOIX                         
                    SET  TROUVE           TO TRUE                               
                 END-IF                                                         
              END-PERFORM                                                       
           END-PERFORM.                                                         
           IF TROUVE                                                            
              COMPUTE I = I - 1                                                 
              COMPUTE J = J - 1                                                 
           END-IF.                                                              
      *---------------------------------------                                  
       READ-TS-CMA                     SECTION.                                 
      *---------------------------------------                                  
           MOVE 4096                TO TS-LENGTH.                               
           MOVE 1                   TO ITEM-TS.                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS READQ TS QUEUE(NOM-TS-CMA)                                 
      *                       SET(ADDRESS OF TS-HLP-COMMAREA)                   
      *                       LENGTH(TS-LENGTH)                                 
      *                       ITEM(ITEM-TS)                                     
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS READQ TS QUEUE(NOM-TS-CMA)                                 
                              SET(ADDRESS OF TS-HLP-COMMAREA)                   
                              LENGTH(TS-LENGTH)                                 
                              ITEM(ITEM-TS)                                     
           END-EXEC.                                                            
      *}                                                                        
           MOVE TS-HLP-COMMAREA     TO Z-COMMAREA.                              
      *---------------------------------------                                  
       READ-TS-MAP                     SECTION.                                 
      *---------------------------------------                                  
           MOVE LENGTH OF TS-HLP-ECRAN TO TS-LENGTH.                            
           MOVE 1                      TO ITEM-TS.                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS READQ TS QUEUE(NOM-TS-MAP)                                 
      *                       INTO(TS-HLP-ECRAN)                                
      *                       LENGTH(TS-LENGTH)                                 
      *                       ITEM(ITEM-TS)                                     
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS READQ TS QUEUE(NOM-TS-MAP)                                 
                              INTO(TS-HLP-ECRAN)                                
                              LENGTH(TS-LENGTH)                                 
                              ITEM(ITEM-TS)                                     
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       READ-TS-DATA                    SECTION.                                 
      *---------------------------------------                                  
JB         MOVE LENGTH OF TS-DATA   TO TS-LENGTH.                               
           MOVE COMM-HELP-NOITEM    TO ITEM-TS.                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS READQ TS QUEUE(NOM-TS-DATA)                                
      *                       INTO(TS-DATA)                                     
      *                       LENGTH(TS-LENGTH)                                 
      *                       ITEM(ITEM-TS)                                     
      *                       NOHANDLE                                          
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS READQ TS QUEUE(NOM-TS-DATA)                                
                              INTO(TS-DATA)                                     
                              LENGTH(TS-LENGTH)                                 
                              ITEM(ITEM-TS)                                     
                              NOHANDLE                                          
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       READ-TS-LIBELLE                 SECTION.                                 
      *---------------------------------------                                  
JB         PERFORM CALCUL-LONG-TS.                                              
JB         MOVE TS-LIB-LONG         TO TS-LENGTH.                               
           MOVE COMM-HELP-NOITEM    TO ITEM-TS.                                 
           IF COMM-HELP-NCHAMPL > 2                                             
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS READQ TS QUEUE(NOM-TS-LIB)                              
      *                       INTO(TS-LIBELLE)                                  
      *                       LENGTH(TS-LENGTH)                                 
      *                       ITEM(ITEM-TS)                                     
      *                       NOHANDLE                                          
      *              END-EXEC                                                   
      *--                                                                       
              EXEC CICS READQ TS QUEUE(NOM-TS-LIB)                              
                              INTO(TS-LIBELLE)                                  
                              LENGTH(TS-LENGTH)                                 
                              ITEM(ITEM-TS)                                     
                              NOHANDLE                                          
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *---------------------------------------                                  
JB     CALCUL-LONG-TS                  SECTION.                                 
      *---------------------------------------                                  
      *                                                                         
           MOVE 0 TO TS-LIB-LONG.                                               
           IF COMM-HELP-NCHAMPL > 2                                             
              PERFORM VARYING JB FROM 2 BY 1 UNTIL                              
                                              JB > 10                           
                                           OR JB > COMM-HELP-NCHAMPL            
                    ADD COMM-HELP-LGTABLE (JB) TO TS-LIB-LONG                   
              END-PERFORM                                                       
              COMPUTE TS-LIB-LONG = COMM-HELP-OCCV * TS-LIB-LONG                
           END-IF.                                                              
      *                                                                         
      *---------------------------------------                                  
       REWRITE-TS-MAP                  SECTION.                                 
      *---------------------------------------                                  
           MOVE 1                   TO ITEM-TS.                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS WRITEQ TS QUEUE(NOM-TS-MAP)                                
      *                        FROM(TS-HLP-ECRAN)                               
      *                        LENGTH(TS-LENGTH)                                
      *                        ITEM(ITEM-TS)                                    
      *                        REWRITE                                          
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS WRITEQ TS QUEUE(NOM-TS-MAP)                                
                               FROM(TS-HLP-ECRAN)                               
                               LENGTH(TS-LENGTH)                                
                               ITEM(ITEM-TS)                                    
                               REWRITE                                          
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       DELETE-TS-ALL                   SECTION.                                 
      *---------------------------------------                                  
           PERFORM DELETE-TS-MAP.                                               
           PERFORM DELETE-TS-CMA.                                               
           PERFORM DELETE-TS-DATA.                                              
           PERFORM DELETE-TS-LIB.                                               
      *---------------------------------------                                  
       DELETE-TS-MAP                   SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-MAP)                               
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-MAP)                               
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       DELETE-TS-CMA                   SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-CMA)                               
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-CMA)                               
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       DELETE-TS-DATA                  SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-DATA)                              
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-DATA)                              
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       DELETE-TS-LIB                   SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-LIB)                               
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-LIB)                               
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
      * AIDA  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA CICS          * SYKC....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       S02-COPY SECTION. CONTINUE. COPY SYKCRTCO.                               
      *---------------------------------------                                  
       XCTL-PROG-COMMAREA              SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS XCTL PROGRAM(NOM-PROG-XCTL)                                
      *                   COMMAREA(Z-COMMAREA)                                  
      *                   LENGTH(LONG-COMMAREA)                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS XCTL PROGRAM(NOM-PROG-XCTL)                                
                          COMMAREA(Z-COMMAREA)                                  
                          LENGTH(LONG-COMMAREA)                                 
           END-EXEC.                                                            
      *}                                                                        
      *                                                                         
      *AIDA-SQL-01 SECTION. CONTINUE. COPY  SYKSTRAC.                           
      *AIDA-SQL-02 SECTION. CONTINUE. COPY  SYKSRETC.                           
      * -AIDA *********************************************************         
      *        MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES              
      *****************************************************************         
       T99-COPY SECTION. CONTINUE. COPY SYKCERRO.                               
      *****************************************************************         
JB     MHLPA-SECTION                  SECTION.                                  
           MOVE ZCOMPA              TO ZHWA.                                    
           IF ZHW > 4095                                                        
           OR ZHW < ZERO                                                        
              PERFORM VARYING AD3 FROM 1 BY 1                                   
                      UNTIL   AD3 > 64                                          
                      OR      T-ADDR (AD3) = ZHWA (1:1)                         
              END-PERFORM                                                       
              PERFORM VARYING AD4 FROM 1 BY 1                                   
                      UNTIL   AD4 > 64                                          
                      OR      T-ADDR (AD4) = ZHWA (2:1)                         
              END-PERFORM                                                       
              IF AD3 > 64                                                       
              OR AD4 > 64                                                       
                 COMPUTE ZHW = ZHW / WZERO                                      
              END-IF                                                            
              COMPUTE ZHW = ((AD3 - 1) * 64) + (AD4 - 1)                        
              MOVE ZHWA             TO ZCOMPA                                   
           END-IF.                                                              
JB     FIN-MHLPA-SECTION.             EXIT.                                     
           EJECT                                                                
