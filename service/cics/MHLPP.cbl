      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     MHLPP.                                   
       AUTHOR. PROJET DSA0.                                                     
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *==============================================================           
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  C                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  C                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  L                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  M                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  M                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-A                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I-A                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  P                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  P                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  ADA.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ADE                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
               10  ADE                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD1                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD1                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD2                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD2                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  ZCOMPA.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ZCOMP                 PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
               10  ZCOMP                 PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LG-SEND                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LG-SEND                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TS-LENGTH                 PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  TS-LENGTH                 PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SF-SET                    PIC X        VALUE X'1D'.              
      *--                                                                       
           05  SF-SET                    PIC X        VALUE X'1D'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SBA-SET                   PIC X        VALUE X'11'.              
      *--                                                                       
           05  SBA-SET                   PIC X        VALUE X'11'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  IC-SET                    PIC X        VALUE X'13'.              
      *--                                                                       
           05  IC-SET                    PIC X        VALUE X'13'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  PT-SET                    PIC X        VALUE X'05'.              
      *--                                                                       
           05  PT-SET                    PIC X        VALUE X'09'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  RA-SET                    PIC X        VALUE X'3C'.              
      *--                                                                       
           05  RA-SET                    PIC X        VALUE X'14'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  EAU-SET                   PIC X        VALUE X'12'.              
      *--                                                                       
           05  EAU-SET                   PIC X        VALUE X'12'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SFE-SET                   PIC X        VALUE X'29'.              
      *--                                                                       
           05  SFE-SET                   PIC X        VALUE X'89'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SA-SET                    PIC X        VALUE X'28'.              
      *--                                                                       
           05  SA-SET                    PIC X        VALUE X'88'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  MF-SET                    PIC X        VALUE X'2C'.              
      *--                                                                       
           05  MF-SET                    PIC X        VALUE X'8C'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  FA-SET                    PIC X        VALUE X'C0'.              
      *--                                                                       
           05  FA-SET                    PIC X        VALUE X'E9'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  HILI-SET                  PIC X        VALUE X'41'.              
      *--                                                                       
           05  HILI-SET                  PIC X        VALUE X'A0'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  COLOR-SET                 PIC X        VALUE X'42'.              
      *--                                                                       
           05  COLOR-SET                 PIC X        VALUE X'E2'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  PS-SET                    PIC X        VALUE X'43'.              
      *--                                                                       
           05  PS-SET                    PIC X        VALUE X'E4'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  APL-SET                   PIC X        VALUE X'08'.              
      *--                                                                       
           05  APL-SET                   PIC X        VALUE X'97'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  DFH-ASK                   PIC X        VALUE X'F0'.              
      *--                                                                       
           05  DFH-ASK                   PIC X        VALUE X'30'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  DFH-BLINK                 PIC X        VALUE X'F1'.              
      *--                                                                       
           05  DFH-BLINK                 PIC X        VALUE X'31'.              
      *}                                                                        
           05  W-ATTR                    PIC X        VALUE LOW-VALUE.          
      *{ Tr-Hexa-Map 1.5                                                        
      *        88  W-UNPROT              VALUES X'40'                           
      *--                                                                       
               88  W-UNPROT              VALUES X'20'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'C1'                           
      *--                                                                       
                                                X'41'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'C4'                           
      *--                                                                       
                                                X'44'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'C5'                           
      *--                                                                       
                                                X'45'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'C8'                           
      *--                                                                       
                                                X'48'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'C9'                           
      *--                                                                       
                                                X'49'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'4C'                           
      *--                                                                       
                                                X'3C'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'4D'                           
      *--                                                                       
                                                X'28'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'50'                           
      *--                                                                       
                                                X'26'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'D1'                           
      *--                                                                       
                                                X'4A'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'D4'                           
      *--                                                                       
                                                X'4D'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'D5'                           
      *--                                                                       
                                                X'4E'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'D8'                           
      *--                                                                       
                                                X'51'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'D9'                           
      *--                                                                       
                                                X'52'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'5C'                           
      *--                                                                       
                                                X'2A'                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *                                         X'5D'.                          
      *--                                                                       
                                                X'29'.                          
      *}                                                                        
           05  TAB-ADDR.                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'40C1C2C3C4C5C6C7C8C94A4B4C4D4E4F'.                         
      *--                                                                       
                   X'20414243444546474849B02E3C282B21'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'50D1D2D3D4D5D6D7D8D95A5B5C5D5E5F'.                         
      *--                                                                       
                   X'264A4B4C4D4E4F505152A7242A293B5E'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'6061E2E3E4E5E6E7E8E96A6B6C6D6E6F'.                         
      *--                                                                       
                   X'2D2F535455565758595AF92C255F3E3F'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'F0F1F2F3F4F5F6F7F8F97A7B7C7D7E7F'.                         
      *--                                                                       
                   X'303132333435363738393AA3E0273D22'.                         
      *}                                                                        
           05  FILLER      REDEFINES TAB-ADDR.                                  
               10  T-ADDR                PIC X        OCCURS 64.                
           05  ZIO-ECRAN                 PIC X(4096)  VALUE LOW-VALUE.          
           COPY TSHELP.                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER          PIC X OCCURS 4096 DEPENDING ON EIBCALEN.         
      *=================================================================        
       PROCEDURE DIVISION.                                                      
           MOVE DFHCOMMAREA         TO TS-HLP-ECRAN.                            
           PERFORM CREATION-PROTECTION-ECRAN.                                   
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN         END-EXEC.                                   
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       CREATION-PROTECTION-ECRAN       SECTION.                                 
      *---------------------------------------                                  
           MOVE ZERO                TO P.                                       
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > TS-LGMESS                                        
              IF TS-MESSAGE (I:1) = SBA-SET                                     
                 COMPUTE M = I + 3                                              
                 IF TS-MESSAGE (M:1) = SF-SET                                   
                 OR TS-MESSAGE (M:1) = SFE-SET                                  
                 OR TS-MESSAGE (M:1) = MF-SET                                   
                    PERFORM PROTECTION-ECRAN                                    
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE ADE = TS-CURPOS - 1.                                         
           ADD  1                   TO P.                                       
           MOVE SBA-SET             TO ZIO-ECRAN (P:1).                         
           PERFORM CALCUL-ADRESSE-ECRAN.                                        
           ADD  1                   TO P.                                       
           MOVE MF-SET              TO ZIO-ECRAN (P:1).                         
           ADD  1                   TO P.                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *    MOVE X'01'               TO ZIO-ECRAN (P:1).                         
      *--                                                                       
           MOVE X'01'               TO ZIO-ECRAN (P:1).                         
      *}                                                                        
           ADD  1                   TO P.                                       
           MOVE HILI-SET            TO ZIO-ECRAN (P:1).                         
           ADD  1                   TO P.                                       
           MOVE DFH-BLINK           TO ZIO-ECRAN (P:1).                         
           COMPUTE ADE = TS-CURPOS.                                             
           ADD  1                   TO P.                                       
           MOVE SBA-SET             TO ZIO-ECRAN (P:1).                         
           PERFORM CALCUL-ADRESSE-ECRAN.                                        
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > TS-LGCHAMP                                       
              ADD  1                TO P                                        
              MOVE '*'              TO ZIO-ECRAN (P:1)                          
           END-PERFORM.                                                         
           MOVE P                   TO LG-SEND.                                 
           IF LG-SEND > ZERO                                                    
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS SEND FROM(ZIO-ECRAN)                                    
      *                      LENGTH(LG-SEND)                                    
      *                 END-EXEC                                                
      *--                                                                       
              EXEC CICS SEND FROM(ZIO-ECRAN)                                    
                             LENGTH(LG-SEND)                                    
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *---------------------------------------                                  
       PROTECTION-ECRAN                SECTION.                                 
      *---------------------------------------                                  
           IF TS-MESSAGE (M:1) = SF-SET                                         
              COMPUTE I-A = M + 1                                               
           END-IF.                                                              
           IF TS-MESSAGE (M:1) = SFE-SET                                        
              COMPUTE I-A = M + 2                                               
              PERFORM VARYING I-A FROM I-A BY 1                                 
                      UNTIL   TS-MESSAGE(I-A:1) = FA-SET                        
                      OR      I-A > TS-LGMESS                                   
              END-PERFORM                                                       
              COMPUTE I-A = I-A + 1                                             
           END-IF.                                                              
           IF I-A NOT > TS-LGMESS                                               
              MOVE TS-MESSAGE (I-A:1)  TO W-ATTR                                
              IF W-UNPROT                                                       
                 ADD  1                  TO P                                   
                 MOVE TS-MESSAGE (I:3)   TO ZIO-ECRAN (P:3)                     
                 ADD  3                  TO P                                   
                 MOVE MF-SET             TO ZIO-ECRAN (P:1)                     
                 ADD  1                  TO P                                   
      *{ Tr-Hexa-Map 1.5                                                        
      *          MOVE X'01'              TO ZIO-ECRAN (P:1)                     
      *--                                                                       
                 MOVE X'01'              TO ZIO-ECRAN (P:1)                     
      *}                                                                        
                 ADD  1                  TO P                                   
                 MOVE FA-SET             TO ZIO-ECRAN (P:1)                     
                 ADD  1                  TO P                                   
                 MOVE DFH-ASK            TO ZIO-ECRAN (P:1)                     
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       CALCUL-ADRESSE-ECRAN            SECTION.                                 
      *---------------------------------------                                  
      *{ Tr-Hexa-Map 1.5                                                        
      *    IF TS-14BITS = X'01'                                                 
      *--                                                                       
           IF TS-14BITS = X'01'                                                 
      *}                                                                        
              ADD  1              TO P                                          
              MOVE ADA            TO ZIO-ECRAN (P:2)                            
              ADD  1              TO P                                          
           ELSE                                                                 
              DIVIDE ADE BY 64 GIVING AD1 REMAINDER AD2                         
              ADD  1              TO AD1                                        
              ADD  1              TO AD2                                        
              ADD  1              TO P                                          
              MOVE T-ADDR (AD1)   TO ZIO-ECRAN (P:1)                            
              ADD  1              TO P                                          
              MOVE T-ADDR (AD2)   TO ZIO-ECRAN (P:1)                            
           END-IF.                                                              
