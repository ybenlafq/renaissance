      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     THLPI.                                   
       AUTHOR. PROJET DSA0.                                                     
      *               AFFICHAGE MAP A TRAITER                                   
      *---------------------------------------------------------------*         
      * MAQUETTE DU CODE PROJET AIDA : DSA0                           *         
      *---------------------------------------------------------------*         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.
      *{Post-translation Correct-Val-pointeur
         77  NULLV PIC S9(8) COMP-5 VALUE -16777216.
         77  NULLP REDEFINES NULLV USAGE IS POINTER.
      *}
                  COPY  SYKWDIV0.                                               
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
       01  WS.                                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                  PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  I                  PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ADE                PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  ADE                PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD1                PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  AD1                PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD2                PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  AD2                PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-SCRNHT           PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  W-SCRNHT           PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-SCRNWD           PIC S9(4)   COMP    VALUE ZERO.               
      *--                                                                       
           05  W-SCRNWD           PIC S9(4) COMP-5    VALUE ZERO.               
      *}                                                                        
           05  W-MESSAGE.                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  FILLER             PIC X           VALUE X'11'.              
      *--                                                                       
               10  FILLER             PIC X           VALUE X'11'.              
      *}                                                                        
               10  W-ADDR             PIC XX          VALUE SPACES.             
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  FILLER             PIC XX          VALUE X'2902'.            
      *--                                                                       
               10  FILLER             PIC XX          VALUE X'8902'.            
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  FILLER             PIC XX          VALUE X'C0F0'.            
      *--                                                                       
               10  FILLER             PIC XX          VALUE X'E930'.            
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  FILLER             PIC XX          VALUE X'42F7'.            
      *--                                                                       
               10  FILLER             PIC XX          VALUE X'E237'.            
      *}                                                                        
               10  W-MESS             PIC X(79)       VALUE                     
                  'Mettre le curseur au d�but du champ desir�, et faire         
      -    'entr�e ou PA2'.                                                     
           05  TAB-ADDR.                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'40C1C2C3C4C5C6C7C8C94A4B4C4D4E4F'.                         
      *--                                                                       
                   X'20414243444546474849B02E3C282B21'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'50D1D2D3D4D5D6D7D8D95A5B5C5D5E5F'.                         
      *--                                                                       
                   X'264A4B4C4D4E4F505152A7242A293B5E'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'6061E2E3E4E5E6E7E8E96A6B6C6D6E6F'.                         
      *--                                                                       
                   X'2D2F535455565758595AF92C255F3E3F'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'F0F1F2F3F4F5F6F7F8F97A7B7C7D7E7F'.                         
      *--                                                                       
                   X'303132333435363738393AA3E0273D22'.                         
      *}                                                                        
           05  FILLER      REDEFINES TAB-ADDR.                                  
               10  T-ADDR                PIC X        OCCURS 64.                
           COPY TSHELP.                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION  DES ZONES D'INTERFACE DB2                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
           COPY SYKWZCMD.                                                       
      * -AIDA *********************************************************         
      *   ZONES GENERALES OBLIGATOIRES                                *         
      *****************************************************************         
           COPY  SYKWEIB0.                                                      
           COPY  SYKWDATH.                                                      
           COPY  SYKWCWA0.                                                      
           COPY  SYKWTCTU.                                                      
           COPY  SYKWECRA.                                                      
      * -AIDA *********************************************************         
      *   ZONES DE CONTROLE ET DE TRAITEMENT                          *         
      *****************************************************************         
           COPY  SYKWAGE0.                                                      
           COPY  SYKWDATE.                                                      
           COPY  SYKWDECI.                                                      
           COPY  SYKWVALN.                                                      
           COPY  SYKWTIME.                                                      
           COPY  SYKWMONT.                                                      
           COPY  SYKWCADD.                                                      
      * -AIDA *********************************************************         
      *  ZONES DE COMMAREA                                                      
      *****************************************************************         
           COPY  COMMHELP.                                                      
           COPY  SYKWCOMM.                                                      
           COPY  SYKWSTAR.                                                      
      * -AIDA *********************************************************         
      *  ZONE DE COMMAREA POUR PROGRAMME APPELE PAR LINK                        
      *****************************************************************         
      *                                                                         
       01  FILLER             PIC X(16)  VALUE 'Z-COMMAREA-LINK'.               
       01  Z-COMMAREA-LINK    PIC X(200).                                       
      *                                                                         
           COPY COMMDATC.                                                       
      * -AIDA *********************************************************         
      *       * ZONE BUFFER D'ENTREE-SORTIE                                     
      *****************************************************************         
           COPY  SYKWZINO.                                                      
      * -AIDA *********************************************************         
      *       * ZONE DE GESTION DES ERREURS                                     
      *****************************************************************         
           COPY  SYKWERRO.                                                      
      * -AIDA *********************************************************         
      *       * ZONES DE GESTION DES FICHIERS                                   
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *         DESCRIPTION DES BASES DE DONNEES RELATIONNELLES                 
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      * -AIDA *********************************************************         
      *                                                               *         
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   *         
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   *         
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          *         
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          *         
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          *         
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    *         
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    *         
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 4096 DEPENDING ON EIBCALEN.                  
           COPY  SYKLINKB.                                                      
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           - MODULE DE BASE DE LA TRANSACTION HLPI -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-HLPI                    SECTION.                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      *                                                                         
           IF TRAITEMENT                                                        
              PERFORM MODULE-TRAITEMENT                                         
           END-IF.                                                              
      *                                                                         
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-HLPI.    EXIT.                                                
      * -AIDA *********************************************************         
      *                                                               *         
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        *         
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  *         
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        *         
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        *         
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                    -----------------------                    *         
      *                    -   MODULE D'ENTREE   -                    *         
      *                    -----------------------                    *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -      -            -                   -           *         
      *  ----------  --------  ----------  -----------------------    *         
      *  - HANDLE -  - USER -  - ADRESS -  - RECEPTION MESSAGE   -    *         
      *  ----------  --------  ----------  -----------------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-ENTREE              SECTION.                                      
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
      *                                                                         
           PERFORM INIT-USER.                                                   
      *                                                                         
           PERFORM INIT-ADDRESS.                                                
      *                                                                         
           PERFORM RECEPTION-MESSAGE.                                           
      *                                                                         
       FIN-MODULE-ENTREE.         EXIT.                                         
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
      *****************************************************************         
      *    INITIALISATION DES ZONES QUI NE PEUVENT PAS ETRE EN VALUE  *         
      *****************************************************************         
      *                                                                         
       INIT-USER                  SECTION.                                      
           MOVE 'EHLPI'                TO NOM-MAP.                              
           MOVE 'EHLPI'                TO NOM-MAPSET.                           
           MOVE 'THLPI'                TO NOM-PROG.                             
           MOVE 'HLPI'                 TO NOM-TACHE.                            
           MOVE 'HLP0'                 TO NOM-LEVEL-MAX.                        
      *                                                                         
           MOVE   'NON'       TO  DEBUGGIN.                                     
      *                                                                         
      *----ON NE RENTRE PAS DIRECTEMENT DANS CE PROGRAMME                       
           IF  EIBCALEN  =  0                                                   
               MOVE SPACES       TO  Z-COMMAREA                                 
               MOVE 'THELP'      TO  NOM-PROG-XCTL                              
               PERFORM XCTL-NO-COMMAREA                                         
           END-IF.                                                              
      *                                                                         
      *----RECEPTION COMMAREA                                                   
           MOVE COM-HELP-LONG-COMMAREA TO LONG-COMMAREA                         
                                             LONG-START.                        
      *                                                                         
      *                                                                         
           IF  LONG-COMMAREA  LESS  220   OR  GREATER  4096                     
               MOVE 'LONG-COMMAREA HORS-LIMITES' TO MESS                        
               GO TO ABANDON-TACHE                                              
           END-IF.                                                              
      *                                                                         
           IF EIBCALEN  LESS  220   OR  GREATER  4096                           
              MOVE 'EIBCALEN HORS LIMITES' TO MESS                              
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
      *                                                                         
           IF EIBCALEN  NOT =   LONG-COMMAREA                                   
              MOVE 'EIBCALEN NOT = LONG-COMMAREA' TO MESS                       
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
      *                                                                         
           MOVE DFHCOMMAREA  TO  Z-COMMAREA.                                    
      *                                                                         
           PERFORM COMMAREA-HELP-INPUT.                                         
      *                                                                         
       FIN-INIT-USER.             EXIT.                                         
      * DARTY ********************************************************          
      *       * CONTROLES INFORMATIONS APPLICATIVES COMMAREA         *          
      ****************************************************************          
      *                                                                         
       COMMAREA-HELP-INPUT       SECTION.                                       
      *                                                                         
      *    IF COM-HELP-XXXX  NOT =  VALEUR-VOULUE                               
      *       MOVE 'COMMAREA : COM-HELP-XXXX HORS-LIMITES' TO MESS              
      *       GO TO ABANDON-TACHE                                               
      *    END-IF.                                                              
      *                                                                         
       FIN-COMMAREA-HELP-INPUT.      EXIT.                                      
       E02-COPY SECTION. CONTINUE. COPY SYKCADDR.                               
      *****************************************************************         
      * DETERMINATION DU TRAITEMENT EN FONCTION DE L'ENVIRONNEMENT    *         
      *****************************************************************         
      *                                                                         
       RECEPTION-MESSAGE          SECTION.                                      
      *                                                                         
      *----AUTRE TACHE                                                          
           IF EIBTRNID NOT = NOM-TACHE                                          
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                      
              GO TO FIN-RECEPTION-MESSAGE                                       
           END-IF.                                                              
      *                                                                         
           MOVE EIBAID        TO WORKAID.                                       
      *                                                                         
      *----LEVEL-MAX KA                                                         
           IF  TOUCHE-PF4                                                       
               MOVE CODE-LEVEL-MAX TO FONCTION                                  
           END-IF.                                                              
      *                                                                         
      *----LEVEL-SUP KC                                                         
           IF  TOUCHE-PF3                                                       
               MOVE CODE-LEVEL-SUP TO FONCTION                                  
           END-IF.                                                              
      *                                                                         
      *----SUITE 01                                                             
           IF TOUCHE-ENTER                                                      
           OR TOUCHE-PA2                                                        
              MOVE CODE-TRAITEMENT-NORMAL TO FONCTION                           
           END-IF.                                                              
      *                                                                         
      *----MAPFAIL                                                              
           IF  TOUCHE-CLEAR                                                     
               MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                     
           END-IF.                                                              
      *                                                                         
           PERFORM POSIT-ATTR-INITIAL.                                          
      *                                                                         
       FIN-RECEPTION-MESSAGE.      EXIT.                                        
      *E03-COPY SECTION. CONTINUE. COPY SYKCRECV.                               
      *E04-COPY SECTION. CONTINUE. COPY SYKCZCMD.                               
      * DARTY ********************************************************          
      *       * POSITIONNEMENT ATTRIBUTS INITIAUX DE LA MAP BMS      *          
      ****************************************************************          
      *                                                                         
       POSIT-ATTR-INITIAL         SECTION.                                      
      *                                                                         
      * PENSEZ QU'IL Y A TROIS ATTRIBUTS PAR VARIABLES                          
      *        A = ATTRIBUT STANDARD                                            
      *        C = COULEUR                                                      
      *        H = ATTRIBUT ETENDUS                                             
      *                                                                         
      * ON TROUVERA DANS LE COPY SYKWECRA LES NOMS ET LES VALEURS               
      * ACCORDES A CHACUN. ON POURRA EGALEMENT EN CREER DE NOUVEAUX.            
      *                                                                         
      *    MOVE 'I'           TO  MXXXXXA.                                      
      *    MOVE '2'           TO  MXXXXXC.                                      
      *    MOVE '2'           TO  MXXXXXH.                                      
      *                                                                         
      *    MOVE BRT-NUM-FSET  TO  MZZZZZA.                                      
      *    MOVE DFH-RED       TO  MZZZZZC.                                      
      *    MOVE DFH-REVRS     TO  MZZZZZH.                                      
      *                                                                         
       FIN-POSIT-ATTR-INITIAL.    EXIT.                                         
      * -AIDA *********************************************************         
      *                                                               *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  *         
      *     TTT    RRR RRRR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RR  AAA   AAA    III       TTT    EEE        *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE TRAITEMENT                                          *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT          SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
               PERFORM MODULE-TRAITEMENT-NORMAL                                 
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
               PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                            
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT.      EXIT.                                        
      *****************************************************************         
      *****************************************************************         
      ***************** TRAITEMENT AUTOMATIQUE ************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ......  * TRAITEMENT AUTOMATIQUE  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   TRAITEMENT AUTOMATIQUE    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *                               -                               *         
      *                               -                               *         
      *                    ---------------------                      *         
      *                    - REMPLISSAGE ECRAN -                      *         
      *                    ---------------------                      *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                                  
      *                                                                         
           PERFORM REMPLISSAGE-FORMAT-ECRAN.                                    
      *                                                                         
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -   REMPLISSAGE DE L'ECRAN    -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * ----------------------   -------------   -----------------    *         
      * - ZONES OBLIGATOIRES -   - PROTEGEES -   - NON PROTEGEES -    *         
      * ----------------------   -------------   -----------------    *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * REMPLISSAGE DE LA MAP     *  HLPI    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-FORMAT-ECRAN      SECTION.                                   
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                             
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-NO-PROTEGEES.                             
      *                                                                         
       FIN-REMPLISSAGE-FORMAT-ECRAN. EXIT.                                      
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES OBLIGATOIRES        *  HLPI    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-OBLIGATOIRES SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-REMP-ZONES-OBL.            EXIT.                                     
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES PROTEGEES           *  HLPI    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-PROTEGEES    SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-REMP-ZONES-PROT.           EXIT.                                     
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES NON PROTEGEES       *  HLPI    * TRAITEMENT AUTOMATIQUE  *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       REMPLISSAGE-ZONES-NO-PROTEGEES SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-REMP-ZONES-NO-PROT.        EXIT.                                     
      *****************************************************************         
      *****************************************************************         
      ***************                                ******************         
      *************** T R A I T E M T    N O R M A L ******************         
      ***************                                ******************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE BASE DU CONTAINER ....... * TRAITEMENT NORMAL       *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -      TRAITEMENT NORMAL      -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------- -------------------- ------------------- *         
      * - CONTROLE SYNTAXE - - CONTROLE LOGIQUE - - TRAITEMENT TACHE- *         
      * -------------------- -------------------- ------------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       MODULE-TRAITEMENT-NORMAL        SECTION.                                 
      *---------------------------------------                                  
      *****************************************************************         
      *****************************************************************         
      *************** CONTROLES SYNYAXIQUES ***************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  SYNTAXIQUES   *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       CONTROLE-SYNTAXE                SECTION.                                 
      *---------------------------------------                                  
      *****************************************************************         
      *************** CONTROLES LOGIQUES ******************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CONTROLES  LOGIQUES      *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       CONTROLE-LOGIQUE                SECTION.                                 
      *---------------------------------------                                  
      *                                                                         
       FIN-CONTROL-LOGIQUE.       EXIT.                                         
      *****************************************************************         
      *****************************************************************         
      *************** TRAITEMENT TACHE ********************************         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  TRAITEMENTS DE LA TACHE  *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -  TRAITEMENT DE LA TACHE     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -                   -                   -           *         
      * -------------------  ----------------  ---------------------  *         
      * - GESTION FICHIER -  - GESTION  MAP -  - GESTION COMMAREA  -  *         
      * -------------------  ----------------  ---------------------  *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       TRAITEMENT-TACHE                SECTION.                                 
      *---------------------------------------                                  
      *****************************************************************         
      *************** GESTION DES FICHIERS ****************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  GESTION DES FICHIERS     *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *                -------------------------------                *         
      *                -    GESTION DES FICHIERS     -                *         
      *                -------------------------------                *         
      *                               -                               *         
      *           -----------------------------------------           *         
      *           -            -             -            -           *         
      * ---------------- ------------ ---------------- -------------- *         
      * - DETERMINATION- - CREATION - - MODIFICATION - - SUPRESSION - *         
      * ---------------- ------------ ---------------- -------------- *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       TRAITEMENT-FICHIER              SECTION.                                 
      *---------------------------------------                                  
           PERFORM DETERMINATION-TRAITEMENT.                                    
      *                                                                         
           IF CREATION                                                          
              PERFORM CREATION-RECORD                                           
           END-IF.                                                              
      *                                                                         
           IF MODIFICATION                                                      
              PERFORM MODIFICATION-RECORD                                       
           END-IF.                                                              
      *                                                                         
           IF SUPPRESSION                                                       
              PERFORM SUPPRESSION-RECORD                                        
           END-IF.                                                              
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DETERMINATION DE L'ACTION *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       DETERMINATION-TRAITEMENT        SECTION.                                 
      *---------------------------------------                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  CREATION                 *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *---------------------------------------                                  
       CREATION-RECORD                 SECTION.                                 
      *---------------------------------------                                  
      *---------------------------------------                                  
       MODIFICATION-RECORD             SECTION.                                 
      *---------------------------------------                                  
      *---------------------------------------                                  
       MODIFICATION-NOMCHAMP           SECTION.                                 
      *---------------------------------------                                  
      *---------------------------------------                                  
       MODIFICATION-POSCHAMP           SECTION.                                 
      *---------------------------------------                                  
      *---------------------------------------                                  
       SUPPRESSION-RECORD              SECTION.                                 
      *---------------------------------------                                  
      *****************************************************************         
      *************** GESTION DE LA COMMAREA **************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA COMMAREA    *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-COMMAREA            SECTION.                                  
      *                                                                         
      *    MOVE XX01-XXXXXX       TO COM-HELP-XXXXXX.                           
      *                                                                         
       FIN-TRAITEMENT-COMMAREA.       EXIT.                                     
      *****************************************************************         
      *************** GESTION DE LA MAP *******************************         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * GESTION DE LA MAP         *  HLPI    * TRAITEMENT NORMAL       *        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TRAITEMENT-MAP                 SECTION.                                  
      *                                                                         
      *                                                                         
       FIN-TRAITEMENT-MAP.            EXIT.                                     
      * -AIDA *********************************************************         
      *                                                               *         
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  *         
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  *         
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        *         
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        *         
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *                                                               *         
      *****************************************************************         
      *****************************************************************         
      *                 MODULE DE SORTIE GENERALISE                   *         
      *****************************************************************         
      *                                                                         
       MODULE-SORTIE              SECTION.                                      
      *                                                                         
           IF  TRAITEMENT-AUTOMATIQUE                                           
                       PERFORM          SORTIE-AFFICHAGE-FORMAT                 
           END-IF.                                                              
      *                                                                         
           IF NOT OK                                                            
              PERFORM          SORTIE-ERREUR                                    
           END-IF.                                                              
      *                                                                         
           IF  TRAITEMENT-NORMAL                                                
                       PERFORM          SORTIE-SUITE                            
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-SUP                                                        
                       PERFORM          SORTIE-LEVEL-SUPERIEUR                  
           END-IF.                                                              
      *                                                                         
           IF  LEVEL-MAX OR JUMP                                                
                       PERFORM          SORTIE-LEVEL-MAX                        
           END-IF.                                                              
      *                                                                         
           IF  ERREUR-MANIPULATION                                              
                       PERFORM          SORTIE-ERREUR-MANIP                     
           END-IF.                                                              
      *                                                                         
      * ABANDON * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS.              
           GO TO ABANDON-TACHE.                                                 
      *                                                                         
       FIN-MODULE-SORTIE.        EXIT.                                          
      ****************************************************************          
      * SORTIE DU TRAITEMENT AUTOMATIQUE                             *          
      ****************************************************************          
      *                                                                         
       SORTIE-AFFICHAGE-FORMAT    SECTION.                                      
      *                                                                         
           PERFORM SEND-MAP.                                                    
      *                                                                         
           MOVE SPACES     TO  Z-COMMAREA-TACHE-JUMP.                           
           MOVE NOM-TACHE  TO  NOM-TACHE-RETOUR.                                
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-AFFICHAGE-FORMAT. EXIT.                                       
      *****************************************************************         
      *  AFFICHAGE DE LA MAP EN ERREUR                                *         
      *                      EN CONFIRMATION                          *         
      *  ET RETURN AU MEME PROGRAMME                                  *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR              SECTION.                                      
      *                                                                         
           PERFORM SEND-MAP-DATA-ONLY.                                          
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR. EXIT.                                                 
      *****************************************************************         
      *  AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT  *         
      *****************************************************************         
      *---------------------------------------                                  
       SORTIE-SUITE                    SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LINK PROGRAM('MHLPM')                                      
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS LINK PROGRAM('MHLPM')                                      
           END-EXEC.                                                            
      *}                                                                        
           MOVE 'THLP2'             TO NOM-PROG-XCTL.                           
           MOVE NOM-PROG            TO COM-PGMPRC.                              
           MOVE EIBTRNID            TO Z-COMMAREA-NOM-TACHE                     
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-SUITE. EXIT.                                                  
      *****************************************************************         
      *    RETOUR APRES PF3 AU LEVEL SUPERIEUR                      *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-SUPERIEUR     SECTION.                                      
      *                                                                         
           MOVE 'THLP0'   TO  NOM-PROG-XCTL.                                    
           MOVE NOM-PROG  TO  COM-PGMPRC.                                       
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-SUPERIEUR. EXIT.                                        
      *****************************************************************         
      *    RETOUR APRES PF4 AU MENU                                 *           
      *****************************************************************         
      *                                                                         
       SORTIE-LEVEL-MAX           SECTION.                                      
      *                                                                         
           MOVE 'THLP0'   TO  NOM-PROG-XCTL.                                    
           MOVE NOM-PROG  TO  COM-PGMPRC.                                       
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-MAX. EXIT.                                              
      *****************************************************************         
      *         SORTIE ERREUR MANIPULATION                            *         
      *****************************************************************         
      *                                                                         
       SORTIE-ERREUR-MANIP        SECTION.                                      
      *                                                                         
           MOVE 'TOUCHE DE FONCTION NON AUTORISEE' TO W-MESS.                   
      *    MOVE CURSEUR                      TO MXXXXXL.                        
      *                                                                         
           PERFORM SEND-MAP-DATA-ONLY.                                          
      *                                                                         
           MOVE NOM-TACHE TO NOM-TACHE-RETOUR.                                  
      *                                                                         
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR-MANIP. EXIT.                                           
      * DARTY ********************************************************          
      *       * SAUVEGARDE DES ATTRIBUTS BMS EN CAS DE SWAP          *          
      ****************************************************************          
      *                                                                         
       SAVE-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *    MOVE MXXXXXA     TO COMM-SWAP-ATTR (001).                            
      *    MOVE MXXXXXC     TO COMM-SWAP-ATTR (002).                            
      *    MOVE MXXXXXH     TO COMM-SWAP-ATTR (003).                            
      *                                                                         
      *    MOVE MZZZZZA     TO COMM-SWAP-ATTR (148).                            
      *    MOVE MZZZZZC     TO COMM-SWAP-ATTR (149).                            
      *    MOVE MZZZZZH     TO COMM-SWAP-ATTR (150).                            
      *                                                                         
       FIN-SAVE-SWAP-ATTR.      EXIT.                                           
      * DARTY ********************************************************          
      *       * RESTAURATION DES ATTRIBUTS BMS EN CAS DE SWAP        *          
      ****************************************************************          
      *                                                                         
       REST-SWAP-ATTR             SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE EIBCPOSN             TO COMM-SWAP-CURS.                         
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (001) TO MXXXXXA.                                
      *    MOVE COMM-SWAP-ATTR (002) TO MXXXXXC.                                
      *    MOVE COMM-SWAP-ATTR (003) TO MXXXXXH.                                
      *                                                                         
      *    MOVE COMM-SWAP-ATTR (148) TO MZZZZZA.                                
      *    MOVE COMM-SWAP-ATTR (149) TO MZZZZZC.                                
      *    MOVE COMM-SWAP-ATTR (150) TO MZZZZZH.                                
      *                                                                         
       FIN-REST-SWAP-ATTR.      EXIT.                                           
      * AIDA  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA CICS          * SYKC....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *S01-COPY SECTION. CONTINUE. COPY SYKCHELP.                               
       S02-COPY SECTION. CONTINUE. COPY SYKCRTCO.                               
       S03-COPY SECTION. CONTINUE. COPY SYKCRTNO.                               
       S04-COPY SECTION. CONTINUE. COPY SYKCRT00.                               
      *S05-COPY SECTION. CONTINUE. COPY SYKCSMDO.                               
      *S06-COPY SECTION. CONTINUE. COPY SYKCSMEM.                               
      *S07-COPY SECTION. CONTINUE. COPY SYKCSMER.                               
      *S08-COPY SECTION. CONTINUE. COPY SYKCSMNO.                               
      *S09-COPY SECTION. CONTINUE. COPY SYKCSMSE.                               
      *S10-COPY SECTION. CONTINUE. COPY SYKCSM00.                               
      *---------------------------------------                                  
       SEND-MAP                        SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND MAP(Z-COMMAREA-NOM-MAP)                               
      *                   MAPONLY                                               
      *                   ERASE                                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND MAP(Z-COMMAREA-NOM-MAP)                               
                          MAPONLY                                               
                          ERASE                                                 
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASSIGN SCRNHT(W-SCRNHT)                                    
      *                     SCRNWD(W-SCRNWD)                                    
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS ASSIGN SCRNHT(W-SCRNHT)                                    
                            SCRNWD(W-SCRNWD)                                    
           END-EXEC.                                                            
      *}                                                                        
      *    COMPUTE ADE = (W-SCRNHT - 1) * W-SCRNWD.                             
           MOVE ZERO                TO ADE.                                     
           DIVIDE ADE BY 64 GIVING AD1 REMAINDER AD2.                           
           ADD  1                   TO AD1.                                     
           ADD  1                   TO AD2.                                     
           MOVE T-ADDR (AD1)        TO W-ADDR (1:1).                            
           MOVE T-ADDR (AD2)        TO W-ADDR (2:1).                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(W-MESSAGE)                                       
      *                   LENGTH(LENGTH OF W-MESSAGE)                           
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(W-MESSAGE)                                       
                          LENGTH(LENGTH OF W-MESSAGE)                           
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       SEND-MAP-DATA-ONLY              SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASSIGN SCRNHT(W-SCRNHT)                                    
      *                     SCRNWD(W-SCRNWD)                                    
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS ASSIGN SCRNHT(W-SCRNHT)                                    
                            SCRNWD(W-SCRNWD)                                    
           END-EXEC.                                                            
      *}                                                                        
           COMPUTE ADE = (W-SCRNHT - 1) * W-SCRNWD.                             
           DIVIDE ADE BY 64 GIVING AD1 REMAINDER AD2.                           
           ADD  1                   TO AD1.                                     
           ADD  1                   TO AD2.                                     
           MOVE T-ADDR (AD1)        TO W-ADDR (1:1).                            
           MOVE T-ADDR (AD2)        TO W-ADDR (2:1).                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(W-MESSAGE)                                       
      *                   LENGTH(LENGTH OF W-MESSAGE)                           
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(W-MESSAGE)                                       
                          LENGTH(LENGTH OF W-MESSAGE)                           
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       XCTL-PROG-COMMAREA              SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS XCTL PROGRAM(NOM-PROG-XCTL)                                
      *                   COMMAREA(Z-COMMAREA)                                  
      *                   LENGTH(LONG-COMMAREA)                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS XCTL PROGRAM(NOM-PROG-XCTL)                                
                          COMMAREA(Z-COMMAREA)                                  
                          LENGTH(LONG-COMMAREA)                                 
           END-EXEC.                                                            
      *}                                                                        
      *=================================================================        
      *S11-COPY SECTION. CONTINUE. COPY SYKCSPAG.                               
       S12-COPY SECTION. CONTINUE. COPY SYKCST00.                               
       S13-COPY SECTION. CONTINUE. COPY SYKCSTRT.                               
      *S14-COPY SECTION. CONTINUE. COPY SYKCXCTL.                               
       S15-COPY SECTION. CONTINUE. COPY SYKCLINK.                               
      *S16-COPY SECTION. CONTINUE. COPY SYKCSWAP.                               
      *S17-COPY SECTION. CONTINUE. COPY SYKXSWAP.                               
       S18-COPY SECTION. CONTINUE. COPY SYKCRETR.                               
      *                                                                         
      * -AIDA *********************************************************         
      *        MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES              
      *****************************************************************         
       T01-COPY SECTION. CONTINUE. COPY SYKPAGE0.                               
       T02-COPY SECTION. CONTINUE. COPY SYKPDATE.                               
       T03-COPY SECTION. CONTINUE. COPY SYKPDECI.                               
       T04-COPY SECTION. CONTINUE. COPY SYKPVALN.                               
       T05-COPY SECTION. CONTINUE. COPY SYKPTIME.                               
       T06-COPY SECTION. CONTINUE. COPY SYKPMONT.                               
       T07-COPY SECTION. CONTINUE. COPY SYKPCADD.                               
       99-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
