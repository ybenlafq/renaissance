      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     MHLPM.                                   
       AUTHOR. PROJET DSA0.                                                     
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *==============================================================           
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  P                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  P                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  C                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  C                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L                         PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  L                         PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-A                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I-A                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-L                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  I-L                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RP                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  RP                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CP                        PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  CP                        PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  UN                        PIC S9(4)    COMP  VALUE +1.           
      *--                                                                       
           05  UN                        PIC S9(4) COMP-5  VALUE +1.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-CURPOS                  PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-CURPOS                  PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  ZCOMPA.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ZCOMP                     PIC S9(4)    COMP.                 
      *--                                                                       
               10  ZCOMP                     PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LMAP                      PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LMAP                      PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LG-SEND                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LG-SEND                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LG-RECV                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  LG-RECV                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD1                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD1                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD2                       PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  AD2                       PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-ADRCHAMP                PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  W-ADRCHAMP                PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TS-LENGTH                 PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  TS-LENGTH                 PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZIO-POS                   PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  ZIO-POS                   PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZIO-LG                    PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  ZIO-LG                    PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-CTLCHAR                 PIC X        VALUE X'02'.              
      *--                                                                       
           05  W-CTLCHAR                 PIC X        VALUE X'02'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SF-SET                    PIC X        VALUE X'1D'.              
      *--                                                                       
           05  SF-SET                    PIC X        VALUE X'1D'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SBA-SET                   PIC X        VALUE X'11'.              
      *--                                                                       
           05  SBA-SET                   PIC X        VALUE X'11'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  IC-SET                    PIC X        VALUE X'13'.              
      *--                                                                       
           05  IC-SET                    PIC X        VALUE X'13'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  PT-SET                    PIC X        VALUE X'05'.              
      *--                                                                       
           05  PT-SET                    PIC X        VALUE X'09'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  RA-SET                    PIC X        VALUE X'3C'.              
      *--                                                                       
           05  RA-SET                    PIC X        VALUE X'14'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  EAU-SET                   PIC X        VALUE X'12'.              
      *--                                                                       
           05  EAU-SET                   PIC X        VALUE X'12'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SFE-SET                   PIC X        VALUE X'29'.              
      *--                                                                       
           05  SFE-SET                   PIC X        VALUE X'89'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  SA-SET                    PIC X        VALUE X'28'.              
      *--                                                                       
           05  SA-SET                    PIC X        VALUE X'88'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  MF-SET                    PIC X        VALUE X'2C'.              
      *--                                                                       
           05  MF-SET                    PIC X        VALUE X'8C'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  FA-SET                    PIC X        VALUE X'C0'.              
      *--                                                                       
           05  FA-SET                    PIC X        VALUE X'E9'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  APL-SET                   PIC X        VALUE X'08'.              
      *--                                                                       
           05  APL-SET                   PIC X        VALUE X'97'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  DFH-ASK                   PIC X        VALUE X'F0'.              
      *--                                                                       
           05  DFH-ASK                   PIC X        VALUE X'30'.              
      *}                                                                        
           05  SFIELD.                                                          
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  FILLER            PIC X(6)     VALUE X'00064000F102'.        
      *--                                                                       
               10  FILLER            PIC X(6)     VALUE X'008620003102'.        
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *        10  SRP               PIC X(5)     VALUE X'0005090001'.          
      *--                                                                       
               10  SRP               PIC X(5)     VALUE X'00098D0001'.          
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QUERY                 PIC X(5)     VALUE X'000501FF02'.          
      *--                                                                       
           05  QUERY                 PIC X(5)     VALUE X'000901FF02'.          
      *}                                                                        
           05  W-EXTDS               PIC X        VALUE LOW-VALUE.              
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QR                    PIC X        VALUE X'81'.                  
      *--                                                                       
           05  QR                    PIC X        VALUE X'61'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QRC                   PIC X        VALUE X'86'.                  
      *--                                                                       
           05  QRC                   PIC X        VALUE X'66'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QREH                  PIC X        VALUE X'87'.                  
      *--                                                                       
           05  QREH                  PIC X        VALUE X'67'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QRUA                  PIC X        VALUE X'81'.                  
      *--                                                                       
           05  QRUA                  PIC X        VALUE X'61'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QRSF                  PIC X        VALUE X'88'.                  
      *--                                                                       
           05  QRSF                  PIC X        VALUE X'68'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QRSS                  PIC X        VALUE X'85'.                  
      *--                                                                       
           05  QRSS                  PIC X        VALUE X'65'.                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  QRIP                  PIC X        VALUE X'A6'.                  
      *--                                                                       
           05  QRIP                  PIC X        VALUE X'77'.                  
      *}                                                                        
           05  TAB-ADDR.                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'40C1C2C3C4C5C6C7C8C94A4B4C4D4E4F'.                         
      *--                                                                       
                   X'20414243444546474849B02E3C282B21'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'50D1D2D3D4D5D6D7D8D95A5B5C5D5E5F'.                         
      *--                                                                       
                   X'264A4B4C4D4E4F505152A7242A293B5E'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'6061E2E3E4E5E6E7E8E96A6B6C6D6E6F'.                         
      *--                                                                       
                   X'2D2F535455565758595AF92C255F3E3F'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'F0F1F2F3F4F5F6F7F8F97A7B7C7D7E7F'.                         
      *--                                                                       
                   X'303132333435363738393AA3E0273D22'.                         
      *}                                                                        
           05  FILLER      REDEFINES TAB-ADDR.                                  
               10  T-ADDR                PIC X        OCCURS 64.                
           05  MINUSCULES                PIC X(26)    VALUE                     
               'abcdefghijklmnopqrstuvwxyz'.                                    
           05  MAJUSCULES                PIC X(26)    VALUE                     
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.                                    
           05  NOM-TS-SCRN.                                                     
               10  TS-SCRN-TRMID         PIC X(4)     VALUE SPACES.             
               10  TS-SCRN-CATT          PIC X(4)     VALUE 'scra'.             
           COPY TSHELP.                                                         
           COPY SYKWECRA.                                                       
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER          PIC X OCCURS 4096 DEPENDING ON EIBCALEN.         
       01  ZIO-ECRAN       PIC X(4096).                                         
      *=================================================================        
       PROCEDURE DIVISION.                                                      
           IF EIBCALEN = ZERO                                                   
              MOVE EIBTRMID            TO TS-MAP-TRMID                          
           ELSE                                                                 
              MOVE DFHCOMMAREA         TO NOM-TS-MAP                            
           END-IF.                                                              
           MOVE EIBTRMID            TO TS-SCRN-TRMID.                           
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-MAP)                               
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-MAP)                               
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
           PERFORM RECEIVE-MESSAGE.                                             
           PERFORM POSITIONNER-CHAMP.                                           
           COMPUTE TS-LGMESS = P - 1.                                           
           COMPUTE TS-LENGTH = TS-LGMESS + LENGTH OF TS-CDIV.                   
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS WRITEQ TS QUEUE(NOM-TS-MAP)                                
      *                        FROM(TS-HLP-ECRAN)                               
      *                        LENGTH(TS-LENGTH)                                
      *                        NOHANDLE                                         
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS WRITEQ TS QUEUE(NOM-TS-MAP)                                
                               FROM(TS-HLP-ECRAN)                               
                               LENGTH(TS-LENGTH)                                
                               NOHANDLE                                         
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LINK PROGRAM('MHLPP')                                      
      *                   COMMAREA(TS-HLP-ECRAN)                                
      *                   LENGTH(TS-LENGTH)                                     
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS LINK PROGRAM('MHLPP')                                      
                          COMMAREA(TS-HLP-ECRAN)                                
                          LENGTH(TS-LENGTH)                                     
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN         END-EXEC.                                   
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       RECEIVE-MESSAGE                 SECTION.                                 
      *---------------------------------------                                  
           INITIALIZE TS-HLP-ECRAN.                                             
           MOVE LOW-VALUE           TO TS-DCOLOR.                               
           MOVE DFH-BLUE            TO TS-BLUE.                                 
           MOVE DFH-RED             TO TS-RED.                                  
           MOVE DFH-PINK            TO TS-PINK.                                 
           MOVE DFH-GREEN           TO TS-GREEN.                                
           MOVE DFH-TURQ            TO TS-TURQ.                                 
           MOVE DFH-YELLO           TO TS-YELLOW.                               
           MOVE DFH-NEUTR           TO TS-NEUTR.                                
           MOVE DFH-BASE            TO TS-DHILI.                                
           MOVE DFH-BLINK           TO TS-BLINK.                                
           MOVE DFH-REVRS           TO TS-REVRS.                                
           MOVE DFH-UNDLN           TO TS-UNDLN.                                
           MOVE SF-SET              TO TS-FSET.                                 
           MOVE LOW-VALUE           TO TS-APLTXT.                               
           MOVE LOW-VALUE           TO TS-14BITS.                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS READQ TS QUEUE(NOM-TS-SCRN)                                
      *                       INTO(TS-SCRNATT)                                  
      *                       LENGTH(LENGTH OF TS-SCRNATT)                      
      *                       ITEM(UN)                                          
      *                       NOHANDLE                                          
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS READQ TS QUEUE(NOM-TS-SCRN)                                
                              INTO(TS-SCRNATT)                                  
                              LENGTH(LENGTH OF TS-SCRNATT)                      
                              ITEM(UN)                                          
                              NOHANDLE                                          
           END-EXEC.                                                            
      *}                                                                        
           IF EIBRESP NOT = ZERO                                                
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ASSIGN EXTDS(W-EXTDS)                                   
      *                        SCRNWD(TS-SCRNWD)                                
      *                        SCRNHT(TS-SCRNHT)                                
      *                 END-EXEC                                                
      *--                                                                       
              EXEC CICS ASSIGN EXTDS(W-EXTDS)                                   
                               SCRNWD(TS-SCRNWD)                                
                               SCRNHT(TS-SCRNHT)                                
              END-EXEC                                                          
      *}                                                                        
              IF W-EXTDS = HIGH-VALUE                                           
                 PERFORM DEVICE-CHARACTERISTICS                                 
              END-IF                                                            
           ELSE                                                                 
              MOVE HIGH-VALUE       TO W-EXTDS                                  
           END-IF.                                                              
           IF W-EXTDS = HIGH-VALUE                                              
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS SEND FROM(SRP)                                          
      *                      LENGTH(LENGTH OF SRP)                              
      *                      STRFIELD                                           
      *                 END-EXEC                                                
      *--                                                                       
      *{Post-translation Comment-Temp-Instr
      *        EXEC CICS SEND FROM(SRP)                                          
      *                       LENGTH(LENGTH OF SRP)                              
      *                       STRFIELD                                           
      *        END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
           MOVE 4096                TO LMAP.                                    
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RECEIVE SET(ADDRESS OF ZIO-ECRAN)                          
      *                      LENGTH(LMAP)                                       
      *                      ASIS                                               
      *                      BUFFER                                             
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS RECEIVE SET(ADDRESS OF ZIO-ECRAN)                          
                             LENGTH(LMAP)                                       
                             ASIS                                               
                             BUFFER                                             
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBCPOSN            TO W-CURPOS.                                
      *---------------------------------------                                  
       DEVICE-CHARACTERISTICS          SECTION.                                 
      *---------------------------------------                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(LG-SEND)                                         
      *                   LENGTH(LG-SEND)                                       
      *                   CTLCHAR(W-CTLCHAR)                                    
      *                   WAIT                                                  
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(LG-SEND)                                         
                          LENGTH(LG-SEND)                                       
                          CTLCHAR(W-CTLCHAR)                                    
                          WAIT                                                  
           END-EXEC.                                                            
      *}                                                                        
           MOVE 4096                TO LMAP.                                    
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS CONVERSE FROM(QUERY)                                       
      *                       FROMLENGTH(LENGTH OF QUERY)                       
      *                       SET(ADDRESS OF ZIO-ECRAN)                         
      *                       TOLENGTH(LMAP)                                    
      *                       STRFIELD                                          
      *              END-EXEC.                                                  
      *--                                                                       
      *{Post-translation Comment-Temp-Instr      
      *     EXEC CICS CONVERSE FROM(QUERY)                                       
      *                        FROMLENGTH(LENGTH OF QUERY)                       
      *                        SET(ADDRESS OF ZIO-ECRAN)                         
      *                        TOLENGTH(LMAP)                                    
      *                        STRFIELD                                          
      *     END-EXEC.                                                            
      *}
      *}                                                                        
           MOVE 1                      TO ZIO-POS.                              
           MOVE ZIO-ECRAN (ZIO-POS:2)  TO ZCOMPA.                               
           MOVE ZCOMP                  TO ZIO-LG.                               
           PERFORM VARYING I FROM 3 BY 1                                        
                   UNTIL   I > LMAP                                             
              IF ZIO-ECRAN (I:1) = QR                                           
                 ADD  1                TO I                                     
                 IF ZIO-ECRAN (I:1)    = QRC                                    
                    PERFORM QUERY-REPLY-COLOR                                   
                 END-IF                                                         
                 IF ZIO-ECRAN (I:1)    = QREH                                   
                    PERFORM QUERY-REPLY-HILI                                    
                 END-IF                                                         
                 IF ZIO-ECRAN (I:1)    = QRUA                                   
                    PERFORM QUERY-REPLY-AREA                                    
                 END-IF                                                         
                 IF ZIO-ECRAN (I:1)    = QRSF                                   
                    PERFORM QUERY-REPLY-STRFIELD                                
                 END-IF                                                         
                 IF ZIO-ECRAN (I:1)    = QRSS                                   
                    PERFORM QUERY-REPLY-SYMBOLS                                 
                 END-IF                                                         
                 IF ZIO-ECRAN (I:1)    = QRIP                                   
                    PERFORM QUERY-REPLY-PARTITION                               
                 END-IF                                                         
                 ADD  ZIO-LG                 TO ZIO-POS                         
                 MOVE ZIO-ECRAN (ZIO-POS:2)  TO ZCOMPA                          
                 MOVE ZCOMP                  TO ZIO-LG                          
                 COMPUTE I = ZIO-POS + 1                                        
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE ZERO             TO LG-SEND                                     
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(LG-SEND)                                         
      *                   LENGTH(LG-SEND)                                       
      *                   CTLCHAR(W-CTLCHAR)                                    
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(LG-SEND)                                         
                          LENGTH(LG-SEND)                                       
                          CTLCHAR(W-CTLCHAR)                                    
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS WRITEQ TS QUEUE(NOM-TS-SCRN)                               
      *                        FROM(TS-SCRNATT)                                 
      *                        LENGTH(LENGTH OF TS-SCRNATT)                     
      *                        MAIN                                             
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS WRITEQ TS QUEUE(NOM-TS-SCRN)                               
                               FROM(TS-SCRNATT)                                 
                               LENGTH(LENGTH OF TS-SCRNATT)                     
                               MAIN                                             
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       QUERY-REPLY-COLOR               SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 4.                                                   
      *    ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-DCOLOR.                               
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-BLUE.                                 
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-RED.                                  
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-PINK.                                 
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-GREEN.                                
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-TURQ.                                 
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-YELLOW.                               
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-NEUTR.                                
      *---------------------------------------                                  
       QUERY-REPLY-HILI                SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 2.                                                   
           MOVE ZIO-ECRAN (P:1)     TO TS-DHILI.                                
           ADD  3                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-BLINK.                                
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-REVRS.                                
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:1)     TO TS-UNDLN.                                
      *---------------------------------------                                  
       QUERY-REPLY-AREA                SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 1.                                                   
           MOVE ZERO                TO ZCOMP.                                   
           MOVE ZIO-ECRAN (P:1)     TO ZCOMPA (2:1).                            
           COMPUTE ZCOMP = ZCOMP * 128.                                         
           MOVE LOW-VALUE           TO ZCOMPA (1:1).                            
           COMPUTE ZCOMP = ZCOMP / 128.                                         
           MOVE ZCOMPA (2:1)        TO TS-14BITS.                               
      *---------------------------------------                                  
       QUERY-REPLY-PARTITION           SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 1.                                                   
           ADD  5                   TO P.                                       
           MOVE ZIO-ECRAN (P:2)     TO ZCOMPA.                                  
           MOVE ZCOMP               TO TS-SCRNWD.                               
           ADD  2                   TO P.                                       
           MOVE ZIO-ECRAN (P:2)     TO ZCOMPA.                                  
           MOVE ZCOMP               TO TS-SCRNHT.                               
      *---------------------------------------                                  
       QUERY-REPLY-STRFIELD            SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 2.                                                   
      *{ Tr-Hexa-Map 1.5                                                        
      *    IF ZIO-ECRAN (P:1) = X'01'                                           
      *--                                                                       
           IF ZIO-ECRAN (P:1) = X'01'                                           
      *}                                                                        
              MOVE SFE-SET          TO TS-FSET                                  
           ELSE                                                                 
              MOVE SF-SET           TO TS-FSET                                  
           END-IF.                                                              
      *---------------------------------------                                  
       QUERY-REPLY-SYMBOLS             SECTION.                                 
      *---------------------------------------                                  
           COMPUTE P = I + 1.                                                   
           MOVE ZERO                TO ZCOMP.                                   
           MOVE ZIO-ECRAN (P:1)     TO ZCOMPA (2:1).                            
           IF ZCOMP NOT < 128                                                   
              MOVE HIGH-VALUE          TO TS-APLTXT                             
           END-IF.                                                              
      *---------------------------------------                                  
       POSITIONNER-CHAMP               SECTION.                                 
      *---------------------------------------                                  
           MOVE 1                   TO I.                                       
           MOVE 1                   TO P.                                       
           MOVE ZERO                TO C.                                       
           PERFORM UNTIL   I > LMAP                                             
              IF ZIO-ECRAN (I:1) = SF-SET                                       
              OR ZIO-ECRAN (I:1) = SFE-SET                                      
              OR ZIO-ECRAN (I:1) = MF-SET                                       
                 PERFORM POSITIONNER-ADRESSE                                    
                 PERFORM CHAMP-ECRAN                                            
              ELSE                                                              
                 IF ZIO-ECRAN (I:1) = SBA-SET                                   
                    MOVE ZIO-ECRAN (I:3) TO TS-MESSAGE (P:3)                    
                    COMPUTE I = I + 3                                           
                    COMPUTE P = P + 3                                           
                    PERFORM CHAMP-ECRAN                                         
                 ELSE                                                           
                    IF ZIO-ECRAN (I:1) = SA-SET                                 
                    OR ZIO-ECRAN (I:1) = EAU-SET                                
                       MOVE ZIO-ECRAN (I:3) TO TS-MESSAGE (P:3)                 
                       COMPUTE I = I + 3                                        
                       COMPUTE P = P + 3                                        
                    ELSE                                                        
                       IF ZIO-ECRAN (I:1) NOT = IC-SET     AND                  
                          ZIO-ECRAN (I:1) NOT = PT-SET                          
                          IF ZIO-ECRAN (I:1) NOT = APL-SET                      
                             COMPUTE C = C + 1                                  
                          END-IF                                                
                          IF ZIO-ECRAN (I:1) NOT = LOW-VALUE                    
                             MOVE ZIO-ECRAN (I:1)   TO TS-MESSAGE (P:1)         
                             COMPUTE P = P + 1                                  
                          END-IF                                                
                       END-IF                                                   
                       COMPUTE I = I + 1                                        
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE SBA-SET           TO TS-MESSAGE (P:1).                          
           ADD  1                 TO P.                                         
      *{ Tr-Hexa-Map 1.5                                                        
      *    IF TS-14BITS = X'01'                                                 
      *--                                                                       
           IF TS-14BITS = X'01'                                                 
      *}                                                                        
              MOVE TS-CURPOS         TO ZCOMP                                   
              MOVE ZCOMPA            TO TS-MESSAGE (P:2)                        
              ADD  2                 TO P                                       
           ELSE                                                                 
              DIVIDE TS-CURPOS BY 64 GIVING AD1 REMAINDER AD2                   
              COMPUTE AD1 = AD1 + 1                                             
              COMPUTE AD2 = AD2 + 1                                             
              MOVE T-ADDR (AD1)      TO TS-MESSAGE (P:1)                        
              ADD  1                 TO P                                       
              MOVE T-ADDR (AD2)      TO TS-MESSAGE (P:1)                        
              ADD  1                 TO P                                       
           END-IF.                                                              
           MOVE IC-SET            TO TS-MESSAGE (P:1).                          
           ADD  1                 TO P.                                         
           MOVE P                 TO TS-ADRCHAMP.                               
           PERFORM VARYING I FROM W-ADRCHAMP BY 1                               
                   UNTIL   I > LMAP                                             
                   OR      ZIO-ECRAN (I:1) = SBA-SET                            
                   OR      ZIO-ECRAN (I:1) = SF-SET                             
                   OR      ZIO-ECRAN (I:1) = SFE-SET                            
                   OR      ZIO-ECRAN (I:1) = MF-SET                             
                   OR      ZIO-ECRAN (I:1) = IC-SET                             
              IF ZIO-ECRAN (I:1) = SA-SET                                       
              OR ZIO-ECRAN (I:1) = SBA-SET                                      
              OR ZIO-ECRAN (I:1) = EAU-SET                                      
                 COMPUTE I = I + 2                                              
              ELSE                                                              
                 IF ZIO-ECRAN (I:1) NOT = IC-SET     AND                        
                    ZIO-ECRAN (I:1) NOT = PT-SET                                
                    MOVE ZIO-ECRAN (I:1) TO TS-MESSAGE (P:1)                    
                    ADD  1                TO P                                  
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           INSPECT TS-MESSAGE (TS-ADRCHAMP:TS-LGCHAMP)                          
                   CONVERTING MINUSCULES TO MAJUSCULES.                         
           IF TS-MESSAGE (CP:1) = SF-SET                                        
              ADD  1                   TO CP                                    
              PERFORM ATT-MDT-ON                                                
           END-IF.                                                              
           IF TS-MESSAGE (CP:1) = SFE-SET                                       
           OR TS-MESSAGE (CP:1) = MF-SET                                        
              ADD  1                      TO CP                                 
              MOVE ZERO                   TO ZCOMP                              
              MOVE TS-MESSAGE (CP:1)      TO ZCOMPA                             
              MOVE ZCOMP                  TO L                                  
              ADD  1                      TO CP                                 
              PERFORM VARYING I FROM 1 BY 1                                     
                      UNTIL   I > L                                             
                 IF TS-MESSAGE (CP:1) = FA-SET                                  
                    ADD  1                   TO CP                              
                    PERFORM ATT-MDT-ON                                          
                 ELSE                                                           
                    ADD  2                   TO CP                              
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      *---------------------------------------                                  
       ATT-MDT-ON                      SECTION.                                 
      *---------------------------------------                                  
           MOVE ZERO                TO ZCOMP.                                   
           MOVE TS-MESSAGE (CP:1)   TO ZCOMPA (2:1).                            
           COMPUTE ZCOMP = ZCOMP * (2 ** 7).                                    
           MOVE LOW-VALUE           TO ZCOMPA (2:1).                            
           COMPUTE ZCOMP = ZCOMP / (2 ** 7).                                    
           IF ZCOMP = ZERO                                                      
              PERFORM VARYING AD1 FROM 1 BY 1                                   
                      UNTIL   AD1 > 63                                          
                      OR T-ADDR (AD1) = TS-MESSAGE (CP:1)                       
              END-PERFORM                                                       
              IF AD1 > 63                                                       
      *{ normalize-exec-xx 1.5                                                  
      *          EXEC CICS ABEND ABCODE('ADDM') END-EXEC                        
      *--                                                                       
                 EXEC CICS ABEND ABCODE('ADDM')                                 
                 END-EXEC                                                       
      *}                                                                        
              ELSE                                                              
                 ADD  1                TO AD1                                   
                 MOVE T-ADDR (AD1)     TO TS-MESSAGE (CP:1)                     
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       POSITIONNER-ADRESSE             SECTION.                                 
      *---------------------------------------                                  
           MOVE SBA-SET             TO TS-MESSAGE (P:1).                        
           ADD  1                   TO P.                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *    IF TS-14BITS = X'01'                                                 
      *--                                                                       
           IF TS-14BITS = X'01'                                                 
      *}                                                                        
              MOVE C                 TO ZCOMP                                   
              MOVE ZCOMPA            TO TS-MESSAGE (P:2)                        
              ADD  2                 TO P                                       
           ELSE                                                                 
              DIVIDE C BY 64 GIVING AD1 REMAINDER AD2                           
              COMPUTE AD1 = AD1 + 1                                             
              COMPUTE AD2 = AD2 + 1                                             
              MOVE T-ADDR (AD1)      TO TS-MESSAGE (P:1)                        
              ADD  1                 TO P                                       
              MOVE T-ADDR (AD2)      TO TS-MESSAGE (P:1)                        
              ADD  1                 TO P                                       
           END-IF.                                                              
      *---------------------------------------                                  
       CHAMP-ECRAN                     SECTION.                                 
      *---------------------------------------                                  
           MOVE P                   TO RP.                                      
           IF ZIO-ECRAN (I:1) = SF-SET                                          
              MOVE ZIO-ECRAN (I:2)  TO TS-MESSAGE (P:2)                         
              COMPUTE I = I + 2                                                 
              COMPUTE P = P + 2                                                 
           ELSE                                                                 
              IF ZIO-ECRAN (I:1) = SFE-SET                                      
              OR ZIO-ECRAN (I:1) = MF-SET                                       
                 MOVE ZIO-ECRAN (I:2)    TO TS-MESSAGE (P:2)                    
                 COMPUTE I = I + 1                                              
                 MOVE ZERO               TO ZCOMP                               
                 MOVE ZIO-ECRAN (I:1)    TO ZCOMPA (2:1)                        
                 COMPUTE I = I + 1                                              
                 COMPUTE P = P + 2                                              
                 COMPUTE L = ZCOMP * 2                                          
                 MOVE ZIO-ECRAN (I:L)    TO TS-MESSAGE (P:L)                    
                 COMPUTE I = I + L                                              
                 COMPUTE P = P + L                                              
              END-IF                                                            
           END-IF.                                                              
           IF C = W-CURPOS                                                      
              ADD  1                TO W-CURPOS                                 
           END-IF.                                                              
           IF C > W-CURPOS                                                      
              IF TS-LGCHAMP = ZERO                                              
                 COMPUTE TS-LGCHAMP = C  - TS-CURPOS                            
              END-IF                                                            
           END-IF.                                                              
           ADD  1                TO C.                                          
           IF C NOT > W-CURPOS                                                  
              MOVE C                TO TS-CURPOS                                
              MOVE I                TO W-ADRCHAMP                               
              MOVE RP               TO CP                                       
           END-IF.                                                              
