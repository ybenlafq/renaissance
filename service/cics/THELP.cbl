      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     THELP.                                   
       AUTHOR. PROJET DSA0.                                                     
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *==============================================================           
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TS-LENGTH                 PIC S9(4)    COMP  VALUE +0.           
      *--                                                                       
           05  TS-LENGTH                 PIC S9(4) COMP-5  VALUE +0.            
      *}                                                                        
           05  NOM-TS-CMA.                                                      
               10  FILLER                PIC X(4)     VALUE 'HELP'.             
               10  TS-CMA-TRMID          PIC X(4)     VALUE SPACES.             
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA.                                                         
           05  FILLER          PIC X OCCURS 4096 DEPENDING ON EIBCALEN.         
      *=================================================================        
       PROCEDURE DIVISION.                                                      
           MOVE EIBTRMID            TO TS-CMA-TRMID.                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS-CMA)                               
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS-CMA)                               
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBCALEN            TO TS-LENGTH.                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS WRITEQ TS QUEUE(NOM-TS-CMA)                                
      *                        FROM(DFHCOMMAREA)                                
      *                        LENGTH(TS-LENGTH)                                
      *                        NOHANDLE                                         
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS WRITEQ TS QUEUE(NOM-TS-CMA)                                
                               FROM(DFHCOMMAREA)                                
                               LENGTH(TS-LENGTH)                                
                               NOHANDLE                                         
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LINK PROGRAM('MHLPM')                                      
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS LINK PROGRAM('MHLPM')                                      
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS START TRANSID('HLP1')                                      
      *                    TERMID(EIBTRMID)                                     
      *                    NOHANDLE                                             
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS START TRANSID('HLP1')                                      
                           TERMID(EIBTRMID)                                     
                           NOHANDLE                                             
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN         END-EXEC.                                   
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
