      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  TGX02.                                                      
       AUTHOR. GILLES E.                                                        
      *                                                                         
      * MODULE DE COMMUNICATION APPC HOST ---->  36                             
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       CONFIGURATION SECTION.                                                   
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
                                                                                
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
       01  MESS          PIC X(40)          VALUE SPACES.                       
       01  CONVID        PIC X(08)          VALUE SPACES.                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RECVLGR       PIC S9(4) COMP     VALUE 0.                            
      *--                                                                       
       01  RECVLGR       PIC S9(4) COMP-5     VALUE 0.                          
      *}                                                                        
       01  I             PIC 9(3)           VALUE 0.                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  IA            PIC S9(4) COMP          VALUE 0.                       
      *--                                                                       
       01  IA            PIC S9(4) COMP-5          VALUE 0.                     
      *}                                                                        
       01  NBITEM        PIC 9(3)           VALUE 0.                            
       01  NOM-TS        PIC X(8).                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  ITEM          PIC S9(4) COMP     VALUE 0.                            
      *--                                                                       
       01  ITEM          PIC S9(4) COMP-5     VALUE 0.                          
      *}                                                                        
       01  TS-DATA                    PIC X(40).                                
       01  TS-DONNEES.                                                          
           03 TS-NBITEM               PIC  9(3).                                
           03 TS-PROC                 PIC  X(6).                                
           03 FILLER                  PIC  X(4087).                             
       01  ZONE-LINK.                                                           
           03 LINK-NOM-TS          PIC X(8).                            00320000
           03 LINK-PROC-BIBL.                                                   
              04 LINK-PB           PIC X OCCURS 17.                             
           03 LINK-LIEU            PIC X(6).                                    
           03 LINK-TERM            PIC X(4).                                    
           03 LINK-REMOTESYST      PIC X(4).                                    
           03 LINK-RET             PIC 9(1).                            00320000
       01  Z-COMMAREA-LINK            PIC X(200).                               
           COPY SYKWEIB0.                                                       
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05  FILLER       PIC X(50).                                          
       PROCEDURE DIVISION.                                                      
           MOVE DFHCOMMAREA  TO  ZONE-LINK.                                     
           EXEC CICS HANDLE CONDITION                                           
                INVREQ   (MESSAGE1)                                             
                SESSBUSY (MESSAGE2)                                             
                SYSBUSY  (MESSAGE3)                                             
                SYSIDERR (MESSAGE4)                                             
                NOTALLOC (MESSAGE5)                                             
           END-EXEC.                                                            
           EXEC CICS ALLOCATE                                                   
                SYSID    (LINK-REMOTESYST)                                      
                NOQUEUE                                                         
           END-EXEC.                                                            
           MOVE  EIBRSRCE    TO    CONVID.                                      
           MOVE  'TSRETOUR'  TO    NOM-TS.                                      
           EXEC CICS DELETEQ TS QUEUE (NOM-TS)                                  
                     NOHANDLE                                                   
           END-EXEC.                                                            
           MOVE   1           TO    ITEM.                                       
           PERFORM LECTURE-TS-DONNEES.                                          
           MOVE EIBRCODE  TO  EIB-RCODE.                                        
           IF NOT EIB-NORMAL                                                    
              PERFORM RETOUR                                                    
           END-IF.                                                              
           MOVE TS-NBITEM TO NBITEM.                                            
           PERFORM VARYING IA FROM 17 BY -1 UNTIL (IA = 0) OR                   
                                   (LINK-PB (IA) NOT = SPACES)                  
                                       END-PERFORM.                             
           EXEC CICS CONNECT PROCESS                                            
                CONVID   (CONVID)                                               
                PROCNAME (LINK-PROC-BIBL)                                       
                PROCLENGTH (IA)                                                 
                SYNCLEVEL  (0)                                                  
           END-EXEC.                                                            
           PERFORM VARYING I FROM 1 BY 1 UNTIL I = NBITEM                       
              EXEC CICS SEND                                                    
                   CONVID   (CONVID)                                            
                   FROM     (TS-DONNEES)                                        
                   LENGTH   (4096)                                              
                   WAIT                                                         
              END-EXEC                                                          
              IF EIBERR  =  HIGH-VALUE                                          
                 PERFORM YAERR                                                  
              END-IF                                                            
              ADD 1 TO ITEM                                                     
              PERFORM LECTURE-TS-DONNEES                                        
              MOVE EIBRCODE  TO  EIB-RCODE                                      
           END-PERFORM.                                                         
           EXEC CICS SEND                                                       
                CONVID   (CONVID)                                               
                FROM     (TS-DONNEES)                                           
                LENGTH   (4096)                                                 
                LAST                                                            
           END-EXEC.                                                            
           IF EIBERR  =  HIGH-VALUE                                             
              PERFORM YAERR                                                     
           END-IF.                                                              
           MOVE  0        TO LINK-RET                                           
           PERFORM RETOUR.                                                      
      *---------------------------------------------------------------          
      *                    LECTURE TS DE DONNEES                                
      *---------------------------------------------------------------          
       LECTURE-TS-DONNEES SECTION.                                              
           EXEC  CICS  READQ  TS  QUEUE  (LINK-NOM-TS)                          
                                  INTO   (TS-DONNEES)                           
                                  ITEM   (ITEM)                                 
                                  NOHANDLE                                      
           END-EXEC.                                                            
       F-LECTURE-TS-DONNEES. EXIT.                                              
      *---------------------------------------------------------------          
      *                    ECRITURE TS RETOUR                                   
      *---------------------------------------------------------------          
       ECRITURE-TS-RETOUR SECTION.                                              
           MOVE  40          TO    RECVLGR.                                     
           MOVE  'TSRETOUR'  TO    NOM-TS.                                      
           EXEC CICS WRITEQ TS QUEUE (NOM-TS)                                   
                     FROM (TS-DATA)                                             
                     LENGTH  (RECVLGR)                                          
                     NOHANDLE                                                   
           END-EXEC.                                                            
       F-ECRITURE-TS-RETOUR. EXIT.                                              
      *----------------------------------------------------------------         
      *                  MESSAGES  HANDLES CONDITION                            
      *----------------------------------------------------------------         
       MESSAGE1  SECTION.                                                       
           MOVE 'ALLOCATE - INVREQ'     TO  TS-DATA.                            
           PERFORM ECRITURE-TS-RETOUR.                                          
           PERFORM FREE-CONVID.                                                 
           PERFORM RETOUR.                                                      
       F-MESSAGE1. EXIT.                                                        
      *-----------------------------------------------------------------        
       MESSAGE2  SECTION.                                                       
           STRING LINK-REMOTESYST                                               
           ' ALLOCATE - SESSBUSY'  DELIMITED BY SIZE INTO  TS-DATA.             
           PERFORM ECRITURE-TS-RETOUR.                                          
           PERFORM FREE-CONVID.                                                 
           PERFORM RETOUR.                                                      
       F-MESSAGE2. EXIT.                                                        
      *-----------------------------------------------------------------        
       MESSAGE3  SECTION.                                                       
           STRING LINK-REMOTESYST                                               
           ' ALLOCATE - SYSBUSY' DELIMITED BY SIZE INTO  TS-DATA.               
           PERFORM ECRITURE-TS-RETOUR.                                          
      *    PERFORM FREE-CONVID.                                                 
           PERFORM RETOUR.                                                      
       F-MESSAGE3. EXIT.                                                        
      *-----------------------------------------------------------------        
       MESSAGE4  SECTION.                                                       
           STRING LINK-REMOTESYST                                               
           ' ALLOCATE - SYSIDERR' DELIMITED BY SIZE INTO  TS-DATA.              
           PERFORM ECRITURE-TS-RETOUR.                                          
           PERFORM FREE-CONVID.                                                 
           PERFORM RETOUR.                                                      
       F-MESSAGE4. EXIT.                                                        
      *-----------------------------------------------------------------        
       MESSAGE5  SECTION.                                                       
           STRING LINK-REMOTESYST                                               
           ' CONNECT - NOTALLOC' DELIMITED BY SIZE INTO  TS-DATA.               
           PERFORM ECRITURE-TS-RETOUR.                                          
      *    PERFORM FREE-CONVID.                                                 
           PERFORM RETOUR.                                                      
       F-MESSAGE5. EXIT.                                                        
      *----------------------------------------------------------------         
      *                  FREE  CONVID                                           
      *----------------------------------------------------------------         
       FREE-CONVID   SECTION.                                                   
           EXEC CICS FREE CONVID  (CONVID)  NOHANDLE                            
           END-EXEC.                                                            
       F-FREE-CONVID. EXIT.                                                     
      *----------------------------------------------------------------         
      *                            YAERR                                        
      *----------------------------------------------------------------         
       YAERR  SECTION.                                                          
           EXEC CICS IGNORE CONDITION ERROR                                     
           END-EXEC.                                                            
           MOVE 'ERREUR - EIBERR'   TO TS-DATA.                                 
           PERFORM ECRITURE-TS-RETOUR.                                          
           PERFORM FREE-CONVID.                                                 
       F-YAERR. EXIT.                                                           
      *----------------------------------------------------------------         
      *                            YAERR                                        
      *----------------------------------------------------------------         
       RRECV2 SECTION.                                                          
           EXEC CICS IGNORE CONDITION ERROR                                     
           END-EXEC.                                                            
           MOVE 'ERREUR - NO DATA RECV'  TO TS-DATA.                            
           PERFORM ECRITURE-TS-RETOUR.                                          
           PERFORM FREE-CONVID.                                                 
       F-RRECV2. EXIT.                                                          
      *----------------------------------------------------------------         
      *                             RETOUR                                      
      *----------------------------------------------------------------         
       RETOUR       SECTION.                                                    
           MOVE ZONE-LINK TO DFHCOMMAREA.                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
       F-RETOUR. EXIT.                                                          
