      * @(#) MetaWare Technologies Cataloguer 0.9.18
      * Translated: 19/10/2016 18:50
      * Portfolio: IMAGE01
      *!!!!TRANSLATION-ISSUE!!!!
      *    1 translation issue shown below.
      *    0 additional translation issues not shown below.
      *    1 total translation issue.
      *!!!!TRANSLATION-ISSUE!!!!
      *E1--NYS: Untranslated ON statement: RE::*UNDEFINED*
      * *** REMISE A LOW DE LA MAP ****/
      *  GESTION DE LA COMMAREA  */
      * AUTRE TACHE *********************************************/
      * 10 */
      * 10 */
      * PROT SKIP MDT-ON */
      * LA ZONE DE COMMUNICATION EST LA TWA */
      * CODE IMPRIMANTE */
      * TEST DU CODE RETOUR */
      * CALL TRAITEMENT_COMMAREA; */
      * CALL TRAITEMENT_MAP;      */
      * IF ACTION = CREATION         */
      *    THEN CALL CREATION_RECORD; */
      * IF ACTION = MODIFICATION         */
      *    THEN CALL MODIFICATION_RECORD; */
      * IF ACTION = SUPPRESSION         */
      *    THEN CALL SUPPRESSION_RECORD; */
      * TRAITEMENT DEMANDE IMPRESSION OU REPRISE */
      * TRAITEMENT DEMANDE CONSULTATION */
      * NUMERO CONFIGURATION */
      * TYPE IMPRESSION      */
      * HTR PAGE TEXTE       */
      * NBR LIGNES PAR PAGE  */
      * NOMBRE COLONNES      */
      * CODE IMPRIMANTE      */
      * PROGRAMME ORIGINE    */
      * NOM DE LA TS         */
      * LONGUEUR DE LA TS    */
      * NOMBRE ITEM DE LA TS */
      * DATE DU JOUR AAMMJJ  */
      * NUMERO CONFIGURATION */
      * TYPE IMPRESSION      */
      * LNG ZONE COMMANDES   */
      * LNG ZONE DONNEES     */
      * NUMERO CONFIGURATION */
      * TYPE IMPRESSION      */
      * LNG ZONE COMMANDES   */
      * LNG ZONE DONNEES     */
      * HTR PAGE GRAPHIQUE   */
      * CODE IMPRIMANTE      */
      * PROGRAMME ORIGINE    */
      * NOM DE LA TS         */
      * LONGUEUR DE LA TS    */
      * NOMBRE ITEM DE LA TS */
      * DATE DU JOUR AAMMJJ  */
      * TRAITEMENT IMPRESSION */
      * TRAITEMENT REPRISE    */
      * IF FONCTION = TRAITEMENT_AUTOMATIQUE        */
      *    THEN CALL REMPLISSAGE_ZONES_NO_PROTEGEES; */
      * LIBELLE ETAT            */
      * NUMERO CONFIGURATION    */
      * TYPE IMPRESSION         */
      * ATTACHEMENT IMPRIMANTES */
      * PROTOCOLE D'IMPRESSION  */
      * NOM ETAT        */
      * DATE EDITION    */
      * DESTINATION     */
      * DOCUMENT        */
      * TYPE CONSULTA   */
      * DATE EDITION    */
      * DATE IMPRESSION */
      * DATE REPRISE    */
      * DATE ANNULATION */
      * DATE CONSULTATI */
      * ABANDON * * * * * * * * * * * * * * * * * * * * * * * * * **/
      * CALL SEND_MAP_NO_ERASE; */
      *           IF GFBQT0 > 36524 THEN  */
      *              GFBS = 20;           */
      *           ELSE                    */
      *              GFBS = 19;           */
      * CALCUL ANNEE BISSEXTILE.                                */
      * UNE ANNEE EST BISSEXTILE LORSQU'ELLE EST DIVISIBLE      */
      * PAR 4 SAUF POUR LES FIN DE SIECLE QUI NE SONT PAS DES   */
      * FIN DE MILLENAIRE                                       */
       IDENTIFICATION DIVISION.
       PROGRAM-ID. TIG66.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01  S--PROGRAM                PIC X(8) VALUE 'TIG66'.
       01  ENDVAL--26                PIC S9(5).
       01  ENDVAL--27                PIC S9(5).
       01  ENDVAL--28                PIC S9(5).
       01  MW-REPEAT-CHAR--131       PIC X(131).
       01  MW-REPEAT-CHAR--8         PIC X(8).
       COPY SP-VERIFY-COPY.
       LOCAL-STORAGE SECTION.
       01  MW--LENGTH                PIC S9(4) COMP-5.
       01  MW--STG                   PIC S9(4) COMP-5.
       01  SYKWDIV0.
         05 FIL01_WDIV               PIC X(20) VALUE 'ZONES DECLAR.==>'.
         05 FIL02_WDIV               PIC X(13) VALUE 'DEBUG........'.
         05 DEBUGGIN                 PIC X(3) VALUE 'OUI'.
         05 FIL03_WDIV               PIC X(14) VALUE 'FONCTION......'.
         05 FONCTION                 PIC X(2) VALUE 'FF'.
         05 FIL04_WDIV               PIC X(15) VALUE 'KONTROL........'.
         05 KONTROL                  PIC X(1) VALUE '0'.
         05 FIL05_WDIV               PIC X(15) VALUE 'CODE_RETOUR....'.
         05 CODE_RETOUR              PIC X(1) VALUE '0'.
         05 FIL06_WDIV               PIC X(15) VALUE 'TYPE_CIRCUIT...'.
         05 TYPE_CIRCUIT             PIC X(1) VALUE '1'.
         05 FIL07_WDIV               PIC X(15) VALUE 'ETAT_ECRAN.....'.
         05 ETAT_ECRAN               PIC X(1) VALUE '0'.
         05 FIL08_WDIV               PIC X(15) VALUE 'ACTION.........'.
         05 ACTION                   PIC X(1) VALUE ' '.
      * ****************************************************************
      *                                                             ***/
      *         DECLARATIVES DES INDICES
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 FIL09_WDIV               PIC X(14) VALUE 'INDICE_IA.....'.
         05 IA                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL10_WDIV               PIC X(14) VALUE 'INDICE_AI.....'.
         05 AI                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL11_WDIV               PIC X(14) VALUE 'INDICE_IT.....'.
         05 IT                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL12_WDIV               PIC X(14) VALUE 'INDICE_LIGNE..'.
         05 IL                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL13_WDIV               PIC X(14) VALUE 'INDICE_PAGE...'.
         05 IP                       PIC S9(3) COMP-3 VALUE 0.
         05 FIL14_WDIV               PIC X(15) VALUE 'ETAT_CONSULT...'.
         05 ETAT_CONSULTATION        PIC X(1) VALUE '0'.
         05 FIL15_WDIV               PIC X(11) VALUE 'NUM_5......'.
         05 NUM_5                    PIC 9(5) VALUE 0.
         05 FIL16_WDIV               PIC X(12) VALUE 'COMPTEUR....'.
         05 COMPTEUR                 PIC S9(7) COMP-3 VALUE 0.
         05 B1                       PIC X(1) VALUE ' '.
      * COMMENTAIRE */
      * ****************************************************************
      *                                                             ***/
      *         CODE ABANDON
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 FIL17_WDIV               PIC X(15) VALUE 'CODE_ABANDON...'.
         05 CODE_ABANDON             PIC X(1) VALUE ' '.
      * ****************************************************************
      *                                                             ***/
      *         DATE ET HEURE DE COMPILATION
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 Z_WHEN_COMPILED          PIC X(18) VALUE
                                                   '24.DEC.99 11.11.11'.
      * ****************************************************************
      *                                                             ***/
      *         DIFFERENTES ZONES POUR LES ORDRES CICS
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 LONG_COMMAREA            PIC S9(4) COMP-5 VALUE +100.
         05 LONG_COMMAREA_LINK       PIC S9(4) COMP-5 VALUE +4096.
         05 LONG_START               PIC S9(4) COMP-5 VALUE +4096.
         05 LONG_TD                  PIC S9(4) COMP-5 VALUE +0.
         05 LONG_TS                  PIC S9(4) COMP-5 VALUE +0.
         05 RANG_TS                  PIC S9(4) COMP-5 VALUE +0.
         05 FILE_NAME                PIC X(8).
         05 FILE_LONG                PIC S9(4) COMP-5 VALUE +0.
      * ****************************************************************
      *                                                             ***/
      *         ZONES   DIVERSES
      *                                                               */
      * ****************************************************************
      *                                                             ***/
         05 NOM_MAP                  PIC X(7).
         05 NOM_MAPSET               PIC X(7).
         05 NOM_PROG                 PIC X(7).
         05 NOM_TACHE                PIC X(4).
         05 NOM_LEVEL_MAX            PIC X(4).
         05 NOM_PROG_XCTL            PIC X(7).
         05 NOM_PROG_LINK            PIC X(7).
         05 NOM_TACHE_RETOUR         PIC X(4).
         05 NOM_TACHE_START          PIC X(4).
         05 TERM_START               PIC X(4).
         05 NOM_TD                   PIC X(4).
         05 IDENT_TS                 PIC X(8).
         05 CODE_TRAITEMENT_NORMAL   PIC X(2) VALUE '01'.
         05 CODE_TRAITEMENT_CRITERES PIC X(2) VALUE '11'.
         05 CODE_TRAITEMENT_DATAS    PIC X(2) VALUE '22'.
         05 CODE_TRAITEMENT_AUTOMATIQUE PIC X(2) VALUE '10'.
         05 CODE_LEVEL_MAX           PIC X(2) VALUE 'KA'.
         05 CODE_HELP                PIC X(2) VALUE 'KB'.
         05 CODE_LEVEL_SUP           PIC X(2) VALUE 'KC'.
         05 CODE_SUSPENSION          PIC X(2) VALUE '03'.
         05 CODE_FIN                 PIC X(2) VALUE '04'.
         05 CODE_PREMIERE            PIC X(2) VALUE '05'.
         05 CODE_DERNIERE            PIC X(2) VALUE '06'.
         05 CODE_PRECEDENTE          PIC X(2) VALUE '07'.
         05 CODE_SUIVANTE            PIC X(2) VALUE '08'.
         05 CODE_ERREUR_MANIPULATION PIC X(2) VALUE 'FF'.
         05 CODE_SWAP                PIC X(2) VALUE 'KS'.
         05 CODE_CREATION            PIC X(1) VALUE 'C'.
         05 CODE_MODIFICATION        PIC X(1) VALUE 'M'.
         05 CODE_INTERROGATION       PIC X(1) VALUE 'I'.
         05 CODE_SUPPRESSION         PIC X(1) VALUE 'S'.
         05 CODE_FIN_BOUCLE_IA       PIC S9(3) COMP-3 VALUE +999.
         05 CODE_FIN_BOUCLE_AI       PIC S9(3) COMP-3 VALUE +999.
         05 CODE_FIN_TABLE           PIC S9(3) COMP-3 VALUE +999.
         05 CODE_CONSULTATION_EN_COURS PIC X(1) VALUE '0'.
         05 CODE_FIN_CONSULTATION    PIC X(1) VALUE '1'.
         05 APOSTROPHE               PIC X(1) VALUE ' '.
         05 BINAIRE                  PIC S9(4) COMP-5 VALUE 0.
         05 L_VAR                    PIC S9(9) COMP-5 VALUE 0.
         05 W_TRACE_CICS             PIC X(50).
       01  ZONES_SWAP.
         05 SWAP_LONG_TS             PIC S9(4) COMP-5 VALUE +6096.
         05 SWAP_FILLER_IDENT_TS.
           10 SWAP_IDENT_TS_A        PIC X(4).
           10 SWAP_IDENT_TS_B        PIC X(4).
         05 SWAP_RANG_TS             PIC S9(4) COMP-5 VALUE 0.
       01  VALEURS_INDIC.
         05 TRAITEMENT               PIC X(2) VALUE '01'.
         05 PAGINATION               PIC X(2) VALUE '05'.
         05 TRAITEMENT_NORMAL        PIC X(2) VALUE '01'.
         05 TRAITEMENT_CRITERES      PIC X(2) VALUE '11'.
         05 TRAITEMENT_DATAS         PIC X(2) VALUE '22'.
         05 TRAITEMENT_AUTOMATIQUE   PIC X(2) VALUE '10'.
         05 SUSPENSION               PIC X(2) VALUE '03'.
         05 FIN                      PIC X(2) VALUE '04'.
         05 PREMIERE                 PIC X(2) VALUE '05'.
         05 DERNIERE                 PIC X(2) VALUE '06'.
         05 PRECEDENTE               PIC X(2) VALUE '07'.
         05 SUIVANTE                 PIC X(2) VALUE '08'.
         05 LEVEL_MAX                PIC X(2) VALUE 'KA'.
         05 HELP                     PIC X(2) VALUE 'KB'.
         05 LEVEL_SUP                PIC X(2) VALUE 'KC'.
         05 ABANDON                  PIC X(2) VALUE 'KD'.
         05 ERREUR_MANIPULATION      PIC X(2) VALUE 'FF'.
         05 SWAP                     PIC X(2) VALUE 'KS'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR KONTROL
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ERREUR_AIDA              PIC X(1) VALUE '1'.
         05 ERREUR                   PIC X(1) VALUE '2'.
         05 OK                       PIC X(1) VALUE '0'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR CODE_RETOUR
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 TROUVE                   PIC X(1) VALUE '0'.
         05 NORMAL                   PIC X(1) VALUE '0'.
         05 DATE_OK                  PIC X(1) VALUE '0'.
         05 SELECTE                  PIC X(1) VALUE '0'.
         05 NON_TROUVE               PIC X(1) VALUE '1'.
         05 NON_SELECTE              PIC X(1) VALUE '1'.
         05 ANORMAL                  PIC X(1) VALUE '1'.
         05 EXISTE_DEJA              PIC X(1) VALUE '2'.
         05 FIN_FICHIER              PIC X(1) VALUE '3'.
         05 ERREUR_DATE              PIC X(1) VALUE '4'.
         05 ERREUR_FORMAT            PIC X(1) VALUE '5'.
         05 ERREUR_HEURE             PIC X(1) VALUE '6'.
         05 DOUBLE                   PIC X(1) VALUE '7'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR TYPE_CIRCUIT
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 CIRCUIT_COURT            PIC X(1) VALUE '7'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR ETAT_ECRAN
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ECRAN_MAPFAIL            PIC X(1) VALUE '1'.
      * ****************************************************************
      *                                                               */
      *    DIFFERENTES VALEURS DE L'INDICATEUR ACTION
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 CREATION                 PIC X(1) VALUE 'C'.
         05 MODIFICATION             PIC X(1) VALUE 'M'.
         05 SUPPRESSION              PIC X(1) VALUE 'S'.
         05 INTERROGATION            PIC X(1) VALUE 'I'.
      * ****************************************************************
      *                                                               */
      *    VALEURS DES FIN DE BOUCLES
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 FIN_BOUCLE_IA            PIC S9(3) COMP-3 VALUE +999.
         05 FIN_BOUCLE_AI            PIC S9(3) COMP-3 VALUE +999.
         05 FIN_TABLE                PIC S9(3) COMP-3 VALUE +999.
         05 FIN_BOUCLE_IL            PIC S9(3) COMP-3 VALUE +999.
      * ****************************************************************
      *                                                               */
      *    VALEURS DE ETAT_CONSULTATION
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 FIN_CONSULTATION         PIC X(1) VALUE '1'.
      * ****************************************************************
      *                                                               */
      *    VALEURS DE CODE_ABANDON
      *                                                               */
      * ****************************************************************
      *                                                               */
         05 ABEND_ABANDON            PIC X(1) VALUE 'A'.
         05 AIDA_ABANDON             PIC X(1) VALUE 'K'.
         05 TACHE_ABANDON            PIC X(1) VALUE 'T'.
         05 CICS_ABANDON             PIC X(1) VALUE 'C'.
         05 DL1_ABANDON              PIC X(1) VALUE 'D'.
       01  FIL_LIGNE                 PIC X(16) VALUE '**** LIGNE *****'.
       01  LIGNE                     PIC X(131).
       01  ALPHABETIQUE              PIC X(26) VALUE
                                           'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
       01  KW--ALPHANUMERIC          PIC X(36) VALUE
                                 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.
       01  NUMERIQUE                 PIC X(10) VALUE '0123456789'.
       01  IG65_TS_ZES               PIC X(1080).
       01  W_BIN_PAG                 PIC S9(4) COMP-5.
       01  W_PIC_MAX                 PIC 9999.
       01  W_BIN_MAX                 PIC S9(4) COMP-5.
       01  TACH_PIC                  PIC 9999999.
       01  ZES_IMP_REP               PIC X(37).
       01  ITEM_IMP_REP              PIC S9(4) COMP-5.
       01  ZES_CON_LIG               PIC X(37).
       01  ITEM_CON_LIG              PIC S9(4) COMP-5.
       01  NETNAME                   PIC X(8).
       01  MESYRD                    PIC X(40).
       01  TOP_CON                   PIC X(1).
       01  TOP_IMP                   PIC X(1).
       01  TOP_REP                   PIC X(1).
       01  I                         PIC S9(4) COMP-5.
       01  IA                        PIC S9(4) COMP-5.
       01  IC                        PIC S9(4) COMP-5.
       01  EIB_RCODE                 PIC X(6).
       01  FIL_EIB.
      * X'00'*/
         05 FIL_NORMAL               PIC 1(8) BIT VALUE B'00000000'.
      * X'81'*/
         05 FIL_NOTFND               PIC 1(8) BIT VALUE B'10000001'.
      * X'0F'*/
         05 FIL_ENDFILE              PIC 1(8) BIT VALUE B'00001111'.
      * X'04'*/
         05 FIL_MAPFAIL              PIC 1(8) BIT VALUE B'00000100'.
      * X'01'*/
         05 FIL_ENDDATA              PIC 1(8) BIT VALUE B'00000001'.
      * X'02'*/
         05 FIL_QIDERR               PIC 1(8) BIT VALUE B'00000010'.
      * X'01'*/
         05 FIL_QZERO                PIC 1(8) BIT VALUE B'00000001'.
      * X'01'*/
         05 FIL_ITEMERR              PIC 1(8) BIT VALUE B'00000001'.
      * X'82'*/
         05 FIL_DUPREC               PIC 1(8) BIT VALUE B'10000010'.
         05 FIL_DUPKEY               PIC 1(8) BIT VALUE B'10000100'.
       01  Z_TIMER_DATJOU_FILE       PIC X(8).
       01  Z_TIMER_DATJOU            PIC X(8) VALUE '00/00/00'.
       01  Z_TIMER_TIMJOU            PIC X(9) VALUE '00H00M00S'.
       01  Z_TIMER_0HHMMSS           PIC 9(7).
       01  Z_CWA                     PIC X(100).
       01  Z_TCTUA                   PIC X(255).
       01  GRP--INIT_BMS1.
      * WORKAID        X'00' */
      * BRT_ALP_FSET   X'C9' */
      * BRT_NUM_FSET   X'D9' */
      * BRT_PRO_FSET   X'F9' */
      * DRK_ALP_FSET   X'4D' */
      * DRK_PRO_FSET   X'7D' */
      * DRK_PRO_RSET   X'7C' */
      * NOR_ALP_FSET   X'C5' */
      * NOR_NUM_FSET   X'D5' */
      * NOR_PRO_FSET   X'F5' */
      * WCC_SCREEN     X'C3' */
      * WCC_PRINTER    X'C8' */
      * SAUT_PAGE_EOM X'0C19'*/
      *        X'0C'         */
      *        X'19'         */
      * TYPE_TERMINAL   X'40'*/
      * MODELE_TERMINAL X'40'*/
      * CURSEUR  = -1        */
      *        X'FF'         */
         05 INIT_BMS1 OCCURS 18      PIC 1(8) BIT.
       01  GRP--INIT_BMS2.
      * TOUCHE_NULL     X'00'  */
      * TOUCHE_ENTER    X'7D'  */
      * TOUCHE_CLEAR    X'6D'  */
      * TOUCHE_PEN      X'7E'  */
      * TOUCHE_OPID     X'E6'  */
      * TOUCHE_MSRE     X'E7'  */
      * TOUCHE_STRF     X'C8'  */
      * TOUCHE_TRIG     X'7F'  */
      * TOUCHE_PA1      X'6C'  */
      * TOUCHE_PA2      X'6E'  */
      * TOUCHE_PA3      X'6B'  */
      * TOUCHE_PF1      X'F1'  */
      * TOUCHE_PF2      X'F2'  */
      * TOUCHE_PF3      X'F3'  */
      * TOUCHE_PF4      X'F4'  */
      * TOUCHE_PF5      X'F5'  */
      * TOUCHE_PF6      X'F6'  */
      * TOUCHE_PF7      X'F7'  */
      * TOUCHE_PF8      X'F8'  */
      * TOUCHE_PF9      X'F9'  */
      * TOUCHE_PF10     X'7A'  */
      * TOUCHE_PF11     X'7B'  */
      * TOUCHE_PF12     X'7C'  */
      * TOUCHE_PF13     X'C1'  */
      * TOUCHE_PF14     X'C2'  */
      * TOUCHE_PF15     X'C3'  */
      * TOUCHE_PF16     X'C4'  */
      * TOUCHE_PF17     X'C5'  */
      * TOUCHE_PF18     X'C6'  */
      * TOUCHE_PF19     X'C7'  */
      * TOUCHE_PF20     X'C8'  */
      * TOUCHE_PF21     X'C9'  */
      * TOUCHE_PF22     X'4A'  */
      * TOUCHE_PF23     X'4B'  */
      * TOUCHE_PF24     X'4C'  */
      * TERMINAL_ECRAN_91 X'91'*/
      * TERMINAL_ECRAN_99 X'99'*/
      * TERMINAL_IMPRIMANTE_93 X'93' '*/
      * TERMINAL_IMPRIMANTE_94 X'94' '*/
      * TERMINAL_IMPRIMANTE_9B X'9B' '*/
      * TERMINAL_IMPRIMANTE_9C X'9C' '*/
         05 INIT_BMS2 OCCURS 42      PIC 1(8) BIT.
       01  FIL_MAP                   PIC X(16) VALUE '*** MAP IG66 ***'.
       01  Z_MAP.
         05 DFHMS1                   PIC X(12).
      * PAGE-ITEM DEBUT
      *                                                               */
         05 MPAGDEBL                 PIC S9(4) COMP-5.
         05 MPAGDEBF                 PIC X(1).
         05 MPAGDEBI                 PIC X(4).
      * PAGE-ITEM FIN
      *                                                               */
         05 MPAGFINL                 PIC S9(4) COMP-5.
         05 MPAGFINF                 PIC X(1).
         05 MPAGFINI                 PIC X(4).
      * CODE IMPRIMANTE
      *                                                               */
         05 MIMPRIML                 PIC S9(4) COMP-5.
         05 MIMPRIMF                 PIC X(1).
         05 MIMPRIMI                 PIC X(4).
      * LIBELE ETAT
      *                                                               */
         05 MLIBELEL                 PIC S9(4) COMP-5.
         05 MLIBELEF                 PIC X(1).
         05 MLIBELEI                 PIC X(40).
      * TYPE CONSULTATION
      *                                                               */
         05 MCONSULL                 PIC S9(4) COMP-5.
         05 MCONSULF                 PIC X(1).
         05 MCONSULI                 PIC X(27).
      * NUMERO CONFIGURATION
      *                                                               */
         05 MCONFIGL                 PIC S9(4) COMP-5.
         05 MCONFIGF                 PIC X(1).
         05 MCONFIGI                 PIC X(1).
      * TYPE IMPRESSION
      *                                                               */
         05 MTYPIMPL                 PIC S9(4) COMP-5.
         05 MTYPIMPF                 PIC X(1).
         05 MTYPIMPI                 PIC X(1).
      * ATTACHEMENT IMPRIMANTES
      *                                                               */
         05 MATTACHL                 PIC S9(4) COMP-5.
         05 MATTACHF                 PIC X(1).
         05 MATTACHI                 PIC X(1).
      * PROTOCOLE D'IMPRESSION
      *                                                               */
         05 MPROTOCL                 PIC S9(4) COMP-5.
         05 MPROTOCF                 PIC X(1).
         05 MPROTOCI                 PIC X(1).
         05 GRP--M81I.
           10 M81I OCCURS 15.
      * SELECTION TRAITEMENT
      *                                                               */
             15 MSELECTL             PIC S9(4) COMP-5.
             15 MSELECTF             PIC X(1).
             15 MSELECTI             PIC X(1).
      * NOM ETAT
      *                                                               */
             15 MNOMETAL             PIC S9(4) COMP-5.
             15 MNOMETAF             PIC X(1).
             15 MNOMETAI             PIC X(6).
      * DATE EDITION
      *                                                               */
             15 MDATEDIL             PIC S9(4) COMP-5.
             15 MDATEDIF             PIC X(1).
             15 MDATEDII             PIC X(6).
      * DESTINATION
      *                                                               */
             15 MDESTINL             PIC S9(4) COMP-5.
             15 MDESTINF             PIC X(1).
             15 MDESTINI             PIC X(9).
      * DOCUMENT
      *                                                               */
             15 MDOCUMEL             PIC S9(4) COMP-5.
             15 MDOCUMEF             PIC X(1).
             15 MDOCUMEI             PIC X(15).
      * STATUT ETAT
      *                                                               */
             15 MSTATUTL             PIC S9(4) COMP-5.
             15 MSTATUTF             PIC X(1).
             15 MSTATUTI             PIC X(1).
      * DATE DERNIERE EDITION
      *                                                               */
             15 MDATDERL             PIC S9(4) COMP-5.
             15 MDATDERF             PIC X(1).
             15 MDATDERI             PIC X(6).
      * DATE IMPRESSION
      *                                                               */
             15 MDATIMPL             PIC S9(4) COMP-5.
             15 MDATIMPF             PIC X(1).
             15 MDATIMPI             PIC X(6).
      * DATE REPRISE IMPRESSION
      *                                                               */
             15 MDATREPL             PIC S9(4) COMP-5.
             15 MDATREPF             PIC X(1).
             15 MDATREPI             PIC X(6).
      * DATE ANNULATION
      *                                                               */
             15 MDATSUPL             PIC S9(4) COMP-5.
             15 MDATSUPF             PIC X(1).
             15 MDATSUPI             PIC X(6).
      * DATE CONSULTATION
      *                                                               */
             15 MDATCONL             PIC S9(4) COMP-5.
             15 MDATCONF             PIC X(1).
             15 MDATCONI             PIC X(6).
      * MESSAGE
      *                                                               */
         05 MLIBERRL                 PIC S9(4) COMP-5.
         05 MLIBERRF                 PIC X(1).
         05 MLIBERRI                 PIC X(78).
         05 FILL0094                 PIC X(1).
       01  FIL_WSAM                  PIC X(28) VALUE
                                         '..VSAM....AREA..VSAM_KEY....'.
       01  VSAM_KEY                  PIC X(256).
       01  FIL_WCLEF.
         05 WCLEF_TCTPRT             PIC X(4).
         05 WCLEF_APPLID             PIC X(8).
         05 FIL_DIC                  PIC X(1).
       01  FIG01                     PIC X(100).
       01  COM_IG60_LONG_COMMAREA    PIC S9(4) COMP-5 VALUE +500.
       01  Z_COMMAREA                PIC X(500).
       01  COMMET00_LONG             PIC S9(4) COMP-5 VALUE +396.
       01  COMMET00_ABAN             PIC X(396).
       01  STRT_IG6A_LONG_AREA       PIC S9(4) COMP-5 VALUE 103.
       01  Z_STRTIG6A                PIC X(103).
       01  STRT_IGAL_LONG_AREA       PIC S9(4) COMP-5 VALUE 103.
       01  Z_STRTIGLP                PIC X(103).
       01  STRT_IGAG_LONG_AREA       PIC S9(4) COMP-5 VALUE 103.
       01  Z_STRTIGGP                PIC X(103).
       01  Z_START                   PIC X(500).
       01  Z_INOUT                   PIC X(6096) VALUE ' '.
       01  Z_ERREUR.
         05 FIL_ERR00                PIC X(256).
         05 Z_ERREUR_TRACE_MESS      PIC X(80).
         05 Z_ERREUR_EIBLK           PIC X(66).
       01  FIL_ERR01.
         05 TRACE_MESSAGE_LONG       PIC S9(4) COMP-5 VALUE +84.
         05 TRACE_MESSAGE.
      * START_BUFFER_ADDRESS */
           10 FIL_ERR02              PIC X(1) VALUE ''.
      * 23EME LIGNE 1ERE COLONNE */
           10 FIL_ERR03              PIC X(2) VALUE '$-'.
      * START_FIELD */
           10 FIL_ERR04              PIC X(1) VALUE ''.
      * ATTRIBUT */
           10 FIL_ERR05              PIC X(1) VALUE 'Y'.
           10 TRACE_MESSX.
             15 MESS1                PIC X(15) VALUE ''.
             15 MESS                 PIC X(64) VALUE ''.
       01  PTR_COMM                  POINTER.
       01  PTR_CWA                   POINTER.
       01  PTR_TWA                   POINTER.
       01  PTR_TCTUA                 POINTER.
       01  PTR_USER                  POINTER.
       01  SWAP_PTR_TS               POINTER.
       01  CWA_LONG                  PIC S9(4) COMP-5.
       01  TWA_LONG                  PIC S9(4) COMP-5.
       01  TCTUA_LONG                PIC S9(4) COMP-5.
      * TABLE DE COMMUNIC. */
       01  GFDX.
      * CODE D'APPEL       */
         05 GFDATA                   PIC X(1).
      * JOUR               */
         05 GFJOUR                   PIC X(2).
         05 GFJJ REDEFINES GFJOUR    PIC 99.
      * MOIS               */
         05 GFMOIS                   PIC X(2).
         05 GFMM REDEFINES GFMOIS    PIC 99.
      * SIECLE             */
         05 GFSIECLE                 PIC X(2).
         05 GFSS REDEFINES GFSIECLE  PIC 99.
      * ANNEE              */
         05 GFANNEE                  PIC X(2).
         05 GFAA REDEFINES GFANNEE   PIC 99.
      * QUANTIEME CALEND.  */
         05 GFQNTA                   PIC 999.
      * QUANT. 'STANDARD'  */
         05 GFQNT0                   PIC 9(5).
      * JOUR DE LA SEMAINE */
         05 GFSMN                    PIC 9.
      * DATE CONCATENEE   */
         05 GFAMJ                    PIC X(6).
         05 GFAAMMJJ REDEFINES GFAMJ PIC 9(6).
      * DATE CONCATENEE   */
         05 GFSAMJ                   PIC X(8).
         05 GFSSAAMMJJ REDEFINES GFSAMJ PIC 9(8).
      * CODE REPONSE       */
         05 GFVDAT                   PIC 1(1) BIT.
      * ANNEE BISSEXTILE   */
         05 GFBISS                   PIC 1(1) BIT.
      * JJ/MM/AA           */
         05 GFJMA_ED                 PIC X(8).
      * JJ/MM/SSAA         */
         05 GFJMSA_ED                PIC X(10).
      * LIB MOIS LONG      */
         05 GFLMOISL                 PIC X(9).
      * LIB MOIS COURT     */
         05 GFLMOISC                 PIC X(3).
         05 GFLJOUR                  PIC X(8).
       01  GFMESS                    PIC X(60).
       01  GRP--GFTABJ.
         05 GFTABJ OCCURS 12         PIC S9(4) COMP-5.
       01  GRP--GFTABLML.
         05 GFTABLML OCCURS 12       PIC X(9).
       01  GRP--GFTABLMC.
         05 GFTABLMC OCCURS 12       PIC X(3).
       01  GRP--GFTABLJ.
         05 GFTABLJ OCCURS 7         PIC X(8).
       01  FLAG1                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG2                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG3                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG4                     PIC 1(1) BIT SYNCHRONIZED.
       01  GFX0                      PIC S9(9) COMP-5.
       01  GFX1                      PIC S9(9) COMP-5.
       01  GFX2                      PIC S9(9) COMP-5.
       01  GFBJ                      PIC S9(9) COMP-5.
       01  GFBM                      PIC S9(9) COMP-5.
       01  GFBA                      PIC S9(9) COMP-5.
       01  GFBS                      PIC S9(9) COMP-5.
       01  GFBQTA                    PIC S9(9) COMP-5.
       01  GFBQT0                    PIC S9(9) COMP-5.
       01  I                         PIC S9(4) COMP-5.
       LINKAGE SECTION.
       01  PTR_PREF                  POINTER.
       01  SWAP_IDENT_TS             PIC X(8).
       01  NUM_4                     PIC 9(4).
       01  NUM_3                     PIC 9(3).
       01  NUM_2                     PIC 9(2).
       01  NUM_1                     PIC 9(1).
       01  REDEF_BINAIRE             PIC X(2).
       01  KW--STRING                PIC X(2000).
       01  GRP--IG65_TS_TAB.
         05 IG65_TS_TAB OCCURS 15.
      * SELECTION TRAITEMENT    */
           10 IG65_TS_SEL            PIC X(1).
      * NOM ETAT                */
           10 IG65_TS_ETA            PIC X(6).
      * DATE EDITION            */
           10 IG65_TS_DAT            PIC X(6).
      * DESTINATION             */
           10 IG65_TS_DST            PIC X(9).
      * DOCUMENT                */
           10 IG65_TS_DOC            PIC X(15).
      * DATE DERNIERE EDITION   */
           10 IG65_TS_EDI            PIC X(6).
      * DATE IMPRESSION         */
           10 IG65_TS_IMP            PIC X(6).
      * DATE REPRISE IMPRESSION */
           10 IG65_TS_REP            PIC X(6).
      * DATE ANNULATION         */
           10 IG65_TS_SUP            PIC X(6).
      * DATE CONSULTATION       */
           10 IG65_TS_CON            PIC X(6).
      * TYPE DE CONSULTATION    */
           10 IG65_TS_TRA            PIC X(1).
           10 IG65_TS_FIL            PIC X(4).
       01  W_PIC_PAG                 PIC 9999.
       01  TACH_CHAR.
         05 TACH_CHAR_1              PIC X(3).
         05 TACH_CHAR_2              PIC X(4).
       01  EIB_CODE                  PIC X(1).
       01  FIL_EIBX.
         05 EIB_NORMAL               PIC X(1).
         05 EIB_NOTFND               PIC X(1).
         05 EIB_ENDFILE              PIC X(1).
         05 EIB_MAPFAIL              PIC X(1).
         05 EIB_ENDDATA              PIC X(1).
         05 EIB_QIDERR               PIC X(1).
         05 EIB_QZERO                PIC X(1).
         05 EIB_ITEMERR              PIC X(1).
         05 EIB_DUPREC               PIC X(1).
         05 EIB_DUPKEY               PIC X(1).
       01  Z_DATH2.
         05 DATH2_SS                 PIC X(2) VALUE '00'.
         05 DATH2_AA                 PIC X(2) VALUE '00'.
         05 DATH2_MM                 PIC X(2) VALUE '00'.
         05 DATH2_JJ                 PIC X(2) VALUE '00'.
       01  Z_DATH3.
         05 DATH3_JJ                 PIC X(2) VALUE '00'.
         05 DATH3_FIL1               PIC X(1) VALUE '/'.
         05 DATH3_MM                 PIC X(2) VALUE '00'.
         05 DATH3_FIL2               PIC X(1) VALUE '/'.
         05 DATH3_AA                 PIC X(2) VALUE '00'.
       01  Z_TIMER.
         05 TIME_HH                  PIC X(2) VALUE '00'.
         05 TIME_FIL1                PIC X(1) VALUE 'H'.
         05 TIME_MM                  PIC X(2) VALUE '00'.
         05 TIME_FIL2                PIC X(1) VALUE 'M'.
         05 TIME_SS                  PIC X(2) VALUE '00'.
         05 TIME_FIL3                PIC X(1) VALUE 'S'.
       01  GRP--Z_TIMER_TIMJOU_CAR.
         05 Z_TIMER_TIMJOU_CAR OCCURS 7 PIC X(1).
       01  TIMJOU_DARTY.
         05 FILER_JJ                 PIC 99.
         05 EIB_ANNEE                PIC 99.
         05 EIB_QQQ                  PIC 999.
       01  Z_CWA_DATJOU.
         05 Z_CWA_AA                 PIC X(2).
         05 FIL_CWA1                 PIC X(1).
         05 Z_CWA_MM                 PIC X(2).
         05 FIL_CWA2                 PIC X(1).
         05 Z_CWA_JJ                 PIC X(2).
       01  TCT_DARTY.
      * SIGNATURE-SECURITE  */
         05 TCT_SEC_SIG.
      *   .GROUPE           */
           10 TCT_SEC_GRP            PIC X(8).
      *   .SOUS-GROUPE      */
           10 TCT_SEC_SGR            PIC X(8).
      *   .UTILISATEUR      */
           10 TCT_SEC_UTI            PIC X(8).
         05 GRP--TCT_SEC_TBL.
      * TABLE DES .../...   */
           10 TCT_SEC_TBL OCCURS 20.
      * FONCTIONS ACCORDEES */
             15 TCT_SEC_FON          PIC X(4).
         05 TCT_SEC_FLR              PIC X(151).
       01  FIL_BMS1.
         05 WORKAID                  PIC X(1).
      * ****************************/
      * DEFINITION DES ATTRIBUTS  */
      * ****************************/
         05 BRT_ALP_FSET             PIC X(1).
         05 BRT_NUM_FSET             PIC X(1).
         05 BRT_PRO_FSET             PIC X(1).
         05 DRK_ALP_FSET             PIC X(1).
         05 DRK_PRO_FSET             PIC X(1).
         05 DRK_PRO_RSET             PIC X(1).
         05 NOR_ALP_FSET             PIC X(1).
         05 NOR_NUM_FSET             PIC X(1).
         05 NOR_PRO_FSET             PIC X(1).
      * WCC_SCREEN */
         05 WCC_SCREEN               PIC X(1).
      * WCC_PRINTER*/
         05 WCC_PRINTER              PIC X(1).
      * SAUT_PAGE_EOM*/
         05 SAUT_PAGE_EOM            PIC X(2).
      * *********************************************/
      * ATTENTION , LES DEUX ZONES CI-DESSOUS:     */
      * TYPE_TERMINAL ET MODELE_TERMINAL           */
      * DOIVENT RESTER GROUPEES, ET DANS CET ORDRE */
      * *********************************************/
         05 TERMCODE.
           10 TYPE_TERMINAL          PIC X(1).
           10 MODELE_TERMINAL        PIC X(1).
         05 CURSEUR                  PIC S9(4) COMP-5.
       01  FIL_BMS2.
         05 TOUCHE_NULL              PIC X(1).
         05 TOUCHE_ENTER             PIC X(1).
         05 TOUCHE_CLEAR             PIC X(1).
         05 TOUCHE_PEN               PIC X(1).
         05 TOUCHE_OPID              PIC X(1).
         05 TOUCHE_MSRE              PIC X(1).
         05 TOUCHE_STRF              PIC X(1).
         05 TOUCHE_TRIG              PIC X(1).
         05 TOUCHE_PA1               PIC X(1).
         05 TOUCHE_PA2               PIC X(1).
         05 TOUCHE_PA3               PIC X(1).
         05 TOUCHE_PF1               PIC X(1).
         05 TOUCHE_PF2               PIC X(1).
         05 TOUCHE_PF3               PIC X(1).
         05 TOUCHE_PF4               PIC X(1).
         05 TOUCHE_PF5               PIC X(1).
         05 TOUCHE_PF6               PIC X(1).
         05 TOUCHE_PF7               PIC X(1).
         05 TOUCHE_PF8               PIC X(1).
         05 TOUCHE_PF9               PIC X(1).
         05 TOUCHE_PF10              PIC X(1).
         05 TOUCHE_PF11              PIC X(1).
         05 TOUCHE_PF12              PIC X(1).
         05 TOUCHE_PF13              PIC X(1).
         05 TOUCHE_PF14              PIC X(1).
         05 TOUCHE_PF15              PIC X(1).
         05 TOUCHE_PF16              PIC X(1).
         05 TOUCHE_PF17              PIC X(1).
         05 TOUCHE_PF18              PIC X(1).
         05 TOUCHE_PF19              PIC X(1).
         05 TOUCHE_PF20              PIC X(1).
         05 TOUCHE_PF21              PIC X(1).
         05 TOUCHE_PF22              PIC X(1).
         05 TOUCHE_PF23              PIC X(1).
         05 TOUCHE_PF24              PIC X(1).
         05 TERMINAL_ECRAN_91        PIC X(1).
         05 TERMINAL_ECRAN_99        PIC X(1).
         05 TERMINAL_IMPRIMANTE_93   PIC X(1).
         05 TERMINAL_IMPRIMANTE_94   PIC X(1).
         05 TERMINAL_IMPRIMANTE_9B   PIC X(1).
         05 TERMINAL_IMPRIMANTE_9C   PIC X(1).
         05 TERMINAL_IMPRIMANTE_B6   PIC X(1).
       01  Z_TWA_PRINTER.
      * NOM DU TERMINAL */
         05 Z_TWA_PRT                PIC X(4).
      * FILLER          */
         05 Z_TWA_TYP                PIC X(1).
         05 Z_TWA_RET                PIC X(1).
       01  EIG66O.
         05 DFHMS2                   PIC X(12).
      * PAGE-ITEM DEBUT
      *                                                               */
         05 DFHMS3                   PIC S9(4) COMP-5.
         05 MPAGDEBA                 PIC X(1).
         05 MPAGDEBO                 PIC X(4).
      * PAGE-ITEM FIN
      *                                                               */
         05 DFHMS4                   PIC S9(4) COMP-5.
         05 MPAGFINA                 PIC X(1).
         05 MPAGFINO                 PIC X(4).
      * CODE IMPRIMANTE
      *                                                               */
         05 DFHMS5                   PIC S9(4) COMP-5.
         05 MIMPRIMA                 PIC X(1).
         05 MIMPRIMO                 PIC X(4).
      * LIBELE ETAT
      *                                                               */
         05 DFHMS6                   PIC S9(4) COMP-5.
         05 MLIBELEA                 PIC X(1).
         05 MLIBELEO                 PIC X(40).
      * TYPE CONSULTATION
      *                                                               */
         05 DFHMS7                   PIC S9(4) COMP-5.
         05 MCONSULA                 PIC X(1).
         05 MCONSULO                 PIC X(27).
      * NUMERO CONFIGURATION
      *                                                               */
         05 DFHMS8                   PIC S9(4) COMP-5.
         05 MCONFIGA                 PIC X(1).
         05 MCONFIGO                 PIC X(1).
      * TYPE IMPRESSION
      *                                                               */
         05 DFHMS9                   PIC S9(4) COMP-5.
         05 MTYPIMPA                 PIC X(1).
         05 MTYPIMPO                 PIC X(1).
      * ATTACHEMENT IMPRIMANTES
      *                                                               */
         05 DFHMS10                  PIC S9(4) COMP-5.
         05 MATTACHA                 PIC X(1).
         05 MATTACHO                 PIC X(1).
      * PROTOCOLE D'IMPRESSION
      *                                                               */
         05 DFHMS11                  PIC S9(4) COMP-5.
         05 MPROTOCA                 PIC X(1).
         05 MPROTOCO                 PIC X(1).
         05 GRP--M81O.
           10 M81O OCCURS 15.
      * SELECTION TRAITEMENT
      *                                                               */
             15 DFHMS12              PIC S9(4) COMP-5.
             15 MSELECTA             PIC X(1).
             15 MSELECTO             PIC X(1).
      * NOM ETAT
      *                                                               */
             15 DFHMS13              PIC S9(4) COMP-5.
             15 MNOMETAA             PIC X(1).
             15 MNOMETAO             PIC X(6).
      * DATE EDITION
      *                                                               */
             15 DFHMS14              PIC S9(4) COMP-5.
             15 MDATEDIA             PIC X(1).
             15 MDATEDIO             PIC X(6).
      * DESTINATION
      *                                                               */
             15 DFHMS15              PIC S9(4) COMP-5.
             15 MDESTINA             PIC X(1).
             15 MDESTINO             PIC X(9).
      * DOCUMENT
      *                                                               */
             15 DFHMS16              PIC S9(4) COMP-5.
             15 MDOCUMEA             PIC X(1).
             15 MDOCUMEO             PIC X(15).
      * STATUT ETAT
      *                                                               */
             15 DFHMS17              PIC S9(4) COMP-5.
             15 MSTATUTA             PIC X(1).
             15 MSTATUTO             PIC X(1).
      * DATE DERNIERE EDITION
      *                                                               */
             15 DFHMS18              PIC S9(4) COMP-5.
             15 MDATDERA             PIC X(1).
             15 MDATDERO             PIC X(6).
      * DATE IMPRESSION
      *                                                               */
             15 DFHMS19              PIC S9(4) COMP-5.
             15 MDATIMPA             PIC X(1).
             15 MDATIMPO             PIC X(6).
      * DATE REPRISE IMPRESSION
      *                                                               */
             15 DFHMS20              PIC S9(4) COMP-5.
             15 MDATREPA             PIC X(1).
             15 MDATREPO             PIC X(6).
      * DATE ANNULATION
      *                                                               */
             15 DFHMS21              PIC S9(4) COMP-5.
             15 MDATSUPA             PIC X(1).
             15 MDATSUPO             PIC X(6).
      * DATE CONSULTATION
      *                                                               */
             15 DFHMS22              PIC S9(4) COMP-5.
             15 MDATCONA             PIC X(1).
             15 MDATCONO             PIC X(6).
      * MESSAGE
      *                                                               */
         05 DFHMS23                  PIC S9(4) COMP-5.
         05 MLIBERRA                 PIC X(1).
         05 MLIBERRO                 PIC X(78).
         05 FILL0094                 PIC X(1).
       01  FIGENREG.
      * CLE ACCES VSAM      */
         05 FIG01_CLE.
      * CODE TCT            */
           10 FIG01_COD              PIC X(4).
      * CICS ACTIF = APPLID */
           10 FIG01_CIC              PIC X(8).
      * IDENTIFICATION      */
         05 FIG01_ITF.
      * MARQUE              */
           10 FIG01_MAR              PIC X(15).
      * MODELE              */
           10 FIG01_MOD              PIC X(8).
      * PROTOCOLE L,T,G     */
           10 FIG01_PTC              PIC X(1).
      * IDENTIFIANT         */
           10 FIG01_IDE              PIC X(8).
      * LOCALISATION        */
         05 FIG01_LOC.
      * SOCIETE             */
           10 FIG01_SOC              PIC X(3).
      * LIEU                */
           10 FIG01_LIE              PIC X(3).
      * SPOOL               */
           10 FIG01_SPO              PIC X(3).
      * NB LIGNES SAUT PG 36*/
         05 FIG01_LIG                PIC 999.
         05 FIG01_FILLER             PIC X(44).
       01  FIG01_FIG01K_CLEF         PIC X(12).
       01  FIG01_FIG01K_CLEFX.
         05 FIG01_FIG01K_TCTPRT      PIC X(4).
         05 FIG01_FIG01K_APPLID      PIC X(8).
         05 FIG01_FIG01K_FIL         PIC X(1).
       01  Z_COMMAREA_PREF           PIC X(500).
       01  Z_COMMAREA_IG60.
      * *********** RESERVE AIDA
      *                      ******************************************/
         05 PREFIX_AIDA              PIC X(100).
      * *********** APPARTENANT A TRANSACTION IG60
      *                                        ************************/
      * APPLID CICS            */
         05 COM_IG60_CICS            PIC X(8).
      * LIGNE VTAM             */
         05 COM_IG60_LIG             PIC X(8).
      * CONCERNANT SIGNATURE UTILISATEUR ---------------------------- 32
      *                                                               */
      * SIGNATURE              */
         05 COM_IG60_SIG.
      *       GROUPE           */
           10 COM_IG60_GRP           PIC X(8).
      *       SOUS-GROUPE      */
           10 COM_IG60_SGP           PIC X(8).
      *       UTILISATEUR      */
           10 COM_IG60_UTI           PIC X(8).
      * NOM ETAT               */
         05 COM_IG60_ETA             PIC X(6).
      * CHOIX DU TRAITEMENT    */
         05 COM_IG60_TRA             PIC X(1).
      *       I = IMPRESSION   */
      *       R = REPRISE      */
      *       A = ANNULATION   */
      *       C = CONSULTATION */
      * TYPE DE CONSULTATION   */
         05 COM_TYP_SEL              PIC X(1).
      *       * = INTEGRALE    */
      *       I = IMPRESSION   */
      *       R = REPRISE      */
      *       C = CONSULTATION */
      * PROVENANT SEGMENT DIGSA IDENTIFICATION ETAT ----------------- 76
      *                                                               */
      * TITRE ETAT             */
         05 COM_DIGSA_TIT            PIC X(40).
      * INFO TYPE PAPIER       */
         05 COM_DIGSA_PAP            PIC X(20).
      * NUMERO CONFIGURATION   */
         05 COM_DIGSA_CFG            PIC X(1).
      * TYPE IMPRESSION        */
         05 COM_DIGSA_IMP            PIC X(1).
      *       U = UNIQUE       */
      *       M = MULTIPLE     */
      * ATTACHEMENT IMPRIMANTE */
         05 COM_DIGSA_ATT            PIC X(1).
      *       S = STANDARD     */
      *       P = PARTICULIER  */
      * PROTOCOLE D'IMPRESSION */
         05 COM_DIGSA_PTC            PIC X(1).
      *       T = TEXT         */
      *       L = LASER        */
      *       G = GRAPHIQUE    */
      * ETAT TYPE TEXTE */
      * HAUTEUR ENTRE PLIURES  */
         05 COM_DIGSA_HTR            PIC S9(4) COMP-5.
      * TOTAL LIGNES EDITABLES */
         05 COM_DIGSA_LNG            PIC S9(4) COMP-5.
      * NOMBRE COLONNES        */
         05 COM_DIGSA_COL            PIC S9(4) COMP-5.
      * ETAT TYPE LASER */
      * LNG ZONE COMMANDES     */
         05 COM_DIGSA_CMD            PIC S9(4) COMP-5.
      * LNG ZONE DONNEES       */
         05 COM_DIGSA_DON            PIC S9(4) COMP-5.
      * FORMAT                 */
         05 COM_DIGSA_FOR            PIC X(2).
      * PROVENANT SEGMENT DIGSD AUTORISATIONS-SIGNATURES ------------ 35
      *                                                               */
      * GROUPE                 */
         05 COM_DIGSD_GRP            PIC X(8).
      * SOUS-GROUPE            */
         05 COM_DIGSD_SGP            PIC X(8).
      * UTILISATEUR            */
         05 COM_DIGSD_UTI            PIC X(8).
      * DESTINATION            */
         05 COM_DIGSD_DST            PIC X(9).
      * AUTORISATION REPRISE   */
         05 COM_DIGSD_REP            PIC X(1).
      * AUTORISATION ANNULAT   */
         05 COM_DIGSD_SUP            PIC X(1).
      * GESTION DES IMPRIMANTES ------------------------------------- 12
      *                                                               */
      * IMPRIMANTE 1 ATTACHEE  */
         05 COM_IG60_PR1             PIC X(4).
      * IMPRIMANTE 2 ATTACHEE  */
         05 COM_IG60_PR2             PIC X(4).
      * IMPRIMANTE TRAITEMENT  */
         05 COM_STRT_PRT             PIC X(4).
      * CONFIGURATION DE TRAVAIL ------------------------------------ 57
      *                                                               */
      * NOM ETAT               */
         05 COM_CFG_ETA              PIC X(6).
      * DATE EDITION DEBUT     */
         05 COM_CFG_DTD              PIC X(6).
      * DATE EDITION FIN       */
         05 COM_CFG_DTF              PIC X(6).
      * DESTINATION            */
         05 COM_CFG_DST              PIC X(9).
      * DOCUMENT DEBUT         */
         05 COM_CFG_DCD              PIC X(15).
      * DOCUMENT FIN           */
         05 COM_CFG_DCF              PIC X(15).
      * TS ASSOCIES AU TRAITEMENT REPRISE OU IMPRESSION  ------------ 12
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOM               PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNG               PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAX               PIC S9(4) COMP-5.
      * TS ASSOCIES AU TRAITEMENT CONSULTATION CONFIGURATIONS ------- 25
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOMC              PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNGC              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXC              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIC              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGC              PIC S9(4) COMP-5.
      * NBR CONSULTATION       */
         05 COM_NB_CONC              PIC S9(5) COMP-3.
      * NBR IMPRESSION         */
         05 COM_NB_IMPC              PIC S9(5) COMP-3.
      * NBR REPRISE            */
         05 COM_NB_REPC              PIC S9(5) COMP-3.
      * TS ASSOCIES AU TRAITEMENT AFFICHAGE LIGNE ETAT -------------- 16
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOML              PIC X(8).
         05 Z_COMMAREA_IG60          PIC X(1).
      * LONGUEUR TS            */
         05 COM_TS_LNGL              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXL              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIL              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGL              PIC S9(4) COMP-5.
      * TS ASSOCIES AU TRAITEMENT ANNULATION ------------------------ 31
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOMA              PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNGA              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXA              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIA              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGA              PIC S9(4) COMP-5.
      * NBR SUPPRESSION PHYSIQ */
         05 COM_NB_SUPA              PIC S9(5) COMP-3.
      * NBR ANNULATION         */
         05 COM_NB_ANUA              PIC S9(5) COMP-3.
      * NBR DESANNULATION      */
         05 COM_NB_DESA              PIC S9(5) COMP-3.
      * SYSTEME SECURITE ATTACHE --------------------------------------3
      *                                                               */
         05 COM_SECURI               PIC X(3).
       01  COM_AIDA.
         05 COM_CODERR               PIC X(6).
         05 COM_PGMPRC               PIC X(8).
         05 Z_COMMAREA_NOM_MAP       PIC X(8).
         05 Z_COMMAREA_NOM_MAPSET    PIC X(8).
         05 Z_COMMAREA_NOM_TACHE     PIC X(4).
         05 Z_COMMAREA_LONG          PIC S9(4) COMP-5.
         05 COM_FILLER               PIC X(64).
       01  COMMET00_STRU.
      * 1 = EXEC CICS     */
         05 ORI_TYP_ERR              PIC X(1).
      * 2 = EXEC DL1      */
      * DATE              */
         05 EIB_DATE                 PIC X(10).
      * HEURE             */
         05 EIB_TIME                 PIC X(10).
      * TERMINAL          */
         05 EIB_TRMID                PIC X(4).
      * NOM DE LA TACHE   */
         05 EIB_TRNID                PIC X(4).
      * NOM DU PROGRAMME  */
         05 ORI_NOM_PGM              PIC X(7).
      * NOM LIGNE VTAM    */
         05 ORI_NOM_LIG              PIC X(8).
      * DATE COMPILATION  */
         05 ORI_DAT_CPL              PIC X(18).
      * EXEC CICS ==> INFORMATIONS BLOCK EIB */
         05 EIB_SEL_INF.
      * CODE FONCTION     */
           10 EIB_FN                 PIC X(2).
      * CODE REPONSE      */
           10 EIB_RCODE              PIC X(6).
      * DATA-SET NAME     */
           10 EIB_DS                 PIC X(8).
      * EXEC DL1 ==> RESULTATS DU SCHEDULING CALL */
         05 DL1_PCB_CAL.
           10 DL1_FCTR               PIC X(1).
           10 DL1_DLTR               PIC X(1).
      * EXEC DL1 ==> MASQUE PCB DL1 EN COURS */
         05 DL1_PCB_MAS.
      * NOM DU DBD        */
           10 DL1_DBD_NOM            PIC X(8).
      * NIVEAU DU SEGMENT */
           10 DL1_SEG_LEV            PIC X(2).
      * CODE RETOUR       */
           10 DL1_COD_RET            PIC X(2).
      * PROCESSING OPTION */
           10 DL1_PRO_OPT            PIC X(4).
      * NOM DU SEGMENT    */
           10 DL1_SEG_NAM            PIC X(8).
      * LONGUEUR CLE CONC */
           10 DL1_LON_CLE            PIC S9(9) COMP-5.
      * NOMBRE SEGMENTS   */
           10 DL1_NBR_SEG            PIC S9(9) COMP-5.
      * CLE CONCATENEE    */
           10 DL1_CLE_VAL            PIC X(264).
         05 COMMET00_STRU            PIC X(3).
      * EXEC DL1 ==> AUTRES INFORMATIONS DL1 */
         05 DL1_INF_SUP.
      * FONCTION DL1      */
           10 DL1_TYP_FUN            PIC X(4).
      * NUMERO PCB        */
           10 DL1_NUM_PCB            PIC 9.
      * NOM DU PSB        */
           10 DL1_PSB_NOM            PIC X(8).
           10 DL1_NBR_PAR            PIC S9(9) COMP-5.
       01  Z_STRTAREA_IG6A.
      * --CONFIGURATION ETAT A IMPRIMER */
         05 STRT_IG6A_CLE.
      * NOM ETAT      */
           10 STRT_IG6A_ETA          PIC X(6).
      * DATE EDITION  */
           10 STRT_IG6A_DAT          PIC X(6).
      * DESTINATION   */
           10 STRT_IG6A_DST          PIC X(9).
      * DOCUMENT      */
           10 STRT_IG6A_DOC          PIC X(15).
      * --CARACTERISTIQUES ETAT */
      * N0 CONFIG     */
         05 STRT_IG6A_CFG            PIC X(1).
      * TYP IMPRESS   */
         05 STRT_IG6A_IMP            PIC X(1).
      * --CARACTERISTIQUES IMPRIMANTE */
      * CODE PRT TCT  */
         05 STRT_IG6A_PRT            PIC X(4).
      * TYPE PRT SYRD */
         05 STRT_IG6A_TYP            PIC X(1).
      * MARQUE        */
         05 STRT_IG6A_MRQ            PIC X(20).
      * TYPE PRT SYRD */
         05 STRT_IG6A_MDL            PIC X(8).
      * --CARACTERISTIQUES ETAT PROTOCOLE TEXTE */
      * HAUTEUR PAGE  */
         05 STRT_IG6A_HTR            PIC S9(4) COMP-5.
      * NBR LIG IMPR  */
         05 STRT_IG6A_LNG            PIC S9(4) COMP-5.
      * NBR COLONNES  */
         05 STRT_IG6A_COL            PIC S9(4) COMP-5.
      * --PROVENANCE */
      * PGM ORIGINE   */
         05 STRT_IG6A_PGM            PIC X(8).
      * NOM TS        */
         05 STRT_IG6A_TSN            PIC X(8).
      * LONGUEUR TS   */
         05 STRT_IG6A_TSL            PIC S9(4) COMP-5.
      * NBR ITEM TS   */
         05 STRT_IG6A_TSI            PIC S9(4) COMP-5.
      * --DATE DU JOUR AAMMJJ */
         05 STRT_IG6A_AMJ            PIC X(6).
       01  Z_STRTAREA_IGAL.
      * --CONFIGURATION ETAT A IMPRIMER */
         05 STRT_IGAL_CLE.
           10 STRT_IGAL_ETA          PIC X(6).
           10 STRT_IGAL_DAT          PIC X(6).
           10 STRT_IGAL_DST          PIC X(9).
           10 STRT_IGAL_DOC          PIC X(15).
      * --CARACTERISTIQUES ETAT        */
      *     . NUMERO DE CONFIGURATION */
      *     . TYPE IMPRESSION         */
         05 STRT_IGAL_CFG            PIC X(1).
         05 STRT_IGAL_IMP            PIC X(1).
      * --CARACTERISTIQUES IMPRIMANTE */
      *     . CODE IMPRIMANTE TCT    */
      *     . TYPE IMPRIMANTE (SYRD) */
      *     . MARQUE IMPRIMANTE      */
      *     . MODELE IMPRIMANTE      */
         05 STRT_IGAL_PRT            PIC X(4).
         05 STRT_IGAL_TYP            PIC X(1).
         05 STRT_IGAL_MRQ            PIC X(20).
         05 STRT_IGAL_MDL            PIC X(8).
      * --CARACTERISTIQUES PROTOCOLE    */
      *      . LONGUEUR ZONE COMMANDES */
      *      . LONGUEUR ZONE DONNEES   */
      *      . FORMAT                  */
         05 STRT_IGAL_CMD            PIC S9(4) COMP-5.
         05 STRT_IGAL_DON            PIC S9(4) COMP-5.
         05 STRT_IGAL_FOR            PIC X(2).
      * --PROVENANCE                    */
      *      . ORIGINE PROGRAMME       */
      *      . NOM DE LA TS            */
      *      . LONGUEUR DE LA TS       */
      *      . NOMBRE ITEM DE LA TS    */
         05 STRT_IGAL_PGM            PIC X(8).
         05 STRT_IGAL_TSN            PIC X(8).
         05 STRT_IGAL_TSL            PIC S9(4) COMP-5.
         05 STRT_IGAL_TSI            PIC S9(4) COMP-5.
      * --DATE DU JOUR AAMMJJ           */
         05 STRT_IGAL_AMJ            PIC X(6).
       01  Z_STRTAREA_IGAG.
      * --CONFIGURATION ETAT A IMPRIMER */
         05 STRT_IGAG_CLE.
           10 STRT_IGAG_ETA          PIC X(6).
           10 STRT_IGAG_DAT          PIC X(6).
           10 STRT_IGAG_DST          PIC X(9).
           10 STRT_IGAG_DOC          PIC X(15).
      * --CARACTERISTIQUES ETAT        */
      *     . NUMERO DE CONFIGURATION */
      *     . TYPE IMPRESSION         */
         05 STRT_IGAG_CFG            PIC X(1).
         05 STRT_IGAG_IMP            PIC X(1).
      * --CARACTERISTIQUES IMPRIMANTE */
      *     . CODE IMPRIMANTE TCT    */
      *     . TYPE IMPRIMANTE (SYRD) */
      *     . MARQUE IMPRIMANTE      */
      *     . MODELE PROGRAMME       */
         05 STRT_IGAG_PRT            PIC X(4).
         05 STRT_IGAG_TYP            PIC X(1).
         05 STRT_IGAG_MRQ            PIC X(20).
         05 STRT_IGAG_MDL            PIC X(8).
      * --CARACTERISTIQUES PROTOCOLE    */
      *      . LONGUEUR ZONE COMMANDES */
      *      . LONGUEUR ZONE DONNEES   */
      *      . HAUTEUR PAGE GRAPHIQUE  */
         05 STRT_IGAG_CMD            PIC S9(4) COMP-5.
         05 STRT_IGAG_DON            PIC S9(4) COMP-5.
         05 STRT_IGAG_HTR            PIC S9(4) COMP-5.
      * --PROVENANCE                    */
      *      . ORIGINE PROGRAMME       */
      *      . NOM DE LA TS            */
      *      . LONGUEUR DE LA TS       */
      *      . NOMBRE ITEM DE LA TS    */
         05 STRT_IGAG_PGM            PIC X(8).
         05 STRT_IGAG_TSN            PIC X(8).
         05 STRT_IGAG_TSL            PIC S9(4) COMP-5.
         05 STRT_IGAG_TSI            PIC S9(4) COMP-5.
      * --DATE DU JOUR AAMMJJ */
         05 STRT_IGAG_AMJ            PIC X(6).
       01  Z_SWAP.
         05 Z_INOUT_MAP_FILLER       PIC X(2000).
         05 Z_INOUT_COMMAREA_FILLER  PIC X(4096).
       01  Z_INOUT_MAP               PIC X(2000).
       01  Z_INOUT_COMMAREA          PIC X(4096).
       01  MESS_ABCODE               PIC X(4).
       01  TRACE_MESS                PIC X(79).
       01  LINK_CWA                  PIC X(512).
       01  LINK_TWA                  PIC X(500).
       01  LINK_TCTUA                PIC X(255).
       01  LINK_USER                 PIC X(4000).
       01  LINK_TS.
         05 LINK_TS_MAP              PIC X(2000).
         05 LINK_TS_COMMAREA         PIC X(4096).
       01  GFJMSA                    PIC X(8).
       PROCEDURE DIVISION USING PTR_PREF.
       BEGIN--MAIN SECTION.
           SET ADDRESS OF Z_COMMAREA_PREF TO PTR_PREF
           SET ADDRESS OF SWAP_IDENT_TS TO ADDRESS OF
                                                    SWAP_FILLER_IDENT_TS
           SET ADDRESS OF NUM_4 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_3 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_2 TO ADDRESS OF NUM_5
           SET ADDRESS OF NUM_1 TO ADDRESS OF NUM_5
           SET ADDRESS OF REDEF_BINAIRE TO ADDRESS OF BINAIRE
           MOVE ALL ' ' TO MW-REPEAT-CHAR--131
           MOVE MW-REPEAT-CHAR--131 TO LIGNE
           SET ADDRESS OF IG65_TS_TAB TO ADDRESS OF IG65_TS_ZES
           SET ADDRESS OF W_PIC_PAG TO ADDRESS OF MPAGDEBI
           SET ADDRESS OF TACH_CHAR TO ADDRESS OF TACH_PIC
           SET ADDRESS OF EIB_CODE TO ADDRESS OF EIB_RCODE
           SET ADDRESS OF FIL_EIBX TO ADDRESS OF FIL_EIB
           MOVE ALL '0' TO MW-REPEAT-CHAR--8
           MOVE MW-REPEAT-CHAR--8 TO Z_TIMER_DATJOU_FILE
           SET ADDRESS OF Z_DATH2 TO ADDRESS OF Z_TIMER_DATJOU_FILE
           SET ADDRESS OF Z_DATH3 TO ADDRESS OF Z_TIMER_DATJOU
           SET ADDRESS OF Z_TIMER TO ADDRESS OF Z_TIMER_TIMJOU
           SET ADDRESS OF Z_TIMER_TIMJOU_CAR TO ADDRESS OF
                                                         Z_TIMER_0HHMMSS
           SET ADDRESS OF TIMJOU_DARTY TO ADDRESS OF Z_TIMER_0HHMMSS
           SET ADDRESS OF Z_CWA_DATJOU TO ADDRESS OF Z_CWA
           SET ADDRESS OF TCT_DARTY TO ADDRESS OF Z_TCTUA
           MOVE B'00000000' TO INIT_BMS1(1)
           MOVE B'11001001' TO INIT_BMS1(2)
           MOVE B'11011001' TO INIT_BMS1(3)
           MOVE B'11111001' TO INIT_BMS1(4)
           MOVE B'01001101' TO INIT_BMS1(5)
           MOVE B'01111101' TO INIT_BMS1(6)
           MOVE B'01111100' TO INIT_BMS1(7)
           MOVE B'11000101' TO INIT_BMS1(8)
           MOVE B'11010101' TO INIT_BMS1(9)
           MOVE B'11110101' TO INIT_BMS1(10)
           MOVE B'11000011' TO INIT_BMS1(11)
           MOVE B'11001000' TO INIT_BMS1(12)
           MOVE B'00001100' TO INIT_BMS1(13)
           MOVE B'00011001' TO INIT_BMS1(14)
           MOVE B'01000000' TO INIT_BMS1(15)
           MOVE B'01000000' TO INIT_BMS1(16)
           MOVE B'11111111' TO INIT_BMS1(17)
           MOVE B'11111111' TO INIT_BMS1(18)
           SET ADDRESS OF FIL_BMS1 TO ADDRESS OF GRP--INIT_BMS1
           MOVE B'00000000' TO INIT_BMS2(1)
           MOVE B'01111101' TO INIT_BMS2(2)
           MOVE B'01101101' TO INIT_BMS2(3)
           MOVE B'01111110' TO INIT_BMS2(4)
           MOVE B'11100110' TO INIT_BMS2(5)
           MOVE B'11100111' TO INIT_BMS2(6)
           MOVE B'11001000' TO INIT_BMS2(7)
           MOVE B'01111111' TO INIT_BMS2(8)
           MOVE B'01101100' TO INIT_BMS2(9)
           MOVE B'01101110' TO INIT_BMS2(10)
           MOVE B'01101011' TO INIT_BMS2(11)
           MOVE B'11110001' TO INIT_BMS2(12)
           MOVE B'11110010' TO INIT_BMS2(13)
           MOVE B'11110011' TO INIT_BMS2(14)
           MOVE B'11110100' TO INIT_BMS2(15)
           MOVE B'11110101' TO INIT_BMS2(16)
           MOVE B'11110110' TO INIT_BMS2(17)
           MOVE B'11110111' TO INIT_BMS2(18)
           MOVE B'11111000' TO INIT_BMS2(19)
           MOVE B'11111001' TO INIT_BMS2(20)
           MOVE B'01111010' TO INIT_BMS2(21)
           MOVE B'01111011' TO INIT_BMS2(22)
           MOVE B'01111100' TO INIT_BMS2(23)
           MOVE B'11000001' TO INIT_BMS2(24)
           MOVE B'11000010' TO INIT_BMS2(25)
           MOVE B'11000011' TO INIT_BMS2(26)
           MOVE B'11000100' TO INIT_BMS2(27)
           MOVE B'11000101' TO INIT_BMS2(28)
           MOVE B'11000110' TO INIT_BMS2(29)
           MOVE B'11000111' TO INIT_BMS2(30)
           MOVE B'11001000' TO INIT_BMS2(31)
           MOVE B'11001001' TO INIT_BMS2(32)
           MOVE B'01001010' TO INIT_BMS2(33)
           MOVE B'01001011' TO INIT_BMS2(34)
           MOVE B'01001100' TO INIT_BMS2(35)
           MOVE B'10010001' TO INIT_BMS2(36)
           MOVE B'10011001' TO INIT_BMS2(37)
           MOVE B'10010011' TO INIT_BMS2(38)
           MOVE B'10010100' TO INIT_BMS2(39)
           MOVE B'10011011' TO INIT_BMS2(40)
           MOVE B'10011100' TO INIT_BMS2(41)
           MOVE B'10110110' TO INIT_BMS2(42)
           SET ADDRESS OF FIL_BMS2 TO ADDRESS OF GRP--INIT_BMS2
           SET ADDRESS OF Z_TWA_PRINTER TO ADDRESS OF LINK_TWA
           SET ADDRESS OF EIG66O TO ADDRESS OF Z_MAP
           SET ADDRESS OF FIGENREG TO ADDRESS OF FIG01
           SET ADDRESS OF FIG01_FIG01K_CLEF TO ADDRESS OF
                                                     FIG01_FIG01K_TCTPRT
           SET ADDRESS OF FIG01_FIG01K_CLEFX TO ADDRESS OF FIG01
           SET ADDRESS OF Z_COMMAREA_IG60 TO ADDRESS OF Z_COMMAREA
           SET ADDRESS OF COM_AIDA TO ADDRESS OF Z_COMMAREA
           SET ADDRESS OF COMMET00_STRU TO ADDRESS OF COMMET00_ABAN
           SET ADDRESS OF Z_STRTAREA_IG6A TO ADDRESS OF Z_STRTIG6A
           SET ADDRESS OF Z_STRTAREA_IGAL TO ADDRESS OF Z_STRTIGLP
           SET ADDRESS OF Z_STRTAREA_IGAG TO ADDRESS OF Z_STRTIGGP
           SET ADDRESS OF Z_SWAP TO ADDRESS OF Z_INOUT
           SET ADDRESS OF Z_INOUT_MAP TO ADDRESS OF Z_INOUT_MAP_FILLER
           SET ADDRESS OF Z_INOUT_COMMAREA TO ADDRESS OF
                                                 Z_INOUT_COMMAREA_FILLER
           SET ADDRESS OF MESS_ABCODE TO ADDRESS OF MESS
           SET ADDRESS OF TRACE_MESS TO ADDRESS OF TRACE_MESSX
           SET ADDRESS OF LINK_CWA TO PTR_CWA
           SET ADDRESS OF LINK_TWA TO PTR_TWA
           SET ADDRESS OF LINK_TCTUA TO PTR_TCTUA
           SET ADDRESS OF LINK_USER TO PTR_USER
           SET ADDRESS OF LINK_TS TO SWAP_PTR_TS
           SET ADDRESS OF GFJMSA TO ADDRESS OF GFJOUR
           MOVE 31 TO GFTABJ(1)
           MOVE 28 TO GFTABJ(2)
           MOVE 31 TO GFTABJ(3)
           MOVE 30 TO GFTABJ(4)
           MOVE 31 TO GFTABJ(5)
           MOVE 30 TO GFTABJ(6)
           MOVE 31 TO GFTABJ(7)
           MOVE 31 TO GFTABJ(8)
           MOVE 30 TO GFTABJ(9)
           MOVE 31 TO GFTABJ(10)
           MOVE 30 TO GFTABJ(11)
           MOVE 31 TO GFTABJ(12)
           MOVE 'JANVIER  ' TO GFTABLML(1)
           MOVE 'FEVRIER  ' TO GFTABLML(2)
           MOVE 'MARS     ' TO GFTABLML(3)
           MOVE 'AVRIL    ' TO GFTABLML(4)
           MOVE 'MAI      ' TO GFTABLML(5)
           MOVE 'JUIN     ' TO GFTABLML(6)
           MOVE 'JUILLET  ' TO GFTABLML(7)
           MOVE 'AOUT     ' TO GFTABLML(8)
           MOVE 'SEPTEMBRE' TO GFTABLML(9)
           MOVE 'OCTOBRE  ' TO GFTABLML(10)
           MOVE 'NOVEMBRE ' TO GFTABLML(11)
           MOVE 'DECEMBRE ' TO GFTABLML(12)
           MOVE 'JAN' TO GFTABLMC(1)
           MOVE 'FEV' TO GFTABLMC(2)
           MOVE 'MAR' TO GFTABLMC(3)
           MOVE 'AVR' TO GFTABLMC(4)
           MOVE 'MAI' TO GFTABLMC(5)
           MOVE 'JUN' TO GFTABLMC(6)
           MOVE 'JUL' TO GFTABLMC(7)
           MOVE 'AOU' TO GFTABLMC(8)
           MOVE 'SEP' TO GFTABLMC(9)
           MOVE 'OCT' TO GFTABLMC(10)
           MOVE 'NOV' TO GFTABLMC(11)
           MOVE 'DEC' TO GFTABLMC(12)
           MOVE 'LUNDI   ' TO GFTABLJ(1)
           MOVE 'MARDI   ' TO GFTABLJ(2)
           MOVE 'MERCREDI' TO GFTABLJ(3)
           MOVE 'JEUDI   ' TO GFTABLJ(4)
           MOVE 'VENDREDI' TO GFTABLJ(5)
           MOVE 'SAMEDI  ' TO GFTABLJ(6)
           MOVE 'DIMANCHE' TO GFTABLJ(7)
           CONTINUE.
       BEGIN--PROGRAM SECTION.
           PERFORM MODULE_ENTREE THRU E--MODULE_ENTREE
           IF FONCTION > '00' AND FONCTION < '99' THEN
             PERFORM MODULE_TRAITEMENT THRU E--MODULE_TRAITEMENT
           END-IF
           PERFORM MODULE_SORTIE THRU E--MODULE_SORTIE
           CONTINUE.
           COPY CBGOBACK.
       MODULE_ENTREE.
           PERFORM INIT_USER THRU E--INIT_USER
           PERFORM INIT_ADDRESS THRU E--INIT_ADDRESS
           PERFORM RECEPTION_MESSAGE THRU E--RECEPTION_MESSAGE
           CONTINUE.
       E--MODULE_ENTREE.
           EXIT.
       INIT_USER.
           SET ADDRESS OF KW--STRING TO ADDRESS OF Z_MAP
           COMPUTE MW--STG = LENGTH OF Z_MAP
           COMPUTE MW--STG = LENGTH OF Z_MAP
           MOVE LOW-VALUE TO KW--STRING(1:MW--STG)
           MOVE 'EIG66' TO NOM_MAP
           MOVE 'EIG66' TO NOM_MAPSET
           MOVE 'TIG66' TO NOM_PROG
           MOVE 'IG66' TO NOM_TACHE
           MOVE 'IG60' TO NOM_LEVEL_MAX
           MOVE 'NON' TO DEBUGGIN
           COMPUTE LONG_COMMAREA = COM_IG60_LONG_COMMAREA
           IF EIBCALEN NOT = 0 THEN
             MOVE Z_COMMAREA_PREF TO Z_COMMAREA
           ELSE
             INITIALIZE Z_COMMAREA
             PERFORM SORTIE_LEVEL_MAX THRU E--SORTIE_LEVEL_MAX
           END-IF
           MOVE COM_IG60_LIG TO NETNAME
           CONTINUE.
       E--INIT_USER.
           EXIT.
       INIT_ADDRESS.
           IF EIBTRMID NOT = ' ' AND EIBTRMID NOT = LOW-VALUE THEN
             EXEC CICS                                 ASSIGN TERMCODE (
                                                      TERMCODE) NOHANDLE
             END-EXEC
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           EXEC CICS                          ASSIGN CWALENG   (
                                                               CWA_LONG)
                                  TWALENG   (       TWA_LONG)
                                  TCTUALENG (       TCTUA_LONG)
                                  NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           IF CWA_LONG > 0 THEN
             EXEC CICS                          ADDRESS CWA    (
                                                       PTR_CWA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_CWA TO PTR_CWA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
             MOVE LINK_CWA TO Z_CWA
           ELSE
             MOVE 'CWA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           IF TWA_LONG > 0 THEN
             EXEC CICS                          ADDRESS TWA    (
                                                       PTR_TWA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_TWA TO PTR_TWA
             SET ADDRESS OF Z_TWA_PRINTER TO ADDRESS OF LINK_TWA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           ELSE
             MOVE 'TWA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           IF TCTUA_LONG > 0 THEN
             EXEC CICS                          ADDRESS TCTUA  (
                                                     PTR_TCTUA) NOHANDLE
             END-EXEC
             SET ADDRESS OF LINK_TCTUA TO PTR_TCTUA
             MOVE EIBRCODE TO EIB_RCODE
             IF EIB_CODE NOT = EIB_NORMAL THEN
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
             MOVE LINK_TCTUA TO Z_TCTUA
           ELSE
             MOVE 'TCTUA NON ADRESSEE' TO MESS
             PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           END-IF
           MOVE EIBDATE TO Z_TIMER_0HHMMSS
           MOVE EIB_ANNEE TO GFANNEE
           COMPUTE GFQNTA = EIB_QQQ
           MOVE '2' TO GFDATA
           PERFORM METDATE THRU E--METDATE
           MOVE '20' TO DATH2_SS
           MOVE GFANNEE TO DATH2_AA
           IF DATH2_AA > '50' THEN
             MOVE '19' TO DATH2_SS
           END-IF
           MOVE GFMOIS TO DATH2_MM
           MOVE GFJOUR TO DATH2_JJ
           MOVE GFANNEE TO DATH3_AA
           MOVE GFMOIS TO DATH3_MM
           MOVE GFJOUR TO DATH3_JJ
           MOVE EIBTIME TO Z_TIMER_0HHMMSS
           STRING Z_TIMER_TIMJOU_CAR(2) Z_TIMER_TIMJOU_CAR(3) DELIMITED
                                                    BY SIZE INTO TIME_HH
           STRING Z_TIMER_TIMJOU_CAR(4) Z_TIMER_TIMJOU_CAR(5) DELIMITED
                                                    BY SIZE INTO TIME_MM
           STRING Z_TIMER_TIMJOU_CAR(6) Z_TIMER_TIMJOU_CAR(7) DELIMITED
                                                    BY SIZE INTO TIME_SS
           CONTINUE.
       E--INIT_ADDRESS.
           EXIT.
       RECEPTION_MESSAGE.
           IF EIBTRNID NOT = NOM_TACHE THEN
             MOVE CODE_TRAITEMENT_AUTOMATIQUE TO FONCTION
             GO TO E--RECEPTION_MESSAGE
           END-IF
           PERFORM RECEIVE_MAP THRU E--RECEIVE_MAP
           MOVE EIBAID TO WORKAID
           IF WORKAID = TOUCHE_PF4 THEN
             MOVE CODE_LEVEL_MAX TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_PF3 THEN
             MOVE CODE_LEVEL_SUP TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_ENTER THEN
             MOVE CODE_TRAITEMENT_NORMAL TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_PF8 THEN
             MOVE CODE_SUIVANTE TO FONCTION
           END-IF
           IF WORKAID = TOUCHE_PF7 THEN
             MOVE CODE_PRECEDENTE TO FONCTION
           END-IF
           IF ETAT_ECRAN = ECRAN_MAPFAIL THEN
             SET ADDRESS OF KW--STRING TO ADDRESS OF Z_MAP
             COMPUTE MW--STG = LENGTH OF Z_MAP
             COMPUTE MW--STG = LENGTH OF Z_MAP
             MOVE LOW-VALUE TO KW--STRING(1:MW--STG)
             MOVE CODE_TRAITEMENT_AUTOMATIQUE TO FONCTION
           END-IF
           CONTINUE.
       E--RECEPTION_MESSAGE.
           EXIT.
       RECEIVE_MAP.
           EXEC CICS                          RECEIVE
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    INTO   (       Z_MAP)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE = EIB_NORMAL THEN
             MOVE '0' TO CODE_RETOUR
           ELSE
             IF EIB_CODE = EIB_MAPFAIL THEN
               MOVE '1' TO ETAT_ECRAN
             ELSE
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           CONTINUE.
       E--RECEIVE_MAP.
           EXIT.
       MODULE_TRAITEMENT.
           IF FONCTION = TRAITEMENT_NORMAL OR FONCTION = CODE_SUIVANTE
                                      OR FONCTION = CODE_PRECEDENTE THEN
             PERFORM MODULE_TRAITEMENT_NORMAL THRU
                                             E--MODULE_TRAITEMENT_NORMAL
           END-IF
           IF KONTROL NOT = OK THEN
             PERFORM SORTIE_ERREUR THRU E--SORTIE_ERREUR
           END-IF
           IF FONCTION > '04' AND FONCTION < '11' THEN
             PERFORM MODULE_TRAITEMENT_AUTOMATIQUE THRU
                                        E--MODULE_TRAITEMENT_AUTOMATIQUE
           END-IF
           CONTINUE.
       E--MODULE_TRAITEMENT.
           EXIT.
       MODULE_TRAITEMENT_NORMAL.
           PERFORM CONTROLE_SYNTAXE THRU E--CONTROLE_SYNTAXE
           IF KONTROL = OK THEN
             PERFORM CONTROLE_LOGIQUE THRU E--CONTROLE_LOGIQUE
             IF KONTROL = OK THEN
               IF FONCTION = CODE_SUIVANTE THEN
                 GO TO E--MODULE_TRAITEMENT_NORMAL
               END-IF
               IF FONCTION = CODE_PRECEDENTE THEN
                 GO TO E--MODULE_TRAITEMENT_NORMAL
               END-IF
               PERFORM TRAITEMENT_TACHE THRU E--TRAITEMENT_TACHE
             END-IF
           END-IF
           CONTINUE.
       E--MODULE_TRAITEMENT_NORMAL.
           EXIT.
       CONTROLE_SYNTAXE.
           PERFORM CTL_PAG_DEB THRU E--CTL_PAG_DEB
           IF KONTROL = '1' THEN
             GO TO E--CONTROLE_SYNTAXE
           END-IF
           PERFORM TIG65_LECT_TS THRU E--TIG65_LECT_TS
           MOVE 'N' TO TOP_CON
           MOVE 'N' TO TOP_IMP
           MOVE 'N' TO TOP_REP
           MOVE 1 TO IA
           GO TO LOOP--COND--0
           CONTINUE.
       LOOP--BODY--0.
           IF MSELECTI(IA) = ' ' THEN
             MOVE '1' TO MSELECTA(IA)
             GO TO FIN_DO_IA
           END-IF
           MOVE 'I' TO MSELECTA(IA)
           IF MSELECTI(IA) = '.' THEN
             MOVE '.' TO IG65_TS_SEL(IA)
             GO TO FIN_DO_IA
           END-IF
           IF MSELECTI(IA) = 'C' THEN
             MOVE 'C' TO IG65_TS_SEL(IA)
             MOVE 'O' TO TOP_CON
             COMPUTE COM_NB_CONC = COM_NB_CONC + 1
             GO TO FIN_DO_IA
           END-IF
           IF MSELECTI(IA) = 'I' THEN
             IF IG65_TS_SUP(IA) NOT = '000000' THEN
               STRING 'TIG66 * ' 'I = IMPRESSION IMPOSSIBLE '
                 'CONFIGURATION ANNULEE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             IF IG65_TS_IMP(IA) NOT = '000000' AND COM_DIGSA_IMP = 'U'
             THEN
               STRING 'TIG66 * ' 'I = IMPRESSION IMPOSSIBLE '
                'TYPE IMPRESSION UNIQUE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             IF IG65_TS_REP(IA) NOT = '000000' THEN
               STRING 'TIG66 * ' 'I = IMPRESSION IMPOSSIBLE '
                  'ETAT DEJA EN REPRISE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             MOVE 'I' TO IG65_TS_SEL(IA)
             MOVE 'O' TO TOP_IMP
             COMPUTE COM_NB_IMPC = COM_NB_IMPC + 1
             GO TO FIN_DO_IA
           END-IF
           IF MSELECTI(IA) = 'R' THEN
             IF IG65_TS_SUP(IA) NOT = '000000' THEN
               STRING 'TIG66 * ' 'R = REPRISE IMPOSSIBLE '
                 'CONFIGURATION ANNULEE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             IF COM_DIGSD_REP = 'N' THEN
               STRING 'TIG66 * ' 'R = REPRISE INCOMPATIBLE '
                        'AVEC SIGNATURE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             IF IG65_TS_IMP(IA) = '000000' THEN
               STRING 'TIG66 * ' 'R = REPRISE IMPOSSIBLE '
               'CONFIGURATION NON IMPRIMEE' DELIMITED BY SIZE INTO
                                                                MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             IF COM_DIGSA_IMP = 'M' THEN
               STRING 'TIG66 * ' 'R = PAS DE REPRISE SI '
                   'IMPRESSION MULTIPLE' DELIMITED BY SIZE INTO MLIBERRI
               PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
               GO TO E--CONTROLE_SYNTAXE
             END-IF
             MOVE 'R' TO IG65_TS_SEL(IA)
             MOVE 'O' TO TOP_REP
             COMPUTE COM_NB_REPC = COM_NB_REPC + 1
             GO TO FIN_DO_IA
           END-IF
           MOVE 'TIG66 * LA VALEUR DE CETTE ZONE EST IMPOSSIBLE' TO
                                                                MLIBERRI
           PERFORM SYNTAXE_ERREUR THRU E--SYNTAXE_ERREUR
           GO TO E--CONTROLE_SYNTAXE
           CONTINUE.
       FIN_DO_IA.
           COMPUTE IA = IA + 1
           CONTINUE.
       LOOP--COND--0.
           IF IA <= 15 THEN
             GO TO LOOP--BODY--0
           END-IF
           CONTINUE.
       E--CONTROLE_SYNTAXE.
           EXIT.
       TIG65_LECT_TS.
           MOVE COM_TS_NOMC TO IDENT_TS
           COMPUTE LONG_TS = COM_TS_LNGC
           COMPUTE RANG_TS = COM_TS_UTIC
           PERFORM READ_TS THRU E--READ_TS
           IF CODE_RETOUR NOT = '0' THEN
             PERFORM ERR_LECT_TS THRU E--ERR_LECT_TS
           END-IF
           MOVE Z_INOUT TO IG65_TS_ZES
           CONTINUE.
       E--TIG65_LECT_TS.
           EXIT.
       SYNTAXE_ERREUR.
           MOVE BRT_ALP_FSET TO MSELECTA(IA)
           COMPUTE MSELECTL(IA) = CURSEUR
           MOVE '1' TO KONTROL
           CONTINUE.
       E--SYNTAXE_ERREUR.
           EXIT.
       CTL_PAG_DEB.
           COMPUTE MW--LENGTH = LENGTH OF MPAGDEBI
           MOVE MW--LENGTH TO L_VAR
           IF MPAGDEBI NOT = LOW-VALUE AND MPAGDEBI NOT = ' ' THEN
             MOVE NUMERIQUE TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF MPAGDEBI MPAGDEBI LENGTH OF
                           NUMERIQUE NUMERIQUE VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE BRT_NUM_FSET TO MPAGDEBA
               IF KONTROL = OK THEN
                 STRING 'TIG66 * ' 'CETTE ZONE DOIT ' 'ETRE NUMERIQUE'
                                         DELIMITED BY SIZE INTO MLIBERRI
                 COMPUTE MPAGDEBL = CURSEUR
                 MOVE '1' TO KONTROL
               END-IF
             END-IF
           END-IF
           CONTINUE.
       E--CTL_PAG_DEB.
           EXIT.
       CONTROLE_LOGIQUE.
           MOVE COM_TS_NOMC TO IDENT_TS
           COMPUTE LONG_TS = COM_TS_LNGC
           COMPUTE RANG_TS = COM_TS_UTIC
           MOVE IG65_TS_ZES TO Z_INOUT
           PERFORM REWRITE_TS THRU E--REWRITE_TS
           IF TOP_REP = 'O' OR TOP_IMP = 'O' THEN
             PERFORM CTL_IMPRIM THRU E--CTL_IMPRIM
             IF KONTROL = '1' THEN
               GO TO E--CONTROLE_LOGIQUE
             END-IF
             PERFORM PRT_SYRD THRU E--PRT_SYRD
             IF KONTROL = '1' THEN
               GO TO E--CONTROLE_LOGIQUE
             END-IF
             PERFORM PRT_COMPATIBLE THRU E--PRT_COMPATIBLE
           END-IF
           MOVE MIMPRIMI TO COM_STRT_PRT
           CONTINUE.
       E--CONTROLE_LOGIQUE.
           EXIT.
       CTL_IMPRIM.
           COMPUTE MW--LENGTH = LENGTH OF MIMPRIMI
           MOVE MW--LENGTH TO L_VAR
           IF MIMPRIMI = 'XXXX' OR MIMPRIMI = '****' OR MIMPRIMI =
              '    ' OR MIMPRIMI = EIBTRMID OR MIMPRIMI = LOW-VALUE THEN
             IF KONTROL = OK THEN
               MOVE '1' TO KONTROL
               MOVE BRT_ALP_FSET TO MIMPRIMA
               COMPUTE MIMPRIML = CURSEUR
               STRING 'TIG66 * ' 'LA VALEUR DU CODE IMPRIMANTE '
                        'EST IMPOSSIBLE' DELIMITED BY SIZE INTO MLIBERRI
             END-IF
           END-IF
           CONTINUE.
       E--CTL_IMPRIM.
           EXIT.
       PRT_SYRD.
           MOVE MIMPRIMI TO Z_TWA_PRT
           INITIALIZE MESYRD
           PERFORM LINK_SYRD THRU E--LINK_SYRD
           EVALUATE Z_TWA_RET
             WHEN '0'
               GO TO E--PRT_SYRD
             WHEN '1'
               MOVE 'TERMINAL INEXISTANT' TO MESYRD
             WHEN '2'
               MOVE 'TERMINAL NON IMPRIMANTE' TO MESYRD
             WHEN '3'
               MOVE 'UNITE DE CONTROLE HORS SERVICE' TO MESYRD
             WHEN '4'
               MOVE 'TERMINAL HORS SERVICE' TO MESYRD
             WHEN '5'
               MOVE 'TERMINAL NON RECEIVE ONLY' TO MESYRD
             WHEN '6'
               MOVE 'TERMINAL ETAT QUIESCING' TO MESYRD
             WHEN OTHER
               PERFORM ABANDON_TACHE THRU E--ABANDON_TACHE
           END-EVALUATE
           MOVE '1' TO KONTROL
           MOVE BRT_ALP_FSET TO MIMPRIMA
           COMPUTE MIMPRIML = CURSEUR
           STRING 'TIG66 * ' 'SYRD' ' : ' Z_TWA_RET ' - ' MESYRD
                                         DELIMITED BY SIZE INTO MLIBERRI
           CONTINUE.
       E--PRT_SYRD.
           EXIT.
       PRT_COMPATIBLE.
           IF COM_DIGSA_PTC = 'T' OR COM_DIGSA_PTC = ' ' THEN
             GO TO E--PRT_COMPATIBLE
           END-IF
           MOVE MIMPRIMI TO WCLEF_TCTPRT
           MOVE COM_IG60_CICS TO WCLEF_APPLID
           PERFORM CLEF_FIG01K THRU E--CLEF_FIG01K
           PERFORM READ_EQUAL THRU E--READ_EQUAL
           IF CODE_RETOUR NOT = TROUVE THEN
             MOVE '1' TO KONTROL
             MOVE BRT_ALP_FSET TO MIMPRIMA
             COMPUTE MIMPRIML = CURSEUR
             STRING 'TIG66 * '
             'IMPRIMANTE INCONNUE DANS LE FICHIER FIGPRT' DELIMITED BY
                                                      SIZE INTO MLIBERRI
             GO TO E--PRT_COMPATIBLE
           END-IF
           MOVE Z_INOUT TO FIG01
           IF FIG01_PTC NOT = COM_DIGSA_PTC THEN
             STRING 'TIG66 * ' 'PROTOCOLE ETAT ET '
              'IMPRIMANTE INCOMPATIBLES' DELIMITED BY SIZE INTO MLIBERRI
             MOVE BRT_ALP_FSET TO MIMPRIMA
             COMPUTE MIMPRIML = CURSEUR
             MOVE '1' TO KONTROL
             GO TO E--PRT_COMPATIBLE
           END-IF
           MOVE FIG01_MAR TO STRT_IGAL_MRQ
           MOVE FIG01_IDE TO STRT_IGAL_MDL
           MOVE FIG01_MAR TO STRT_IGAG_MRQ
           MOVE FIG01_IDE TO STRT_IGAG_MDL
           CONTINUE.
       E--PRT_COMPATIBLE.
           EXIT.
       TRAITEMENT_TACHE.
           PERFORM TRAITEMENT_FICHIER THRU E--TRAITEMENT_FICHIER
           CONTINUE.
       E--TRAITEMENT_TACHE.
           EXIT.
       TRAITEMENT_FICHIER.
           PERFORM DETERMINATION_TRAITEMENT THRU
                                             E--DETERMINATION_TRAITEMENT
           CONTINUE.
       E--TRAITEMENT_FICHIER.
           EXIT.
       DETERMINATION_TRAITEMENT.
           IF COM_NB_REPC > 0 OR COM_NB_IMPC > 0 THEN
             MOVE EIBTASKN TO TACH_PIC
             STRING EIBTRMID TACH_CHAR_2 DELIMITED BY SIZE INTO
                                                              COM_TS_NOM
             MOVE 37 TO COM_TS_LNG
             MOVE -1 TO COM_TS_MAX
             MOVE 0 TO ITEM_IMP_REP
             MOVE COM_TS_NOM TO IDENT_TS
             PERFORM DELETE_TS THRU E--DELETE_TS
             PERFORM TS_IMP_REP THRU E--TS_IMP_REP
             IF COM_TS_MAX > 0 THEN
               IF COM_DIGSA_PTC = 'T' THEN
                 PERFORM START_TEXTE THRU E--START_TEXTE
               END-IF
               IF COM_DIGSA_PTC = 'L' THEN
                 PERFORM START_LASER THRU E--START_LASER
               END-IF
               IF COM_DIGSA_PTC = 'G' THEN
                 PERFORM START_GRAPHIQUE THRU E--START_GRAPHIQUE
               END-IF
               PERFORM START_TACHE THRU E--START_TACHE
             END-IF
           END-IF
           MOVE -1 TO COM_TS_MAXL
           IF COM_NB_CONC > 0 THEN
             STRING EIBTRMID NOM_TACHE DELIMITED BY SIZE INTO
                                                             COM_TS_NOML
             MOVE 37 TO COM_TS_LNGL
             MOVE 0 TO COM_TS_UTIL
             MOVE 0 TO COM_TS_PAGL
             MOVE 0 TO ITEM_CON_LIG
             MOVE COM_TS_NOML TO IDENT_TS
             PERFORM DELETE_TS THRU E--DELETE_TS
             PERFORM TS_CON_LIG THRU E--TS_CON_LIG
           END-IF
           IF COM_TS_MAXL <= 0 THEN
             MOVE CODE_LEVEL_SUP TO FONCTION
           ELSE
             MOVE COM_TS_NOMC TO IDENT_TS
             PERFORM DELETE_TS THRU E--DELETE_TS
           END-IF
           CONTINUE.
       E--DETERMINATION_TRAITEMENT.
           EXIT.
       START_TEXTE.
           MOVE COM_DIGSA_CFG TO STRT_IG6A_CFG
           MOVE COM_DIGSA_IMP TO STRT_IG6A_IMP
           COMPUTE STRT_IG6A_HTR = COM_DIGSA_HTR
           COMPUTE STRT_IG6A_LNG = COM_DIGSA_LNG
           COMPUTE STRT_IG6A_COL = COM_DIGSA_COL
           MOVE COM_STRT_PRT TO STRT_IG6A_PRT
           MOVE 'TIG66' TO STRT_IG6A_PGM
           MOVE COM_TS_NOM TO STRT_IG6A_TSN
           COMPUTE STRT_IG6A_TSL = COM_TS_LNG
           COMPUTE STRT_IG6A_TSI = COM_TS_MAX
           MOVE GFAMJ TO STRT_IG6A_AMJ
           MOVE STRT_IG6A_PRT TO TERM_START
           MOVE 'IG6A' TO NOM_TACHE_START
           COMPUTE LONG_START = STRT_IG6A_LONG_AREA
           MOVE Z_STRTIG6A TO Z_START
           CONTINUE.
       E--START_TEXTE.
           EXIT.
       START_LASER.
           MOVE COM_DIGSA_CFG TO STRT_IGAL_CFG
           MOVE COM_DIGSA_IMP TO STRT_IGAL_IMP
           COMPUTE STRT_IGAL_CMD = COM_DIGSA_CMD
           COMPUTE STRT_IGAL_DON = COM_DIGSA_DON
           MOVE COM_DIGSA_FOR TO STRT_IGAL_FOR
           MOVE COM_STRT_PRT TO STRT_IGAL_PRT
           MOVE 'TIG66' TO STRT_IGAL_PGM
           MOVE COM_TS_NOM TO STRT_IGAL_TSN
           COMPUTE STRT_IGAL_TSL = COM_TS_LNG
           COMPUTE STRT_IGAL_TSI = COM_TS_MAX
           MOVE GFAMJ TO STRT_IGAL_AMJ
           MOVE STRT_IGAL_PRT TO TERM_START
           MOVE 'IGAL' TO NOM_TACHE_START
           COMPUTE LONG_START = STRT_IGAL_LONG_AREA
           MOVE Z_STRTIGLP TO Z_START
           CONTINUE.
       E--START_LASER.
           EXIT.
       START_GRAPHIQUE.
           MOVE COM_DIGSA_CFG TO STRT_IGAG_CFG
           MOVE COM_DIGSA_IMP TO STRT_IGAG_IMP
           COMPUTE STRT_IGAG_CMD = COM_DIGSA_CMD
           COMPUTE STRT_IGAG_DON = COM_DIGSA_DON
           COMPUTE STRT_IGAG_HTR = COM_DIGSA_HTR
           MOVE COM_STRT_PRT TO STRT_IGAG_PRT
           MOVE 'TIG66' TO STRT_IGAG_PGM
           MOVE COM_TS_NOM TO STRT_IGAG_TSN
           COMPUTE STRT_IGAG_TSL = COM_TS_LNG
           COMPUTE STRT_IGAG_TSI = COM_TS_MAX
           MOVE GFAMJ TO STRT_IGAG_AMJ
           MOVE STRT_IGAG_PRT TO TERM_START
           MOVE 'IGAG' TO NOM_TACHE_START
           COMPUTE LONG_START = STRT_IGAG_LONG_AREA
           MOVE Z_STRTIGGP TO Z_START
           CONTINUE.
       E--START_GRAPHIQUE.
           EXIT.
       TS_IMP_REP.
           COMPUTE ENDVAL--26 = COM_TS_MAXC
           PERFORM VARYING IA FROM 1 BY 1 UNTIL IA > ENDVAL--26
             MOVE COM_TS_NOMC TO IDENT_TS
             COMPUTE LONG_TS = COM_TS_LNGC
             COMPUTE RANG_TS = IA
             PERFORM READ_TS THRU E--READ_TS
             IF CODE_RETOUR NOT = '0' THEN
               PERFORM ERR_LECT_TS THRU E--ERR_LECT_TS
             END-IF
             MOVE Z_INOUT TO IG65_TS_ZES
             PERFORM VARYING IC FROM 1 BY 1 UNTIL IC > 15
               IF IG65_TS_SEL(IC) = 'I' THEN
                 PERFORM WRITE_TS_IMP_REP THRU E--WRITE_TS_IMP_REP
               END-IF
               IF IG65_TS_SEL(IC) = 'R' THEN
                 PERFORM WRITE_TS_IMP_REP THRU E--WRITE_TS_IMP_REP
               END-IF
             END-PERFORM
           END-PERFORM
           CONTINUE.
       E--TS_IMP_REP.
           EXIT.
       WRITE_TS_IMP_REP.
           INITIALIZE ZES_IMP_REP
           COMPUTE ITEM_IMP_REP = ITEM_IMP_REP + 1
           MOVE COM_TS_NOM TO IDENT_TS
           COMPUTE LONG_TS = COM_TS_LNG
           COMPUTE RANG_TS = ITEM_IMP_REP
           STRING IG65_TS_SEL(IC) IG65_TS_ETA(IC) IG65_TS_DAT(IC)
           IG65_TS_DST(IC) IG65_TS_DOC(IC) DELIMITED BY SIZE INTO
                                                             ZES_IMP_REP
           MOVE ZES_IMP_REP TO Z_INOUT
           PERFORM WRITE_TS THRU E--WRITE_TS
           COMPUTE COM_TS_MAX = ITEM_IMP_REP
           COMPUTE STRT_IGAL_TSI = ITEM_IMP_REP
           CONTINUE.
       E--WRITE_TS_IMP_REP.
           EXIT.
       TS_CON_LIG.
           COMPUTE ENDVAL--27 = COM_TS_MAXC
           PERFORM VARYING IA FROM 1 BY 1 UNTIL IA > ENDVAL--27
             MOVE COM_TS_NOMC TO IDENT_TS
             COMPUTE LONG_TS = COM_TS_LNGC
             COMPUTE RANG_TS = IA
             PERFORM READ_TS THRU E--READ_TS
             IF CODE_RETOUR NOT = '0' THEN
               PERFORM ERR_LECT_TS THRU E--ERR_LECT_TS
             END-IF
             MOVE Z_INOUT TO IG65_TS_ZES
             PERFORM VARYING IC FROM 1 BY 1 UNTIL IC > 15
               IF IG65_TS_SEL(IC) = 'C' THEN
                 PERFORM WRITE_TS_CON THRU E--WRITE_TS_CON
               END-IF
             END-PERFORM
           END-PERFORM
           CONTINUE.
       E--TS_CON_LIG.
           EXIT.
       WRITE_TS_CON.
           INITIALIZE ZES_CON_LIG
           COMPUTE ITEM_CON_LIG = ITEM_CON_LIG + 1
           MOVE COM_TS_NOML TO IDENT_TS
           COMPUTE LONG_TS = COM_TS_LNGL
           COMPUTE RANG_TS = ITEM_CON_LIG
           STRING IG65_TS_SEL(IC) IG65_TS_ETA(IC) IG65_TS_DAT(IC)
           IG65_TS_DST(IC) IG65_TS_DOC(IC) DELIMITED BY SIZE INTO
                                                             ZES_CON_LIG
           MOVE ZES_CON_LIG TO Z_INOUT
           PERFORM WRITE_TS THRU E--WRITE_TS
           COMPUTE COM_TS_MAXL = ITEM_CON_LIG
           CONTINUE.
       E--WRITE_TS_CON.
           EXIT.
       MODULE_TRAITEMENT_AUTOMATIQUE.
           PERFORM POSITIONNEMENT_FICHIER THRU E--POSITIONNEMENT_FICHIER
           PERFORM REMPLISSAGE_FORMAT_ECRAN THRU
                                             E--REMPLISSAGE_FORMAT_ECRAN
           PERFORM SAVE_POSITIONNEMENT THRU E--SAVE_POSITIONNEMENT
           CONTINUE.
       E--MODULE_TRAITEMENT_AUTOMATIQUE.
           EXIT.
       POSITIONNEMENT_FICHIER.
           STRING 'TIG66 * ' '==> C:CONSULTATION  ' '==> I:IMPRESSION  '
            '==> R:REPRISE      ' '==> PF3-4-7-8 ENTER' DELIMITED BY
                                                      SIZE INTO MLIBERRI
           IF COM_TS_UTIC = 0 THEN
             MOVE 1 TO COM_TS_PAGC
             PERFORM TIG65_READ_TS THRU E--TIG65_READ_TS
             GO TO E--POSITIONNEMENT_FICHIER
           END-IF
           COMPUTE W_BIN_PAG = W_PIC_PAG
           IF W_BIN_PAG < COM_TS_PAGC THEN
             MOVE CODE_PRECEDENTE TO FONCTION
             COMPUTE COM_TS_PAGC = W_BIN_PAG + 1
           END-IF
           IF W_BIN_PAG > COM_TS_PAGC THEN
             MOVE CODE_SUIVANTE TO FONCTION
             COMPUTE COM_TS_PAGC = W_BIN_PAG - 1
           END-IF
           IF FONCTION = CODE_PRECEDENTE THEN
             IF COM_TS_PAGC <= 1 THEN
               MOVE 1 TO COM_TS_PAGC
               MOVE 'TIG66 * PREMIERE PAGE' TO MLIBERRI
             ELSE
               COMPUTE COM_TS_PAGC = COM_TS_PAGC - 1
             END-IF
             PERFORM TIG65_READ_TS THRU E--TIG65_READ_TS
             GO TO E--POSITIONNEMENT_FICHIER
           END-IF
           IF FONCTION = CODE_SUIVANTE THEN
             IF COM_TS_PAGC >= COM_TS_MAXC THEN
               COMPUTE COM_TS_PAGC = COM_TS_MAXC
               MOVE 'TIG66 * DERNIERE PAGE' TO MLIBERRI
             ELSE
               COMPUTE COM_TS_PAGC = COM_TS_PAGC + 1
             END-IF
             PERFORM TIG65_READ_TS THRU E--TIG65_READ_TS
             GO TO E--POSITIONNEMENT_FICHIER
           END-IF
           CONTINUE.
       E--POSITIONNEMENT_FICHIER.
           EXIT.
       TIG65_READ_TS.
           MOVE COM_TS_NOMC TO IDENT_TS
           COMPUTE LONG_TS = COM_TS_LNGC
           COMPUTE RANG_TS = COM_TS_PAGC
           PERFORM READ_TS THRU E--READ_TS
           IF CODE_RETOUR NOT = '0' THEN
             PERFORM ERR_LECT_TS THRU E--ERR_LECT_TS
           END-IF
           MOVE Z_INOUT TO IG65_TS_ZES
           CONTINUE.
       E--TIG65_READ_TS.
           EXIT.
       ERR_LECT_TS.
           MOVE 'ERREUR LECTURE TS' TO MESS
           PERFORM DELETE_TS THRU E--DELETE_TS
           PERFORM ABANDON_TACHE THRU E--ABANDON_TACHE
           CONTINUE.
       E--ERR_LECT_TS.
           EXIT.
       REMPLISSAGE_FORMAT_ECRAN.
           PERFORM REMPLISSAGE_ZONES_OBLIGATOIRES THRU
                                       E--REMPLISSAGE_ZONES_OBLIGATOIRES
           PERFORM REMPLISSAGE_ZONES_PROTEGEES THRU
                                          E--REMPLISSAGE_ZONES_PROTEGEES
           CONTINUE.
       E--REMPLISSAGE_FORMAT_ECRAN.
           EXIT.
       REMPLISSAGE_ZONES_OBLIGATOIRES.
           MOVE COM_DIGSA_TIT TO MLIBELEI
           MOVE COM_DIGSA_CFG TO MCONFIGI
           MOVE COM_DIGSA_IMP TO MTYPIMPI
           MOVE COM_DIGSA_ATT TO MATTACHI
           MOVE COM_DIGSA_PTC TO MPROTOCI
           IF COM_TYP_SEL = 'C' THEN
             MOVE 'C = ETATS NON IMPRIMES' TO MCONSULI
           END-IF
           IF COM_TYP_SEL = 'I' THEN
             MOVE 'I = ETATS IMPRIMES' TO MCONSULI
           END-IF
           IF COM_TYP_SEL = 'R' THEN
             MOVE 'R = ETATS EN REPRISE' TO MCONSULI
           END-IF
           IF COM_TYP_SEL = 'A' THEN
             MOVE 'A = ETATS ANNULES' TO MCONSULI
           END-IF
           IF COM_TYP_SEL = '*' THEN
             MOVE '* = LISTE INTEGRALE' TO MCONSULI
           END-IF
           MOVE 'I' TO MIMPRIMA
           IF COM_IG60_PR1 NOT = 'XXXX' THEN
             MOVE COM_IG60_PR1 TO MIMPRIMI
             GO TO E--REMPLISSAGE_ZONES_OBLIGATOIRES
           END-IF
           IF COM_IG60_PR2 NOT = 'XXXX' THEN
             MOVE COM_IG60_PR1 TO MIMPRIMI
             GO TO E--REMPLISSAGE_ZONES_OBLIGATOIRES
           END-IF
           MOVE 'XXXX' TO MIMPRIMI
           CONTINUE.
       E--REMPLISSAGE_ZONES_OBLIGATOIRES.
           EXIT.
       REMPLISSAGE_ZONES_PROTEGEES.
           MOVE 1 TO I
           GO TO LOOP--COND--1
           CONTINUE.
       LOOP--BODY--1.
           IF IG65_TS_ETA(I) NOT = '      ' THEN
             MOVE IG65_TS_SEL(I) TO MSELECTI(I)
             MOVE BRT_ALP_FSET TO MSELECTA(I)
           ELSE
             MOVE ' ' TO MSELECTI(I)
             MOVE NOR_PRO_FSET TO MSELECTA(I)
           END-IF
           MOVE IG65_TS_ETA(I) TO MNOMETAI(I)
           MOVE IG65_TS_DAT(I) TO MDATEDII(I)
           MOVE IG65_TS_DST(I) TO MDESTINI(I)
           MOVE IG65_TS_DOC(I) TO MDOCUMEI(I)
           MOVE IG65_TS_TRA(I) TO MSTATUTI(I)
           IF COM_DIGSA_CFG < '5' THEN
             IF IG65_TS_EDI(I) = '000000' THEN
               MOVE MDATEDII(I) TO MDATDERI(I)
             ELSE
               MOVE IG65_TS_EDI(I) TO MDATDERI(I)
             END-IF
           ELSE
             MOVE IG65_TS_EDI(I) TO MDATDERI(I)
           END-IF
           IF IG65_TS_IMP(I) = '000000' THEN
             MOVE '      ' TO MDATIMPI(I)
           ELSE
             MOVE IG65_TS_IMP(I) TO MDATIMPI(I)
           END-IF
           IF IG65_TS_REP(I) = '000000' THEN
             MOVE '      ' TO MDATREPI(I)
           ELSE
             MOVE IG65_TS_REP(I) TO MDATREPI(I)
           END-IF
           IF IG65_TS_SUP(I) = '000000' THEN
             MOVE '      ' TO MDATSUPI(I)
           ELSE
             MOVE IG65_TS_SUP(I) TO MDATSUPI(I)
           END-IF
           IF IG65_TS_CON(I) = '000000' THEN
             MOVE '      ' TO MDATCONI(I)
           ELSE
             MOVE IG65_TS_CON(I) TO MDATCONI(I)
           END-IF
           CONTINUE.
       FIN_DO_LOOP.
           COMPUTE I = I + 1
           CONTINUE.
       LOOP--COND--1.
           IF I <= 15 THEN
             GO TO LOOP--BODY--1
           END-IF
           COMPUTE MSELECTL(1) = CURSEUR
           COMPUTE W_PIC_PAG = COM_TS_PAGC
           MOVE W_PIC_PAG TO MPAGDEBI
           MOVE BRT_NUM_FSET TO MPAGDEBA
           COMPUTE W_PIC_MAX = COM_TS_MAXC
           MOVE W_PIC_MAX TO MPAGFINI
           MOVE BRT_PRO_FSET TO MPAGFINA
           CONTINUE.
       E--REMPLISSAGE_ZONES_PROTEGEES.
           EXIT.
       SAVE_POSITIONNEMENT.
           COMPUTE COM_TS_UTIC = COM_TS_PAGC
           CONTINUE.
       E--SAVE_POSITIONNEMENT.
           EXIT.
       MODULE_SORTIE.
           IF FONCTION > '04' AND FONCTION < '11' THEN
             PERFORM SORTIE_AFFICHAGE_FORMAT THRU
                                              E--SORTIE_AFFICHAGE_FORMAT
           END-IF
           IF KONTROL NOT = OK THEN
             PERFORM SORTIE_ERREUR THRU E--SORTIE_ERREUR
           END-IF
           IF FONCTION = TRAITEMENT_NORMAL THEN
             PERFORM SORTIE_SUITE THRU E--SORTIE_SUITE
           END-IF
           IF FONCTION = LEVEL_SUP THEN
             PERFORM SORTIE_LEVEL_SUPERIEUR THRU
                                               E--SORTIE_LEVEL_SUPERIEUR
           END-IF
           IF FONCTION = LEVEL_MAX THEN
             PERFORM SORTIE_LEVEL_MAX THRU E--SORTIE_LEVEL_MAX
           END-IF
           IF FONCTION = ERREUR_MANIPULATION THEN
             PERFORM SORTIE_ERREUR_MANIP THRU E--SORTIE_ERREUR_MANIP
           END-IF
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS
           PERFORM ABANDON_TACHE THRU E--ABANDON_TACHE
           CONTINUE.
       E--MODULE_SORTIE.
           EXIT.
       SORTIE_AFFICHAGE_FORMAT.
           PERFORM SEND_MAP THRU E--SEND_MAP
           MOVE NOM_TACHE TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_AFFICHAGE_FORMAT.
           EXIT.
       SORTIE_ERREUR.
           PERFORM VARYING IA FROM 1 BY 1 UNTIL IA > 15
             IF IG65_TS_ETA(IA) = ' ' THEN
               MOVE NOR_PRO_FSET TO MSELECTA(IA)
               MOVE ' ' TO MSELECTI(IA)
             END-IF
           END-PERFORM
           PERFORM SEND_MAP_ERREUR THRU E--SEND_MAP_ERREUR
           MOVE EIBTRNID TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_ERREUR.
           EXIT.
       SORTIE_SUITE.
           IF COM_DIGSA_PTC = 'T' THEN
             MOVE 'TIG68' TO NOM_PROG_XCTL
           END-IF
           IF COM_DIGSA_PTC = 'L' OR COM_DIGSA_PTC = 'G' THEN
             MOVE 'TIG6D' TO NOM_PROG_XCTL
           END-IF
           MOVE NOM_PROG TO COM_PGMPRC
           PERFORM XCTL_PROG_COMMAREA THRU E--XCTL_PROG_COMMAREA
           CONTINUE.
       E--SORTIE_SUITE.
           EXIT.
       SORTIE_LEVEL_SUPERIEUR.
           MOVE 'TIG65' TO NOM_PROG_XCTL
           MOVE NOM_PROG TO COM_PGMPRC
           PERFORM XCTL_PROG_COMMAREA THRU E--XCTL_PROG_COMMAREA
           CONTINUE.
       E--SORTIE_LEVEL_SUPERIEUR.
           EXIT.
       SORTIE_LEVEL_MAX.
           MOVE 'TIG60' TO NOM_PROG_XCTL
           MOVE NOM_PROG TO COM_PGMPRC
           PERFORM XCTL_PROG_COMMAREA THRU E--XCTL_PROG_COMMAREA
           CONTINUE.
       E--SORTIE_LEVEL_MAX.
           EXIT.
       SORTIE_ERREUR_MANIP.
           MOVE 'TIG66 * ERREUR MANIPULATION' TO MLIBERRI
           COMPUTE MSELECTL(1) = CURSEUR
           PERFORM SEND_MAP_ERREUR THRU E--SEND_MAP_ERREUR
           MOVE NOM_TACHE TO NOM_TACHE_RETOUR
           PERFORM RETOUR_COMMAREA THRU E--RETOUR_COMMAREA
           CONTINUE.
       E--SORTIE_ERREUR_MANIP.
           EXIT.
       READ_EQUAL.
           EXEC CICS                         READ     DATASET (
                                                              FILE_NAME)
                                    RIDFLD  (       VSAM_KEY)
                                    INTO    (       Z_INOUT)
                                    LENGTH  (       FILE_LONG)
                                    EQUAL
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE = EIB_NORMAL THEN
             MOVE '0' TO CODE_RETOUR
             GO TO E--READ_EQUAL
           END-IF
           IF EIB_CODE = EIB_NOTFND THEN
             MOVE '1' TO CODE_RETOUR
             GO TO E--READ_EQUAL
           END-IF
           IF EIB_CODE = EIB_DUPKEY THEN
             MOVE '7' TO CODE_RETOUR
             GO TO E--READ_EQUAL
           END-IF
           PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           CONTINUE.
       E--READ_EQUAL.
           EXIT.
       CLEF_FIG01K.
           MOVE WCLEF_TCTPRT TO FIG01_FIG01K_TCTPRT
           MOVE WCLEF_APPLID TO FIG01_FIG01K_APPLID
           MOVE FIG01_FIG01K_CLEF TO VSAM_KEY
           MOVE 'FIGPRT' TO FILE_NAME
           MOVE +100 TO FILE_LONG
           MOVE FIG01 TO Z_INOUT
           CONTINUE.
       E--CLEF_FIG01K.
           EXIT.
       RETOUR_COMMAREA.
           EXEC CICS                          RETURN
                                    TRANSID  (       NOM_TACHE_RETOUR)
                                    COMMAREA (       Z_COMMAREA)
                                    LENGTH   (       LONG_COMMAREA)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--RETOUR_COMMAREA.
           EXIT.
       SEND_MAP_ERREUR.
           EXEC CICS                          SEND
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    FROM   (       Z_MAP)
                                    ERASE
                                    CURSOR
                                    ALARM
                                    FRSET
                                    FREEKB
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_MAP_ERREUR.
           EXIT.
       SEND_MAP.
           EXEC CICS                          SEND
                                    MAP    (       NOM_MAP)
                                    MAPSET (       NOM_MAPSET)
                                    FROM   (       Z_MAP)
                                    ERASE
                                    CURSOR
                                     NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--SEND_MAP.
           EXIT.
       START_TACHE.
           EXEC CICS                          START
                                    TRANSID  (       NOM_TACHE_START)
                                    TERMID   (       TERM_START)
                                    FROM     (       Z_START)
                                    LENGTH   (       LONG_START)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--START_TACHE.
           EXIT.
       XCTL_PROG_COMMAREA.
           EXEC CICS                       SYNCPOINT
           END-EXEC
           EXEC CICS                          XCTL
                                    PROGRAM  (       NOM_PROG_XCTL)
                                    COMMAREA (       Z_COMMAREA)
                                    LENGTH   (       LONG_COMMAREA)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--XCTL_PROG_COMMAREA.
           EXIT.
       DELETE_TS.
           EXEC CICS                       DELETEQ TS
                                    QUEUE    (       IDENT_TS)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE = EIB_NORMAL THEN
             MOVE '0' TO CODE_RETOUR
           ELSE
             IF EIB_CODE = EIB_QIDERR THEN
               MOVE '1' TO CODE_RETOUR
             ELSE
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           CONTINUE.
       E--DELETE_TS.
           EXIT.
       READ_TS.
           EXEC CICS                          READQ TS
                                    QUEUE    (       IDENT_TS)
                                    INTO     (       Z_INOUT)
                                    LENGTH   (       LONG_TS)
                                    ITEM     (       RANG_TS)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE = EIB_NORMAL THEN
             MOVE '0' TO CODE_RETOUR
           ELSE
             IF EIB_CODE = EIB_QIDERR OR EIB_CODE = EIB_ITEMERR THEN
               MOVE '1' TO CODE_RETOUR
             ELSE
               PERFORM ABANDON_CICS THRU E--ABANDON_CICS
             END-IF
           END-IF
           CONTINUE.
       E--READ_TS.
           EXIT.
       REWRITE_TS.
           EXEC CICS                          WRITEQ TS
                                    QUEUE    (       IDENT_TS)
                                    FROM     (       Z_INOUT)
                                    LENGTH   (       LONG_TS)
                                    ITEM     (       RANG_TS)
                                    REWRITE
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--REWRITE_TS.
           EXIT.
       WRITE_TS.
           EXEC CICS                          WRITEQ TS
                                    QUEUE    (       IDENT_TS)
                                    FROM     (       Z_INOUT)
                                    LENGTH   (       LONG_TS)
                                    ITEM     (       RANG_TS)
                                    NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--WRITE_TS.
           EXIT.
       METDATE.
           INITIALIZE GFMESS
           MOVE B'0' TO FLAG1
           MOVE B'0' TO FLAG2
           MOVE B'0' TO FLAG3
           MOVE B'0' TO FLAG4
           MOVE '1234569' TO VERIFY-MASK
           CALL SP-VERIFY USING LENGTH OF GFDATA GFDATA LENGTH OF
                           '1234569' '1234569' VERIFY-MASK RETURN-VERIFY
           IF RETURN-VERIFY NOT = 0 THEN
             MOVE 'CODE APPEL INVALIDE' TO GFMESS
             GO TO GFERDAT
           END-IF
           IF GFDATA = '1' THEN
             MOVE B'1' TO FLAG1
           END-IF
           IF GFDATA = '2' THEN
             MOVE B'1' TO FLAG2
           END-IF
           IF GFDATA = '3' THEN
             MOVE B'1' TO FLAG3
           END-IF
           IF GFDATA = '4' THEN
             MOVE B'1' TO FLAG4
           END-IF
           IF GFDATA = '5' THEN
             MOVE GFJMA_ED(1:2) TO GFJOUR
             MOVE GFJMA_ED(4:2) TO GFMOIS
             MOVE GFJMA_ED(7:2) TO GFANNEE
             MOVE B'1' TO FLAG1
           END-IF
           IF GFDATA = '6' THEN
             MOVE GFJMSA_ED(1:2) TO GFJOUR
             MOVE GFJMSA_ED(4:2) TO GFMOIS
             MOVE GFJMSA_ED(7:2) TO GFSIECLE
             MOVE GFJMSA_ED(9:2) TO GFANNEE
             MOVE B'1' TO FLAG4
           END-IF
           IF GFDATA = '9' THEN
             MOVE FUNCTION CURRENT-DATE(7:2) TO GFJOUR
             MOVE FUNCTION CURRENT-DATE(5:2) TO GFMOIS
             MOVE FUNCTION CURRENT-DATE(3:2) TO GFANNEE
             MOVE FUNCTION CURRENT-DATE(1:2) TO GFSIECLE
             MOVE B'1' TO FLAG4
           END-IF
           IF FLAG1 OR FLAG2 THEN
             IF GFAA < 50 THEN
               MOVE 20 TO GFSS
             ELSE
               MOVE 19 TO GFSS
             END-IF
           END-IF
           IF FLAG1 THEN
             MOVE B'0' TO FLAG1
             MOVE B'1' TO FLAG4
           END-IF
           IF FLAG4 OR FLAG2 THEN
             IF GFSS < 19 OR GFSS > 20 THEN
               MOVE 'SIECLE INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFANNEE GFANNEE LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'ANNEE NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             COMPUTE GFBS = GFSS
             COMPUTE GFBA = GFAA
             PERFORM BISSEXTILE THRU E--BISSEXTILE
           END-IF
           IF FLAG4 THEN
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFMOIS GFMOIS LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'MOIS NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFJOUR GFJOUR LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               MOVE 'JOUR NON NUMERIQUE' TO GFMESS
               GO TO GFERDAT
             END-IF
             COMPUTE GFBM = GFMM
             COMPUTE GFBJ = GFJJ
             IF GFBM < 1 OR GFBM > 12 THEN
               MOVE 'MOIS HORS LIMITES' TO GFMESS
               GO TO GFERDAT
             END-IF
             IF GFBJ < 1 OR GFBJ > GFTABJ(GFBM) THEN
               MOVE 'JOUR HORS LIMITES' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG2 THEN
             COMPUTE GFBQTA = GFQNTA
             IF GFBISS THEN
               MOVE 366 TO GFX1
             ELSE
               MOVE 365 TO GFX1
             END-IF
             IF GFBQTA < 1 OR GFBQTA > GFX1 THEN
               MOVE 'QUANTIEME CALENDAIRE INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG3 THEN
             COMPUTE GFBQT0 = GFQNT0
             IF GFBQT0 < 1 OR GFBQT0 > 73049 THEN
               MOVE 'QUANTIEME STANDARD INVALIDE' TO GFMESS
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG4 THEN
             COMPUTE GFBQTA = GFBJ
             COMPUTE ENDVAL--28 = GFBM - 1
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > ENDVAL--28
               COMPUTE GFBQTA = GFBQTA + GFTABJ(I)
             END-PERFORM
           END-IF
           IF FLAG2 OR FLAG4 THEN
             COMPUTE GFX0 = GFBA + (GFBS * 100) - 1900
             COMPUTE GFX1 = GFX0 * 365
             COMPUTE GFX2 = (GFX0 - 1) / 4
             COMPUTE GFBQT0 = GFX1 + GFX2 + GFBQTA
           END-IF
           IF FLAG3 THEN
             COMPUTE GFX1 = (GFBQT0 * 4) / 1461
             COMPUTE GFBS = (GFX1 / 100) + 19
             COMPUTE GFBA = GFX1 - ((GFBS - 19) * 100)
             COMPUTE GFX0 = (((GFX1 * 1461) + 3) / 4) - 1
             COMPUTE GFBQTA = GFBQT0 - GFX0
             PERFORM BISSEXTILE THRU E--BISSEXTILE
           END-IF
           IF FLAG2 OR FLAG3 THEN
             COMPUTE GFX1 = GFBQTA
             PERFORM VARYING GFBM FROM 1 BY 1 UNTIL NOT (GFX1 > 0)
               COMPUTE GFBJ = GFX1
               COMPUTE GFX1 = GFX1 - GFTABJ(GFBM)
             END-PERFORM
             COMPUTE GFBM = GFBM - 1
           END-IF
           COMPUTE GFSMN = FUNCTION MOD (GFBQT0 + 6 7) + 1
           MOVE B'1' TO GFVDAT
           COMPUTE GFJJ = GFBJ
           COMPUTE GFMM = GFBM
           COMPUTE GFAA = GFBA
           COMPUTE GFSS = GFBS
           COMPUTE GFQNTA = GFBQTA
           COMPUTE GFQNT0 = GFBQT0
           STRING GFANNEE GFMOIS GFJOUR DELIMITED BY SIZE INTO GFAMJ
           STRING GFSIECLE GFANNEE GFMOIS GFJOUR DELIMITED BY SIZE INTO
                                                                  GFSAMJ
           STRING GFJOUR '/' GFMOIS '/' GFANNEE DELIMITED BY SIZE INTO
                                                                GFJMA_ED
           STRING GFJOUR '/' GFMOIS '/' GFSIECLE GFANNEE DELIMITED BY
                                                     SIZE INTO GFJMSA_ED
           MOVE GFTABLML(GFBM) TO GFLMOISL
           MOVE GFTABLMC(GFBM) TO GFLMOISC
           MOVE GFTABLJ(GFBJ) TO GFLJOUR
           GO TO E--METDATE
           CONTINUE.
       GFERDAT.
           MOVE B'0' TO GFVDAT
           GO TO E--METDATE
           CONTINUE.
       E--METDATE.
           EXIT.
       BISSEXTILE.
           IF GFBA NOT = 0 OR FUNCTION MOD (GFBS 10) = 0 THEN
             IF FUNCTION MOD (GFBA 4) = 0 THEN
               MOVE B'1' TO GFBISS
             ELSE
               MOVE B'0' TO GFBISS
             END-IF
           END-IF
           IF GFBISS THEN
             MOVE 29 TO GFTABJ(2)
           ELSE
             MOVE 28 TO GFTABJ(2)
           END-IF
           CONTINUE.
       E--BISSEXTILE.
           EXIT.
       LINK_SYRD.
           EXEC CICS                    LINK PROGRAM  (       'SYRD')
                          NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           END-IF
           CONTINUE.
       E--LINK_SYRD.
           EXIT.
       ABANDON_TACHE.
           PERFORM ABANDON_ABEND THRU E--ABANDON_ABEND
           CONTINUE.
       E--ABANDON_TACHE.
           EXIT.
       ABANDON_ABEND.
           PERFORM ABANDON_CAID THRU E--ABANDON_CAID
           CONTINUE.
       E--ABANDON_ABEND.
           EXIT.
       ABANDON_CAID.
           PERFORM ABANDON_AIDA THRU E--ABANDON_AIDA
           CONTINUE.
       E--ABANDON_CAID.
           EXIT.
       ABANDON_AIDA.
           MOVE 'ABANDON-TACHE' TO MESS1
           GO TO ABANDON_ABANDON
           PERFORM ABANDON_CICS THRU E--ABANDON_CICS
           CONTINUE.
       E--ABANDON_AIDA.
           EXIT.
       ABANDON_CICS.
           INITIALIZE COMMET00_ABAN
           MOVE 'ABANDON-CICS' TO MESS1
           MOVE '1' TO ORI_TYP_ERR
           MOVE Z_TIMER_DATJOU TO EIB_DATE
           MOVE Z_TIMER_TIMJOU TO EIB_TIME
           MOVE EIBTRMID TO EIB_TRMID
           MOVE EIBTRNID TO EIB_TRNID
           MOVE NOM_PROG TO ORI_NOM_PGM
           MOVE NETNAME TO ORI_NOM_LIG
           MOVE Z_WHEN_COMPILED TO ORI_DAT_CPL
           MOVE EIBFN TO EIB_FN
           MOVE EIBRCODE TO EIB_RCODE
           MOVE EIBDS TO EIB_DS
           EXEC CICS                     LINK PROGRAM  (       'TET00')
                           COMMAREA (       COMMET00_ABAN)
                           LENGTH   (       COMMET00_LONG)
                           NOHANDLE
           END-EXEC
           MOVE EIBRCODE TO EIB_RCODE
           IF EIB_CODE NOT = EIB_NORMAL THEN
             GO TO ABEND_AIDA
           END-IF
           GO TO ABANDON_ABANDON
           PERFORM SEND_MESSAGE_ABANDON THRU E--SEND_MESSAGE_ABANDON
           CONTINUE.
       E--ABANDON_CICS.
           EXIT.
       SEND_MESSAGE_ABANDON.
           CONTINUE.
       ABANDON_ABANDON.
           EVALUATE TYPE_TERMINAL WHEN TERMINAL_ECRAN_91
             WHEN TERMINAL_ECRAN_99
               EXEC CICS                                         SEND
                                FROM    (       TRACE_MESSAGE)
                                LENGTH  (       TRACE_MESSAGE_LONG)
                                CTLCHAR (       WCC_SCREEN)
                                NOHANDLE
               END-EXEC WHEN TERMINAL_IMPRIMANTE_93 WHEN
               TERMINAL_IMPRIMANTE_94 WHEN TERMINAL_IMPRIMANTE_9B WHEN
                                                  TERMINAL_IMPRIMANTE_9C
             WHEN TERMINAL_IMPRIMANTE_B6
               EXEC CICS                                         SEND
                                FROM    (       TRACE_MESSAGE)
                                LENGTH  (       TRACE_MESSAGE_LONG)
                                CTLCHAR (       WCC_PRINTER)
                                NOHANDLE
               END-EXEC
             WHEN OTHER
               CONTINUE
           END-EVALUATE
           IF DEBUGGIN = 'OUI' THEN
             MOVE 1 TO BINAIRE
             EXEC CICS                                         RECEIVE
                                          INTO     (       Z_MAP)
                                          LENGTH   (       BINAIRE)
                                          NOHANDLE
             END-EXEC
           END-IF
           CONTINUE.
       ABEND_AIDA.
           MOVE LOW-VALUE TO LINK_TCTUA(1:255)
           EXEC CICS                         ABEND
                          CANCEL
                          NOHANDLE
           END-EXEC
           CONTINUE.
       E--SEND_MESSAGE_ABANDON.
           EXIT.
       END--MAIN.
           EXIT.
       END PROGRAM TIG66.
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
      *
