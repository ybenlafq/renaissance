      * @(#) MetaWare Technologies Cataloguer 0.9.18
      * Translated: 19/10/2016 18:50
      * Portfolio: IMAGE01
      * CARACTERE NEW LIGNE */
      * OCCUPATION BUFFER   */
      * DEPLACEMENT BUFFER  */
      * NBR LIGNES ECRITES  */
      * LIGNE EDITION       */
      * OCCUPATION BUFFER   */
      * DEPLACEMENT BUFFER  */
      * NBR LIGNES ECRITES  */
       IDENTIFICATION DIVISION.
       PROGRAM-ID. TIG6B.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01  S--PROGRAM                PIC X(8) VALUE 'TIG6B'.
       01  ENDVAL--33                PIC S9(5).
       01  ENDVAL--34                PIC S9(5).
       01  MW-REPEAT-CHAR--132       PIC X(132).
      * PRINTER EOM            */
      * *
       01  DFHBMPEM                  PIC X(1) VALUE X'19'.
      * PRINTER NL             */
      * *
       01  DFHBMPNL                  PIC X(1) VALUE X'15'.
      * PRINTER FF             */
      * *
       01  DFHBMPFF                  PIC X(1) VALUE X'0C'.
      * PRINTER CR             */
      * *
       01  DFHBMPCR                  PIC X(1) VALUE X'0D'.
      * AUTO SKIP              */
       01  DFHBMASK                  PIC X(1) VALUE '0'.
      * UNPROTECTED            */
      * *
       01  DFHBMUNP                  PIC X(1) VALUE X'40'.
      * UNPROT + NUM           */
      * *
       01  DFHBMUNN                  PIC X(1) VALUE X'50'.
      * PROTECTED              */
       01  DFHBMPRO                  PIC X(1) VALUE '-'.
      * BRIGHT                 */
       01  DFHBMBRY                  PIC X(1) VALUE 'H'.
      * DARK                   */
       01  DFHBMDAR                  PIC X(1) VALUE '<'.
      * MDT SET                */
       01  DFHBMFSE                  PIC X(1) VALUE 'A'.
      * PROT + MDT SET         */
       01  DFHBMPRF                  PIC X(1) VALUE '/'.
      * ASKP+MDT               */
       01  DFHBMASF                  PIC X(1) VALUE '1'.
      * AUTO + BRIGHT          */
       01  DFHBMASB                  PIC X(1) VALUE '8'.
      * SHIFT OUT              */
      * *
       01  DFHBMPSO                  PIC X(1) VALUE X'0E'.
      * SHIFT IN               */
      * *
       01  DFHBMPSI                  PIC X(1) VALUE X'0F'.
      *  FIELD FLAG VALUE SET BY INPUT MAPPING
      *                                                               */
      * FIELD ERASED           */
      * *
       01  DFHBMEOF                  PIC X(1) VALUE X'80'.
      * CURSOR IN FIELD        */
      * *
       01  DFHBMCUR                  PIC X(1) VALUE X'02'.
      *  FIELD DATA VALUE SET BY INPUT MAPPING
      *                                                               */
      * FIELD DETECTED         */
       01  DFHBMDET                  PIC X(1) VALUE '�'.
      *  CODE FOR SA ORDER
      *                                                               */
      * SA ORDER (X'28')       */
      * *
       01  DFHSA                     PIC X(1) VALUE X'28'.
      *  CODE FOR ERROR CODE
      *                                                               */
      * ERROR CODE-X'3F'       */
      * *
       01  DFHERROR                  PIC X(1) VALUE X'3F'.
      *  EXTENDED ATTRIBUTE TYPE CODES
      *                                                               */
      * COLOR    (X'42')       */
       01  DFHCOLOR                  PIC X(1) VALUE '�'.
      * PS       (X'43')       */
       01  DFHPS                     PIC X(1) VALUE '�'.
      * HIGHLIGHT(X'41')       */
       01  DFHHLT                    PIC X(1) VALUE '�'.
      * 3270     (X'C0')       */
       01  DFH3270                   PIC X(1) VALUE '�'.
      * VALIDT'N (X'C1')       */
       01  DFHVAL                    PIC X(1) VALUE 'A'.
      * OUTLINE                */
       01  DFHOUTLN                  PIC X(1) VALUE 'B'.
      * BACKGROUND             */
       01  DFHBKTRN                  PIC X(1) VALUE '�'.
      * TRANSP   (X'46')       */
      * ALL , RESET TO         */
      * *
       01  DFHALL                    PIC X(1) VALUE X'00'.
      * DEFAULT  (X'00')       */
      *  DEFAULT ATTRIBUTE CODE  - TO SET ATTRIBUTES IN MAPS
      *                                                               */
      * DEFAULT  (X'FF')       */
       01  DFHDFT                    PIC X(1) VALUE '�'.
      * �P2C*/
      *  COLOR ATTRIBUTE VALUES
      *                                                               */
      * DEFAULT                */
      * *
      * MANDATORY ENTER+       */
      * TRIGGER                */
       01  DFHDFCOL                  PIC X(1) VALUE X'00'.
      * BLUE                   */
       01  DFHBLUE                   PIC X(1) VALUE '1'.
      * RED                    */
       01  DFHRED                    PIC X(1) VALUE '2'.
      * PINK                   */
       01  DFHPINK                   PIC X(1) VALUE '3'.
      * GREEN                  */
       01  DFHGREEN                  PIC X(1) VALUE '4'.
      * TURQUOISE              */
       01  DFHTURQ                   PIC X(1) VALUE '5'.
      * YELLOW                 */
       01  DFHYELLO                  PIC X(1) VALUE '6'.
      * NEUTRAL                */
       01  DFHNEUTR                  PIC X(1) VALUE '7'.
      *  BASE PS  ATTRIBUTE VALUE
      *                                                               */
      * BASE PS                */
      * *
       01  DFHBASE                   PIC X(1) VALUE X'00'.
      *  HIGHLIGHT ATTRIBUTE VALUES
      *                                                               */
      * NORMAL                 */
      * *
       01  DFHDFHI                   PIC X(1) VALUE X'00'.
      * BLINK                  */
       01  DFHBLINK                  PIC X(1) VALUE '1'.
      * REVERSE VIDEO          */
       01  DFHREVRS                  PIC X(1) VALUE '2'.
      * UNDERSCORE             */
       01  DFHUNDLN                  PIC X(1) VALUE '4'.
      *  VALIDATION ATTRIBUTE VALUES
      *                                                               */
      * MANDATORY FILL         */
      * *
       01  DFHMFIL                   PIC X(1) VALUE X'04'.
      * MANDATORY ENTER        */
      * *
       01  DFHMENT                   PIC X(1) VALUE X'02'.
      * MANDATORY FILL+        */
      * *
       01  DFHMFE                    PIC X(1) VALUE X'06'.
      *  MANDATORY ENTER       */
      *  ADDITIONAL ATTRIBUTES
      *                                                               */
      * UNPROTECTED            */
       01  DFHUNNOD                  PIC X(1) VALUE '('.
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNIMD                  PIC X(1) VALUE 'I'.
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNNUM                  PIC X(1) VALUE 'J'.
      * NUMERIC                */
      * MDT                    */
      * UNPROTECTED        �01A*/
       01  DFHUNNUB                  PIC X(1) VALUE 'Q'.
      * NUMERIC            �01A*/
      * INTENSIFY          �01A*/
      * LIGHT PEN DET.     �01A*/
      *                    �01A*/
      * UNPROTECTED            */
       01  DFHUNINT                  PIC X(1) VALUE 'R'.
      * NUMERIC                */
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNNON                  PIC X(1) VALUE ')'.
      * NUMERIC                */
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * MDT                    */
      * PROTECTED              */
       01  DFHPROTI                  PIC X(1) VALUE 'Y'.
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * PROTECTED              */
       01  DFHPROTN                  PIC X(1) VALUE '%'.
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * TRIGGER                */
      * *
       01  DFHMT                     PIC X(1) VALUE X'01'.
      * MANDATORY FILL+        */
      * *
       01  DFHMFT                    PIC X(1) VALUE X'05'.
      * TRIGGER                */
      * MANDATORY ENTER+       */
      * *
       01  DFHMET                    PIC X(1) VALUE X'03'.
      * TRIGGER                */
      * MANDATORY FILL+        */
      * *
       01  DFHMFET                   PIC X(1) VALUE X'07'.
      *  FIELD OUTLINING ATTRIBUTE CODES       KJ0001
      *                                                               */
      * DEFAULT OUTLINE        */
      * *
       01  DFHDFFR                   PIC X(1) VALUE X'00'.
      * LEFT                   */
      * *
       01  DFHLEFT                   PIC X(1) VALUE X'08'.
      * OVERLINE               */
      * *
       01  DFHOVER                   PIC X(1) VALUE X'04'.
      * RIGHT                  */
      * *
       01  DFHRIGHT                  PIC X(1) VALUE X'02'.
      * UNDER                  */
      * *
       01  DFHUNDER                  PIC X(1) VALUE X'01'.
      * LEFT+OVER+RIGHT+       */
      * *
       01  DFHBOX                    PIC X(1) VALUE X'0F'.
      * UNDER LINES            */
      *  SOSI ATTRIBUTE CODES
      *                                                               */
      * SOSI = YES             */
      * *
       01  DFHSOSI                   PIC X(1) VALUE X'01'.
      *  BACKGROUND TRANSPARENCY ATTRIBUTE CODES
      *                                                               */
      * TRANSP = YES           */
       01  DFHTRANS                  PIC X(1) VALUE '0'.
      * TRANSP = NO            */
       01  DFHOPAQ                   PIC X(1) VALUE '�'.
       LOCAL-STORAGE SECTION.
       01  COM_IG60_LONG_COMMAREA    PIC S9(4) COMP-5 VALUE +500.
       01  Z_COMMAREA                PIC X(500).
       01  CAR_EOL                   PIC X(1).
       01  CAR_EOM                   PIC X(1).
       01  CAR_WCC                   PIC X(1).
       01  NBR_ECR                   PIC S9(4) COMP-5.
       01  I                         PIC S9(4) COMP-5.
       01  I_EOL                     PIC S9(4) COMP-5.
       01  CTR_CAR_EOL               PIC S9(4) COMP-5.
       01  PTR_BUF                   POINTER.
       01  BIN_IMP                   PIC S9(9) COMP-5.
       01  BUF_STG                   PIC S9(9) COMP-5.
       01  LIGNE_DETAIL              PIC X(132).
       01  TIG6B_ERR                 PIC X(4).
       01  MESSERR                   PIC X(79).
       LINKAGE SECTION.
       01  PTR_PREF                  POINTER.
       01  Z_COMMAREA_PREF           PIC X(500).
       01  Z_COMMAREA_IG60.
      * *********** RESERVE AIDA
      *                      ******************************************/
         05 PREFIX_AIDA              PIC X(100).
      * *********** APPARTENANT A TRANSACTION IG60
      *                                        ************************/
      * APPLID CICS            */
         05 COM_IG60_CICS            PIC X(8).
      * LIGNE VTAM             */
         05 COM_IG60_LIG             PIC X(8).
      * CONCERNANT SIGNATURE UTILISATEUR ---------------------------- 32
      *                                                               */
      * SIGNATURE              */
         05 COM_IG60_SIG.
      *       GROUPE           */
           10 COM_IG60_GRP           PIC X(8).
      *       SOUS-GROUPE      */
           10 COM_IG60_SGP           PIC X(8).
      *       UTILISATEUR      */
           10 COM_IG60_UTI           PIC X(8).
      * NOM ETAT               */
         05 COM_IG60_ETA             PIC X(6).
      * CHOIX DU TRAITEMENT    */
         05 COM_IG60_TRA             PIC X(1).
      *       I = IMPRESSION   */
      *       R = REPRISE      */
      *       A = ANNULATION   */
      *       C = CONSULTATION */
      * TYPE DE CONSULTATION   */
         05 COM_TYP_SEL              PIC X(1).
      *       * = INTEGRALE    */
      *       I = IMPRESSION   */
      *       R = REPRISE      */
      *       C = CONSULTATION */
      * PROVENANT SEGMENT DIGSA IDENTIFICATION ETAT ----------------- 76
      *                                                               */
      * TITRE ETAT             */
         05 COM_DIGSA_TIT            PIC X(40).
      * INFO TYPE PAPIER       */
         05 COM_DIGSA_PAP            PIC X(20).
      * NUMERO CONFIGURATION   */
         05 COM_DIGSA_CFG            PIC X(1).
      * TYPE IMPRESSION        */
         05 COM_DIGSA_IMP            PIC X(1).
      *       U = UNIQUE       */
      *       M = MULTIPLE     */
      * ATTACHEMENT IMPRIMANTE */
         05 COM_DIGSA_ATT            PIC X(1).
      *       S = STANDARD     */
      *       P = PARTICULIER  */
      * PROTOCOLE D'IMPRESSION */
         05 COM_DIGSA_PTC            PIC X(1).
      *       T = TEXT         */
      *       L = LASER        */
      *       G = GRAPHIQUE    */
      * ETAT TYPE TEXTE */
      * HAUTEUR ENTRE PLIURES  */
         05 COM_DIGSA_HTR            PIC S9(4) COMP-5.
      * TOTAL LIGNES EDITABLES */
         05 COM_DIGSA_LNG            PIC S9(4) COMP-5.
      * NOMBRE COLONNES        */
         05 COM_DIGSA_COL            PIC S9(4) COMP-5.
      * ETAT TYPE LASER */
      * LNG ZONE COMMANDES     */
         05 COM_DIGSA_CMD            PIC S9(4) COMP-5.
      * LNG ZONE DONNEES       */
         05 COM_DIGSA_DON            PIC S9(4) COMP-5.
      * FORMAT                 */
         05 COM_DIGSA_FOR            PIC X(2).
      * PROVENANT SEGMENT DIGSD AUTORISATIONS-SIGNATURES ------------ 35
      *                                                               */
      * GROUPE                 */
         05 COM_DIGSD_GRP            PIC X(8).
      * SOUS-GROUPE            */
         05 COM_DIGSD_SGP            PIC X(8).
      * UTILISATEUR            */
         05 COM_DIGSD_UTI            PIC X(8).
      * DESTINATION            */
         05 COM_DIGSD_DST            PIC X(9).
      * AUTORISATION REPRISE   */
         05 COM_DIGSD_REP            PIC X(1).
      * AUTORISATION ANNULAT   */
         05 COM_DIGSD_SUP            PIC X(1).
      * GESTION DES IMPRIMANTES ------------------------------------- 12
      *                                                               */
      * IMPRIMANTE 1 ATTACHEE  */
         05 COM_IG60_PR1             PIC X(4).
      * IMPRIMANTE 2 ATTACHEE  */
         05 COM_IG60_PR2             PIC X(4).
      * IMPRIMANTE TRAITEMENT  */
         05 COM_STRT_PRT             PIC X(4).
      * CONFIGURATION DE TRAVAIL ------------------------------------ 57
      *                                                               */
      * NOM ETAT               */
         05 COM_CFG_ETA              PIC X(6).
      * DATE EDITION DEBUT     */
         05 COM_CFG_DTD              PIC X(6).
      * DATE EDITION FIN       */
         05 COM_CFG_DTF              PIC X(6).
      * DESTINATION            */
         05 COM_CFG_DST              PIC X(9).
      * DOCUMENT DEBUT         */
         05 COM_CFG_DCD              PIC X(15).
      * DOCUMENT FIN           */
         05 COM_CFG_DCF              PIC X(15).
      * TS ASSOCIES AU TRAITEMENT REPRISE OU IMPRESSION  ------------ 12
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOM               PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNG               PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAX               PIC S9(4) COMP-5.
      * TS ASSOCIES AU TRAITEMENT CONSULTATION CONFIGURATIONS ------- 25
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOMC              PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNGC              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXC              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIC              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGC              PIC S9(4) COMP-5.
      * NBR CONSULTATION       */
         05 COM_NB_CONC              PIC S9(5) COMP-3.
      * NBR IMPRESSION         */
         05 COM_NB_IMPC              PIC S9(5) COMP-3.
      * NBR REPRISE            */
         05 COM_NB_REPC              PIC S9(5) COMP-3.
      * TS ASSOCIES AU TRAITEMENT AFFICHAGE LIGNE ETAT -------------- 16
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOML              PIC X(8).
         05 Z_COMMAREA_IG60          PIC X(1).
      * LONGUEUR TS            */
         05 COM_TS_LNGL              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXL              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIL              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGL              PIC S9(4) COMP-5.
      * TS ASSOCIES AU TRAITEMENT ANNULATION ------------------------ 31
      *                                                               */
      * NOM TS                 */
         05 COM_TS_NOMA              PIC X(8).
      * LONGUEUR TS            */
         05 COM_TS_LNGA              PIC S9(4) COMP-5.
      * ITEM MAXIMUM TS        */
         05 COM_TS_MAXA              PIC S9(4) COMP-5.
      * UTILISATION PAGINATION */
         05 COM_TS_UTIA              PIC S9(4) COMP-5.
      * PAGE EN COURS          */
         05 COM_TS_PAGA              PIC S9(4) COMP-5.
      * NBR SUPPRESSION PHYSIQ */
         05 COM_NB_SUPA              PIC S9(5) COMP-3.
      * NBR ANNULATION         */
         05 COM_NB_ANUA              PIC S9(5) COMP-3.
      * NBR DESANNULATION      */
         05 COM_NB_DESA              PIC S9(5) COMP-3.
      * SYSTEME SECURITE ATTACHE --------------------------------------3
      *                                                               */
         05 COM_SECURI               PIC X(3).
       01  BUF_IMP                   PIC X(1920) BASED.
       01  PTR_IMP                   POINTER.
       01  LIG_132                   PIC X(132).
       01  LIG_CAR                   PIC X(1).
       PROCEDURE DIVISION USING PTR_PREF.
       BEGIN--MAIN SECTION.
           SET ADDRESS OF Z_COMMAREA_PREF TO PTR_PREF
           SET ADDRESS OF Z_COMMAREA_IG60 TO ADDRESS OF Z_COMMAREA
           SET ADDRESS OF BUF_IMP TO PTR_BUF
           SET ADDRESS OF PTR_IMP TO ADDRESS OF BIN_IMP
           SET ADDRESS OF LIG_CAR TO PTR_IMP
           SET ADDRESS OF LIG_132 TO PTR_IMP
           MOVE ALL '*' TO MW-REPEAT-CHAR--132
           MOVE MW-REPEAT-CHAR--132 TO LIGNE_DETAIL
           CONTINUE.
       BEGIN--PROGRAM SECTION.
           EXEC CICS                   HANDLE CONDITION ENDDATA   (
                                                          TIG6B_ENDDATA)
                                   ENVDEFERR (       TIG6B_ENVDEFERR)
                                   LENGERR   (       TIG6B_LENGERR)
                                   INVREQ    (       TIG6B_INVREQ)
                                   NOTFND    (       TIG6B_NOTFND)
                                   INVTSREQ  (       TIG6B_INVTSREQ)
                                   IOERR     (       TIG6B_IOERR)
                                                                END-EXEC
           EXEC CICS                   RETRIEVE INTO   (
                                                             Z_COMMAREA)
                           LENGTH (       COM_IG60_LONG_COMMAREA)
                                                                END-EXEC
           ALLOCATE BUF_IMP INITIALIZED RETURNING PTR_BUF
           SET PTR_IMP TO PTR_BUF
           SET ADDRESS OF LIG_CAR TO PTR_IMP
           SET ADDRESS OF LIG_132 TO PTR_IMP
           MOVE 0 TO BUF_STG
           MOVE 0 TO NBR_ECR
           MOVE DFHBMPNL TO CAR_EOL
           MOVE DFHBMPEM TO CAR_EOM
           MOVE 'H' TO CAR_WCC
           MOVE '0000' TO TIG6B_ERR
           COMPUTE ENDVAL--33 = COM_DIGSA_LNG
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > ENDVAL--33
             IF I = 1 THEN
               MOVE 0 TO CTR_CAR_EOL
             ELSE
               MOVE 1 TO CTR_CAR_EOL
             END-IF
             IF COM_DIGSA_COL = 132 THEN
               MOVE 0 TO CTR_CAR_EOL
             END-IF
             IF BUF_STG > 1920 - (132 + COM_DIGSA_LNG + 1) THEN
               PERFORM TIG6B_SND_TRM THRU E--TIG6B_SND_TRM
             END-IF
             PERFORM TIG6B_ECR_EOL THRU E--TIG6B_ECR_EOL
             PERFORM TIG6B_ECR_LIG THRU E--TIG6B_ECR_LIG
           END-PERFORM
           COMPUTE CTR_CAR_EOL = (COM_DIGSA_HTR - NBR_ECR)
           PERFORM TIG6B_ECR_EOL THRU E--TIG6B_ECR_EOL
           PERFORM TIG6B_SND_TRM THRU E--TIG6B_SND_TRM
           EXEC CICS               RETURN END-EXEC
           CONTINUE.
       TIG6B_ENDDATA.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE ENDDATA' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_ENVDEFERR.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE ENVDEFERR' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_LENGERR.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE LENGERR' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_NOTFND.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE NOTFND' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_INVREQ.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE INVRED' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_INVTSREQ.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE INVTSREQ' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
       TIG6B_IOERR.
           MOVE 'TIG6B * RETRIEVE * ERREUR HANDLE IOERR' TO MESSERR
           PERFORM SEND_TXT_ERR THRU E--SEND_TXT_ERR
           CONTINUE.
           COPY CBGOBACK.
       TIG6B_ECR_EOL.
           IF CTR_CAR_EOL <= 0 THEN
             GO TO E--TIG6B_ECR_EOL
           END-IF
           COMPUTE ENDVAL--34 = CTR_CAR_EOL
           PERFORM VARYING I_EOL FROM 1 BY 1 UNTIL I_EOL > ENDVAL--34
             MOVE CAR_EOL TO LIG_CAR
             COMPUTE BUF_STG = BUF_STG + 1
             COMPUTE BIN_IMP = BIN_IMP + 1
             COMPUTE NBR_ECR = NBR_ECR + 1
           END-PERFORM
           CONTINUE.
       E--TIG6B_ECR_EOL.
           EXIT.
       TIG6B_ECR_LIG.
           MOVE LIGNE_DETAIL TO LIG_132
           COMPUTE BUF_STG = BUF_STG + COM_DIGSA_COL
           COMPUTE BIN_IMP = BIN_IMP + COM_DIGSA_COL
           IF CTR_CAR_EOL <= 0 THEN
             COMPUTE NBR_ECR = NBR_ECR + 1
           END-IF
           CONTINUE.
       E--TIG6B_ECR_LIG.
           EXIT.
       TIG6B_SND_TRM.
           MOVE CAR_EOM TO LIG_CAR
           COMPUTE BIN_IMP = BIN_IMP + 1
           COMPUTE BUF_STG = BUF_STG + 1
           EXEC CICS                   SEND FROM    (       BUF_IMP)
                       LENGTH  (       BUF_STG)
                       WAIT
                       CTLCHAR (       CAR_WCC)
                       ERASE END-EXEC
           SET PTR_IMP TO PTR_BUF
           SET ADDRESS OF LIG_CAR TO PTR_IMP
           SET ADDRESS OF LIG_132 TO PTR_IMP
           INITIALIZE BUF_IMP
           MOVE 0 TO BUF_STG
           MOVE 0 TO CTR_CAR_EOL
           CONTINUE.
       E--TIG6B_SND_TRM.
           EXIT.
       SEND_TXT_ERR.
           EXEC CICS                   SEND TEXT FROM (       MESSERR)
                            LENGTH (       79)
                            ERASE
                            PRINT
                            ALARM
                            NOHANDLE END-EXEC
           EXEC CICS                   ABEND CANCEL NOHANDLE END-EXEC
           CONTINUE.
       E--SEND_TXT_ERR.
           EXIT.
       END--MAIN.
           EXIT.
       END PROGRAM TIG6B.
      *
      *
      *
      *
