      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                BCM801.                                       
       AUTHOR.                    DSA024.                                       
      * ************************************************************** +        
      *    + ------------------------------------------------------ +  *        
      *    !                 CALENDRIER MAGASINS                    !  *        
      *    + ------------------------------------------------------ +  *        
      * ************************************************************** +        
      *                                                                *        
      *  PROJET     : CALENDRIER MAGASINS                              *        
      *  PROGRAMME  : BCM801                                           *        
      *  CREATION   : 25/04/07                                         *        
      *  FONCTION   : ce programme modifie l�g�rement le codage xml    *        
      *               donn� par les chaines icommerce et qu'il n'est   *        
      *               pas possible d'int�grer chez ibm... ...          *        
      *  PERIODICITE: HEBDO                                            *        
      * -------------------------------------------------------------- *        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FCM801I  ASSIGN TO FCM801I.                                  
      *--                                                                       
            SELECT FCM801I  ASSIGN TO FCM801I                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FCM801O  ASSIGN TO FCM801O.                                  
      *--                                                                       
            SELECT FCM801O  ASSIGN TO FCM801O                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FCM801I                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER                       PIC X(27956).                        
       FD  FCM801O                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FCM801O-ENR                  PIC X(27956).                           
       WORKING-STORAGE SECTION.                                                 
       01 WETAT-FICHIER             PIC X(01)    VALUE '0'.                     
         88 FICHIER-OK                           VALUE '0'.                     
         88 FIN-FICHIER                          VALUE '1'.                     
       01 WZONE1                    PIC X(27956) VALUE SPACES.                  
       01 WZONE2                    PIC X(27956) VALUE SPACES.                  
       01 I                         PIC S9(5)    PACKED-DECIMAL.                
       01 J                         PIC S9(5)    PACKED-DECIMAL.                
       01 W-LECTURE                 PIC S9(08)   VALUE 0.                       
       01 WZONE-SEARCH1.                                                        
       05 FILLER        PIC X(20) VALUE "<?xml version='1.0' ".                 
       05 FILLER        PIC X(20) VALUE "encoding='iso-8859-1".                 
       05 FILLER        PIC X(20) VALUE "'?><!DOCTYPE ouvertu".                 
       05 FILLER        PIC X(20) VALUE "res SYSTEM 'ouvertur".                 
       05 FILLER        PIC X(20) VALUE "es_10.dtd'><ouvertur".                 
       05 FILLER        PIC X(03) VALUE "es>".                                  
       01 WZONE-ENTETE  PIC X(103) VALUE SPACES.                                
       01 Z-INOUT                    PIC X(236) VALUE SPACES.                   
           COPY SYBWERRO.                                                       
           COPY ABENDCOP.                                                       
       PROCEDURE DIVISION.                                                      
           OPEN INPUT  FCM801I.                                                 
           OPEN OUTPUT FCM801O.                                                 
           MOVE 0 TO I J.                                                       
           move WZONE-SEARCH1 to wzone-entete.                                  
           PERFORM LECTURE-FICHIER                                              
           INSPECT WZONE1 TALLYING J FOR CHARACTERS BEFORE                      
                   "</ouvertures>"                                              
           MOVE WZONE1 (1:J) TO WZONE2                                          
           PERFORM ECRITURE-FICHIER                                             
           if not fin-fichier PERFORM LECTURE-FICHIER end-if                    
           PERFORM UNTIL FIN-FICHIER                                            
                   MOVE 0 TO I J                                                
                   INSPECT WZONE1 TALLYING I FOR CHARACTERS after               
                           wzone-entete                                         
                   compute i = 27957 - i                                        
                   INSPECT WZONE1 TALLYING J FOR CHARACTERS BEFORE              
                            "</ouvertures>"                                     
                   compute j = j - i + 1                                        
                   move spaces to wzone2                                        
                   MOVE WZONE1 (I:J) TO WZONE2                                  
                   PERFORM ECRITURE-FICHIER                                     
                   PERFORM LECTURE-FICHIER                                      
           END-PERFORM.                                                         
           move 0 to i j                                                        
           INSPECT WZONE1 TALLYING J FOR CHARACTERS BEFORE                      
                   "</ouvertures>"                                              
           INSPECT WZONE1 TALLYING I FOR CHARACTERS after                       
                   "</ouvertures>"                                              
           compute i = 27957 - i                                                
           compute j = j + 1                                                    
           compute i = i - j                                                    
           MOVE WZONE1 (j:I) TO WZONE2.                                         
           PERFORM ECRITURE-FICHIER.                                            
           CLOSE FCM801I FCM801O.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       LECTURE-FICHIER SECTION.                                                 
           READ FCM801I INTO WZONE1                                             
           AT END                                                               
                SET FIN-FICHIER TO TRUE                                         
           NOT AT END                                                           
                ADD 1 TO W-LECTURE                                              
           END-READ.                                                            
       F-LECTURE-FICHIER. EXIT.                                                 
       ECRITURE-FICHIER SECTION.                                                
           WRITE FCM801O-ENR FROM WZONE2.                                       
       F-ECRITURE-FICHIER. EXIT.                                                
