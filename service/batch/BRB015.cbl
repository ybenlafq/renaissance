      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BRB015.                                           00000040
       AUTHOR.        DSA045-EY.                                        00000050
       DATE-WRITTEN.  19/04/2006.                                       00000060
      *----------------------------------------------------------------*00000080
      * RECEPTION DES MOUVEMENTS QUOTIDIENS DE COMPLETEL               *00000090
      * METTRE LE FICHIER .CSV AVEC LES SEPARATEURS ';' DANS UN        *00000090
      * FICHIER PLAT                                                   *00000090
      *----------------------------------------------------------------*00000080
      * DATE   : AOUT 2006                                             *        
      * MODIFICATION :                                                 *        
      * AUTEUR : JEREMY CELAIRE  JC0806                                *        
      * COMMENTAIRE :                                                  *        
      *  - PASSAGE DE 15 A 16 CARAC. SUR LE LOGIN ET MOT DE PASSE      *        
      *    (FRB015).                                                   *        
      *----------------------------------------------------------------*00000080
      * DATE   : sept 2006                                             *        
      * MODIFICATION :                                                 *        
      * AUTEUR : JEREMY CELAIRE  JC0906                                *        
      * COMMENTAIRE :                                                  *        
      *  - LECTURE FRB014 EN TOTALITE (MODIF EY).                      *        
      *  - FICHIER FRB014 VARIABLE.                                    *        
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB014     ASSIGN TO FRB014                           00000250
      *                      FILE STATUS ST-FRB014.                             
      *--                                                                       
           SELECT FRB014     ASSIGN TO FRB014                                   
                             FILE STATUS ST-FRB014                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB015     ASSIGN TO FRB015                           00000250
      *                      FILE STATUS ST-FRB015.                             
      *--                                                                       
           SELECT FRB015     ASSIGN TO FRB015                                   
                             FILE STATUS ST-FRB015                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *    FICHIER EN ENTR�E .CSV                                       00000430
       FD  FRB014                                                       00000440
JC0906*    RECORDING F                                                  00000450
JC0906     RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
JC0906     RECORD CONTAINS 1 TO 500 CHARACTERS                          00000470
JC0906     DATA RECORD IS FRB001-ENR.                                   00000470
JC0906 01  FRB014-ENR.                                                  00000480
JC0906     10  FRB014-ENREG           PICTURE X(01)                     00000480
JC0906                                OCCURS 1 TO 500 TIMES             00000480
JC0906                                DEPENDING ON I-LONG.                      
      *                                                                 00000600
JC0906*01  FRB014-ENR.                                                  00000480
JC0906*    05  FRB014-ENREGISTREMENT PIC X(500).                        00000480
      *                                                                 00000600
       FD  FRB015                                                       00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FRB015-ENR.                                                  00000480
           05  FRB015-FILLEUR         PIC X(320).                       00000480
      *                                                                 00000600
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       77  ST-FRB014                  PIC 9(02) VALUE ZERO.                     
       77  ST-FRB015                  PIC 9(02) VALUE ZERO.                     
           COPY  FRB015.                                                        
      *    COPIES D'ABEND ET DE CONTR�LE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      *    DESCRIPTION DES TABLES UTILIS�ES.                            00001910
      *    EXEC  SQL  INCLUDE  SQLCA     END-EXEC.                      00001940
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    R�CUP�RATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  WS-NBR-LUS                 PIC 9(06)  COMP-3   VALUE 0.              
       01  CPT-FRB015                 PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01  WS-DCOMMANDE               PIC X(10).                        00000480
       01  WS-DPRODUCTION             PIC X(10).                        00000480
       01  LG-FRB014                  PIC 9(05) VALUE ZERO.                     
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FRB014             PIC 9(01)           VALUE 0.              
           88 FIN-FRB014                                  VALUE 1.              
      *                                                                         
JC0906 01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
JC0906 01  I                          PIC S9(3)  COMP-3 VALUE 0.                
JC0906 01  FRB014-ENREGISTREMENT      PIC X(500).                               
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BRB015       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    LECTURE DU FICHIER EN ENTR�E.                                00001900
      *    C'EST LE FICHIER .CSV CE N'EST PAS UN POSITIONNEL            00001900
      *    LE BUT EST DE LE METTRE DANS UN POSITIONEL                   00001900
      *    LECTURE DU FICHIER.                                          00001900
      *    PREMIERE LECTURE C'EST POUR OMMETRE L'ENTETE1                00001900
EY    *    PERFORM  READ-FRB014 .                                               
      *    PREMIERE LECTURE C'EST POUR OMMETRE L'ENTETE2                00001900
EY    *    PERFORM  READ-FRB014 .                                               
      *    C'EST LA 3� EME LECTURE QUI EST LA BONNE                             
           PERFORM  READ-FRB014                                                 
           PERFORM UNTIL FIN-FRB014                                             
              PERFORM FORMATAGE-DONNEES                                         
              PERFORM ECRITURE-FRB015                                           
              PERFORM READ-FRB014                                               
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BRB015.                                                 
      *                                                                         
       FIN-MODULE-BRB015.  EXIT.                                                
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * REFORMATAGE DES DONN�ES.                                                
      *-----------------------------------------------------------------        
       FORMATAGE-DONNEES  SECTION.                                      00002160
      *                                                                 00001900
           INITIALIZE FRB015-ENR                                                
                      FRB015-RECORD                                             
                      FRB014-ENREGISTREMENT.                                    
      *                                                                 00001900
JC0906     PERFORM VARYING I FROM 1 BY 1 UNTIL I > 500                          
JC0906         MOVE FRB014-ENREG(I)                                             
JC0906                        TO  FRB014-ENREGISTREMENT(I:1)                    
JC0906     END-PERFORM                                                          
      *                                                                         
      *    AJOUTER LE DERNIER ';' � LA FIN DE CHAINE SI MANQUE                  
           PERFORM VARYING LG-FRB014 FROM 1 BY 1 UNTIL                          
              FRB014-ENREGISTREMENT (LG-FRB014:) NOT > SPACES                   
           OR LG-FRB014      >= LENGTH OF FRB014-ENREGISTREMENT                 
           END-PERFORM                                                          
           IF FRB014-ENREGISTREMENT (LG-FRB014 - 1:1) NOT = ';'                 
              MOVE ';' TO  FRB014-ENREGISTREMENT (LG-FRB014:1)                  
           END-IF .                                                             
      *    DISPLAY FRB014-ENREGISTREMENT                                        
           UNSTRING FRB014-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
                    FRB015-NFOURNISSEUR                                      000
                    FRB015-SSAAMMJJ                                          000
                    FRB015-HHMMSSCC                                          000
                    FRB015-TEQUIPEMENT                                       000
                    FRB015-CFAM                                              000
                    FRB015-LFAM                                              000
                    FRB015-CMARQ                                             000
                    FRB015-LMARQ                                             000
                    FRB015-LREFFOURN                                         000
                    FRB015-LEMBALLAGE                                        000
                    FRB015-NCODIC                                            000
                    FRB015-NEAN                                              000
                    FRB015-NSERIE                                            000
                    FRB015-ADRMAC                                            000
                    FRB015-NHARDWARE                                         000
                    FRB015-NFIRMWARE                                         000
                    FRB015-CLOGIN                                            000
                    FRB015-CPASSE                                            000
                    FRB015-LCONSTRUCTEUR                                     000
                    FRB015-WCMPL-ALP .                                       000
           IF FRB015-WCMPL-ALP     = 'OK'                                       
              MOVE '2' TO FRB015-WCOMPLETEL                                     
           ELSE                                                                 
              MOVE '3' TO FRB015-WCOMPLETEL                                     
           END-IF.                                                              
       FIN-FORMATAGE-DONNEES. EXIT.                                     00002620
                                                                        00002620
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! RECEPTION DES DONNEES COMPLETEL              !'  00002400
           DISPLAY  '! FORMATAGE DU FICHIER .CSV     .              !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 23/04/2006 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTR�E.                                      
           OPEN  INPUT  FRB014 .                                        00002950
      *    OUVERTURE DU FICHIER EN SORTIE.                                      
           OPEN  OUTPUT FRB015 .                                        00002950
      *    R�CUP�RATION DE LA DATE SYST�ME.                                     
           PERFORM LECTURE-DSYST .                                              
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * R�CUP�RATION DE LA DATE SYST�ME.                                        
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BRB015 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS        00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS ECRIT   : ' CPT-FRB015        00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FRB014 FRB015                                                 
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRB015. EXIT.                                            00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FRB014 FRB015.                                                
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB015'  TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FRB014  SECTION.                                                    
           READ FRB014  AT END                                                  
                SET FIN-FRB014     TO TRUE                                      
                        NOT AT END                                              
                ADD  1             TO WS-NBR-LUS                                
           END-READ.                                                            
       FIN-READ-FRB014. EXIT.                                           00009300
      *                                                                         
       ECRITURE-FRB015              SECTION.                                    
           WRITE  FRB015-ENR from FRB015-RECORD.                                
           ADD 1 TO CPT-FRB015 .                                                
       FIN-ECRITURE-FRB015   . EXIT.                                            
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
      *-----------------------------------------------------------------        
      * TEST DES CODES RETOUR SQL.                                              
      *-----------------------------------------------------------------        
      *TEST-CODE-RETOUR-SQL          SECTION.                                   
      *    MOVE '0' TO CODE-RETOUR.                                     00012220
      *    EVALUATE SQLCODE                                             00012230
      *             WHEN +0                                             00012240
      *              SET TROUVE TO TRUE                                 00012250
      *             WHEN +100                                           00012260
      *              SET NON-TROUVE TO TRUE                             00012270
      *             WHEN -803                                           00012280
      *              SET EXISTE-DEJA TO TRUE                            00012290
      *             WHEN OTHER                                          00012300
      *              MOVE SQLCODE        TO TRACE-SQL-CODE                      
      *              DISPLAY TRACE-SQL-MESSAGE                                  
      *              MOVE 'ERREUR DB2 '  TO ABEND-MESS                  00012310
      *              MOVE SQLCODE TO ABEND-CODE                         00012320
      *              PERFORM FIN-ANORMALE                               00012330
      *    END-EVALUATE.                                                00012340
      *FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      *                                                                         
       COPY SYBCERRO.                                                           
