      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020000
       TITLE 'IMPRESSION DES SORTIES ESPECES (INM181)'.                 00030001
       PROGRAM-ID. BNM181.                                              00040001
       AUTHOR. DSA003.                                                  00050001
      ******************************************************************00060000
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *00070000
      ******************************************************************00080000
      *                                                                *00090000
      *  PROJET     : PARTIE HOST DU NOUVEL ENCAISSEMENT MAGASIN       *00100000
      *  PROGRAMME  : BNM181                                           *00110001
      *  TITRE      : IMPRESSION DES SORTIES ESPECES POUR REMBOURSEMENT*00120001
      *               CHEZ DARTY                                       *00130001
      *  CREATION   : 04/06/2002                                       *00140001
      *  PERIODICITE: HEBDOMADAIRE                                     *00150001
      *  CHAINE     : PGM BNM180 (EXTRACTION DES DONNEES)              *00160001
      *             : TRI DU FICHIER FNM180, ISSU DE BNM180, SUR       *00170001
      *               SOCIETE, DATE TRANSACTION                        *00180001
      *                  ==> FICHIER TRI� : FNM181                     *00190001
      *             : PGM BNM181 (IMPRESSION DES DONNEES)              *00200001
      *                                                                *00210000
      *  ENTREE     : FICHIER FNM181                                   *00220001
      *                                                                *00230000
      *  SORTIE     : INM181  (ETAT PAPIER)                            *00240001
      ******************************************************************00250001
      *  MODIFICATIONS                                                 *00260001
      *                                                                *00270001
      *   LE 20.09.02    CB01                                          *00271001
      *      NE PAS EDITER LE MONTANT D'UNE M�ME TRANSACTION SUR TOUTES*00272001
      *      LES LIGNES DE LA TRANSACTION SINON LE TOTAL MAGASIN EST   *00273001
      *      FAUX                                                      *00274001
      *                                                                *00275001
      ******************************************************************00280000
       ENVIRONMENT DIVISION.                                            00290000
       CONFIGURATION SECTION.                                           00300000
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00310000
       INPUT-OUTPUT SECTION.                                            00320000
                                                                        00330001
       FILE-CONTROL.                                                    00340000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNM181      ASSIGN  TO  FNM181.                       00350001
      *                                                                         
      *--                                                                       
           SELECT FNM181      ASSIGN  TO  FNM181                                
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00360001
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT INM181      ASSIGN  TO  INM181.                       00370001
      *                                                                         
      *--                                                                       
           SELECT INM181      ASSIGN  TO  INM181                                
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00380001
      *}                                                                        
       DATA DIVISION.                                                   00390000
       FILE SECTION.                                                    00400000
                                                                        00410001
      ******************************************************************00420001
      * FICHIER FNM181                                                 *00430001
      ******************************************************************00440001
       FD  FNM181                                                       00450001
           RECORDING F                                                  00460001
           LABEL RECORD STANDARD                                        00470001
           BLOCK  CONTAINS 0   RECORDS.                                 00480001
       01  FNM180-RECORD       PIC X(120).                              00490001
                                                                        00500001
                                                                        00510001
      ******************************************************************00520000
      * FICHIER IMPRESSION INM181                                      *00530001
      ******************************************************************00540001
       FD  INM181                                                       00550001
           RECORDING F                                                  00560001
           LABEL RECORD STANDARD                                        00570001
           BLOCK  CONTAINS 0   RECORDS.                                 00580001
       01  INM181-RECORD      PIC X(133).                               00590001
                                                                        00600001
      ******************************************************************00610001
                                                                        00620001
       WORKING-STORAGE SECTION.                                         00630000
       01  FILLER                     PIC X(16) VALUE '*** WORKING ***'.00640000
                                                                        00650001
      *                                                                 00660001
       01  W-COMPTEUR                 PIC 9(2).                         00670001
       01  CPT-PAGES                  PIC 9(3) VALUE ZEROES.            00680001
       01  CPT-LIGNES-PAGE            PIC 9(2) VALUE ZEROES.            00690001
                                                                        00700001
      * COMPTE LE NB DE LECTURES EFFECTUEES                             00710001
       01  CPT-LECTURE                PIC 9(5) VALUE ZEROES.            00720001
                                                                        00730001
      * COMPTE LE NB DE LIGNES ECRITES                                  00740001
       01  CPT-LIGNES                 PIC 9(5) VALUE ZEROES.            00750001
                                                                        00760001
      *                                                                 00770001
       01  CPT-RUPTURE                PIC 9(3) VALUE 0.                 00780001
                                                                        00790001
      *                                                                 00800001
       01  TOP-LECTURE-FNM181         PIC 9   VALUE 1.                  00810001
           88  DEBUT-FNM181                   VALUE 1.                  00820001
           88  FIN-FNM181                     VALUE 2.                  00830001
                                                                        00840001
      * VARIABLES DE SAUVEGARDE DES N� SOCIETE ET LIEU POUR             00850001
      * IDENTIFIER SI RUPTURE OU PAS                                    00860001
       01  SAVE-NSOCIETE              PIC X(03).                        00870001
       01  SAVE-NLIEU                 PIC X(03).                        00880001
CB01   01  SAVE-DCAISSE               PIC X(08).                        00881001
 "     01  SAVE-NCAISSE               PIC X(03).                        00882001
AL0810*01  SAVE-NTRANS                PIC X(04).                        00883001
 "     01  SAVE-NTRANS                PIC X(08).                        00884001
                                                                        00890001
      * VARIABLES DE RUPTURE POUR SOCIETE                               00900001
       01  TOP-RUPTURE-SOCIETE        PIC 9   VALUE 1.                  00910001
           88  PAS-RUPTURE-SOCIETE            VALUE 1.                  00920001
           88  RUPTURE-SOCIETE                VALUE 2.                  00930001
                                                                        00940001
      * VARIABLES DE RUPTURE POUR LES MAGASINS                          00950001
       01  TOP-RUPTURE-LIEU           PIC 9   VALUE 1.                  00960001
           88  PAS-RUPTURE-LIEU               VALUE 1.                  00970001
           88  RUPTURE-LIEU                   VALUE 2.                  00980001
                                                                        00990001
CB01  * VARIABLES DE RUPTURE POUR DATE DE CAISSE                        00991001
 "     01  TOP-RUPTURE-DCAISSE        PIC 9   VALUE 1.                  00992001
 "         88  PAS-RUPTURE-DCAISSE            VALUE 1.                  00993001
 "         88  RUPTURE-DCAISSE                VALUE 2.                  00994001
 "                                                                      00995001
 "    * VARIABLES DE RUPTURE POUR N� DE CAISSE                          00996001
 "     01  TOP-RUPTURE-NCAISSE        PIC 9   VALUE 1.                  00997001
 "         88  PAS-RUPTURE-NCAISSE            VALUE 1.                  00998001
 "         88  RUPTURE-NCAISSE                VALUE 2.                  00999001
 "                                                                      00999101
 "    * VARIABLES DE RUPTURE POUR N� DE TRANSACTION                     00999201
 "     01  TOP-RUPTURE-NTRANS         PIC 9   VALUE 1.                  00999301
 "         88  PAS-RUPTURE-NTRANS             VALUE 1.                  00999401
 "         88  RUPTURE-NTRANS                 VALUE 2.                  00999501
                                                                        00999601
      *                                                                 01000001
      * ZONES DES TABLES                                                01010001
       01  W-NSOCIETE                 PIC X(03).                        01020001
       01  W-NLIEU                    PIC X(03).                        01030001
       01  W-DCAISSE.                                                   01040001
           05  W-JJ-DCAISSE           PIC X(02).                        01041001
           05  FILLER                 PIC X(01) VALUE SPACE.            01042001
           05  W-MM-DCAISSE           PIC X(02).                        01044001
           05  FILLER                 PIC X(01) VALUE SPACE.            01045001
           05  W-SA-DCAISSE           PIC X(04).                        01046001
       01  W-NCAISSE                  PIC X(03).                        01050001
AL0810*01  W-NTRANS                   PIC X(04).                        01060001
       01  W-NTRANS                   PIC X(08).                        01061001
       01  W-NEMPORT                  PIC X(08).                        01070001
       01  W-HEURE-VENTE.                                               01080001
           05  W-HHVENTE              PIC X(02).                        01081001
           05  FILLER                 PIC X(01) VALUE ':'.              01082001
           05  W-MNVENTE              PIC X(02).                        01090001
AL0810*01  W-NOPERATEUR               PIC X(04).                        01100001
       01  W-NOPERATEUR               PIC X(07).                        01101001
       01  W-LREF                     PIC X(20).                        01110001
AL0810*01  W-CRAYON                   PIC X(02).                        01120001
       01  W-NTYPVENTE                PIC X(03).                        01121001
       01  W-PREGLRENDU               PIC S9(7)V9(2).                   01130001
       01  W-CUM-TOTAL                PIC S9(9)V9(2) VALUE 0.           01131001
                                                                        01140001
      *                                                                 01150001
       01  DATEAMJ                    PIC X(6).                         01160001
       01  DATETRAIT-JMSA.                                              01170001
           02 DATETRAIT-JJ            PIC X(2).                         01180001
           02 FILLER                  PIC X VALUE '/'.                  01190001
           02 DATETRAIT-MM            PIC X(2).                         01200001
           02 FILLER                  PIC X VALUE '/'.                  01210001
           02 FILLER                  PIC XX VALUE '20'.                01220001
           02 DATETRAIT-AA            PIC X(2).                         01230001
      *                                                                 01240001
      **** DESCRIPTION COPY UTILISATEUR                                 01250001
           COPY FNM180.                                                 01260001
      *                                                                 01270001
      ***************************************************************** 01280001
      * DESCRIPTION DU FICHIER DE SORTIE (FICHIER D'EDITION FNM180)   * 01290001
      * LONGUEUR D' UNE LIGNE 150 C.                                  * 01300001
      ***************************************************************** 01310001
       01  FILLER   PIC X(20)  VALUE 'FNM180'.                          01320001
       01  DSECT-INM181.                                                01330001
           05 OCTET-RESERVE           PIC X.                            01340001
           05 VRAIE-DSECT-INM181      PIC X(132).                       01350001
      *                                                                 01360001
       01  FILLER.                                                      01370001
      * LIBELLES DES COLONNES                                           01380001
          05  INM181-ENTETE-LIBELLE-PART1.                              01390001
               10 FILLER                 PIC  X(01) VALUE ' '.          01400001
               10 FILLER                 PIC  X(02) VALUE '! '.         01410001
               10 INM181-LIB-NSOCIETE    PIC  X(03) VALUE 'SOC'.        01420001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01430001
               10 INM181-LIB-NLIEU       PIC  X(04) VALUE 'LIEU'.       01440001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01450001
               10 INM181-LIB-DCAISSE     PIC  X(10) VALUE '   DATE   '. 01460001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01470001
               10 INM181-LIB-NCAISSE     PIC  X(06) VALUE 'CAISSE'.     01480001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01490001
               10 INM181-LIB-NTRANS      PIC  X(08) VALUE 'TRANSAC.'.   01500001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01510001
               10 INM181-LIB-NVENTE-EMPORT                              01520001
                                         PIC  X(08) VALUE 'N�VENTE'.    01530001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01540001
               10 INM181-LIB-HEURE       PIC  X(05) VALUE 'HEURE'.      01550001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01560001
               10 INM181-LIB-NOPERATEUR  PIC  X(07) VALUE 'OPERAT.'.    01570001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01580001
               10 INM181-LIB-NCODIC-LREF PIC  X(20)                     01590001
                              VALUE ' NCODIC / REFERENCE '.             01600001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01610001
AL0810*        10 INM181-LIB-CRAYON      PIC  X(05) VALUE 'NAT  '.      01620001
               10 INM181-LIB-NTYPVENTE   PIC  X(03) VALUE 'NAT'.        01621004
               10 FILLER                 PIC  X(03) VALUE ' ! '.        01630001
               10 INM181-LIB-PREGLRENDU  PIC  X(13)                     01640001
                              VALUE '   MONTANT   '.                    01650001
               10 FILLER                 PIC  X(02) VALUE ' !'.         01660001
                                                                        01670001
          05  INM181-ENTETE-LIBELLE-PART2.                              01680001
               10 FILLER              PIC  X(01) VALUE ' '.             01690001
SOC            10 FILLER              PIC  X(06) VALUE '!     '.        01700001
LIEU           10 FILLER              PIC  X(07) VALUE '!      '.       01710001
DATE           10 FILLER              PIC  X(13)                        01720001
                         VALUE '!            '.                         01730001
CAIS           10 FILLER              PIC  X(09)                        01740001
                         VALUE '!        '.                             01750001
TRANS          10 FILLER              PIC  X(11)                        01760003
                         VALUE '!          '.                           01770003
MVT1           10 FILLER              PIC  X(02)                        01780001
                         VALUE '! '.                                    01790001
MVT2           10 INM181-LIB-MVT-FIN  PIC X(08)                         01800001
                         VALUE 'MVT FIN'.                               01810001
MVT3           10 FILLER              PIC  X(01)                        01820001
                         VALUE ' '.                                     01830001
DVENT          10 FILLER              PIC  X(08)                        01840001
                         VALUE '!       '.                              01850001
NOPE           10 FILLER              PIC  X(10)                        01860003
                         VALUE '!         '.                            01870003
LREF           10 FILLER              PIC  X(23)                        01880001
                         VALUE '!                      '.               01890001
CRAY           10 FILLER              PIC  X(06)                        01900004
                         VALUE '! VTE '.                                01910004
REND           10 FILLER              PIC  X(16)                        01920001
                         VALUE '!               '.                      01930001
               10 FILLER              PIC  X(01) VALUE '!'.             01940001
                                                                        01950001
      * LIGNES DETAIL                                                   01960001
          05  INM181-ENTETE-DETAIL.                                     01970001
             06  INM181-RUPTURES.                                       01980001
               10 FILLER                 PIC  X(01) VALUE ' '.          01990001
               10 FILLER                 PIC  X(02) VALUE '! '.         02000001
               10 INM181-NSOCIETE        PIC  X(03).                    02010001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02020001
               10 FILLER                 PIC  X(01) VALUE ' '.          02030001
               10 INM181-NLIEU           PIC  X(03).                    02040001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02050001
               10 INM181-DCAISSE         PIC  X(10).                    02060001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02070001
               10 FILLER                 PIC  X(02) VALUE '  '.         02080001
               10 INM181-NCAISSE         PIC  X(03).                    02090001
               10 FILLER                 PIC  X(01) VALUE ' '.          02100001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02110003
             06  INM181-CHAMPS.                                         02120001
               10 INM181-NTRANS          PIC  X(08).                    02130003
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02140003
               10 INM181-NVENTE-NEMPORT  PIC  X(08).                    02150001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02160001
               10 INM181-DHVENTE         PIC  X(05).                    02170001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02200001
AL0810*        10 INM181-NOPERATEUR      PIC  X(04).                    02210001
               10 INM181-NOPERATEUR      PIC  X(07).                    02211001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02220001
               10 INM181-LIBELLE         PIC  X(20).                    02230001
               10 FILLER                 PIC  X(03) VALUE ' ! '.        02240001
AL0810*        10 INM181-CRAYON          PIC  X(02).                    02260001
               10 INM181-NTYPVENTE       PIC  X(02).                    02261001
               10 FILLER                 PIC  X(04) VALUE '  ! '.       02280004
CB01           10 INM181-PREGLRENDU      PIC  ----------,--.            02290001
               10 FILLER                 PIC  X(02) VALUE ' !'.         02300001
                                                                        02310001
      *   LIGNES D'ENTETE                                               02320001
           05  INM181-ENTETE.                                           02330001
               10 FILLER                 PIC  X(01) VALUE '1'.          02341003
               10 FILLER                 PIC  X(07) VALUE ' INM181'.    02350001
               10 FILLER                 PIC  X(10) VALUE SPACES.       02360001
               10 FILLER                 PIC  X(53) VALUE               02370001
               '            SORTIE ESPECES DES CAISSES               '. 02380001
               10 FILLER                 PIC  X(05) VALUE SPACES.       02390001
               10 INM181-E1-DATE         PIC  X(10).                    02400001
               10 FILLER                 PIC  X(10) VALUE '   PAGE : '. 02410001
               10 INM181-ENT-NPAGE       PIC  ZZ9.                      02420001
               10 FILLER                 PIC  X(34) VALUE SPACES.       02430001
                                                                        02440001
           05  INM181-ENTETE-BLANC.                                     02450001
               10 FILLER              PIC  X(001) VALUE SPACES.         02460001
               10 FILLER              PIC  X(132) VALUE SPACES.         02470001
                                                                        02480001
           05  INM181-ENTETE-TIRET.                                     02490001
               10 FILLER              PIC  X(01)  VALUE ' '.            02500003
               10 FILLER              PIC  X(01)  VALUE '!'.            02500103
               10 INM181-TIRET        PIC  X(119) VALUE ' '.            02501004
               10 FILLER              PIC  X(01)  VALUE '!'.            02502002
                                                                        02720001
                                                                        02730000
      **** DESCRIPTION COPY AIDA                                        02740000
           COPY SYBWDIV0.                                               02750001
       01  Z-INOUT                    PIC X(236)           VALUE SPACES.02760000
           COPY SYBWERRO.                                               02770001
           COPY ABENDCOP.                                               02780000
                                                                        02790001
       LINKAGE SECTION.                                                 02800000
                                                                        02810001
       PROCEDURE DIVISION.                                              02820000
      *                                                                 02830000
      ******************************************************************02840000
      *                      T R A I T E M E N T                       *02850000
      ******************************************************************02860000
      *                                                                 02870000
      *---------------------------------------                          02880001
       MODULE-BNM181                  SECTION.                          02890001
      *---------------------------------------                          02900000
           MOVE 0 TO W-COMPTEUR.                                        02910001
                                                                        02920001
           PERFORM MODULE-ENTREE.                                       02930001
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FNM181.                  02940001
           PERFORM MODULE-SORTIE.                                       02950000
      *                                                                 02960000
       FIN-MODULE-BNM181. EXIT.                                         02970001
      *                                                                 02980000
      *---------------------------------------                          02990001
       MODULE-ENTREE                  SECTION.                          03000000
      *---------------------------------------                          03010000
      *                                                                 03020000
           DISPLAY  '************************************************'. 03030001
           DISPLAY  '**              PROGRAMME BNM181              **'. 03040001
           DISPLAY  '**           SORTIE DE L ETAT INM181          **'. 03050001
           DISPLAY  '**  LISTE DES SORTIES REMBOURSEMENT ESPECES   **'. 03060001
           DISPLAY  '**                    DARTY                   **'. 03070001
           DISPLAY  '************************************************'. 03080001
                                                                        03090001
           MOVE 'BNM181' TO ABEND-PROG.                                 03100001
                                                                        03110001
           OPEN INPUT FNM181.                                           03120001
           OPEN OUTPUT INM181.                                          03130001
                                                                        03140001
      * PREMIERE LECTURE DU FICHIER FNM181                              03150001
           PERFORM LECTURE-FNM181.                                      03160001
           IF FIN-FNM181                                                03170001
              DISPLAY '** FICHIER FNM181 VIDE (!!!)  '                  03180001
           END-IF.                                                      03190001
      *                                                                 03200001
           ACCEPT DATEAMJ     FROM DATE.                                03210001
           MOVE  DATEAMJ(1:2) TO DATETRAIT-AA.                          03220001
           MOVE  DATEAMJ(3:2) TO DATETRAIT-MM.                          03230001
           MOVE  DATEAMJ(5:2) TO DATETRAIT-JJ.                          03240001
                                                                        03241002
           MOVE ALL '-' TO INM181-TIRET.                                03242002
      *                                                                 03250000
       FIN-MODULE-ENTREE. EXIT.                                         03260000
      *                                                                 03270000
      *                                                                 03280000
      *---------------------------------------                          03290001
       MODULE-TRAITEMENT              SECTION.                          03300000
      *---------------------------------------                          03310000
      *                                                                 03320000
                                                                        03330001
      * RUPTURE AVEC CHANGEMENT DE PAGE ET REECRITURE DE L'ENTE         03340001
      * SI CHGMT DE SOC,                                                03350001
      * SI CHGMT DE LIEU,                                               03360001
      * SI PAGE PLEINE (56 LIGNES)                                      03370001
                                                                        03380001
      *  POUR LE PREMIER PASSAGE EN LECTURE                             03390001
           IF CPT-LECTURE = 0                                           03400001
           THEN                                                         03410001
                MOVE FNM180-NSOC           TO SAVE-NSOCIETE             03420001
                MOVE FNM180-NLIEU          TO SAVE-NLIEU                03430001
                SET PAS-RUPTURE-SOCIETE    TO TRUE                      03440001
                SET PAS-RUPTURE-LIEU       TO TRUE                      03450001
           END-IF.                                                      03460001
                                                                        03470001
      *                                                                 03480001
           IF CPT-LIGNES-PAGE = 0                                       03490001
           OR CPT-LECTURE = 1                                           03500001
              PERFORM DEBUT-PAGE                                        03510001
           END-IF.                                                      03520001
      *                                                                 03530001
           IF CPT-LIGNES-PAGE > 0 AND CPT-LIGNES-PAGE < 56              03540001
              IF  PAS-RUPTURE-SOCIETE AND PAS-RUPTURE-LIEU              03550001
              THEN                                                      03560001
                   PERFORM DETAIL-LIGNES                                03570001
              ELSE                                                      03580001
                   PERFORM FIN-PAGE                                     03590001
                   PERFORM DEBUT-PAGE                                   03600001
                   PERFORM DETAIL-LIGNES                                03610001
              END-IF                                                    03611001
           END-IF.                                                      03620001
      *                                                                 03630001
           IF CPT-LIGNES-PAGE = 56                                      03640001
              PERFORM FIN-PAGE                                          03650001
           END-IF.                                                      03660001
                                                                        03670001
      * LECTURE SUITE                                                   03680001
           PERFORM LECTURE-FNM181.                                      03690001
      *                                                                 03710001
       FIN-MODULE-TRAITEMENT. EXIT.                                     03720001
      *                                                                 03730000
      *---------------------------------------                          03740001
       LECTURE-FNM181                 SECTION.                          03750001
      *---------------------------------------                          03760001
      *                                                                 03770001
           READ FNM181 INTO DSECT-FNM180                                03780001
                AT END SET FIN-FNM181 TO TRUE.                          03790001
                                                                        03791001
           IF NOT FIN-FNM181                                            03800001
              IF CPT-LECTURE > 0                                        03810001
                                                                        03820001
      *  GESTION DE LA RUPTURE AU NIVEAU SOCIETE                        03830001
                 IF FNM180-NSOC NOT = SAVE-NSOCIETE                     03840001
                    SET RUPTURE-SOCIETE        TO TRUE                  03850001
                 ELSE                                                   03860001
                    SET PAS-RUPTURE-SOCIETE    TO TRUE                  03870001
                 END-IF                                                 03880001
                                                                        03890001
      *  GESTION DE LA RUPTURE AU NIVEAU MAGASIN                        03900001
                 IF FNM180-NLIEU NOT = SAVE-NLIEU                       03910001
                    SET RUPTURE-LIEU           TO TRUE                  03920001
                 ELSE                                                   03930001
                    SET PAS-RUPTURE-LIEU       TO TRUE                  03940001
                 END-IF                                                 03950001
                                                                        03960101
              END-IF                                                    03960201
                                                                        03960301
CB01  *  GESTION DE LA RUPTURE AU NIVEAU DATE CAISSE                    03961001
 "            IF FNM180-DCAISSE NOT = SAVE-DCAISSE                      03962001
 "               SET RUPTURE-DCAISSE        TO TRUE                     03963001
 "            ELSE                                                      03964001
 "               SET PAS-RUPTURE-DCAISSE    TO TRUE                     03965001
 "            END-IF                                                    03966001
 "                                                                      03967001
 "    *  GESTION DE LA RUPTURE AU NIVEAU N� CAISSE                      03968001
 "            IF FNM180-NCAISSE NOT = SAVE-NCAISSE                      03969001
 "               SET RUPTURE-NCAISSE        TO TRUE                     03969101
 "            ELSE                                                      03969201
 "               SET PAS-RUPTURE-NCAISSE    TO TRUE                     03969301
 "            END-IF                                                    03969401
 "                                                                      03969501
 "    *  GESTION DE LA RUPTURE AU NIVEAU N� TRANSACTION                 03969601
 "            IF FNM180-NTRANS NOT = SAVE-NTRANS                        03969701
 "               SET RUPTURE-NTRANS         TO TRUE                     03969801
 "            ELSE                                                      03969901
 "               SET PAS-RUPTURE-NTRANS     TO TRUE                     03970001
CB01          END-IF                                                    03970101
                                                                        03980001
              IF CPT-LECTURE = 99999                                    03990001
              THEN                                                      04000001
                   DISPLAY ' ==> EXPLOSION COMPTEURS NB DE LECTURES'    04010001
                   MOVE 0 TO CPT-LECTURE                                04020001
              END-IF                                                    04030001
                                                                        04040001
              ADD  1  TO CPT-LECTURE                                    04050001
           END-IF.                                                      04060001
      *                                                                 04070001
           MOVE FNM180-NSOC    TO SAVE-NSOCIETE.                        04080001
           MOVE FNM180-NLIEU   TO SAVE-NLIEU.                           04090001
CB01       MOVE FNM180-DCAISSE TO SAVE-DCAISSE.                         04100001
 "         MOVE FNM180-NCAISSE TO SAVE-NCAISSE.                         04110001
CB01       MOVE FNM180-NTRANS  TO SAVE-NTRANS.                          04120001
      *                                                                 04130001
       FIN-LECTURE-FNM181. EXIT.                                        04140001
      *                                                                 04150001
      *                                                                 04160001
      *---------------------------------------                          04170001
       ECRITURE-INM181                SECTION.                          04180001
      *---------------------------------------                          04190001
      *                                                                 04200001
           INITIALIZE INM181-RECORD.                                    04210001
           WRITE INM181-RECORD FROM DSECT-INM181.                       04220001
                                                                        04230001
           ADD  1  TO CPT-LIGNES-PAGE.                                  04240001
      *                                                                 04250001
       FIN-ECRITURE-INM181. EXIT.                                       04260001
      *                                                                 04270001
      *                                                                 04280001
      *---------------------------------------                          04290001
       DEBUT-PAGE                     SECTION.                          04300001
      *---------------------------------------                          04310001
      *  AFFICHAGE DATE DE TRAITEMENT, N� DE PAGE ET EN-T�TE            04320001
           ADD 1 TO CPT-PAGES.                                          04330001
                                                                        04340001
           MOVE DATETRAIT-JMSA TO INM181-E1-DATE.                       04350001
           MOVE CPT-PAGES   TO INM181-ENT-NPAGE.                        04360001
           MOVE INM181-ENTETE  TO DSECT-INM181.                         04370001
           PERFORM ECRITURE-INM181.                                     04380001
                                                                        04390001
           MOVE INM181-ENTETE-BLANC TO DSECT-INM181.                    04400001
           PERFORM ECRITURE-INM181.                                     04410001
                                                                        04420001
           MOVE INM181-ENTETE-TIRET TO DSECT-INM181.                    04430001
           PERFORM ECRITURE-INM181.                                     04440001
                                                                        04450001
           MOVE INM181-ENTETE-LIBELLE-PART1 TO DSECT-INM181.            04460001
           PERFORM ECRITURE-INM181.                                     04470001
                                                                        04480001
           MOVE INM181-ENTETE-LIBELLE-PART2 TO DSECT-INM181.            04490001
           PERFORM ECRITURE-INM181.                                     04500001
                                                                        04510001
           MOVE INM181-ENTETE-TIRET TO DSECT-INM181.                    04520001
           PERFORM ECRITURE-INM181.                                     04530001
      *                                                                 04540001
       FIN-DEBUT-PAGE. EXIT.                                            04550001
      *                                                                 04560001
      *---------------------------------------                          04570001
       DETAIL-LIGNES                  SECTION.                          04580001
      *---------------------------------------                          04590001
      *                                                                 04600001
      * RENSEIGNEMENT DES ZONES INM181 POUR IMPRESSION                  04601001
           PERFORM RENSEIGNEMENT-ZONES.                                 04602001
      *                                                                 04603001
           MOVE INM181-ENTETE-DETAIL TO DSECT-INM181.                   04610001
           PERFORM ECRITURE-INM181.                                     04620001
      *                                                                 04630001
           IF CPT-LECTURE = 99999                                       04640001
           THEN                                                         04650001
                DISPLAY ' ==> EXPLOSION COMPTEURS NB DE LECTURES'       04660001
                MOVE 0 TO CPT-LIGNES                                    04670001
           END-IF                                                       04680001
                                                                        04690001
      *                                                                 04700001
           ADD 1 TO CPT-LIGNES.                                         04710001
      *                                                                 04720001
       FIN-DETAIL-LIGNES. EXIT.                                         04730001
      *                                                                 04740001
      *---------------------------------------                          04750001
       FIN-PAGE                       SECTION.                          04760001
      *---------------------------------------                          04770001
      *                                                                 04780001
           MOVE INM181-ENTETE-TIRET TO DSECT-INM181.                    04790001
           PERFORM ECRITURE-INM181.                                     04800001
                                                                        04810001
           IF RUPTURE-LIEU                                              04811001
              PERFORM EDITION-TOTAL                                     04812001
           END-IF.                                                      04813001
                                                                        04814001
           MOVE 0 TO CPT-LIGNES-PAGE.                                   04821001
      *                                                                 04830001
       FIN-FIN-PAGE. EXIT.                                              04840001
      *                                                                 04850001
      *---------------------------------------                          04851001
       EDITION-TOTAL                  SECTION.                          04852001
      *---------------------------------------                          04853001
      *                                                                 04854001
           MOVE SPACES                  TO INM181-NSOCIETE.             04854101
           MOVE SPACES                  TO INM181-NLIEU.                04854201
           MOVE SPACES                  TO INM181-DCAISSE               04854301
                                           INM181-NCAISSE               04854401
                                           INM181-NTRANS                04854501
                                           INM181-NVENTE-NEMPORT        04854601
                                           INM181-DHVENTE               04854701
                                           INM181-NOPERATEUR            04854901
                                           INM181-LIBELLE               04855001
AL0810*                                    INM181-CRAYON.               04855101
                                           INM181-NTYPVENTE.            04855201
           MOVE W-CUM-TOTAL             TO INM181-PREGLRENDU.           04855301
                                                                        04855401
           MOVE INM181-ENTETE-DETAIL TO DSECT-INM181.                   04855501
           PERFORM ECRITURE-INM181.                                     04856001
                                                                        04857001
           MOVE INM181-ENTETE-TIRET TO DSECT-INM181.                    04858001
           PERFORM ECRITURE-INM181.                                     04858101
                                                                        04858201
           MOVE 0 TO W-CUM-TOTAL.                                       04858301
      *                                                                 04859001
       FIN-EDITION-TOTAL. EXIT.                                         04859101
      *                                                                 04859201
      *---------------------------------------                          04860001
       RENSEIGNEMENT-ZONES            SECTION.                          04870001
      *---------------------------------------                          04880001
      *                                                                 04890001
           MOVE FNM180-NSOC             TO INM181-NSOCIETE.             04900001
           MOVE FNM180-NLIEU            TO INM181-NLIEU.                04910001
           MOVE FNM180-DCAISSE (1:4)    TO W-SA-DCAISSE.                04920001
           MOVE FNM180-DCAISSE (5:2)    TO W-MM-DCAISSE.                04920101
           MOVE FNM180-DCAISSE (7:2)    TO W-JJ-DCAISSE.                04920201
           MOVE W-DCAISSE               TO INM181-DCAISSE.              04921001
           MOVE FNM180-NCAISSE          TO INM181-NCAISSE.              04930001
           MOVE FNM180-NTRANS           TO INM181-NTRANS.               04940001
           MOVE FNM180-NVENTE-NEMPORT   TO INM181-NVENTE-NEMPORT.       04950001
           MOVE FNM180-DHVENTE          TO W-HHVENTE.                   04960001
           MOVE FNM180-DMVENTE          TO W-MNVENTE.                   04970001
           MOVE W-HEURE-VENTE           TO INM181-DHVENTE.              04971001
           MOVE FNM180-NOPERATEUR       TO INM181-NOPERATEUR.           04980001
           MOVE FNM180-LIBELLE          TO INM181-LIBELLE.              04990001
AL0810*    MOVE FNM180-CRAYON           TO INM181-CRAYON.               05000001
           MOVE FNM180-NTYPVENTE        TO INM181-NTYPVENTE.            05000101
                                                                        05000201
CB01       IF RUPTURE-SOCIETE                                           05001001
 "          OR  RUPTURE-LIEU                                            05002001
 "          OR  RUPTURE-DCAISSE                                         05003001
 "          OR  RUPTURE-NCAISSE                                         05004001
 "          OR  RUPTURE-NTRANS                                          05005001
 "              MOVE FNM180-PREGLRENDU  TO INM181-PREGLRENDU            05010001
 "              ADD  FNM180-PREGLRENDU  TO W-CUM-TOTAL                  05010101
 "         ELSE                                                         05011001
 "              MOVE ZERO               TO INM181-PREGLRENDU            05012001
CB01       END-IF.                                                      05013001
                                                                        05020001
       FIN-RENSEIGNEMENT-ZONES. EXIT.                                   05030001
      *                                                                 05040001
      *---------------------------------------                          05050001
       MODULE-SORTIE                  SECTION.                          05060001
      *---------------------------------------                          05070000
      *                                                                 05080000
      *  ECRITURE DE LA FIN DE PAGE DE INM180                           05090001
           PERFORM FIN-PAGE.                                            05100001
                                                                        05100101
           IF W-CUM-TOTAL < 0                                           05100201
              PERFORM EDITION-TOTAL                                     05101001
           END-IF.                                                      05102001
                                                                        05110001
      *                                                                 05120001
           DISPLAY '**                                            **'   05130001
           IF ERREUR                                                    05140001
              DISPLAY '**   EXECUTION STOPPEE PAR LES CONTROLES      **'05150001
           ELSE                                                         05160001
              DISPLAY '**   EXECUTION TERMINEE NORMALEMENT           **'05170001
              DISPLAY '**                                            **'05180001
           END-IF                                                       05190001
           DISPLAY '**                                            **'   05200001
           DISPLAY '************************************************'   05210001
      *                                                                 05220001
           DISPLAY '                                                '.  05230001
           DISPLAY '                                                '.  05240001
           DISPLAY '                                                '.  05250001
           DISPLAY '   *************'.                                  05260001
           DISPLAY '   * COMPTEURS *'.                                  05270001
           DISPLAY '   *************'.                                  05280001
           DISPLAY ' ==> NOMBRE DE LECTURES EFFECTUEES SUR FICHIER'     05290001
                   ' FNM181 : '                                         05300001
                     CPT-LECTURE.                                       05310001
           DISPLAY ' ==> NOMBRE DE LIGNES ECRITES SUR ETAT INM181 '     05320001
                   '        : '                                         05330001
                     CPT-LIGNES.                                        05340001
      *                                                                 05350001
           CLOSE FNM181.                                                05360001
           CLOSE INM181.                                                05370001
           GOBACK.                                                      05380001
      *                                                                 05390000
       FIN-MODULE-SORTIE. EXIT.                                         05400000
      *                                                                 05410000
      *                                                                 05420000
      ***************************************************************** 05430000
       PLANTAGE                     SECTION.                            05440000
      ***************************************************************** 05450000
           CLOSE FNM181.                                                05460001
           CLOSE INM181.                                                05470001
                                                                        05480001
           DISPLAY '*--------------------*'                             05490000
           DISPLAY '   P L A N T A G E    '                             05500000
           DISPLAY '*--------------------*'                             05510000
                                                                        05520001
           MOVE 'BNM181'   TO ABEND-PROG.                               05530001
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    05540000
      *                                                                 05550001
      *                                                                 05560000
