      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       TITLE  'CREATION DU FICHIER DES MISES A JOUR     '.                      
       PROGRAM-ID. BPC900.                                              00000030
       AUTHOR. DSA008.                                                  00000040
      **************************************************************    00000050
      *                                                            *            
      *    PROJET   : NEM                                          *    00000050
      *    FONCTION : PROGRAMME DE CREATION                        *    00000070
      *               DU FICHIER FPC900 (MAJ A ENVOYER AU MAGASIN) *    00000080
      *    DATE CREATION : 04/04/14                                *    00000100
      *    COPIE DU BFT900 MAIS AVEC DES ENREGS MAXI DE 500        *            
      **************************************************************    00000110
       ENVIRONMENT DIVISION.                                            00000120
       CONFIGURATION SECTION.                                           00000130
       SPECIAL-NAMES.                                                   00000140
           DECIMAL-POINT IS COMMA.                                      00000150
       INPUT-OUTPUT SECTION.                                            00000160
       FILE-CONTROL.                                                    00000170
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPC900 ASSIGN TO FPC900.                              00000190
      *--                                                                       
           SELECT FPC900 ASSIGN TO FPC900                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPC901 ASSIGN TO FPC901.                              00000200
      *--                                                                       
           SELECT FPC901 ASSIGN TO FPC901                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPC902 ASSIGN TO FPC902.                              00000210
      *--                                                                       
           SELECT FPC902 ASSIGN TO FPC902                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                              00000220
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000230
       DATA DIVISION.                                                   00000240
       FILE SECTION.                                                    00000250
      *---------------------------------------------------------------* 00000260
      *  D E F I N I T I O N    F I C H I E R    F T F 2 0 0          * 00000270
      *---------------------------------------------------------------* 00000280
      *                                                                 00000290
      *    NOUVEAU FICHIER                                              00000290
       FD  FPC900                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FPC900-ENR   PIC X(633).                                     00000340
      *                                                                 00000350
      *    ANCIEN FICHIER                                               00000290
       FD  FPC901                                                       00000360
           RECORDING F                                                  00000370
           BLOCK 0 RECORDS                                              00000380
           LABEL RECORD STANDARD.                                       00000390
       01  FPC901-ENR   PIC X(633).                                     00000400
      *                                                                 00000410
      *    FICHIER DES MISES A JOUR                                     00000290
       FD  FPC902                                                       00000420
           RECORDING F                                                  00000430
           BLOCK 0 RECORDS                                              00000440
           LABEL RECORD STANDARD.                                       00000450
       01  FPC902-ENR   PIC X(633).                                     00000460
      *                                                                 00000470
       FD   FDATE                                                       00000480
            RECORDING F                                                 00000490
            BLOCK 0 RECORDS                                             00000500
            LABEL RECORD STANDARD.                                      00000510
       01   ENR-FDATE.                                                  00000520
            03  FILLER      PIC X(80).                                  00000530
      *                                                                 00000540
      *                                                                 00000550
      *---------------------------------------------------------------* 00000560
      *                 W O R K I N G  -  S T O R A G E               * 00000570
      *---------------------------------------------------------------* 00000580
      *                                                                 00000590
       WORKING-STORAGE SECTION.                                         00000600
      *                                                                 00000610
      *                                                                 00000620
      *---------------------------------------------------------------* 00000630
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00000640
      *  S P E C I F I Q U E S    A U   F I C H I E R   F T F 2 0 0   * 00000650
      *---------------------------------------------------------------* 00000660
      *                                                                 00000670
           COPY FPC900C.                                                00000680
      *                                                                 00000690
      *                                                                 00000700
      *=================================================================00000710
      * DESCRIPTION DE FDATE                                            00000720
      *=================================================================00000730
      *                                                                 00000740
           COPY WORKDATC.                                               00000750
      *                                                                 00000760
       01  FDATE-ENREG.                                                 00000770
            02  FDAT.                                                   00000780
                04  FDATE-JJ   PIC X(02).                               00000790
                04  FDATE-MM   PIC X(02).                               00000800
                04  FDATE-SS   PIC X(02).                               00000810
                04  FDATE-AA   PIC X(02).                               00000820
            02  FILLER         PIC X(72).                               00000830
      *                                                                 00000840
       01  FDATE-TRANS.                                                 00000850
                04  TRANS-SS   PIC X(02).                               00000860
                04  TRANS-AA   PIC X(02).                               00000870
                04  TRANS-MM   PIC X(02).                               00000880
                04  TRANS-JJ   PIC X(02).                               00000890
      *                                                                 00000900
      *                                                                 00000910
           COPY ABENDCOP.                                               00000920
           EJECT                                                        00000930
      *                                                                 00000940
      *---------------------------------------------------------------* 00000950
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00000960
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      * 00000970
      *---------------------------------------------------------------* 00000980
      *                                                                 00000990
      *                                                                 00001000
       01  COMPTEURS.                                                   00001010
           03  CTR-FPC900             PIC  S9(06) COMP-3 VALUE +0.      00001020
           03  CTR-FPC901             PIC  S9(06) COMP-3 VALUE +0.      00001030
           03  CTR-FPC902             PIC  S9(06) COMP-3 VALUE +0.      00001040
           03  NOMBRE                 PIC  ZZZZ9.                       00001050
      *                                                                 00001060
       01  WSSAAMMJJ.                                                   00001070
           03  WSS                    PIC  X(02)  VALUE '19'.           00001080
           03  WAAMMJJ                PIC  X(06).                       00001090
      *                                                                 00001100
      *                                                                 00001110
       01  INDICES.                                                     00001120
           03  I                      PIC  999.                         00001130
           03  L                      PIC  999.                         00001140
           03  J                      PIC  999.                         00001150
           03  K                      PIC  99.                          00001160
      *                                                                 00001170
       01 ETAT-FICHIER-FPC901 PIC X VALUE '1'.                          00001180
           88 NON-FIN-FPC901  VALUE '1'.                                00001190
           88 FIN-FPC901      VALUE '0'.                                00001200
      *                                                                 00001210
       01 ETAT-FICHIER-FPC900 PIC X VALUE '1'.                          00001220
           88 NON-FIN-FPC900  VALUE '1'.                                00001230
           88 FIN-FPC900      VALUE '0'.                                00001240
      *                                                                 00001250
       PROCEDURE DIVISION.                                              00001260
      *                                                                 00001270
       MODULE-BPC900.                                                   00001280
      *                                                                 00001290
           PERFORM  DEBUT                                               00001300
           PERFORM  TRAITEMENT UNTIL FIN-FPC901 AND FIN-FPC900          00001310
           PERFORM  FIN                                                 00001320
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00001330
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00001360
      *---------------------------------------------------------------* 00001370
      *                    D E B U T                                  * 00001380
      *---------------------------------------------------------------* 00001390
      *                                                                 00001400
       DEBUT.                                                           00001410
      *======                                                           00001420
           OPEN OUTPUT FPC902.                                          00001430
           OPEN INPUT FPC901 FPC900 FDATE.                              00001440
           PERFORM LECTURE-FPC901.                                      00001450
           PERFORM LECTURE-FPC900.                                      00001460
      *                                                                 00001470
           READ   FDATE  INTO   FDATE-ENREG   AT END                    00001480
                MOVE  ' FDATE ! DATE DU JOUR : JJMMSSAA '               00001490
                                       TO   ABEND-MESS                  00001500
                MOVE  01                     TO   ABEND-CODE            00001510
           PERFORM  FIN-ANORMALE.                                       00001520
      *                                                                 00001530
           MOVE '1' TO GFDATA.                                          00001540
           MOVE FDATE-JJ  TO GFJOUR.                                    00001550
           MOVE FDATE-MM  TO GFMOIS.                                    00001560
           MOVE FDATE-SS  TO GFSIECLE.                                  00001570
           MOVE FDATE-AA  TO GFANNEE.                                   00001580
           CALL 'BETDATC' USING WORK-BETDATC.                           00001590
           IF GFVDAT = 0                                                00001600
              MOVE 'DATE INCORRECTE  ' TO ABEND-MESS                    00001610
              MOVE  02                     TO   ABEND-CODE              00001620
              PERFORM  FIN-ANORMALE                                     00001630
           END-IF.                                                      00001640
                                                                        00001650
           MOVE FDATE-SS  TO TRANS-SS.                                  00001660
           MOVE FDATE-AA  TO TRANS-AA.                                  00001670
           MOVE FDATE-MM  TO TRANS-MM.                                  00001680
           MOVE FDATE-JJ  TO TRANS-JJ.                                  00001690
      *                                                                 00001700
      *---------------------------------------------------------------* 00001710
      *                     T R A I T E M E N T                       * 00001720
      *---------------------------------------------------------------* 00001730
      *                                                                 00001740
       TRAITEMENT.                                                      00001750
      *===========                                                      00001760
      *                                                                 00001770
      *---------> CAS D'UNE ANNULATION                                  00001780
      *                                                                 00001790
           IF ( PC901-CLEL    < PC900-CLEL    OR FIN-FPC900 )           00001800
                     AND NON-FIN-FPC901                                 00001810
               PERFORM ANNULATION-ENREG                                 00001820
               PERFORM LECTURE-FPC901                                   00001830
           END-IF.                                                      00001840
      *                                                                 00001850
      *---------> CAS D'UNE MODIFICATION POSSIBLE                       00001860
      *                                                                 00001870
           IF PC900-CLEL    = PC901-CLEL                                00001880
                       AND NON-FIN-FPC901                               00001890
                       AND NON-FIN-FPC900                               00001900
               PERFORM COMPARAISON-ENREG                                00001910
               PERFORM LECTURE-FPC901                                   00001920
               PERFORM LECTURE-FPC900                                   00001930
           END-IF.                                                      00001940
      *                                                                 00001950
      *---------> CAS D'UNE CREATION                                    00001960
      *                                                                 00001970
           IF ( PC901-CLEL    > PC900-CLEL    OR FIN-FPC901 )           00001980
                     AND NON-FIN-FPC900                                 00001990
               PERFORM CREATION-ENREG                                   00002000
               PERFORM LECTURE-FPC900                                   00002010
           END-IF.                                                      00002020
      *                                                                 00002070
       ANNULATION-ENREG.                                                00002080
      *==================                                               00002090
      *                                                                 00002100
           MOVE PC901-ENREG  TO PC902-ENREG.                            00002110
           MOVE 'S'          TO PC902-MAJ.                              00002120
           PERFORM ECRITURE-FPC902.                                     00002130
      *                                                                 00002140
       COMPARAISON-ENREG.                                               00002190
      *==================                                               00002200
      *                                                                 00002210
           IF PC900-DATA NOT = PC901-DATA                               00002220
               MOVE PC900-ENREG  TO PC902-ENREG                         00002230
               MOVE 'M'          TO PC902-MAJ                           00002240
               PERFORM ECRITURE-FPC902                                  00002250
           END-IF.                                                      00002260
      *                                                                 00002270
       CREATION-ENREG.                                                  00002320
      *=================                                                00002330
      *                                                                 00002340
           MOVE PC900-ENREG  TO PC902-ENREG                             00002350
           MOVE 'C'          TO PC902-MAJ.                              00002360
           PERFORM ECRITURE-FPC902.                                     00002370
      *                                                                 00002420
       LECTURE-FPC901.                                                  00002430
      *===============                                                  00002440
      *                                                                 00002450
           READ FPC901                                                  00002460
                INTO PC901-ENREG                                        00002470
                AT END SET FIN-FPC901 TO TRUE                           00002480
           END-READ.                                                    00002490
           IF NON-FIN-FPC901                                            00002500
               ADD 1 TO CTR-FPC901                                      00002510
           END-IF.                                                      00002520
      *                                                                 00002530
       LECTURE-FPC900.                                                  00002590
      *===============                                                  00002600
      *                                                                 00002610
           READ FPC900                                                  00002620
                INTO PC900-ENREG                                        00002630
                AT END SET FIN-FPC900 TO TRUE                           00002640
           END-READ.                                                    00002650
           IF NON-FIN-FPC900                                            00002660
               ADD 1 TO CTR-FPC900                                      00002670
           END-IF.                                                      00002680
      *                                                                 00002690
       ECRITURE-FPC902.                                                 00002750
      *================                                                 00002760
           ADD 1 TO CTR-FPC902                                          00002770
           MOVE FDATE-TRANS TO PC902-DATMAJ                             00002780
           ACCEPT PC902-HEUREMAJ FROM TIME                              00002790
           WRITE FPC902-ENR FROM PC902-ENREG                            00002840
           END-WRITE.                                                   00002850
      *                                                                 00002860
      *---------------------------------------------------------------* 00002870
      *                             F I N                             * 00002880
      *---------------------------------------------------------------* 00002890
      *                                                                 00002900
       FIN.                                                             00002910
      *====                                                             00002920
           CLOSE FPC900 FPC901 FPC902 FDATE.                            00002930
           DISPLAY '**********************************************'     00002940
           DISPLAY '*    PROGRAMME BPC900 TERMINE NORMALEMENT    *'     00002950
           DISPLAY '*                                            *'     00002960
           MOVE CTR-FPC901 TO NOMBRE.                                   00002970
           DISPLAY '*  NOMBRE DE LIGNES LUES SUR FPC901 = ' NOMBRE      00002980
                   '  *'.                                               00002990
           MOVE CTR-FPC900 TO NOMBRE.                                   00003000
           DISPLAY '*  NOMBRE DE LIGNES LUES SUR FPC900 = ' NOMBRE      00003010
                   '  *'.                                               00003020
           MOVE CTR-FPC902 TO NOMBRE.                                   00003030
           DISPLAY '*  NOMBRE D''ENREG ECRITS DS FPC902  = ' NOMBRE     00003040
                   '  *'.                                               00003050
           DISPLAY '*                                            *'     00003060
           DISPLAY '**********************************************'.    00003070
      *                                                                 00003080
           EJECT                                                        00003090
      *                                                                 00003100
      *---------------------------------------------------------------* 00003110
      *          P A R A G R A P H E   F I N - A N O R M A L E        * 00003120
      *---------------------------------------------------------------* 00003130
      *                                                                 00003140
       FIN-ANORMALE.                                                    00003150
      *=============                                                    00003160
           MOVE 'BPC900' TO ABEND-PROG                                  00003170
           CALL 'ABEND' USING                                           00003180
                        ABEND-PROG                                      00003190
                        ABEND-MESS.                                     00003200
      *                                                                 00003210
           EJECT                                                        00003220
