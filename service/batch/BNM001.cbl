      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.BNM001.                                               00000030
       AUTHOR.                                                          00000040
      ***************************************************************   00000050
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *   00000060
      ***************************************************************   00000070
      *                                                                 00000080
      *  PROJET     : DSA014                                            00000090
      *  PROGRAMME  : BNM001                                            00000100
      *  CREATION   : 99/06                                             00000110
      *  FONCTION   :                                                   00000120
      *  - ECRITURE DES CAISSES EN REPRISE                              00000130
      *                                                                 00000140
      *  PERIODICITE: JOURNALIER.                                       00000150
      *                                                                 00000170
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   00000180
      * MODIFICATIONS :                                                         
      *   27.03.02 :                                                            
      *   LIRE LE 1ER ENR. DE FNM001 PAR LE PAVE DE LECTURE ET NON              
      *   PAS PAR UN READ DIRECT, AFIN D'ALIMENTER LA ZONE CLE-FNM001           
      *   POUR COMPARAISON AVEC LE FICHIER DES REPRISES (CB01)                  
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   00000180
      *                                                                 00000190
       ENVIRONMENT DIVISION.                                            00000200
       CONFIGURATION SECTION.                                           00000210
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00000220
       INPUT-OUTPUT SECTION.                                            00000230
       FILE-CONTROL.                                                    00000240
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNM001  ASSIGN TO FNM001.                            00000250
      *--                                                                       
            SELECT FNM001  ASSIGN TO FNM001                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNM002  ASSIGN TO FNM002.                            00000250
      *--                                                                       
            SELECT FNM002  ASSIGN TO FNM002                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNM003  ASSIGN TO FNM003.                            00000250
      *--                                                                       
            SELECT FNM003  ASSIGN TO FNM003                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI00  ASSIGN TO FFTI00.                            00000340
      *--                                                                       
            SELECT FFTI00  ASSIGN TO FFTI00                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI01  ASSIGN TO FFTI01.                            00000340
      *--                                                                       
            SELECT FFTI01  ASSIGN TO FFTI01                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI02  ASSIGN TO FFTI02.                            00000340
      *--                                                                       
            SELECT FFTI02  ASSIGN TO FFTI02                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFTI03  ASSIGN TO FFTI03.                            00000340
      *--                                                                       
            SELECT FFTI03  ASSIGN TO FFTI03                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FREPRIS ASSIGN TO FREPRIS.                           00000350
      *--                                                                       
            SELECT FREPRIS ASSIGN TO FREPRIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000370
       DATA DIVISION.                                                   00000380
       FILE SECTION.                                                    00000390
      *                                                                 00000400
       FD   FREPRIS RECORDING F                                                 
                    BLOCK 0 RECORDS                                             
                    LABEL RECORD STANDARD.                              00000470
      *                                                                 00000490
      *   ENRGISTREMENT REPRISE CAISSE ERREUR                           00000500
      *                                                                 00000510
       01   FREPRIS-CLE PIC X(17).                                      00000480
      *                                                                 00000400
       FD   FNM001 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                               00000470
      *                                                                 00000490
      *01   FNM001-ENR.                                                 00000480
      *  02 FNM001-CLE    PIC X(17).                                    00000480
      *  02 FNM001-ENREG  PIC X(183).                                   00000480
           COPY FNM001.                                                         
      *                                                                 00000400
       FD   FNM002 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                               00000470
      *                                                                 00000490
 "     01   FNM002-ENR    PIC X(200).                                   00000480
      *                                                                 00000400
       FD   FNM003 RECORDING F                                                  
                   BLOCK 0 RECORDS                                              
                   LABEL RECORD STANDARD.                               00000470
      *                                                                 00000490
 "     01   FNM003-ENR    PIC X(200).                                   00000480
      *                                                                 00001180
       FD   FFTI00  RECORDING F BLOCK 0 RECORDS                         00001190
                    LABEL RECORD STANDARD.                              00001200
       01   FFTI00-ENR.                                                 00001260
           02  FFTI00-CLE     PIC X(17).                                        
           02  FFTI00-ENREG  PIC X(400).                                        
      *                                                                         
       FD   FFTI01  RECORDING F BLOCK 0 RECORDS                         00001190
                    LABEL RECORD STANDARD.                              00001200
       01   FFTI01-ENR PIC X(400).                                      00001260
      *                                                                 00000540
       FD   FFTI02  RECORDING F BLOCK 0 RECORDS                         00001190
                    LABEL RECORD STANDARD.                              00001200
       01   FFTI02-ENR.                                                 00001260
           02  FFTI02-CLE     PIC X(17).                                        
           02  FFTI02-ENREG  PIC X(400).                                        
      *                                                                         
       FD   FFTI03  RECORDING F BLOCK 0 RECORDS                         00001190
                    LABEL RECORD STANDARD.                              00001200
       01   FFTI03-ENR PIC X(400).                                      00001260
      *                                                                 00000540
       WORKING-STORAGE SECTION.                                         00001280
      *                                                                 00001290
          COPY SYBWDIV0.                                                00001340
      *                                                                 00001950
           02  ETAT-FREPRIS        PIC X VALUE '1'.                     00002040
               88 FIN-FREPRIS      VALUE '0'.                           00002050
               88 NON-FIN-FREPRIS  VALUE '1'.                           00002060
      *                                                                 00001950
           02  ETAT-FNM001         PIC X VALUE '1'.                     00002040
               88 FIN-FNM001       VALUE '0'.                           00002050
               88 NON-FIN-FNM001   VALUE '1'.                           00002060
      *                                                                 00001950
           02  ETAT-FFTI00         PIC X VALUE '1'.                     00002040
               88 FIN-FFTI00       VALUE '0'.                           00002050
               88 NON-FIN-FFTI00   VALUE '1'.                           00002060
      *                                                                 00001950
      *                                                                 00001950
           02  ETAT-FFTI02         PIC X VALUE '1'.                     00002040
               88 FIN-FFTI02       VALUE '0'.                           00002050
               88 NON-FIN-FFTI02   VALUE '1'.                           00002060
      *                                                                 00001950
           02  W-ERREUR       PIC X VALUE SPACE.                        00002830
           02  W-DATTRANS     PIC X(8).                                 00002830
      **                                                                00002430
           02  CLE-FNM001     PIC X(17) VALUE SPACES.                           
      *                                                                 00002800
      * ZONE DE RUPTURE                                                 00002840
      *                                                                 00002850
           02  SOC-PREC       PIC X(05).                                00002860
           02  MAG-PREC       PIC X(03).                                00002870
           02  NUMCAIS-PREC   PIC X(04).                                00002880
           02  DATTRANS-PREC  PIC X(08).                                00002890
      *                                                                 00002930
      * COMPTEURS                                                       00002940
      *                                                                 00002950
           02  WCPT-FNM001   PIC 9(06)  VALUE ZEROS.                    00002960
           02  WCPT-FFTI00   PIC 9(06)  VALUE ZEROS.                    00003020
           02  WCPT-FFTI02   PIC 9(06)  VALUE ZEROS.                    00003020
           02  WCPT-FNM002   PIC 9(06)  VALUE ZEROS.                    00002960
           02  WCPT-FNM003   PIC 9(06)  VALUE ZEROS.                    00002960
           02  WCPT-FFTI01   PIC 9(06)  VALUE ZEROS.                    00003020
           02  WCPT-FFTI03   PIC 9(06)  VALUE ZEROS.                    00003020
           02  WCPT-FREPRIS  PIC 9(06)  VALUE ZEROS.                    00003040
      *                                                                 00003500
      * INDICES                                                         00003530
      *                                                                 00003540
           02  I         PIC 9(4) VALUE 0.                              00003550
      *                                                                 00003710
       01  W-TABLEAU.                                                           
           02  TABLEAU-CAISSE   OCCURS 2000.                                    
             03  TAB-CLE          PIC X(17).                                    
      /                                                                 00004910
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00004920
      *                                                                 00004930
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00004940
      *               C O P Y   D E   C O N T R O L E                 * 00004950
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00004960
      *                                                                 00004970
           COPY  SYBWDATH.                                              00004980
      /                                                                 00004990
           COPY  SYBWDATE.                                              00005000
           COPY  ABENDCOP.                                              00005020
      *                                                                 00005030
      *                                                                 00005210
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005220
      *         ZONES     BUFFER      D'ENTREE   /  SORTIE            * 00005230
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005240
      *                                                                 00005250
       01  FILLER    PIC X(16)    VALUE   '*** Z-INOUT ****'.           00005260
       01  Z-INOUT   PIC X(4096)  VALUE  SPACE.                         00005270
      *                                                                 00005280
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005290
      *       ZONES     DE      GESTION     DES     ERREURS           * 00005300
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005310
      *                                                                 00005320
           COPY SYBWERRO.                                               00005330
      *                                                                 00005510
       LINKAGE SECTION.                                                 00005520
      *                                                                 00005530
       PROCEDURE DIVISION.                                              00005550
      *                                                                 00005530
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
      *                                                                 00006050
       MODULE-ENTREE SECTION.                                           00006020
      *                                                                 00006050
           MOVE '*** BNM001 EXECUTION STARTEE NORMALEMENT ***' TO MESS. 00006070
                    DISPLAY MESS.                                       00006080
           OPEN INPUT  FFTI00  FFTI02 FNM001 FREPRIS                    00006100
           OPEN OUTPUT FFTI01  FFTI03 FNM002 FNM003                     00006130
           SET NON-FIN-FREPRIS TO TRUE                                          
           SET NON-FIN-FNM001  TO TRUE                                          
           SET NON-FIN-FFTI00  TO TRUE                                          
           SET NON-FIN-FFTI02  TO TRUE                                          
           READ FREPRIS AT END                                                  
                DISPLAY 'FICHIER REPRISE VIDE'                                  
                SET FIN-FREPRIS TO TRUE                                         
           END-READ                                                     00006380
      *                                                                         
      * CHARGEMENT DU FICHIER DES CAISSES EN ERREUR EN TABLE INTERNE            
      *                                                                         
           INITIALIZE W-TABLEAU                                                 
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2000                         
                   OR FIN-FREPRIS                                               
              MOVE FREPRIS-CLE      TO TAB-CLE  (I)                             
              PERFORM LECTURE-FREPRIS                                           
           END-PERFORM.                                                         
           IF NON-FIN-FREPRIS                                                   
              DISPLAY 'TABLE INTERNE TROP PETITE'                               
              MOVE 'TABLE CAISSES REJETEES TROP PETITE' TO ABEND-MESS   S       
              PERFORM ABANDON-PROGRAMME                                         
           END-IF.                                                              
CB01  **   READ FNM001 AT END                                                   
CB01  **        DISPLAY 'FICHIER FNM001 VIDE'                                   
CB01  **        SET FIN-FNM001 TO TRUE                                          
CB01  **   END-READ.                                                    00006380
CB01       PERFORM LECTURE-FNM001.                                              
      *                                                                 00006520
       MODULE-TRAITEMENT SECTION.                                       00006530
      *                                                                 00025840
           PERFORM UNTIL FIN-FNM001                                             
              SET NON-TROUVE TO TRUE                                            
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2000                      
                 OR TROUVE OR TAB-CLE (I ) = SPACES                             
                 IF CLE-FNM001 = TAB-CLE (I)                                    
                    SET TROUVE TO TRUE                                          
                 END-IF                                                         
              END-PERFORM                                                       
              IF TROUVE                                                         
                 PERFORM ECRITURE-FNM003                                        
              ELSE                                                              
                 PERFORM ECRITURE-FNM002                                        
              END-IF                                                            
              PERFORM LECTURE-FNM001                                            
           END-PERFORM                                                          
      *                                                                 00025840
           READ FFTI00 AT END                                                   
                DISPLAY 'FICHIER FFTI00 VIDE'                                   
                SET FIN-FFTI00 TO TRUE                                          
           END-READ                                                     00006380
           PERFORM UNTIL FIN-FFTI00                                             
              SET NON-TROUVE TO TRUE                                            
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2000                      
                 OR TROUVE OR TAB-CLE (I ) = SPACES                             
                 IF FFTI00-CLE = TAB-CLE (I)                                    
                    SET TROUVE TO TRUE                                          
                 END-IF                                                         
              END-PERFORM                                                       
              IF NON-TROUVE                                                     
                 PERFORM ECRITURE-FFTI01                                        
              END-IF                                                            
              PERFORM LECTURE-FFTI00                                            
           END-PERFORM.                                                         
      *                                                                 00025840
           READ FFTI02 AT END                                                   
                DISPLAY 'FICHIER FFTI02 VIDE'                                   
                SET FIN-FFTI02 TO TRUE                                          
           END-READ                                                     00006380
           PERFORM UNTIL FIN-FFTI02                                             
              SET NON-TROUVE TO TRUE                                            
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2000                      
                 OR TROUVE OR TAB-CLE (I ) = SPACES                             
                 IF FFTI02-CLE = TAB-CLE (I)                                    
                    SET TROUVE TO TRUE                                          
                 END-IF                                                         
              END-PERFORM                                                       
              IF NON-TROUVE                                                     
                 PERFORM ECRITURE-FFTI03                                        
              END-IF                                                            
              PERFORM LECTURE-FFTI02                                            
           END-PERFORM.                                                         
      *                                                                 00025840
       MODULE-SORTIE SECTION.                                           00025850
      *                                                                 00025860
           MOVE '*** BNM001 EXECUTION TERMINEE NORMALEMENT **' TO MESS. 00026480
                    DISPLAY MESS.                                       00026490
           CLOSE FNM001 FNM002 FNM003 FFTI00 FFTI01 FFTI02 FFTI03       00025870
                 FREPRIS                                                00025880
           DISPLAY '      **  FICHIERS EN ENTREE **    '                00025920
           DISPLAY 'NBR ENREGIST. CAISSE ........... : ' WCPT-FNM001    00025920
           DISPLAY 'ENREGISTREMENT COMPTABLE........ : ' WCPT-FFTI00    00025980
           DISPLAY 'ENREGISTREMENT COMPTABLE REG DACEM:' WCPT-FFTI02    00025980
           DISPLAY 'NOMBRE DE CAISSE A REPRENDRE.... : ' WCPT-FREPRIS   00026000
           DISPLAY '      **  FICHIERS EN SORTIE **    '                00025920
           DISPLAY 'NBR ENREGIST. CAISSE A REPRENDRE : ' WCPT-FNM003    00025920
           DISPLAY 'NBR ENREGIST. CAISSE ........... : ' WCPT-FNM002    00025920
           DISPLAY 'ENREGISTREMENT COMPTABLE........ : ' WCPT-FFTI01    00025980
           DISPLAY 'ENREGISTREMENT COMPTABLE.REG DACEM:' WCPT-FFTI03    00025980
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00025840
       LECTURE-FREPRIS SECTION.                                         00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FREPRIS                                        06      
           READ FREPRIS AT END                                                  
                SET FIN-FREPRIS TO TRUE                                         
           END-READ.                                                    00006   
      *                                                                 00025840
       LECTURE-FNM001 SECTION.                                          00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FNM001                                                 
           READ FNM001 AT END                                                   
                SET FIN-FNM001 TO TRUE                                  00006370
           END-READ.                                                    00006380
           STRING FNM001-CLE-DATETR FNM001-CLE-NSOC FNM001-CLE-NLIEU            
                  FNM001-CLE-NCAISS DELIMITED BY SIZE INTO CLE-FNM001.          
      *                                                                 00025840
       LECTURE-FFTI00 SECTION.                                          00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FFTI00                                                 
           READ FFTI00 AT END                                                   
                SET FIN-FFTI00 TO TRUE                                  00006370
           END-READ.                                                    00006380
      *                                                                 00025840
      *                                                                 00025840
       LECTURE-FFTI02 SECTION.                                          00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FFTI02                                                 
           READ FFTI02 AT END                                                   
                SET FIN-FFTI02 TO TRUE                                  00006370
           END-READ.                                                    00006380
      *                                                                 00025840
       ECRITURE-FNM002 SECTION.                                         00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FNM002                                                 
           MOVE FNM001-ENR TO FNM002-ENR                                        
           WRITE FNM002-ENR.                                            00006370
      *                                                                 00025840
       ECRITURE-FNM003 SECTION.                                         00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FNM003                                                 
           MOVE FNM001-ENR TO FNM003-ENR                                        
           WRITE FNM003-ENR.                                            00006370
      *                                                                 00025840
       ECRITURE-FFTI01 SECTION.                                         00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FFTI01                                                 
           MOVE FFTI00-ENREG TO FFTI01-ENR                                      
           WRITE FFTI01-ENR.                                            00006370
      *                                                                         
      *                                                                 00025840
       ECRITURE-FFTI03 SECTION.                                         00025850
      *                                                                 00025860
           ADD 1 TO WCPT-FFTI03                                                 
           MOVE FFTI02-ENREG TO FFTI03-ENR                                      
           WRITE FFTI03-ENR.                                            00006370
      *                                                                         
      * ------------------------ FIN-ANORMALE ---------------------- *  03850005
                                                                        03860005
       ABANDON-PROGRAMME SECTION.                                       03870005
                                                                        03880005
           CLOSE FNM001 FFTI00 FREPRIS                                  03890011
                 FNM002 FFTI01 FFTI02 FFTI03                                    
           MOVE 'BNM001' TO ABEND-PROG.                                 03900005
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    03910005
                                                                        03920005
