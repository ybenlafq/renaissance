      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID         DIVISION.                                             00000020
       PROGRAM-ID.  BHE601.                                             00000040
       AUTHOR.   DSA045-CP.                                             00000050
       DATE-COMPILED.                                                   00000060
      ******************************************************************00000080
      * 11.06.2007                                                     *00000090
      * ==========                                                     *00000090
      *                                                                *00000090
      * EXTRACTION DU FICHIER SEQUENTIEL A PARTIR DU FICHIER VSAM :    *00000090
      *       -  ETATS BON ENVOI FOURNISSEUR (JHE601)                  *00000090
      *          EXTRACTIONS QUOTIDIENNES                              *00000090
      ******************************************************************00000080
      *                                                                 00000080
       ENVIRONMENT DIVISION.                                            00000150
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *****************************************************************         
      * FICHIERS EN ENTREE :                                          *         
      *****************************************************************         
      * FJHE60E : FICHIER D'EDITION BON D'ENVOI FOURNISSEUR ET        *         
      *           LISTE DE DESTOCKAGE                                 *         
      *****************************************************************         
           SELECT FJHE60E    ASSIGN   TO    FJHE60E                     00000250
                             ORGANIZATION   INDEXED                             
                             ACCESS MODE    DYNAMIC                             
                             RECORD KEY FJHE60E-CLE                             
                             FILE STATUS ST-FJHE60E.                            
      *                                                                 00000190
      *****************************************************************         
      * FDATE   : FICHIER DATE D'EXTRACTION                           *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN   TO     FDATE                      00000250
      *                      FILE STATUS  ST-FDATE.                             
      *--                                                                       
           SELECT FDATE      ASSIGN   TO     FDATE                              
                             FILE STATUS  ST-FDATE                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *****************************************************************         
      * FICHIERS CSV EN SORTIE :                                      *         
      *****************************************************************         
      * FJHE60S : FICHIER D'EDITION BON D'ENVOI FOURNISSEUR SEQUENTIEL*         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJHE60S    ASSIGN   TO    FJHE60S                     00000250
      *                      FILE STATUS ST-FJHE60S.                            
      *--                                                                       
           SELECT FJHE60S    ASSIGN   TO    FJHE60S                             
                             FILE STATUS ST-FJHE60S                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE  SECTION.                                                   00000290
      *                                                                 00000190
       FD  FDATE                                                        00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FDATE-ENREG               PIC X(80).                                 
      *                                                                 00000190
       FD  FJHE60E.                                                     00000440
       01  FJHE60E-ENREG.                                                       
           05  FJHE60E-CLE.                                                     
               10  FJHE60E-ETAT      PIC X(006).                                
               10  FJHE60E-DATE      PIC X(008).                                
               10  FJHE60E-USER      PIC X(008).                                
               10  FJHE60E-NSEQ      PIC 9(005).                                
           05  FJHE60E-RESTES        PIC X(223).                                
      *                                                                 00000190
       FD  FJHE60S                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FJHE60S-ENREG             PIC X(243).                                
      *                                                                         
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                         00000710
       77  ST-FDATE                  PIC X(02).                                 
       77  ST-FJHE60E                PIC X(02).                                 
       77  ST-FJHE60S                PIC X(02).                                 
       77  WRK-NBRLUS                PIC 9(05)  COMP-3.                         
       77  WRK-JHEECR                PIC 9(05)  COMP-3.                         
      *                                                                 00000600
       01  WRK-FLAGS                 PIC X(01)  VALUE SPACES.                   
           88  WRK-FINTRT            VALUE '1'.                                 
      *                                                                         
       01  WRK-DATEXTR.                                                         
           05  WRK-DATE-JJ           PIC X(02).                                 
           05  WRK-DATE-MM           PIC X(02).                                 
           05  WRK-DATE-SA.                                                     
               10  WS-DATE-SS        PIC X(02).                                 
               10  WS-DATE-AA        PIC X(02).                                 
      *                                                                 00000600
       01  WRK-PARAMETRE.                                                       
           05  WRK-CLEFIC.                                                      
               10  WRK-ETAT              PIC X(06)  VALUE 'JHE601'.             
               10  WRK-DATED             PIC X(08).                             
               10  FILLER                REDEFINES  WRK-DATED.                  
                   15  WRK-DAT-DEBSSAA.                                         
                       20  WRK-DAT-DEBSS PIC X(02).                             
                       20  WRK-DAT-DEBAA PIC X(02).                             
                   15  WRK-DAT-DEBMOIS   PIC X(02).                             
                   15  WRK-DAT-DEBJOUR   PIC X(02).                             
      *                                                                 00000600
           COPY SYKWHE60.                                                       
      *                                                                 00000600
      *                                                                 00000600
           COPY SWEFHE60.                                                       
      *                                                                 00000600
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
       TRT-GENERAL  SECTION.                                                    
           PERFORM  MODULE-ENTREE.                                              
           PERFORM  MODULE-INITIAL  UNTIL WRK-FINTRT.                           
           PERFORM  MODULE-SORTIE.                                              
           GOBACK.                                                              
       TRT-GENERAL-F. EXIT.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *    OUVERTURE DES FICHIERS FDATE FJHE60E FJHE60S                *        
      *----------------------------------------------------------------*        
       MODULE-ENTREE  SECTION.                                                  
           INITIALIZE  WRK-FLAGS.                                               
           MOVE ZEROS                TO WRK-JHEECR                              
                                        WRK-NBRLUS.                             
           OPEN  INPUT FDATE.                                                   
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FDATE  = ' ST-FDATE            
               GOBACK                                                           
           END-IF.                                                              
           OPEN  INPUT FJHE60E.                                                 
           IF  ST-FJHE60E        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE60E = ' ST-FJHE60E         
               CLOSE  FDATE                                                     
               GOBACK                                                           
           END-IF.                                                              
           OPEN OUTPUT FJHE60S.                                                 
           IF  ST-FJHE60S        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE60S = ' ST-FJHE60S         
               CLOSE  FDATE FJHE60E                                             
               GOBACK                                                           
           END-IF.                                                              
           ACCEPT WRK-DATED        FROM DATE YYYYMMDD.                          
      *---------------------------------------------------------                
      *--- LECTURE DU FICHIER DATE AAMMSSAA                                     
      *---------------------------------------------------------                
           READ FDATE    INTO WRK-DATEXTR.                                      
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY 'FICHIER FDATE VIDE'                                     
               DISPLAY 'DATE DU JOUR PRISE PAR DEFAUT'                          
               MOVE WRK-DAT-DEBSSAA  TO WRK-DATE-SA                             
               MOVE WRK-DAT-DEBMOIS  TO WRK-DATE-MM                             
               MOVE WRK-DAT-DEBJOUR  TO WRK-DATE-JJ                             
           ELSE                                                                 
               MOVE WRK-DATE-SA      TO WRK-DAT-DEBSSAA                         
               MOVE WRK-DATE-MM      TO WRK-DAT-DEBMOIS                         
               MOVE WRK-DATE-JJ      TO WRK-DAT-DEBJOUR                         
           END-IF.                                                              
           MOVE WRK-CLEFIC           TO FJHE60E-CLE.                            
      *--- SI CODE RETOUR=23 ----> PAS D'ENREGISTREMENT TROUVE                  
           START FJHE60E KEY IS      >  FJHE60E-CLE                             
               INVALID KEY                                                      
                   SET WRK-FINTRT    TO TRUE.                                   
           IF  NOT WRK-FINTRT                                                   
               PERFORM  MODULE-READN-FJHE60E                                    
           END-IF.                                                              
       MODULE-ENTREE-F. EXIT.                                                   
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *    CREATION DU FICHIER FJHE60S                                 *        
      *----------------------------------------------------------------*        
      *                                                                 00002950
       MODULE-INITIAL  SECTION.                                         00002330
           ADD 1                     TO WRK-NBRLUS.                             
           IF  WRK-ETAT              =  FJHE60E-ETAT  AND                       
               FJHE60E-DATE          =  WRK-DATED                               
               MOVE FJHE60E-ENREG    TO RBA-L10                                 
               PERFORM  MODULE-REMPL-FJHE60S                                    
           END-IF.                                                              
           IF FJHE60E-ETAT           >  WRK-ETAT                                
              SET WRK-FINTRT         TO TRUE                                    
           ELSE                                                                 
               PERFORM MODULE-READN-FJHE60E                                     
           END-IF.                                                              
       MODULE-INITIAL-F. EXIT.                                          00002620
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *      FERMETURE DU FICHIER                                      *        
      *----------------------------------------------------------------*        
       MODULE-SORTIE  SECTION.                                          00002330
           CLOSE  FDATE  FJHE60E  FJHE60S.                                      
           IF  ST-FDATE              = '00' AND                                 
               ST-FJHE60E            = '00' AND                                 
               ST-FJHE60S            = '00'                                     
               DISPLAY '* BHE601 : CREATION FJHE60S O.K ***'                    
           ELSE                                                                 
               DISPLAY '* BHE601 : PROBLEME FERMETURE FICHIERS '                
               DISPLAY '* ------   FICHIER FDATE  = ' ST-FDATE                  
               DISPLAY '*          FICHIER FJHE60E = ' ST-FJHE60E               
               DISPLAY '*          FICHIER FJHE60S = ' ST-FJHE60S               
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*          EXTRACTION ETAT = ' WRK-ETAT.                    
           DISPLAY '*          DATE DU         = ' WRK-DATEXTR.                 
           DISPLAY '*'.                                                         
           DISPLAY '*          NBR ENREG. LUS FJHE60E = ' WRK-NBRLUS.           
           DISPLAY '*          NBR ENREG. ECR FJHE60S = ' WRK-JHEECR.           
       MODULE-SORTIE-F. EXIT.                                           00002620
      *                                                                         
      *                                                                         
       MODULE-READN-FJHE60E SECTION.                                            
           READ FJHE60E NEXT.                                                   
           IF  ST-FJHE60E        NOT =  '00'                                    
                                 AND    '10'                                    
               DISPLAY '* BHE601 : PB LECTURE FJHE60E = ' ST-FJHE60E            
               CLOSE FDATE FJHE60E FJHE60S                                      
               GOBACK                                                           
           END-IF.                                                              
           IF  ST-FJHE60E            = '10'                                     
               SET WRK-FINTRT        TO TRUE                                    
           END-IF.                                                              
       MODULE-READN-FJHE60E-F. EXIT.                                            
      *                                                                         
      *                                                                         
       MODULE-REMPL-FJHE60S  SECTION.                                           
           MOVE WRK-ETAT             TO HE60-NOMETAT.                           
           MOVE WRK-DATED            TO HE60-DATED                              
      *                                 HE60-DATEF.                             
      *--- REUTILISATION DE LA ZONE DATEF                                       
           MOVE RBA-NUMSEQL          TO HE60-DATEF                              
           MOVE RBA-CENTRE           TO HE60-CENTRE.                            
           MOVE RBA-NDEM             TO HE60-NDEM.                              
           MOVE RBA-CTIERS           TO HE60-CTIERS.                            
           MOVE RBA-FAM              TO HE60-FAM.                               
           MOVE RBA-CODIC            TO HE60-CODIC.                             
           MOVE RBA-MARQ             TO HE60-MARQ.                              
           MOVE RBA-REF              TO HE60-REF.                               
           MOVE RBA-NDEP             TO HE60-NDEP.                              
           MOVE RBA-NHS              TO HE60-NHS.                               
           MOVE RBA-NTRAI            TO HE60-NTRAI.                             
           MOVE RBA-DVENTE           TO HE60-DVENTE.                            
           MOVE RBA-DDEP             TO HE60-DDEP.                              
           MOVE RBA-SERIE            TO HE60-SERIE.                             
           MOVE RBA-NACCORD          TO HE60-NACCORD.                           
           MOVE RBA-LPAN             TO HE60-LPAN.                              
           MOVE RBA-PVTTC-R          TO HE60-PVTTC-R.                           
           MOVE RBA-ORI              TO HE60-ORI.                               
           MOVE RBA-PRMP-R           TO HE60-PRMP-R.                            
           MOVE RBA-ECOP-R           TO HE60-ECOP-R.                            
           IF RBA-PDOSNASC  > ' '                                               
             MOVE RBA-PDOSNASC         TO HE60-PDOSNASC                         
             MOVE RBA-DOSNASC          TO HE60-DOSNASC                          
             MOVE '/'    TO HE60-SEP                                            
           ELSE                                                                 
             MOVE SPACES TO HE60-PDOSNASC                                       
             MOVE SPACES TO HE60-DOSNASC                                        
             MOVE SPACES TO HE60-SEP                                            
           END-IF.                                                              
           MOVE RBA-NEAN             TO HE60-NEAN.                              
           MOVE VSAM-NSEQ            TO HE60-NSEQ.                              
           WRITE FJHE60S-ENREG  FROM HE60-ENREG.                                
           IF  ST-FJHE60S        NOT =  '00'                                    
               DISPLAY '* BHE601 : PB ECRITURE FJHE60S = ' ST-FJHE60S           
               CLOSE FDATE FJHE60E FJHE60S                                      
               GOBACK                                                           
           END-IF.                                                              
           ADD 1                     TO WRK-JHEECR.                             
       MODULE-REMPL-FJHE60S-F. EXIT.                                            
      *                                                                 00002950
      *********** F I N  D E  'B H E 6 0 1' ****************************00002950
