      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       ID         DIVISION.                                             00000020
       PROGRAM-ID.  BHC011.                                             00000040
       AUTHOR.   DSA045-CP.                                             00000050
       DATE-COMPILED.                                                   00000060
      ******************************************************************00000080
      * 13.06.2007                                                     *00000090
      * ==========                                                     *00000090
      *                                                                *00000090
      * EXTRACTION DU FICHIER SEQUENTIEL A PARTIR DU FICHIER VSAM :    *00000090
      *       -  LISTE DE DESTOCKAGE         (JHC011)                  *00000090
      ******************************************************************00000080
      *                                                                 00000080
       ENVIRONMENT DIVISION.                                            00000150
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       CONFIGURATION SECTION.                                                   
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
                                                                                
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *****************************************************************         
      * FICHIERS EN ENTREE :                                          *         
      *****************************************************************         
      * FJHC01E : LISTE DE DESTOCKAGE                                 *         
      *****************************************************************         
           SELECT FJHC01E    ASSIGN   TO    FJHC01E                     00000250
                             ORGANIZATION   INDEXED                             
                             ACCESS MODE    DYNAMIC                             
                             RECORD KEY FJHC01E-CLE                             
                             FILE STATUS ST-FJHC01E.                            
      *                                                                 00000190
      *****************************************************************         
      * FDATE   : FICHIER DATE D'EXTRACTION                           *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN   TO     FDATE                      00000250
      *                      FILE STATUS  ST-FDATE.                             
      *--                                                                       
           SELECT FDATE      ASSIGN   TO     FDATE                              
                             FILE STATUS  ST-FDATE                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *****************************************************************         
      * FICHIERS CSV EN SORTIE :                                      *         
      *****************************************************************         
      * FJHC01S : FICHIER D'EDITION LISTE DE DESTOCKAGE               *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJHC01S    ASSIGN   TO    FJHC01S                     00000250
      *                      FILE STATUS ST-FJHC01S.                            
      *--                                                                       
           SELECT FJHC01S    ASSIGN   TO    FJHC01S                             
                             FILE STATUS ST-FJHC01S                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE  SECTION.                                                   00000290
      *                                                                 00000190
       FD  FDATE                                                        00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FDATE-ENREG               PIC X(80).                                 
      *                                                                 00000190
       FD  FJHC01E.                                                     00000440
       01  FJHC01E-ENREG.                                                       
           05  FJHC01E-CLE.                                                     
               10  FJHC01E-ETAT      PIC X(006).                                
               10  FJHC01E-DATE      PIC X(008).                                
               10  FJHC01E-USER      PIC X(008).                                
               10  FJHC01E-NSEQ      PIC 9(005).                                
           05  FJHC01E-RESTES        PIC X(223).                                
      *                                                                 00000190
       FD  FJHC01S                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FJHC01S-ENREG             PIC X(210).                                
      *                                                                         
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                         00000710
       77  ST-FDATE                  PIC X(02).                                 
       77  ST-FJHC01E                PIC X(02).                                 
       77  ST-FJHC01S                PIC X(02).                                 
       77  WRK-NBRLUS                PIC 9(05)  COMP-3.                         
       77  WRK-JHCECR                PIC 9(05)  COMP-3.                         
      *                                                                 00000600
       01  WRK-FLAGS                 PIC X(01)  VALUE SPACES.                   
           88  WRK-FINTRT            VALUE '1'.                                 
      *                                                                         
       01  WRK-DATEXTR.                                                         
           05  WRK-DATE-JJ           PIC X(02).                                 
           05  WRK-DATE-MM           PIC X(02).                                 
           05  WRK-DATE-SA.                                                     
               10  WS-DATE-SS        PIC X(02).                                 
               10  WS-DATE-AA        PIC X(02).                                 
      *                                                                 00000600
       01  WRK-PARAMETRE.                                                       
           05  WRK-CLEFIC.                                                      
               10  WRK-ETAT              PIC X(06)  VALUE 'JHC011'.             
               10  WRK-DATED             PIC X(08).                             
               10  FILLER                REDEFINES  WRK-DATED.                  
                   15  WRK-DAT-DEBSSAA.                                         
                       20  WRK-DAT-DEBSS PIC X(02).                             
                       20  WRK-DAT-DEBAA PIC X(02).                             
                   15  WRK-DAT-DEBMOIS   PIC X(02).                             
                   15  WRK-DAT-DEBJOUR   PIC X(02).                             
      *                                                                 00000600
           COPY SYKWHC06.                                                       
      *                                                                 00000600
      *                                                                 00000600
           COPY SWEFHC06.                                                       
      *                                                                 00000600
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
       TRT-GENERAL  SECTION.                                                    
           PERFORM  MODULE-ENTREE.                                              
           PERFORM  MODULE-INITIAL  UNTIL WRK-FINTRT.                           
           PERFORM  MODULE-SORTIE.                                              
           GOBACK.                                                              
       TRT-GENERAL-F. EXIT.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *    OUVERTURE DES FICHIERS FDATE FJHC01E FJHC01S                *        
      *----------------------------------------------------------------*        
       MODULE-ENTREE  SECTION.                                                  
           INITIALIZE  WRK-FLAGS.                                               
           MOVE ZEROS                TO WRK-JHCECR                              
                                        WRK-NBRLUS.                             
           OPEN  INPUT FDATE.                                                   
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FDATE  = ' ST-FDATE            
               GOBACK                                                           
           END-IF.                                                              
           OPEN  INPUT FJHC01E.                                                 
           IF  ST-FJHC01E        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHC01E = ' ST-FJHC01E         
               CLOSE  FDATE                                                     
               GOBACK                                                           
           END-IF.                                                              
           OPEN OUTPUT FJHC01S.                                                 
           IF  ST-FJHC01S        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHC01S = ' ST-FJHC01S         
               CLOSE  FDATE FJHC01E                                             
               GOBACK                                                           
           END-IF.                                                              
           ACCEPT WRK-DATED        FROM DATE YYYYMMDD.                          
      *---------------------------------------------------------                
      *--- LECTURE DU FICHIER DATE AAMMSSAA                                     
      *---------------------------------------------------------                
           READ FDATE    INTO WRK-DATEXTR.                                      
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY 'FICHIER FDATE VIDE'                                     
               DISPLAY 'DATE DU JOUR PRISE PAR DEFAUT'                          
               MOVE WRK-DAT-DEBSSAA  TO WRK-DATE-SA                             
               MOVE WRK-DAT-DEBMOIS  TO WRK-DATE-MM                             
               MOVE WRK-DAT-DEBJOUR  TO WRK-DATE-JJ                             
           ELSE                                                                 
               MOVE WRK-DATE-SA      TO WRK-DAT-DEBSSAA                         
               MOVE WRK-DATE-MM      TO WRK-DAT-DEBMOIS                         
               MOVE WRK-DATE-JJ      TO WRK-DAT-DEBJOUR                         
           END-IF.                                                              
           MOVE WRK-CLEFIC           TO FJHC01E-CLE.                            
      *--- SI CODE RETOUR=23 ----> PAS D'ENREGISTREMENT TROUVE                  
           START FJHC01E KEY IS      >  FJHC01E-CLE                             
               INVALID KEY                                                      
                   SET WRK-FINTRT    TO TRUE.                                   
           IF  NOT WRK-FINTRT                                                   
               PERFORM  MODULE-READN-FJHC01E                                    
           END-IF.                                                              
       MODULE-ENTREE-F. EXIT.                                                   
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *    CREATION DU FICHIER FJHC01S                                 *        
      *----------------------------------------------------------------*        
      *                                                                 00002950
       MODULE-INITIAL  SECTION.                                         00002330
           ADD 1                     TO WRK-NBRLUS.                             
           IF  WRK-ETAT              =  FJHC01E-ETAT  AND                       
               FJHC01E-DATE          =  WRK-DATED                               
               MOVE FJHC01E-ENREG    TO RBA-D10                                 
               PERFORM  MODULE-REMPL-FJHC01S                                    
           END-IF.                                                              
           IF FJHC01E-ETAT           >  WRK-ETAT                                
              SET WRK-FINTRT         TO TRUE                                    
           ELSE                                                                 
               PERFORM MODULE-READN-FJHC01E                                     
           END-IF.                                                              
       MODULE-INITIAL-F. EXIT.                                          00002620
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *      FERMETURE DU FICHIER                                      *        
      *----------------------------------------------------------------*        
       MODULE-SORTIE  SECTION.                                          00002330
           CLOSE  FDATE  FJHC01E  FJHC01S.                                      
           IF  ST-FDATE              = '00' AND                                 
               ST-FJHC01E            = '00' AND                                 
               ST-FJHC01S            = '00'                                     
               DISPLAY '* BHC011 : CREATION FJHC01S O.K ***'                    
           ELSE                                                                 
               DISPLAY '* BHC011 : PROBLEME FERMETURE FICHIERS '                
               DISPLAY '* ------   FICHIER FDATE  = ' ST-FDATE                  
               DISPLAY '*          FICHIER FJHC01E = ' ST-FJHC01E               
               DISPLAY '*          FICHIER FJHC01S = ' ST-FJHC01S               
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*          EXTRACTION ETAT = ' WRK-ETAT.                    
           DISPLAY '*          DATE DU         = ' WRK-DATEXTR.                 
           DISPLAY '*'.                                                         
           DISPLAY '*          NBR ENREG. LUS FJHC01E = ' WRK-NBRLUS.           
           DISPLAY '*          NBR ENREG. ECR FJHC01S = ' WRK-JHCECR.           
       MODULE-SORTIE-F. EXIT.                                           00002620
      *                                                                         
      *                                                                         
       MODULE-READN-FJHC01E SECTION.                                            
           READ FJHC01E NEXT.                                                   
           IF  ST-FJHC01E        NOT =  '00'                                    
                                 AND    '10'                                    
               DISPLAY '* BHC011 : PB LECTURE FJHC01E = ' ST-FJHC01E            
               CLOSE FDATE FJHC01E FJHC01S                                      
               GOBACK                                                           
           END-IF.                                                              
           IF  ST-FJHC01E            = '10'                                     
               SET WRK-FINTRT        TO TRUE                                    
           END-IF.                                                              
       MODULE-READN-FJHC01E-F. EXIT.                                            
      *                                                                         
      *                                                                         
       MODULE-REMPL-FJHC01S  SECTION.                                           
           MOVE WRK-ETAT             TO HC06-NOMETAT.                           
           MOVE WRK-DATED            TO HC06-DATED                              
                                        HC06-DATEF.                             
           MOVE RBA-D10-CTIERS       TO HC06-CTIERS.                            
           MOVE RBA-D10-NENVOI       TO HC06-NENVOI.                            
           MOVE RBA-D10-DENVOI       TO HC06-DENVOI.                            
           MOVE RBA-D10-ADRESSE      TO HC06-ADRESSE.                           
           MOVE RBA-D10-NCODIC       TO HC06-NCODIC.                            
           MOVE RBA-D10-QTE          TO HC06-QTE.                               
           MOVE RBA-D10-CFAM         TO HC06-CFAM.                              
           MOVE RBA-D10-CMARQ        TO HC06-CMARQ.                             
           MOVE RBA-D10-LREF         TO HC06-LREF.                              
           MOVE RBA-D10-CSERIE       TO HC06-CSERIE.                            
           MOVE RBA-D10-NLIEUHC      TO HC06-NLIEUHC.                           
           MOVE RBA-D10-NCHS         TO HC06-NCHS.                              
           MOVE RBA-D10-NACCORD      TO HC06-NACCORD.                           
           MOVE RBA-D10-DACCFOUR     TO HC06-DACCFOUR.                          
           MOVE RBA-D10-INTERLOC     TO HC06-INTERLOC.                          
           MOVE RBA-D10-PVTTC-R      TO HC06-PVTTC-R.                           
           MOVE RBA-D10-PRMP-R       TO HC06-PRMP-R.                            
           MOVE RBA-D10-ECOP-R       TO HC06-ECOP-R.                            
           WRITE FJHC01S-ENREG  FROM HC06-ENREG.                                
           IF  ST-FJHC01S        NOT =  '00'                                    
               DISPLAY '* BHC011 : PB ECRITURE FJHC01S = ' ST-FJHC01S           
               CLOSE FDATE FJHC01E FJHC01S                                      
               GOBACK                                                           
           END-IF.                                                              
           ADD 1                     TO WRK-JHCECR.                             
       MODULE-REMPL-FJHC01S-F. EXIT.                                            
      *                                                                 00002950
      *********** F I N  D E  'B H C 0 1 1' ****************************00002950
