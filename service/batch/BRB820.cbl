      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
      *{ MW DAR-168                                                             
      *  PROGRAM-ID.    BRB810.                                                 
        PROGRAM-ID.  BRB820.                                                    
      * }                                                                       
      *                                                                         
      *****************************************************************         
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *         
      *****************************************************************         
      *                                                               *         
      *  PROJET     : DGSA                                            *         
      *  PROGRAMME  : BRB810                                          *         
      *                                                               *         
      *  FONCTION   : VENTE AVEC PRESTATION ENERGIE                   *         
      *                                                               *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FEXTRACE ASSIGN TO FEXTRACE                                   
      *       FILE STATUS IS FEXTRACE-CODRET.                                   
      *--                                                                       
           SELECT FEXTRACE ASSIGN TO FEXTRACE                                   
              FILE STATUS IS FEXTRACE-CODRET                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FEXTRACS ASSIGN TO FEXTRACS.                                  
      *--                                                                       
           SELECT FEXTRACS ASSIGN TO FEXTRACS                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FEXTRACS                                                             
           LABEL RECORD STANDARD                                                
           BLOCK 0                                                              
           RECORDING MODE IS F.                                                 
       01  ENR-FEXTRACS.                                                        
           05 FILLER               PIC X(250).                                  
      *                                                                         
       FD  FEXTRACE                                                             
           LABEL RECORD STANDARD                                                
           BLOCK 0                                                              
           RECORDING MODE IS F.                                                 
       01  ENR-FEXTRACE.                                                        
            10 W-GV11-NSOCIETE         PIC X(03).                               
            10 W-GV11-NLIEU            PIC X(03).                               
            10 W-GV11-NVENTE           PIC X(07).                               
            10 W-GV08-VALCTRL          PIC X(50).                               
            10 FILLER                  PIC X(01).                               
            10 W-GV11-NSEQNQ           PIC S9(5)V USAGE COMP-3.                 
            10 W-GV10-DVENTE           PIC X(08).                               
            10 W-GV11-CENREG           PIC X(05).                               
            10 W-GV11-DANNULATION      PIC X(08).                               
            10 W-GV11-CMODDEL          PIC X(03).                               
            10 W-GV11-CVENDEUR         PIC X(06).                               
            10 W-GV31-LVENDEUR         PIC X(15).                               
            10 W-GV02-LPRENOM          PIC X(15).                               
            10 W-GV02-LNOM             PIC X(25).                               
            10 W-GV02-CPOSTAL          PIC X(05).                               
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                               *         
      *****************************************************************         
      *                                                                         
       01  Z-COMMAREA-LINK     PIC X(200).                                      
           COPY  SYKWDIV0.                                                      
      *                                                               *         
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                               *         
       01  VARIABLES-NUM.                                                       
           05  CPT-FEXTRACS         PIC 9(06) VALUE 0.                          
           05  CPT-FEXTRACE             PIC 9(06) VALUE 0.                      
      *                                                                         
        01 WS-FIN-FEXTRACTE               PIC X(01) VALUE '0'.                  
           88 FIN-FEXTRACT-KO               VALUE '0'.                          
           88 FIN-FEXTRACT                  VALUE '1'.                          
        77 FEXTRACE-CODRET                PIC X(02)  VALUE SPACES.              
      *                                                               *         
       01  EDT-COMPTEUR         PIC -B---B--9.                                  
      *                                                               *         
      *****************************************************************         
      *   DESCRIPTION DES LIGNES D'EDITION                            *         
      *****************************************************************         
      *                                                               *         
       01 E-TITRE1.                                                             
           05  E-T01.                                                           
               10  FILLER                         PIC X(06) VALUE               
                  '"STE";'.                                                     
               10  FILLER                         PIC X(07) VALUE               
                  '"LIEU";'.                                                    
               10  FILLER                         PIC X(10) VALUE               
                  '"N�VENTE";'.                                                 
               10  FILLER                         PIC X(10) VALUE               
                  '"CONTRAT";'.                                                 
               10  FILLER                         PIC X(12) VALUE               
                  '"NUM LIGNE";'.                                               
               10  FILLER                         PIC X(13) VALUE               
                  '"DATE VENTE";'.                                              
               10  FILLER                         PIC X(09) VALUE               
                   '"PRESTA";'.                                                 
               10  FILLER                         PIC X(14) VALUE               
                   '"DATE ANNUL.";'.                                            
               10  FILLER                         PIC X(15) VALUE               
                   '"MODE DE DEL.";'.                                           
               10  FILLER                         PIC X(15) VALUE               
                   '"CODE VENDEUR";'.                                           
               10  FILLER                         PIC X(10) VALUE               
                   '"VENDEUR";'.                                                
               10  FILLER                         PIC X(16) VALUE               
                   '"PRENOM CLIENT";'.                                          
               10  FILLER                         PIC X(13) VALUE               
                   '"NOM CLIENT";'.                                             
               10  FILLER                         PIC X(13) VALUE               
                   '"CODE POSTAL"'.                                             
      *                                                                         
       01 E-DETAIL.                                                             
            10 FILLER                  PIC X(01) VALUE '"'.                     
            10 E-GV11-NSOCIETE         PIC X(03).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-NLIEU            PIC X(03).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-NVENTE           PIC X(07).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV08-VALCTRL          PIC X(50).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-NSEQNQ           PIC 99999.                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV10-DVENTE.                                                   
              15 E-GV10-DVENTE-JJ      PIC X(02).                               
              15 FILLER                PIC X(01) VALUE '.'.                     
              15 E-GV10-DVENTE-MM      PIC X(02).                               
              15 FILLER                PIC X(01) VALUE '.'.                     
              15 E-GV10-DVENTE-SSAA    PIC X(04).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-CENREG           PIC X(05).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-DANNULATION.                                              
              15 E-GV11-DANNULATION-JJ   PIC X(02).                             
              15 E-GV11-DANNULATION-F1   PIC X(01) VALUE '.'.                   
              15 E-GV11-DANNULATION-MM   PIC X(02).                             
              15 E-GV11-DANNULATION-F2   PIC X(01) VALUE '.'.                   
              15 E-GV11-DANNULATION-SSAA PIC X(04).                             
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-CMODDEL          PIC X(03).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV11-CVENDEUR         PIC X(06).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV31-LVENDEUR         PIC X(15).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV02-LPRENOM          PIC X(15).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV02-LNOM             PIC X(25).                               
            10 FILLER                  PIC X(03) VALUE '";"'.                   
            10 E-GV02-CPOSTAL          PIC X(05).                               
            10 FILLER                  PIC X(01) VALUE '"'.                     
      *                                                                         
      * -AIDA *********************************************************         
      *  ZONES DE GESTION DES FICHIERS                                          
      *****************************************************************         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       PROGRAMME-BRB810 SECTION.                                                
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      *                                                                         
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FEXTRACT                         
      *                                                                         
           PERFORM MODULE-SORTIE.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
      *===============================================================*         
       MODULE-ENTREE SECTION.                                                   
      *===============================================================*         
           DISPLAY ' *-*-*-*-*-*'                                               
           DISPLAY ' B R B 8 1 0'                                               
           DISPLAY ' *-*-*-*-*-*'                                               
           DISPLAY '            '                                               
      *                                                                         
      *                                                                         
      *                                                                         
           OPEN INPUT FEXTRACE                                                  
           OPEN OUTPUT FEXTRACS                                                 
      *                                                                         
           WRITE ENR-FEXTRACS FROM E-TITRE1                                     
           ADD 1   TO   CPT-FEXTRACS.                                           
           PERFORM READ-FEXTRACE.                                               
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * MODULE DE TRAITEMENT                                          *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       MODULE-TRAITEMENT SECTION.                                               
      *                                                                         
           MOVE W-GV11-NSOCIETE         TO E-GV11-NSOCIETE                      
           MOVE W-GV11-NLIEU            TO E-GV11-NLIEU                         
           MOVE W-GV11-NVENTE           TO E-GV11-NVENTE                        
           INSPECT W-GV08-VALCTRL REPLACING ALL LOW-VALUE BY SPACE.             
           MOVE W-GV08-VALCTRL          TO E-GV08-VALCTRL                       
           MOVE W-GV11-NSEQNQ           TO E-GV11-NSEQNQ                        
           MOVE W-GV10-DVENTE(7:2)      TO E-GV10-DVENTE-JJ                     
           MOVE W-GV10-DVENTE(5:2)      TO E-GV10-DVENTE-MM                     
           MOVE W-GV10-DVENTE(1:4)      TO E-GV10-DVENTE-SSAA                   
           MOVE W-GV11-CENREG           TO E-GV11-CENREG                        
           IF W-GV11-DANNULATION > ' '                                          
             MOVE W-GV11-DANNULATION(7:2) TO E-GV11-DANNULATION-JJ              
             MOVE '.'              TO E-GV11-DANNULATION-F1                     
                                      E-GV11-DANNULATION-F2                     
             MOVE W-GV11-DANNULATION(5:2) TO E-GV11-DANNULATION-MM              
             MOVE W-GV11-DANNULATION(1:4) TO E-GV11-DANNULATION-SSAA            
           ELSE                                                                 
             MOVE SPACES                TO E-GV11-DANNULATION                   
           END-IF                                                               
           MOVE W-GV11-CMODDEL          TO E-GV11-CMODDEL                       
           MOVE W-GV11-CVENDEUR         TO E-GV11-CVENDEUR                      
           MOVE W-GV31-LVENDEUR         TO E-GV31-LVENDEUR                      
           MOVE W-GV02-LPRENOM          TO E-GV02-LPRENOM                       
           MOVE W-GV02-LNOM             TO E-GV02-LNOM                          
           MOVE W-GV02-CPOSTAL          TO E-GV02-CPOSTAL                       
      *                                                                         
           WRITE ENR-FEXTRACS FROM E-DETAIL                                     
           ADD 1   TO   CPT-FEXTRACS                                            
           PERFORM READ-FEXTRACE                                                
           .                                                                    
      *                                                                         
      *                                                                 00000000
      *                                                                         
       MODULE-SORTIE SECTION.                                                   
      *                                                                         
           CLOSE FEXTRACS                                                       
      *                                                                         
           MOVE CPT-FEXTRACE   TO EDT-COMPTEUR                                  
           DISPLAY ' NBRE LECTURE   : ' EDT-COMPTEUR                            
           MOVE CPT-FEXTRACS   TO EDT-COMPTEUR                                  
           DISPLAY ' NBRE ECRITURE  : ' EDT-COMPTEUR                            
           DISPLAY '*----------------------------------------------*'           
           DISPLAY '*       BRB810 S''EST TERMINE NORMALEMENT      *'           
           DISPLAY '*----------------------------------------------*'.          
      *                                                                         
      *                                                                         
      *                                                                         
       READ-FEXTRACE SECTION.                                                   
           READ FEXTRACE          AT END                                        
                SET FIN-FEXTRACT  TO TRUE                                       
                        NOT AT END                                              
                ADD  1             TO CPT-FEXTRACE                              
           END-READ.                                                            
      *----------------------------------------------------------------*        
