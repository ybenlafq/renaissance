      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BRB210.                                           00000040
       AUTHOR.        DSA015-EC.                                        00000050
       DATE-WRITTEN.  16/04/2010.                                       00000060
      *----------------------------------------------------------------*00000080
      * EXTRACTION DES INTER DE TYPE STSB4 POUR INTEGRATION DANS ABEL  *00000090
      * AU FORMAT DES FAS                                              *        
      *----------------------------------------------------------------*00000080
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN TO FDATE.                           00000250
      *--                                                                       
           SELECT FDATE      ASSIGN TO FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB210I    ASSIGN TO FRB210I                          00000250
      *                      FILE STATUS ST-FRB210I.                            
      *--                                                                       
           SELECT FRB210I    ASSIGN TO FRB210I                                  
                             FILE STATUS ST-FRB210I                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB210O    ASSIGN TO FRB210O                          00000250
      *                      FILE STATUS ST-FRB210O.                            
      *--                                                                       
           SELECT FRB210O    ASSIGN TO FRB210O                                  
                             FILE STATUS ST-FRB210O                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *                                                                 00000600
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FDATE.                                                          
            03  FILLER      PIC X(80).                                          
      *                                                                 00000480
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FRB210I                                                      00000440
           RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
           RECORD CONTAINS 1 TO 500 CHARACTERS                          00000470
           DATA RECORD IS FRB210I-ENR.                                  00000470
       01  FRB210I-ENR.                                                 00000480
           10  FRB210I-ENREG          PICTURE X(01)                     00000480
                                      OCCURS 1 TO 500 TIMES             00000480
                                      DEPENDING ON I-LONG.                      
      *                                                                 00000480
       FD   FRB210O                                                             
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   FRB210O-RECORD.                                                     
      *** DESCRIPTION DU FICHIER ISSU DE LA FACTURATION COMPLETEL               
            03   FILLER                    PIC X(105).                          
      *                                                                         
      *                                                                 00000430
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       77  STRING-FRBXXX              PIC X(130) VALUE SPACES.                  
       77  ST-FRB210O                 PIC 9(02) VALUE ZERO.                     
       77  ST-FRB210I                 PIC 9(02) VALUE ZERO.                     
       01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
       01  WW-FRB210I                 PIC X(500).                               
       01  WS-WTABLEG                 PIC X(80)           VALUE SPACES.         
      *    COPIES D'ABEND ET DE CONTROLE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
                                                                        00001910
        01 ENREG-FRB210I.                                               00001940
      *    NOM SOUS-TRAITANT                                                    
            05 FRB210I-NOM-SOUS-TRAITANT PIC X(20).                             
      *    SAV FOURNI PAR DARTY                                                 
            05 FRB210I-SAV-SOUS-TRAITANT PIC X(06).                             
      *    NUM�RO INTER FOURNI PAR DARTY                                        
            05 FRB210I-NINTER           PIC X(08).                              
      *    NUM�RO CONTRAT FOURNI PAR DARTY                                      
            05 FRB210I-NCONTREDBOX      PIC X(18).                              
      *    DATE INTER FOURNIE PAR DARTY                                         
            05 FRB210I-DINTER           PIC X(08).                              
      *    NOM CLIENT                                                           
            05 FRB210I-LNOM             PIC X(20).                              
      *    PR�NOM CLIENT                                                        
            05 FRB210I-LPRENOM          PIC X(15).                              
      *    CODE TOPAGE                                                          
            05 FRB210I-CTOPAGE          PIC X(05).                              
      *    PRIX UNITAIRE EN �                                                   
            05 FRB210I-PINTER           PIC X(8).                               
      *    TYPE DE RACCORDEMENT                                                 
            05 FRB210I-LRACCORDEMENT    PIC X(20).                              
      *    POSITIONNEMENT                                                       
            05 FRB210I-LPOSITIONNEMENT  PIC X(20).                              
      *    TYPE DE LOGEMENT                                                     
            05 FRB210I-LOGEMENT         PIC X(20).                              
      *    LIBELLE D ERREUR                                                     
            05 FRB210I-LERREUR          PIC X(50).                              
      *** DESCRIPTION DU FICHIER DE SORTIE                                      
           05 ENREG-FRB210O.                                                    
             10 FILLER            PIC X(01) VALUE SPACE.                        
      *      CONTRAT REDBOX                                                     
             10 FRB210O-IDCONTRAT  PIC X(18).                                   
             10 FILLER            PIC X(03) VALUE SPACE.                        
      *      DATE PRISE EN COMPTE POUR LA CREATION DU FAS                       
             10 FRB210O-DTACTI-FACT.                                            
                15 FRB210O-DTACTI-FACT-JJ     PIC X(02).                        
                15 FRB210O-DTACTI-FACT-FIL1   PIC X(01).                        
                15 FRB210O-DTACTI-FACT-MM     PIC X(02).                        
                15 FRB210O-DTACTI-FACT-FIL2   PIC X(01).                        
                15 FRB210O-DTACTI-FACT-SSAA   PIC X(04).                        
             10 FILLER            PIC X(03) VALUE SPACE.                        
      *      TYPE DE FAS                                                        
             10 FRB210O-TYPE-FAS-FACT    PIC X(04).                             
             10 FILLER            PIC X(03) VALUE SPACE.                        
      *      LIBELLE DU FAS                                                     
             10 FRB210O-LIB-FAS-FACT     PIC X(50).                             
             10 FILLER            PIC X(03) VALUE SPACE.                        
      *      VALEUR HT DE L'IMMOBILISATION DU FAS                               
             10 FRB210O-MT-FAS-FACT      PIC X(06).                             
             10 FILLER            PIC X(03) VALUE SPACE.                        
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
       01  WS-MONTANT                 PIC ----9,99.                             
      *    R�CUP�RATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
        01  STATUT-FRB210I                 PIC  X(02).                          
      *{ remove-comma-in-dde 1.5                                                
      *     88  STATUT-FRB210I-OK                  VALUE '00' , '  '.           
      *--                                                                       
            88  STATUT-FRB210I-OK                  VALUE '00'   '  '.           
      *}                                                                        
        01  STATUT-FRB210O                 PIC  X(02).                          
      *{ remove-comma-in-dde 1.5                                                
      *     88  STATUT-FRB210O-OK                  VALUE '00' , '  '.           
      *--                                                                       
            88  STATUT-FRB210O-OK                  VALUE '00'   '  '.           
      *}                                                                        
       01  WS-CPT-ECRIS               PIC S9(07) COMP-3   VALUE 0.              
       01  WS-NBR-LUS-FRB210I         PIC S9(07) COMP-3   VALUE 0.              
       01 WS-FRB210I-PINTER-ENT           PIC X(5).                             
       01 FRB210I-PINTERN                 PIC --9,99.                           
       01 WS-FRB210I-PINTER REDEFINES FRB210I-PINTERN.                          
              10 FRB210I-PINTER-ENT       PIC X(3) JUST RIGHT.                  
              10 FRB210I-PINTER-VIRG      PIC X(1).                             
              10 FRB210I-PINTER-DEC       PIC X(2).                             
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FRB210I            PIC 9(01)           VALUE 0.              
           88 FIN-FRB210I                                 VALUE 1.              
       01  FDATE-ENREG.                                                         
            02  FDAT.                                                           
                04  FDATE-JJ   PIC X(02).                                       
                04  FDATE-MM   PIC X(02).                                       
                04  FDATE-SS   PIC X(02).                                       
                04  FDATE-AA   PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
      *                                                                         
       01  FDATE-TRANS.                                                         
                 04  TRANS-SS   PIC X(02).                                      
                 04  TRANS-AA   PIC X(02).                                      
                 04  TRANS-MM   PIC X(02).                                      
                 04  TRANS-JJ   PIC X(02).                                      
       01  IND-MKG             PIC 9(5).                                        
       01  I                   PIC 9(5).                                        
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BRB210O      SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    on saute l'entete                                            00001900
      *    LECTURE DES FICHIERS EN ENTR�E.                              00001900
           PERFORM  READ-FRB210I .                                              
      *                                                                         
           PERFORM UNTIL FIN-FRB210I                                            
                 IF FRB210I-CTOPAGE = 'STSB4'                                   
                    PERFORM WRITE-FRB210O                                       
                 END-IF                                                         
                 PERFORM READ-FRB210I                                           
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BRB210O.                                                
      *                                                                         
       FIN-MODULE-BRB210O. EXIT.                                                
              EJECT                                                     00002620
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! EXTRACTION DES INTER STSB4                   !'  00002400
           DISPLAY  '! ET MISE AU FORMAT DES FAS                    !'  00002400
           DISPLAY  '! POUR INTEGRATION DANS ABEL                   !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 16/04/2010 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTR�E.                                      
           OPEN  OUTPUT FRB210O                                         00002950
           OPEN  INPUT  FRB210I FDATE                                   00002950
      *                                                                         
           READ   FDATE  INTO   FDATE-ENREG   AT END                            
                MOVE  ' FDATE ! DATE DU JOUR : JJMMSSAA '                       
                                       TO   ABEND-MESS                          
                MOVE  01                     TO   ABEND-CODE                    
           PERFORM  FIN-ANORMALE.                                               
           MOVE '1' TO GFDATA.                                                  
           MOVE FDATE-JJ  TO GFJOUR.                                            
           MOVE FDATE-MM  TO GFMOIS.                                            
           MOVE FDATE-SS  TO GFSIECLE.                                          
           MOVE FDATE-AA  TO GFANNEE.                                           
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT = 0                                                        
              MOVE 'DATE INCORRECTE  ' TO ABEND-MESS                            
              MOVE  02                     TO   ABEND-CODE                      
              PERFORM  FIN-ANORMALE                                             
           END-IF.                                                              
           MOVE '3' TO GFDATA.                                                  
           ADD 1 TO GFQNT0.                                                     
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT = 0                                                        
              MOVE 'DATE INCORRECTE  ' TO ABEND-MESS                            
              MOVE  02                     TO   ABEND-CODE                      
              PERFORM  FIN-ANORMALE                                             
           END-IF.                                                              
           DISPLAY 'DATE PRISE EN COMPTE : ' GFSAMJ-0                           
           INITIALIZE STATUT-FRB210I STATUT-FRB210O .                           
           PERFORM LECTURE-DSYST.                                               
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BRB210O SECTION.                                             00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS-FRB210I00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS TRAITES : ' WS-CPT-ECRIS      00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FRB210O FRB210I .                                             
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRB210O. EXIT.                                           00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FRB210I FRB210O .                                             
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB210' TO    ABEND-PROG                              00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FRB210I SECTION.                                                    
       READ-AGAIN.                                                              
           INITIALIZE ENREG-FRB210I                                             
           READ FRB210I INTO   WW-FRB210I                                       
                AT END     SET FIN-FRB210I    TO TRUE                           
                NOT AT END ADD 1  TO WS-NBR-LUS-FRB210I                         
                UNSTRING WW-FRB210I    DELIMITED BY     ';'                     
                         INTO                                                   
                  FRB210I-NOM-SOUS-TRAITANT                                     
                  FRB210I-SAV-SOUS-TRAITANT                                     
                  FRB210I-NINTER                                                
                  FRB210I-NCONTREDBOX                                           
                  FRB210I-DINTER                                                
                  FRB210I-LNOM                                                  
                  FRB210I-LPRENOM                                               
                  FRB210I-CTOPAGE                                               
                  FRB210I-PINTER                                                
                  FRB210I-LRACCORDEMENT                                         
                  FRB210I-LPOSITIONNEMENT                                       
                  FRB210I-LOGEMENT                                              
           IF NOT STATUT-FRB210I-OK                                     59350001
              DISPLAY 'PB LECTURE  FRB210I ' STATUT-FRB210I             59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           .                                                                    
       FIN-READ-FRB210I. EXIT.                                          00009300
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
       COPY SYBCERRO.                                                           
      *                                                                 58680001
      *                                                                 59260001
       WRITE-FRB210O                   SECTION.                         59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           INITIALIZE ENREG-FRB210O                                             
           MOVE FRB210I-NCONTREDBOX TO FRB210O-IDCONTRAT                        
           MOVE FRB210I-DINTER(1:4) TO FRB210O-DTACTI-FACT-SSAA                 
           MOVE '/'                 TO FRB210O-DTACTI-FACT-FIL1                 
           MOVE FRB210I-DINTER(5:2) TO FRB210O-DTACTI-FACT-MM                   
           MOVE '/'                 TO FRB210O-DTACTI-FACT-FIL2                 
           MOVE FRB210I-DINTER(7:2) TO FRB210O-DTACTI-FACT-JJ                   
           MOVE '0004'              TO FRB210O-TYPE-FAS-FACT                    
           MOVE SPACES  TO FRB210I-PINTER-ENT                                   
                           FRB210I-PINTER-DEC                                   
                           WS-FRB210I-PINTER-ENT                                
           UNSTRING FRB210I-PINTER DELIMITED BY     ','                         
                    INTO                                                        
             WS-FRB210I-PINTER-ENT                                              
             FRB210I-PINTER-DEC                                                 
           UNSTRING WS-FRB210I-PINTER-ENT DELIMITED BY  SPACE                   
                    INTO                                                        
             FRB210I-PINTER-ENT                                                 
           INSPECT FRB210I-PINTER-DEC  REPLACING ALL ' ' BY  '0'                
           MOVE ','   TO FRB210I-PINTER-VIRG                                    
           MOVE FRB210I-PINTERN     TO FRB210O-MT-FAS-FACT                      
           WRITE FRB210O-RECORD FROM ENREG-FRB210O                              
                                                                        59340001
           IF NOT STATUT-FRB210O-OK                                     59350001
              DISPLAY 'PB ECRITURE FRB210O ' STATUT-FRB210O             59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           ADD 1 TO WS-CPT-ECRIS.                                               
      *                                                                 59420001
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      *                                                                 58680001
