      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BRB000.                                           00000040
       AUTHOR.        DSA045-EY.                                        00000050
       DATE-WRITTEN.  19/04/2006.                                       00000060
      *----------------------------------------------------------------*00000080
      * RECEPTION DES MOUVEMENTS QUOTIDIENS SUR LA RECEPTION ENTREPOT. *00000090
      * METTRE LE FICHIER .CSV AVEC LES SEPARATEURS ';' DANS UN        *00000090
      * FICHIER PLAT                                                   *00000090
      *----------------------------------------------------------------*00000080
      * DATE   : AOUT 2006                                             *        
      * MODIFICATION :                                                 *        
      * AUTEUR : JEREMY CELAIRE  JC0806                                *        
      * COMMENTAIRE :                                                  *        
      *  - PASSAGE DE 15 A 16 CARAC. SUR LE LOGIN ET MOT DE PASSE      *        
      *    DU FICHIER FRB000.                                          *        
      *  - FICHIER FRB001 VARIABLE.                                    *        
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000160
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                   00000170
      *    DECIMAL-POINT IS COMMA.                                      00000180
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB001     ASSIGN TO FRB001                           00000250
      *                      FILE STATUS ST-FRB001.                             
      *--                                                                       
           SELECT FRB001     ASSIGN TO FRB001                                   
                             FILE STATUS ST-FRB001                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB000     ASSIGN TO FRB000                           00000250
      *                      FILE STATUS ST-FRB000.                             
      *--                                                                       
           SELECT FRB000     ASSIGN TO FRB000                                   
                             FILE STATUS ST-FRB000                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FRB001                                                       00000440
JC0806*    RECORDING F                                                  00000450
JC0806     RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
JC0806     RECORD CONTAINS 1 TO 400 CHARACTERS                          00000470
JC0806     DATA RECORD IS FRB001-ENR.                                   00000470
JC0806*01  FRB001-ENR.                                                  00000480
JC0806*    05  FRB001-ENREGISTRE PIC X(400).                            00000480
MB     01  FRB001-ENR.                                                  00000480
MB         10  FRB065-ENREGISTRE      PICTURE X(01)                     00000480
MB                                    OCCURS 1 TO 400 TIMES             00000480
MB                                    DEPENDING ON I-LONG.                      
      *                                                                 00000600
       FD  FRB000                                                       00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FRB000-ENR.                                                  00000480
           05  FRB000-FILLEUR         PIC X(300).                       00000480
      *                                                                 00000600
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       77  ST-FRB001                  PIC 9(02) VALUE ZERO.                     
       77  ST-FRB000                  PIC 9(02) VALUE ZERO.                     
           COPY  FRB000.                                                        
      *    COPIES D'ABEND ET DE CONTR�LE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      *    DESCRIPTION DES TABLES UTILIS�ES.                            00001910
      *    EXEC  SQL  INCLUDE  SQLCA     END-EXEC.                      00001940
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    R�CUP�RATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  WS-NBR-LUS                 PIC 9(06)  COMP-3   VALUE 0.              
       01  CPT-FRB000                 PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01  WS-DCOMMANDE               PIC X(10).                        00000480
       01  WWS-DCOMMANDE              PIC X(10).                        00000480
       01  WS-DPRODUCTION             PIC X(10).                        00000480
       01  WWS-DPRODUCTION            PIC X(10).                        00000480
       01  LG-FRB001                  PIC 9(05) VALUE ZERO.                     
JC0806 01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
JC0806 01  I                          PIC S9(3)  COMP-3 VALUE 0.                
JC0806 01  FRB001-ENREGISTREMENT      PIC X(400).                               
JC0806 01  FRB001-ENR1.                                                 00000480
JC0806     10  FRB001-ENREG           PICTURE X(01)                     00000480
JC0806                                OCCURS 400.                       00000480
JC0806*                               DEPENDING ON I-LONG.                      
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FRB001             PIC 9(01)           VALUE 0.              
           88 FIN-FRB001                                  VALUE 1.              
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BRB000       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    LECTURE DU FICHIER EN ENTR�E.                                00001900
      *    C'EST LE FICHIER .CSV CE N'EST PAS UN POSITIONNEL            00001900
      *    LE BUT EST DE LE METTRE DANS UN POSITIONEL                   00001900
      *    LECTURE DU FICHIER.                                          00001900
      *    PREMIERE LECTURE C'EST POUR OMMETRE L'ENTETE                 00001900
           PERFORM  READ-FRB001 .                                               
      *    c'est la 2� eme lecture qui est la bonne                             
           PERFORM  READ-FRB001                                                 
           PERFORM UNTIL FIN-FRB001                                             
              PERFORM FORMATAGE-DONNEES                                         
              PERFORM ECRITURE-FRB000                                           
              PERFORM READ-FRB001                                               
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BRB000.                                                 
      *                                                                         
       FIN-MODULE-BRB000.  EXIT.                                                
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * REFORMATAGE DES DONN�ES.                                                
      *-----------------------------------------------------------------        
       FORMATAGE-DONNEES  SECTION.                                      00002160
      *                                                                 00001900
           INITIALIZE FRB000-ENR                                                
                      FRB000-RECORD  .                                          
      *                                                                 00001900
JC0806     PERFORM VARYING I FROM 1 BY 1 UNTIL I > 400                          
JC0806         MOVE FRB001-ENREG(I)                                             
JC0806                        TO  FRB001-ENREGISTREMENT(I:1)                    
JC0806     END-PERFORM                                                          
      *                                                                         
      *    AJOUTER LE DERNIER ';' � LA FIN DE CHAINE SI MANQUE                  
           PERFORM VARYING LG-FRB001 FROM 1 BY 1 UNTIL                          
              FRB001-ENREGISTREMENT (LG-FRB001:) NOT > SPACES                   
           OR LG-FRB001      >= LENGTH OF FRB001-ENREGISTREMENT                 
           END-PERFORM                                                          
           IF FRB001-ENREGISTREMENT (LG-FRB001 - 1:1) NOT = ';'                 
              MOVE ';' TO  FRB001-ENREGISTREMENT (LG-FRB001:1)                  
           END-IF .                                                             
           UNSTRING FRB001-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
                    FRB000-NFOURNISSEUR                                      000
                    FRB000-LREFFOURN                                         000
                    FRB000-LREFCOMM                                          000
                    FRB000-NSERIE                                            000
                    FRB000-ADRMAC                                            000
                    FRB000-NHARDWARE                                         000
                    FRB000-NFIRMWARE                                         000
                    FRB000-NCOMMANDE                                            
                    WS-DCOMMANDE                                             000
                    WS-DPRODUCTION                                           000
                    FRB000-NLOTPROD                                          000
                    FRB000-NPALETTE                                             
                    FRB000-LLGRECEPTION                                      000
                    FRB000-NEAN                                              000
                    FRB000-CLOGIN                                            000
                    FRB000-CPASSE                                            000
                    FRB000-LCONSTRUCTEUR                                     000
                    FRB000-NBL                                               000
                    FRB000-TEQUIPEMENT                                       000
240608              FRB000-ADRMAC2 .                                         000
      *    MODIFICATION DE FORMAT DES DATES. ELLES SONT R�PUT�ES                
      *    CORRECTES <=> NON CONTR�L�ES.                                        
      *    DATE D'OP�RATION.                                                    
           IF  WS-DCOMMANDE(2:1) = '/'                                          
               INITIALIZE WWS-DCOMMANDE                                         
               MOVE   WS-DCOMMANDE   TO  WWS-DCOMMANDE                          
               STRING '0'   WWS-DCOMMANDE                                       
               DELIMITED BY SIZE INTO WS-DCOMMANDE                              
           END-IF                                                               
           IF  WS-DCOMMANDE(5:1) = '/'                                          
               INITIALIZE WWS-DCOMMANDE                                         
               MOVE   WS-DCOMMANDE   TO  WWS-DCOMMANDE                          
               STRING WWS-DCOMMANDE (1:3) '0' WWS-DCOMMANDE (4:)                
               DELIMITED BY SIZE INTO WS-DCOMMANDE                              
           END-IF                                                               
           STRING WS-DCOMMANDE(7:4) WS-DCOMMANDE(4:2)                           
                  WS-DCOMMANDE(1:2)                                             
           DELIMITED BY SIZE INTO FRB000-DCOMMANDE .                            
           IF  WS-DPRODUCTION(2:1) = '/'                                        
               INITIALIZE WWS-DPRODUCTION                                       
               MOVE   WS-DPRODUCTION TO  WWS-DPRODUCTION                        
               STRING '0'   WWS-DPRODUCTION                                     
               DELIMITED BY SIZE INTO WS-DPRODUCTION                            
           END-IF                                                               
           IF  WS-DPRODUCTION(5:1) = '/'                                        
               INITIALIZE WWS-DPRODUCTION                                       
               MOVE   WS-DPRODUCTION TO  WWS-DPRODUCTION                        
               STRING WWS-DPRODUCTION (1:3) '0' WWS-DPRODUCTION (4:)            
               DELIMITED BY SIZE INTO WS-DPRODUCTION                            
           END-IF                                                               
           STRING WS-DPRODUCTION(7:4) WS-DPRODUCTION(4:2)                       
                  WS-DPRODUCTION(1:2)                                           
           DELIMITED BY SIZE INTO FRB000-DPRODUCTION .                          
       FIN-FORMATAGE-DONNEES. EXIT.                                     00002620
                                                                        00002620
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! RECEPTION DES DONNEES REDBOX  .              !'  00002400
           DISPLAY  '! FORMATAGE DU FICHIER .CSV     .              !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 19/04/2006 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTR�E.                                      
           OPEN  INPUT  FRB001 .                                        00002950
      *    OUVERTURE DU FICHIER EN SORTIE.                                      
           OPEN  OUTPUT FRB000 .                                        00002950
      *    R�CUP�RATION DE LA DATE SYST�ME.                                     
           PERFORM LECTURE-DSYST .                                              
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * R�CUP�RATION DE LA DATE SYST�ME.                                        
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BRB000 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS        00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS ECRIT   : ' CPT-FRB000        00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FRB001 FRB000                                                 
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRB000. EXIT.                                            00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FRB001 FRB000.                                                
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB000'  TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FRB001  SECTION.                                                    
           READ FRB001  INTO FRB001-ENR1  AT END                                
                SET FIN-FRB001     TO TRUE                                      
                        NOT AT END                                              
                ADD  1             TO WS-NBR-LUS                                
           END-READ.                                                            
       FIN-READ-FRB001. EXIT.                                           00009300
      *                                                                         
       ECRITURE-FRB000              SECTION.                                    
           WRITE  FRB000-ENR from FRB000-RECORD.                                
           ADD 1 TO CPT-FRB000 .                                                
           INITIALIZE FRB000-ENR                                                
                      FRB000-RECORD  .                                          
       FIN-ECRITURE-FRB000   . EXIT.                                            
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
      *-----------------------------------------------------------------        
      * TEST DES CODES RETOUR SQL.                                              
      *-----------------------------------------------------------------        
       TEST-CODE-RETOUR-SQL          SECTION.                                   
      *    MOVE '0' TO CODE-RETOUR.                                     00012220
      *    EVALUATE SQLCODE                                             00012230
      *             WHEN +0                                             00012240
      *              SET TROUVE TO TRUE                                 00012250
      *             WHEN +100                                           00012260
      *              SET NON-TROUVE TO TRUE                             00012270
      *             WHEN -803                                           00012280
      *              SET EXISTE-DEJA TO TRUE                            00012290
      *             WHEN OTHER                                          00012300
      *              DISPLAY TRACE-SQL-MESSAGE                                  
      *              MOVE 'ERREUR DB2 '  TO ABEND-MESS                  00012310
      *              MOVE SQLCODE TO ABEND-CODE                         00012320
      *              PERFORM FIN-ANORMALE                               00012330
      *    END-EVALUATE.                                                00012340
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      *                                                                         
       COPY SYBCERRO.                                                           
