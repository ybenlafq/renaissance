      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BQC340.                                           00000040
      *----------------------------------------------------------------*00000080
      * TRAITEMENT DES QUESTIONNAIRES POST-APPEL  RECU VIA  FICHIER CSV*00000090
      * DE SIEBEL                                                      *        
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FQC341     ASSIGN TO FQC341                           00000250
      *                      FILE STATUS ST-FQC341.                             
      *--                                                                       
           SELECT FQC341     ASSIGN TO FQC341                                   
                             FILE STATUS ST-FQC341                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FQC342     ASSIGN TO FQC342                           00000250
      *                      FILE STATUS ST-FQC342.                             
      *--                                                                       
           SELECT FQC342     ASSIGN TO FQC342                                   
                             FILE STATUS ST-FQC342                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *                                                                 00000600
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FQC341                                                       00000440
           RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
           RECORD CONTAINS 1 TO 700 CHARACTERS                          00000470
           DATA RECORD IS FQC341-ENR.                                   00000470
       01  FQC341-ENR.                                                  00000480
           10  FQC341-ENREG           PICTURE X(01)                     00000480
                                      OCCURS 1 TO 700 TIMES             00000480
                                      DEPENDING ON I-LONG.                      
      *                                                                 00000430
       FD  FQC342                                                               
           LABEL RECORD STANDARD                                                
           BLOCK 0                                                              
           RECORDING MODE IS F.                                                 
       01  WS-FQC342          PIC X(600).                                       
      *                                                                 00000430
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OPERATIONS SUR FICHIER.                                   
       77  STRING-FRBXXX              PIC X(130) VALUE SPACES.                  
       77  ST-FQC341                  PIC X(02) VALUE '00'.                     
       77  ST-FQC342                  PIC X(02) VALUE '00'.                     
       01  STATUT-FRB501                  PIC  X(02).                           
      *{ remove-comma-in-dde 1.5                                                
      *    88  STATUT-FRB501-OK                   VALUE '00' , '  '.            
      *--                                                                       
           88  STATUT-FRB501-OK                   VALUE '00'   '  '.            
      *}                                                                        
       01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
       01  WW-FQC341                  PIC X(700).                               
      *    COPIES D'ABEND ET DE CONTROLE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
                                                                        00001910
        01  STATUT-FQC341                  PIC  X(02).                          
      *{ remove-comma-in-dde 1.5                                                
      *     88  STATUT-FQC341-OK                   VALUE '00' , '  '.           
      *--                                                                       
            88  STATUT-FQC341-OK                   VALUE '00'   '  '.           
      *}                                                                        
        01  STATUT-999                     PIC  X(02).                          
            88  STATUT-999-OK                   VALUE '00'.                     
            88  STATUT-999-KO                   VALUE '01'.                     
      *    NOM DES MODULES APPELES.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    RECUPERATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
      *    VARIABLES DE TRAVAIL.                                                
       01  WS-NBR-LUS-FQC341          PIC S9(07) COMP-3   VALUE 0.              
       01  WS-NBR-ECR-FQC342          PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FQC341             PIC 9(01)           VALUE 0.              
           88 FIN-FQC341                                  VALUE 1.              
       01  FDATE-ENREG.                                                         
            02  FDAT.                                                           
                04  FDATE-JJ   PIC X(02).                                       
                04  FDATE-MM   PIC X(02).                                       
                04  FDATE-SS   PIC X(02).                                       
                04  FDATE-AA   PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
      *                                                                         
       01  ENR-FQC342.                                                          
            10 FQC342-NCISOC           PIC X(03).                               
            10 FQC342-CTQUEST          PIC 9(02).                               
            10 FQC342-NQUEST           PIC X(15).                               
            10 FQC342-NSOCIETE         PIC X(03).                               
            10 FQC342-NLIEU            PIC X(03).                               
            10 FQC342-DEVENT           PIC X(08).                               
            10 FQC342-CINT             PIC X(10).                               
            10 FQC342-EMAIL            PIC X(50).                               
            10 FQC342-NCLIENT          PIC X(08).                               
            10 FQC342-NADDR            PIC X(08).                               
            10 FQC342-CTITRE           PIC X(05).                               
            10 FQC342-LNOM             PIC X(25).                               
            10 FQC342-LPRENOM          PIC X(15).                               
            10 FQC342-LADR1            PIC X(32).                               
            10 FQC342-LADR2            PIC X(32).                               
            10 FQC342-CPOSTAL          PIC X(05).                               
            10 FQC342-LCOMMUNE         PIC X(32).                               
            10 FQC342-LPAYS            PIC X(32).                               
            10 FQC342-CFAM             PIC X(05).                               
            10 FQC342-CPICTO           PIC X(10).                               
            10 FQC342-LPRODUIT         PIC X(20).                               
            10 FQC342-CINT2            PIC X(10).                               
            10 FQC342-CINT3            PIC X(10).                               
            10 FQC342-NREFEVENT        PIC X(16).                               
            10 FQC342-CMARQUE          PIC X(10).                               
            10 FQC342-LREFPRODUIT      PIC X(20).                               
            10 FQC342-NTELCLIENT       PIC X(10).                               
            10 FQC342-LINFOCMPL        PIC X(10).                               
            10 FQC342-CHAMP1           PIC X(30).                               
            10 FQC342-CHAMP2           PIC X(30).                               
            10 FQC342-CHAMP3           PIC X(30).                               
            10 FQC342-CHAMP4           PIC X(15).                               
            10 FQC342-CHAMP5           PIC X(15).                               
      *                                                                         
       01  W-FQC341.                                                            
            10 FQC341-NCISOC           PIC X(03).                               
            10 FQC341-CTQUEST          PIC X(02) JUST RIGHT.                    
            10 FQC341-NQUEST           PIC X(15).                               
            10 FQC341-NSOCIETE         PIC X(03).                               
            10 FQC341-NLIEU            PIC X(03).                               
            10 FQC341-DEVENT           PIC X(08).                               
            10 FQC341-CINT             PIC X(10).                               
            10 FQC341-EMAIL            PIC X(50).                               
            10 FQC341-NCLIENT          PIC X(08).                               
            10 FQC341-NADDR            PIC X(08).                               
            10 FQC341-CTITRE           PIC X(05).                               
            10 FQC341-LNOM             PIC X(25).                               
            10 FQC341-LPRENOM          PIC X(15).                               
            10 FQC341-LADR1            PIC X(32).                               
            10 FQC341-LADR2            PIC X(32).                               
            10 FQC341-CPOSTAL          PIC X(05).                               
            10 FQC341-LCOMMUNE         PIC X(32).                               
            10 FQC341-LPAYS            PIC X(32).                               
            10 FQC341-CFAM             PIC X(05).                               
            10 FQC341-CPICTO           PIC X(10).                               
            10 FQC341-LPRODUIT         PIC X(20).                               
            10 FQC341-CINT2            PIC X(10).                               
            10 FQC341-CINT3            PIC X(10).                               
            10 FQC341-NREFEVENT        PIC X(16).                               
            10 FQC341-CMARQUE          PIC X(10).                               
            10 FQC341-LREFPRODUIT      PIC X(20).                               
            10 FQC341-NTELCLIENT       PIC X(10).                               
            10 FQC341-LINFOCMPL        PIC X(10).                               
            10 FQC341-LFLASH           PIC X(150).                              
            10 FQC341-CHAMP1           PIC X(30).                               
            10 FQC341-CHAMP2           PIC X(30).                               
            10 FQC341-CHAMP3           PIC X(30).                               
            10 FQC341-CHAMP4           PIC X(15).                               
            10 FQC341-CHAMP5           PIC X(15).                               
      *                                                                 00000430
      *                                                                         
       01  I                    PIC 9(5).                                       
       01  WS-NSEQ              PIC X(5) JUST RIGHT.                            
       01  WS-NSEQ-N            PIC 9(5).                                       
       01  WS-NSOCIETE          PIC X(3) JUST RIGHT.                            
       01  WS-NSOCIETE-N        PIC 9(3).                                       
       01  WS-NLIEU             PIC X(3) JUST RIGHT.                            
       01  WS-NLIEU-N           PIC 9(3).                                       
       01  WS-NVENTE            PIC X(7) JUST RIGHT.                            
       01  WS-NVENTE-N          PIC 9(7).                                       
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BQC340       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    ON SAUTE L'ENTETE                                            00001900
      *    LECTURE DES FICHIERS EN ENTREE.                              00001900
           PERFORM  READ-FQC341 .                                               
      *                                                                         
           PERFORM UNTIL FIN-FQC341                                             
                 PERFORM TRAITEMENT                                             
                 PERFORM READ-FQC341                                            
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BQC340.                                                 
      *                                                                         
       FIN-MODULE-BQC340. EXIT.                                                 
              EJECT                                                     00002620
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE DEBUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! MISE EN FORME FICHIER DES POST-APPEL         !'  00002400
           DISPLAY  '!                                              !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!    CE PROGRAMME DEBUTE NORMALEMENT (BQC340)  !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION COMPILEE LE ' DATE-COMPILE.                      
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTREE.                                      
           OPEN  INPUT  FQC341                                          00002950
           OPEN  OUTPUT  FQC342.                                        00002950
           INITIALIZE STATUT-FRB501 .                                           
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
       TRAITEMENT SECTION.                                                      
      *--------------------                                                     
           MOVE FQC341-NCISOC          TO FQC342-NCISOC                         
           MOVE FQC341-CTQUEST         TO FQC342-CTQUEST                        
           MOVE FQC341-NQUEST          TO FQC342-NQUEST                         
           MOVE FQC341-NSOCIETE        TO FQC342-NSOCIETE                       
           MOVE FQC341-NLIEU           TO FQC342-NLIEU                          
      *    MOVE FQC341-DEVENT          TO FQC342-DEVENT                         
           string FQC341-DEVENT(1:4) FQC341-DEVENT(7:2)                         
                 FQC341-DEVENT(5:2) delimited by size into FQC342-DEVENT        
           MOVE FQC341-CINT            TO FQC342-CINT                           
           MOVE FQC341-EMAIL           TO FQC342-EMAIL                          
           MOVE FQC341-NCLIENT         TO FQC342-NCLIENT                        
           MOVE FQC341-NADDR           TO FQC342-NADDR                          
           MOVE FQC341-CTITRE          TO FQC342-CTITRE                         
           MOVE FQC341-LNOM            TO FQC342-LNOM                           
           MOVE FQC341-LPRENOM         TO FQC342-LPRENOM                        
           MOVE FQC341-LADR1           TO FQC342-LADR1                          
           MOVE FQC341-LADR2           TO FQC342-LADR2                          
           MOVE FQC341-CPOSTAL         TO FQC342-CPOSTAL                        
           MOVE FQC341-LCOMMUNE        TO FQC342-LCOMMUNE                       
           MOVE FQC341-LPAYS           TO FQC342-LPAYS                          
           MOVE FQC341-CFAM            TO FQC342-CFAM                           
           MOVE FQC341-CPICTO          TO FQC342-CPICTO                         
           MOVE FQC341-LPRODUIT        TO FQC342-LPRODUIT                       
           MOVE FQC341-CINT2           TO FQC342-CINT2                          
           MOVE FQC341-CINT3           TO FQC342-CINT3                          
           MOVE FQC341-NREFEVENT       TO FQC342-NREFEVENT                      
           MOVE FQC341-CMARQUE         TO FQC342-CMARQUE                        
           MOVE FQC341-LREFPRODUIT     TO FQC342-LREFPRODUIT                    
           MOVE FQC341-NTELCLIENT      TO FQC342-NTELCLIENT                     
           MOVE FQC341-LINFOCMPL       TO FQC342-LINFOCMPL                      
           MOVE FQC341-CHAMP1          TO FQC342-CHAMP1                         
           MOVE FQC341-CHAMP2          TO FQC342-CHAMP2                         
           MOVE FQC341-CHAMP3          TO FQC342-CHAMP3                         
           MOVE FQC341-CHAMP4          TO FQC342-CHAMP4                         
           MOVE FQC341-CHAMP5          TO FQC342-CHAMP5                         
           PERFORM WRITE-FQC342.                                                
       FIN-TRAITEMENT. EXIT.                                                    
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BQC340 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS        : '                00002400
                     WS-NBR-LUS-FQC341                                  00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS TRAITES    : '                00002400
                     WS-NBR-ECR-FQC342                                  00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DU PROGRAMME BQC340                !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FQC341 .                                                      
           CLOSE  FQC342 .                                                      
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BQC340O. EXIT.                                           00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FQC341 .                                                      
           CLOSE  FQC342 .                                                      
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BQC340' TO    ABEND-PROG                              00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTREE.                                           
      *-----------------------------------------------------------------        
       READ-FQC341 SECTION.                                                     
       READ-AGAIN.                                                              
           INITIALIZE          WW-FQC341                                        
           READ FQC341 INTO    WW-FQC341                                        
                AT END     SET FIN-FQC341     TO TRUE                           
                NOT AT END ADD 1  TO WS-NBR-LUS-FQC341                          
                UNSTRING WW-FQC341     DELIMITED BY     ';'                     
                         INTO                                                   
      *        W-FQC341                                                         
               FQC341-NCISOC                                                    
               FQC341-CTQUEST                                                   
               FQC341-NQUEST                                                    
               FQC341-NSOCIETE                                                  
               FQC341-NLIEU                                                     
               FQC341-DEVENT                                                    
               FQC341-CINT                                                      
               FQC341-EMAIL                                                     
               FQC341-NCLIENT                                                   
               FQC341-NADDR                                                     
               FQC341-CTITRE                                                    
               FQC341-LNOM                                                      
               FQC341-LPRENOM                                                   
               FQC341-LADR1                                                     
               FQC341-LADR2                                                     
               FQC341-CPOSTAL                                                   
               FQC341-LCOMMUNE                                                  
               FQC341-LPAYS                                                     
               FQC341-CFAM                                                      
               FQC341-CPICTO                                                    
               FQC341-LPRODUIT                                                  
               FQC341-CINT2                                                     
               FQC341-CINT3                                                     
               FQC341-NREFEVENT                                                 
               FQC341-CMARQUE                                                   
               FQC341-LREFPRODUIT                                               
               FQC341-NTELCLIENT                                                
               FQC341-LINFOCMPL                                                 
               FQC341-LFLASH                                                    
               FQC341-CHAMP1                                                    
               FQC341-CHAMP2                                                    
               FQC341-CHAMP3                                                    
               FQC341-CHAMP4                                                    
               FQC341-CHAMP5                                                    
           END-READ                                                             
           IF NOT STATUT-FRB501-OK                                      59350001
              DISPLAY 'PB LECTURE  FQC341 ' STATUT-FRB501               59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-READ-FQC341. EXIT.                                           00009300
      *                                                                         
       WRITE-FQC342         SECTION.                                            
           WRITE WS-FQC342   FROM ENR-FQC342                                    
           IF ST-FQC342       NOT = '00'                                        
              DISPLAY 'PB ECRITURE FQC342 ' ST-FQC342                           
              PERFORM  FIN-ANORMALE                                             
           ELSE                                                                 
              ADD 1   TO WS-NBR-ECR-FQC342                                      
           END-IF                                                               
           INITIALIZE WS-FQC342                                                 
           .                                                                    
       FIN-WRITE-FQC342. EXIT.                                                  
