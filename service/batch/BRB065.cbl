      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BRB065.                                           00000040
       AUTHOR.        DSA021-EY.                                        00000050
       DATE-WRITTEN.  04/01/2007.                                       00000060
      *----------------------------------------------------------------*00000080
      * RECEPTION DU FICHIER FOURNI PAR HIGHDEAL.                      *00000090
      * METTRE LE FICHIER .CSV AVEC LES SEPARATEURS ';' DANS UN        *00000090
      * FICHIER PLAT                                                   *00000090
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB065     ASSIGN TO FRB065                           00000250
      *                      FILE STATUS ST-FRB065.                             
      *--                                                                       
           SELECT FRB065     ASSIGN TO FRB065                                   
                             FILE STATUS ST-FRB065                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB066     ASSIGN TO FRB066                           00000250
      *                      FILE STATUS ST-FRB066.                             
      *--                                                                       
           SELECT FRB066     ASSIGN TO FRB066                                   
                             FILE STATUS ST-FRB066                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      * CARTE FNSOC  : NUMERO DE SOCIETE A TRAITER                              
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNSOC    ASSIGN TO FNSOC.                                     
      *--                                                                       
           SELECT FNSOC    ASSIGN TO FNSOC                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FRB065                                                       00000440
           RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
           RECORD CONTAINS 1 TO 146 CHARACTERS                          00000470
           DATA RECORD IS FRB065-ENR.                                   00000470
                                                                        00000480
       01  FRB065-ENR.                                                  00000480
           10  FRB065-ENREG           PICTURE X(01)                     00000480
                                      OCCURS 1 TO 146 TIMES             00000480
                                      DEPENDING ON I-LONG.                      
      *                                                                 00000600
       FD  FRB066                                                       00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FRB066-ENR.                                                  00000480
           05  FRB066-FILLEUR         PIC X(150).                       00000480
       FD  FNSOC  RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER         PIC X(80).                                         
      *                                                                 00000600
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OPERATIONS SUR FICHIER.                                   
       77  ST-FRB065                  PIC 9(02) VALUE ZERO.                     
       77  ST-FRB066                  PIC 9(02) VALUE ZERO.                     
       01 W-FNSOC.                                                              
          03  W-NSOC                 PIC  X(03)        VALUE SPACES.            
          03  FILLER                 PIC  X(77)        VALUE SPACES.            
       01  FRB066-RECORD.                                               00000480
      *    RECORD = 80 + 70 = 150 CAR                                           
           05  FRB066-NCONTRAT        PIC X(18).                        00000480
           05  FRB066-NSERIE          PIC X(20).                        00000480
           05  FRB066-NCODIC          PIC X(07).                        00000480
           05  FRB066-NSOC            PIC X(03).                        00000480
           05  FRB066-NLIEU           PIC X(03).                        00000480
           05  FRB066-NVENTE          PIC X(7).                         00000480
           05  FRB066-MTHT-PCESSION   PIC 9(5)V99.                      00000480
           05  FRB066-MTHT-ENCAISS    PIC 9(5)V99.                      00000480
           05  FRB066-DFACT           PIC X(8).                         00000480
      * CE CHAMPS EST UTILISE DE FACON PROVISOIR POUR LES LIB           00000600
      * ANOMALIE ON PEUT TOUJOURS LE DECALER SI BESOIN EST              00000600
           05  FRB066-FILLER          PIC X(70).                        00000480
      *                                                                 00000600
      *    COPIES D'ABEND ET DE CONTR�LE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      *    DESCRIPTION DES TABLES UTILISEES.                            00001910
      *    EXEC  SQL  INCLUDE  SQLCA     END-EXEC.                      00001940
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    RECUPERATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  WS-NBR-LUS                 PIC 9(06)  COMP-3   VALUE 0.              
       01  CPT-FRB066                 PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01  WS-NCODIC                  PIC 9(07).                        00000480
       01  WS-NLIEU                   PIC 9(03).                        00000480
       01  WS-NVENTE                  PIC 9(07).                        00000480
       01  WS-MONTANT1                PIC 9(07).                        00000480
       01  WS-MONTANT2                PIC 9(07).                        00000480
       01  WS-DFACT                   PIC X(10).                        00000480
       01  WWS-DFACT                  PIC X(10).                        00000480
       01  LG-FRB065                  PIC 9(05) VALUE ZERO.                     
       01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
       01  I                          PIC S9(3)  COMP-3 VALUE 0.                
       01  FRB065-ENREGISTREMENT      PIC X(150).                               
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FRB065             PIC 9(01)           VALUE 0.              
           88 FIN-FRB065                                  VALUE 1.              
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BRB065       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    LECTURE DU FICHIER EN ENTREE.                                00001900
      *    C'EST LE FICHIER .CSV CE N'EST PAS UN POSITIONNEL            00001900
      *    LE BUT EST DE LE METTRE DANS UN POSITIONEL                   00001900
      *    LECTURE DU FICHIER.                                          00001900
      *--  PREMIERE LECTURE.                                            00001900
           PERFORM  READ-FRB065.                                                
           PERFORM UNTIL FIN-FRB065                                             
              PERFORM FORMATAGE-DONNEES                                         
      *       ON N'ECRIT SEULEMENT LES DONNEES CONCERNANT LA SOCIETE            
      *       PASSEE EN PARAMETRE                                               
              IF FRB066-NSOC = W-NSOC                                           
                 PERFORM ECRITURE-FRB066                                        
              END-IF                                                            
              PERFORM READ-FRB065                                               
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BRB065.                                                 
      *                                                                         
       FIN-MODULE-BRB065.  EXIT.                                                
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * REFORMATAGE DES DONN�ES.                                                
      *-----------------------------------------------------------------        
       FORMATAGE-DONNEES  SECTION.                                      00002160
      *                                                                 00001900
           INITIALIZE FRB066-ENR                                                
                      FRB066-RECORD  .                                          
      *                                                                 00001900
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 150                          
               MOVE FRB065-ENREG(I) TO  FRB065-ENREGISTREMENT(I:1)              
           END-PERFORM                                                          
      *                                                                         
      *    AJOUTER LE DERNIER ';' � LA FIN DE CHAINE SI MANQUE                  
           PERFORM VARYING LG-FRB065 FROM 1 BY 1 UNTIL                          
              FRB065-ENREGISTREMENT (LG-FRB065:) NOT > SPACES                   
           OR LG-FRB065      >= LENGTH OF FRB065-ENREGISTREMENT                 
           END-PERFORM                                                          
           IF FRB065-ENREGISTREMENT (LG-FRB065 - 1:1) NOT = ';'                 
              MOVE ';' TO  FRB065-ENREGISTREMENT (LG-FRB065:1)                  
           END-IF .                                                             
           MOVE ZERO   TO WS-NCODIC                                             
                          WS-NLIEU                                              
                          WS-NVENTE                                             
                          WS-MONTANT1                                           
                          WS-MONTANT2                                           
           MOVE SPACES TO WS-DFACT                                              
                          WWS-DFACT                                             
           UNSTRING FRB065-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
                    FRB066-NCONTRAT                                          000
                    FRB066-NSERIE                                            000
                    WS-NCODIC                                                000
                    FRB066-NSOC                                              000
                    WS-NLIEU                                                 000
                    WS-NVENTE                                                000
                    WS-MONTANT1                                              000
                    WS-MONTANT2                                              000
                    WS-DFACT.                                                   
           MOVE WS-NCODIC  TO  FRB066-NCODIC                                    
           MOVE WS-NLIEU   TO  FRB066-NLIEU                                     
           MOVE WS-NVENTE  TO  FRB066-NVENTE                                    
           COMPUTE FRB066-MTHT-PCESSION = WS-MONTANT1 / 100.                    
           COMPUTE FRB066-MTHT-ENCAISS  = WS-MONTANT2 / 100.                    
      *    MODIFICATION DE FORMAT DES DATES. ELLES SONT REPUTEES                
      *    CORRECTES <=> NON CONTR�L�ES.                                        
      *    DATE D'OP�RATION.                                                    
           IF  WS-DFACT(2:1) = '/'                                              
               INITIALIZE WWS-DFACT                                             
               MOVE   WS-DFACT       TO  WWS-DFACT                              
               STRING '0'   WWS-DFACT                                           
               DELIMITED BY SIZE INTO WS-DFACT                                  
           END-IF                                                               
           IF  WS-DFACT(5:1) = '/'                                              
               INITIALIZE WWS-DFACT                                             
               MOVE   WS-DFACT       TO  WWS-DFACT                              
               STRING WWS-DFACT (1:3) '0' WWS-DFACT (4:)                        
               DELIMITED BY SIZE INTO WS-DFACT                                  
           END-IF                                                               
           IF WS-DFACT(7:4) IS NOT NUMERIC                                      
              DISPLAY 'MAUVAIS FORMAT DATE :' FRB066-NSERIE ' ' WS-DFACT        
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           STRING WS-DFACT(7:4) WS-DFACT(4:2)                                   
                  WS-DFACT(1:2)                                                 
           DELIMITED BY SIZE INTO FRB066-DFACT .                                
       FIN-FORMATAGE-DONNEES. EXIT.                                     00002620
                                                                        00002620
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! RECEPTION DES DONNEES HIGHDEAL.              !'  00002400
           DISPLAY  '! FORMATAGE DU FICHIER .CSV     .              !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 04/01/2007 COMPILEE LE ' DATE-COMPILE.        
      *    RECUPERATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DES FICHIERS EN ENTREE.                                    
           OPEN  INPUT  FRB065 FNSOC.                                   00002950
      *    OUVERTURE DU FICHIER EN SORTIE.                                      
           OPEN  OUTPUT FRB066 .                                        00002950
           READ FNSOC INTO W-FNSOC AT END                                       
              MOVE  'ERREUR EN LECTURE FNSOC'  TO  ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
      *    R�CUP�RATION DE LA DATE SYST�ME.                                     
           PERFORM LECTURE-DSYST .                                              
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * R�CUP�RATION DE LA DATE SYST�ME.                                        
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BRB065 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS        00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS ECRIT   : ' CPT-FRB066        00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FRB065 FRB066 FNSOC                                           
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRB065. EXIT.                                            00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FRB065 FRB066 FNSOC.                                          
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB065'  TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FRB065  SECTION.                                                    
           READ FRB065  AT END                                                  
                SET FIN-FRB065     TO TRUE                                      
                        NOT AT END                                              
                ADD  1             TO WS-NBR-LUS                                
           END-READ.                                                            
       FIN-READ-FRB065. EXIT.                                           00009300
      *                                                                         
       ECRITURE-FRB066              SECTION.                                    
           WRITE  FRB066-ENR FROM FRB066-RECORD.                                
           ADD 1 TO CPT-FRB066 .                                                
       FIN-ECRITURE-FRB066   . EXIT.                                            
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
      *-----------------------------------------------------------------        
      * TEST DES CODES RETOUR SQL.                                              
      *-----------------------------------------------------------------        
       TEST-CODE-RETOUR-SQL          SECTION.                                   
      *    MOVE '0' TO CODE-RETOUR.                                     00012220
      *    EVALUATE SQLCODE                                             00012230
      *             WHEN +0                                             00012240
      *              SET TROUVE TO TRUE                                 00012250
      *             WHEN +100                                           00012260
      *              SET NON-TROUVE TO TRUE                             00012270
      *             WHEN -803                                           00012280
      *              SET EXISTE-DEJA TO TRUE                            00012290
      *             WHEN OTHER                                          00012300
      *              DISPLAY TRACE-SQL-MESSAGE                                  
      *              MOVE 'ERREUR DB2 '  TO ABEND-MESS                  00012310
      *              MOVE SQLCODE TO ABEND-CODE                         00012320
      *              PERFORM FIN-ANORMALE                               00012330
      *    END-EVALUATE.                                                00012340
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      *                                                                         
       COPY SYBCERRO.                                                           
