      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    brb400.                                           00000040
       AUTHOR.        HV.                                               00000050
       DATE-WRITTEN.  13/12/2010.                                       00000060
      *----------------------------------------------------------------*00000080
      * envoi vers la ncg des 'abonnements fluiid' RE1                 *00000090
      * envoi vers la ncg des 'ventes vad BLM    ' RE4                 *00000090
      * envoi vers la ncg des 'corrections assurances' RE2             *00000090
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      * Carte FDATE  : Date de g�n�ration du fichier                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE    ASSIGN TO FDATE.                                    
      *--                                                                       
            SELECT FDATE    ASSIGN TO FDATE                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRB400   ASSIGN TO FRB400.                                   
      *--                                                                       
            SELECT FRB400   ASSIGN TO FRB400                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
       FD  FDATE                                                        00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  MW-FILLER         PIC X(80).                                         
       FD  FRB400                                                       00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
      *MW DAR-2                                                                 
       01  MW-FILLER         PIC X(5909).                                       
      *                                                                 00000600
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OPERATIONS SUR FICHIER.                                   
      *    COPIES D'ABEND ET DE CONTR�LE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION PAR MQ.                                                
           COPY WORKMQ21.                                                       
       01  ws-mesrb001.                                                         
           COPY MESRB001.                                                       
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    RECUPERATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  ws-MESRB01-NOMVOIE1     pic x(80)     value spaces.                  
       01  ws-MESRB01-NOMVOIE2     pic x(80)     value spaces.                  
       01  ws-delai                pic x(03)     value zeroes.                  
       01  ws-delai-n              pic 9(03)     value zeroes.                  
       01  WS-NBR-LUS              PIC 9(07)  COMP-3   VALUE 0.                 
       01  WCPT-UPDATE-RTGV08      PIC S9(07) COMP-3   VALUE 0.                 
       01  WCPT-ENVOI-MQ1          PIC S9(07) COMP-3 VALUE ZEROES.              
       01  WCPT-ENVOI-MQ2          PIC S9(07) COMP-3 VALUE ZEROES.              
       01  WCPT-FANO01             PIC S9(07) COMP-3 VALUE ZEROES.              
       01  NOM-MODULE                 PIC X(08)         VALUE SPACES.           
       01  WS-NBR-RB31-INSERT         PIC S9(07) COMP-3   VALUE 0.              
       01  ws-dprec                   PIC X(08)           VALUE SPACES.         
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  DB2-SSAAMMJJ               PIC  X(8).                                
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01  LG-FRB066                  PIC 9(05) VALUE ZERO.                     
       01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
       01  I                          PIC S9(3)  COMP-3 VALUE 0.                
       01  I-003                      PIC S9(3)  COMP-3 VALUE 0.                
       01  FRB400-ENREGISTREMENT      PIC X(5909).                              
       01  WS-pvente-tab.                                                       
           05  WS-pvente-tab1 occurs 80.                                        
             10  WS-pvente            PIC 9(7).                                 
       01  WS-NLIEU-PREC              PIC X(03).                                
       01  WS-NVENTE-PREC             PIC X(07).                                
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
      * Flags fin de curseur                                                    
       01 LECTURE-frb400              PIC 9            VALUE 0.                 
          88 DEBUT-frb400                              VALUE 0.                 
          88   FIN-frb400                              VALUE 1.                 
      * Flags fin de curseur                                                    
       01 WS-ETAT-ENREG               PIC 9            VALUE 0.                 
          88 WS-ENREG-OK                               VALUE 0.                 
          88 WS-ENREG-KO                               VALUE 1.                 
       01 WS-ETAT-ENREG-PREC          PIC 9            VALUE 0.                 
          88 WS-ENREG-PREC-OK                          VALUE 0.                 
          88 WS-ENREG-PREC-KO                          VALUE 1.                 
       01  TOP-UPD                   PIC X(01) VALUE '1'.                       
           88 TOP-UPD-OUI                      VALUE '0'.                       
           88 TOP-UPD-NON                      VALUE '1'.                       
      *                                                                         
       01  WS-ETAT-IMMO              PIC X(01) VALUE '1'.                       
           88 WS-IMMO-OK                       VALUE '0'.                       
           88 WS-IMMO-KO                       VALUE '1'.                       
      *                                                                         
       01  W-NMODIF                   PIC S9(05) COMP-3 VALUE 0.                
       01  W-NSEQREF                  PIC S9(05) COMP-3 VALUE 0.                
       01  W-NSEQNQ                   PIC S9(05) COMP-3 VALUE 0.                
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-brb400       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *    TRAITEMENT.                                                  00001900
           PERFORM  TRAITEMENT-brb400                                   00002180
      *                                                                         
           PERFORM  FIN-brb400.                                                 
      *                                                                         
       FIN-MODULE-brb400.  EXIT.                                                
              EJECT                                                     00002620
      *                                                                         
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! Envoi vers NCG des abonnements fluiid        !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!    CE PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 07/09/2010 COMPILEE LE ' DATE-COMPILE.        
      *    RECUPERATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  '!PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2)00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  '!HEURE     :'  WS-HHMMSSCC                         00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DES FICHIERS EN ENTREE.                                    
           OPEN  INPUT  FDATE frb400.                                   00002950
           READ FDATE INTO GFJJMMSSAA AT END                                    
              MOVE  '<001> Erreur en lecture FDATE'  TO  ABEND-MESS             
              PERFORM FIN-ANORMALE                                              
           END-READ                                                             
           CLOSE FDATE.                                                         
           display 'FDATE=' GFJJMMSSAA                                          
           MOVE '1'      TO  GFDATA                                             
           CALL 'BETDATC'  USING  WORK-BETDATC                                  
           IF GFVDAT NOT = '1'                                                  
              MOVE '* PB BETDATC AVEC FDATE *' TO ABEND-MESS                    
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              MOVE GFJJMMSSAA(5:4)   TO DB2-SSAAMMJJ(1:4)                       
              MOVE GFJJMMSSAA(3:2)   TO DB2-SSAAMMJJ(5:2)                       
              MOVE GFJJMMSSAA(1:2)   TO DB2-SSAAMMJJ(7:2)                       
           END-IF.                                                              
      *    R�CUP�RATION DE LA DATE SYST�ME.                                     
           PERFORM LECTURE-DSYST .                                              
      *    LECTURE DU FICHIER EN ENTREE.                                00001900
           PERFORM lec-frb400.                                                  
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *                                                                 00001900
       TRAITEMENT-brb400       SECTION.                                         
      *-------------------------------                                          
           MOVE SPACES TO MESRB001-DATA                                         
           INITIALIZE     WORK-MQ21-APPLI.                                      
           PERFORM UNTIL FIN-frb400                                             
      *---    ON MET A JOUR LA TABLE RTGV11                             00001900
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (1:7)) = '*ASSUR*'                   
                 perform mise-en-forme2                                         
              else                                                              
                 perform mise-en-forme                                          
              end-if                                                            
              PERFORM ENVOI-NCG                                                 
              PERFORM lec-frb400                                                
           END-PERFORM .                                                        
      *                                                                         
       FIN-TRAITEMENT-brb400.  EXIT.                                            
              EJECT                                                     00002620
      *                                                                         
       ENVOI-NCG SECTION.                                                       
      *---------------------                                                    
      *    U --> CONNECT, OPEN, PUT, CLOSE ET DISCONNECT                        
           MOVE MESRB01-TSOC                  TO WORK-MQ21-NSOCDST              
                                                 WORK-MQ21-NSOC                 
           MOVE MESRB01-TLIEU                 TO WORK-MQ21-NLIEUDST             
                                                 WORK-MQ21-NLIEU                
           MOVE 'U'                           TO WORK-MQ21-ACTION               
           if MESRB01-NVENTEGV > ' '                                            
              MOVE 'RE2'                      TO WORK-MQ21-CFONC                
              move ws-MESRB01-NOMVOIE1        to MESRB01-NOMVOIE(1)             
              move ws-MESRB01-NOMVOIE2        to MESRB01-NOMVOIE(2)             
           else                                                                 
              if MESRB01-TREDBOX = '*VAD*' or 'BTADSL'                          
              or 'BTTHD' or 'BTMOBILE'                                          
                 MOVE 'RE4'                   TO WORK-MQ21-CFONC                
                 move ws-MESRB01-NOMVOIE1(1:5)   to MESRB01-NUMVOIE (01)        
                 move ws-MESRB01-NOMVOIE1(6:4)   to MESRB01-TYPVOIE (01)        
                 move ws-MESRB01-NOMVOIE1(10:21) to MESRB01-NOMVOIE (01)        
                 move ws-MESRB01-NOMVOIE1(31:32) to MESRB01-CADRFAC1(01)        
                 move ws-MESRB01-NOMVOIE2(1:5)   to MESRB01-NUMVOIE (02)        
                 move ws-MESRB01-NOMVOIE2(6:4)   to MESRB01-TYPVOIE (02)        
                 move ws-MESRB01-NOMVOIE2(10:21) to MESRB01-NOMVOIE (02)        
                 move ws-MESRB01-NOMVOIE2(31:32) to MESRB01-CADRFAC1(02)        
              else                                                              
                 MOVE 'RE1'                   TO WORK-MQ21-CFONC                
                 move ws-MESRB01-NOMVOIE1        to MESRB01-NOMVOIE(1)          
                 move ws-MESRB01-NOMVOIE2        to MESRB01-NOMVOIE(2)          
              end-if                                                            
           end-if                                                               
           MOVE 'N'                           TO WORK-MQ21-SYNCP                
           MOVE 7                             TO WORK-MQ21-PRIORITY             
           MOVE 'RB400'                       TO WORK-MQ21-NOMPROG              
           MOVE SPACES                        TO WORK-MQ21-MSGID                
                                                 WORK-MQ21-CORRELID             
           MOVE 1                             TO WORK-MQ21-NB-MESSAGES          
           MOVE 1                             TO WORK-MQ21-NBOCC                
           MOVE LENGTH OF  mesrb001-DATA TO WORK-MQ21-LONG-ENREG.               
           ADD 105 TO WORK-MQ21-LONG-ENREG.                                     
           MOVE mesrb001-DATA    TO WORK-MQ21-MESSAGE                           
           MOVE 'MMQ021'        TO NOM-MODULE                                   
           MOVE 'V1'            TO WORK-MQ21-VERSION                            
           MOVE WS-DSYST        TO WORK-MQ21-DSYST                              
           INITIALIZE           WORK-MQ21-LOG                                   
           CALL NOM-MODULE       USING WORK-MQ21-APPLI                          
      *                                                                         
           IF WORK-MQ21-CODRET NOT = '00' THEN                                  
              DISPLAY ' ERREUR APPEL MODULE MMQ021 POUR REDBOX '                
              DISPLAY ' -------------------------------------- '                
              DISPLAY WORK-MQ21-ERREUR                                          
              MOVE ' ERREUR MODULE MQ021' TO ABEND-MESS MESS                    
              MOVE WORK-MQ21-CODRET       TO ABEND-CODE                         
              DISPLAY 'ENREG=' FRB400-ENREGISTREMENT                            
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           ADD  1                    TO WCPT-ENVOI-MQ1.                         
           MOVE SPACES TO mesrb001-DATA                                         
           INITIALIZE  WORK-MQ21-APPLI.                                         
       FIN-ENVOI-NCG. EXIT.                                                     
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-brb400 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS        00002400
                    '.'                                                 00002400
           DISPLAY  'NB TOTAL DES MESSAGES MQ NCG : ' WCPT-ENVOI-MQ1            
                    '.'                                                 00002400
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                    !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *                                                                         
           CLOSE frb400.                                                        
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-brb400. EXIT.                                            00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           DISPLAY 'ENREG=' FRB400-ENREGISTREMENT                               
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB400'  TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
      *                                                                         
      *                                                                         
       COPY SYBCERRO.                                                           
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      * LECTURE DE LA TABLE GV11                                                
      *-----------------------------------------------------------------        
      *                                                                         
       lec-frb400                        SECTION.                               
      *                                                                         
       lec-frb400-encore.                                                       
           READ FRB400  INTO FRB400-ENREGISTREMENT AT END                       
                   SET FIN-FRB400     TO TRUE                                   
                           NOT AT END                                           
                   ADD  1             TO WS-NBR-LUS                             
           END-READ.                                                            
           if not fin-frb400                                                    
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (1:7)) = 'CCLIENT'                   
                 go to lec-frb400-encore                                        
              end-if                                                            
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (2:7)) = 'CCLIENT'                   
                 go to lec-frb400-encore                                        
              end-if                                                            
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (3:7)) = 'CCLIENT'                   
                 go to lec-frb400-encore                                        
              end-if                                                            
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (4:7)) = 'CCLIENT'                   
                 go to lec-frb400-encore                                        
              end-if                                                            
              if FUNCTION UPPER-CASE                                            
                    (frb400-enregistrement (5:7)) = 'CCLIENT'                   
                 go to lec-frb400-encore                                        
              end-if                                                            
           end-if.                                                              
      *                                                                         
      *{ Tr-Empty-Sentence 1.6                                                  
      *               .                                                         
      *}                                                                        
       FIN-lec-frb400. EXIT.                                                    
      *                                                                         
       mise-en-forme                     SECTION.                               
      *                                                                         
             perform varying i from 1 by 1 until i > 80                         
                move 0 to ws-pvente (i)                                         
             end-perform                                                        
      *                                                                         
           UNSTRING FRB400-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
      *   CODE CLIENT                                                           
             MESRB01-CCLIENT                                                    
      *   CIVILIT�                                                              
             MESRB01-CIVILITE                                                   
      *   NOM CLIENT                                                            
             MESRB01-NOMCLI                                                     
      *   PR�NOM CLIENT                                                         
             MESRB01-PRENOM                                                     
      *   T�L�PHONE FIXE                                                        
             MESRB01-TELFIXE                                                    
      *   T�L�PHONE PORTABLE                                                    
             MESRB01-TELPORT                                                    
      *   T�L�PHONE BUREAU                                                      
             MESRB01-TELBUR                                                     
      *   POSTE BUREAU                                                          
             MESRB01-PSTBUR                                                     
      *   E-MAIL                                                                
             MESRB01-EMAIL                                                      
      *   ADRESSES FACTURATION ET LIVRAISON                                     
      *      MESRB01-ADRESSE OCCURS 2.                                          
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)                   
                MESRB01-TYPADR  (01)                                            
      *      NUM�RO DE VOIE                                                     
                MESRB01-NUMVOIE (01)                                            
      *      TYPE DE VOIE                                                       
                MESRB01-TYPVOIE (01)                                            
      *      NOM DE VOIE                                                        
      *         MESRB01-NOMVOIE (01)                                            
                ws-MESRB01-NOMVOIE1                                             
      *      COMPL�MENT ADRESSE DE LIVRAISON 1                                  
                MESRB01-CADRFAC1(01)                                            
      *      COMPL�MENT ADRESSE DE LIVRAISON2                                   
                MESRB01-CADRFAC2(01)                                            
      *      B�TIMENT                                                           
                MESRB01-BATIMENT(01)                                            
      *      ESCALIER                                                           
                MESRB01-ESCALIER(01)                                            
      *      �TAGE                                                              
                MESRB01-ETAGE   (01)                                            
      *      PORTE                                                              
                MESRB01-PORTE   (01)                                            
      *      CODE PAYS                                                          
                MESRB01-CPAYS   (01)                                            
      *      CODE INSEE                                                         
                MESRB01-CINSEE  (01)                                            
      *      COMMUNE                                                            
                MESRB01-COMMUNE (01)                                            
      *      CODE POSTAL                                                        
                MESRB01-CPOSTAL (01)                                            
      *      CODE client ucm                                                    
                MESRB01-idcliucm(01)                                            
      *      CODE adresse ucm                                                   
                MESRB01-idadrucm(01)                                            
      *                                                                         
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)                   
                MESRB01-TYPADR  (02)                                            
      *      NUM�RO DE VOIE                                                     
                MESRB01-NUMVOIE (02)                                            
      *      TYPE DE VOIE                                                       
                MESRB01-TYPVOIE (02)                                            
      *      NOM DE VOIE                                                        
      *         MESRB01-NOMVOIE (02)                                            
                ws-MESRB01-NOMVOIE2                                             
      *      COMPL�MENT ADRESSE DE LIVRAISON 1                                  
                MESRB01-CADRFAC1(02)                                            
      *      COMPL�MENT ADRESSE DE LIVRAISON2                                   
                MESRB01-CADRFAC2(02)                                            
      *      B�TIMENT                                                           
                MESRB01-BATIMENT(02)                                            
      *      ESCALIER                                                           
                MESRB01-ESCALIER(02)                                            
      *      �TAGE                                                              
                MESRB01-ETAGE   (02)                                            
      *      PORTE                                                              
                MESRB01-PORTE   (02)                                            
      *      CODE PAYS                                                          
                MESRB01-CPAYS   (02)                                            
      *      CODE INSEE                                                         
                MESRB01-CINSEE  (02)                                            
      *      COMMUNE                                                            
                MESRB01-COMMUNE (02)                                            
      *      CODE POSTAL                                                        
                MESRB01-CPOSTAL (02)                                            
      *      CODE client ucm                                                    
                MESRB01-idcliucm(02)                                            
      *      CODE adresse ucm                                                   
                MESRB01-idadrucm(02)                                            
      *                                                                         
      *      DONN�ES REDBOX                                                     
      *                                                                         
      *   N� TRANSACTION REDBOX                                                 
             MESRB01-TREDBOX                                                    
      *   N� VENTE GV                                                           
             MESRB01-NVENTEGV                                                   
      *   N� DE CONTRAT                                                         
             MESRB01-NCONTRAT                                                   
      *   ST� DE TRANSACTION                                                    
             MESRB01-TSOC                                                       
      *   LIEU DE TRANSACTION                                                   
             MESRB01-TLIEU                                                      
      *   DATE DE TRANSACTION                                                   
             MESRB01-TDATE                                                      
      *   HEURE DE TRANSACTION                                                  
             MESRB01-THEURE                                                     
      *   TYPE D'ENLEVEMENT ('M' / 'E')                                         
             MESRB01-TENLV                                                      
      *                                                                         
      *      DONN�ES LIGNE DE VENTE                                             
      *                                                                         
      *   *                                                                     
      *      MESRB01-DETVENTE OCCURS 80.                                        
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (01)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (01)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (01)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (01)                                         
      *      PRIX DE VENTE                                                      
                WS-PVENTE     (01)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (01)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (01)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (01)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (01)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (01)                                         
      *                                                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (02)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (02)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (02)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (02)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (02)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (02)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (02)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (02)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (02)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (02)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (03)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (03)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (03)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (03)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (03)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (03)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (03)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (03)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (03)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (03)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (04)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (04)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (04)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (04)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (04)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (04)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (04)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (04)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (04)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (04)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (05)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (05)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (05)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (05)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (05)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (05)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (05)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (05)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (05)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (05)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (06)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (06)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (06)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (06)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (06)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (06)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (06)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (06)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (06)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (06)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (07)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (07)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (07)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (07)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (07)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (07)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (07)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (07)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (07)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (07)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (08)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (08)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (08)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (08)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (08)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (08)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (08)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (08)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (08)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (08)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (09)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (09)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (09)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (09)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (09)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (09)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (09)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (09)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (09)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (09)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (10)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (10)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (10)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (10)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (10)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (10)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (10)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (10)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (10)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (10)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (11)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (11)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (11)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (11)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (11)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (11)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (11)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (11)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (11)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (11)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (12)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (12)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (12)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (12)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (12)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (12)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (12)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (12)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (12)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (12)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (13)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (13)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (13)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (13)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (13)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (13)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (13)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (13)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (13)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (13)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (14)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (14)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (14)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (14)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (14)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (14)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (14)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (14)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (14)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (14)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (15)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (15)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (15)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (15)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (15)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (15)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (15)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (15)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (15)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (15)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (16)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (16)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (16)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (16)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (16)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (16)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (16)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (16)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (16)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (16)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (17)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (17)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (17)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (17)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (17)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (17)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (17)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (17)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (17)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (17)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (18)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (18)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (18)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (18)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (18)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (18)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (18)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (18)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (18)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (18)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (19)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (19)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (19)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (19)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (19)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (19)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (19)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (19)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (19)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (19)                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (20)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (20)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (20)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (20)                                         
      *      PRIX DE VENTE                                                      
                ws-PVENTE     (20)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (20)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (20)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (20)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (20)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (20).                                        
      *                                                                         
             perform varying i from 1 by 1 until i > 80                         
                compute mesrb01-pvente (i) =                                    
                     ws-pvente (i) / 100                                        
             end-perform                                                        
      *                                                                         
                      .                                                         
       FIN-en-forme. EXIT.                                                      
      *                                                                         
      *                                                                         
       mise-en-forme2                    SECTION.                               
      *                                                                         
             perform varying i from 1 by 1 until i > 80                         
                move 0 to ws-pvente (i)                                         
             end-perform                                                        
      *   CODE CLIENT                                                           
             move spaces to MESRB01-CCLIENT                                     
      *   CIVILIT�                                                              
             move spaces to MESRB01-CIVILITE                                    
      *   NOM CLIENT                                                            
             move spaces to MESRB01-NOMCLI                                      
      *   PR�NOM CLIENT                                                         
             move spaces to MESRB01-PRENOM                                      
      *   T�L�PHONE FIXE                                                        
             move spaces to MESRB01-TELFIXE                                     
      *   T�L�PHONE PORTABLE                                                    
             move spaces to MESRB01-TELPORT                                     
      *   T�L�PHONE BUREAU                                                      
             move spaces to MESRB01-TELBUR                                      
      *   POSTE BUREAU                                                          
             move spaces to MESRB01-PSTBUR                                      
      *   E-MAIL                                                                
             move spaces to MESRB01-EMAIL                                       
      *   ADRESSES FACTURATION ET LIVRAISON                                     
      *      MESRB01-ADRESSE OCCURS 2.                                          
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)                   
                move spaces to MESRB01-TYPADR (01)                              
      *      NUM�RO DE VOIE                                                     
                move spaces to MESRB01-NUMVOIE (01)                             
      *      TYPE DE VOIE                                                       
                move spaces to MESRB01-TYPVOIE (01)                             
      *      NOM DE VOIE                                                        
                move spaces to MESRB01-NOMVOIE (01)                             
      *      COMPL�MENT ADRESSE DE LIVRAISON 1                                  
                move spaces to MESRB01-CADRFAC1(01)                             
      *      COMPL�MENT ADRESSE DE LIVRAISON2                                   
                move spaces to MESRB01-CADRFAC2(01)                             
      *      B�TIMENT                                                           
                move spaces to MESRB01-BATIMENT(01)                             
      *      ESCALIER                                                           
                move spaces to MESRB01-ESCALIER(01)                             
      *      �TAGE                                                              
                move spaces to MESRB01-ETAGE (01)                               
      *      PORTE                                                              
                move spaces to MESRB01-PORTE (01)                               
      *      CODE PAYS                                                          
                move spaces to MESRB01-CPAYS (01)                               
      *      CODE INSEE                                                         
                move spaces to MESRB01-CINSEE (01)                              
      *      COMmove spaces to MUNE                                             
                move spaces to MESRB01-COMMUNE (01)                             
      *      CODE POSTAL                                                        
                move spaces to MESRB01-CPOSTAL (01)                             
      *      CODE client ucm                                                    
                move spaces to MESRB01-idcliucm(01)                             
      *      CODE adresse ucm                                                   
                move spaces to MESRB01-idadrucm(01)                             
      *                                                                         
      *      TYPE D'ADRESSE (OCCURS 1 ==> A ; OCCURS 2 ==> B)                   
                move spaces to MESRB01-TYPADR (02)                              
      *      NUM�RO DE VOIE                                                     
                move spaces to MESRB01-NUMVOIE (02)                             
      *      TYPE DE VOIE                                                       
                move spaces to MESRB01-TYPVOIE (02)                             
      *      NOM DE VOIE                                                        
                move spaces to MESRB01-NOMVOIE (02)                             
      *      COMPL�MENT ADRESSE DE LIVRAISON 1                                  
                move spaces to MESRB01-CADRFAC1(02)                             
      *      COMPL�MENT ADRESSE DE LIVRAISON2                                   
                move spaces to MESRB01-CADRFAC2(02)                             
      *      B�TIMENT                                                           
                move spaces to MESRB01-BATIMENT(02)                             
      *      ESCALIER                                                           
                move spaces to MESRB01-ESCALIER(02)                             
      *      �TAGE                                                              
                move spaces to MESRB01-ETAGE (02)                               
      *      PORTE                                                              
                move spaces to MESRB01-PORTE (02)                               
      *      CODE PAYS                                                          
                move spaces to MESRB01-CPAYS (02)                               
      *      CODE INSEE                                                         
                move spaces to MESRB01-CINSEE (02)                              
      *      COMmove spaces to MUNE                                             
                move spaces to MESRB01-COMMUNE (02)                             
      *      CODE POSTAL                                                        
                move spaces to MESRB01-CPOSTAL (02)                             
      *      CODE client ucm                                                    
                move spaces to MESRB01-idcliucm(02)                             
      *      CODE adresse ucm                                                   
                move spaces to MESRB01-idadrucm(02)                             
      *                                                                         
      *      DONN�ES REDBOX                                                     
      *                                                                         
      *                                                                         
           UNSTRING FRB400-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
      *   N� TRANSACTION REDBOX                                                 
             MESRB01-TREDBOX                                                    
      *   N� VENTE GV                                                           
             MESRB01-NVENTEGV                                                   
      *   N� DE CONTRAT                                                         
             MESRB01-NCONTRAT                                                   
      *   ST� DE TRANSACTION                                                    
             MESRB01-TSOC                                                       
      *   LIEU DE TRANSACTION                                                   
             MESRB01-TLIEU                                                      
      *   DATE DE TRANSACTION                                                   
             MESRB01-TDATE                                                      
      *   HEURE DE TRANSACTION                                                  
             MESRB01-THEURE                                                     
      *   TYPE D'ENLEVEMENT ('M' / 'E')                                         
             MESRB01-TENLV                                                      
      *                                                                         
      *      DONN�ES LIGNE DE VENTE                                             
      *                                                                         
      *   *                                                                     
      *      MESRB01-DETVENTE OCCURS 80.                                        
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                MESRB01-TTRAIT     (01)                                         
      *      CODE OFFRE                                                         
                MESRB01-COFFRE     (01)                                         
      *      CODIC OU CODE PRESTATION                                           
                MESRB01-CPREST     (01)                                         
      *      QUANTIT�                                                           
                MESRB01-QUANTITE   (01)                                         
      *      PRIX DE VENTE                                                      
                WS-PVENTE     (01)                                              
      *      MODE DE D�LIVRANCE                                                 
                MESRB01-CMODDEL    (01)                                         
      *      DATE DE D�LIVRANCE                                                 
                MESRB01-DDELIV     (01)                                         
      *      FILIALE DU VENDEUR                                                 
                MESRB01-FVEND      (01)                                         
      *      MATRICULE DU VENDEUR                                               
                MESRB01-CVEND      (01)                                         
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                MESRB01-NSEREQ     (01)                                         
      *                                                                         
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (02)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (02)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (02)                              
      *      QUANTIT�                                                           
                move 0      TO MESRB01-QUANTITE (02)                            
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (02)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (02)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (02)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (02)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (02)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (02)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (03)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (03)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (03)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (03)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (03)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (03)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (03)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (03)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (03)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (03)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (04)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (04)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (04)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (04)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (04)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (04)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (04)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (04)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (04)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (04)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (05)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (05)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (05)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (05)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (05)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (05)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (05)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (05)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (05)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (05)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (06)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (06)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (06)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (06)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (06)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (06)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (06)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (06)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (06)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (06)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (07)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (07)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (07)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (07)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (07)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (07)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (07)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (07)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (07)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (07)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (08)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (08)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (08)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (08)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (08)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (08)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (08)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (08)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (08)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (08)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (09)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (09)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (09)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (09)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (09)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (09)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (09)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (09)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (09)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (09)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (10)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (10)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (10)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (10)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (10)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (10)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (10)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (10)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (10)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (10)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (11)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (11)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (11)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (11)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (11)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (11)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (11)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (11)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (11)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (11)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (12)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (12)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (12)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (12)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (12)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (12)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (12)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (12)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (12)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (12)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (13)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (13)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (13)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (13)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (13)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (13)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (13)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (13)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (13)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (13)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (14)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (14)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (14)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (14)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (14)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (14)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (14)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (14)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (14)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (14)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (15)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (15)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (15)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (15)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (15)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (15)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (15)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (15)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (15)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (15)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (16)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (16)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (16)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (16)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (16)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (16)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (16)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (16)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (16)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (16)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (17)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (17)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (17)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (17)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (17)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (17)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (17)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (17)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (17)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (17)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (18)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (18)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (18)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (18)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (18)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (18)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (18)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (18)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (18)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (18)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (19)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (19)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (19)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (19)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (19)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (19)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (19)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (19)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (19)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (19)                              
      *      TYPE DE TRAITEMENT   ('A' / 'C')                                   
                move spaces to MESRB01-TTRAIT (20)                              
      *      CODE OFFRE                                                         
                move spaces to MESRB01-COFFRE (20)                              
      *      CODIC OU CODE PRESTATION                                           
                move spaces to MESRB01-CPREST (20)                              
      *      QUANTIT�                                                           
                move 0       to MESRB01-QUANTITE (20)                           
      *      PRIX DE VENTE                                                      
                move 0       to WS-PVENTE (20)                                  
      *      MODE DE D�LIVRANCE                                                 
                move spaces to MESRB01-CMODDEL (20)                             
      *      DATE DE D�LIVRANCE                                                 
                move spaces to MESRB01-DDELIV (20)                              
      *      FILIALE DU VENDEUR                                                 
                move spaces to MESRB01-FVEND (20)                               
      *      MATRICULE DU VENDEUR                                               
                move spaces to MESRB01-CVEND (20)                               
      *      N� DE S�RIE DE L'�QUIPEMENT                                        
                move spaces to MESRB01-NSEREQ (20).                             
      *                                                                         
             perform varying i from 1 by 1 until i > 80                         
                compute mesrb01-pvente (i) =                                    
                     ws-pvente (i) / 100                                        
             end-perform                                                        
      *                                                                         
                      .                                                         
       FIN-en-forme2. EXIT.                                                     
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      * R�CUP�RATION DE LA DATE SYST�ME.                                        
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
