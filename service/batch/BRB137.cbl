      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
      *                                                                 00000020
       PROGRAM-ID.  BRB137.                                             00000030
       AUTHOR.  DSA042.                                                 00000040
      *                                                                 00000060
      ***************************************************************** 00000070
      *   EXTRACTION PRESTATTIONS PRISE DE RENDEZ-VOUS                * 00000090
      ***************************************************************** 00000120
      *   MISE EN FORME FICHIER EXTRACTION                            *         
      *****************************************************************         
      *  MODIFIXATION :                                               *         
      *  --------------                                               *         
      *  XX.XX.XX    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                  *         
      *****************************************************************         
      *                                                                 00000130
       ENVIRONMENT DIVISION.                                            00000140
       CONFIGURATION SECTION.                                           00000150
      *SPECIAL-NAMES.                                                   00000160
      *    DECIMAL-POINT IS COMMA.                                      00000170
      *                                                                 00000180
       INPUT-OUTPUT SECTION.                                            00000190
       FILE-CONTROL.                                                    00000200
      *                                                                 00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE  ASSIGN  TO   FDATE.                           00000230
      *--                                                                       
           SELECT  FDATE  ASSIGN  TO   FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRB136 ASSIGN  TO   FRB136.                          00000240
      *--                                                                       
           SELECT  FRB136 ASSIGN  TO   FRB136                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRB137 ASSIGN  TO   FRB137                           00000240
      *            FILE STATUS IS FRB137-STATUS.                        00000241
      *                                                                         
      *--                                                                       
           SELECT  FRB137 ASSIGN  TO   FRB137                                   
                   FILE STATUS IS FRB137-STATUS                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
       DATA DIVISION.                                                   00000260
      *                                                                 00000270
       FILE SECTION.                                                    00000280
      *                                                                 00000290
       FD  FDATE                                                        00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FILLEUR     PIC X(80).                                       00000480
      *                                                                 00000510
       FD   FRB136                                                      00000550
            RECORDING F                                                 00000560
            BLOCK 0  RECORDS                                            00000570
            LABEL RECORD STANDARD.                                      00000580
       01   ENR-FRB136   PIC X(520).                                    00000590
      *                                                                 00000610
       FD   FRB137                                                      00000550
           RECORDING V                                                  00000560
           BLOCK 0 RECORDS                                              00000570
           LABEL RECORD STANDARD                                        00000580
           RECORD CONTAINS 1 TO 520 CHARACTERS                          00000590
           DATA RECORD IS FRB007-ENR.                                   00000600
       01  FRB007-ENR.                                                          
           05  FRB007-ENREGISTREMENT  PICTURE X(01)                             
                                      OCCURS 1 TO 520 TIMES                     
                                      DEPENDING ON I-LONG.                      
      *                                                                 00000610
      *                                                                 00000620
      *                                                                 00000620
      ***************************************************************** 00000630
      *                 W O R K I N G  -  S T O R A G E               * 00000640
      ***************************************************************** 00000650
       WORKING-STORAGE SECTION.                                         00000660
      *------------------------                                         00000670
      *                                                                 00000680
      *                                                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  I                COMP       PIC  S9(4).                      00000740
      *--                                                                       
       77  I COMP-5       PIC  S9(4).                                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J                COMP       PIC  S9(4).                      00000740
      *--                                                                       
       77  J COMP-5       PIC  S9(4).                                           
      *}                                                                        
       01  I-LONG      PIC S9(3)  COMP-3 VALUE 0.                               
       01  I-ZONE      PIC S9(3)  COMP-3 VALUE 0.                               
       01  I-TEXTE     PIC S9(3)  COMP-3 VALUE 0.                               
       01  I-ENREG-MAX PIC S9(3)  COMP-3 VALUE 520.                             
       77  W-DSYST     PIC S9(13) COMP-3 VALUE 0.                       00380001
       01  WS-ENREGISTREMENT      PIC X(520).                                   
       01  ZONE-EDIT              PIC X(520).                                   
       01  ZONEX                  PIC X(520).                                   
      *                                                                 00000750
      *===============================================================* 00000690
      *                 DESCRIPTION DE FRB136                         * 00000700
      *===============================================================* 00000710
      *                                                                 00000810
       01  FRB136-STATUS               PIC 99 VALUE ZERO.               00000820
       01  FRB137-STATUS               PIC 99 VALUE ZERO.               00000820
       COPY FRB135.                                                             
      *                                                                 00000880
      *                                                                 00000880
      *                                                                 00000810
       01  W-LIGNES-FRB136-LUES          PIC S9(9) COMP-3  VALUE +0.    00001700
       01  W-LIGNES-FRB137-ECRITES       PIC S9(9) COMP-3  VALUE +0.    00001700
      *                                                                 00001470
      *                                                                 00002260
      *                                                                 00001800
       01 FIC-FRB136          PIC X VALUE '0'.                                  
          88 FIN-FRB136       VALUE '1'.                                        
      ***************************************************************** 00002270
      *                                                                 00002750
           COPY  ABENDCOP.                                              00002760
      *                                                                 00002770
      *************************************************                 00500001
      * MODULE DE CONTROLE ET DE TRAITEMENT DES DATES *                 00510001
      *************************************************                 00520001
      *                                                                 00530001
      *---  ZONES. POUR CALCUL DSYST----------------------------------  00540001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-RET            PIC S9(04) COMP.                            00550001
      *--                                                                       
       01  W-RET            PIC S9(04) COMP-5.                                  
      *}                                                                        
       01  W-DATHEUR        PIC S9(13) COMP-3.                          00560001
       01  W-MESS-SPDATDB2.                                             00570001
           05  W-MESS-SPDAT PIC X(32) VALUE                             00580001
                'RETOUR SPDATDB2 ANORMAL, CODE : '.                     00590001
           05  WRET-PIC     PIC 9(4).                                   00600001
                                                                        00610001
           COPY  WORKDATC.                                              00002780
           COPY  SYKWMONT.                                                      
           EJECT                                                        00002790
      *                                                                 00002800
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00002810
      *     DESCRITIF DES ZONES SPECIFIQUES A DB2                       00002820
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00002830
      *                                                                 00002840
      *                                                                 00003050
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
       01  WS-SSAAMMJJ                     PIC X(08)   VALUE SPACES.            
       01  WS-HHMMSSCC   .                                                      
          05  WS-HHMMSS                       PIC X(06) .                       
          05  WS-CC                           PIC X(02) .                       
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
      ******************************************************************        
      *  ZONES FICHIERS EN SORTIE                                       00003060
      ******************************************************************        
       01 W-ENTETE1 PIC X(100)  value                                           
            'code rayon;intitul� raron;s�quence rayon;code famille;intit        
      -    'ul� famille;s�quence famille;code marque;'.                         
       01 W-ENTETE2 PIC X(125)  VALUE                                           
            'intitul� marque;codic;intitul�;s�quence;�puis�;prix;afficha        
      -    'ge;code rdv;codic rattachement;quantit� max;infos compl�ment        
      -    'aires '.                                                            
      *                                                                 00003060
      ***************************************************************** 00003070
      *             P R O C E D U R E   D I V I S I O N               * 00003080
      ***************************************************************** 00003090
      *                                                                 00003100
       PROCEDURE DIVISION.                                              00003110
      *                                                                 00003120
      *---------------------------------------------------------------* 00003130
      *              D E B U T   B S V 0 1 0                          * 00003140
      *---------------------------------------------------------------* 00003150
      *==============================================================*  00003190
      *                                                                 00003200
      *                                                                 00003210
       MODULE-BRB137       SECTION.                                     00003220
      *---------------                                                  00003230
      *                                                                 00003240
           PERFORM  DEBUT.                                              00003250
           PERFORM  TRAITEMENT UNTIL FIN-FRB136                         00003260
           PERFORM  FIN.                                                00003270
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003280
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00003290
       FIN-MODULE-BRB137.  EXIT.                                        00003300
              EJECT                                                     00003310
      *                                                                 00003320
      *===============================================================* 00003330
      *                     D E B U T                                 * 00003340
      *===============================================================* 00003350
      *                                                                 00003360
       DEBUT                 SECTION.                                   00003370
      *------------------------------                                   00003380
      *                                                                 00003390
           DISPLAY  '************************************************'. 00003400
           DISPLAY  '**                                            **'. 00003410
           DISPLAY  '**   PROGRAMME  :  BRB137                     **'. 00003420
           DISPLAY  '**   CREATION DU FICHIER FRB007               **'. 00003430
           DISPLAY  '************************************************'. 00003450
           DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'. 00003460
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '* VERSION DU 14/01/2009 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *                                                                 00003470
           OPEN  OUTPUT FRB137                                          00003490
           OPEN INPUT FRB136                                            00003840
      *---------------------------------------------------------------* 00003820
      * DSYST                                                           00003830
      *---------------------------------------------------------------* 00003820
      *                                                                 00003830
           CALL 'SPDATDB2' USING W-RET W-DATHEUR.                       01200001
           IF W-RET NOT = 0                                             01210001
              MOVE W-RET           TO WRET-PIC                          01211001
              MOVE W-MESS-SPDATDB2 TO ABEND-MESS                        01212001
              PERFORM ABANDON-PROGRAMME                                 01213001
           END-IF.                                                      01214001
           MOVE W-DATHEUR TO W-DSYST.                                   01215001
                                                                        01220000
      *--- LECTURE PREMIER  ENREGISTREMENT                              01220000
           PERFORM LECTURE-FRB136.                                      01220000
      *--- ECRITURE ENTETE                                                      
           STRING W-ENTETE1 W-ENTETE2 DELIMITED BY SIZE  INTO                   
      *                              WS-ENREGISTREMENT                  00001430
                                     ZONE-EDIT.                                 
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT         TO  WS-ENREGISTREMENT                        
           PERFORM CALCULER-LONGUEUR.                                           
           WRITE  FRB007-ENR FROM WS-ENREGISTREMENT.                            
      *                                                                 00001430
       FIN-DEBUT.                 EXIT.                                 00004820
      *==============================================================*  00005080
      *                  T R A I T E M E N T                         *  00005090
      *==============================================================*  00005100
       TRAITEMENT    SECTION.                                           00005110
      *                                                                 00005120
           INITIALIZE ZONE-EDIT                                                 
           MOVE  FRB135-ENR        TO  ZONE-EDIT                                
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT         TO  WS-ENREGISTREMENT                        
           PERFORM CALCULER-LONGUEUR                                            
           WRITE  FRB007-ENR FROM WS-ENREGISTREMENT                             
           ADD 1 TO   W-LIGNES-FRB137-ECRITES                                   
      *--- LECTURE SUIVANTE                                                     
           PERFORM LECTURE-FRB136.                                              
      *                                                                 00006760
       FIN-TRAITEMENT.        EXIT.                                     00006780
      *                                                                 00006760
       MISE-EN-FORME-ENREG  SECTION.                                            
      *                                                                         
      *--- SUPPRESSION DE BLANCS NON SIGNIFICATIFS                              
           INSPECT ZONE-EDIT   REPLACING ALL LOW-VALUE BY SPACE                 
           PERFORM VARYING I-TEXTE FROM 1 BY 1                                  
             UNTIL (I-TEXTE > I-ENREG-MAX)                                      
                OR (ZONE-EDIT(I-TEXTE:) = ' ')                                  
              IF ZONE-EDIT(I-TEXTE:2) = '  '                                    
              OR ZONE-EDIT(I-TEXTE:2) = ' ;'                                    
                 COMPUTE I-ZONE = I-TEXTE + 1                                   
                 MOVE ZONE-EDIT(I-ZONE:) TO ZONEX                               
                 MOVE ZONEX TO ZONE-EDIT(I-TEXTE:)                              
                 IF I-TEXTE > 1                                                 
                    COMPUTE I-TEXTE = I-TEXTE - 1                               
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-MISE-EN-FORME-ENREG. EXIT.                                           
      *                                                                         
       CALCULER-LONGUEUR.                                                       
      *------------------                                                       
      *--- POUR CINNAITRE A LONGUEYR ON PART DE LA FIN DE CHAINE ET             
      *--- ON REMONTE JUSQU AU PREMIER ';'                                      
           MOVE I-ENREG-MAX    TO I                                             
      *    PERFORM UNTIL WS-ENREGISTREMENT(I:1) = ';'                           
           PERFORM UNTIL WS-ENREGISTREMENT(I:1) NOT = ' '                       
              SUBTRACT 1 FROM I                                                 
           END-PERFORM.                                                         
           COMPUTE I-LONG = I.                                                  
       FIN-CALCULER-LONGUEUR.  EXIT.                                            
      *===============================================================* 00004840
      *       F I N   N O R M A L E   D U   P R O G R A M M E         * 00004850
      *===============================================================* 00004860
      *                                                                 00004870
       FIN                        SECTION.                              00004880
      *-----------------------------------                              00004890
      *                                                                 00004900
           DISPLAY  '************************************************'. 00004910
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'. 00004920
           DISPLAY  '***                 brb137                   ***'. 00004930
           DISPLAY  '************************************************'. 00004940
           DISPLAY  '** NBR DE LIGNES FRB136 LUES          : '          00004970
                    W-LIGNES-FRB136-LUES.                               00004980
           DISPLAY  '** NBR DE LIGNES FRB137 ECRITES       : '          00004970
                    W-LIGNES-FRB137-ECRITES.                            00004980
      *                                                                 00004990
           CLOSE  FRB136 .                                              00005000
           IF FRB136-STATUS NOT = 0                                     00005001
              STRING 'STATUS=' FRB136-STATUS ' PB CLOSE FRB136'         00005002
              DELIMITED BY SIZE INTO ABEND-MESS PERFORM                 00005003
              ABANDON-PROGRAMME                                         00005004
           END-IF.                                                      00005005
           CLOSE  FRB137 .                                              00005000
           IF FRB137-STATUS NOT = 0                                     00005001
              STRING 'STATUS=' FRB137-STATUS ' PB CLOSE FRB137'         00005002
              DELIMITED BY SIZE INTO ABEND-MESS PERFORM                 00005003
              ABANDON-PROGRAMME                                         00005004
           END-IF.                                                      00005005
      *                                                                 00005050
       FIN-FIN.        EXIT.                                            00005060
      *                                                                 00005070
      *                                                                 00006800
      *                                                                 00006810
      *                                                                 00006940
       LECTURE-FRB136 SECTION.                                                  
      *-----------------------                                                  
           READ FRB136 INTO FRB135-ENR                                          
               AT END SET FIN-FRB136  TO TRUE                                   
      *               DISPLAY 'FIN FRB136'                                      
           NOT AT END ADD 1 TO W-LIGNES-FRB136-LUES                             
           END-READ                                                             
           IF FRB136-STATUS  NOT = 00 AND 10                                    
              MOVE 'PROBLEME LECTURE FRB136' TO ABEND-MESS                      
              MOVE FRB136-STATUS             TO ABEND-CODE                      
              PERFORM ABANDON-PROGRAMME                                         
           END-IF                                                               
           .                                                                    
       FIN-LECTURE-FFT21D. EXIT.                                                
      *                                                                 00006940
      *                                                                 00006950
      *                                                                 00012370
      *                                                                 00012380
      *_________________________________________________                00012390
      *                                                                 00012400
      *    PROCEDURE D'ABANDON                                          00012410
      *                                                                 00012420
      *_________________________________________________                00012430
      *                                                                 00012440
      *                                                                 00012450
      *---------------------------------------------------------------* 00012460
      *             A B A N D O N - P R O G R A M M E                 * 00012470
      *---------------------------------------------------------------* 00012480
      *                                                                 00012490
       ABANDON-PROGRAMME        SECTION.                                00012500
      *---------------------------------                                00012510
      *                                                                 00012520
      *                                                                 00012550
           DISPLAY '***************************************'.           00012560
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.           00012570
           DISPLAY '***************************************'.           00012580
           CLOSE FRB136 FRB137.                                                 
      *                                                                 00012550
           MOVE  'BRB137'  TO    ABEND-PROG.                            00012600
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00012610
                                                                        00012620
      *                                                                 00012630
       FIN-ABANDON-PROGRAMME. EXIT.                                     00012640
      *COPY SYKPMONT.                                                           
          EJECT                                                         00012650
      *                                                                 00012660
