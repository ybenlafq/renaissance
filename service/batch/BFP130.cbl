      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BFP130.                                                 
       AUTHOR.          M.M.                                                    
      ******************************************************************        
      * BFP130 * LISTE DE CONTROLE DES FACTURES RETROCEDEES ET NON     *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFP120  ASSIGN TO FFP120.                                    
      *--                                                                       
            SELECT FFP120  ASSIGN TO FFP120                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT JFP130  ASSIGN TO JFP130.                                    
      *--                                                                       
            SELECT JFP130  ASSIGN TO JFP130                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FFP120                                                               
      *---LONGUEUR = 250                                                        
            RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.                  
            COPY FFP050.                                                        
      *                                                                         
       FD  JFP130 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  JFP130-FICHIER PIC X(133).                                           
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      **  ZONES DE TRAVAIL                                                      
      ******************************************************************        
       01  W-PAGE                   PIC 999 VALUE  0.                           
       01  W-LIGNE                  PIC 99  VALUE  99.                          
       01  W-MAX-DETAIL             PIC 99  VALUE  60.                          
      *                                                                         
       01  TOT-SAV                  PIC 9(10)V99 VALUE 0.                       
       01  TOT-RA9                  PIC 9(10)V99 VALUE 0.                       
      * DATE ET HEURE                                                           
       01 W-TIME.                                                               
           03 W-HEURE               PIC 9(2).                                   
           03 W-MINUTE              PIC 9(2).                                   
           03 W-SECONDE             PIC 9(2).                                   
           03 FILLER                PIC 9(2).                                   
       01 W-DATE                    PIC 9(6).                                   
      *                                                                         
       01  W-NFACTURE-SUP.                                                      
           03  W-NSOCDEST          PIC  X(03).                                  
           03  W-NFACTURE          PIC  X(10).                                  
           03  FILLER              PIC  X(02).                                  
      *                                                                         
       01  W-NSINISTRE.                                                         
           03  W-NSINI1            PIC  X(10).                                  
           03  W-NSINI2            PIC  X(03).                                  
           03  W-NSINI3            PIC  X(03).                                  
      *                                                                         
      * REFERENCE DOCUMENT                                                      
       01 W-FFP050-REFBOR.                                                      
          03 W-FFP050-REFBOR-SSAA   PIC 9(04).                                  
          03 W-FFP050-REFBOR-MM     PIC 9(02).                                  
          03 W-FFP050-REFBOR-JJ     PIC 9(02).                                  
      *                                                                         
      * COMPTEURS                                                               
       01  CPT-FFP120               PIC 9999    VALUE 0.                        
       01  CPT-EDIT                 PIC Z(3)9.                                  
       01  TOT-A-REGLER-NSOCCOMPT   PIC 9(10)V99 VALUE 0.                       
      *                                                                         
      * RUPTURES NSOCCOMPT                                                      
      *                                                                         
       01  RPT-NSOCCOMPT-LU         PIC X(03)   VALUE SPACES.                   
       01  RPT-NSOCCOMPT-STOCK      PIC X(03)   VALUE SPACES.                   
      *                                                                         
      * FLAG POUR TRAITEMENT                                                    
      *                                                                         
       01  LECTURE-FFP120           PIC 9       VALUE 0.                        
           88  TERMINE-FFP120                   VALUE 1.                        
      *                                                                         
      ******************************************************************        
      * DESCRIPTION DE L'ETAT  -> JFP130                                        
      ******************************************************************        
      *** ENTETE LIGNE N�1                                                      
       01  JFP050-ENTETE1.                                                      
          03  FILLER              PIC X(001) VALUE '1'.                         
          03  FILLER              PIC X(006) VALUE 'JFP130'.                    
          03  FILLER              PIC X(011) VALUE SPACES.                      
          03  FILLER              PIC X(045) VALUE                              
              '       REFACTURATION VIA LE NETTING  '.                          
          03  FILLER              PIC X(028) VALUE SPACES.                      
          03  FILLER              PIC X(009) VALUE                              
              'EDITE LE'.                                                       
          03  JFP050-DATE-JJ      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP050-DATE-MM      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP050-DATE-SS      PIC X(002) VALUE SPACES.                      
          03  JFP050-DATE-AA      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE ' A '.                       
          03  JFP050-HEURE        PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE 'H'.                         
          03  JFP050-MINUTE       PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(006) VALUE ' PAGE '.                    
          03  JFP050-PAGE         PIC ZZZ9.                                     
      **                                                                        
       01  JFP050-ENTETE2.                                                      
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  FILLER              PIC X(024) VALUE                              
              '      SOCIETE         : '.                                       
          03  JFP050-NSOCCOMPT    PIC X(005) VALUE SPACES.                      
          03  JFP050-LIBNSOC      PIC X(025) VALUE SPACES.                      
          03  FILLER              PIC X(073) VALUE SPACES.                      
       01  JFP050-ENTETE4.                                                      
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  FILLER              PIC X(024) VALUE                              
              '             LOT N�   : '.                                       
          03  JFP050-NLOT         PIC X(006) VALUE SPACES.                      
          03  FILLER              PIC X(017) VALUE                              
              '  A LA DATE DU : '.                                              
          03  JFP050-FDATE-JJ     PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP050-FDATE-MM     PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP050-FDATE-SSAA   PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(075) VALUE SPACES.                      
       01  JFP050-TRAIT-VIDE.                                                   
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  JFP050-LIB-VIDE     PIC X(132) VALUE SPACE.                       
       01  JFP050-TRAIT.                                                        
          03  JFP050-TRAIT-ASA    PIC X(001) VALUE ' '.                         
          03  FILLER              PIC X(001) VALUE '+'.                         
          03  FILLER              PIC X(127) VALUE ALL '-'.                     
          03  FILLER              PIC X(001) VALUE '+'.                         
       01  JFP050-TRAIT-TITRE1.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '              '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                ' LIEU DE '.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                ' TYPE'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '    DATE   '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   DATE DE  '.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(025) VALUE                              
                                '                   '.                          
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '                 '.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   MONTANT '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                '        '.                                     
          03  FILLER              PIC X(001) VALUE '!'.                         
       01  JFP050-TRAIT-TITRE2.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '   N� FACTURE '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                ' GESTION '.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                '  DE '.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                'D''INTERVENT.'.                                
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                ' CREATION OU'.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(025) VALUE                              
                                '    CRI/BC = LETTRAGE '.                       
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '   N� DE SINISTRE'.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   NET TTC '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                '      '.                                       
          03  FILLER              PIC X(001) VALUE '!'.                         
       01  JFP050-TRAIT-TITRE3.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '              '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                'TIERS GCT'.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                ' FACT'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '  OU DE BC '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                ' MISE A JOUR'.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(025) VALUE                              
                                '                   '.                          
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '                 '.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   A REGLER'.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                '       '.                                      
          03  FILLER              PIC X(001) VALUE '!'.                         
      *** LIGNE                                                                 
       01  JFP050-LIGNE.                                                        
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NSOCDEST     PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NFACTURE     PIC X(010).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-TIERS        PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NLIEUTIERS   PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-CTYPINTER    PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-DFACTURE-JJ   PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP050-DFACTURE-MM   PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP050-DFACTURE-SSAA PIC X(004).                                  
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-DMAJ-JJ       PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP050-DMAJ-MM       PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP050-DMAJ-SSAA    PIC X(004).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(007) VALUE SPACES.                      
          03  JFP050-NCRI         PIC X(10).                                    
          03  FILLER              PIC X(008) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NSINI1       PIC X(10).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NSINI2       PIC X(03).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-NSINI3       PIC X(03).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  JFP050-A-REGLER     PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
      **                                                                        
       01  JFP050-TOTAL.                                                        
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(010) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACE.                       
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(25)  VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-LIBELLE      PIC X(15).                                    
          03  JFP050-NSOCTOT      PIC X(05).                                    
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  JFP050-TOT-A-REGLER PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC XX     VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
      **                                                                        
       01  JFP050-TOTAL-SR.                                                     
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(010) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACE.                       
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(25)  VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP050-LIBELLE-SR   PIC X(15).                                    
          03  FILLER              PIC X(05)  VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  JFP050-TOT-SR       PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC XX     VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
      **                                                                        
      ******************************************************************        
      **  MODULES :  ABEND                                                      
      ******************************************************************        
           COPY  ABENDCOP.                                                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND     PIC X(8)  VALUE 'ABEND'.                                   
      *--                                                                       
       01  MW-ABEND     PIC X(8)  VALUE 'ABEND'.                                
      *}                                                                        
           COPY  WORKDATC.                                                      
       01  BETDATC   PIC X(8)  VALUE 'BETDATC'.                                 
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           DISPLAY '*******************************************'                
           DISPLAY '*                 BFP130                  *'.               
           DISPLAY '*******************************************'                
      * OUVERTURE DES FICHIERS                                                  
           OPEN INPUT  FFP120.                                                  
           OPEN OUTPUT JFP130.                                                  
      *  DATE  ET HEURE                                                         
            ACCEPT  W-TIME   FROM TIME                                          
            ACCEPT  W-DATE   FROM DATE.                                         
            MOVE    W-DATE (5:2)  TO  JFP050-DATE-JJ                            
            MOVE    W-DATE (3:2)  TO  JFP050-DATE-MM                            
            MOVE    W-DATE (1:2)  TO  JFP050-DATE-AA                            
            MOVE    '20'   TO  JFP050-DATE-SS                                   
            MOVE    W-HEURE       TO  JFP050-HEURE                              
            MOVE    W-MINUTE      TO  JFP050-MINUTE.                            
      *                                                                         
      ******************************************************************        
      * TRAITEMENT                                                              
      ******************************************************************        
      * PRE-LECTURE                                                             
      *                                                                         
           PERFORM LIRE-FFP120.                                                 
           MOVE  FFP050-NSOCCOMPT    TO   RPT-NSOCCOMPT-STOCK                   
      *                                                                         
      *---------------------------------------------------------*               
      *----------- TRAITEMENT FICHIER  VIDE                                     
      *---------------------------------------------------------*               
      *                                                                         
           IF TERMINE-FFP120                                                    
              PERFORM  EDITION-FICHIER-VIDE                                     
              DISPLAY 'FICHIER FFP120 VIDE '                                    
              DISPLAY '*******************************************'             
              DISPLAY '*   LE PROGRAMME SE TERMINE NORMALEMENT   *'             
              DISPLAY '*******************************************'             
              CLOSE   FFP120 JFP130                                             
      *{ Ba-Stop-Run-Statement 1.1                                              
      *       STOP RUN                                                          
      *--                                                                       
              EXIT PROGRAM                                                      
      *}                                                                        
           END-IF.                                                              
      *                                                                         
      *----------------------------------------------------------*              
      * BOUCLE DE LECTURE                                                       
      *                                                                         
           PERFORM UNTIL TERMINE-FFP120                                         
            IF RPT-NSOCCOMPT-STOCK  NOT =  RPT-NSOCCOMPT-LU                     
               PERFORM  TOTAL-NSOC                                              
               PERFORM  TOTAL-SAV                                               
               PERFORM  TOTAL-RA9                                               
               PERFORM  ECRITURE-ENTETE                                         
               MOVE  RPT-NSOCCOMPT-LU  TO  RPT-NSOCCOMPT-STOCK                  
            END-IF                                                              
      *                                                                         
            PERFORM TRAITEMENT-FFP120                                           
            PERFORM LIRE-FFP120                                                 
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
      *------ EDITION TOTAUX                                                    
      *                                                                         
             PERFORM  TOTAL-NSOC                                                
             PERFORM  TOTAL-SAV                                                 
             PERFORM  TOTAL-RA9                                                 
      *                                                                         
      *------ FERMETURE FICHIERS                                                
      *                                                                         
           CLOSE FFP120 JFP130.                                                 
      *                                                                         
      *------ EDITION COMPTEURS                                                 
      *                                                                         
           MOVE CPT-FFP120  TO CPT-EDIT.                                        
           DISPLAY 'LECTURE ' CPT-EDIT                                          
                   ' ENREGISTREMENTS DE FFP120 '                                
           DISPLAY '*******************************************'                
           DISPLAY '*   LE PROGRAMME SE TERMINE NORMALEMENT   *'                
           DISPLAY '*******************************************'                
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
      ***********************************************************               
       LIRE-FFP120  SECTION.                                                    
      ***********************************************************               
           READ FFP120                                                          
              AT END                                                            
                 SET TERMINE-FFP120  TO TRUE                                    
              NOT AT END                                                        
                 ADD 1 TO CPT-FFP120                                            
                 MOVE  FFP050-NSOCCOMPT   TO   RPT-NSOCCOMPT-LU                 
           END-READ.                                                            
      *                                                                         
      ***********************************************************               
       TRAITEMENT-FFP120 SECTION.                                               
      ***********************************************************               
      *                                                                         
            IF NOT  TERMINE-FFP120                                              
               MOVE FFP050-NFACTURE      TO   W-NFACTURE-SUP                    
               MOVE W-NSOCDEST           TO   JFP050-NSOCDEST                   
               MOVE W-NFACTURE           TO   JFP050-NFACTURE                   
               MOVE FFP050-TIERS         TO   JFP050-TIERS                      
               MOVE FFP050-NLIEUTIERS    TO   JFP050-NLIEUTIERS                 
               MOVE FFP050-CTYPINTER     TO   JFP050-CTYPINTER                  
               MOVE FFP050-DFACTURE-JJ   TO   JFP050-DFACTURE-JJ                
               MOVE FFP050-DFACTURE-MM   TO   JFP050-DFACTURE-MM                
               MOVE FFP050-DFACTURE-SSAA TO   JFP050-DFACTURE-SSAA              
               MOVE FFP050-DMAJ-JJ       TO   JFP050-DMAJ-JJ                    
               MOVE FFP050-DMAJ-MM       TO   JFP050-DMAJ-MM                    
               MOVE FFP050-DMAJ-SSAA     TO   JFP050-DMAJ-SSAA                  
               MOVE FFP050-NCRI          TO   JFP050-NCRI                       
               MOVE FFP050-NSINISTRE     TO   W-NSINISTRE                       
               MOVE W-NSINI1             TO   JFP050-NSINI1                     
               MOVE W-NSINI2             TO   JFP050-NSINI2                     
               MOVE W-NSINI3             TO   JFP050-NSINI3                     
               MOVE FFP050-MTTTC         TO   JFP050-A-REGLER                   
               ADD  FFP050-MTTTC         TO   TOT-A-REGLER-NSOCCOMPT            
      *                                                                         
               IF   FFP050-CTYPINTER   = 'SAV'                                  
                   COMPUTE  TOT-SAV  = TOT-SAV  + FFP050-MTTTC                  
               ELSE                                                             
                   COMPUTE  TOT-RA9  = TOT-RA9  + FFP050-MTTTC                  
               END-IF                                                           
      *                                                                         
             PERFORM ECRIRE-JFP130                                              
           END-IF.                                                              
      ***********************************************************               
       ECRIRE-JFP130    SECTION.                                                
      ***********************************************************               
      *                                                                         
      *** ECRITURE D'UNE LIGNE D'ETAT                                           
      *                                                                         
           IF  W-LIGNE  >  W-MAX-DETAIL                                         
               IF W-LIGNE NOT = 99                                              
                IF RPT-NSOCCOMPT-LU  =  RPT-NSOCCOMPT-STOCK                     
      *---- SI NON RUPTURE CLE ET                                               
      *- NON PREMIERE FOIS, FERMETURE  TABLEAU                                  
                  WRITE JFP130-FICHIER FROM JFP050-TRAIT                        
                END-IF                                                          
               END-IF                                                           
               PERFORM  ECRITURE-ENTETE                                         
           END-IF.                                                              
     *                                                                          
           WRITE JFP130-FICHIER FROM JFP050-LIGNE.                              
           ADD 1  TO W-LIGNE.                                                   
      *                                                                         
      ***********************************************************               
       TOTAL-NSOC    SECTION.                                                   
      ***********************************************************               
      *                                                                         
           WRITE JFP130-FICHIER          FROM JFP050-TRAIT.                     
           MOVE  'TOTAL SOCIETE '        TO   JFP050-LIBELLE                    
           MOVE  RPT-NSOCCOMPT-STOCK     TO   JFP050-NSOCTOT                    
           MOVE  TOT-A-REGLER-NSOCCOMPT  TO   JFP050-TOT-A-REGLER               
           MOVE  0                       TO   TOT-A-REGLER-NSOCCOMPT            
           WRITE JFP130-FICHIER          FROM JFP050-TOTAL.                     
           WRITE JFP130-FICHIER          FROM JFP050-TRAIT.                     
      *                                                                         
      ***********************************************************               
       TOTAL-SAV     SECTION.                                                   
      ***********************************************************               
      *                                                                         
           MOVE  'TOTAL SAV     '     TO   JFP050-LIBELLE-SR                    
           MOVE  TOT-SAV              TO   JFP050-TOT-SR                        
           MOVE  0                    TO   TOT-SAV                              
           WRITE JFP130-FICHIER       FROM JFP050-TOTAL-SR.                     
      *                                                                         
      ***********************************************************               
       TOTAL-RA9     SECTION.                                                   
      ***********************************************************               
      *                                                                         
           MOVE  'TOTAL RA9     '     TO   JFP050-LIBELLE-SR                    
           MOVE  TOT-RA9              TO   JFP050-TOT-SR                        
           MOVE  0                    TO   TOT-RA9                              
           WRITE JFP130-FICHIER       FROM JFP050-TOTAL-SR.                     
           WRITE JFP130-FICHIER       FROM JFP050-TRAIT.                        
      *                                                                         
      ***********************************************************               
       ECRITURE-ENTETE SECTION.                                                 
      ***********************************************************               
      *                                                                         
           MOVE   FFP050-NSOCCOMPT      TO  JFP050-NSOCCOMPT                    
           MOVE   FFP050-LIBNSOC        TO  JFP050-LIBNSOC                      
           MOVE   FFP050-NLOT           TO  JFP050-NLOT                         
           MOVE   FFP050-FDATE-JJ       TO  JFP050-FDATE-JJ                     
           MOVE   FFP050-FDATE-MM       TO  JFP050-FDATE-MM                     
           MOVE   FFP050-FDATE-SSAA     TO  JFP050-FDATE-SSAA                   
           ADD    1 TO W-PAGE                                                   
           MOVE   W-PAGE                TO  JFP050-PAGE.                        
      *                                                                         
           WRITE JFP130-FICHIER  FROM  JFP050-ENTETE1.                          
           WRITE JFP130-FICHIER  FROM  JFP050-ENTETE2.                          
      *-----SAUT 1 LIGNE                                                        
           WRITE JFP130-FICHIER  FROM  JFP050-ENTETE4.                          
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
      *-----SAUT 1 LIGNE                                                        
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT                             
      *-----LIGNE COLONNE                                                       
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-TITRE1.                     
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-TITRE2.                     
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-TITRE3.                     
      *-----LIGNE CADRE                                                         
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT                             
      *                                                                         
           MOVE  12  TO   W-LIGNE.                                              
      *                                                                         
       EDITION-FICHIER-VIDE           SECTION.                                  
      ****************************************                                  
      *                                                                         
           WRITE JFP130-FICHIER  FROM  JFP050-ENTETE1.                          
           WRITE JFP130-FICHIER  FROM  JFP050-ENTETE4.                          
           MOVE  ALL '*'         TO    JFP050-LIB-VIDE                          
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP050-LIB-VIDE                          
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
           MOVE '**********  RIEN   A   EDITER  **********************'         
                 TO  JFP050-LIB-VIDE                                            
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
           MOVE '**********  AUCUNE DONNEE EXTRAITE  *****************'         
                 TO  JFP050-LIB-VIDE                                            
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP050-LIB-VIDE                          
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP050-LIB-VIDE                          
           WRITE JFP130-FICHIER  FROM  JFP050-TRAIT-VIDE.                       
      ***********************************************************               
       ABANDON-PROGRAMME SECTION.                                               
      ***********************************************************               
           MOVE 'BFP130' TO ABEND-PROG.                                         
           DISPLAY ' '                                                          
           DISPLAY 'FIN ANORMALE PROVOQUEE PAR PROGRAMME :'                     
           DISPLAY ABEND-MESS                                                   
           DISPLAY ' '                                                          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
