      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BRB002.                                           00000040
       AUTHOR.        DSA045-EY.                                        00000050
       DATE-WRITTEN.  15/05/2006.                                       00000060
      *----------------------------------------------------------------*00000080
      * ==> FOURNISSEUR NAGRA                                          *00000090
      * R�CEPTIONS DES MOUVEMENTS QUOTIDIENS SUR LA RECEPTION ENTREPOT.*00000090
      * METTRE LE FICHIER .CSV AVEC LES SEPARATEURS ';' DANS UN        *00000090
      * FICHIER PLAT                                                   *00000090
      *----------------------------------------------------------------*00000080
      * DATE   : AOUT 2006                                             *        
      * MODIFICATION :                                                 *        
      * AUTEUR : JEREMY CELAIRE  JC0806                                *        
      * COMMENTAIRE :                                                  *        
      *  - FICHIER FRB001 VARIABLE.                                    *        
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000160
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                   00000170
      *    DECIMAL-POINT IS COMMA.                                      00000180
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNA000     ASSIGN TO FNA000                           00000250
      *                      FILE STATUS ST-FNA000.                             
      *--                                                                       
           SELECT FNA000     ASSIGN TO FNA000                                   
                             FILE STATUS ST-FNA000                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRB002     ASSIGN TO FRB002                           00000250
      *                      FILE STATUS ST-FRB002.                             
      *--                                                                       
           SELECT FRB002     ASSIGN TO FRB002                                   
                             FILE STATUS ST-FRB002                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *    FICHIER EN ENTREE .CSV                                       00000430
       FD  FNA000                                                       00000440
JC0806*    RECORDING F                                                  00000450
JC0806     RECORDING V                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD                                        00000470
JC0806     RECORD CONTAINS 54 TO 400 CHARACTERS                         00000470
JC0806     DATA RECORD IS FNA000-ENR.                                   00000470
JC0806*01  FNA000-ENR.                                                  00000480
JC0806*    05  FNA000-ENREGISTREMENT PIC X(400).                        00000480
       01  FNA000-ENR.                                                  00000480
JC0806     05  FNA000-ENREG           PICTURE X(01)                     00000480
JC0806                                OCCURS 54 TO 400 TIMES            00000480
JC0806                                DEPENDING ON I-LONG.                      
      *                                                                 00000600
       FD  FRB002                                                       00000440
           RECORDING F                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FRB002-ENR.                                                  00000480
           05  FRB002-FILLEUR         PIC X(200).                       00000480
      *                                                                 00000600
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       77  ST-FNA000                  PIC 9(02) VALUE ZERO.                     
       77  ST-FRB002                  PIC 9(02) VALUE ZERO.                     
       01  LG-FNA000                  PIC 9(05) VALUE ZERO.                     
           COPY  FRB002.                                                        
      *    COPIES D'ABEND ET DE CONTR�LE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      *    DESCRIPTION DES TABLES UTILIS�ES.                            00001910
      *    EXEC  SQL  INCLUDE  SQLCA     END-EXEC.                      00001940
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    R�CUP�RATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  WS-NBR-LUS                 PIC 9(06)  COMP-3   VALUE 0.              
       01  CPT-FRB002                 PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01  WS-DCOMMANDE               PIC 9(08).                        00000480
       01  WS-DPRODUCTION             PIC 9(08).                        00000480
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FNA000             PIC 9(01)           VALUE 0.              
           88 FIN-FNA000                                  VALUE 1.              
      *                                                                         
JC0806 01  I-LONG                     PIC S9(3)  COMP-3 VALUE 0.                
JC0806 01  I                          PIC S9(3)  COMP-3 VALUE 0.                
JC0806 01  FNA000-ENREGISTREMENT      PIC X(400).                               
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BRB002       SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
      *    LECTURE DU FICHIER EN ENTR�E.                                00001900
      *    C'EST LE FICHIER .CSV CE N'EST PAS UN POSITIONNEL            00001900
      *    LE BUT EST DE LE METTRE DANS UN POSITIONEL                   00001900
      *    LECTURE DU FICHIER.                                          00001900
      *    PREMIERE LECTURE C'EST POUR OMMETRE L'ENTETE                 00001900
      *             PAS D'OMMISION POUR NAGRA                           00001900
      *    PERFORM  READ-FNA000 .                                               
      *    C'EST LA 2� EME LECTURE QUI EST LA BONNE                             
           PERFORM  READ-FNA000                                                 
           PERFORM UNTIL FIN-FNA000                                             
              PERFORM FORMATAGE-DONNEES                                         
              PERFORM ECRITURE-FRB002                                           
              PERFORM READ-FNA000                                               
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BRB002.                                                 
      *                                                                         
       FIN-MODULE-BRB002.  EXIT.                                                
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * REFORMATAGE DES DONN�ES.                                                
      *-----------------------------------------------------------------        
       FORMATAGE-DONNEES  SECTION.                                      00002160
      *                                                                 00001900
           INITIALIZE FRB002-ENR                                                
                      FRB002-RECORD                                             
                      LG-FNA000.                                                
      *                                                                 00001900
JC0806     PERFORM VARYING I FROM 1 BY 1 UNTIL I > 400                          
JC0806         MOVE FNA000-ENREG(I)                                             
JC0806                        TO  FNA000-ENREGISTREMENT(I:1)                    
JC0806     END-PERFORM                                                          
      *                                                                         
      *    AJOUTER LE DERNIER ';' � LA FIN DE CHAINE SI MANQUE                  
           PERFORM VARYING LG-FNA000 FROM 1 BY 1 UNTIL                          
              FNA000-ENREGISTREMENT (LG-FNA000:) NOT > SPACES                   
              OR LG-FNA000      >= LENGTH OF FNA000-ENREGISTREMENT              
           END-PERFORM                                                          
           IF FNA000-ENREGISTREMENT (LG-FNA000 - 1:1) NOT = ';'                 
              MOVE ';' TO  FNA000-ENREGISTREMENT (LG-FNA000:1)                  
           END-IF .                                                             
           UNSTRING FNA000-ENREGISTREMENT    DELIMITED BY     ';'               
                    INTO                                                        
                    FRB002-NFOURNISSEUR                                      000
                    FRB002-NBL                                               000
                    FRB002-NCOMMANDE                                         000
                    WS-DCOMMANDE                                             000
                    FRB002-NLOTPROD                                          000
                    FRB002-NBARQUETTE                                        000
                    FRB002-CPRODUIT                                          000
                    FRB002-NSERIE .                                          000
      *    MODIFICATION DE FORMAT DES DATES. ELLES SONT R�PUT�ES                
      *    CORRECTES <=> NON CONTR�L�ES.                                        
      *    DATE D'OP�RATION.                                                    
           STRING WS-DCOMMANDE(5:4) WS-DCOMMANDE(3:2)                           
                  WS-DCOMMANDE(1:2)                                             
           DELIMITED BY SIZE INTO  FRB002-DCOMMANDE .                           
      *    DISPLAY 'ENREG      ' FNA000-ENREGISTREMENT.                         
      *    DISPLAY 'NSERIE EST ' FRB002-NSERIE .                                
                                                                             000
       FIN-FORMATAGE-DONNEES. EXIT.                                     00002620
                                                                        00002620
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! RECEPTION DES DONNEES REDBOX  .              !'  00002400
           DISPLAY  '! FORMATAGE DU FICHIER .CSV     .              !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!       PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 15/05/2006 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTR�E.                                      
           OPEN  INPUT  FNA000 .                                        00002950
      *    OUVERTURE DU FICHIER EN SORTIE.                                      
           OPEN  OUTPUT FRB002 .                                        00002950
      *    R�CUP�RATION DE LA DATE SYST�ME.                                     
           PERFORM LECTURE-DSYST .                                              
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * R�CUP�RATION DE LA DATE SYST�ME.                                        
      *-----------------------------------------------------------------        
       LECTURE-DSYST   SECTION.                                                 
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
       FIN-LECTURE-DSYST. EXIT.                                         00008940
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BRB002 SECTION.                                              00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS        00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS ECRIT   : ' CPT-FRB002        00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FNA000 FRB002                                                 
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRB002. EXIT.                                            00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FNA000 FRB002.                                                
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BRB002'  TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FNA000  SECTION.                                                    
           READ FNA000  AT END                                                  
                SET FIN-FNA000     TO TRUE                                      
                        NOT AT END                                              
                ADD  1             TO WS-NBR-LUS                                
           END-READ.                                                            
       FIN-READ-FNA000. EXIT.                                           00009300
      *                                                                         
       ECRITURE-FRB002              SECTION.                                    
           WRITE  FRB002-ENR FROM FRB002-RECORD.                                
           ADD 1 TO CPT-FRB002 .                                                
       FIN-ECRITURE-FRB002   . EXIT.                                            
      *                                                                         
       COPY SYBCERRO.                                                           
