      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BFP110.                                                 
       AUTHOR.          M.M.                                                    
      ******************************************************************        
      * BFP110 * LISTE DE CONTROLE DES FACTURES REJETEES PAR PACIFICA  *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFP110  ASSIGN TO FFP110.                                    
      *--                                                                       
            SELECT FFP110  ASSIGN TO FFP110                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT JFP110  ASSIGN TO JFP110.                                    
      *--                                                                       
            SELECT JFP110  ASSIGN TO JFP110                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FFP110                                                               
      *---LONGUEUR = 250                                                        
            RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.                  
            COPY FFP050.                                                        
      *                                                                         
       FD  JFP110 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  JFP110-FICHIER PIC X(133).                                           
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      **  ZONES DE TRAVAIL                                                      
      ******************************************************************        
       01  W-PAGE                   PIC 999 VALUE  0.                           
       01  W-LIGNE                  PIC 99  VALUE  99.                          
       01  W-MAX-DETAIL             PIC 99  VALUE  60.                          
       01  W-NFACTURE-SUP.                                                      
           03  W-NSOCDEST           PIC X(03).                                  
           03  W-NFACTURE           PIC X(10).                                  
           03  FILLER               PIC X(02).                                  
      *                                                                         
       01  W-NSINISTRE.                                                         
           03  W-NSINI1            PIC  X(10).                                  
           03  W-NSINI2            PIC  X(03).                                  
           03  W-NSINI3            PIC  X(03).                                  
      *                                                                         
      * DATE ET HEURE                                                           
       01 W-TIME.                                                               
           03 W-HEURE               PIC 9(2).                                   
           03 W-MINUTE              PIC 9(2).                                   
           03 W-SECONDE             PIC 9(2).                                   
           03 FILLER                PIC 9(2).                                   
       01 W-DATE                    PIC 9(6).                                   
      *                                                                         
       01  TOT-SAV                  PIC 9(10)V99 VALUE 0.                       
       01  TOT-RA9                  PIC 9(10)V99 VALUE 0.                       
      *                                                                         
      * COMPTEURS                                                               
       01  CPT-FFP110               PIC 9999    VALUE 0.                        
       01  CPT-EDIT                 PIC Z(3)9.                                  
       01  FFP050-A-REGLER          PIC 9(09)V99 VALUE 0.                       
       01  TOT-A-REGLER-NSOCCOMPT   PIC 9(10)V99 VALUE 0.                       
      *                                                                         
       01  RPT-NSOCCOMPT-LU         PIC X(03)   VALUE SPACES.                   
       01  RPT-NSOCCOMPT-STOCK      PIC X(03)   VALUE SPACES.                   
      *                                                                         
      * FLAG POUR TRAITEMENT                                                    
      *                                                                         
       01  LECTURE-FFP110           PIC 9       VALUE 0.                        
           88  TERMINE-FFP110                   VALUE 1.                        
      *                                                                         
      ******************************************************************        
      * DESCRIPTION DE L'ETAT  -> JFP110                                        
      ******************************************************************        
      *** ENTETE LIGNE N�1                                                      
       01  JFP110-ENTETE1.                                                      
          03  FILLER              PIC X(001) VALUE '1'.                         
          03  FILLER              PIC X(006) VALUE 'JFP110'.                    
          03  FILLER              PIC X(011) VALUE SPACES.                      
          03  FILLER              PIC X(045) VALUE                              
              'FACTURES REJETEES PAR PACIFICA               '.                  
          03  FILLER              PIC X(028) VALUE SPACES.                      
          03  FILLER              PIC X(009) VALUE                              
              'EDITE LE'.                                                       
          03  JFP110-DATE-JJ      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP110-DATE-MM      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP110-DATE-SS      PIC X(002) VALUE SPACES.                      
          03  JFP110-DATE-AA      PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE ' A '.                       
          03  JFP110-HEURE        PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE 'H'.                         
          03  JFP110-MINUTE       PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(006) VALUE ' PAGE '.                    
          03  JFP110-PAGE         PIC ZZZ9.                                     
       01  JFP110-ENTETE2.                                                      
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  FILLER              PIC X(024) VALUE                              
              '      SOCIETE         : '.                                       
          03  JFP110-NSOCCOMPT    PIC X(005) VALUE SPACES.                      
          03  JFP110-LIBNSOC      PIC X(025) VALUE SPACES.                      
          03  FILLER              PIC X(073) VALUE SPACES.                      
       01  JFP110-ENTETE4.                                                      
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  FILLER              PIC X(024) VALUE                              
              '      LOT ENVOYE NO   : '.                                       
          03  JFP110-NLOT         PIC X(006) VALUE SPACES.                      
          03  FILLER              PIC X(017) VALUE                              
              '  A LA DATE DU : '.                                              
          03  JFP110-FDATE-JJ     PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP110-FDATE-MM     PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '/'.                         
          03  JFP110-FDATE-SSAA   PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(075) VALUE SPACES.                      
       01  JFP110-TRAIT-VIDE.                                                   
          03  FILLER              PIC X(001) VALUE '0'.                         
          03  JFP110-LIB-VIDE     PIC X(132) VALUE SPACE.                       
       01  JFP110-TRAIT.                                                        
          03  JFP110-TRAIT-ASA    PIC X(001) VALUE ' '.                         
          03  FILLER              PIC X(001) VALUE '+'.                         
          03  FILLER              PIC X(130) VALUE ALL '-'.                     
          03  FILLER              PIC X(001) VALUE '+'.                         
       01  JFP110-TRAIT-TITRE1.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '              '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                ' LIEU DE '.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                ' TYPE'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                ' CODE'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '    DATE   '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   DATE DE  '.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(022) VALUE                              
                                '                   '.                          
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '                 '.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   MONTANT '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                'N� ORDRE'.                                     
          03  FILLER              PIC X(001) VALUE '!'.                         
       01  JFP110-TRAIT-TITRE2.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '   N� FACTURE '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                ' GESTION '.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                '  DE '.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                '  DE '.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                'D''INTERVENT.'.                                
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                ' CREATION OU'.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(022) VALUE                              
                                '      NOM DU CLIENT'.                          
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '   N� DE SINISTRE'.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   NET TTC '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                '   DE '.                                       
          03  FILLER              PIC X(001) VALUE '!'.                         
       01  JFP110-TRAIT-TITRE3.                                                 
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(016) VALUE                              
                                '              '.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(009) VALUE                              
                                'TIERS GCT'.                                    
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                ' FACT'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(005) VALUE                              
                                'REJET'.                                        
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '  OU DE BC '.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                ' MISE A JOUR'.                                 
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(022) VALUE                              
                                '                   '.                          
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(020) VALUE                              
                                '                 '.                            
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(012) VALUE                              
                                '   A REGLER'.                                  
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(008) VALUE                              
                                'PRESENT'.                                      
          03  FILLER              PIC X(001) VALUE '!'.                         
      *** LIGNE                                                                 
       01  JFP110-LIGNE.                                                        
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NSOCDEST     PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NFACTURE     PIC X(010).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-TIERS        PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NLIEUTIERS   PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-CTYPINTER    PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-CREJET       PIC X(003).                                   
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-DFACTURE-JJ   PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP110-DFACTURE-MM   PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP110-DFACTURE-SSAA PIC X(004).                                  
          03  FILLER               PIC X(001) VALUE SPACES.                     
          03  FILLER               PIC X(001) VALUE '!'.                        
          03  FILLER               PIC X(001) VALUE SPACES.                     
          03  JFP110-DMAJ-JJ       PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP110-DMAJ-MM       PIC X(002).                                  
          03  FILLER               PIC X(001) VALUE '/'.                        
          03  JFP110-DMAJ-SSAA     PIC X(004).                                  
          03  FILLER               PIC X(001) VALUE SPACES.                     
          03  FILLER               PIC X(001) VALUE '!'.                        
          03  JFP110-CLIENT        PIC X(22).                                   
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NSINI1       PIC X(10).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NSINI2       PIC X(03).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  JFP110-NSINI3       PIC X(03).                                    
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  JFP110-A-REGLER     PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '!'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  JFP110-NPRESENT     PIC 99.                                       
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '!'.                         
      **                                                                        
       01  JFP110-TOTAL.                                                        
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(010) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACE.                       
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(22)  VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  JFP110-LIBELLE      PIC X(15).                                    
          03  JFP110-NSOCTOT      PIC X(05).                                    
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  JFP110-TOT-A-REGLER PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC XX     VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
      **                                                                        
       01  JFP110-TOTAL-SR.                                                     
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(010) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACE.                       
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(002) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(004) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(25)  VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  JFP110-LIBELLE-SR   PIC X(15).                                    
          03  FILLER              PIC X(05)  VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  JFP110-TOT-SR       PIC Z(008)9,99.                               
          03  FILLER              PIC X(001) VALUE '*'.                         
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC XX     VALUE SPACES.                      
          03  FILLER              PIC X(003) VALUE SPACES.                      
          03  FILLER              PIC X(001) VALUE '*'.                         
      **                                                                        
      **                                                                        
      ******************************************************************        
      **  MODULES :  ABEND                                                      
      ******************************************************************        
           COPY  ABENDCOP.                                                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND     PIC X(8)  VALUE 'ABEND'.                                   
      *--                                                                       
       01  MW-ABEND     PIC X(8)  VALUE 'ABEND'.                                
      *}                                                                        
           COPY  WORKDATC.                                                      
       01  BETDATC   PIC X(8)  VALUE 'BETDATC'.                                 
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           DISPLAY '*******************************************'                
           DISPLAY '*                 BFP110                  *'.               
           DISPLAY '*******************************************'                
      * OUVERTURE DES FICHIERS                                                  
           OPEN INPUT  FFP110.                                                  
           OPEN OUTPUT JFP110.                                                  
      *  DATE  ET HEURE                                                         
            ACCEPT  W-TIME   FROM TIME                                          
            ACCEPT  W-DATE   FROM DATE.                                         
            MOVE    W-DATE (5:2)  TO  JFP110-DATE-JJ                            
            MOVE    W-DATE (3:2)  TO  JFP110-DATE-MM                            
            MOVE    W-DATE (1:2)  TO  JFP110-DATE-AA                            
            MOVE    '20'   TO  JFP110-DATE-SS                                   
            MOVE    W-HEURE       TO  JFP110-HEURE                              
            MOVE    W-MINUTE      TO  JFP110-MINUTE.                            
      *                                                                         
      ******************************************************************        
      * TRAITEMENT                                                              
      ******************************************************************        
      * PRE-LECTURE                                                             
      *                                                                         
           PERFORM LIRE-FFP110.                                                 
           MOVE  FFP050-NSOCCOMPT    TO   RPT-NSOCCOMPT-STOCK                   
      *                                                                         
      *---------------------------------------------------------*               
      *----------- TRAITEMENT FICHIER  VIDE                                     
      *---------------------------------------------------------*               
      *                                                                         
           IF TERMINE-FFP110                                                    
              PERFORM  EDITION-FICHIER-VIDE                                     
              DISPLAY 'FICHIER FFP110 VIDE '                                    
              DISPLAY '*******************************************'             
              DISPLAY '*   LE PROGRAMME SE TERMINE NORMALEMENT   *'             
              DISPLAY '*******************************************'             
              CLOSE   FFP110 JFP110                                             
      *{ Ba-Stop-Run-Statement 1.1                                              
      *       STOP RUN                                                          
      *--                                                                       
              EXIT PROGRAM                                                      
      *}                                                                        
           END-IF.                                                              
      *                                                                         
      *----------------------------------------------------------*              
      * BOUCLE DE LECTURE                                                       
      *                                                                         
           PERFORM UNTIL TERMINE-FFP110                                         
             IF RPT-NSOCCOMPT-STOCK  NOT  =  RPT-NSOCCOMPT-LU                   
                PERFORM  TOTAL-NSOC                                             
                PERFORM  TOTAL-SAV                                              
                PERFORM  TOTAL-RA9                                              
                PERFORM  ECRITURE-ENTETE                                        
                MOVE  RPT-NSOCCOMPT-LU   TO  RPT-NSOCCOMPT-STOCK                
            END-IF                                                              
      *                                                                         
            PERFORM TRAITEMENT-FFP110                                           
            PERFORM LIRE-FFP110                                                 
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
      *------ EDITION TOTAUX                                                    
      *                                                                         
             PERFORM  TOTAL-NSOC                                                
             PERFORM  TOTAL-SAV                                                 
             PERFORM  TOTAL-RA9                                                 
      *                                                                         
      *------ FERMETURE FICHIERS                                                
      *                                                                         
           CLOSE FFP110 JFP110.                                                 
      *                                                                         
      *------ EDITION COMPTEURS                                                 
      *                                                                         
           MOVE CPT-FFP110  TO CPT-EDIT.                                        
           DISPLAY 'LECTURE ' CPT-EDIT                                          
                   ' ENREGISTREMENTS DE FFP110 '                                
           DISPLAY '*******************************************'                
           DISPLAY '*   LE PROGRAMME SE TERMINE NORMALEMENT   *'                
           DISPLAY '*******************************************'                
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
      ***********************************************************               
       LIRE-FFP110  SECTION.                                                    
      ***********************************************************               
           READ FFP110                                                          
              AT END                                                            
                 SET TERMINE-FFP110  TO TRUE                                    
              NOT AT END                                                        
                 ADD 1 TO CPT-FFP110                                            
                 MOVE  FFP050-NSOCCOMPT  TO  RPT-NSOCCOMPT-LU                   
           END-READ.                                                            
      *                                                                         
      ***********************************************************               
       TRAITEMENT-FFP110 SECTION.                                               
      ***********************************************************               
            IF NOT  TERMINE-FFP110                                              
               MOVE FFP050-NFACTURE      TO   W-NFACTURE-SUP                    
               MOVE W-NSOCDEST           TO   JFP110-NSOCDEST                   
               MOVE W-NFACTURE           TO   JFP110-NFACTURE                   
               MOVE FFP050-TIERS         TO   JFP110-TIERS                      
               MOVE FFP050-NLIEUTIERS    TO   JFP110-NLIEUTIERS                 
               MOVE FFP050-CTYPINTER     TO   JFP110-CTYPINTER                  
               MOVE FFP050-DFACTURE-JJ   TO   JFP110-DFACTURE-JJ                
               MOVE FFP050-DFACTURE-MM   TO   JFP110-DFACTURE-MM                
               MOVE FFP050-DFACTURE-SSAA TO   JFP110-DFACTURE-SSAA              
               MOVE FFP050-DMAJ-JJ       TO   JFP110-DMAJ-JJ                    
               MOVE FFP050-DMAJ-MM       TO   JFP110-DMAJ-MM                    
               MOVE FFP050-DMAJ-SSAA     TO   JFP110-DMAJ-SSAA                  
               MOVE  FFP050-NOM          TO   JFP110-CLIENT                     
               MOVE FFP050-NSINISTRE     TO   W-NSINISTRE                       
               MOVE W-NSINI1             TO   JFP110-NSINI1                     
               MOVE W-NSINI2             TO   JFP110-NSINI2                     
               MOVE W-NSINI3             TO   JFP110-NSINI3                     
               IF  FFP050-CTYPINTER  =  'SAV'                                   
                   COMPUTE  FFP050-A-REGLER  =                                  
                        FFP050-MTTTC -  FFP050-MTFRANCHISE                      
                   COMPUTE  TOT-SAV  = TOT-SAV  + FFP050-A-REGLER               
               ELSE                                                             
                   COMPUTE  FFP050-A-REGLER  =                                  
               ((FFP050-MTTTC  +  FFP050-MTLIVR) -  FFP050-MTFRANCHISE)         
                   COMPUTE  TOT-RA9  = TOT-RA9  + FFP050-A-REGLER               
               END-IF                                                           
               MOVE  FFP050-A-REGLER   TO   JFP110-A-REGLER                     
               ADD   FFP050-A-REGLER   TO   TOT-A-REGLER-NSOCCOMPT              
               MOVE  FFP050-NPRESENT   TO   JFP110-NPRESENT                     
               MOVE  FFP050-CREJET     TO   JFP110-CREJET                       
      *                                                                         
             PERFORM ECRIRE-JFP110                                              
           END-IF.                                                              
      ***********************************************************               
       ECRIRE-JFP110    SECTION.                                                
      ***********************************************************               
      *                                                                         
      *** ECRITURE D'UNE LIGNE D'ETAT                                           
      *                                                                         
           IF  W-LIGNE  >  W-MAX-DETAIL                                         
               IF W-LIGNE NOT = 99                                              
                IF RPT-NSOCCOMPT-LU  =  RPT-NSOCCOMPT-STOCK                     
      *---- SI NON RUPTURE CLE ET                                               
      *- NON PREMIERE FOIS, FERMETURE  TABLEAU                                  
                  WRITE JFP110-FICHIER FROM JFP110-TRAIT                        
                END-IF                                                          
               END-IF                                                           
               PERFORM  ECRITURE-ENTETE                                         
           END-IF.                                                              
     *                                                                          
           WRITE JFP110-FICHIER FROM JFP110-LIGNE.                              
           ADD 1  TO W-LIGNE.                                                   
      *                                                                         
      ***********************************************************               
       TOTAL-NSOC    SECTION.                                                   
      ***********************************************************               
      *                                                                         
           WRITE JFP110-FICHIER       FROM JFP110-TRAIT.                        
           MOVE  'TOTAL SOCIETE '     TO   JFP110-LIBELLE                       
           MOVE  RPT-NSOCCOMPT-STOCK  TO   JFP110-NSOCTOT                       
           MOVE  TOT-A-REGLER-NSOCCOMPT    TO   JFP110-TOT-A-REGLER             
           MOVE  0                    TO   TOT-A-REGLER-NSOCCOMPT               
           WRITE JFP110-FICHIER       FROM JFP110-TOTAL.                        
           WRITE JFP110-FICHIER       FROM JFP110-TRAIT.                        
      *                                                                         
      ***********************************************************               
       TOTAL-SAV     SECTION.                                                   
      ***********************************************************               
      *                                                                         
           MOVE  'TOTAL SAV     '     TO   JFP110-LIBELLE-SR                    
           MOVE  TOT-SAV              TO   JFP110-TOT-SR                        
           MOVE  0                    TO   TOT-SAV                              
           WRITE JFP110-FICHIER       FROM JFP110-TOTAL-SR.                     
      *                                                                         
      ***********************************************************               
       TOTAL-RA9     SECTION.                                                   
      ***********************************************************               
      *                                                                         
           MOVE  'TOTAL RA9     '     TO   JFP110-LIBELLE-SR                    
           MOVE  TOT-RA9              TO   JFP110-TOT-SR                        
           MOVE  0                    TO   TOT-RA9                              
           WRITE JFP110-FICHIER       FROM JFP110-TOTAL-SR.                     
           WRITE JFP110-FICHIER       FROM JFP110-TRAIT.                        
      *                                                                         
      ***********************************************************               
       ECRITURE-ENTETE SECTION.                                                 
      ***********************************************************               
      *                                                                         
           MOVE   FFP050-NSOCCOMPT      TO  JFP110-NSOCCOMPT                    
           MOVE   FFP050-LIBNSOC        TO  JFP110-LIBNSOC                      
           MOVE   FFP050-NLOT           TO  JFP110-NLOT                         
           MOVE   FFP050-FDATE-JJ       TO  JFP110-FDATE-JJ                     
           MOVE   FFP050-FDATE-MM       TO  JFP110-FDATE-MM                     
           MOVE   FFP050-FDATE-SSAA     TO  JFP110-FDATE-SSAA                   
           ADD    1 TO W-PAGE                                                   
           MOVE   W-PAGE                TO  JFP110-PAGE.                        
      *                                                                         
           WRITE JFP110-FICHIER  FROM  JFP110-ENTETE1.                          
           WRITE JFP110-FICHIER  FROM  JFP110-ENTETE2.                          
      *-----SAUT 1 LIGNE                                                        
           WRITE JFP110-FICHIER  FROM  JFP110-ENTETE4.                          
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
      *-----SAUT 1 LIGNE                                                        
      *-----LIGNE CADRE                                                         
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT                             
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-TITRE1.                     
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-TITRE2.                     
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-TITRE3.                     
      *-----LIGNE CADRE                                                         
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT                             
      *                                                                         
           MOVE  12  TO   W-LIGNE.                                              
      *                                                                         
       EDITION-FICHIER-VIDE           SECTION.                                  
      ****************************************                                  
      *                                                                         
           WRITE JFP110-FICHIER  FROM  JFP110-ENTETE1.                          
           MOVE  ALL '*'         TO    JFP110-LIB-VIDE                          
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP110-LIB-VIDE                          
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
           MOVE '**********  RIEN   A   EDITER  **********************'         
                 TO  JFP110-LIB-VIDE                                            
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
           MOVE '**********  AUCUNE DONNEE EXTRAITE  *****************'         
                 TO  JFP110-LIB-VIDE                                            
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP110-LIB-VIDE                          
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE                        
           MOVE  ALL '*'         TO    JFP110-LIB-VIDE                          
           WRITE JFP110-FICHIER  FROM  JFP110-TRAIT-VIDE.                       
      ***********************************************************               
       ABANDON-PROGRAMME SECTION.                                               
      ***********************************************************               
           MOVE 'BFP110' TO ABEND-PROG.                                         
           DISPLAY ' '                                                          
           DISPLAY 'FIN ANORMALE PROVOQUEE PAR PROGRAMME :'                     
           DISPLAY ABEND-MESS                                                   
           DISPLAY ' '                                                          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
