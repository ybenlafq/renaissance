      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     MHLPA.                                   
       AUTHOR. PROJET DSA0.                                                     
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *==============================================================           
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  WZERO                     PIC S9(4)    COMP  VALUE ZERO.         
      *--                                                                       
           05  WZERO                     PIC S9(4) COMP-5  VALUE ZERO.          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD1                       PIC S9(4)    COMP  VALUE ZERO.         
      *--                                                                       
           05  AD1                       PIC S9(4) COMP-5  VALUE ZERO.          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AD2                       PIC S9(4)    COMP  VALUE ZERO.         
      *--                                                                       
           05  AD2                       PIC S9(4) COMP-5  VALUE ZERO.          
      *}                                                                        
           05  ZHWA.                                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  ZHW                   PIC S9(4)    COMP  VALUE ZERO.         
      *--                                                                       
               10  ZHW                   PIC S9(4) COMP-5  VALUE ZERO.          
      *}                                                                        
           05  TAB-ADDR.                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'40C1C2C3C4C5C6C7C8C94A4B4C4D4E4F'.                         
      *--                                                                       
                   X'20414243444546474849B02E3C282B21'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'50D1D2D3D4D5D6D7D8D95A5B5C5D5E5F'.                         
      *--                                                                       
                   X'264A4B4C4D4E4F505152A7242A293B5E'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'6061E2E3E4E5E6E7E8E96A6B6C6D6E6F'.                         
      *--                                                                       
                   X'2D2F535455565758595AF92C255F3E3F'.                         
      *}                                                                        
               10  FILLER                PIC X(16)    VALUE                     
      *{ Tr-Hexa-Map 1.5                                                        
      *            X'F0F1F2F3F4F5F6F7F8F97A7B7C7D7E7F'.                         
      *--                                                                       
                   X'303132333435363738393AA3E0273D22'.                         
      *}                                                                        
           05  FILLER      REDEFINES TAB-ADDR.                                  
               10  T-ADDR                PIC X        OCCURS 64.                
       LINKAGE SECTION.                                                         
       01  L-ADDR              PIC XX.                                          
      *=================================================================        
       PROCEDURE DIVISION USING L-ADDR.                                         
           MOVE L-ADDR              TO ZHWA.                                    
           IF ZHW > 4095                                                        
           OR ZHW < ZERO                                                        
              PERFORM VARYING AD1 FROM 1 BY 1                                   
                      UNTIL   AD1 > 64                                          
                      OR      T-ADDR (AD1) = ZHWA (1:1)                         
              END-PERFORM                                                       
              PERFORM VARYING AD2 FROM 1 BY 1                                   
                      UNTIL   AD2 > 64                                          
                      OR      T-ADDR (AD2) = ZHWA (2:1)                         
              END-PERFORM                                                       
              IF AD1 > 64                                                       
              OR AD2 > 64                                                       
                 COMPUTE ZHW = ZHW / WZERO                                      
              END-IF                                                            
              COMPUTE ZHW = ((AD1 - 1) * 64) + (AD2 - 1)                        
              MOVE ZHWA             TO L-ADDR                                   
           END-IF.                                                              
           GOBACK.                                                              
