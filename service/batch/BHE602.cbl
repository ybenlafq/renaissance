      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID         DIVISION.                                             00000020
       PROGRAM-ID.  BHE602.                                             00000040
       AUTHOR.   DSA042.                                                00000050
       DATE-COMPILED.                                                   00000060
      ******************************************************************00000080
      * 06.05.2009                                                     *00000090
      * ==========                                                     *00000090
      *                                                                *00000090
      * SUPPRESSION DE DOUBLON                                         *00000090
      ******************************************************************00000080
      *                                                                 00000080
       ENVIRONMENT DIVISION.                                            00000150
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *****************************************************************         
      * FICHIERS EN ENTREE :                                          *         
      *****************************************************************         
      * FJHE60E : FICHIER D'EDITION BON D'ENVOI FOURNISSEUR           *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJHE60E    ASSIGN   TO    FJHE60E                     00000250
      *                      FILE STATUS ST-FJHE60E.                            
      *--                                                                       
           SELECT FJHE60E    ASSIGN   TO    FJHE60E                             
                             FILE STATUS ST-FJHE60E                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *****************************************************************         
      * FJHE60S : FICHIER D'EDITION BON D'ENVOI FOURNISSEUR SANS      *         
      *           DOUBLON                                             *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJHE60S    ASSIGN   TO    FJHE60S                     00000250
      *                      FILE STATUS ST-FJHE60S.                            
      *--                                                                       
           SELECT FJHE60S    ASSIGN   TO    FJHE60S                             
                             FILE STATUS ST-FJHE60S                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE  SECTION.                                                   00000290
      *                                                                 00000190
       FD  FJHE60E                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FJHE60E-ENREG             PIC X(243).                                
      *                                                                 00000190
      *                                                                 00000190
       FD  FJHE60S                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FJHE60S-ENREG             PIC X(243).                                
      *                                                                         
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                         00000710
       77  ST-FJHE60E                PIC X(02).                                 
       77  ST-FJHE60S                PIC X(02).                                 
       77  WRK-NBRLUS                PIC 9(05)  COMP-3 VALUE ZERO.              
       77  WRK-JHEECR                PIC 9(05)  COMP-3 VALUE ZERO.              
      *                                                                 00000600
       01  WRK-FLAGS                 PIC X(01)  VALUE SPACES.                   
           88  WRK-FINTRT            VALUE '1'.                                 
      *                                                                         
       01 W-CLE-FIC.                                                            
          03 W-CLE-CENTRE       PIC X(05).                                      
          03 W-CLE-NLOT         PIC 9(07).                                      
      *   03 W-CLE-NDEP         PIC X(03).                                      
      *   03 W-CLE-NHS          PIC X(07).                                      
       01 WS-CLE-FIC.                                                           
          03 WS-CLE-CENTRE       PIC X(05).                                     
          03 WS-CLE-NLOT         PIC 9(07).                                     
      *   03 WS-CLE-NDEP         PIC X(03).                                     
      *   03 WS-CLE-NHS          PIC X(07).                                     
       01 WS-DERNUM              PIC X(08).                                     
      *                                                                 00000600
           COPY SWEFHE60.                                                       
      *                                                                 00000600
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
       TRT-GENERAL  SECTION.                                                    
           PERFORM  MODULE-ENTREE.                                              
           PERFORM  MODULE-INITIAL  UNTIL WRK-FINTRT.                           
           PERFORM  MODULE-SORTIE.                                              
           GOBACK.                                                              
       TRT-GENERAL-F. EXIT.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *    OUVERTURE DES FICHIERS FJHE60E FJHE60S                      *        
      *----------------------------------------------------------------*        
       MODULE-ENTREE  SECTION.                                                  
           OPEN  INPUT FJHE60E.                                                 
           IF  ST-FJHE60E        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE60E = ' ST-FJHE60E         
               GOBACK                                                           
           END-IF.                                                              
           OPEN OUTPUT FJHE60S.                                                 
           IF  ST-FJHE60S        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE60S = ' ST-FJHE60S         
               CLOSE   FJHE60E                                                  
               GOBACK                                                           
           END-IF.                                                              
      *--- PREMIERE LECTURE                                                     
           PERFORM  MODULE-READ-FJHE60E.                                        
       MODULE-ENTREE-F. EXIT.                                                   
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *    CREATION DU FICHIER FJHE60S                                 *        
      *----------------------------------------------------------------*        
      *                                                                 00002950
       MODULE-INITIAL  SECTION.                                         00002330
           MOVE HE60-CENTRE          TO WS-CLE-CENTRE                           
           MOVE HE60-NDEM            TO WS-CLE-NLOT                             
      *    MOVE HE60-NDEP            TO WS-CLE-NDEP                             
      *    MOVE HE60-NHS             TO WS-CLE-NHS                              
      *--- A CHAQUE CHANGEMENT DE CLE, ECRITURE DANS LE FIC EN SORTIE           
      *--- A LA CLE, VERIFIER QU ON EST SUR LA DER MODIF DU LOT                 
           IF W-CLE-FIC NOT = WS-CLE-FIC                                        
              MOVE WS-CLE-FIC   TO W-CLE-FIC                                    
              MOVE HE60-DATEF   TO WS-DERNUM                                    
              PERFORM MODULE-ECRIT-FJHE60S                                      
           ELSE                                                                 
              IF HE60-DATEF  =  WS-DERNUM                                       
                 PERFORM MODULE-ECRIT-FJHE60S                                   
              END-IF                                                            
           END-IF                                                               
      *--- LECTURE SUIVANTE                                                     
           PERFORM MODULE-READ-FJHE60E.                                         
       MODULE-INITIAL-F. EXIT.                                          00002620
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *      FERMETURE DU FICHIER                                      *        
      *----------------------------------------------------------------*        
       MODULE-SORTIE  SECTION.                                          00002330
           CLOSE   FJHE60E  FJHE60S.                                            
           IF  ST-FJHE60E            = '00' AND                                 
               ST-FJHE60S            = '00'                                     
               DISPLAY '* BHE602 : CREATION FJHE60S O.K ***'                    
           ELSE                                                                 
               DISPLAY '* BHE602 : PROBLEME FERMETURE FICHIERS '                
               DISPLAY '*          FICHIER FJHE60E = ' ST-FJHE60E               
               DISPLAY '*          FICHIER FJHE60S = ' ST-FJHE60S               
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*          NBR ENREG. LUS FJHE60E = ' WRK-NBRLUS.           
           DISPLAY '*          NBR ENREG. ECR FJHE60S = ' WRK-JHEECR.           
       MODULE-SORTIE-F. EXIT.                                           00002620
      *                                                                         
      *                                                                         
       MODULE-READ-FJHE60E SECTION.                                             
           READ FJHE60E AT END                                                  
                SET WRK-FINTRT     TO TRUE                                      
            NOT AT END                                                          
                IF ST-FJHE60E  =  '00'                                          
                   MOVE FJHE60E-ENREG    TO HE60-ENREG                          
                   ADD  1          TO WRK-NBRLUS                                
                ELSE                                                            
                   DISPLAY '* BHE602 : PB LECTURE FJHE60E = '                   
                      ST-FJHE60E                                                
                   CLOSE FJHE60E FJHE60S                                        
                   GOBACK                                                       
                END-IF                                                          
           END-READ.                                                            
       MODULE-READ-FJHE60E-F. EXIT.                                             
      *                                                                         
      *                                                                         
       MODULE-ECRIT-FJHE60S  SECTION.                                           
           MOVE SPACES               TO HE60-NSEQ.                              
           MOVE HE60-DATED           TO HE60-DATEF                              
           WRITE FJHE60S-ENREG  FROM HE60-ENREG.                                
           IF  ST-FJHE60S        NOT =  '00'                                    
               DISPLAY '* BHE602 : PB ECRITURE FJHE60S = ' ST-FJHE60S           
               CLOSE FJHE60E FJHE60S                                            
               GOBACK                                                           
           END-IF.                                                              
           ADD 1                     TO WRK-JHEECR.                             
       MODULE-ECRIT-FJHE60S-F. EXIT.                                            
      *                                                                 00002950
      *********** F I N  D E  'B H E 6 0 1' ****************************00002950
