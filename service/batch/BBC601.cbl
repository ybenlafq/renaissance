      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BBC601.                                                   
       AUTHOR.        J.CELAIRE.                                                
       DATE-WRITTEN.  27/10/2006.                                               
      ******************************************************************        
      * PROJET CARTE DARTY : EXTRACTION CARTE CLIENT                   *        
      *                                                                *        
      * MISE EN FORME CSV DU FICHIER D'EXTRACTION DES CARTES CLIENT    *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
      *                                                                         
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *****************************************************************         
      * FICHIER EN ENTREE:                                            *         
      *****************************************************************         
      * FBC600  : FICHIER D'EXTRACTION                                *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FBC600     ASSIGN TO FBC600                                   
      *                      FILE STATUS ST-FBC600.                             
      *--                                                                       
           SELECT FBC600     ASSIGN TO FBC600                                   
                             FILE STATUS ST-FBC600                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *****************************************************************         
      * FICHIER EN SORTIE:                                            *         
      *****************************************************************         
      * FBC601  : FICHIER DONNEES CLIENT POUR PROSODIE                *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FBC601     ASSIGN TO FBC601                                   
      *                      FILE STATUS ST-FBC601.                             
      *--                                                                       
           SELECT FBC601     ASSIGN TO FBC601                                   
                             FILE STATUS ST-FBC601                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *****************************************************************         
      * DESCRIPTION DES FD DES FICHIERS                               *         
      *****************************************************************         
      ****** ENTREE ********                                                    
       FD  FBC600                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD                                                
           DATA RECORD IS FBC600-ENREG.                                         
       01  FBC600-ENREG.                                                        
           05  FBC600-ENREGISTREMENT PIC X(100).                                
      *                                                                         
      ****** SORTIE ********                                                    
       FD  FBC601                                                               
           RECORDING V                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD                                                
           RECORD CONTAINS 1 TO 100 CHARACTERS                                  
           DATA RECORD IS FBC601-ENREG.                                         
       01  FBC601-ENREG.                                                        
           05  FBC601-ENREGISTREMENT PIC X(1)                                   
                                     OCCURS 1 TO 100 TIMES                      
                                     DEPENDING ON I-LONG.                       
      *                                                                         
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      *    STATUT DES OPERATIONS SUR FICHIER.                                   
       77  ST-FBC600                  PIC 9(02) VALUE ZERO.                     
       77  ST-FBC601                  PIC 9(02) VALUE ZERO.                     
      *    RECUPERATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    COPIES D'ABEND ET DE CONTROLE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                                      
           COPY  WORKDATC.                                                      
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
       01  WS-SSAAMMJJ                     PIC X(08)   VALUE SPACES.            
       01  WS-HHMMSSCC   .                                                      
          05  WS-HHMMSS                    PIC X(06) .                          
          05  WS-CC                        PIC X(02) .                          
      *                                                                         
       01  WS-ENTETE-1                PIC X(111)     VALUE              00000480
           ';Etat sur les ventes, sur l''attribution des cartes client;'        
                   .                                                            
       01  WS-ENTETE-1B               PIC X(111)     VALUE              00000480
           ';en magasin et sur les cartes expediees par courrier;'.             
       01  WS-ENTETE-2                PIC X(13)      VALUE              00000480
           ';Periode du '.                                                      
       01  WS-ENTETE-3                PIC X(85)      VALUE              00000480
           'no_societe;lib_soc;no_lieu;lib_lieu;c_vendeur;nbr_carte_attr        
      -    ';nbr_carte_exp;nbr_vente;'.                                         
      *                                                                         
        01  W-DEB-FBC600              PIC  X(01)  VALUE  ' '.                   
          88  DEB-FBC600                          VALUE  ' '.                   
          88  FIN-FBC600                          VALUE  '1'.                   
      *                                                                         
        01 WS-SSAAMMJJ-DEB            PIC X(08) VALUE SPACES.                   
        01 WS-SSAAMMJJ-FIN            PIC X(08) VALUE SPACES.                   
      *                                                                         
      ****  NOM DU MODULE DATE                                                  
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *                                                                         
       01 WS-ENREGISTREMENT   PIC X(100).                                       
      *                                                                         
       01 WS-ENREG.                                                             
         02 WS-NSOCIETE       PIC X(03).                                        
         02 WS-NLIEU          PIC X(03).                                        
         02 WS-CVENDEUR       PIC X(06).                                        
         02 WS-NBR-CARTE-X    PIC X(09).                                        
         02 FILLER REDEFINES WS-NBR-CARTE-X.                                    
           03 WS-NBR-CARTE-9  PIC 9(09).                                        
         02 WS-NBR-DEMAN-X    PIC X(09).                                        
         02 FILLER REDEFINES WS-NBR-DEMAN-X.                                    
           03 WS-NBR-DEMAN-9  PIC 9(09).                                        
         02 WS-NBR-VENTE-X    PIC X(09).                                        
         02 FILLER REDEFINES WS-NBR-VENTE-X.                                    
           03 WS-NBR-VENTE-9  PIC 9(09).                                        
         02 WS-LSOC           PIC X(20).                                        
         02 WS-LLIEU          PIC X(20).                                        
         02 WS-DATE-DEB       PIC X(08).                                        
         02 WS-DATE-FIN       PIC X(08).                                        
         02 WS-FILLER         PIC X(05).                                        
      *                                                                         
       01 WS-ENREG-ANC.                                                         
         02 WS-NSOCIETE-ANC   PIC X(03).                                        
         02 WS-NLIEU-ANC      PIC X(03).                                        
         02 WS-CVENDEUR-ANC   PIC X(06).                                        
         02 WS-NBR-CARTE-X-ANC    PIC X(09).                                    
         02 FILLER REDEFINES WS-NBR-CARTE-X-ANC.                                
           03 WS-NBR-CARTE-9-ANC  PIC 9(09).                                    
         02 WS-NBR-DEMAN-X-ANC    PIC X(09).                                    
         02 FILLER REDEFINES WS-NBR-DEMAN-X-ANC.                                
           03 WS-NBR-DEMAN-9-ANC  PIC 9(09).                                    
         02 WS-NBR-VENTE-X-ANC    PIC X(09).                                    
         02 FILLER REDEFINES WS-NBR-VENTE-X-ANC.                                
           03 WS-NBR-VENTE-9-ANC  PIC 9(09).                                    
         02 WS-LSOC-ANC           PIC X(20).                                    
         02 WS-LLIEU-ANC          PIC X(20).                                    
         02 WS-FILLER             PIC X(21).                                    
      *                                                                         
       01 ZONE-EDIT           PIC X(100).                               00000480
       01 ZONEX               PIC X(100).                               00000480
       01 I-TEXTE             PIC S9(3)  COMP-3 VALUE 0.                        
       01 I-ZONE              PIC S9(3)  COMP-3 VALUE 0.                        
       01 I-LONG              PIC S9(3)  COMP-3 VALUE 0.                        
      *                                                                         
      *****************************************************************         
      *    FORMATAGE DU COMPTE-RENDU                                  *         
      *****************************************************************         
       01  LIGNE1.                                                              
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE2.                                                              
         02  FILLER   PIC X(31) VALUE '! DEBUT NORMAL DU PROGRAMME    '.        
         02  FILLER   PIC X(24) VALUE '                       !'.               
       01  LIGNE3.                                                              
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE4.                                                              
         02  FILLER   PIC X(31) VALUE '! ESPACE CLIENT : EXTRACTION CA'.        
         02  FILLER   PIC X(24) VALUE 'RTES CLIENT DARTY      !'.               
       01  LIGNE5.                                                              
         02  FILLER   PIC X(31) VALUE '! MISE EN FORME .CSV DU FICHIER'.        
         02  FILLER   PIC X(24) VALUE ' D EXTRACTION DE LA    !'.               
       01  LIGNE6.                                                              
         02  FILLER   PIC X(31) VALUE '! CARTE CLIENT DARTY.          '.        
         02  FILLER   PIC X(24) VALUE '                       !'.               
       01  LIGNE8.                                                              
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE9.                                                              
         02  FILLER   PIC X(21) VALUE '! DATE  DE PASSAGE : '.                  
         02  L9-Z1      PIC X(2).                                               
         02  FILLER   PIC X(1) VALUE '/'.                                       
         02  L9-Z2      PIC X(2).                                               
         02  FILLER   PIC X(1) VALUE '/'.                                       
         02  L9-Z3      PIC X(4).                                               
         02  FILLER   PIC X(24) VALUE '                       !'.               
       01  LIGNE10.                                                             
         02  FILLER   PIC X(21) VALUE '! HEURE DE PASSAGE : '.                  
         02  L10-Z1     PIC X(8).                                               
         02  FILLER   PIC X(27) VALUE '                         ! '.            
       01  LIGNE11.                                                             
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE12.                                                             
         02  FILLER   PIC X(21) VALUE '! VERSION COMPILEE : '.                  
         02  L12-Z1      PIC X(2).                                              
         02  FILLER   PIC X(1) VALUE '/'.                                       
         02  L12-Z2      PIC X(2).                                              
         02  FILLER   PIC X(3) VALUE '/20'.                                     
         02  L12-Z3      PIC X(2).                                              
         02  FILLER   PIC X(24) VALUE '                       !'.               
       01  LIGNE13.                                                             
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE14.                                                             
         02  FILLER   PIC X(1) VALUE ' '.                                       
       01  LIGNE16.                                                             
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE17.                                                             
         02  FILLER   PIC X(31) VALUE '! NBRE ENREGISTREMENTS LUS    :'.        
         02  FILLER   PIC X(14) VALUE '      '.                                 
         02  L17-Z1   PIC X(9)  VALUE SPACE.                                    
       01  LIGNE18.                                                             
         02  FILLER   PIC X(31) VALUE '! NBRE ENREGISTREMENTS ECRITS :'.        
         02  FILLER   PIC X(14) VALUE '      '.                                 
         02  L18-Z1   PIC X(9)  VALUE SPACE.                                    
       01  LIGNE19.                                                             
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
       01  LIGNE20.                                                             
         02  FILLER   PIC X(31) VALUE '! FIN NORMAL DU PROGRAMME      '.        
         02  FILLER   PIC X(24) VALUE '                       !'.               
       01  LIGNE21.                                                             
         02  FILLER   PIC X(31) VALUE '!------------------------------'.        
         02  FILLER   PIC X(24) VALUE '-----------------------!'.               
      *                                                                         
       01  WS-NBR-LUS-1                PIC S9(07) COMP-3   VALUE 0.             
       01  WS-NBR-ECR-1                PIC S9(07) COMP-3   VALUE 0.             
      *                                                                         
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *      PROGRAMME PRINCIPAL                                       *        
      *----------------------------------------------------------------*        
      *                                                                         
       MODULE-BBC601                     SECTION.                               
      *                                                                         
      * **** INITIALISATIONS **********                                         
           PERFORM DEBUT-ENTETE                                                 
      *                                                                         
      * **** LECTURE FICHIER SEQUENTIELLE ****                                  
           PERFORM READ-FBC600                                                  
      *                                                                         
      * **** ECRITURE DE L'ENTETE ****                                          
           IF DEB-FBC600                                                        
             PERFORM ECRITURE-ENTETE                                            
           END-IF                                                               
      *                                                                         
           PERFORM UNTIL FIN-FBC600                                             
             IF  ( WS-NSOCIETE NOT = WS-NSOCIETE-ANC                            
               OR  WS-NLIEU    NOT = WS-NLIEU-ANC                               
               OR  WS-CVENDEUR NOT = WS-CVENDEUR-ANC )                          
               AND WS-NSOCIETE-ANC > SPACES                                     
               PERFORM ALIMENTATION-FBC601                                      
             END-IF                                                             
      *                                                                         
             MOVE WS-NSOCIETE      TO WS-NSOCIETE-ANC                           
             MOVE WS-NLIEU         TO WS-NLIEU-ANC                              
             MOVE WS-CVENDEUR      TO WS-CVENDEUR-ANC                           
             IF WS-NBR-CARTE-9 IS NUMERIC                                       
                MOVE WS-NBR-CARTE-X TO WS-NBR-CARTE-X-ANC                       
             END-IF                                                             
             IF WS-NBR-DEMAN-9 IS NUMERIC                                       
                MOVE WS-NBR-DEMAN-X TO WS-NBR-DEMAN-X-ANC                       
             END-IF                                                             
             IF WS-NBR-VENTE-9 IS NUMERIC                                       
                MOVE WS-NBR-VENTE-X TO WS-NBR-VENTE-X-ANC                       
             END-IF                                                             
             IF WS-LSOC > SPACES                                                
                MOVE WS-LSOC          TO WS-LSOC-ANC                            
             END-IF                                                             
             IF WS-LLIEU > SPACES                                               
                MOVE WS-LLIEU         TO WS-LLIEU-ANC                           
             END-IF                                                             
      *                                                                         
      * **** LECTURE ENREGISTREMENT SUIVANT ***********                         
             PERFORM READ-FBC600                                                
           END-PERFORM                                                          
      *                                                                         
      * **** ALIMENTATION DU DERNIER ENREGISTREMENT ***                         
           IF WS-NBR-LUS-1 > 0                                                  
             PERFORM ALIMENTATION-FBC601                                        
           END-IF                                                               
      *                                                                         
           PERFORM FIN-BBC601.                                                  
      *                                                                         
       FIN-MODULE-BBC601.  EXIT.                                                
              EJECT                                                             
      *                                                                         
      *-----------------------------------------------------------------        
      * ECRITURE ENTETE                                                         
      *-----------------------------------------------------------------        
      *                                                                         
       ECRITURE-ENTETE                   SECTION.                               
      *                                                                         
      ***** ENTETE 1/4 ****                                                     
           INITIALIZE WS-ENREGISTREMENT                                         
           INITIALIZE ZONE-EDIT                                                 
           MOVE  WS-ENTETE-1          TO ZONE-EDIT                              
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT            TO WS-ENREGISTREMENT                      
           COMPUTE I-LONG = I-TEXTE - 1                                         
           WRITE  FBC601-ENREG    FROM WS-ENREGISTREMENT                        
      *                                                                         
      ***** ENTETE 2/4 ****                                                     
           INITIALIZE WS-ENREGISTREMENT                                         
           INITIALIZE ZONE-EDIT                                                 
           MOVE  WS-ENTETE-1B         TO ZONE-EDIT                              
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT            TO WS-ENREGISTREMENT                      
           COMPUTE I-LONG = I-TEXTE - 1                                         
           WRITE  FBC601-ENREG    FROM WS-ENREGISTREMENT                        
      *                                                                         
      ***** ENTETE 3/4 ****                                                     
           INITIALIZE WS-ENREGISTREMENT                                         
           INITIALIZE ZONE-EDIT                                                 
           STRING WS-ENTETE-2                                                   
                      WS-DATE-DEB(7:2) '/'                                      
                      WS-DATE-DEB(5:2) '/'                                      
                      WS-DATE-DEB(1:4) ' au '                                   
                      WS-DATE-FIN(7:2) '/'                                      
                      WS-DATE-FIN(5:2) '/'                                      
                      WS-DATE-FIN(1:4)                                          
           DELIMITED BY SIZE         INTO WS-ENREGISTREMENT                     
      *                                                                         
           MOVE  WS-ENREGISTREMENT    TO ZONE-EDIT                              
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT            TO WS-ENREGISTREMENT                      
           COMPUTE I-LONG = I-TEXTE - 1                                         
           WRITE  FBC601-ENREG    FROM WS-ENREGISTREMENT                        
      *                                                                         
      ***** ENTETE 4/4 ****                                                     
           INITIALIZE WS-ENREGISTREMENT                                         
           INITIALIZE ZONE-EDIT                                                 
           MOVE  WS-ENTETE-3          TO ZONE-EDIT                              
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT            TO WS-ENREGISTREMENT                      
           COMPUTE I-LONG = I-TEXTE - 1                                         
           WRITE  FBC601-ENREG    FROM WS-ENREGISTREMENT                        
      *                                                                         
           ADD 3 TO WS-NBR-ECR-1                                                
                               .                                                
       FIN-ECRITURE-ENTETE . EXIT.                                              
              EJECT                                                             
      *                                                                         
      *-----------------------------------------------------------------        
      * ALIMENTATION FBC601                                                     
      *-----------------------------------------------------------------        
      *                                                                         
       ALIMENTATION-FBC601               SECTION.                               
      *                                                                         
           INITIALIZE WS-ENREGISTREMENT                                         
           INITIALIZE ZONE-EDIT                                                 
      *                                                                 00002950
           IF WS-NSOCIETE-ANC = 'XXX'                                           
             MOVE SPACES          TO WS-NSOCIETE-ANC                            
           END-IF                                                               
           IF WS-NLIEU-ANC = 'XXX'                                              
             MOVE SPACES          TO WS-NLIEU-ANC                               
           END-IF                                                               
           IF WS-CVENDEUR-ANC = 'XXXXXX'                                        
             MOVE SPACES          TO WS-CVENDEUR-ANC                            
           END-IF                                                               
           IF WS-NBR-CARTE-9-ANC IS NOT NUMERIC                                 
             MOVE 0               TO WS-NBR-CARTE-9-ANC                         
           END-IF                                                               
           IF WS-NBR-DEMAN-9-ANC IS NOT NUMERIC                                 
             MOVE 0               TO WS-NBR-DEMAN-9-ANC                         
           END-IF                                                               
           IF WS-NBR-VENTE-9-ANC IS NOT NUMERIC                                 
             MOVE '   NC'             TO WS-NBR-VENTE-X-ANC                     
           END-IF                                                               
      *                                                                 00002950
           STRING                                                               
                   WS-NSOCIETE-ANC                      ';'                     
                   WS-LSOC-ANC                          ';'                     
                   WS-NLIEU-ANC                         ';'                     
                   WS-LLIEU-ANC                         ';'                     
                   WS-CVENDEUR-ANC                      ';'                     
                   WS-NBR-CARTE-X-ANC                   ';'                     
                   WS-NBR-DEMAN-X-ANC                   ';'                     
                   WS-NBR-VENTE-X-ANC                   ';'                     
           DELIMITED BY SIZE INTO WS-ENREGISTREMENT                             
      *                                                                 00002950
           MOVE  WS-ENREGISTREMENT    TO ZONE-EDIT                              
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE  ZONE-EDIT            TO WS-ENREGISTREMENT                      
      *                                                                         
           ADD 1 TO WS-NBR-ECR-1                                                
           COMPUTE I-LONG = I-TEXTE - 1                                         
           WRITE  FBC601-ENREG      FROM WS-ENREGISTREMENT                      
      *                                                                         
           INITIALIZE WS-ENREG-ANC                                              
                               .                                                
      *                                                                         
       FIN-ALIMENTATION-FBC601. EXIT.                                           
              EJECT                                                             
      *                                                                         
      *-----------------------------------------------------------------        
      * LES BLANC SUPERFLU SON ENLEVEES                                         
      *-----------------------------------------------------------------        
      *                                                                 00002950
       MISE-EN-FORME-ENREG  SECTION.                                    00002330
      *                                                                 00002950
           INSPECT ZONE-EDIT   REPLACING ALL LOW-VALUE BY SPACE                 
      *                                                                 00002950
           PERFORM VARYING I-TEXTE FROM 1 BY 1                                  
             UNTIL (I-TEXTE > 100)                                              
                OR (ZONE-EDIT(I-TEXTE:) = ' ')                                  
              IF ZONE-EDIT(I-TEXTE:2) = '  '                                    
              OR ZONE-EDIT(I-TEXTE:2) = ' ;'                                    
                 COMPUTE I-ZONE = I-TEXTE + 1                                   
                 MOVE ZONE-EDIT(I-ZONE:) TO ZONEX                               
                 MOVE ZONEX TO ZONE-EDIT(I-TEXTE:)                              
                 IF I-TEXTE > 1                                                 
                    COMPUTE I-TEXTE = I-TEXTE - 1                               
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                 00002950
       FIN-MEF-ENREG         . EXIT.                                    00002620
              EJECT                                                     00002620
      *                                                                 00002950
      *-----------------------------------------------------------------        
      * ENTETE                                                                  
      *-----------------------------------------------------------------        
      *                                                                         
       DEBUT-ENTETE                      SECTION.                               
      *                                                                         
      *    MESSAGE DE DEBUT DE PROGRAMME.                                       
      *                                                                         
           DISPLAY  LIGNE1                                                      
           DISPLAY  LIGNE2                                                      
           DISPLAY  LIGNE3                                                      
           DISPLAY  LIGNE4                                                      
           DISPLAY  LIGNE5                                                      
           DISPLAY  LIGNE6                                                      
      *                                                                         
           DISPLAY  LIGNE8                                                      
      *    RECUPERATION DE DATE ET HEURE DE TRAITEMENT                          
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD                               
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           MOVE  WS-SSAAMMJJ(7:2)  TO L9-Z1                                     
           MOVE  WS-SSAAMMJJ(5:2)  TO L9-Z2                                     
           MOVE  WS-SSAAMMJJ(1:4)  TO L9-Z3                                     
           DISPLAY  LIGNE9                                                      
           MOVE  WS-HHMMSSCC       TO L10-Z1                                    
           DISPLAY  LIGNE10                                                     
           DISPLAY  LIGNE11                                                     
      *    RECUPERATION DE DATE DE COMPILATION PROGRAMME                        
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED                               
           MOVE WS-WHEN-COMPILED-JJ-1 TO L12-Z1                                 
           MOVE WS-WHEN-COMPILED-MM-1 TO L12-Z2                                 
           MOVE WS-WHEN-COMPILED-AA-1 TO L12-Z3                                 
           DISPLAY  LIGNE12                                                     
           DISPLAY  LIGNE13                                                     
           DISPLAY  LIGNE14                                                     
      *                                                                         
      *    OUVERTURE DU FICHIER EN ENTREE.                                      
           OPEN  INPUT  FBC600                                                  
           OPEN  OUTPUT FBC601                                                  
           PERFORM CHARGEMENT-DSYST                                             
                        .                                                       
      *                                                                         
       FIN-DEBUT-ENTETE. EXIT.                                                  
              EJECT                                                             
      *                                                                         
      *----------------------------------------------------------------*        
      * FIN NORMALE DU PROGRAMME                                       *        
      *----------------------------------------------------------------*        
      *                                                                         
       FIN-BBC601                        SECTION.                               
      *                                                                         
           MOVE WS-NBR-LUS-1     TO L17-Z1                                      
           MOVE WS-NBR-ECR-1     TO L18-Z1                                      
      *                                                                         
      *    MESSAGE DE FIN DE PROGRAMME                                          
           DISPLAY  LIGNE16                                                     
           DISPLAY  LIGNE17                                                     
           DISPLAY  LIGNE18                                                     
           DISPLAY  LIGNE19                                                     
           DISPLAY  LIGNE20                                                     
           DISPLAY  LIGNE21                                                     
      *                                                                         
           CLOSE  FBC600 FBC601                                                 
                    .                                                           
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BBC601. EXIT.                                                    
              EJECT                                                             
      *                                                                         
      *----------------------------------------------------------------*        
      * FIN ANORMALE DU PROGRAMME                                      *        
      *----------------------------------------------------------------*        
      *                                                                         
       FIN-ANORMALE                      SECTION.                               
      *                                                                         
           CLOSE  FBC600 FBC601                                                 
           DISPLAY '***************************************'                    
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'                    
           DISPLAY '***************************************'                    
      *                                                                         
           MOVE  'BBC601'  TO    ABEND-PROG                                     
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
      *                                                                         
       FIN-FIN-ANORMALE. EXIT.                                                  
              EJECT                                                             
      *                                                                         
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTREE                                            
      *-----------------------------------------------------------------        
      *                                                                         
       READ-FBC600                       SECTION.                               
      *                                                                         
           INITIALIZE WS-ENREG                                                  
      *                                                                         
           READ FBC600  INTO   WS-ENREG  AT END                                 
                SET FIN-FBC600     TO TRUE                                      
                        NOT AT END                                              
                ADD  1             TO WS-NBR-LUS-1                              
           END-READ                                                             
                          .                                                     
       FIN-READ-FBC600. EXIT.                                                   
      *                                                                         
      *----------------------------------------------------------------*        
      * RECUPERATION DE LA DATE SYSTEME                                *        
      *----------------------------------------------------------------*        
      *                                                                         
       CHARGEMENT-DSYST                  SECTION.                               
      *                                                                         
           CALL 'SPDATDB2' USING WS-RET                                         
                                 WS-DSYST                                       
           IF WS-RET NOT = ZERO                                                 
              MOVE 'ERREUR RECUPERATION DSYST.' TO ABEND-MESS                   
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
      *                                                                         
       FIN-CHARGEMENT-DSYST     . EXIT.                                         
              EJECT                                                             
       COPY SYBCERRO.                                                           
