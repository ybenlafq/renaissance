      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                     00020099
       PROGRAM-ID. BMC028.                                              00030099
       AUTHOR.     AD - APSIDE.                                                 
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : MGD - RAPPROCHEMENT EMAIL - DATE DE NAISSANCE   *        
      *  PROGRAMME   : BMC028                                          *        
      *  CREATION    : 06/06/2015                                      *        
      *  FONCTION    : ON RAJOUTE LA DATE DE NAISSANCE DU CLIENT       *        
      *                DANS LE FICHIER ISSU DU BMC027                  *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FMC027I                                         *        
      *                FADRESSE                                        *        
      *  EN SORTIE   :                                                 *        
      *                FMC028O                                         *        
      *                                                                *        
      ******************************************************************        
      *****************************************************************         
       ENVIRONMENT DIVISION.                                            00210099
       CONFIGURATION SECTION.                                           00220099
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00230099
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMC027I  ASSIGN TO FMC027I.                                   
      *--                                                                       
           SELECT FMC027I  ASSIGN TO FMC027I                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMC028O  ASSIGN TO FMC028O.                                   
      *--                                                                       
           SELECT FMC028O  ASSIGN TO FMC028O                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FADRESSE ASSIGN TO FADRESSE.                                  
      *--                                                                       
           SELECT FADRESSE ASSIGN TO FADRESSE                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00322599
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIERS EN ENTREE                                                   
      *                                                                         
      *--- FICHIER FMC027I                                                      
       FD   FMC027I                                                             
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  WS-FMC027                     PIC X(500).                            
      *                                                                         
      *--- FICHIER FADRESSE                                                     
       FD   FADRESSE                                                            
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  WS-FADRESSE                   PIC X(100).                            
      *                                                                         
      * FICHIER DE SORTIE                                                       
       FD  FMC028O                                                              
           RECORDING F                                                          
           LABEL RECORD STANDARD                                                
           BLOCK CONTAINS 0 RECORDS.                                            
       01  WS-FMC028O                    PIC X(500).                            
      *                                                                         
      *                                                                         
       WORKING-STORAGE SECTION.                                         00471699
      *-------------------------------*                                         
      *    CONSTANTES - VARAIBLES                                               
      *-------------------------------*                                         
       77 WC-ABEND                       PIC X(08)  VALUE 'ABEND'.      00472099
      *                                                                         
      * COMPTEURS                                                               
       77  WS-CPT-LG-LUES     PIC 9(6) VALUE 0.                                 
       77  WS-CPT-LG-ECRT     PIC 9(9) VALUE 0.                                 
      * DSECT FICHIERS                                                          
      * ON NE DETAILLE PAS TOUT LE FIC CAR ON A JUSTE BESOIN DE L'EMAIL         
       01 ENR-FMC027.                                                           
1        05 FMC027-DATA          PIC X(317).                                    
318      05 FMC027-DNAISS        PIC X(08).                                     
326      05 FILLER               PIC X(01) VALUE ';'.                           
327      05 FMC027-EMAIL         PIC X(100).                                    
427      05 FILLER               PIC X(74) VALUE SPACES.                        
       01 ENR-FADRESSE.                                                         
1        05 FADR-EMAIL           PIC X(80).                                     
81       05 FADR-DNAISS          PIC X(10).                                     
91       05 FILLER               PIC X(10).                                     
       01 ENR-FMC028.                                                           
1        05 FMC028-DATA          PIC X(317) VALUE SPACES.                       
318      05 FMC028-DNAISS        PIC X(010) VALUE SPACES.                       
328      05 FILLER               PIC X(173) VALUE SPACES.                       
      * FLAG FICHIERS                                                           
       01 FMC027I-FLAG              PIC X VALUE 'O'.                            
          88 NON-FIN-FMC027I              VALUE 'O'.                            
          88 FIN-FMC027I                  VALUE 'N'.                            
       01 FADRESSE-FLAG             PIC X VALUE 'O'.                            
          88 NON-FADRESSE                 VALUE 'O'.                            
          88 FIN-FADRESSE                 VALUE 'N'.                            
      *                                                                         
      *-------------------------------*                                         
      *     COPY                                                                
      *-------------------------------*                                         
      *    COPY ABENDCOP.                                               00800099
      *    COPY SYKWSQ10.                                                       
      *    COPY SYBWDIV0.                                                       
      *================================================================*01110099
       PROCEDURE DIVISION.                                              01060099
      *================================================================*01110099
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
      *****************************************************************         
       MODULE-ENTREE             SECTION.                                       
      ***********************************                                       
           DISPLAY '**********************************************'             
           DISPLAY '*  PROGRAMME BMC028 : EXTRACTION CLIENTS MGD *'             
           DISPLAY '**********************************************'             
           OPEN OUTPUT  FMC028O                                                 
           OPEN INPUT   FMC027I FADRESSE.                                       
       FIN-MODULE-ENTREE.           EXIT.                                       
      *                                                                         
      ******************************************************************01110099
       MODULE-TRAITEMENT         SECTION.                                       
      ***********************************                                       
      *                                                                         
           PERFORM LECTURE-FMC027I                                              
           PERFORM LECTURE-FADRESSE                                             
      *                                                                         
           PERFORM UNTIL FIN-FMC027I                                            
              INITIALIZE ENR-FMC028                                             
              MOVE FMC027-DATA TO FMC028-DATA                                   
              PERFORM UNTIL FADR-EMAIL >= FMC027-EMAIL                          
                            OR FIN-FADRESSE                                     
                 PERFORM LECTURE-FADRESSE                                       
              END-PERFORM                                                       
              IF FADR-EMAIL = FMC027-EMAIL THEN                                 
                 MOVE FADR-DNAISS TO FMC028-DNAISS                              
              END-IF                                                            
              PERFORM WRITE-FMC028                                              
              PERFORM LECTURE-FMC027I                                           
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT.       EXIT.                                       
      *                                                                         
      ***************************************************************           
       MODULE-SORTIE             SECTION.                                       
      ***********************************                                       
           CLOSE FMC027I FADRESSE FMC028O                                       
           DISPLAY '*'                                                          
           DISPLAY '**********************************************'             
           DISPLAY '*   RAPPORT INTEGRATION DU FICHIER AXAPTA    *'             
           DISPLAY '**********************************************'             
           DISPLAY '*   NB ENREG LUS              = ' WS-CPT-LG-LUES            
           '    *'                                                              
           DISPLAY '*   NB ENREG ECRITS           = ' WS-CPT-LG-ECRT            
           '    *'                                                              
           DISPLAY '**********************************************'             
           DISPLAY 'FIN PROGRAMME BMC028'                                       
           DISPLAY '**********************************************'             
           GOBACK.                                                              
      *                                                                         
       FIN-MODULE-SORTIE.           EXIT.                                       
      *                                                                         
      *================================================================*01110099
      *================================================================*01110099
      *                     TRAITEMENT DES FICHIERS                    *        
      *================================================================*01110099
      *================================================================*01110099
      *****************************************************************         
       WRITE-FMC028         SECTION.                                            
      ***************************************                                   
      *                                                                         
           WRITE WS-FMC028O FROM ENR-FMC028                                     
           ADD 1   TO WS-CPT-LG-ECRT                                            
           INITIALIZE WS-FMC028O                                                
           .                                                                    
       FIN-WRITE-FMC028.       EXIT.                                            
      *                                                                         
      *****************************************************************         
       LECTURE-FMC027I             SECTION.                                     
      ***************************************                                   
      *                                                                         
           INITIALIZE ENR-FMC027                                                
           READ FMC027I     INTO  ENR-FMC027                                    
                AT END SET FIN-FMC027I TO TRUE                                  
           END-READ                                                             
           IF NOT FIN-FMC027I THEN                                              
              ADD 1 TO WS-CPT-LG-LUES                                           
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FMC027I.           EXIT.                                     
      *                                                                         
      *****************************************************************         
       LECTURE-FADRESSE            SECTION.                                     
      ***************************************                                   
      *                                                                         
           INITIALIZE ENR-FADRESSE                                              
           READ FADRESSE    INTO  ENR-FADRESSE                                  
                AT END SET FIN-FADRESSE TO TRUE                                 
                       MOVE SPACES TO ENR-FADRESSE                              
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FADRESSE.          EXIT.                                     
      *                                                                         
