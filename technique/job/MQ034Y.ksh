#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ034Y.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYMQ034 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/10/10 AT 18.02.45 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MQ034Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMQ035                                                                      
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MQ034YA
       ;;
(MQ034YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MQ034YAA
       ;;
(MQ034YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y1
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y2
#                                                                              
# ***** PARAMETRES                                                             
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y3
#                                                                              
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y4
#                                                                              
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y5
#                                                                              
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y6
#                                                                              
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y7
#                                                                              
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y8
#                                                                              
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y9
#                                                                              
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y10
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y11
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y12
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQOUT ${DATA}/PXX0/F45.FMQ035AY
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQREC ${DATA}/PXX0/F45.FMQ035BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ035 
       JUMP_LABEL=MQ034YAB
       ;;
(MQ034YAB)
       m_CondExec 04,GE,MQ034YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035AP                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ034YAD
       ;;
(MQ034YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y13
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FMQIN ${DATA}/PXX0/F45.FMQ035AY
       m_OutputAssign -c 9 -w IAN010Y IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034YAE
       ;;
(MQ034YAE)
       m_CondExec 04,GE,MQ034YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035BY                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MQ034YAG
       ;;
(MQ034YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y14
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FMQIN ${DATA}/PXX0/F45.FMQ035BY
       m_OutputAssign -c 9 -w IAN010Y IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034YAH
       ;;
(MQ034YAH)
       m_CondExec 04,GE,MQ034YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 99                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034YAJ PGM=BMQ033     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MQ034YAJ
       ;;
(MQ034YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y15
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y16
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y17
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y18
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A3} FMQIN ${DATA}/PXX0/F45.FMQ035AY
       m_ProgramExec BMQ033 
#                                                                              
# ********************************************************************         
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 02                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034YAM PGM=BMQ033     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MQ034YAM
       ;;
(MQ034YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y19
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y20
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y21
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034Y22
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A4} FMQIN ${DATA}/PXX0/F45.FMQ035BY
       m_ProgramExec BMQ033 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
