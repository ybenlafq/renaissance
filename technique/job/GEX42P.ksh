#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEX42P.ksh                       --- VERSION DU 08/10/2016 13:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGEX42 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/17 AT 14.23.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: GEX42P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEX42PA
       ;;
(GEX42PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GEX42PAA
       ;;
(GEX42PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F07.DAHORD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FEX510FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "      "
 /FIELDS FLD_CH_32_6 32 CH 6
 /FIELDS FLD_CH_9_11 9 CH 11
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_2 FLD_CH_32_6 EQ CST_1_6 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_32_6 ASCENDING,
   FLD_CH_9_11 ASCENDING
 /SUMMARIZE
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEX42PAB
       ;;
(GEX42PAB)
       m_CondExec 00,EQ,GEX42PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX510 TRAITEMENT DU FICHIER DES COMMANDES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEX42PAD
       ;;
(GEX42PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSSU10   : NAME=RSSU10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
#                                                                              
#    RSSU10   : NAME=RSSU10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 560                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOC560
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GEX42PAD
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A1} FEX510 ${DATA}/PXX0/F07.FEX510FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX510 
       JUMP_LABEL=GEX42PAE
       ;;
(GEX42PAE)
       m_CondExec 04,GE,GEX42PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX511 VERIFICATION DES LIGNES DE COMMANDES                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEX42PAG
       ;;
(GEX42PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH00 /dev/null
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS25   : NAME=RSGS25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS25 /dev/null
#    RSSU10   : NAME=RSSU10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
#                                                                              
#    RSSU10   : NAME=RSSU10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 560                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOC560
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GEX42PAG
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX511 
       JUMP_LABEL=GEX42PAH
       ;;
(GEX42PAH)
       m_CondExec 04,GE,GEX42PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX512 TRAITEMENT DES COMMANDES                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEX42PAJ
       ;;
(GEX42PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLI01   : NAME=RSLI01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI01 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSPT99   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT99 /dev/null
#    RSSU10   : NAME=RSSU10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
#                                                                              
#    RSSU10   : NAME=RSSU10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSPT99   : NAME=RSPT01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT99 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 560                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOC560
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GEX42PAJ
#         FICHIER SORTIE                                                       
# FEX512   FILE  NAME=FEX512BP,MODE=O                                          
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX512 
       JUMP_LABEL=GEX42PAK
       ;;
(GEX42PAK)
       m_CondExec 04,GE,GEX42PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEX514 CR TRAITEMENT                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEX42PAM
       ;;
(GEX42PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#    RSPT99   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT99 /dev/null
#    RSSU10   : NAME=RSSU10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU10 /dev/null
#    RSSU20   : NAME=RSSU20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU20 /dev/null
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 936                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOC560
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/GEX42PAM
#         FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 164 -t LSEQ -g +1 FEX512 ${DATA}/PXX0/F07.FEX512BP
#                                                                              
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          

       m_ProgramExec -b BEX514 
       JUMP_LABEL=GEX42PAQ
       ;;
(GEX42PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEX42PAQ.sysin
       m_UtilityExec
# ****************************************************************             
#  REMISE A ZERO DES FICHIERS RDPP.SEM.XXX510AP ET 512AP                       
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GEX42PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEX42PAT
       ;;
(GEX42PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.DAHORD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F07.FEX510BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +2 OUT3 ${DATA}/PXX0/F07.FEX510FP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEX42PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GEX42PAU
       ;;
(GEX42PAU)
       m_CondExec 16,NE,GEX42PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
