#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ101O.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POMQ101 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/02/28 AT 10.16.07 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MQ101O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMQ100 : RECUPERATION DES MESSAGES MQ S59 QUI POLLUE LA QUE 0003            
#           ET QUI RALLONGE LES DUREES DES BATCHS                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MQ101OA
       ;;
(MQ101OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=MQ101OAA
       ;;
(MQ101OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****  PARAMETRAGE PGM                                                       
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MQ101OAA
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/MQ101OAA
#                                                                              
# *****  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                          
#    RSMQ15O  : NAME=RSMQ15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  FICHIER LG 49                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -g +1 FTS059 ${DATA}/PTEM/MQ101OAA.BNM159AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=MQ101OAB
       ;;
(MQ101OAB)
       m_CondExec 04,GE,MQ101OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES S58 CA PAS TRIATE              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ101OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ101OAD
       ;;
(MQ101OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MQ101OAD
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/MQ101OAD
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15O  : NAME=RSMQ15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  FICHIER LG 30 FICHIER VENTES DARTY                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FTS058 ${DATA}/PXX0/F16.BNM158AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=MQ101OAE
       ;;
(MQ101OAE)
       m_CondExec 04,GE,MQ101OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BMQ101                                                               
#   ------------                                                               
#   PURGE DES QUEUES APRES REMONT�ES                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ101OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MQ101OAG
       ;;
(MQ101OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# -----   PARAMETRES                                                           
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ101OAG
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MQ101O1
# ------  TABLE EN LECTURE                                                     
#    RTMQ15   : NAME=RSMQ15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTMQ15 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ101 
       JUMP_LABEL=MQ101OAH
       ;;
(MQ101OAH)
       m_CondExec 04,GE,MQ101OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MQ101OZA
       ;;
(MQ101OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MQ101OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
