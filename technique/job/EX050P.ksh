#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EX050P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEX050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/03 AT 13.59.53 BY OPERATB                      
#    STANDARDS: P  JOBSET: EX050P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BEX050 ALIMENTATION MUT MAGIQUE POUR LES OPCO                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EX050PA
       ;;
(EX050PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EX050PAA
       ;;
(EX050PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLES EN MAJ                                                               
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#    RSSU50   : NAME=RSSU50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU50 /dev/null
#    RSSU90   : NAME=RSSU90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU90 /dev/null
#    RSHV15   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# TABLES EN MAJ                                                                
#    RSGB05   : NAME=RSGB05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSSU50   : NAME=RSSU50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU50 /dev/null
#    RSSU90   : NAME=RSSU90,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU90 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX050 
       JUMP_LABEL=EX050PAB
       ;;
(EX050PAB)
       m_CondExec 04,GE,EX050PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSU001                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX050PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EX050PAD
       ;;
(EX050PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLES EN MAJ                                                               
#    RSSU90   : NAME=RSSU90,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSU90 /dev/null
#  FIC EN SORTIE                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -t LSEQ -g +1 FSU001 ${DATA}/PXX0/F07.BSU001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -t LSEQ -g +1 FSU050 ${DATA}/PXX0/F07.BSU001BP
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -t LSEQ -g +1 FSU090 ${DATA}/PXX0/F07.BSU001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -t LSEQ -g +1 FSU010 ${DATA}/PXX0/F07.BSU001DP
#                                                                              
# ***************************************************************              
# POUR INFO //DD:DD1 -n RTSU*                                                  
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP EX050PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BSU001 
       JUMP_LABEL=EX050PAG
       ;;
(EX050PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EX050PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.FTEX050P
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EX050PAE
       ;;
(EX050PAE)
       m_CondExec 04,GE,EX050PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX050PAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EX050PAJ
       ;;
(EX050PAJ)
       m_CondExec ${EXAAP},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F07.BSU001AP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 27998 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.BSU001GP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EX050PAJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTEX50BP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX050PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EX050PAM
       ;;
(EX050PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EX050PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/EX050PAM.FTEX50BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTADO3BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX050PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EX050PAQ
       ;;
(EX050PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EX050PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.EX050PAM.FTEX50BP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EX050PZA
       ;;
(EX050PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EX050PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
