#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EX000P.ksh                       --- VERSION DU 08/10/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEX000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/11/16 AT 17.12.55 BY PREPA2                       
#    STANDARDS: P  JOBSET: EX000P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  CREE LES DIFFERENTS MEMBRES DES SAUVES PAR LA CLIST :                       
#           SYSB.EXPLOIT.CLIS(BTSELSAU)                                        
#                                                                              
#     SI OPTION = MENSUEL  : DEPUIS LE FICHIER DDIN ON FAIT UN CHANGE          
#                            DE   ',UPDATE' PAR ''                             
#                            QUE L ON ECRIT DANS LE FIC DDOUT                  
#     SI OPTION = EXCLUDE  : DEPUIS LE FICHIER DDIN ON FAIT UN CHANGE          
#                            DE   'SELECT'  PAR 'EXCLUDE'                      
#                            QUE L ON ECRIT DANS LE FIC DDOUT                  
#                                                                              
#     L OPTION ETANT PRECISEE DANS LA SYSIN EXEMPLE :%BTSELSAU MENSUEL         
# ********************************************************************         
#  CREATION DU MEMBRE MENSUEL DE  MAIL  : MENBAMAI                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA SAUVE MENSUELLE MAIL                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EX000PA
       ;;
(EX000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  95/11/16 AT 17.12.55 BY PREPA2                   
# *    JOBSET INFORMATION:    NAME...: EX000P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'MBRES DES SAUVES'                      
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAA
       ;;
(EX000PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAMAI
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBAMAI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAB
       ;;
(EX000PAB)
       m_CondExec 04,GE,EX000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  MAIL  : EXCBAMAI                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAD
       ;;
(EX000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAMAI
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBAMAI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAE
       ;;
(EX000PAE)
       m_CondExec 04,GE,EX000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  SAV   : MENBASAV                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA SAUVE MENSUELLE SAV                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAG
       ;;
(EX000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBASAV
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBASAV
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAH
       ;;
(EX000PAH)
       m_CondExec 04,GE,EX000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  SAV   : EXCBASAV                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAJ
       ;;
(EX000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBASAV
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBASAV
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAK
       ;;
(EX000PAK)
       m_CondExec 04,GE,EX000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DACEM : MENBADAC                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA   SAUVE MENSUELLE DACEM                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAM
       ;;
(EX000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADAC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBADAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAN
       ;;
(EX000PAN)
       m_CondExec 04,GE,EX000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DACEM : EXCBADAC                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAQ
       ;;
(EX000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADAC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBADAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAR
       ;;
(EX000PAR)
       m_CondExec 04,GE,EX000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DACEM : MENDBDAC                             
#         BASE DDB2                                                            
#         REPRIS DANS LA   SAUVE MENSUELLE DACEM                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAT
       ;;
(EX000PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAU
       ;;
(EX000PAU)
       m_CondExec 04,GE,EX000PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DACEM : EXCDBDAC                             
#         BASE DDB2                                                            
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PAX PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PAX
       ;;
(EX000PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PAY
       ;;
(EX000PAY)
       m_CondExec 04,GE,EX000PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  KAPRO : MENBAKAP                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA SAUVE MENSUELLE KAPRO                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBA PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBA
       ;;
(EX000PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAKAP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBAKAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBB
       ;;
(EX000PBB)
       m_CondExec 04,GE,EX000PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  KAPRO : EXCBAKAP                             
#         BASE DL1,VSAMS,CICS,                                                 
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBD
       ;;
(EX000PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAKAP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBAKAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBE
       ;;
(EX000PBE)
       m_CondExec 04,GE,EX000PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DAR   : MENBADAR                             
#         BASE DL1,VSAMS,CICS,BMC,QMF                                          
#         REPRIS DANS LA SAUVE MENSUELLE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBG
       ;;
(EX000PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADAR
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBADAR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBH
       ;;
(EX000PBH)
       m_CondExec 04,GE,EX000PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  RDAR  : EXCBADAR                             
#         BASE DL1,VSAMS,CICS,BMC,QMF                                          
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBJ
       ;;
(EX000PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADAR
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBADAR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBK
       ;;
(EX000PBK)
       m_CondExec 04,GE,EX000PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL           : MENDBDAR                             
#         DB2(USER,SYSTEME)       DE COMPTA                                    
#         REPRIS DANS LA SAUVE MENSUELLE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBM
       ;;
(EX000PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAR
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDAR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBN
       ;;
(EX000PBN)
       m_CondExec 04,GE,EX000PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE : EXCDBDAR                                       
#         DB2(USER,SYSTEME)       DE COMPTA                                    
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBQ
       ;;
(EX000PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAR
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDAR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBR
       ;;
(EX000PBR)
       m_CondExec 04,GE,EX000PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  NCP   : MENBANCP                             
#         BASE DL1,VSAMS,CICS,BSDS,LOGDB2,TABLE DB2 (BMC ET QMF)               
#         REPRIS DANS LA SAUVE MENSUELLE NCP                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBT
       ;;
(EX000PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBANCP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBANCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBU
       ;;
(EX000PBU)
       m_CondExec 04,GE,EX000PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  NCP   : EXCBANCP                             
#         BASE DL1,VSAMS,CICS,BSDS,LOGDB2,TABLE DB2 (BMC ET QMF)               
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PBX PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PBX
       ;;
(EX000PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBANCP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBANCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PBY
       ;;
(EX000PBY)
       m_CondExec 04,GE,EX000PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  NCP   : MENDBNCP                             
#         TABLE DB2 (SYSTEME ET USER)                                          
#         REPRIS DANS LA SAUVE MENSUELLE NCP                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCA PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCA
       ;;
(EX000PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBNCP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBNCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCB
       ;;
(EX000PCB)
       m_CondExec 04,GE,EX000PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  NCP   : EXCDBNCP                             
#         TABLE DB2 (SYSTEME ET USER )                                         
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCD
       ;;
(EX000PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBNCP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBNCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCE
       ;;
(EX000PCE)
       m_CondExec 04,GE,EX000PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL           : MENBAFIL                             
#         VSAMS,CICS,BSDS,LOGDB2,TABLES DB2(QMF,BMC)  DES FILIALIES            
#         REPRIS DANS LA SAUVE MENSUELLE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCG
       ;;
(EX000PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAFIL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBAFIL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCH
       ;;
(EX000PCH)
       m_CondExec 04,GE,EX000PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE : EXCBAFIL                                       
#         VSAMS,CICS,BSDS,LOGDB2,TABLES DB2(QMF,BMC) DES FILIALIES             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCJ
       ;;
(EX000PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAFIL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBAFIL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCK
       ;;
(EX000PCK)
       m_CondExec 04,GE,EX000PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL           : MENDBFIL                             
#         DB2(SYSTEME)       DE FILIALE                                        
#         REPRIS DANS LA SAUVE MENSUELLE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCM
       ;;
(EX000PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBFIL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBFIL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCN
       ;;
(EX000PCN)
       m_CondExec 04,GE,EX000PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE : EXCDBFIL                                       
#         DB2(SYSTEME)       DE FILIALE                                        
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCQ
       ;;
(EX000PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBFIL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBFIL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCR
       ;;
(EX000PCR)
       m_CondExec 04,GE,EX000PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL           : MENBADPM                             
#         VSAMS,                  DE MARSEILLE                                 
#         REPRIS DANS LA SAUVE MENSUELLE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCT
       ;;
(EX000PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADPM
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBADPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCU
       ;;
(EX000PCU)
       m_CondExec 04,GE,EX000PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE : EXCBADPM                                       
#         VSAMS,                  DE MARSEILLE                                 
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PCX PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PCX
       ;;
(EX000PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADPM
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBADPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PCY
       ;;
(EX000PCY)
       m_CondExec 04,GE,EX000PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DPM   : MENDBDPM                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DPM                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDA PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDA
       ;;
(EX000PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDPM
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDB
       ;;
(EX000PDB)
       m_CondExec 04,GE,EX000PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DPM   : EXCDBDPM                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDD
       ;;
(EX000PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDPM
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDE
       ;;
(EX000PDE)
       m_CondExec 04,GE,EX000PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DN    : MENDBDN                              
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DN                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDG
       ;;
(EX000PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDN
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDH
       ;;
(EX000PDH)
       m_CondExec 04,GE,EX000PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DN    : EXCDBDN                              
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDJ
       ;;
(EX000PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDN
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDK
       ;;
(EX000PDK)
       m_CondExec 04,GE,EX000PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DRA   : MENDBDRA                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DRA                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDM
       ;;
(EX000PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDRA
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDN
       ;;
(EX000PDN)
       m_CondExec 04,GE,EX000PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DRA   : EXCDBDRA                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDQ
       ;;
(EX000PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDRA
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDR
       ;;
(EX000PDR)
       m_CondExec 04,GE,EX000PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DAL   : MENDBDAL                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DAL                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDT
       ;;
(EX000PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDU
       ;;
(EX000PDU)
       m_CondExec 04,GE,EX000PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DAL   : EXCDBDAL                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PDX PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PDX
       ;;
(EX000PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDAL
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PDY
       ;;
(EX000PDY)
       m_CondExec 04,GE,EX000PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DPC   : MENDBDPC                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DPC                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEA PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEA
       ;;
(EX000PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDPC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENDBDPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEB
       ;;
(EX000PEB)
       m_CondExec 04,GE,EX000PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DPC   : EXCDBDPC                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PED PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PED
       ;;
(EX000PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELDBDPC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCDBDPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEE
       ;;
(EX000PEE)
       m_CondExec 04,GE,EX000PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  DPC   : MENBADPC                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE DPC                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEG
       ;;
(EX000PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADPC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBADPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEH
       ;;
(EX000PEH)
       m_CondExec 04,GE,EX000PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  DPC   : EXCBADPC                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEJ
       ;;
(EX000PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBADPC
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBADPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEK
       ;;
(EX000PEK)
       m_CondExec 04,GE,EX000PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  EXP   : MENBAEXP                             
#         BASE DB2                                                             
#         REPRIS DANS LA SAUVE MENSUELLE EXP                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEM
       ;;
(EX000PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAEXP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBAEXP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEN
       ;;
(EX000PEN)
       m_CondExec 04,GE,EX000PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  EXP   : EXCBAEXP                             
#         BASE DB2                                                             
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEQ
       ;;
(EX000PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBAEXP
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBAEXP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PER
       ;;
(EX000PER)
       m_CondExec 04,GE,EX000PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE MENSUEL DE  NCD   : MENBANCD                             
#         BASE DL1,VSAMS,CICS,BSDS,LOGDB2,TABLE DB2 (BMC ET QMF)               
#         REPRIS DANS LA SAUVE MENSUELLE NCD                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PET PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PET
       ;;
(EX000PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBANCD
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/MENBANCD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEU
       ;;
(EX000PEU)
       m_CondExec 04,GE,EX000PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU MEMBRE EXCLUDE DE  NCD   : EXCBANCD                             
#         BASE DL1,VSAMS,CICS,BSDS,LOGDB2,TABLE DB2 (BMC ET QMF)               
#         REPRIS DANS LA  SAUVE DE TYPE AUTO                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EX000PEX PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=EX000PEX
       ;;
(EX000PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR ISPPROF ${DATA}/BATCH.ISPF.ISPPROF
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYSB.EXPLOIT.CLIS
       m_FileAssign -d SHR DDIN ${DATA}/CORTEX4.P.MTXTFIX1/SELBANCD
# -M-MTXTFIX5 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR DDOUT ${DATA}/CORTEX4.P.MTXTFIX5/EXCBANCD
# ****                                                                         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTSELSAU 
       JUMP_LABEL=EX000PEY
       ;;
(EX000PEY)
       m_CondExec 04,GE,EX000PEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
