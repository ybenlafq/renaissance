#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ034D.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDMQ034 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/10/10 AT 16.22.38 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MQ034D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE QUEUE HI    DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MQ034DA
       ;;
(MQ034DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MQ034DAA
       ;;
(MQ034DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D1
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D2
#                                                                              
# ***** PARAMETRES                                                             
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D3
#                                                                              
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D4
#                                                                              
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D5
#                                                                              
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D6
#                                                                              
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D7
#                                                                              
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D8
#                                                                              
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D9
#                                                                              
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D10
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D11
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D12
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQOUT ${DATA}/PXX0/F91.FMQ035AD
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQREC ${DATA}/PXX0/F91.FMQ035BD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ035 
       JUMP_LABEL=MQ034DAB
       ;;
(MQ034DAB)
       m_CondExec 04,GE,MQ034DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035AP                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ034DAD
       ;;
(MQ034DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D13
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FMQIN ${DATA}/PXX0/F91.FMQ035AD
       m_OutputAssign -c 9 -w IAN010D IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034DAE
       ;;
(MQ034DAE)
       m_CondExec 04,GE,MQ034DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035BD                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MQ034DAG
       ;;
(MQ034DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D14
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FMQIN ${DATA}/PXX0/F91.FMQ035BD
       m_OutputAssign -c 9 -w IAN010D IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034DAH
       ;;
(MQ034DAH)
       m_CondExec 04,GE,MQ034DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 99                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034DAJ PGM=BMQ033     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MQ034DAJ
       ;;
(MQ034DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D15
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D16
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D17
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D18
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A3} FMQIN ${DATA}/PXX0/F91.FMQ035AD
       m_ProgramExec BMQ033 
#                                                                              
# ********************************************************************         
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 02                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034DAM PGM=BMQ033     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MQ034DAM
       ;;
(MQ034DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D19
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D20
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D21
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034D22
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A4} FMQIN ${DATA}/PXX0/F91.FMQ035BD
       m_ProgramExec BMQ033 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
