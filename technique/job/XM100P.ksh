#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  XM100P.ksh                       --- VERSION DU 20/10/2016 12:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPXM100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/20 AT 10.56.36 BY BURTEC2                      
#    STANDARDS: P  JOBSET: XM100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BXM100  : DETERMINATION CODICS/PRESTAS A TRAITER                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=XM100PA
       ;;
(XM100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=XM100PAA
       ;;
(XM100PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
#    RSGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN68 /dev/null
#    RSGN75   : NAME=RSGN75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN75 /dev/null
#    RSPR10   : NAME=RSPR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR10 /dev/null
#    RSPR11   : NAME=RSPR11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR11 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FXM100 ${DATA}/PTEM/XM100PAA.FXM100AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BXM100 
       JUMP_LABEL=XM100PAB
       ;;
(XM100PAB)
       m_CondExec 04,GE,XM100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU STEP PRECEDENT                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAD
       ;;
(XM100PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/XM100PAA.FXM100AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FXM100HP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/XM100PAD.FXM100BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_50 01 CH 50
 /KEYS
   FLD_CH_1_50 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=XM100PAE
       ;;
(XM100PAE)
       m_CondExec 00,EQ,XM100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BXM101  : ENVOI DES DONN�ES _A HYBRIS (Q1907000PMCPRD)                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAG
       ;;
(XM100PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **                                                                           
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RSXM20   : NAME=RSXM20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSXM20 /dev/null
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A2} FXM101 ${DATA}/PTEM/XM100PAD.FXM100BP
#                                                                              
# **************************************                                       
# ** UNLOAD DES DONN�ES ADMIN                                                  
# **************************************                                       
#                                                                              
# ***********************************                                          
# *   STEP XM100PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          

       m_ProgramExec -b BXM101 
       JUMP_LABEL=XM100PAJ
       ;;
(XM100PAJ)
       m_CondExec ${EXAAP},NE,YES 
# **************************************                                       
#                                                                              
#    RSBS30   : NAME=RSBS30P,MODE=I - DYNAM=YES                                
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/XM100PAJ.FXM100CP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/XM100PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=XM100PAH
       ;;
(XM100PAH)
       m_CondExec 04,GE,XM100PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAM
       ;;
(XM100PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/XM100PAJ.FXM100CP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/XM100PAM.FXM100DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=XM100PAN
       ;;
(XM100PAN)
       m_CondExec 00,EQ,XM100PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BXM102 : COMPAR + ENVOI DES DONNEES VERS HYBRIS (Q1907000PMCADM)            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAQ
       ;;
(XM100PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE EN LECTURE/ECRITURE                                             
#    RSXM20   : NAME=RSXM20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSXM20 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREES                                                    
       m_FileAssign -d SHR -g +0 FXM102J1 ${DATA}/PXX0/F07.FXM100EP
       m_FileAssign -d SHR -g ${G_A4} FXM102J ${DATA}/PTEM/XM100PAM.FXM100DP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FXM102 ${DATA}/PTEM/XM100PAQ.FXM102AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BXM102 
       JUMP_LABEL=XM100PAR
       ;;
(XM100PAR)
       m_CondExec 04,GE,XM100PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAT
       ;;
(XM100PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/XM100PAQ.FXM102AP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BXM102AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_50 1 CH 50
 /KEYS
   FLD_CH_1_50 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=XM100PAU
       ;;
(XM100PAU)
       m_CondExec 00,EQ,XM100PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BXM101  : ENVOI DES DONN�ES _A HYBRIS (Q1907000PMCPRD)                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=XM100PAX
       ;;
(XM100PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **                                                                           
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RSXM20   : NAME=RSXM20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSXM20 /dev/null
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A6} FXM101 ${DATA}/PXX0/F07.BXM102AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BXM101 
       JUMP_LABEL=XM100PAY
       ;;
(XM100PAY)
       m_CondExec 04,GE,XM100PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BXM120 : PURGE DE LA RTXM20                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBA
       ;;
(XM100PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE EN LECTURE/ECRITURE                                             
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ****** TABLE EN LECTURE/ECRITURE                                             
#    RSXM20   : NAME=RSXM20,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSXM20 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BXM120 
       JUMP_LABEL=XM100PBB
       ;;
(XM100PBB)
       m_CondExec 04,GE,XM100PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER BTF001JP POUR COMPARAISON DU LENDEMAIN                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PBD PGM=IEBGENER   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBD
       ;;
(XM100PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PTEM/XM100PAM.FXM100DP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SYSUT2 ${DATA}/PXX0/F07.FXM100EP
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=XM100PBE
       ;;
(XM100PBE)
       m_CondExec 00,EQ,XM100PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ** UNLOAD                                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBG
       ;;
(XM100PBG)
       m_CondExec ${EXABY},NE,YES 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC00 ${DATA}/PTEM/XM100PBG.FGA01UNP
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/XM100PBG.sysin
       m_DBHpuUnload -q SYSIN -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=XM100PBH
       ;;
(XM100PBH)
       m_CondExec 04,GE,XM100PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER EDITION                                                        
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBJ
       ;;
(XM100PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/XM100PBG.FGA01UNP
       m_FileAssign -d NEW,CATLG,DELETE -r 5000 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGA01ULP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/PADBYTE x"20"                     
/COPY
/RECORDLAYOUT all_content {
     row CHARACTER 10000
}
/FIELDS FD_01 1  10000  CH
/DERIVEDFIELD fdExtract    all_content.row       EXTRACT /<.*?\0/   CH
/DERIVEDFIELD build_FD_O1  fdExtract   5000
/REFORMAT     build_FD_O1
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /COPY
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=XM100PBK
       ;;
(XM100PBK)
       m_CondExec 00,EQ,XM100PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU ZG100PZ                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP XM100PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBM
       ;;
(XM100PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
# SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                              
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/XM100PBM.sysin
       m_UtilityExec INPUT
# **************************************************************               
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# **************************************************************               
#                                                                              
# ***********************************                                          
# *   STEP XM100PBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=XM100PBQ
       ;;
(XM100PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FXM100HP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/XM100PBQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=XM100PBR
       ;;
(XM100PBR)
       m_CondExec 16,NE,XM100PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=XM100PZA
       ;;
(XM100PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/XM100PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
