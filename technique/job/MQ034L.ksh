#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ034L.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLMQ034 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/10/10 AT 22.13.06 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MQ034L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMQ035  PARIS                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MQ034LA
       ;;
(MQ034LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MQ034LAA
       ;;
(MQ034LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L1
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L2
#                                                                              
# ***** PARAMETRES                                                             
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L3
#                                                                              
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L4
#                                                                              
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L5
#                                                                              
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L6
#                                                                              
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L7
#                                                                              
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L8
#                                                                              
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L9
#                                                                              
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L10
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L11
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L12
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQOUT ${DATA}/PXX0/F61.FMQ035AL
#                                                                              
# *****  FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQREC ${DATA}/PXX0/F61.FMQ035BL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ035 
       JUMP_LABEL=MQ034LAB
       ;;
(MQ034LAB)
       m_CondExec 04,GE,MQ034LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035AP                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ034LAD
       ;;
(MQ034LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L13
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FMQIN ${DATA}/PXX0/F61.FMQ035AL
       m_OutputAssign -c 9 -w IAN010L IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034LAE
       ;;
(MQ034LAE)
       m_CondExec 04,GE,MQ034LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BAN010  .POUR LE FICHIER FMQ035BL                                     
#  EDITION DE L'ETAT IAN010                                                    
#  REPRISE AU STEP PRéCéDENT                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MQ034LAG
       ;;
(MQ034LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L14
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FMQIN ${DATA}/PXX0/F61.FMQ035BL
       m_OutputAssign -c 9 -w IAN010L IAN010
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAN010 
       JUMP_LABEL=MQ034LAH
       ;;
(MQ034LAH)
       m_CondExec 04,GE,MQ034LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 99                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034LAJ PGM=BMQ033     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MQ034LAJ
       ;;
(MQ034LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L15
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L16
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L17
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L18
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A3} FMQIN ${DATA}/PXX0/F61.FMQ035AL
       m_ProgramExec BMQ033 
#                                                                              
# ********************************************************************         
#  BMQ033 : RECOPIE DU FICHIER DANS LA QUEUE 02                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034LAM PGM=BMQ033     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MQ034LAM
       ;;
(MQ034LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***** QUEUES                                                                 
# ***** QUEUE MANAGER                                                          
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L19
# ***** QUEUE A INSPECTER                                                      
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L20
#                                                                              
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L21
#                                                                              
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034L22
#                                                                              
# *****  FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A4} FMQIN ${DATA}/PXX0/F61.FMQ035BL
       m_ProgramExec BMQ033 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
