#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ000P.ksh                       --- VERSION DU 08/10/2016 12:44
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMQ000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/09/24 AT 09.46.49 BY BURTECA                      
#    STANDARDS: P  JOBSET: MQ000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : CSQUTIL                                                              
#   ------------                                                               
#   SAUVEGARDE DES PARAMETRES DES QUEUES MANAGER POUR MQ1                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MQ000PA
       ;;
(MQ000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2012/09/24 AT 09.46.49 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: MQ000P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'SAVE QUEUES MANAGER'                   
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MQ000PAA
       ;;
(MQ000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUTPUT1 ${DATA}/PXX0/SAUVE.MQ1
       m_FileAssign -d SHR CMDINP ${DATA}/CORTEX4.P.MTXTFIX1/MQ000P1
       m_BatchRunmqsc -q MQ1 -f CMDINP -o OUTPUT1
# ********************************************************************         
#   PGM : CSQUTIL                                                              
#   ------------                                                               
#   SAUVEGARDE DES PARAMETRES DES QUEUES MANAGER POUR MWMP                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ000PAD PGM=CSQUTIL    ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ000PAD
       ;;
(MQ000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUTPUT1 ${DATA}/PXX0/SAUVE.MWMP
       m_FileAssign -d SHR CMDINP ${DATA}/CORTEX4.P.MTXTFIX1/MQ000P2
       m_BatchRunmqsc -q MWMP -f CMDINP -o OUTPUT1
# ********************************************************************         
#   PGM : CSQUTIL                                                              
#   ------------                                                               
#   SAUVEGARDE DES PARAMETRES DES QUEUES MANAGER POUR MDPR                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=CSQUTIL,PARM='MDPR'                                       
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT1  FILE  NAME=MQ000P3,MODE=O                                           
# SYSIN    DATA  *,CLASS=FIX1                                                  
#  COMMAND DDNAME(CMDINP) MAKEDEF(OUTPUT1)                                     
#          DATAEND                                                             
# CMDINP   DATA  *,CLASS=FIX1,MBR=MQ000P3                                      
#  DISPLAY  STGCLASS(*)                                                        
#  DISPLAY  QUEUE(*)  ALL                                                      
#  DISPLAY  NAMELIST(*)  ALL                                                   
#  DISPLAY  PROCESS(*)  ALL                                                    
#  DISPLAY  CHANNEL(*)  ALL                                                    
#          DATAEND                                                             
# ********************************************************************         
#   PGM : CSQUTIL                                                              
#   ------------                                                               
#   SAUVEGARDE DES PARAMETRES DES QUEUES MANAGER POUR MA0F                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=CSQUTIL,PARM='MA0F'                                       
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT1  FILE  NAME=MQ000P4,MODE=O                                           
# SYSIN    DATA  *,CLASS=FIX1                                                  
#  COMMAND DDNAME(CMDINP) MAKEDEF(OUTPUT1)                                     
#          DATAEND                                                             
# CMDINP   DATA  *,CLASS=FIX1,MBR=MQ000P4                                      
#  DISPLAY  STGCLASS(*)                                                        
#  DISPLAY  QUEUE(*)  ALL                                                      
#  DISPLAY  NAMELIST(*)  ALL                                                   
#  DISPLAY  PROCESS(*)  ALL                                                    
#  DISPLAY  CHANNEL(*)  ALL                                                    
#          DATAEND                                                             
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
