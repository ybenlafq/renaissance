#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ034F.ksh                       --- VERSION DU 08/10/2016 23:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFMQ034 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/04/26 AT 14.48.03 BY BURTECA                      
#    STANDARDS: P  JOBSET: MQ034F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MQ034FA
       ;;
(MQ034FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2011/04/26 AT 14.48.03 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: MQ034F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'COPIE QUEUE 0015'                      
# *                           APPL...: REPGROUP                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MQ034FAA
       ;;
(MQ034FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F1
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F2
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F3
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F4
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F5
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F6
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F7
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F8
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F9
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F10
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F11
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F12
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F13
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F14
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAD PGM=HWAIT      ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAD
       ;;
(MQ034FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAG PGM=BMQ034     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAG
       ;;
(MQ034FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F15
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F16
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F17
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F18
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F19
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F20
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F21
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F22
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F23
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F24
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F25
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F26
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F27
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F28
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAJ PGM=HWAIT      ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAJ
       ;;
(MQ034FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAM PGM=BMQ034     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAM
       ;;
(MQ034FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F29
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F30
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F31
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F32
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F33
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F34
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F35
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F36
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F37
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F38
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F39
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F40
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F41
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F42
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAQ PGM=HWAIT      ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAQ
       ;;
(MQ034FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAT PGM=BMQ034     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAT
       ;;
(MQ034FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F43
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F44
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F45
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F46
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F47
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F48
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F49
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F50
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F51
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F52
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F53
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F54
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F55
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F56
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FAX PGM=HWAIT      ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FAX
       ;;
(MQ034FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBA PGM=BMQ034     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBA
       ;;
(MQ034FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F57
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F58
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F59
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F60
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F61
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F62
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F63
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F64
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F65
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F66
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F67
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F68
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F69
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F70
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBD PGM=HWAIT      ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBD
       ;;
(MQ034FBD)
       m_CondExec ${EXABT},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBG PGM=BMQ034     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBG
       ;;
(MQ034FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F71
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F72
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F73
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F74
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F75
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F76
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F77
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F78
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F79
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F80
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F81
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F82
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F83
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F84
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBJ PGM=HWAIT      ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBJ
       ;;
(MQ034FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBM PGM=BMQ034     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBM
       ;;
(MQ034FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F85
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F86
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F87
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F88
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F89
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F90
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F91
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F92
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F93
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F94
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F95
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F96
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F97
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F98
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBQ PGM=HWAIT      ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBQ
       ;;
(MQ034FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBT PGM=BMQ034     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBT
       ;;
(MQ034FBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F99
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA1
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA2
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA3
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA4
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA5
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA6
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA7
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA8
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FA9
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB1
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB2
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB3
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB4
       m_ProgramExec BMQ034 
# ********************************************************************         
#  WAIT POUR EVITER BLOQUAGE AVEC CONCERTIGO                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FBX PGM=HWAIT      ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FBX
       ;;
(MQ034FBX)
       m_CondExec ${EXACX},NE,YES 
       m_ProgramExec HWAIT "60"
# ********************************************************************         
#   PGM : BMQ034                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MQ034FCA PGM=BMQ034     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=MQ034FCA
       ;;
(MQ034FCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034F99
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB5
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB6
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB7
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB8
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FB9
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC1
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC2
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC3
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC4
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC5
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC6
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC7
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQ034FC8
       m_ProgramExec BMQ034 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
