#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQ037P.ksh                       --- VERSION DU 08/10/2016 13:53
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMQ037 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/01/15 AT 11.50.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: MQ037P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQ037                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MQ037PA
       ;;
(MQ037PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2008/01/15 AT 11.50.42 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: MQ037P                                  
# *                           FREQ...: 5W                                      
# *                           TITLE..: 'PUR Q1996000DC0005'                    
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MQ037PAA
       ;;
(MQ037PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P1
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P2
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P3
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P4
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P5
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
# ========================================                                     
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P6
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P7
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P8
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P9
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P10
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P11
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P12
       m_FileAssign -d SHR FCLE ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P13
       m_FileAssign -d SHR FCOPCO ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P14
       m_FileAssign -d SHR FIDSI ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P15
       m_FileAssign -d SHR FNBENR ${DATA}/CORTEX4.P.MTXTFIX1/MQ037P16
# *****   FICHIER DE SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQOUT ${DATA}/PXX0/F07.BMQ037AP
       m_ProgramExec BMQ037 
#                                                                              
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
