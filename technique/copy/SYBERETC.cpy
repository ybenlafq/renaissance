      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      * TEST-CODE-RETOUR DL/I                                           00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       TEST-CODE-RETOUR               SECTION.                          00000500
      *                                                                 00000600
           MOVE 0        TO CODE-RETOUR.                                00000700
           MOVE 0        TO CODE-LEVEL.                                 00000800
           MOVE DIBSTAT  TO PCB-STATUS-CODE.                            00000900
           MOVE DIBSEGM  TO SEG-NAME-FB.                                00001000
           MOVE DIBSEGLV TO SEG-LEVEL.                                  00001100
      *    MOVE DIBKFBL  TO LENGTH-FB-KEY.                              00001200
      *                                                                 00001300
           IF GU                                                        00001400
              IF GE                                                     00001500
                 MOVE 1 TO CODE-RETOUR                                  00001600
               ELSE                                                     00001700
                 IF NOT BB                                              00001800
                    GO TO ABANDON-DL1.                                  00001900
      *                                                                 00002000
           IF GN                                                        00002100
              IF GE                                                     00002200
                 MOVE 1 TO CODE-RETOUR                                  00002300
               ELSE                                                     00002400
                 IF GA                                                  00002500
                    MOVE 1 TO CODE-LEVEL                                00002600
                 ELSE                                                   00002700
                    IF GK                                               00002800
                       MOVE 2 TO CODE-LEVEL                             00002900
                    ELSE                                                00003000
                       IF GB                                            00003100
                          MOVE 3 TO CODE-RETOUR                         00003200
                       ELSE                                             00003300
                          IF NOT BB                                     00003400
                             GO TO ABANDON-DL1.                         00003500
      *                                                                 00003600
           IF GNP                                                       00003700
              IF GE                                                     00003800
                 MOVE 1 TO CODE-RETOUR                                  00003900
               ELSE                                                     00004000
                 IF GA                                                  00004100
                    MOVE 1 TO CODE-LEVEL                                00004200
                 ELSE                                                   00004300
                    IF GK                                               00004400
                       MOVE 2 TO CODE-LEVEL                             00004500
                    ELSE                                                00004600
                       IF NOT BB                                        00004700
                          GO TO ABANDON-DL1.                            00004800
      *                                                                 00004900
           IF ISRT                                                      00005000
              IF II OR NI                                               00005100
                 MOVE 2 TO CODE-RETOUR                                  00005200
              ELSE                                                      00005300
                 IF NOT BB                                              00005400
                    GO TO ABANDON-DL1.                                  00005500
      *                                                                 00005600
           IF REPL                                                      00005700
              IF NOT BB                                                 00005800
                 GO TO ABANDON-DL1.                                     00005900
      *                                                                 00006000
           IF DLET                                                      00006100
              IF NOT BB                                                 00006200
                 GO TO ABANDON-DL1.                                     00006300
      *                                                                 00006400
       FIN-TEST-CODE-RETOUR.                                            00006500
           EXIT.                                                        00006600
                EJECT                                                   00006700
      *                                                                 00006800
      ***************************************************************** 00006900
      *                                                                 00007000
      ***************************************************************** 00008000
      *                                                                 00009000
       SCHEDULING-EXEC               SECTION.                           00010001
      *                                                                 00020000
      *                                                                 00600000
       FIN-SCHEDULING.                                                  00610001
           EXIT.                                                        00620000
                EJECT                                                   00630000
                                                                                
