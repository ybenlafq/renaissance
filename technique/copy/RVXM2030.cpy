      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2030                            *        
      ******************************************************************        
       01  RVXM2030.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2030-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2030-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2030-NCODICK              PIC X(7).                             
      *       49 XM2030-NCODICK-LEN       PIC S9(4) USAGE COMP.                 
      *       49 XM2030-NCODICK-TEXT      PIC X(7).                             
           10 XM2030-CPREST               PIC X(05).                            
      *       49 XM2030-CPREST-LEN        PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CPREST-TEXT       PIC X(5).                             
           10 XM2030-LPRESTATION          PIC X(20).                            
      *       49 XM2030-LPRESTATION-LEN   PIC S9(4) USAGE COMP.                 
      *       49 XM2030-LPRESTATION-TEXT  PIC X(20).                            
           10 XM2030-CMARQ                PIC X(05).                            
      *       49 XM2030-CMARQ-LEN         PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CMARQ-TEXT        PIC X(5).                             
PSE        10 XM2030-REFMNF               PIC X(15).                            
 |    *       49 XM2030-REFMNF-LEN        PIC S9(4) USAGE COMP.                 
PSE   *       49 XM2030-REFMNF-TEXT       PIC X(15).                            
           10 XM2030-CASSORT              PIC X(05).                            
      *       49 XM2030-CASSORT-LEN       PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CASSORT-TEXT      PIC X(5).                             
           10 XM2030-CTAUXTVA             PIC X(05).                            
      *       49 XM2030-CTAUXTVA-LEN      PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CTAUXTVA-TEXT     PIC X(5).                             
           10 XM2030-CNATPREST            PIC X(02).                            
      *       49 XM2030-CNATPREST-LEN     PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CNATPREST-TEXT    PIC X(2).                             
           10 XM2030-CFAM                 PIC X(05).                            
      *       49 XM2030-CFAM-LEN          PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CFAM-TEXT         PIC X(5).                             
           10 XM2030-CHEFPROD             PIC X(05).                            
      *       49 XM2030-CHEFPROD-LEN      PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CHEFPROD-TEXT     PIC X(5).                             
           10 XM2030-DCREATION            PIC X(08).                            
      *       49 XM2030-DCREATION-LEN     PIC S9(4) USAGE COMP.                 
      *       49 XM2030-DCREATION-TEXT    PIC X(08).                            
           10 XM2030-DMAJ                 PIC X(08).                            
      *       49 XM2030-DMAJ-LEN          PIC S9(4) USAGE COMP.                 
      *       49 XM2030-DMAJ-TEXT         PIC X(08).                            
           10 XM2030-CTYPPREST            PIC X(05).                            
      *       49 XM2030-CTYPPREST-LEN     PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CTYPPREST-TEXT    PIC X(5).                             
           10 XM2030-CTYP                 PIC X(03).                            
      *       49 XM2030-CTYP-LEN          PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CTYP-TEXT         PIC X(3).                             
           10 XM2030-MTPREST              PIC S9(5)V9(2)  COMP-3.               
           10 XM2030-MTPRIME              PIC S9(5)V9(2)  COMP-3.               
           10 XM2030-LSTATUT              PIC X(05).                            
      *       49 XM2030-LSTATUT-LEN       PIC S9(4) USAGE COMP.                 
      *       49 XM2030-LSTATUT-TEXT      PIC X(5).                             
           10 XM2030-DEFFET               PIC X(08).                            
      *       49 XM2030-DEFFET-LEN        PIC S9(4) USAGE COMP.                 
      *       49 XM2030-DEFFET-TEXT       PIC X(08).                            
           10 XM2030-MTPRMP               PIC S9(5)V9(2)  COMP-3.               
           10 XM2030-CSIGNE               PIC X(01).                            
      *       49 XM2030-CSIGNE-LEN        PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CSIGNE-TEXT       PIC X(1).                             
      *    10 XM2030-CTYPINO.                                                   
      *       49 XM2030-CTYPINO-LEN       PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CTYPINO-TEXT      PIC X(1).                             
           10 XM2030-DNAISS               PIC X(01).                            
      *       49 XM2030-DNAISS-LEN        PIC S9(4) USAGE COMP.                 
      *       49 XM2030-DNAISS-TEXT       PIC X(01).                            
           10 XM2030-CLIREQ               PIC X(01).                            
      *       49 XM2030-CLIREQ-LEN        PIC S9(4) USAGE COMP.                 
      *       49 XM2030-CLIREQ-TEXT       PIC X(01).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2030                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2030-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-NCODICK-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-NCODICK-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CPREST-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CPREST-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-LPRESTATION-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-LPRESTATION-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CMARQ-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CMARQ-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
PSE   *    02 XM2030-REFMNF-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-REFMNF-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CASSORT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CASSORT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CTAUXTVA-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CTAUXTVA-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CNATPREST-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CNATPREST-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CHEFPROD-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CHEFPROD-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-DCREATION-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-DCREATION-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-DMAJ-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-DMAJ-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CTYPPREST-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CTYPPREST-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CTYP-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CTYP-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CDOSSIER-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CDOSSIER-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-MTPREST-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-MTPREST-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-MTPRIME-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-MTPRIME-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-LSTATUT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-LSTATUT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-DEFFET-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-DEFFET-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-MTPRMP-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-MTPRMP-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CSIGNE-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CSIGNE-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-DNAISS-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-DNAISS-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2030-CLIREQ-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2030-CLIREQ-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 21      *        
      ******************************************************************        
                                                                                
