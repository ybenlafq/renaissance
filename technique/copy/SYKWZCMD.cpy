      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        ZONES DE TRAVAIL DE LA ZONE DE COMMANDE                *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       01  FILLER               PIC X(16) VALUE ' Z-DISPATCH ===>'.             
       01  Z-DISPATCH.                                                          
      *                                                                         
           02  DISPATCH-LEVEL                PIC 9(01).                         
      *                                                                         
           02  DISPATCH-INPUT                PIC X(15).                         
           02  FILLER REDEFINES DISPATCH-INPUT.                                 
               04 OCTET-DISPATCH-INPUT       PIC X(01) OCCURS 15.               
      *                                                                         
           02  DISPATCH-OUTPUT               PIC X(12).                         
           02  FILLER REDEFINES DISPATCH-OUTPUT.                                
               04 DISPATCH-SELECTION                   OCCURS 3.                
                  08 OCTET-DISPATCH-OUTPUT   PIC X(01) OCCURS 4.                
      *                                                                         
           02  DISPATCH-PGMSUI               PIC X(08).                         
      *                                                                         
       EJECT                                                                    
                                                                                
                                                                                
