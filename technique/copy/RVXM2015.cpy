      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2015                  *        
      ******************************************************************        
       01  RVXM2015.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2015-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2015-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2015-NCODIC          PIC X(07).                                 
           10 XM2015-NSOC            PIC X(03).                                 
           10 XM2015-NENTCDE         PIC X(05).                                 
           10 XM2015-WENTCDE         PIC X(01).                                 
           10 XM2015-QDELAIAPPRO     PIC X(03).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2015-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2015-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2015-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-NSOC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2015-NSOC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-NENTCDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2015-NENTCDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-WENTCDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2015-WENTCDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2015-QDELAIAPPRO-F   PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2015-QDELAIAPPRO-F   PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
