      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2063                            *        
      ******************************************************************        
       01  RVXM2063.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2063-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2063-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2063-NLIST.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2063-NLIST-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2063-NLIST-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2063-NLIST-TEXT        PIC X(07).                            
           10 XM2063-LLIST.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2063-LLIST-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2063-LLIST-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2063-LLIST-TEXT        PIC X(20).                            
           10 XM2063-CTYPENT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2063-CTYPENT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2063-CTYPENT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2063-CTYPENT-TEXT      PIC X(02).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2063                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2063-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2063-NLIST-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2063-NLIST-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2063-LLIST-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2063-LLIST-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2063-CTYPENT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2063-CTYPENT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 04      *        
      ******************************************************************        
                                                                                
