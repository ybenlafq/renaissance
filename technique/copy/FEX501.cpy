      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DU FLUX JOURNALIER STOCK (PGM=BEX501)        *00002209
      *                 NCG VERS OPCO                                  *00002309
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      * 04/03/2014 * DSA004 * Y. SCOPIN       * YS0434                          
      * REMETTRE LA ZONE QCDE                                                   
      *-----------------------------------------------------------------        
      * 14/01/2014 * DSA004 * Y. SCOPIN       * YS1414                          
      * SUPPRESSION DE LA ZONE QCDE                                             
      ******************************************************************        
       01  WS-FEX501-ENR.                                                       
           10  FEX501-NCODIC              PIC X(07)    VALUE SPACE.             
           10  FEX501-QSTOCK              PIC 9(07)    VALUE ZERO.              
           10  FEX501-DCDE                PIC X(08)    VALUE SPACE.             
YS1414*    10  FEX501-QCDE                PIC 9(07)    VALUE ZERO.              
YS0434     10  FEX501-QCDE                PIC 9(07)    VALUE ZERO.              
           10  FEX501-NCODICR             PIC X(07)    VALUE SPACE.             
                                                                                
