      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************** 16-05-90 *         
      * TEST-CODE-RETOUR SQL                                          *         
      ****************************************************** SYKSRETC *         
       TEST-CODE-RETOUR-SQL           SECTION.                                  
           MOVE '0'                TO  CODE-RETOUR.                             
           MOVE SQLCODE            TO  SQL-CODE.                                
           MOVE SQLCODE            TO  TRACE-SQL-CODE.                          
           MOVE TRACE-SQL-MESSAGE  TO  MESS.                                    
           IF CODE-SELECT                                                       
              IF SQL-CODE = +100                                                
                 MOVE '1' TO CODE-RETOUR                                        
               ELSE                                                             
                 IF SQL-CODE < 0                                                
                    GO TO CODE-RETOUR-SQL-ABANDON                               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF CODE-INSERT                                                       
              IF SQL-CODE < 0                                                   
                 IF SQL-CODE = -803                                             
                    THEN MOVE '2' TO CODE-RETOUR                                
                    ELSE GO TO CODE-RETOUR-SQL-ABANDON                          
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF CODE-UPDATE                                                       
              IF SQL-CODE = +100                                                
                 MOVE '1' TO CODE-RETOUR                                        
               ELSE                                                             
                 IF SQL-CODE < 0                                                
                    IF SQL-CODE = -803                                          
                       THEN MOVE '2' TO CODE-RETOUR                             
                       ELSE GO TO CODE-RETOUR-SQL-ABANDON                       
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF CODE-DELETE                                                       
              IF SQL-CODE = +100                                                
                 MOVE '1' TO CODE-RETOUR                                        
               ELSE                                                             
                 IF SQL-CODE < 0                                                
                    GO TO CODE-RETOUR-SQL-ABANDON                               
                 END-IF                                                         
              END-IF                                                            
            END-IF.                                                             
           IF CODE-DECLARE                                                      
              MOVE 0  TO  SQL-CODE                                              
              MOVE 0  TO  TRACE-SQL-CODE                                        
      *       IF SQL-CODE < 0                                                   
      *          GO TO CODE-RETOUR-SQL-ABANDON                                  
      *       END-IF                                                            
           END-IF.                                                              
           IF CODE-PREPARE                                                      
              IF SQL-CODE < 0                                                   
                 GO TO CODE-RETOUR-SQL-ABANDON                                  
              END-IF                                                            
           END-IF.                                                              
           IF CODE-FETCH                                                        
              IF SQL-CODE = +100                                                
                 MOVE '1' TO CODE-RETOUR                                        
               ELSE                                                             
                 IF SQL-CODE < 0                                                
                    GO TO CODE-RETOUR-SQL-ABANDON                               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF CODE-OPEN                                                         
              IF SQL-CODE < 0                                                   
                 GO TO CODE-RETOUR-SQL-ABANDON                                  
              END-IF                                                            
           END-IF.                                                              
           IF CODE-CLOSE                                                        
              IF SQL-CODE < 0                                                   
                 GO TO CODE-RETOUR-SQL-ABANDON                                  
              END-IF                                                            
           END-IF.                                                              
       FIN-TEST-CODE-RETOUR-SQL.      EXIT.                                     
       EJECT                                                                    
       CODE-RETOUR-SQL-ABANDON        SECTION.                                  
           MOVE SPACES             TO MESS.                                     
           STRING 'SQLCODE='                                                    
                  TRACE-SQL-CODE                                                
                  ':'                                                           
                  SQLERRMC                                                      
                  DELIMITED BY SIZE   INTO MESS.                                
           GO TO ABANDON-SQL.                                                   
                                                                                
                                                                                
