      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      * TRAITEMENTS MQ - COTE HOST                                              
      * UTILISEE PAR MMQ10 ET APPLICATIFS                                       
      *****************************************************************         
      * ENTREE : 24 OCTETS                                                      
      * SORTIE : 24 + 2 + MESSAGE                                               
      *                                                                         
       01  COMM-MQ10-APPLI REDEFINES Z-COMMAREA-LINK.                           
            05  COMM-MQ10-ENTREE.                                               
               10  COMM-MQ10-CORRELID        PIC X(24).                         
            05  COMM-MQ10-SORTIE.                                               
               10  COMM-MQ10-CODRET          PIC X(2).                          
               10  COMM-MQ10-MESSAGE         PIC X(49781).                      
                                                                                
