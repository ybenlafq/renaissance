      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG2500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG2500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG2500.                                                    00000090
           02  EG25-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG25-NOMCHAMP                                            00000120
               PIC X(0018).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-LIGNECALC                                           00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG25-LIGNECALC                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG25-CHCALC1                                             00000160
               PIC X(0018).                                             00000170
           02  EG25-TYPCALCUL                                           00000180
               PIC X(0002).                                             00000190
           02  EG25-CHCALC2                                             00000200
               PIC X(0018).                                             00000210
           02  EG25-DSYST                                               00000220
               PIC S9(13) COMP-3.                                       00000230
      *                                                                 00000240
      *---------------------------------------------------------        00000250
      *   LISTE DES FLAGS DE LA TABLE RVEG2500                          00000260
      *---------------------------------------------------------        00000270
      *                                                                 00000280
       01  RVEG2500-FLAGS.                                              00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-NOMETAT-F                                           00000300
      *        PIC S9(4) COMP.                                          00000310
      *--                                                                       
           02  EG25-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-NOMCHAMP-F                                          00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  EG25-NOMCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-LIGNECALC-F                                         00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  EG25-LIGNECALC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-CHCALC1-F                                           00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  EG25-CHCALC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-TYPCALCUL-F                                         00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  EG25-TYPCALCUL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-CHCALC2-F                                           00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  EG25-CHCALC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG25-DSYST-F                                             00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  EG25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000440
                                                                                
