      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY *****************************************************          
      *          * RETOUR SANS COMMAREA                              *          
      ***************************************************** SYKCRT00 *          
       XCTL-NO-COMMAREA SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS XCTL                                                           
                 PROGRAM  (NOM-PROG-XCTL)                                       
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��00006   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-PROG-XCTL.                   
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE TO EIB-RCODE.                                          
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
       FIN-XCTL-NO-COMMAREA.  EXIT.                                             
       EJECT                                                                    
                                                                                
                                                                                
