      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * HYBRIS                                                                  
      * MAPPING                                                                 
E0647 ******************************************************************        
      * DE02024 09/10/2012 CODE INITIAL                                         
      ******************************************************************        
       01 MES-XM20-DATA.                                                        
          02 MES-MQHI-ENTREE.                                                   
             05 MES-MQHI-MSGID                          PIC X(24).              
             05 MES-MQHI-CORRELID                       PIC X(24).              
          02 MES-MQHI-SORTIE.                                                   
             05 MES-MQHI-CODRET                         PIC X(2).               
      *   MESSAGE LIMITE A 48K                                                  
          02 MES-MQHI-MESS                              PIC X(49152).           
                                                                                
