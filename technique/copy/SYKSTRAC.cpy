      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000000
      *==> DARTY ****************************************************** 00000104
      *          * TRACE-SQL                                          * 00000204
      ****************************************************** SYKSTRAC * 00000304
      *                                                                 00000400
       TRACE-SQL        SECTION.                                        00000700
      *                                                                 00000800
           MOVE SQLCODE            TO  TRACE-SQL-CODE.                  00000904
           MOVE SQLCODE            TO  SQL-CODE.                        00001004
           MOVE TRACE-SQL-MESSAGE  TO  MESS.                            00001204
      *                                                                 00001300
           IF  TERMINAL-ECRAN                                           00001905
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 FROM    (TRACE-MESSAGE)                                        
                 LENGTH  (TRACE-MESSAGE-LONG)                                   
                 CTLCHAR (WCC-SCREEN)                                           
       END-EXEC                                                                 
      *dfhei*- commente DFHEI1                                                  
      *dfhei*         MOVE '����00013   ' TO DFHEIV0                          
      *dfhei*         CALL 'DFHEI1' USING DFHEIV0  DFHDUMMY DFHDUMMY            
      *dfhei*     TRACE-MESSAGE TRACE-MESSAGE-LONG WCC-SCREEN                   
      *dfhei*} decommente EXEC                                                  
           END-IF.                                                      00002505
      *                                                                 00002800
           IF  TERMINAL-IMPRIMANTE                                      00003005
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 FROM    (TRACE-MESS)                                           
                 LENGTH  (LENGTH OF TRACE-MESS)                                 
                 CTLCHAR (WCC-PRINTER)                                          
       END-EXEC                                                                 
      *dfhei*- commente DFHEI1                                                  
      *dfhei*         MOVE '����00021   ' TO DFHEIV0                          
      *dfhei*         MOVE LENGTH OF TRACE-MESS TO DFHB0020                     
      *dfhei*         CALL 'DFHEI1' USING DFHEIV0  DFHDUMMY DFHDUMMY TRA        
      *dfhei*      DFHB0020 WCC-PRINTER                                         
      *dfhei*} decommente EXEC                                                  
           END-IF.                                                      00003605
      *                                                                 00003900
           MOVE  SPACES  TO  FILLER-SQL.                                00004000
      *                                                                 00004100
       FIN-TRACE-SQL. EXIT.                                             00004200
             EJECT                                                      00004302
                                                                                
                                                                                
