      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000100
      *  MODULE GENERALISE DE DELETE D'UN RECORD                      * 00000200
      ****************************************************** SYKCDELE * 00000300
      *                                                                 00000400
       DELETE-RECORD             SECTION.                               00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS DELETE   DATASET (FILE-NAME)                                   
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '���00007   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME.                       
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001300
           MOVE EIBRCODE TO EIB-RCODE.                                  00001400
      *                                                                 00001300
           IF   EIB-NORMAL                                              00001500
                THEN MOVE '0' TO CODE-RETOUR                            00001600
                ELSE MOVE '1' TO CODE-RETOUR                            00001800
           END-IF.                                                              
      *                                                                 00001900
       FIN-DELETE-RECORD.  EXIT.                                        00002000
       EJECT                                                            00002200
                                                                                
                                                                                
