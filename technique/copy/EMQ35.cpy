      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMQ35   EMQ35                                              00000020
      ***************************************************************** 00000030
       01   EMQ35I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCIETEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUEUEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MQUEUEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQUEUEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQUEUEI   PIC X(24).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MFONCI    PIC X(3).                                       00000290
           02 MDATEFD OCCURS   5 TIMES .                                00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEFL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MDATEFL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEFF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MDATEFI      PIC X(10).                                 00000340
           02 MHEURED OCCURS   5 TIMES .                                00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHEUREL      COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MHEUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MHEUREF      PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MHEUREI      PIC X(10).                                 00000390
           02 MSOCD OCCURS   5 TIMES .                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MSOCI   PIC X(3).                                       00000440
           02 MLIEUD OCCURS   5 TIMES .                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000460
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MLIEUI  PIC X(3).                                       00000490
           02 MFOND OCCURS   5 TIMES .                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFONL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MFONL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MFONF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MFONI   PIC X(3).                                       00000540
           02 MMES1D OCCURS   5 TIMES .                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMES1L  COMP PIC S9(4).                                 00000560
      *--                                                                       
             03 MMES1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMES1F  PIC X.                                          00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MMES1I  PIC X(30).                                      00000590
           02 MMES2D OCCURS   5 TIMES .                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMES2L  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MMES2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMES2F  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MMES2I  PIC X(70).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIBERRI  PIC X(78).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCICSI    PIC X(5).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSCREENI  PIC X(4).                                       00000840
      ***************************************************************** 00000850
      * SDF: EMQ35   EMQ35                                              00000860
      ***************************************************************** 00000870
       01   EMQ35O REDEFINES EMQ35I.                                    00000880
           02 FILLER    PIC X(12).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDATJOUA  PIC X.                                          00000910
           02 MDATJOUC  PIC X.                                          00000920
           02 MDATJOUP  PIC X.                                          00000930
           02 MDATJOUH  PIC X.                                          00000940
           02 MDATJOUV  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MNPAGEA   PIC X.                                          00001050
           02 MNPAGEC   PIC X.                                          00001060
           02 MNPAGEP   PIC X.                                          00001070
           02 MNPAGEH   PIC X.                                          00001080
           02 MNPAGEV   PIC X.                                          00001090
           02 MNPAGEO   PIC X(5).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MSOCIETEA      PIC X.                                     00001120
           02 MSOCIETEC PIC X.                                          00001130
           02 MSOCIETEP PIC X.                                          00001140
           02 MSOCIETEH PIC X.                                          00001150
           02 MSOCIETEV PIC X.                                          00001160
           02 MSOCIETEO      PIC X(3).                                  00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MQUEUEA   PIC X.                                          00001190
           02 MQUEUEC   PIC X.                                          00001200
           02 MQUEUEP   PIC X.                                          00001210
           02 MQUEUEH   PIC X.                                          00001220
           02 MQUEUEV   PIC X.                                          00001230
           02 MQUEUEO   PIC X(24).                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MFONCA    PIC X.                                          00001260
           02 MFONCC    PIC X.                                          00001270
           02 MFONCP    PIC X.                                          00001280
           02 MFONCH    PIC X.                                          00001290
           02 MFONCV    PIC X.                                          00001300
           02 MFONCO    PIC X(3).                                       00001310
           02 DFHMS1 OCCURS   5 TIMES .                                 00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MDATEFA      PIC X.                                     00001340
             03 MDATEFC PIC X.                                          00001350
             03 MDATEFP PIC X.                                          00001360
             03 MDATEFH PIC X.                                          00001370
             03 MDATEFV PIC X.                                          00001380
             03 MDATEFO      PIC X(10).                                 00001390
           02 DFHMS2 OCCURS   5 TIMES .                                 00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MHEUREA      PIC X.                                     00001420
             03 MHEUREC PIC X.                                          00001430
             03 MHEUREP PIC X.                                          00001440
             03 MHEUREH PIC X.                                          00001450
             03 MHEUREV PIC X.                                          00001460
             03 MHEUREO      PIC X(10).                                 00001470
           02 DFHMS3 OCCURS   5 TIMES .                                 00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MSOCA   PIC X.                                          00001500
             03 MSOCC   PIC X.                                          00001510
             03 MSOCP   PIC X.                                          00001520
             03 MSOCH   PIC X.                                          00001530
             03 MSOCV   PIC X.                                          00001540
             03 MSOCO   PIC X(3).                                       00001550
           02 DFHMS4 OCCURS   5 TIMES .                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLIEUA  PIC X.                                          00001580
             03 MLIEUC  PIC X.                                          00001590
             03 MLIEUP  PIC X.                                          00001600
             03 MLIEUH  PIC X.                                          00001610
             03 MLIEUV  PIC X.                                          00001620
             03 MLIEUO  PIC X(3).                                       00001630
           02 DFHMS5 OCCURS   5 TIMES .                                 00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MFONA   PIC X.                                          00001660
             03 MFONC   PIC X.                                          00001670
             03 MFONP   PIC X.                                          00001680
             03 MFONH   PIC X.                                          00001690
             03 MFONV   PIC X.                                          00001700
             03 MFONO   PIC X(3).                                       00001710
           02 DFHMS6 OCCURS   5 TIMES .                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MMES1A  PIC X.                                          00001740
             03 MMES1C  PIC X.                                          00001750
             03 MMES1P  PIC X.                                          00001760
             03 MMES1H  PIC X.                                          00001770
             03 MMES1V  PIC X.                                          00001780
             03 MMES1O  PIC X(30).                                      00001790
           02 DFHMS7 OCCURS   5 TIMES .                                 00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MMES2A  PIC X.                                          00001820
             03 MMES2C  PIC X.                                          00001830
             03 MMES2P  PIC X.                                          00001840
             03 MMES2H  PIC X.                                          00001850
             03 MMES2V  PIC X.                                          00001860
             03 MMES2O  PIC X(70).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLIBERRA  PIC X.                                          00001890
           02 MLIBERRC  PIC X.                                          00001900
           02 MLIBERRP  PIC X.                                          00001910
           02 MLIBERRH  PIC X.                                          00001920
           02 MLIBERRV  PIC X.                                          00001930
           02 MLIBERRO  PIC X(78).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCODTRAA  PIC X.                                          00001960
           02 MCODTRAC  PIC X.                                          00001970
           02 MCODTRAP  PIC X.                                          00001980
           02 MCODTRAH  PIC X.                                          00001990
           02 MCODTRAV  PIC X.                                          00002000
           02 MCODTRAO  PIC X(4).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCICSA    PIC X.                                          00002030
           02 MCICSC    PIC X.                                          00002040
           02 MCICSP    PIC X.                                          00002050
           02 MCICSH    PIC X.                                          00002060
           02 MCICSV    PIC X.                                          00002070
           02 MCICSO    PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNETNAMA  PIC X.                                          00002100
           02 MNETNAMC  PIC X.                                          00002110
           02 MNETNAMP  PIC X.                                          00002120
           02 MNETNAMH  PIC X.                                          00002130
           02 MNETNAMV  PIC X.                                          00002140
           02 MNETNAMO  PIC X(8).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSCREENA  PIC X.                                          00002170
           02 MSCREENC  PIC X.                                          00002180
           02 MSCREENP  PIC X.                                          00002190
           02 MSCREENH  PIC X.                                          00002200
           02 MSCREENV  PIC X.                                          00002210
           02 MSCREENO  PIC X(4).                                       00002220
                                                                                
