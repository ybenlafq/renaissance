      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION EU10 APELLEE PAR GV42  *        
      *               APPELE PAR GV00                                  *        
      *  LONGUEUR   : 200                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-EU10-APPLI.                                             00260000
           02 COMM-EU10-LONG-COMMAREA   PIC 9(03) VALUE 200.            00320000
           02 COMM-EU10-NSOCIETE        PIC X(03).                      00320000
           02 COMM-EU10-NLIEU           PIC X(03).                      00320000
           02 COMM-EU10-NVENTE          PIC X(07).                      00320000
           02 COMM-EU10-DATE-SAMJ       PIC X(08).                              
           02 COMM-EU10-MESSAGE.                                                
                10 COMM-EU10-CODRET          PIC S9(4).                         
                   88  COMM-EU10-CODRET-OK   VALUE 0.                           
                   88  COMM-EU10-CODRET-ERREUR  VALUE 1.                        
                10 COMM-EU10-LIBERR          PIC X(58).                         
           02 FILLER                    PIC X(113).                     00340000
                                                                                
