      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * RECEPTION ZONE DE COMMANDE                                    *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       RECEPTION-ZONE-COMMANDE      SECTION.                                    
      *                                                                         
      * INITIALISATION                                                          
      *                                                                         
           MOVE MZONCMDI  TO  DISPATCH-INPUT.                                   
           MOVE SPACES    TO  DISPATCH-OUTPUT.                                  
           MOVE SPACES    TO  Z-COMMAREA-SELECT.                                
      *                                                                         
      * TEST DU JUMP                                                            
      *                                                                         
           IF OCTET-DISPATCH-INPUT (1) = '='                                    
              THEN MOVE EIBTRNID   TO  Z-COMMAREA-TACHE-JUMP                    
                   MOVE CODE-JUMP  TO  FONCTION                                 
                   MOVE 1          TO  DISPATCH-LEVEL                           
                   MOVE 1          TO  IA                                       
              ELSE MOVE 0          TO  IA                                       
           END-IF.                                                              
      *                                                                         
           MOVE     0  TO  AI.                                                  
           PERFORM  DECOUPAGE-SELECTION UNTIL IA = 15.                          
      *                                                                         
           IF DISPATCH-SELECTION (1) NOT = SPACES                               
              MOVE DISPATCH-SELECTION (1) TO Z-COMMAREA-SELECTION (1)           
           END-IF.                                                              
      *                                                                         
           IF DISPATCH-SELECTION (2) NOT = SPACES                               
              MOVE DISPATCH-SELECTION (2) TO Z-COMMAREA-SELECTION (2)           
           END-IF.                                                              
      *                                                                         
           IF DISPATCH-SELECTION (3) NOT = SPACES                               
              MOVE DISPATCH-SELECTION (3) TO Z-COMMAREA-SELECTION (3)           
           END-IF.                                                              
      *                                                                         
       FIN-RECEPTION-ZCMD. EXIT.                                                
       EJECT                                                                    
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DECOUPAGE SELECTION   * &V2. * TRAITEMENT NORMAL DISPATCHING  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       DECOUPAGE-SELECTION           SECTION.                                   
      *                                                                         
           ADD 1 TO IA.                                                         
      *                                                                         
           IF DISPATCH-LEVEL GREATER THAN 3                                     
              MOVE 15 TO IA                                                     
              GO TO FIN-DECOUPAGE-SELECTION                                     
           END-IF.                                                              
      *                                                                         
           IF OCTET-DISPATCH-INPUT (IA) = LOW-VALUE OR SPACES                   
              MOVE 15 TO IA                                                     
              GO TO FIN-DECOUPAGE-SELECTION                                     
           END-IF.                                                              
      *                                                                         
           IF OCTET-DISPATCH-INPUT (IA) = '.'                                   
              ADD 1   TO DISPATCH-LEVEL                                         
              MOVE 0  TO AI                                                     
              GO TO FIN-DECOUPAGE-SELECTION                                     
           END-IF.                                                              
      *                                                                         
           IF OCTET-DISPATCH-INPUT (IA) NOT = '.'                               
              ADD 1 TO AI                                                       
              MOVE OCTET-DISPATCH-INPUT (IA)                                    
                TO OCTET-DISPATCH-OUTPUT (DISPATCH-LEVEL , AI)                  
              GO TO FIN-DECOUPAGE-SELECTION                                     
           END-IF.                                                              
      *                                                                         
       FIN-DECOUPAGE-SELECTION. EXIT.                                           
       EJECT                                                                    
                                                                                
                                                                                
