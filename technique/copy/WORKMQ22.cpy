      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   ENVOI MESSAGE MQ HOST --> LOCAL PAR BATCH                             
      *   ZONES PASSES EN PARAMETRE DU CALL BMQ022                              
      *****************************************************************         
      *                                                                         
      *                                                                         
      * CODRET : 1 = CONNECT NON OK                                             
      *          2 = MQOPEN NON OK                                              
      *          3 = MQPUT    //                                                
      *          4 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          5 = SELECT SS-TABLE NCGFC NON TROUVE                           
      *          6 = MQCLOSE NON OK                                             
      *          7 = DISCONNECT NON OK                                          
      *          8 = SQLCODE < 0                                                
      *          9 = ERREUR BETDATC                                             
      *                                                                         
       01  WORK-MQ22-APPLI.                                                     
           05  WORK-MQ22-ENTREE.                                                
               10  WORK-MQ22-NSOC            PIC X(03).                         
               10  WORK-MQ22-NSOCDST         PIC X(03).                         
               10  WORK-MQ22-NLIEU           PIC X(03).                         
               10  WORK-MQ22-NLIEUDST        PIC X(03).                         
               10  WORK-MQ22-NSOC-STOCK      PIC X(03).                         
               10  WORK-MQ22-NLIEU-STOCK     PIC X(03).                         
               10  WORK-MQ22-QALIAS          PIC X(24).                         
               10  WORK-MQ22-QALIAS-S        PIC X(24).                         
      *  ACTION :                                                               
      *    PLUSIEURS MESSAGES (1 PAR CALL)                                      
      *           1 --> CONNECT, OPEN, ET PUT                                   
      *           P --> PUT                                                     
      *           D --> PUT, CLOSE ET DISCONNECT                                
      *           F --> CLOSE ET DISCONNECT                                     
      *    UN SEUL MESSAGE                                                      
      *           U --> CONNECT, OPEN, PUT, CLOSE ET DISCONNECT                 
               10  WORK-MQ22-ACTION          PIC X.                             
               10  WORK-MQ22-CFONC           PIC X(03).                         
               10  WORK-MQ22-CFONC-STOCK     PIC X(03).                         
               10  WORK-MQ22-NOMPROG         PIC X(06).                         
               10  WORK-MQ22-MSGID           PIC X(24).                         
               10  WORK-MQ22-CORRELID        PIC X(24).                         
               10  WORK-MQ22-PRIORITY        PIC S9(1).                         
               10  WORK-MQ22-SYNCP           PIC X.                             
               10  WORK-MQ22-IDSI            PIC X(10).                         
               10  WORK-MQ22-OPCO            PIC X(03).                         
               10  WORK-MQ22-CLE             PIC X(45).                         
               10  WORK-MQ22-WENTETE         PIC X.                             
                 88  WORK-MQ22-AVEC-ENTETE   VALUE 'O'.                         
                 88  WORK-MQ22-SANS-ENTETE   VALUE 'N'.                         
               10  WORK-MQ22-STOCKAGE-MSG    PIC X.                             
                 88  WORK-MQ22-STOCKAGE      VALUE 'O'.                         
                 88  WORK-MQ22-ENVOI-SIMPLE  VALUE 'N'.                         
               10  WORK-MQ22-NB-MESSAGES     PIC 9(5).                          
               10  WORK-MQ22-NBOCC           PIC 9(5).                          
               10  WORK-MQ22-LONG-ENREG      PIC 9(5).                          
               10  WORK-MQ22-VERSION         PIC X(2).                          
               10  WORK-MQ22-DISPLAY         PIC X.                             
               10  WORK-MQ22-DSYST           PIC X(13).                         
               10  WORK-MQ22-FILLER          PIC X(50).                         
      *        ZONES SPECIF. AU PROGRAMME                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  WORK-MQ22-HCONN           PIC S9(9) BINARY.                  
      *--                                                                       
               10  WORK-MQ22-HCONN           PIC S9(9) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  WORK-MQ22-1-HOBJ          PIC S9(9) BINARY.                  
      *--                                                                       
               10  WORK-MQ22-1-HOBJ          PIC S9(9) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  WORK-MQ22-2-HOBJ          PIC S9(9) BINARY.                  
      *--                                                                       
               10  WORK-MQ22-2-HOBJ          PIC S9(9) COMP-5.                  
      *}                                                                        
               10  WORK-MQ22-PERS            PIC X.                             
               10  WORK-MQ22-PERS-S          PIC X.                             
               10  WORK-MQ22-EXPIRY          PIC S9(5).                         
               10  WORK-MQ22-EXPIRY-S        PIC S9(5).                         
           05  WORK-MQ22-SORTIE.                                                
               10  WORK-MQ22-CODRET          PIC XX.                            
               10  WORK-MQ22-ERREUR          PIC X(80).                         
           05  WORK-MQ22-MESS.                                                  
               10  WORK-MQ22-MESSAGE         PIC X(50000).                      
                                                                                
