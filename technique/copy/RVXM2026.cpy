      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2026                  *        
      ******************************************************************        
       01  RVXM2026.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2026-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2026-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2026-NCODIC          PIC X(07).                                 
           10 XM2026-CTYPDCL.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2026-CTYPDCL-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2026-CTYPDCL-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2026-CTYPDCL-TEXT      PIC X(5).                             
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2026-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2026-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2026-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2026-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2026-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2026-CTYPDCL-F       PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2026-CTYPDCL-F       PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
