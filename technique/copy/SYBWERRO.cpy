      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES * 00000202
      ***************************************************************** 00000300
      *                                                                 00000401
       01  Z-ERREUR REDEFINES Z-INOUT.                                  00000503
      *                                                                 00000602
           05  FILLER                   PIC X(256).                     00000703
      *                                                                 00000802
           05  Z-ERREUR-TRACE-MESS      PIC X(80).                      00000903
      *                                                                 00001002
       01  FILLER.                                                      00002003
      *                                                                 00003002
           05  TRACE-MESSAGE.                                           00004003
      *                                                                 00005002
               10  TRACE-MESS.                                          00006003
                   15 MESS1             PIC X(15)       VALUE SPACE.    00007002
                   15 MESS              PIC X(64)       VALUE SPACE.    00008002
      *                                                                 00009002
      ***************************************************************** 00010002
                                                                                
