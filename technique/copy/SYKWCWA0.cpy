      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * DESCRIPTION DE LA COMMON STORAGE ....SAMPLE                   *         
      ****************************************************** SYKWCWA0 *         
      *                                                                         
       01  FILLER PIC X(16) VALUE '*** ZONE-CWA ***'.                           
      *                                                                         
       01  Z-CWA.                                                               
      *                                                                         
          03 Z-CWADATJ.                                                         
      *                                                                         
           05 Z-CWA-DATJOU.                                                     
              10  JJ                   PIC 9(2) VALUE 0.                        
              10  FILLER               PIC X.                                   
              10  MM                   PIC 9(2) VALUE 0.                        
              10  FILLER               PIC X.                                   
              10  AA                   PIC 9(2) VALUE 0.                        
          03  FILLER                   PIC X(504).                              
       EJECT                                                                    
                                                                                
                                                                                
