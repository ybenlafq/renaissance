      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2043                            *        
      ******************************************************************        
       01  RVXM2043.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2043-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2043-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2043-CFAM.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2043-CFAM-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2043-CFAM-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2043-CFAM-TEXT         PIC X(05).                            
           10 XM2043-CMODDEL.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2043-CMODDEL-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2043-CMODDEL-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2043-CMODDEL-TEXT      PIC X(03).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2043                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2043-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2043-CFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2043-CFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2043-CMODDEL-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2043-CMODDEL-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 03      *        
      ******************************************************************        
                                                                                
