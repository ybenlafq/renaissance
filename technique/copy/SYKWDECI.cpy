      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TRAVAIL POUR LES TRAITEMENTS DES VALEURS DECIMALES   *         
      ****************************************************** SYKWDECI *         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-DECI ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-DECI-USER             PIC X(20).                               
           05  FILLER                  REDEFINES W-DECI-USER.                   
             10  FILLER                OCCURS 20.                               
               15  W-DECI-USER-CAR     PIC X.                                   
               15  W-DECI-USER-CAR9    REDEFINES W-DECI-USER-CAR PIC 9.         
      *                                                                         
           05  W-DECI-FILE             PIC S9(9)V9(9) VALUE +0.                 
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-DECI-LONG            PIC S9(3)   COMP-3 VALUE +20.             
           05  W-DECI-I               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-DECI-POIDS           PIC S9V9(9) COMP-3 VALUE +0.              
           05  W-DECI-SIGNE           PIC X            VALUE ' '.               
           05  W-DECI-FLAG            PIC X            VALUE SPACE.             
           05  W-DECI-EDIT            PIC -(9)9,9(9)   VALUE ZERO.              
           05  W-DECI-REEDIT REDEFINES W-DECI-EDIT.                             
             10  FILLER               OCCURS 20.                                
               15  W-DECI-EDIT-CAR    PIC X.                                    
           05  W-DECI-TAMPON.                                                   
             10  W-DECI-TAMPON1       PIC X            VALUE SPACE.             
             10  W-DECI-TAMPON2       PIC X(19)        VALUE SPACE.             
       EJECT                                                                    
                                                                                
                                                                                
