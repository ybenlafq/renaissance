      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ RECOPIE ENTETE MESSAGE DEAD LETTER QUEUE              
      *        HOST --> HOST                                                    
      *****************************************************************         
       01  WS-MESSAGE-DLQ.                                                      
         02 MESSAGE-ENTETE-DLQ.                                                 
           05  MES-TYPE-DLQ    PIC    X(4).                                     
           05  FILLER          PIC    X(08).                                    
           05  MES-QDEST.                                                       
               10  MES-QUEUE   PIC    X(24).                                    
               10  FILLER      PIC    X(24).                                    
           05  MES-QMGR        PIC    X(48).                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MES-ENCODING    PIC   S9(09) BINARY.                             
      *--                                                                       
           05  MES-ENCODING    PIC   S9(09) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MES-CODECAR     PIC   S9(09) BINARY.                             
      *--                                                                       
           05  MES-CODECAR     PIC   S9(09) COMP-5.                             
      *}                                                                        
           05  MES-FORMAT      PIC    X(08).                                    
           05  FILLER          PIC    X(48).                                    
                                                                                
