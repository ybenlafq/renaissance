      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2017                  *        
      ******************************************************************        
       01  RVXM2017.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2017-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2017-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2017-NCODIC          PIC X(07).                                 
           10 XM2017-CMODDEL         PIC X(23).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2017-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2017-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2017-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2017-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2017-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2017-CMODDEL-F       PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2017-CMODDEL-F       PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
