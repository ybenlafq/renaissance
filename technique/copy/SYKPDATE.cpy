      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                                         
      * VERIFICATION ET EDITION DES DATES                                       
      *                                                                         
      *****************************************************************         
       TRAITEMENT-DATE SECTION.                                                 
      *****************************************************************         
      *                                                                         
       DEB-TRAITEMENT-DATE.                                                     
      *                                                                         
           MOVE 4 TO CODE-RETOUR.                                               
      *                                                                         
           IF W-DATE-USER NOT = SPACES                                          
                                   PERFORM T-DATE-USER                          
           ELSE IF W-DATE-FILE NOT = SPACES                                     
                                   PERFORM T-DATE-FILE                          
                END-IF                                                          
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-DATE. EXIT.                                               
      *                                                                         
      *****************************************************************         
       T-DATE-USER SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-DATE-USER.                                                         
      *                                                                         
           IF AA OF W-DATE-USER-REDEF = SPACES                                  
              MOVE SS OF W-DATE-USER-REDEF TO AA OF W-DATE-USER-REDEF           
              IF AA OF W-DATE-USER-REDEF  < '50'                                
               MOVE        '20'              TO SS OF W-DATE-USER-REDEF         
              ELSE                                                              
               MOVE        '19'              TO SS OF W-DATE-USER-REDEF         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
      * VERIFICATION A PARTIR DES ZONES USER                                    
      *                                                                         
           IF JJ OF W-DATE-USER NOT NUMERIC                                     
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF MM OF W-DATE-USER NOT NUMERIC                                     
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF SS OF W-DATE-USER NOT NUMERIC                                     
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF AA OF W-DATE-USER NOT NUMERIC                                     
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF S1 OF W-DATE-USER NOT = ' ' AND '-' AND '/'                       
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF S2 OF W-DATE-USER NOT = ' ' AND '-' AND '/'                       
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF S1 OF W-DATE-USER NOT = S2 OF W-DATE-USER                         
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
      * CONTROLE PAR RAPPORT AUX VALEURS EXTREMES                               
           IF SS OF W-DATE-USER NOT = 18 AND 19 AND 20                          
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF MM OF W-DATE-USER < 1                                             
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF MM OF W-DATE-USER > 12                                            
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF JJ OF W-DATE-USER < 1                                             
                                     GO TO FIN-T-DATE-USER                      
           END-IF.                                                              
           IF MM OF W-DATE-USER = 1                                             
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 2                                             
                 IF JJ OF W-DATE-USER > 29                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 3                                             
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 4                                             
                 IF JJ OF W-DATE-USER > 30                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 5                                             
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 6                                             
                 IF JJ OF W-DATE-USER > 30                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 7                                             
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 8                                             
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 9                                             
                 IF JJ OF W-DATE-USER > 30                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 10                                            
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 11                                            
                 IF JJ OF W-DATE-USER > 30                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
           IF MM OF W-DATE-USER = 12                                            
                 IF JJ OF W-DATE-USER > 31                                      
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
           END-IF.                                                              
      * CONTROLE ANNEES BISSEXTILES                                             
           DIVIDE AA OF W-DATE-USER BY 4                                        
                                  GIVING W-DATE-Q REMAINDER W-DATE-R.           
           IF  MM OF W-DATE-USER = 2                                            
             IF  W-DATE-R NOT = 0                                               
               IF JJ OF W-DATE-USER > 28                                        
                                     GO TO FIN-T-DATE-USER                      
               END-IF                                                           
             END-IF                                                             
           END-IF.                                                              
      * EXCEPTIONNELLEMENT, L'ANNEE 1900 N'ETAIT PAS BISSEXTILE                 
           IF  SS OF W-DATE-USER = 19                                           
             IF  AA OF W-DATE-USER = 00                                         
               IF  MM OF W-DATE-USER = 2                                        
                 IF  JJ OF W-DATE-USER > 28                                     
                                     GO TO FIN-T-DATE-USER                      
                 END-IF                                                         
               END-IF                                                           
             END-IF                                                             
           END-IF.                                                              
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
           MOVE CORRESPONDING W-DATE-USER TO W-DATE-FILE.                       
      *                                                                         
       FIN-T-DATE-USER. EXIT.                                                   
      *                                                                         
      *****************************************************************         
       T-DATE-FILE SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-DATE-FILE.                                                         
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
      * PETITE MISE EN FORME                                                    
      *                                                                         
           IF JJ OF W-DATE-FILE NOT NUMERIC                                     
              MOVE MM OF W-DATE-FILE TO JJ OF W-DATE-FILE                       
              MOVE AA OF W-DATE-FILE TO MM OF W-DATE-FILE                       
              MOVE SS OF W-DATE-FILE TO AA OF W-DATE-FILE                       
              IF AA OF W-DATE-FILE  < '50'                                      
               MOVE        '20'              TO SS OF W-DATE-FILE               
              ELSE                                                              
               MOVE        '19'              TO SS OF W-DATE-FILE               
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
      * CONVERSION (SS)AAMMJJ ---> JJ/MM/(SS)AA                                 
      *                                                                         
           MOVE CORRESPONDING W-DATE-FILE TO W-DATE-USER.                       
      *                                                                         
           IF SS OF W-DATE-USER = 19 OR 20                                      
              MOVE AA OF W-DATE-USER TO SS OF W-DATE-USER                       
              MOVE      SPACE        TO AA OF W-DATE-USER-REDEF                 
           END-IF.                                                              
      *                                                                         
           MOVE '-' TO S1 OF W-DATE-USER.                                       
           MOVE '-' TO S2 OF W-DATE-USER.                                       
      *                                                                         
      * CONVERSION SSAAMMJJ ---> SSAAQQQ                                        
      *                                                                         
      *                                                                         
           MOVE SS OF W-DATE-FILE TO SS OF W-DATE-QUANT.                        
           MOVE AA OF W-DATE-FILE TO AA OF W-DATE-QUANT.                        
      *                                                                         
           MOVE 0 TO QQQ OF W-DATE-QUANT.                                       
           IF MM OF W-DATE-FILE > 1                                             
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 2                                             
                      ADD 28 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 3                                             
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 4                                             
                      ADD 30 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 5                                             
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 6                                             
                      ADD 30 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 7                                             
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 8                                             
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 9                                             
                      ADD 30 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 10                                            
                      ADD 31 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
           IF MM OF W-DATE-FILE > 11                                            
                      ADD 30 TO QQQ OF W-DATE-QUANT                             
           END-IF.                                                              
      *                                                                         
           ADD JJ OF W-DATE-FILE TO QQQ OF W-DATE-QUANT.                        
      *                                                                         
           DIVIDE AA OF W-DATE-FILE BY 4                                        
                                  GIVING W-DATE-Q REMAINDER W-DATE-R.           
           IF  W-DATE-R = 0                                                     
               IF  MM OF W-DATE-FILE > 2                                        
                                  ADD 1 TO QQQ OF W-DATE-QUANT                  
               END-IF                                                           
           END-IF.                                                              
      * EXCEPTIONNELLEMENT, L'ANNEE 1900 N'ETAIT PAS BISSEXTILE                 
           IF  SS OF W-DATE-FILE = 19                                           
             IF  AA OF W-DATE-FILE = 00                                         
               IF  MM OF W-DATE-FILE > 2                                        
                                  SUBTRACT 1 FROM QQQ OF W-DATE-QUANT           
               END-IF                                                           
             END-IF                                                             
           END-IF.                                                              
      *                                                                         
       FIN-T-DATE-FILE. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
