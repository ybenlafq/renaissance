      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000000
      *==> DARTY ****************************************************** 00000105
      *          * ENVOI MAP SIMPLE                                   * 00000206
      ****************************************************** SYKCSMDO * 00000306
      *---------------------------------------                          00000400
       SEND-MAP-DATA-ONLY              SECTION.                         00000506
      *---------------------------------------                          00000600
           MOVE SPACES TO COM-CODERR.                                   00000700
           PERFORM INQUIRE-TRANSACTION.                                         
      *    IF NOM-PROG-TACHE = 'TGM99'                                          
      *       PERFORM ENVOI-TS-MAP-DATA-ONLY                                    
      *    ELSE                                                                 
              PERFORM ENVOI-MAP-DATA-ONLY.                                      
      *     END-IF.                                                             
      *---------------------------------------                          00000400
       ENVOI-TS-MAP-DATA-ONLY          SECTION.                         00000506
      *---------------------------------------                          00000600
           SET  COMGM00-DATAONLY    TO TRUE.                                    
           PERFORM MAJ-TS-MAP.                                                  
      *---------------------------------------                          00000400
       ENVOI-MAP-DATA-ONLY             SECTION.                         00000506
      *---------------------------------------                          00000600
      *                                                                 00000800
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 MAP    (NOM-MAP)                                               
                 MAPSET (NOM-MAPSET)                                            
                 FROM   (Z-MAP)                                                 
                 LENGTH (LENGTH OF Z-MAP)                                       
                 DATAONLY                                                       
                 CURSOR                                                         
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�1                                                    
      *dfhei*0026   ' TO DFHEIV0                                                
      *dfhei*     MOVE LENGTH OF Z-MAP TO DFHB0020                              
      *dfhei*     MOVE -1 TO DFHB0021                                           
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHB0020           
      *dfhei*     NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY DFHB0021.               
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001800
           MOVE EIBRCODE TO EIB-RCODE.                                  00001900
      *                                                                 00002005
           IF   NOT EIB-NORMAL                                          00002100
                GO TO ABANDON-CICS                                      00002200
           END-IF.                                                      00002300
      *                                                                 00002400
                                                                                
                                                                                
