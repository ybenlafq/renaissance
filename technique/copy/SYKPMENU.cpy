      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * SORTIE PAR MENU AU LIEU DE CICS                                         
      ******************************************************************        
MENU       STRING 'MENU' EIBTRMID DELIMITED BY SIZE INTO IDENT-TS.              
MENU       MOVE TS-MENU-LONG TO LONG-TS.                                        
MENU       MOVE 1 TO RANG-TS.                                                   
MENU       PERFORM READ-TS.                                                     
MENU       IF TROUVE                                                            
MENU          MOVE Z-INOUT TO TS-MENU                                           
MENU          MOVE 'TMENU' TO NOM-PROG-XCTL                                     
MENU          MOVE TS-MENU TO Z-COMMAREA                                        
MENU          MOVE TS-MENU-LONG TO LONG-COMMAREA                                
MENU          PERFORM XCTL-PROG-COMMAREA                                        
MENU       END-IF.                                                              
                                                                                
