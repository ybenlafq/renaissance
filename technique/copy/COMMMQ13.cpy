      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      * TRAITEMENTS MQ - COTE HOST                                              
      *****************************************************************         
      *                                                                         
      *                                                                         
      * CODRET : 6  = MQOPEN NON OK                                             
      *          7  = MQGET    //                                               
      *          8  = SELECT SS-TABLE MQGET NON TROUVE                          
      *          9  = SELECT SS-TABLE MQGET SQLCODE < 0                         
      *          10 = MQCLOSE NON OK                                            
      *                                                                         
      *                                                                         
       01  COMM-MQ13-APPLI.                                                     
            05  COMM-MQ13-ENTREE.                                               
               10  COMM-MQ13-NSOC            PIC X(03).                         
               10  COMM-MQ13-NOMPROG         PIC X(05).                         
               10  COMM-MQ13-FONCTION        PIC X(03).                         
               10  COMM-MQ13-MSGID           PIC X(24).                         
               10  COMM-MQ13-CORRELID        PIC X(24).                         
            05  COMM-MQ13-SORTIE.                                               
               10  COMM-MQ13-CODRET          PIC XX VALUE '00'.                 
               10  COMM-MQ13-MESSAGE         PIC X(49939).                      
                                                                                
