      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  MODULE GENERALISE DE REWRITE EN MISE A JOUR                    00000200
      ****************************************************************  00000300
      *                                                                 00000400
       REWRITE-RECORD             SECTION.                              00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS REWRITE  DATASET (FILE-NAME)                                   
                          FROM    (Z-INOUT)                                     
                          LENGTH  (FILE-LONG)                                   
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��� 00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME Z-INOUT FILE-LO        
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001400
           MOVE EIBRCODE TO EIB-RCODE.                                  00001500
           IF   NOT EIB-NORMAL                                          00001600
                GO TO ABANDON-CICS                                      00001700
           END-IF.                                                              
      *                                                                 00001800
       FIN-REWRITE-RECORD.  EXIT.                                       00001900
                    EJECT                                               00002100
                                                                                
                                                                                
