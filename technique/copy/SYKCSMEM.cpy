      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000600
      *==> DARTY ****************************************************** 00000100
      *          * ENVOI MAP SANS ERASE (MODIFIE AVEC CURSEUR)        * 00000200
      ****************************************************** SYKCSMEM * 00000400
      *---------------------------------------                          00000600
       SEND-MAP-ERREUR-MANIP           SECTION.                         00000500
      *---------------------------------------                          00000600
           PERFORM INQUIRE-TRANSACTION.                                         
      *    IF NOM-PROG-TACHE = 'TGM99'                                          
      *       PERFORM ENVOI-TS-MAP-ERREUR-MANIP                                 
      *    ELSE                                                                 
              PERFORM ENVOI-MAP-ERREUR-MANIP.                                   
      *    END-IF.                                                              
      *---------------------------------------                          00000600
       ENVOI-TS-MAP-ERREUR-MANIP       SECTION.                         00000500
      *---------------------------------------                          00000600
           MOVE '1'                 TO KONTROL.                                 
           SET  COMGM00-DATAONLY    TO TRUE.                                    
           PERFORM MAJ-TS-MAP.                                                  
      *---------------------------------------                          00000600
       ENVOI-MAP-ERREUR-MANIP          SECTION.                         00000500
      *---------------------------------------                          00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 MAP      (NOM-MAP)                                             
                 MAPSET   (NOM-MAPSET)                                          
                 FROM     (Z-MAP)                                               
                 LENGTH   (LENGTH OF Z-MAP)                                     
                 DATAONLY                                                       
                 CURSOR                                                         
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�1                                                    
      *dfhei*0024   ' TO DFHEIV0                                                
      *dfhei*     MOVE LENGTH OF Z-MAP TO DFHB0020                              
      *dfhei*     MOVE -1 TO DFHB0021                                           
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHB0020           
      *dfhei*     NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY DFHB0021.               
      *dfhei*} decommente EXEC                                                  
      *                                                                 00002100
           MOVE EIBRCODE TO EIB-RCODE.                                  00002200
      *                                                                 00002100
           IF   NOT EIB-NORMAL                                          00002300
                GO TO ABANDON-CICS                                      00002400
           END-IF.                                                              
                                                                                
                                                                                
