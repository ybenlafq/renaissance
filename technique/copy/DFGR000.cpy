      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGR000  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BONS DE RECEPTIONS COMPTABLES.              *         
      *                                                               *         
      *    FGR000       : FICHIER DESTINE A LA MISE A JOUR DES TABLES *         
      *                 : DES  BONS  DE RECEPTION  COMPTABLE  (TABLES *         
      *                 : RTGR30 ET RTGR35).                          *         
      *                                                               *         
      *    STRUCTURE    :    SAM                                      *         
      *    LONGUEUR ENR.:  150 C                                      *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  DSECT-FGR000.                                                        
           03  FGR000-NREC                    PIC  X(07).                       
           03  FGR000-NRECQUAI                PIC  X(07).                       
           03  FGR000-DJRECQUAI               PIC  X(08).                       
           03  FGR000-DSAISREC                PIC  X(08).                       
           03  FGR000-NENTCDE                 PIC  X(05).                       
           03  FGR000-LENTCDE                 PIC  X(20).                       
           03  FGR000-NSOCIETE                PIC  X(03).                       
           03  FGR000-NDEPOT                  PIC  X(03).                       
           03  FGR000-NCDE                    PIC  X(07).                       
           03  FGR000-DVALIDITE               PIC  X(08).                       
           03  FGR000-QDELRGLT                PIC  S9(03)  COMP-3.              
           03  FGR000-NCODIC                  PIC  X(07).                       
           03  FGR000-WTLMELA                 PIC  X(01).                       
           03  FGR000-CFAM                    PIC  X(05).                       
           03  FGR000-CMARQ                   PIC  X(05).                       
           03  FGR000-LREFFOURN               PIC  X(20).                       
           03  FGR000-CTAUXTVA                PIC  X(05).                       
           03  FGR000-CTYPMVT                 PIC  X(03).                       
           03  FGR000-CMVT                    PIC  X(01).                       
           03  FGR000-QREC                    PIC  S9(05)  COMP-3.              
           03  FGR000-DMVTREC                 PIC  X(08).                       
           03  FGR000-NBL                     PIC  X(10).                       
           03  FILLER                         PIC  X(04).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<<<*         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
