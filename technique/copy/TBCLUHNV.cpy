      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00131001
      * COPY DES VARIABLES POUR LE                                     *00131101
      * MODULE DE VERIFICATION VALIDITE CARTE SUR 9 CHIFFRES           *00132001
      * (ALGORITHME DE LUHN OU "MOD 10")      JC0806                   *00133001
      *----------------------------------------------------------------*00134001
      *                                                                 00160000
      * VARIABLE EN ENTREE : NCARTE                                     00295000
       01  W-NCODMOD               PIC X(9) VALUE ZERO.                 00296001
       01  W-NCODMOD-9   REDEFINES W-NCODMOD.                           00296101
           02  W-NCODMOD-99        PIC 9(9).                            00296201
      *                                                                 00296301
      * VARIABLE EN SORTIE                                              00296401
       01  W-MODULO                PIC X VALUE 'N'.                     00296501
           88 MODULO-KO            VALUE 'N'.                           00296601
           88 MODULO-OK            VALUE 'O'.                           00296701
      *                                                                 00296801
      * VARIABLES POUR LES CALCULS                                      00296901
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  IND                       PIC S9(4) COMP   VALUE 0.          00297009
      *--                                                                       
       77  IND                       PIC S9(4) COMP-5   VALUE 0.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  IND2                      PIC S9(4) COMP   VALUE 0.          00297109
      *--                                                                       
       77  IND2                      PIC S9(4) COMP-5   VALUE 0.                
      *}                                                                        
       77  W-CALCUL                  PIC S9(5) COMP-3 VALUE 0.          00297204
       77  W-RESULTAT                PIC S9(5) COMP-3 VALUE 0.          00297313
       77  W-RESTE                   PIC S9(5) COMP-3 VALUE 0.          00298001
      *                                                                 00299001
       01  W-CODMOD                PIC X VALUE 'N'.                     00310000
           88 CODMOD-KO            VALUE 'N'.                           00320000
           88 CODMOD-OK            VALUE 'O'.                           00330000
      *                                                                 00370000
       01  W-CODE0                 PIC 9(9).                            04250005
       01  W-CODE1                 PIC 9(2).                            04260012
       01  W-CEDE1B REDEFINES W-CODE1.                                  04270016
         02  W-CODE1B              PIC 9(1) OCCURS 2.                   04271016
      *                                                                 04310012
                                                                                
