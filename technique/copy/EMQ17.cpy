      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ECS30   ECS30                                              00000020
      ***************************************************************** 00000030
       01   EMQ17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
           02 MCPROGD OCCURS   15 TIMES .                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPROGL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCPROGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPROGF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCPROGI      PIC X(6).                                  00000260
           02 MCFONCD OCCURS   15 TIMES .                               00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFONCL      COMP PIC S9(4).                            00000280
      *--                                                                       
             03 MCFONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFONCF      PIC X.                                     00000290
             03 FILLER  PIC X(4).                                       00000300
             03 MCFONCI      PIC X(3).                                  00000310
           02 MNLIEUD OCCURS   15 TIMES .                               00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MNLIEUI      PIC X(3).                                  00000360
           02 MDATED OCCURS   15 TIMES .                                00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MDATEI  PIC X(10).                                      00000410
           02 MCETATD OCCURS   15 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCETATL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCETATL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCETATF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCETATI      PIC X.                                     00000460
           02 MCOMMENTD OCCURS   15 TIMES .                             00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MCOMMENTI    PIC X(30).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIBERRI  PIC X(78).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCODTRAI  PIC X(4).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCICSI    PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNETNAMI  PIC X(8).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MSCREENI  PIC X(4).                                       00000710
      ***************************************************************** 00000720
      * SDF: ECS30   ECS30                                              00000730
      ***************************************************************** 00000740
       01   EMQ17O REDEFINES EMQ17I.                                    00000750
           02 FILLER    PIC X(12).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MDATJOUA  PIC X.                                          00000780
           02 MDATJOUC  PIC X.                                          00000790
           02 MDATJOUP  PIC X.                                          00000800
           02 MDATJOUH  PIC X.                                          00000810
           02 MDATJOUV  PIC X.                                          00000820
           02 MDATJOUO  PIC X(10).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MTIMJOUA  PIC X.                                          00000850
           02 MTIMJOUC  PIC X.                                          00000860
           02 MTIMJOUP  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUV  PIC X.                                          00000890
           02 MTIMJOUO  PIC X(5).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MNPAGEA   PIC X.                                          00000920
           02 MNPAGEC   PIC X.                                          00000930
           02 MNPAGEP   PIC X.                                          00000940
           02 MNPAGEH   PIC X.                                          00000950
           02 MNPAGEV   PIC X.                                          00000960
           02 MNPAGEO   PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MNSOCA    PIC X.                                          00000990
           02 MNSOCC    PIC X.                                          00001000
           02 MNSOCP    PIC X.                                          00001010
           02 MNSOCH    PIC X.                                          00001020
           02 MNSOCV    PIC X.                                          00001030
           02 MNSOCO    PIC X(3).                                       00001040
           02 DFHMS1 OCCURS   15 TIMES .                                00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MCPROGA      PIC X.                                     00001070
             03 MCPROGC PIC X.                                          00001080
             03 MCPROGP PIC X.                                          00001090
             03 MCPROGH PIC X.                                          00001100
             03 MCPROGV PIC X.                                          00001110
             03 MCPROGO      PIC X(6).                                  00001120
           02 DFHMS2 OCCURS   15 TIMES .                                00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MCFONCA      PIC X.                                     00001150
             03 MCFONCC PIC X.                                          00001160
             03 MCFONCP PIC X.                                          00001170
             03 MCFONCH PIC X.                                          00001180
             03 MCFONCV PIC X.                                          00001190
             03 MCFONCO      PIC X(3).                                  00001200
           02 DFHMS3 OCCURS   15 TIMES .                                00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MNLIEUA      PIC X.                                     00001230
             03 MNLIEUC PIC X.                                          00001240
             03 MNLIEUP PIC X.                                          00001250
             03 MNLIEUH PIC X.                                          00001260
             03 MNLIEUV PIC X.                                          00001270
             03 MNLIEUO      PIC X(3).                                  00001280
           02 DFHMS4 OCCURS   15 TIMES .                                00001290
             03 FILLER       PIC X(2).                                  00001300
             03 MDATEA  PIC X.                                          00001310
             03 MDATEC  PIC X.                                          00001320
             03 MDATEP  PIC X.                                          00001330
             03 MDATEH  PIC X.                                          00001340
             03 MDATEV  PIC X.                                          00001350
             03 MDATEO  PIC X(10).                                      00001360
           02 DFHMS5 OCCURS   15 TIMES .                                00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MCETATA      PIC X.                                     00001390
             03 MCETATC PIC X.                                          00001400
             03 MCETATP PIC X.                                          00001410
             03 MCETATH PIC X.                                          00001420
             03 MCETATV PIC X.                                          00001430
             03 MCETATO      PIC X.                                     00001440
           02 DFHMS6 OCCURS   15 TIMES .                                00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MCOMMENTA    PIC X.                                     00001470
             03 MCOMMENTC    PIC X.                                     00001480
             03 MCOMMENTP    PIC X.                                     00001490
             03 MCOMMENTH    PIC X.                                     00001500
             03 MCOMMENTV    PIC X.                                     00001510
             03 MCOMMENTO    PIC X(30).                                 00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLIBERRA  PIC X.                                          00001540
           02 MLIBERRC  PIC X.                                          00001550
           02 MLIBERRP  PIC X.                                          00001560
           02 MLIBERRH  PIC X.                                          00001570
           02 MLIBERRV  PIC X.                                          00001580
           02 MLIBERRO  PIC X(78).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MCODTRAA  PIC X.                                          00001610
           02 MCODTRAC  PIC X.                                          00001620
           02 MCODTRAP  PIC X.                                          00001630
           02 MCODTRAH  PIC X.                                          00001640
           02 MCODTRAV  PIC X.                                          00001650
           02 MCODTRAO  PIC X(4).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCICSA    PIC X.                                          00001680
           02 MCICSC    PIC X.                                          00001690
           02 MCICSP    PIC X.                                          00001700
           02 MCICSH    PIC X.                                          00001710
           02 MCICSV    PIC X.                                          00001720
           02 MCICSO    PIC X(5).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNETNAMA  PIC X.                                          00001750
           02 MNETNAMC  PIC X.                                          00001760
           02 MNETNAMP  PIC X.                                          00001770
           02 MNETNAMH  PIC X.                                          00001780
           02 MNETNAMV  PIC X.                                          00001790
           02 MNETNAMO  PIC X(8).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MSCREENA  PIC X.                                          00001820
           02 MSCREENC  PIC X.                                          00001830
           02 MSCREENP  PIC X.                                          00001840
           02 MSCREENH  PIC X.                                          00001850
           02 MSCREENV  PIC X.                                          00001860
           02 MSCREENO  PIC X(4).                                       00001870
                                                                                
