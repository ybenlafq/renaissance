      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2032                  *        
      ******************************************************************        
       01  RVXM2029.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2029-REFID            USAGE SQL TYPE IS ROWID.                  
      *--                                                                       
           10 XM2029-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2029-NCODIC           PIC X(07).                                
           10 XM2029-CDOSSIER         PIC X(05).                                
           10 XM2029-NSEQDOS          PIC X(05).                                
           10 XM2029-DEFFET           PIC X(08).                                
           10 XM2029-DFINEFFET        PIC X(08).                                
      ******************************************************************        
      * FLAGS                                                          *        
      ******************************************************************        
       01  RVXM2029-F.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2029-NCODIC-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2029-NCODIC-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2029-CDOSSIER-F           PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2029-CDOSSIER-F           PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2029-NSEQDOS-F            PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2029-NSEQDOS-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2029-DEFFET-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2029-DEFFET-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2029-DFINEFFET-F          PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2029-DFINEFFET-F          PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
                                                                                
