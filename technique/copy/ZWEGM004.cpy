      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ZWORK-COMPARAISON.                                                   
           05  ZWC-RETCODE                 PIC X.                               
           05  ZWC-TYPCHAMP1               PIC X.                               
           05  ZWC-LGCHAMP1                PIC S999   COMP-3.                   
           05  ZWC-DECCHAMP1               PIC S999   COMP-3.                   
           05  ZWC-CONTENU1                PIC X(64).                           
           05  ZWC-COMPARAISON             PIC XX.                              
           05  ZWC-TYPCHAMP2               PIC X.                               
           05  ZWC-LGCHAMP2                PIC S999   COMP-3.                   
           05  ZWC-DECCHAMP2               PIC S999   COMP-3.                   
           05  ZWC-CONTENU2                PIC X(64).                           
                                                                                
