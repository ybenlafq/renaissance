      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * PROJET : SRP                                                            
      * DECRIPTION DES LIGNES D'ETAT                                            
      * MEME DESCRIPTION POUR TOUS LES ETATS - ZONES DE TITRE VARIABLES         
      *--> POUR ETATS ISP030 - ISP031 ET ISP032                                 
      *****************************************************************         
      *                                                                         
       01 ETAT-ISP030.                                                          
      *----------------------------------------------------------------         
      * LIGNES ENTETE                                                           
      *----------------------------------------------------------------         
      *                                                                         
      *--------------------------- LIGNE E00  --------------------------        
       02 ISP030-E00.                                                           
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(16) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E01  --------------------------        
       02 ISP030-E01.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 ISP030-NOMETAT      PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(51) VALUE SPACES.                        
          05 FILLER              PIC X(11) VALUE 'EDITE LE : '.                 
          05 ISP030-DEDITE       PIC X(10) VALUE SPACES.                        
          05  FILLER             PIC X(02) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E02  --------------------------        
       02 ISP030-E02.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(55) VALUE SPACES.                        
          05 FILLER              PIC X(07) VALUE 'PAGE : '.                     
          05 ISP030-NPAGE        PIC ZZZ9  VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E03  --------------------------        
       02 ISP030-E03.                                                           
          05 FILLER          PIC X(52) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'FACTURATION INTERNE.'.            
          05 FILLER          PIC X(42) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E04  --------------------------        
       02 ISP030-E04.                                                           
          05 FILLER          PIC X(41) VALUE SPACES.                            
          05 FILLER          PIC X(17) VALUE 'ETAT DETAILLE DE '.               
          05 FILLER          PIC X(18) VALUE 'L''ECART CUMULE SRP'.             
          05 FILLER          PIC X(10) VALUE ' / PRMP.  '.                      
          05 FILLER          PIC X(46) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E05  --------------------------        
       02 ISP030-E05.                                                           
          05 FILLER          PIC X(50) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'TABLEAU DE SYNTHESE '.            
          05 ISP030-TYPSYNT  PIC X(46) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E06  --------------------------        
       02 ISP030-E06.                                                           
          05 FILLER          PIC X(55) VALUE SPACES.                            
          05 FILLER          PIC X(14) VALUE 'MOIS TRAITE : '.                  
          05 ISP030-MOISTRT  PIC X(05) VALUE SPACES.                            
          05 FILLER          PIC X(58) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E10  --------------------------        
       02 ISP030-E10.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE EMETTRICE  : '.         
          05 ISP030-NSOCEMET   PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP030-LSOCEMET   PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E11  --------------------------        
       02 ISP030-E11.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE RECEPTRICE : '.         
          05 ISP030-NSOCRECEP  PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP030-LSOCRECEP  PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E15  --------------------------        
       02 ISP030-E15.                                                           
          05 FILLER            PIC X(13) VALUE '    MARQUE : '.                 
          05 ISP030-CMARQ      PIC X(05) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP030-LMARQ      PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(15) VALUE '   EXERCICE DU '.               
          05 ISP030-DEBEXER.                                                    
             07 ISP030-DEBEXER-JJ   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP030-DEBEXER-MM   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP030-DEBEXER-AA   PIC X(02) VALUE SPACES.                     
          05 FILLER            PIC X(04) VALUE ' AU '.                          
          05 ISP030-FINEXER.                                                    
             07 ISP030-FINEXER-JJ   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP030-FINEXER-MM   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP030-FINEXER-AA   PIC X(02) VALUE SPACES.                     
          05 FILLER            PIC X(53) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E16  --------------------------        
       02 ISP030-E16.                                                           
          05 FILLER            PIC X(40) VALUE SPACES.                          
          05 FILLER            PIC X(19) VALUE 'ECHEANCE D''AVOIR : '.          
          05 ISP030-DAVOIR.                                                     
             07 ISP030-DAVOIR-JJ  PIC X(02) VALUE SPACES.                       
             07 FILLER            PIC X(01) VALUE '/'.                          
             07 ISP030-DAVOIR-MM  PIC X(02) VALUE SPACES.                       
             07 FILLER            PIC X(01) VALUE '/'.                          
             07 ISP030-DAVOIR-AA  PIC X(02) VALUE SPACES.                       
          05 FILLER            PIC X(65) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E20  --------------------------        
       02 ISP030-E20.                                                           
          05 FILLER            PIC X(20) VALUE '                    '.          
          05 FILLER            PIC X(20) VALUE '     .--------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(16) VALUE '-----------.    '.              
      *                                                                         
      *--------------------------- LIGNE E21  --------------------------        
       02 ISP030-E21.                                                           
          05 FILLER                         PIC X(10) VALUE SPACES.             
          05 FILLER                         PIC X(49) VALUE                     
                  '               !       VALO SRP         VALO PRMP'.          
          05 FILLER                         PIC X(57) VALUE                     
           '                      ECART SRP / PRMP                   '.         
          05 FILLER                         PIC X(16) VALUE                     
           '           !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E22  --------------------------        
        02 ISP030-E22.                                                          
           05 FILLER                         PIC X(58) VALUE                    
           '                         !                                '.        
           05 FILLER                         PIC X(58) VALUE                    
           '              H.T.       TAUX           T.V.A.            '.        
           05 FILLER                         PIC X(16) VALUE                    
           ' T.T.C.    !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E23  --------------------------        
        02  ISP030-E23.                                                         
           05 FILLER                         PIC X(58) VALUE                    
           '  .----------------------!--------------------------------'.        
           05 FILLER                         PIC X(58) VALUE                    
           '----+-----------------+--------+-----------------+--------'.        
           05 FILLER                         PIC X(16) VALUE                    
           '-----------!    '.                                                  
      *                                                                         
      *--------------------------- LIGNE D01  --------------------------        
        02 ISP030-D01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP030-TEBRB.                                                     
              10 ISP030-TEBRB-COM   PIC X(05) VALUE SPACES.                     
              10 ISP030-TEBRB-VAL   PIC X(15) VALUE SPACES.                     
           05 FILLER                PIC X(02) VALUE ' !'.                       
           05 ISP030-VALOSRP        PIC ------------9,99.                       
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-VALOPRMP       PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-ECARTHT        PIC -----------9,99.                        
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-TXTVA          PIC Z9,99.                                  
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-ECARTTVA       PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-ECARTTTC       PIC ------------9,99.                       
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T01  --------------------------        
        02 ISP030-T01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP030-TYPE.                                                      
              10 ISP030-TYPE-COM    PIC X(11) VALUE SPACES.                     
              10 ISP030-TYPE-VAL    PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(02) VALUE ' !'.                       
           05 ISP030-TOTSRP         PIC ------------9,99.                       
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-TOTPRMP        PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-TOTHT          PIC -----------9,99.                        
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-TOTTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-TOTTVA         PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-TOTTTC         PIC ------------9,99.                       
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T10  --------------------------        
        02 ISP030-T10.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP030-TYPE10.                                                    
              10 ISP030-TYPE10-COM  PIC X(11) VALUE SPACES.                     
              10 ISP030-TYPE10-VAL  PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(02) VALUE ' !'.                       
           05 ISP030-GENSRP         PIC ------------9,99.                       
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-GENPRMP        PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-GENHT          PIC -----------9,99.                        
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-GENTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP030-GENTVA         PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP030-GENTTC         PIC ------------9,99.                       
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE B01  --------------------------        
           02  ISP030-B01.                                                      
               05  FILLER                         PIC X(48) VALUE               
           '  ''---------------------------------------------'.                 
               05  FILLER   PIC X(10) VALUE '----------'.                       
               05  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               05  FILLER                         PIC X(16) VALUE               
           '-----------''    '.                                                 
      *                                                                         
      *----------------------- LIGNE VIDE DANS TABLEAU -----------------        
           02  ISP030-TAB-VIDE.                                                 
               05  FILLER                         PIC X(58) VALUE               
           '  !                      !                                '.        
               05  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               05  FILLER                         PIC X(16) VALUE               
           '           !    '.                                                  
      *                                                                         
      *                                                                         
      *--------------------------- LIGNE E23  --------------------------        
        02  ISP030-RECAP.                                                       
           05 FILLER                         PIC X(58) VALUE                    
           '  !-- SYNTHESE / MOIS ---!--------------------------------'.        
           05 FILLER                         PIC X(58) VALUE                    
           '----+-----------------+--------+-----------------+--------'.        
           05 FILLER                         PIC X(16) VALUE                    
           '-----------!    '.                                                  
                                                                                
