      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                                         
      * TRAITEMENT DES VALEURS DECIMALES                                        
      *                                                                         
      *****************************************************************         
       TRAITEMENT-DECIM SECTION.                                                
      *****************************************************************         
      *                                                                         
       DEB-TRAITEMENT-DECI.                                                     
      *                                                                         
           IF  W-DECI-USER NOT = SPACES                                         
               PERFORM T-DECI-USER                                              
           ELSE                                                                 
               PERFORM T-DECI-FILE                                              
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-DECI. EXIT.                                               
      *                                                                         
      *****************************************************************         
       T-DECI-USER SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-DECI-USER.                                                         
      *                                                                         
           MOVE 5 TO CODE-RETOUR.                                               
      *                                                                         
           MOVE   1   TO W-DECI-I.                                              
           MOVE   0   TO W-DECI-FILE.                                           
           MOVE   1   TO W-DECI-POIDS.                                          
           MOVE SPACE TO W-DECI-FLAG.                                           
      *                                                                         
       T-DECI-RECH-DEB.                                                         
           IF W-DECI-USER-CAR (W-DECI-I) = SPACE                                
              ADD 1 TO W-DECI-I                                                 
              GO TO T-DECI-RECH-DEB                                             
           END-IF.                                                              
      *                                                                         
           IF W-DECI-USER-CAR (W-DECI-I) = '+'                                  
              MOVE '+' TO W-DECI-SIGNE                                          
              ADD 1 TO W-DECI-I                                                 
           ELSE                                                                 
              IF W-DECI-USER-CAR (W-DECI-I) = '-'                               
                 MOVE '-' TO W-DECI-SIGNE                                       
                 ADD 1 TO W-DECI-I                                              
              ELSE                                                              
                 IF W-DECI-USER-CAR (W-DECI-I) NUMERIC                          
                    MOVE '+' TO W-DECI-SIGNE                                    
                 ELSE                                                           
                 IF W-DECI-USER-CAR (W-DECI-I) = ','                            
                    COMPUTE W-DECI-I = W-DECI-I - 1                             
                    MOVE '0' TO W-DECI-USER-CAR (W-DECI-I)                      
                 ELSE                                                           
      * ERREUR...                                                               
                    GO TO FIN-T-DECI-USER                                       
                 END-IF                                                         
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       T-DECI-RECH-VIRG.                                                        
           IF  W-DECI-I > W-DECI-LONG                                           
               GO TO T-DECI-MISE-EN-FORME                                       
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) NUMERIC                               
               MOVE '*' TO W-DECI-FLAG                                          
               COMPUTE W-DECI-FILE = (10 * W-DECI-FILE)                         
                                   + W-DECI-USER-CAR9 (W-DECI-I)                
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-RECH-VIRG                                           
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) = ','                                 
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-DECIMALES                                           
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) = SPACE                               
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-FIN                                                 
           END-IF.                                                              
      * ERREUR...                                                               
           GO TO FIN-T-DECI-USER.                                               
      *                                                                         
       T-DECI-DECIMALES.                                                        
           IF  W-DECI-I > W-DECI-LONG                                           
               GO TO T-DECI-MISE-EN-FORME                                       
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) NUMERIC                               
               COMPUTE W-DECI-POIDS = W-DECI-POIDS / 10                         
               COMPUTE W-DECI-FILE = W-DECI-FILE                                
                       + W-DECI-USER-CAR9 (W-DECI-I) * W-DECI-POIDS             
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-DECIMALES                                           
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) = SPACE                               
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-FIN                                                 
           END-IF.                                                              
      * ERREUR...                                                               
           GO TO FIN-T-DECI-USER.                                               
      *                                                                         
       T-DECI-FIN.                                                              
           IF  W-DECI-I > W-DECI-LONG                                           
               GO TO T-DECI-MISE-EN-FORME                                       
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-USER-CAR (W-DECI-I) = SPACE                               
               ADD 1 TO W-DECI-I                                                
               GO TO T-DECI-FIN                                                 
           END-IF.                                                              
      * ERREUR...                                                               
           GO TO FIN-T-DECI-USER.                                               
      *                                                                         
       T-DECI-MISE-EN-FORME.                                                    
           IF  W-DECI-FLAG = SPACE                                              
      * ERREUR...                                                               
               GO TO FIN-T-DECI-USER                                            
           END-IF.                                                              
      *                                                                         
           IF  W-DECI-SIGNE = '-'                                               
               COMPUTE W-DECI-FILE = 0 - W-DECI-FILE                            
           END-IF                                                               
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
       FIN-T-DECI-USER. EXIT.                                                   
      *                                                                         
      *****************************************************************         
       T-DECI-FILE SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-DECI-FILE.                                                         
      *                                                                         
           MOVE W-DECI-FILE TO W-DECI-EDIT.                                     
      * SUPPRESSION DES ZEROS DE LA FIN                                         
           MOVE   20  TO W-DECI-I.                                              
       T-DECI-SERRE-A-DROITE.                                                   
           IF W-DECI-EDIT-CAR (W-DECI-I) = '0'                                  
              MOVE SPACE TO W-DECI-EDIT-CAR (W-DECI-I)                          
              SUBTRACT 1 FROM W-DECI-I                                          
              GO TO T-DECI-SERRE-A-DROITE                                       
           END-IF.                                                              
      * SUPPRESSION DE LA VIRGULE SI C'EST LE DERNIER CARACTERE                 
           IF W-DECI-EDIT-CAR (W-DECI-I) = ','                                  
              MOVE SPACE TO W-DECI-EDIT-CAR (W-DECI-I)                          
           END-IF.                                                              
      * SUPPRESSION DES BLANCS DE TETE                                          
       T-DECI-SERRE-A-GAUCHE.                                                   
           IF W-DECI-EDIT-CAR (1) = SPACE                                       
              MOVE W-DECI-REEDIT  TO W-DECI-TAMPON                              
              MOVE W-DECI-TAMPON2 TO W-DECI-REEDIT                              
              GO TO T-DECI-SERRE-A-GAUCHE                                       
           END-IF.                                                              
      *                                                                         
           MOVE W-DECI-REEDIT  TO W-DECI-USER.                                  
      *                                                                         
       FIN-T-DECI-FILE. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
