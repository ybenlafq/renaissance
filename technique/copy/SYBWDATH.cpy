      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      * ZONES DATE/HEURE DU DEBUT DE PROGRAMME                          00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       01  FILLER PIC X(16) VALUE '* ZONE-TIMER *'.                     00000500
      *                                                                 00000600
       01  FILLER.                                                      00000700
      *                                                                 00000800
         02  Z-TIMER.                                                   00000900
      *                                                                 00001000
           05  Z-TIMER-DATJOU-FILE.                                     00001100
             10  SS                           PIC 9(02) VALUE 0.        00001200
             10  AA                           PIC 9(02) VALUE 0.        00001300
             10  MM                           PIC 9(02) VALUE 0.        00001400
             10  JJ                           PIC 9(02) VALUE 0.        00001500
      *                                                                 00001600
           05  Z-TIMER-DATJOU.                                          00001700
             10  JJ                           PIC 9(02) VALUE 0.        00001800
             10  FILLER                       PIC X     VALUE '/'.      00001900
             10  MM                           PIC 9(02) VALUE 0.        00002000
             10  FILLER                       PIC X     VALUE '/'.      00002100
             10  AA                           PIC 9(02) VALUE 0.        00002200
      *                                                                 00002300
           05  Z-TIMER-TIMJOU.                                          00002400
             10  HH                           PIC 9(02) VALUE  0.       00002500
             10  FILLER                       PIC X     VALUE 'H'.      00002600
             10  MM                           PIC 9(02) VALUE  0.       00002700
             10  FILLER                       PIC X     VALUE 'M'.      00002800
             10  SS                           PIC 9(02) VALUE  0.       00002900
             10  FILLER                       PIC X     VALUE 'S'.      00003000
           05  Z-HHMMSSCC.                                              00003101
             10  Z-TIMER-HHMMSSCC             PIC 9(8).                 00003200
           05  FILLER REDEFINES Z-HHMMSSCC.                             00003301
             10  FILLER                       OCCURS 4.                 00003400
               15  Z-TIMER-TIMJOU-CAR         PIC 99.                   00003500
      *                                                                 00003600
                                                                                
