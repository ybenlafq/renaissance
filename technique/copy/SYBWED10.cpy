      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      * ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DL/1            00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       01  FILLER.                                                      00000500
      *                                                                 00000600
           05  TRACE-DL1-MESSAGE.                                       00000700
      *                                                                 00000800
             10  FILLER         PIC  X(4)   VALUE 'PSB='.               00000900
             10  ETAT-PSB       PIC  X      VALUE '*'.                  00001000
               88  PSB-NOT-OK               VALUE '*'.                  00001100
               88  PSB-OK                   VALUE 'K'.                  00001200
      *                                                                 00001300
             10  FILLER         PIC  X(6)   VALUE ' FUNC='.             00001400
      *                                                                 00001500
             10  FUNCTON       PIC  X(4)   VALUE SPACES.                00001600
               88  GU                       VALUE 'GU  '.               00001700
               88  GN                       VALUE 'GN  '.               00001800
               88  GNP                      VALUE 'GNP '.               00001900
               88  ISRT                     VALUE 'ISRT'.               00002000
               88  REPL                     VALUE 'REPL'.               00002100
               88  DLET                     VALUE 'DLET'.               00002200
               88  PCB                      VALUE 'PCB '.               00002300
               88  CHKP                     VALUE 'CHKP'.               00002400
               88  INPUT-DL1                VALUE 'GU  ' 'GN  ' 'GNP '. 00002500
               88  OUTPUT-DL1               VALUE 'ISRT' 'REPL' 'DLET'. 00002600
      *                                                                 00002700
             10  FILLER         PIC  X(5)   VALUE ' PCB='.              00002800
             10  DL1-NUM-PCB    PIC  9      VALUE 0.                    00002900
      *                                                                 00003000
             10  FILLER         PIC  X(6)   VALUE ' RETC='.             00003100
             10  TRACE-DL1-RC   PIC  X(2)   VALUE '**'.                 00003200
      *                                                                 00003300
             10  FILLER           PIC  X(4)   VALUE ' SSA'.             00003400
             10  TRACE-DL1-NOMBRE PIC  9      VALUE  0.                 00003500
             10  FILLER           PIC  X      VALUE '='.                00003600
             10  TRACE-DL1-SSA    PIC  X(44)  VALUE ALL '*'.            00003700
             10  FILLER REDEFINES TRACE-DL1-SSA.                        00003800
                 15 TRACE-DL1-DECOUPE PIC X OCCURS 1.                   00003900
      *                                                                 00004000
           05  TRACE-DL1-WORKAREA.                                      00004100
      *                                                                 00004200
             10  TRACE-DL1-I    PIC  S9(5) COMP-3 VALUE +0.             00004300
      *                                                                 00004501
      ***************************************************************** 00004601
      *          ZONES D'INTERFACE POUR LES TRACES EXEC DLI             00004701
      ***************************************************************** 00004801
           05  TRACE-EDLI-MESSAGE.                                      00004901
      *                                                                 00005001
             10  FILLER         PIC  X(4)   VALUE 'PSB='.               00005101
             10  ETAT-PSB-EDLI  PIC  X      VALUE '*'.                  00005201
      *                                                                 00005301
             10  FILLER         PIC  X(6)   VALUE ' FUNC='.             00005401
      *                                                                 00005501
             10  TRACE-EDLI-FUNCTION    PIC  X(4)   VALUE SPACES.       00005601
      *                                                                 00006001
             10  FILLER         PIC  X(5)   VALUE ' PCB='.              00006101
             10  TRACE-EDLI-PCB PIC  9      VALUE 0.                    00006201
      *                                                                 00006301
             10  FILLER         PIC  X(6)   VALUE ' RETC='.             00006401
             10  TRACE-EDLI-CODE PIC  X(2)   VALUE '**'.                00006501
      *                                                                 00006601
             10  FILLER           PIC  X(4)   VALUE ' SEG'.             00006701
             10  FILLER           PIC  X      VALUE '='.                00006801
             10  TRACE-EDLI-SEG   PIC  X(08)  VALUE SPACES.             00006901
      *                                                                 00007001
             10  FILLER           PIC  X(4)   VALUE ' KEY'.             00007101
             10  FILLER           PIC  X      VALUE '='.                00007201
             10  TRACE-EDLI-KEY   PIC  X(08)  VALUE SPACES.             00007301
      *                                                                 00007401
             10  FILLER           PIC  X(4)   VALUE ' VAL'.             00007501
             10  FILLER           PIC  X      VALUE '='.                00007601
             10  TRACE-EDLI-VAL   PIC  X(30)  VALUE SPACES.             00007701
      *                                                                 00007801
      ***************************************************************   00009000
      *                                                                 00009100
           05  FILLER-CADRAGE   PIC  X(2)  VALUE SPACE.                 00009200
      *                                                                 00009300
      ***************************************************************   00009400
      *                                                                 00009500
           05  FILLER.                                                  00009600
      *                                                                 00009700
             10  FILLER         PIC  X(8)   VALUE 'DL1-PSB.'.           00009800
             10  NOM-PSB        PIC  X(8)   VALUE SPACE.                00009900
      *                                                                 00010000
             10  FILLER      PIC  X(12)      VALUE 'DL1-NOMBRE..'.      00010100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  NOMBRE      PIC  S9(8) COMP VALUE +0.                  00010200
      *--                                                                       
             10  NOMBRE      PIC  S9(8) COMP-5 VALUE +0.                        
      *}                                                                        
      *                                                                 00010300
           05  FILLER.                                                  00010400
      *                                                                 00010500
             10  FILLER      PIC  X(15)  VALUE 'DL1-FCTR.......'.       00010600
             10  DL1-FCTR    PIC  X      VALUE LOW-VALUE.               00010700
      *                                                                 00010800
           05  FILLER.                                                  00010900
      *                                                                 00011000
             10  FILLER      PIC  X(15)  VALUE 'DL1-DLTR.......'.       00011100
             10  DL1-DLTR    PIC  X      VALUE LOW-VALUE.               00011200
      *                                                                 00011300
           05  FILLER.                                                  00011400
      *                                                                 00011500
             10  FILLER      PIC  X(15)  VALUE 'DL1-LEVEL......'.       00011600
             10  CODE-LEVEL  PIC  X      VALUE '0'.                     00011700
               88 HIGH-LEVEL             VALUE '1'.                     00011800
               88 SAME-LEVEL             VALUE '2'.                     00011900
      *                                                                 00012000
           05  FILLER        PIC  X(16)  VALUE 'DL1-PCB-ENCOURS.'.      00012100
           05  SAVE-PCB.                                                00012200
             10 DBD-NAME             PIC X(8).                          00012300
             10 SEG-LEVEL            PIC X(2).                          00012400
             10 PCB-STATUS-CODE      PIC X(2).                          00012500
               88 BB                 VALUE '  '.                        00012600
               88 GA                 VALUE 'GA'.                        00012700
               88 GB                 VALUE 'GB'.                        00012800
               88 GE                 VALUE 'GE'.                        00012900
               88 GK                 VALUE 'GK'.                        00013000
               88 II                 VALUE 'II'.                        00013100
               88 NI                 VALUE 'NI'.                        00013200
               88 XH                 VALUE 'XH'.                        00013300
             10 PROC-OPTIONS         PIC X(4).                          00013400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 FILLER               PIC S9(5) COMP.                    00013500
      *--                                                                       
             10 FILLER               PIC S9(5) COMP-5.                          
      *}                                                                        
             10 SEG-NAME-FB          PIC X(8).                          00013600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 LENGTH-FB-KEY        PIC S9(5) COMP.                    00013700
      *--                                                                       
             10 LENGTH-FB-KEY        PIC S9(5) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 NUMB-SENS-SEGS       PIC S9(5) COMP.                    00013800
      *--                                                                       
             10 NUMB-SENS-SEGS       PIC S9(5) COMP-5.                          
      *}                                                                        
             10 KEY-FB-AREA          PIC X(60).                         00013900
      *                                                                 00014000
           05  FILLER.                                                  00014100
      *                                                                 00014200
             10  FUNC-GU        PIC  X(4)   VALUE 'GU  '.               00014300
             10  FUNC-GN        PIC  X(4)   VALUE 'GN  '.               00014400
             10  FUNC-GNP       PIC  X(4)   VALUE 'GNP '.               00014500
             10  FUNC-DLET      PIC  X(4)   VALUE 'DLET'.               00014600
             10  FUNC-REPL      PIC  X(4)   VALUE 'REPL'.               00014700
             10  FUNC-ISRT      PIC  X(4)   VALUE 'ISRT'.               00014800
             10  FUNC-PCB       PIC  X(4)   VALUE 'PCB '.               00014900
             10  FUNC-CHKP      PIC  X(4)   VALUE 'CHKP'.               00015000
             10  FILLER         PIC  X(8)   VALUE SPACE.                00015100
      ***************************************************************   00015200
       01  FILLER-EXEC.                                                 00015300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NUM-PCB          PIC S9(4) COMP VALUE +0.                00015400
      *--                                                                       
           05  NUM-PCB          PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-1       PIC S9(4) COMP VALUE +0.                00015500
      *--                                                                       
           05  SEG-LONG-1       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-2       PIC S9(4) COMP VALUE +0.                00015600
      *--                                                                       
           05  SEG-LONG-2       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-3       PIC S9(4) COMP VALUE +0.                00015700
      *--                                                                       
           05  SEG-LONG-3       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-4       PIC S9(4) COMP VALUE +0.                00015800
      *--                                                                       
           05  SEG-LONG-4       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-5       PIC S9(4) COMP VALUE +0.                00015900
      *--                                                                       
           05  SEG-LONG-5       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-6       PIC S9(4) COMP VALUE +0.                00016000
      *--                                                                       
           05  SEG-LONG-6       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-7       PIC S9(4) COMP VALUE +0.                00016100
      *--                                                                       
           05  SEG-LONG-7       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-8       PIC S9(4) COMP VALUE +0.                00016200
      *--                                                                       
           05  SEG-LONG-8       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SEG-LONG-9       PIC S9(4) COMP VALUE +0.                00016300
      *--                                                                       
           05  SEG-LONG-9       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-1       PIC S9(4) COMP VALUE +0.                00016400
      *--                                                                       
           05  KEY-LONG-1       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-2       PIC S9(4) COMP VALUE +0.                00016500
      *--                                                                       
           05  KEY-LONG-2       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-3       PIC S9(4) COMP VALUE +0.                00017000
      *--                                                                       
           05  KEY-LONG-3       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-4       PIC S9(4) COMP VALUE +0.                00018000
      *--                                                                       
           05  KEY-LONG-4       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-5       PIC S9(4) COMP VALUE +0.                00019000
      *--                                                                       
           05  KEY-LONG-5       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-6       PIC S9(4) COMP VALUE +0.                00020000
      *--                                                                       
           05  KEY-LONG-6       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-7       PIC S9(4) COMP VALUE +0.                00030000
      *--                                                                       
           05  KEY-LONG-7       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-8       PIC S9(4) COMP VALUE +0.                00040000
      *--                                                                       
           05  KEY-LONG-8       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  KEY-LONG-9       PIC S9(4) COMP VALUE +0.                00050000
      *--                                                                       
           05  KEY-LONG-9       PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
           05  KEY-VAL-1        PIC X(30).                              01310000
           05  KEY-VAL-2        PIC X(30).                              01320000
           05  KEY-VAL-3        PIC X(30).                              01330000
           05  KEY-VAL-4        PIC X(30).                              01340000
           05  KEY-VAL-5        PIC X(30).                              01350000
           05  KEY-VAL-6        PIC X(30).                              01360000
           05  KEY-VAL-7        PIC X(30).                              01370000
           05  KEY-VAL-8        PIC X(30).                              01380000
           05  KEY-VAL-9        PIC X(30).                              01390000
           05  SEG-NAME-1       PIC X(8).                               01400000
           05  SEG-NAME-2       PIC X(8).                               01410000
           05  SEG-NAME-3       PIC X(8).                               01420000
           05  SEG-NAME-4       PIC X(8).                               01430000
           05  SEG-NAME-5       PIC X(8).                               01440000
           05  SEG-NAME-6       PIC X(8).                               01450000
           05  SEG-NAME-7       PIC X(8).                               01460000
           05  SEG-NAME-8       PIC X(8).                               01470000
           05  SEG-NAME-9       PIC X(8).                               01480000
           05  KEY-NAME-1       PIC X(8).                               01490000
           05  KEY-NAME-2       PIC X(8).                               01500000
           05  KEY-NAME-3       PIC X(8).                               01510000
           05  KEY-NAME-4       PIC X(8).                               01520000
           05  KEY-NAME-5       PIC X(8).                               01530000
           05  KEY-NAME-6       PIC X(8).                               01540000
           05  KEY-NAME-7       PIC X(8).                               01550000
           05  KEY-NAME-8       PIC X(8).                               01560000
           05  KEY-NAME-9       PIC X(8).                               01570000
      *                                                                 01590000
                                                                                
