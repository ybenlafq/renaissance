      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  PASSAGE DU CONTROL A UN PROGRAMME DE LA MEME TACHE             00000200
      ****************************************************************  00000300
      *                                                                 00000400
       LINK-PROG           SECTION.                                     00000500
      *                                                                 00000600
      *****************************************************************         
      *    POUR CICS ETUDES, APPEL AU MODULE QUI CHANGE LE CODE CICS            
      *                      MODIFIE PAR ETD5                                   
      *****************************************************************         
      *    IF EIBCALEN = ZERO                                                   
      *     EXEC CICS LINK PROGRAM('MAPPLI')                                    
      *                   COMMAREA(Z-COMMAREA)                                  
      *                   LENGTH(LENGTH OF Z-COMMAREA)                          
      *                   NOHANDLE                                              
      *          END-EXEC                                                       
      *    END-IF.                                                              
      *****************************************************************         
      *                                                                 00000800
      *    MOVE '*' TO ETAT-PSB.                                        00000701
      *                                                                 00000800
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS LINK  PROGRAM  (NOM-PROG-LINK)                                 
                       COMMAREA (Z-COMMAREA-LINK)                               
                       LENGTH   (LONG-COMMAREA-LINK)                            
                       NOHANDLE                                                 
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*      MOVE '�00024   ' TO DFHEIV0                              
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-PROG-LINK Z-COMMAREA-        
      *dfhei*     LONG-COMMAREA-LINK.                                           
      *dfhei*} decommente EXEC                                                  
      *                                                                 00002000
           MOVE EIBRCODE TO EIB-RCODE.                                  00002100
           IF   NOT EIB-NORMAL                                          00002200
                GO TO ABANDON-CICS                                      00002300
           END-IF.                                                              
      *                                                                 00002400
       FIN-LINK. EXIT.                                                  00002500
           EJECT                                                                
                                                                                
                                                                                
