      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *****************************************************************         
      *                                                                         
      * VERIFICATION ET EDITION DES HEURES                                      
      *                                                                         
      *****************************************************************         
       TRAITEMENT-TIME SECTION.                                                 
      *****************************************************************         
      *                                                                         
       DEB-TRAITEMENT-TIME.                                                     
      *                                                                         
           MOVE 6 TO CODE-RETOUR.                                               
      *                                                                         
           IF W-TIME-USER NOT = SPACES                                          
                                   PERFORM T-TIME-USER                          
           ELSE IF W-TIME-FILE NOT = SPACES                                     
                                   PERFORM T-TIME-FILE                          
                 END-IF                                                         
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-TIME. EXIT.                                               
      *                                                                         
      *****************************************************************         
       T-TIME-USER SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-TIME-USER.                                                         
      *                                                                         
           MOVE '00' TO W-TIME-HH.                                              
           MOVE '00' TO W-TIME-MM.                                              
           MOVE '00' TO W-TIME-SS.                                              
      *                                                                         
           IF W-TIME-X (1) NOT NUMERIC                                          
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           IF W-TIME-X (2) NOT NUMERIC                                          
                           MOVE W-TIME-X (1) TO W-TIME-H2                       
                           MOVE 2 TO W-TIME-I                                   
                           GO TO T-TIME-T-H                                     
           END-IF.                                                              
           MOVE W-TIME-X (1) TO W-TIME-H1.                                      
           MOVE W-TIME-X (2) TO W-TIME-H2.                                      
           MOVE 3 TO W-TIME-I.                                                  
      *                                                                         
       T-TIME-T-H.                                                              
      *                                                                         
           IF W-TIME-X (W-TIME-I) = 'H'                                         
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-MM                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) = 'M'                                         
                           MOVE W-TIME-HH TO W-TIME-MM                          
                           MOVE '00'      TO W-TIME-HH                          
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-SS                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) = 'S'                                         
                           MOVE W-TIME-HH TO W-TIME-SS                          
                           MOVE '00'      TO W-TIME-HH                          
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           GO TO FIN-T-TIME-USER.                                               
      *                                                                         
       T-TIME-T-MM.                                                             
      *                                                                         
           IF W-TIME-X (W-TIME-I) = SPACE                                       
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) NOT NUMERIC                                   
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           MOVE W-TIME-I TO W-TIME-J.                                           
           ADD 1 TO W-TIME-I.                                                   
           IF W-TIME-X (W-TIME-I) NOT NUMERIC                                   
                           MOVE W-TIME-X (W-TIME-J) TO W-TIME-M2                
                           GO TO T-TIME-T-M                                     
           END-IF.                                                              
           MOVE W-TIME-X (W-TIME-J) TO W-TIME-M1.                               
           MOVE W-TIME-X (W-TIME-I) TO W-TIME-M2.                               
           ADD 1 TO W-TIME-I.                                                   
      *                                                                         
       T-TIME-T-M.                                                              
      *                                                                         
           IF W-TIME-X (W-TIME-I) = SPACE                                       
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) NOT = 'M'                                     
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           ADD 1 TO W-TIME-I.                                                   
      *                                                                         
       T-TIME-T-SS.                                                             
      *                                                                         
           IF W-TIME-X (W-TIME-I) = SPACE                                       
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) NOT NUMERIC                                   
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           MOVE W-TIME-I TO W-TIME-J.                                           
           ADD 1 TO W-TIME-I.                                                   
           IF W-TIME-X (W-TIME-I) NOT NUMERIC                                   
                           MOVE W-TIME-X (W-TIME-J) TO W-TIME-S2                
                           GO TO T-TIME-T-S                                     
           END-IF.                                                              
           MOVE W-TIME-X (W-TIME-J) TO W-TIME-S1.                               
           MOVE W-TIME-X (W-TIME-I) TO W-TIME-S2.                               
           ADD 1 TO W-TIME-I.                                                   
      *                                                                         
       T-TIME-T-S.                                                              
      *                                                                         
           IF W-TIME-X (W-TIME-I) = SPACE                                       
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) NOT = 'S'                                     
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           ADD 1 TO W-TIME-I.                                                   
      *                                                                         
       T-TIME-T-BL.                                                             
      *                                                                         
           IF W-TIME-I > 9                                                      
                           GO TO T-TIME-LOGIX                                   
           END-IF.                                                              
           IF W-TIME-X (W-TIME-I) = SPACE                                       
                           ADD 1 TO W-TIME-I                                    
                           GO TO T-TIME-T-BL                                    
           END-IF.                                                              
           GO TO FIN-T-TIME-USER.                                               
      *                                                                         
       T-TIME-LOGIX.                                                            
      *                                                                         
           IF W-TIME-HH > '23'                                                  
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           IF W-TIME-MM > '59'                                                  
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
           IF W-TIME-SS > '59'                                                  
                           GO TO FIN-T-TIME-USER                                
           END-IF.                                                              
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
       FIN-T-TIME-USER. EXIT.                                                   
      *                                                                         
      *****************************************************************         
       T-TIME-FILE SECTION.                                                     
      *****************************************************************         
      *                                                                         
       DEB-T-TIME-FILE.                                                         
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
           MOVE HH OF W-TIME-FILE TO HH OF W-TIME-USER.                         
           MOVE MM OF W-TIME-FILE TO MM OF W-TIME-USER.                         
           MOVE SS OF W-TIME-FILE TO SS OF W-TIME-USER.                         
           MOVE 'H'               TO S1 OF W-TIME-USER.                         
           MOVE 'M'               TO S2 OF W-TIME-USER.                         
           MOVE 'S'               TO S3 OF W-TIME-USER.                         
      *                                                                         
           IF SS OF W-TIME-USER = '00'                                          
              MOVE SPACE TO S3 OF W-TIME-USER                                   
              MOVE SPACE TO SS OF W-TIME-USER                                   
              MOVE SPACE TO S2 OF W-TIME-USER                                   
              IF MM OF W-TIME-USER = '00'                                       
                 MOVE SPACE TO MM OF W-TIME-USER                                
              END-IF                                                            
            END-IF.                                                             
      *                                                                         
       FIN-T-TIME-FILE. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
