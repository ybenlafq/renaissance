      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2019                  *        
      ******************************************************************        
       01  RVXM2019.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2019-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2019-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2019-NCODIC          PIC X(07).                                 
           10 XM2019-NEANCOMPO       PIC X(13).                                 
           10 XM2019-WACTIF          PIC X(01).                                 
           10 XM2019-DACTIF          PIC X(08).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2019-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2019-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2019-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2019-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2019-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2019-NEANCOMPO-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2019-NEANCOMPO-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2019-WACTIF-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2019-WACTIF-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2019-DACTIF-F        PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2019-DACTIF-F        PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
