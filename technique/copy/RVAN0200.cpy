      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
E0265 * DSA057 01/09/06 SUPPORT MAINTENANCE EVOLUTION                           
      *                 TABLE DE LOG                                            
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVAN0200.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVAN0200.                                                             
      *}                                                                        
           10 AN02-PROGID          PIC X(8).                                    
           10 AN02-TYPEID          PIC X(8).                                    
           10 AN02-APPLID          PIC X(8).                                    
           10 AN02-TERMID          PIC X(4).                                    
           10 AN02-USERID          PIC X(8).                                    
           10 AN02-DATA            PIC X(200).                                  
           10 AN02-DCREATION       PIC X(26).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVAN0200-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVAN0200-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-PROGID-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-PROGID-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-TYPEID-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-TYPEID-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-APPLID-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-APPLID-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-TERMID-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-TERMID-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-USERID-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-USERID-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-DATA-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AN02-DATA-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AN02-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AN02-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
