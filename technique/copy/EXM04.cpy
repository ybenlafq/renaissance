      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * parametrage de l"�lement de dtd                                 00000020
      ***************************************************************** 00000030
       01   EXM04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDTDL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCDTDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCDTDF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCDTDI    PIC X(20).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEQL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSEQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSEQF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSEQI    PIC X(6).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELEMENTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MELEMENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MELEMENTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MELEMENTI      PIC X(50).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELEMENTPL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MELEMENTPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MELEMENTPF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MELEMENTPI     PIC X(50).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUSEL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MUSEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MUSEF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MUSEI     PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAXOCCURSL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MMAXOCCURSL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MMAXOCCURSF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MMAXOCCURSI    PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELEMNTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MTELEMNTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTELEMNTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTELEMNTI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELTLEGL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MELTLEGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MELTLEGF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MELTLEGI  PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCOBOLL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTCOBOLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCOBOLF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTCOBOLI  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRLFLUXL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCTRLFLUXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTRLFLUXF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCTRLFLUXI     PIC X.                                     00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGCOBOLL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLGCOBOLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLGCOBOLF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLGCOBOLI      PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBDECL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNBDECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBDECF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNBDECI   PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVALEURL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLVALEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLVALEURF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLVALEURI      PIC X(22).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALEUR1L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MVALEUR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVALEUR1F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MVALEUR1I      PIC X(50).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALEUR2L      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MVALEUR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVALEUR2F      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MVALEUR2I      PIC X(50).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALEUR3L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MVALEUR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVALEUR3F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MVALEUR3I      PIC X(50).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCICSI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * parametrage de l"�lement de dtd                                 00000990
      ***************************************************************** 00001000
       01   EXM04O REDEFINES EXM04I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTIMJOUA  PIC X.                                          00001110
           02 MTIMJOUC  PIC X.                                          00001120
           02 MTIMJOUP  PIC X.                                          00001130
           02 MTIMJOUH  PIC X.                                          00001140
           02 MTIMJOUV  PIC X.                                          00001150
           02 MTIMJOUO  PIC X(5).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCDTDA    PIC X.                                          00001180
           02 MCDTDC    PIC X.                                          00001190
           02 MCDTDP    PIC X.                                          00001200
           02 MCDTDH    PIC X.                                          00001210
           02 MCDTDV    PIC X.                                          00001220
           02 MCDTDO    PIC X(20).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNSEQA    PIC X.                                          00001250
           02 MNSEQC    PIC X.                                          00001260
           02 MNSEQP    PIC X.                                          00001270
           02 MNSEQH    PIC X.                                          00001280
           02 MNSEQV    PIC X.                                          00001290
           02 MNSEQO    PIC X(6).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MELEMENTA      PIC X.                                     00001320
           02 MELEMENTC PIC X.                                          00001330
           02 MELEMENTP PIC X.                                          00001340
           02 MELEMENTH PIC X.                                          00001350
           02 MELEMENTV PIC X.                                          00001360
           02 MELEMENTO      PIC X(50).                                 00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MELEMENTPA     PIC X.                                     00001390
           02 MELEMENTPC     PIC X.                                     00001400
           02 MELEMENTPP     PIC X.                                     00001410
           02 MELEMENTPH     PIC X.                                     00001420
           02 MELEMENTPV     PIC X.                                     00001430
           02 MELEMENTPO     PIC X(50).                                 00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MUSEA     PIC X.                                          00001460
           02 MUSEC     PIC X.                                          00001470
           02 MUSEP     PIC X.                                          00001480
           02 MUSEH     PIC X.                                          00001490
           02 MUSEV     PIC X.                                          00001500
           02 MUSEO     PIC X.                                          00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MMAXOCCURSA    PIC X.                                     00001530
           02 MMAXOCCURSC    PIC X.                                     00001540
           02 MMAXOCCURSP    PIC X.                                     00001550
           02 MMAXOCCURSH    PIC X.                                     00001560
           02 MMAXOCCURSV    PIC X.                                     00001570
           02 MMAXOCCURSO    PIC X(3).                                  00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MTELEMNTA      PIC X.                                     00001600
           02 MTELEMNTC PIC X.                                          00001610
           02 MTELEMNTP PIC X.                                          00001620
           02 MTELEMNTH PIC X.                                          00001630
           02 MTELEMNTV PIC X.                                          00001640
           02 MTELEMNTO      PIC X.                                     00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MELTLEGA  PIC X.                                          00001670
           02 MELTLEGC  PIC X.                                          00001680
           02 MELTLEGP  PIC X.                                          00001690
           02 MELTLEGH  PIC X.                                          00001700
           02 MELTLEGV  PIC X.                                          00001710
           02 MELTLEGO  PIC X(10).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MTCOBOLA  PIC X.                                          00001740
           02 MTCOBOLC  PIC X.                                          00001750
           02 MTCOBOLP  PIC X.                                          00001760
           02 MTCOBOLH  PIC X.                                          00001770
           02 MTCOBOLV  PIC X.                                          00001780
           02 MTCOBOLO  PIC X(2).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCTRLFLUXA     PIC X.                                     00001810
           02 MCTRLFLUXC     PIC X.                                     00001820
           02 MCTRLFLUXP     PIC X.                                     00001830
           02 MCTRLFLUXH     PIC X.                                     00001840
           02 MCTRLFLUXV     PIC X.                                     00001850
           02 MCTRLFLUXO     PIC X.                                     00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLGCOBOLA      PIC X.                                     00001880
           02 MLGCOBOLC PIC X.                                          00001890
           02 MLGCOBOLP PIC X.                                          00001900
           02 MLGCOBOLH PIC X.                                          00001910
           02 MLGCOBOLV PIC X.                                          00001920
           02 MLGCOBOLO      PIC X(3).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNBDECA   PIC X.                                          00001950
           02 MNBDECC   PIC X.                                          00001960
           02 MNBDECP   PIC X.                                          00001970
           02 MNBDECH   PIC X.                                          00001980
           02 MNBDECV   PIC X.                                          00001990
           02 MNBDECO   PIC X.                                          00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLVALEURA      PIC X.                                     00002020
           02 MLVALEURC PIC X.                                          00002030
           02 MLVALEURP PIC X.                                          00002040
           02 MLVALEURH PIC X.                                          00002050
           02 MLVALEURV PIC X.                                          00002060
           02 MLVALEURO      PIC X(22).                                 00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MVALEUR1A      PIC X.                                     00002090
           02 MVALEUR1C PIC X.                                          00002100
           02 MVALEUR1P PIC X.                                          00002110
           02 MVALEUR1H PIC X.                                          00002120
           02 MVALEUR1V PIC X.                                          00002130
           02 MVALEUR1O      PIC X(50).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MVALEUR2A      PIC X.                                     00002160
           02 MVALEUR2C PIC X.                                          00002170
           02 MVALEUR2P PIC X.                                          00002180
           02 MVALEUR2H PIC X.                                          00002190
           02 MVALEUR2V PIC X.                                          00002200
           02 MVALEUR2O      PIC X(50).                                 00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MVALEUR3A      PIC X.                                     00002230
           02 MVALEUR3C PIC X.                                          00002240
           02 MVALEUR3P PIC X.                                          00002250
           02 MVALEUR3H PIC X.                                          00002260
           02 MVALEUR3V PIC X.                                          00002270
           02 MVALEUR3O      PIC X(50).                                 00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLIBERRA  PIC X.                                          00002300
           02 MLIBERRC  PIC X.                                          00002310
           02 MLIBERRP  PIC X.                                          00002320
           02 MLIBERRH  PIC X.                                          00002330
           02 MLIBERRV  PIC X.                                          00002340
           02 MLIBERRO  PIC X(78).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCODTRAA  PIC X.                                          00002370
           02 MCODTRAC  PIC X.                                          00002380
           02 MCODTRAP  PIC X.                                          00002390
           02 MCODTRAH  PIC X.                                          00002400
           02 MCODTRAV  PIC X.                                          00002410
           02 MCODTRAO  PIC X(4).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNETNAMA  PIC X.                                          00002510
           02 MNETNAMC  PIC X.                                          00002520
           02 MNETNAMP  PIC X.                                          00002530
           02 MNETNAMH  PIC X.                                          00002540
           02 MNETNAMV  PIC X.                                          00002550
           02 MNETNAMO  PIC X(8).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MSCREENA  PIC X.                                          00002580
           02 MSCREENC  PIC X.                                          00002590
           02 MSCREENP  PIC X.                                          00002600
           02 MSCREENH  PIC X.                                          00002610
           02 MSCREENV  PIC X.                                          00002620
           02 MSCREENO  PIC X(4).                                       00002630
                                                                                
