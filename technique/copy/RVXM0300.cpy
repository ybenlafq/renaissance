      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVXM0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVXM0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0300.                                                            
           02  XM03-DOCXML                                                      
               PIC X(0020).                                                     
           02  XM03-ELEMENT                                                     
               PIC X(0050).                                                     
           02  XM03-VALEUR                                                      
               PIC X(0150).                                                     
           02  XM03-IMAJ                                                        
               PIC X(0007).                                                     
           02  XM03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM03-DOCXML-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM03-DOCXML-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM03-ELEMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM03-ELEMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM03-VALEUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM03-VALEUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM03-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM03-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
