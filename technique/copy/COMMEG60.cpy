      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-EG60-LONG-COMMAREA PIC S9(4) COMP VALUE +107.                   
      *--                                                                       
       01  COMM-EG60-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +107.                 
      *}                                                                        
       01  Z-COMMAREA-EG60.                                                     
           05  COMM-EG60-SSAAMMJJ.                                              
               10  COMM-EG60-SS         PIC XX.                                 
               10  COMM-EG60-AAMMJJ     PIC X(6).                               
           05  COMM-EG60-NSOCIETE   PIC X(3).                                   
           05  COMM-EG60-NOMTS      PIC X(8).                                   
           05  COMM-EG60-IMPIMM     PIC X.                                      
           05  COMM-EG60-PRT        PIC X(4).                                   
           05  COMM-EG60-MESS       PIC X(79).                                  
           05  COMM-EG60-CDRET      PIC X(4).                                   
                                                                                
