      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      * ZONES DE TRAVAIL POUR LES TRAITEMENTS DES MONTANTS SAISIS     *         
      ****************************************************** SYKWMONT *         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-MONT ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-MONT-USER             PIC X(20).                               
           05  FILLER                  REDEFINES W-MONT-USER.                   
             10  FILLER                OCCURS 20.                               
               15  W-MONT-USER-CAR     PIC X.                                   
               15  W-MONT-USER-CAR9    REDEFINES W-MONT-USER-CAR PIC 9.         
      *                                                                         
           05  W-MONT-FILE             PIC S9(9)V9(9) VALUE +0.                 
      *                                                                         
           05  W-MONT-NB-ENT-USER      PIC S9(3) COMP-3 VALUE +0.               
           05  W-MONT-NB-DEC-USER      PIC S9(3) COMP-3 VALUE +0.               
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-MONT-NUMERIC         PIC  X             VALUE SPACE.           
           05  W-MONT-POS-DEB         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POS-VIR         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-ENT          PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-DEC          PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-LONG            PIC S9(3)   COMP-3 VALUE +20.             
           05  W-MONT-I               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-J               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POIDS           PIC S9V9(9) COMP-3 VALUE +0.              
           05  W-MONT-SIGNE           PIC X            VALUE ' '.               
           05  W-MONT-FLAG            PIC X            VALUE SPACE.             
           05  W-MONT-EDIT            PIC -(9)9,9(9)   VALUE ZERO.              
           05  W-MONT-REEDIT REDEFINES W-MONT-EDIT.                             
             10  FILLER               OCCURS 20.                                
               15  W-MONT-EDIT-CAR    PIC X.                                    
           05  W-MONT-TAMPON.                                                   
             10  W-MONT-TAMPON1       PIC X            VALUE SPACE.             
             10  W-MONT-TAMPON2       PIC X(19)        VALUE SPACE.             
      *                                                                         
             EJECT                                                              
                                                                                
EMOD                                                                            
