      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEX3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEX3000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEX3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEX3000.                                                            
      *}                                                                        
           02  EX30-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  EX30-NTRI                                                        
               PIC S9(5) COMP-3.                                                
           02  EX30-LRUPTURE1                                                   
               PIC X(0026).                                                     
           02  EX30-LRUPTURE2                                                   
               PIC X(0026).                                                     
           02  EX30-LAGREGPRO1                                                  
               PIC X(0010).                                                     
           02  EX30-LAGREGPRO4                                                  
               PIC X(0010).                                                     
           02  EX30-WSEQPRO                                                     
               PIC S9(7) COMP-3.                                                
           02  EX30-WSEQLIEU                                                    
               PIC S9(7) COMP-3.                                                
           02  EX30-DOUVLIEU                                                    
               PIC X(0008).                                                     
           02  EX30-QDONNEESNUM1                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM2                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM3                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM4                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM5                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM6                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM7                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM8                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM9                                                
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM10                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM11                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM12                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM13                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM14                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM15                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM16                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM17                                               
               PIC S9(13)V9(0002) COMP-3.                                       
           02  EX30-QDONNEESNUM18                                               
               PIC S9(13)V9(0002) COMP-3.                                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEX3000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEX3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEX3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-NTRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-NTRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-LRUPTURE1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-LRUPTURE1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-LRUPTURE2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-LRUPTURE2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-LAGREGPRO1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-LAGREGPRO1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-LAGREGPRO4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-LAGREGPRO4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-WSEQPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-WSEQPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-WSEQLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-WSEQLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-DOUVLIEU-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-DOUVLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM1-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM3-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM3-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM4-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM4-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM5-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM5-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM6-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM6-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM7-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM7-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM8-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM8-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM9-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM9-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM10-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM10-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM11-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM11-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM12-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM12-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM13-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM13-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM14-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM14-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM15-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM15-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM16-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM16-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM17-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM17-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EX30-QDONNEESNUM18-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EX30-QDONNEESNUM18-F                                             
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
