      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR VUE RVXM2011                             *        
      ******************************************************************        
       01  RVXM2011.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2011-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2011-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2011-CONTEXT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2011-CONTEXT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2011-CONTEXT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2011-CONTEXT-TEXT      PIC X(50).                            
           10 XM2011-NCODICK.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2011-NCODICK-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2011-NCODICK-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2011-NCODICK-TEXT      PIC X(7).                             
           10 XM2011-NCODIC.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2011-NCODIC-LEN        PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2011-NCODIC-LEN        PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2011-NCODIC-TEXT       PIC X(7).                             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2011                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2011-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2011-CONTEXT-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2011-CONTEXT-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2011-NCODICK-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2011-NCODICK-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2011-NCODIC-F             PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2011-NCODIC-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
