      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2070                            *        
      ******************************************************************        
       01  RVXM2070.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2070-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2070-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2070-CTABLEG1.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2070-CTABLEG1-LEN      PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2070-CTABLEG1-LEN      PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2070-CTABLEG1-TEXT     PIC X(05).                            
           10 XM2070-CTABLEG2.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2070-CTABLEG2-LEN      PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2070-CTABLEG2-LEN      PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2070-CTABLEG2-TEXT     PIC X(15).                            
           10 XM2070-WTABLEG.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2070-WTABLEG-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2070-WTABLEG-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2070-WTABLEG-TEXT      PIC X(80).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2070                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2070-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2070-CTABLEG1-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2070-CTABLEG1-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2070-CTABLEG2-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2070-CTABLEG2-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2070-WTABLEG-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2070-WTABLEG-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
