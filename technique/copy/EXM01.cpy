      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: param de la version XML,TS                                 00000020
      ***************************************************************** 00000030
       01   EXM01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
           02 MLDTDI OCCURS   10 TIMES .                                00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAFFICHL     COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MAFFICHL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MAFFICHF     PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MAFFICHI     PIC X.                                     00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MLCHOIXI     PIC X.                                     00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPPLIL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCAPPLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAPPLIF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCAPPLII     PIC X(6).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNOMTSXSDL  COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCNOMTSXSDL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCNOMTSXSDF  PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCNOMTSXSDI  PIC X(8).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNOMTSCARL  COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCNOMTSCARL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCNOMTSCARF  PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCNOMTSCARI  PIC X(8).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVERSIONL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MVERSIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MVERSIONF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MVERSIONI    PIC X(50).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWCAPPLIL     COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MNWCAPPLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNWCAPPLIF     PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNWCAPPLII     PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWTSXSDL      COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MNWTSXSDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNWTSXSDF      PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNWTSXSDI      PIC X(8).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWTSCARL      COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MNWTSCARL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNWTSCARF      PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNWTSCARI      PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWVERSION1L   COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MNWVERSION1L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNWVERSION1F   PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNWVERSION1I   PIC X(50).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWVERSION2L   COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MNWVERSION2L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNWVERSION2F   PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNWVERSION2I   PIC X(50).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGVERSIONL    COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MLGVERSIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLGVERSIONF    PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLGVERSIONI    PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWVERSION3L   COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MNWVERSION3L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNWVERSION3F   PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNWVERSION3I   PIC X(50).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(74).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: param de la version XML,TS                                 00000960
      ***************************************************************** 00000970
       01   EXM01O REDEFINES EXM01I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC X(3).                                  00001270
           02 MLDTDO OCCURS   10 TIMES .                                00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MAFFICHA     PIC X.                                     00001300
             03 MAFFICHC     PIC X.                                     00001310
             03 MAFFICHP     PIC X.                                     00001320
             03 MAFFICHH     PIC X.                                     00001330
             03 MAFFICHV     PIC X.                                     00001340
             03 MAFFICHO     PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MLCHOIXA     PIC X.                                     00001370
             03 MLCHOIXC     PIC X.                                     00001380
             03 MLCHOIXP     PIC X.                                     00001390
             03 MLCHOIXH     PIC X.                                     00001400
             03 MLCHOIXV     PIC X.                                     00001410
             03 MLCHOIXO     PIC X.                                     00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MCAPPLIA     PIC X.                                     00001440
             03 MCAPPLIC     PIC X.                                     00001450
             03 MCAPPLIP     PIC X.                                     00001460
             03 MCAPPLIH     PIC X.                                     00001470
             03 MCAPPLIV     PIC X.                                     00001480
             03 MCAPPLIO     PIC X(6).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MCNOMTSXSDA  PIC X.                                     00001510
             03 MCNOMTSXSDC  PIC X.                                     00001520
             03 MCNOMTSXSDP  PIC X.                                     00001530
             03 MCNOMTSXSDH  PIC X.                                     00001540
             03 MCNOMTSXSDV  PIC X.                                     00001550
             03 MCNOMTSXSDO  PIC X(8).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MCNOMTSCARA  PIC X.                                     00001580
             03 MCNOMTSCARC  PIC X.                                     00001590
             03 MCNOMTSCARP  PIC X.                                     00001600
             03 MCNOMTSCARH  PIC X.                                     00001610
             03 MCNOMTSCARV  PIC X.                                     00001620
             03 MCNOMTSCARO  PIC X(8).                                  00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MVERSIONA    PIC X.                                     00001650
             03 MVERSIONC    PIC X.                                     00001660
             03 MVERSIONP    PIC X.                                     00001670
             03 MVERSIONH    PIC X.                                     00001680
             03 MVERSIONV    PIC X.                                     00001690
             03 MVERSIONO    PIC X(50).                                 00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNWCAPPLIA     PIC X.                                     00001720
           02 MNWCAPPLIC     PIC X.                                     00001730
           02 MNWCAPPLIP     PIC X.                                     00001740
           02 MNWCAPPLIH     PIC X.                                     00001750
           02 MNWCAPPLIV     PIC X.                                     00001760
           02 MNWCAPPLIO     PIC X(6).                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNWTSXSDA      PIC X.                                     00001790
           02 MNWTSXSDC PIC X.                                          00001800
           02 MNWTSXSDP PIC X.                                          00001810
           02 MNWTSXSDH PIC X.                                          00001820
           02 MNWTSXSDV PIC X.                                          00001830
           02 MNWTSXSDO      PIC X(8).                                  00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNWTSCARA      PIC X.                                     00001860
           02 MNWTSCARC PIC X.                                          00001870
           02 MNWTSCARP PIC X.                                          00001880
           02 MNWTSCARH PIC X.                                          00001890
           02 MNWTSCARV PIC X.                                          00001900
           02 MNWTSCARO      PIC X(8).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNWVERSION1A   PIC X.                                     00001930
           02 MNWVERSION1C   PIC X.                                     00001940
           02 MNWVERSION1P   PIC X.                                     00001950
           02 MNWVERSION1H   PIC X.                                     00001960
           02 MNWVERSION1V   PIC X.                                     00001970
           02 MNWVERSION1O   PIC X(50).                                 00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNWVERSION2A   PIC X.                                     00002000
           02 MNWVERSION2C   PIC X.                                     00002010
           02 MNWVERSION2P   PIC X.                                     00002020
           02 MNWVERSION2H   PIC X.                                     00002030
           02 MNWVERSION2V   PIC X.                                     00002040
           02 MNWVERSION2O   PIC X(50).                                 00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MLGVERSIONA    PIC X.                                     00002070
           02 MLGVERSIONC    PIC X.                                     00002080
           02 MLGVERSIONP    PIC X.                                     00002090
           02 MLGVERSIONH    PIC X.                                     00002100
           02 MLGVERSIONV    PIC X.                                     00002110
           02 MLGVERSIONO    PIC X(5).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNWVERSION3A   PIC X.                                     00002140
           02 MNWVERSION3C   PIC X.                                     00002150
           02 MNWVERSION3P   PIC X.                                     00002160
           02 MNWVERSION3H   PIC X.                                     00002170
           02 MNWVERSION3V   PIC X.                                     00002180
           02 MNWVERSION3O   PIC X(50).                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(74).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
