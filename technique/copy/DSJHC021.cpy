      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  JHC021-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  JHC021-E00.                                                      
               10  FILLER                         PIC X(52) VALUE               
           'JHC021                                           LE '.              
               10  JHC021-E00-DATEDITE            PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  JHC021-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  JHC021-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(07) VALUE               
           '  PAGE '.                                                           
               10  JHC021-E00-NOPAGE              PIC ZZZ.                      
               10  FILLER                         PIC X(51) VALUE               
           '                                                   '.               
      *--------------------------- LIGNE E10  --------------------------        
           05  JHC021-E10.                                                      
               10  FILLER                         PIC X(18) VALUE               
           'BON DE MUTATION : '.                                                
               10  JHC021-E10-LTRAIT              PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(36) VALUE               
           '                                    '.                              
      *--------------------------- LIGNE E20  --------------------------        
           05  JHC021-E20.                                                      
               10  FILLER                         PIC X(18) VALUE               
           'ENVOI DU        : '.                                                
               10  JHC021-E20-DENVOI              PIC X(10).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(46) VALUE               
           '                                              '.                    
      *--------------------------- LIGNE E30  --------------------------        
           05  JHC021-E30.                                                      
               10  FILLER                         PIC X(18) VALUE               
           'NUMERO D"ENVOI  : '.                                                
               10  JHC021-E30-NENVOI              PIC X(07).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(49) VALUE               
           '                                                 '.                 
      *--------------------------- LIGNE E40  --------------------------        
           05  JHC021-E40.                                                      
               10  FILLER                         PIC X(23) VALUE               
           'ENVOI DE MATERIEL DE : '.                                           
               10  JHC021-E40-NSOCORIG            PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-NLIEUORIG           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-NSSLIEUORIG         PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-CLIEUTRTORIG        PIC X(05).                    
               10  FILLER                         PIC X(11) VALUE               
           '    VERS : '.                                                       
               10  JHC021-E40-NSOCDEST            PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-NLIEUDEST           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-NSSLIEUDEST         PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-E40-CLIEUTRTDEST        PIC X(05).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
      *--------------------------- LIGNE E50  --------------------------        
           05  JHC021-E50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------+                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E60  --------------------------        
           05  JHC021-E60.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!ADRESSE  CODIC  QTE - FAMILLE  MARQUE  REFERENCE -  NUMER'.        
               10  FILLER                         PIC X(58) VALUE               
           'O SERIE  IDENTIFIANT !                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E70  --------------------------        
           05  JHC021-E70.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE D10  --------------------------        
           05  JHC021-D10.                                                      
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
               10  JHC021-D10-ADRESSE             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-QTE                 PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-LREF                PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-CSERIE              PIC X(14).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-NLIEUHC             PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JHC021-D10-NCHS                PIC X(07).                    
               10  FILLER                         PIC X(53) VALUE               
           '!                                                    '.             
      *--------------------------- LIGNE B10  --------------------------        
           05  JHC021-B10.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------+                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B12  --------------------------        
           05  JHC021-B12.                                                      
               10  FILLER                         PIC X(28) VALUE               
           '! QUANTITE TOTALE TRAITEE : '.                                      
               10  JHC021-B12-QTRAITEE            PIC ZZZZ9.                    
               10  FILLER                         PIC X(58) VALUE               
           '                                              !           '.        
               10  FILLER                         PIC X(41) VALUE               
           '                                         '.                         
      *--------------------------- LIGNE B14  --------------------------        
           05  JHC021-B14.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------+                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B20  --------------------------        
           05  JHC021-B20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'OBSERVATIONS ---------------  +---EMETTEUR---+ +-TRANSPORT'.        
               10  FILLER                         PIC X(58) VALUE               
           'EUR-+ +-DESTINATAIRE-+                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B30  --------------------------        
           05  JHC021-B30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------  !              ! !          '.        
               10  FILLER                         PIC X(58) VALUE               
           '    ! !              !                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B40  --------------------------        
           05  JHC021-B40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------  !              ! !          '.        
               10  FILLER                         PIC X(58) VALUE               
           '    ! !              !                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B50  --------------------------        
           05  JHC021-B50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                              +--------------+ +----------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----+ +--------------+                                    '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
                                                                                
