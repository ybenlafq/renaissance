      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      * TRAITEMENTS MQ - COTE HOST                                              
      * UTILISEE PAR TMQ00, TMQ10 ET TMQ15                                      
      *****************************************************************         
      * ENTREE : 187 OCTETS                                                     
      *                                                                         
      *                                                                         
       01  COMM-MQ00-APPLI.                                                     
            05  COMM-MQ00-ENTREE.                                               
               10  COMM-MQ00-QNOM            PIC X(48).                         
               10  COMM-MQ00-MSGID           PIC X(24).                         
               10  COMM-MQ00-CORRELID        PIC X(24).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  COMM-MQ00-LONGMES         PIC S9(9) BINARY.                  
      *--                                                                       
               10  COMM-MQ00-LONGMES         PIC S9(9) COMP-5.                  
      *}                                                                        
               10  COMM-MQ00-LINKPROG        PIC X(6).                          
               10  COMM-MQ00-VERS            PIC X(1).                          
               10  COMM-MQ00-DUREE           PIC S9(6).                         
               10  COMM-MQ00-BACK            PIC X(1).                          
               10  COMM-MQ00-DATA            PIC X(1).                          
               10  COMM-MQ00-QUEUEBAK        PIC X(24).                         
               10  COMM-MQ00-QUEUEDTL        PIC X(24).                         
               10  COMM-MQ00-QUEUEDTE        PIC X(24).                         
                                                                                
