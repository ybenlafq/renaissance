      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * RETOUR AVEC COMMAREA                                          *         
      ****************************************************** SYKCRTCO *         
      *                                                                         
       RETOUR-COMMAREA SECTION.                                                 
      *                                                                         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS RETURN   TRANSID  (NOM-TACHE-RETOUR)                           
                          COMMAREA (Z-COMMAREA)                                 
                          LENGTH   (LONG-COMMAREA)                              
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��00007   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-TACHE-RETOUR Z-COMMAR        
      *dfhei*     LONG-COMMAREA.                                                
      *dfhei*} decommente EXEC                                                  
      *                                                                         
           MOVE EIBRCODE TO EIB-RCODE.                                          
      *                                                                         
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
      *                                                                         
       FIN-RETOUR-COMMAREA.  EXIT.                                              
       EJECT                                                                    
                                                                                
                                                                                
