      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/04/2016 15:48 >
      ***************************************************************** 00100000
      *                                                               * 00200000
      * �BANNER_START                           02                    * 00214200
      * Licensed Materials - Property of IBM                          * 00228400
      *                                                               * 00242600
      * "Restricted Materials of IBM"                                 * 00256800
      *                                                               * 00271000
      * 5655-S97              DLIUIB                                  * 00285200
      *                                                               * 00299400
      * (C) Copyright IBM Corp. 1988                                  * 00313600
      *                                                               * 00327800
      * CICS                                                          * 00342000
      * (Element of CICS Transaction Server                           * 00356200
      *   for z/OS, Version 4 Release 1)                              * 00370400
      * �BANNER_END                                                   * 00384600
      *                                                               * 00400000
      * STATUS = 2.1.0                                                * 00500000
      *                                                               * 00600000
      * CHANGE ACTIVITY :                                             * 00700000
      *                                                               * 00800000
      *   $SEG(DFHUIBO),COMP(DL/I),PROD(CICS    ):                    * 00900000
      *                                                               * 01000000
      *  PN= REASON REL YYMMDD HDXXIII : REMARKS                      * 01200000
      * $L0= Base   210 88     HD2GJST : Base                         * 01400000
      *                                                               * 01600000
      ***************************************************************** 01800000
      ******************************************************************02000000
      *                                                                *04000000
      *       MODULE-NAME = DLIUIB                                     *06000000
      *                                                                *08000000
      *       DESCRIPTIVE NAME = STRUCTURE FOR USER INTERFACE BLOCK    *10000000
      *                                                                *12000000
      *                                                                *16000000
      *                                                                *20000000
      *       FUNCTION = DESCRIBE USER INTERFACE BLOCK FIELDS.         *22000000
      *                  THE UIB CONTAINS SCHEDULING AND SYSTEM CALL   *24000000
      *                                                                *28000000
      *       MODULE-TYPE = STRUCTURE                                  *30000000
      *                                                                *32000000
      *       CHANGE ACTIVITY = �BCAC80A                               *34000000
      *                                                                *36000000
      ******************************************************************38000000
       01   DLIUIB.                                                     40000000
      *     DLIUIB    EXTENDED CALL USER INTERFACE BLOCK                42000000
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      02 UIBPCBAL PICTURE S9(8) USAGE IS COMPUTATIONAL.           44000000
      *--
            02 UIBPCBAL PICTURE S9(8) COMP-5.
      *}
      *        UIBPCBAL    PCB ADDRESS LIST                             46000000
            02 UIBRCODE.                                                48000000
      *        UIBRCODE    DL/I RETURN CODES                            50000000
                 03 UIBFCTR PICTURE X.                                  52000000
      *             UIBFCTR    RETURN CODES                             54000000
      *            88  FCNORESP    VALUE ' '.                           56000000
                   88  FCNORESP    VALUE X'00'.                           56000000
                   88  FCNOTOPEN   VALUE ''.                           58000000
                   88  FCINVREQ    VALUE '�'.                           61000000
                   88  FCINVPCB    VALUE ''.                           64000000
                 03 UIBDLTR PICTURE X.                                  67000000
      *             UIBDLTR    ADDITIONAL INFORMATION                   70000000
                   88  DLPSBNF     VALUE ''.                           73000000
                   88  DLTASKNA    VALUE ''.                           76000000
                   88  DLPSBSCH    VALUE ''.                           79000000
                   88  DLLANGCON   VALUE '�'.                           82000000
                   88  DLPSBFAIL   VALUE '	'.                           85000000
                   88  DLPSBNA     VALUE '�'.                           88000000
                   88  DLTERMNS    VALUE ''.                           91000000
                   88  DLFUNCNS    VALUE '�'.                           94000000
                   88  DLINA       VALUE '�'.                           97000000
