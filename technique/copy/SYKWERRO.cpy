      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************* 14-06-89 *        
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES           
      ******************************************************* SYKWERRO *        
       01  Z-ERREUR.                                                            
           05  FILLER                 PIC X(256).                               
           05  Z-ERREUR-TRACE-MESS    PIC X(80).                                
           05  Z-ERREUR-EIBLK         PIC X(66).                                
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACE-MESSAGE-LONG PIC S9(4) COMP VALUE +84.                     
      *--                                                                       
           05  TRACE-MESSAGE-LONG PIC S9(4) COMP-5 VALUE +84.                   
      *}                                                                        
           05  TRACE-MESSAGE.                                                   
      *                                     START-BUFFER-ADDRESS                
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     23EME LIGNE 1ERE COLONNE            
             10  FILLER         PIC  X(2)   VALUE '$-'.                         
      *                                     START-FIELD                         
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     ATTRIBUT                            
             10  FILLER         PIC  X      VALUE 'Y'.                          
             10  TRACE-MESS.                                                    
               15  MESS1          PIC  X(15)  VALUE SPACE.                      
               15  MESS           PIC  X(64)  VALUE SPACE.                      
               15  FILLER REDEFINES MESS.                                       
                 20  MESS-ABCODE  PIC  X(4).                                    
                 20  FILL         PIC  X(60).                                   
       EJECT                                                                    
                                                                                
