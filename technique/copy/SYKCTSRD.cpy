      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      * CONSULTATION DE LA TEMPORARY STORAGE                            00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       READ-TS                    SECTION.                              00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READQ TS                                                       
                          QUEUE    (IDENT-TS)                                   
                          INTO     (Z-INOUT)                                    
                          LENGTH   (LONG-TS)                                    
                          ITEM     (RANG-TS)                                    
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��Yi00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-INOUT LONG-TS         
      *dfhei*     DFHDUMMY RANG-TS.                                             
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001600
           MOVE EIBRCODE TO EIB-RCODE.                                  00001700
           IF   EIB-NORMAL                                              00001800
                MOVE 0 TO CODE-RETOUR                                   00001900
           ELSE                                                         00002000
                IF   EIB-QIDERR OR EIB-ITEMERR                          00002100
                     MOVE 1 TO CODE-RETOUR                              00002200
                ELSE                                                    00002300
                     GO TO ABANDON-CICS                                 00002400
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002500
       FIN-READ-TS. EXIT.                                               00002600
                    EJECT                                               00002800
                                                                                
                                                                                
