      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * GESTION DES CONDITIONS D'ERREURS CICS NON PREVUES             *         
      ****************************************************** SYKCHAND *         
      *                                                                         
       INIT-HANDLE SECTION.                                                     
      *                                                                         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS HANDLE ABEND                                                   
                        LABEL (ABANDON-ABEND)                                   
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�00007   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0                                   
      *dfhei*      SERVICE LABEL                                                
      *dfhei*} decommente EXEC                                                  
      *{ Post-Translation Correct-Copy-DFHEIGDI                                 
      *      GO TO ABANDON-ABEND DEPENDING ON DFHEIGDI.                         
      *} Post-Translation                                                       
      *                                                                         
       FIN-INIT-HANDLE. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
