      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVXM0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVXM0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0200.                                                            
           02  XM02-DOCXML                                                      
               PIC X(0020).                                                     
           02  XM02-ELEMENT                                                     
               PIC X(0050).                                                     
           02  XM02-TELEMENT                                                    
               PIC X(0001).                                                     
           02  XM02-TCOBOL                                                      
               PIC X(0002).                                                     
           02  XM02-LONG                                                        
               PIC S9(03) COMP-3.                                               
           02  XM02-NBDEC                                                       
               PIC S9(01) COMP-3.                                               
           02  XM02-IMAJ                                                        
               PIC X(0007).                                                     
           02  XM02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-DOCXML-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-DOCXML-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-ELEMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-ELEMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-TELEMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-TELEMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-TCOBOL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-TCOBOL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-LONG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-LONG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-NBDEC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-NBDEC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
