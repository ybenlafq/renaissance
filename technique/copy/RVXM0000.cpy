      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVXM0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVXM0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0000.                                                            
           02  XM00-DOCXML                                                      
               PIC X(0020).                                                     
           02  XM00-PROLOGUE                                                    
               PIC X(0150).                                                     
           02  XM00-LGDATAR                                                     
               PIC S9(05) COMP-3.                                               
           02  XM00-IMAJ                                                        
               PIC X(0007).                                                     
           02  XM00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM00-DOCXML-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM00-DOCXML-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM00-PROLOGUE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM00-PROLOGUE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM00-LGDATAR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM00-LGDATAR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM00-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM00-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
