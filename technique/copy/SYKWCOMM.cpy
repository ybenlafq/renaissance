      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ** AIDA *********************************************************         
      *           PREFIXE STANDARD COMMAREA LONG = 100                *         
      ****************************************************** SYKWCOMM *         
      *                                                                         
       01  COM-AIDA REDEFINES Z-COMMAREA.                                       
      *                                                                         
           02 COM-CODERR                  PIC X(04).                            
           02 COM-PGMPRC                  PIC X(08).                            
           02 Z-COMMAREA-NOM-MAP          PIC X(08).                            
           02 Z-COMMAREA-NOM-MAPSET       PIC X(08).                            
           02 Z-COMMAREA-NOM-TACHE        PIC X(04).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 Z-COMMAREA-LONG             PIC S9(4) COMP.                       
      *--                                                                       
           02 Z-COMMAREA-LONG             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 Z-COMMAREA-LONG-MAP         PIC S9(4) COMP.                       
      *--                                                                       
           02 Z-COMMAREA-LONG-MAP         PIC S9(4) COMP-5.                     
      *}                                                                        
           02 Z-COMMAREA-SELECT.                                                
              04 Z-COMMAREA-SELECTION     PIC X(04) OCCURS 3.                   
           02 Z-COMMAREA-TACHE-JUMP       PIC X(04).                            
           02 FILLER                      PIC X(44).                            
      *-----------------                                                        
           02 COMGM00-KONTROL             PIC X.                                
           02 COMGM00-ACTION              PIC X.                                
              88  COMGM00-NGERE           VALUES ' ' '0'.                       
              88  COMGM00-GERE            VALUES 'N' 'E' 'D' 'M' 'R'.           
              88  COMGM00-NOERASE         VALUE 'N'.                            
              88  COMGM00-ERASE           VALUE 'E'.                            
              88  COMGM00-DATAONLY        VALUE 'D'.                            
              88  COMGM00-MAPONLY         VALUE 'M'.                            
              88  COMGM00-RECEIVE         VALUE 'R'.                            
           02 COMGM00-CPOSA.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMGM00-CURPOS           PIC S9(4) COMP.                       
      *--                                                                       
              03 COMGM00-CURPOS           PIC S9(4) COMP-5.                     
      *}                                                                        
      *-----------------                                                        
                                                                                
                                                                                
