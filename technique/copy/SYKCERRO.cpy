      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
       EJECT                                                                    
      *==> DARTY ******************************************* 23-06-89 *         
       SECTION-ABANDON-TACHE             SECTION.                               
      ****************************************************** SYKCERRO *         
       ABANDON-ABEND.                                                           
           MOVE 'ABANDON-ABEND :' TO MESS1.                                     
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-AIDA.                                                            
           MOVE 'ABANDON-AIDA  :' TO MESS1.                                     
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-TACHE.                                                           
           MOVE 'ABANDON-TACHE :' TO MESS1.                                     
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-CICS.                                                            
           MOVE 'ABANDON-CICS  :'  TO MESS1.                                    
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-DL1.                                                             
           MOVE 'ABANDON-DL1   :' TO MESS1.                                     
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-SQL.                                                             
           MOVE 'ABANDON-SQL   :' TO MESS1.                                     
           GO TO SEND-MESSAGE-ABANDON.                                          
       EJECT                                                                    
      *{ Post-Translation Correct-Val-termcode                                  
      * SEND-MESSAGE-ABANDON.                                                   
       SEND-MESSAGE-ABANDON.                                                    
          MOVE 'j' TO TERMCODE                                                  
      *} Post-Translation                                                       
           IF  TERMINAL-ECRAN                                                   
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 FROM    (TRACE-MESSAGE)                                        
                 LENGTH  (TRACE-MESSAGE-LONG)                                   
                 CTLCHAR (WCC-SCREEN)                                           
                 WAIT                                                           
                 NOHANDLE                                                       
       END-EXEC                                                                 
      *dfhei*- commente DFHEI1                                                  
      *dfhei*         MOVE '���	�00033   ' TO DFHEIV0                          
      *dfhei*         CALL 'DFHEI1' USING DFHEIV0  DFHDUMMY DFHDUMMY            
      *dfhei*     TRACE-MESSAGE TRACE-MESSAGE-LONG WCC-SCREEN                   
      *dfhei*} decommente EXEC                                                  
           END-IF.                                                              
           IF TERMINAL-IMPRIMANTE                                               
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 FROM    (TRACE-MESS)                                           
                 LENGTH  (LENGTH OF TRACE-MESS)                                 
                 CTLCHAR (WCC-PRINTER)                                          
                 NOHANDLE                                                       
       END-EXEC                                                                 
      *dfhei*- commente DFHEI1                                                  
      *dfhei*        MOVE '����00044   ' TO DFHEIV0                           
      *dfhei*        MOVE LENGTH OF TRACE-MESS TO DFHB0020                      
      *dfhei*        CALL 'DFHEI1' USING DFHEIV0  DFHDUMMY DFHDUMMY TRAC        
      *dfhei*     DFHB0020 WCC-PRINTER                                          
      *dfhei*} decommente EXEC                                                  
           END-IF.                                                              
      *dfhei*{ decommente EXEC                                                  
       EXEC  CICS  ABEND                                                        
                   CANCEL                                                       
                   NOHANDLE                                                     
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�00052   ' TO DFHEIV0                                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0.                                  
       FIN-SECTION-ABANDON-TACHE. EXIT.                                         
      *dfhei*} decommente EXEC                                                  
      ******************************************************************        
       FIN-PHYSIQUE-PROGRAMME  SECTION.                                         
      ******************************************************************        
           GOBACK.                                                              
       FIN-FIN-PHYSIQUE-PROGRAMME. EXIT.                                        
       EJECT                                                                    
                                                                                
                                                                                
