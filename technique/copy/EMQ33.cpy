      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMQ33   EMQ33                                              00000020
      ***************************************************************** 00000030
       01   EMQ33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSOCCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLIEUCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIEUCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATECL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATECF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATECI   PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHEURECL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MHEURECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MHEURECF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MHEURECI  PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODECL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODECF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODECI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEQL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTYPEQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTYPEQF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTYPEQI   PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUEUECL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MQUEUECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQUEUECF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQUEUECI  PIC X(24).                                      00000450
           02 MACTIOND OCCURS   12 TIMES .                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIONL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MACTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MACTIONF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MACTIONI     PIC X.                                     00000500
           02 MDATEMD OCCURS   12 TIMES .                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEML      COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MDATEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEMF      PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MDATEMI      PIC X(8).                                  00000550
           02 MHEUREMD OCCURS   12 TIMES .                              00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHEUREML     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MHEUREML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHEUREMF     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MHEUREMI     PIC X(4).                                  00000600
           02 MSOCMD OCCURS   12 TIMES .                                00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCML  COMP PIC S9(4).                                 00000620
      *--                                                                       
             03 MSOCML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOCMF  PIC X.                                          00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MSOCMI  PIC X(3).                                       00000650
           02 MLIEUMD OCCURS   12 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUML      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIEUML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIEUMF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIEUMI      PIC X(3).                                  00000700
           02 MCODEMD OCCURS   12 TIMES .                               00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEML      COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MCODEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODEMF      PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MCODEMI      PIC X(3).                                  00000750
           02 MTAILLED OCCURS   12 TIMES .                              00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTAILLEL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MTAILLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTAILLEF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MTAILLEI     PIC X(5).                                  00000800
           02 MPRIORD OCCURS   12 TIMES .                               00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIORL      COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MPRIORL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIORF      PIC X.                                     00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MPRIORI      PIC X(2).                                  00000850
           02 MCHRONOD OCCURS   12 TIMES .                              00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHRONOL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCHRONOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCHRONOF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCHRONOI     PIC X(7).                                  00000900
           02 MTOTALD OCCURS   12 TIMES .                               00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOTALL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MTOTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTOTALF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MTOTALI      PIC X(7).                                  00000950
           02 MEXPIRYD OCCURS   12 TIMES .                              00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEXPIRYL     COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MEXPIRYL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MEXPIRYF     PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MEXPIRYI     PIC X(9).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLIBERRI  PIC X(78).                                      00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCODTRAI  PIC X(4).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MCICSI    PIC X(5).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MNETNAMI  PIC X(8).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MSCREENI  PIC X(4).                                       00001200
      ***************************************************************** 00001210
      * SDF: EMQ33   EMQ33                                              00001220
      ***************************************************************** 00001230
       01   EMQ33O REDEFINES EMQ33I.                                    00001240
           02 FILLER    PIC X(12).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATJOUA  PIC X.                                          00001270
           02 MDATJOUC  PIC X.                                          00001280
           02 MDATJOUP  PIC X.                                          00001290
           02 MDATJOUH  PIC X.                                          00001300
           02 MDATJOUV  PIC X.                                          00001310
           02 MDATJOUO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTIMJOUA  PIC X.                                          00001340
           02 MTIMJOUC  PIC X.                                          00001350
           02 MTIMJOUP  PIC X.                                          00001360
           02 MTIMJOUH  PIC X.                                          00001370
           02 MTIMJOUV  PIC X.                                          00001380
           02 MTIMJOUO  PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNPAGEA   PIC X.                                          00001410
           02 MNPAGEC   PIC X.                                          00001420
           02 MNPAGEP   PIC X.                                          00001430
           02 MNPAGEH   PIC X.                                          00001440
           02 MNPAGEV   PIC X.                                          00001450
           02 MNPAGEO   PIC X(7).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MSOCCA    PIC X.                                          00001480
           02 MSOCCC    PIC X.                                          00001490
           02 MSOCCP    PIC X.                                          00001500
           02 MSOCCH    PIC X.                                          00001510
           02 MSOCCV    PIC X.                                          00001520
           02 MSOCCO    PIC X(3).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIEUCA   PIC X.                                          00001550
           02 MLIEUCC   PIC X.                                          00001560
           02 MLIEUCP   PIC X.                                          00001570
           02 MLIEUCH   PIC X.                                          00001580
           02 MLIEUCV   PIC X.                                          00001590
           02 MLIEUCO   PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MDATECA   PIC X.                                          00001620
           02 MDATECC   PIC X.                                          00001630
           02 MDATECP   PIC X.                                          00001640
           02 MDATECH   PIC X.                                          00001650
           02 MDATECV   PIC X.                                          00001660
           02 MDATECO   PIC X(8).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MHEURECA  PIC X.                                          00001690
           02 MHEURECC  PIC X.                                          00001700
           02 MHEURECP  PIC X.                                          00001710
           02 MHEURECH  PIC X.                                          00001720
           02 MHEURECV  PIC X.                                          00001730
           02 MHEURECO  PIC X(4).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCODECA   PIC X.                                          00001760
           02 MCODECC   PIC X.                                          00001770
           02 MCODECP   PIC X.                                          00001780
           02 MCODECH   PIC X.                                          00001790
           02 MCODECV   PIC X.                                          00001800
           02 MCODECO   PIC X(3).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MTYPEQA   PIC X.                                          00001830
           02 MTYPEQC   PIC X.                                          00001840
           02 MTYPEQP   PIC X.                                          00001850
           02 MTYPEQH   PIC X.                                          00001860
           02 MTYPEQV   PIC X.                                          00001870
           02 MTYPEQO   PIC X.                                          00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MQUEUECA  PIC X.                                          00001900
           02 MQUEUECC  PIC X.                                          00001910
           02 MQUEUECP  PIC X.                                          00001920
           02 MQUEUECH  PIC X.                                          00001930
           02 MQUEUECV  PIC X.                                          00001940
           02 MQUEUECO  PIC X(24).                                      00001950
           02 DFHMS1 OCCURS   12 TIMES .                                00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MACTIONA     PIC X.                                     00001980
             03 MACTIONC     PIC X.                                     00001990
             03 MACTIONP     PIC X.                                     00002000
             03 MACTIONH     PIC X.                                     00002010
             03 MACTIONV     PIC X.                                     00002020
             03 MACTIONO     PIC X.                                     00002030
           02 DFHMS2 OCCURS   12 TIMES .                                00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MDATEMA      PIC X.                                     00002060
             03 MDATEMC PIC X.                                          00002070
             03 MDATEMP PIC X.                                          00002080
             03 MDATEMH PIC X.                                          00002090
             03 MDATEMV PIC X.                                          00002100
             03 MDATEMO      PIC X(8).                                  00002110
           02 DFHMS3 OCCURS   12 TIMES .                                00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MHEUREMA     PIC X.                                     00002140
             03 MHEUREMC     PIC X.                                     00002150
             03 MHEUREMP     PIC X.                                     00002160
             03 MHEUREMH     PIC X.                                     00002170
             03 MHEUREMV     PIC X.                                     00002180
             03 MHEUREMO     PIC X(4).                                  00002190
           02 DFHMS4 OCCURS   12 TIMES .                                00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MSOCMA  PIC X.                                          00002220
             03 MSOCMC  PIC X.                                          00002230
             03 MSOCMP  PIC X.                                          00002240
             03 MSOCMH  PIC X.                                          00002250
             03 MSOCMV  PIC X.                                          00002260
             03 MSOCMO  PIC X(3).                                       00002270
           02 DFHMS5 OCCURS   12 TIMES .                                00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MLIEUMA      PIC X.                                     00002300
             03 MLIEUMC PIC X.                                          00002310
             03 MLIEUMP PIC X.                                          00002320
             03 MLIEUMH PIC X.                                          00002330
             03 MLIEUMV PIC X.                                          00002340
             03 MLIEUMO      PIC X(3).                                  00002350
           02 DFHMS6 OCCURS   12 TIMES .                                00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MCODEMA      PIC X.                                     00002380
             03 MCODEMC PIC X.                                          00002390
             03 MCODEMP PIC X.                                          00002400
             03 MCODEMH PIC X.                                          00002410
             03 MCODEMV PIC X.                                          00002420
             03 MCODEMO      PIC X(3).                                  00002430
           02 DFHMS7 OCCURS   12 TIMES .                                00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MTAILLEA     PIC X.                                     00002460
             03 MTAILLEC     PIC X.                                     00002470
             03 MTAILLEP     PIC X.                                     00002480
             03 MTAILLEH     PIC X.                                     00002490
             03 MTAILLEV     PIC X.                                     00002500
             03 MTAILLEO     PIC X(5).                                  00002510
           02 DFHMS8 OCCURS   12 TIMES .                                00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MPRIORA      PIC X.                                     00002540
             03 MPRIORC PIC X.                                          00002550
             03 MPRIORP PIC X.                                          00002560
             03 MPRIORH PIC X.                                          00002570
             03 MPRIORV PIC X.                                          00002580
             03 MPRIORO      PIC X(2).                                  00002590
           02 DFHMS9 OCCURS   12 TIMES .                                00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MCHRONOA     PIC X.                                     00002620
             03 MCHRONOC     PIC X.                                     00002630
             03 MCHRONOP     PIC X.                                     00002640
             03 MCHRONOH     PIC X.                                     00002650
             03 MCHRONOV     PIC X.                                     00002660
             03 MCHRONOO     PIC X(7).                                  00002670
           02 DFHMS10 OCCURS   12 TIMES .                               00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MTOTALA      PIC X.                                     00002700
             03 MTOTALC PIC X.                                          00002710
             03 MTOTALP PIC X.                                          00002720
             03 MTOTALH PIC X.                                          00002730
             03 MTOTALV PIC X.                                          00002740
             03 MTOTALO      PIC X(7).                                  00002750
           02 DFHMS11 OCCURS   12 TIMES .                               00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MEXPIRYA     PIC X.                                     00002780
             03 MEXPIRYC     PIC X.                                     00002790
             03 MEXPIRYP     PIC X.                                     00002800
             03 MEXPIRYH     PIC X.                                     00002810
             03 MEXPIRYV     PIC X.                                     00002820
             03 MEXPIRYO     PIC X(9).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLIBERRA  PIC X.                                          00002850
           02 MLIBERRC  PIC X.                                          00002860
           02 MLIBERRP  PIC X.                                          00002870
           02 MLIBERRH  PIC X.                                          00002880
           02 MLIBERRV  PIC X.                                          00002890
           02 MLIBERRO  PIC X(78).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCODTRAA  PIC X.                                          00002920
           02 MCODTRAC  PIC X.                                          00002930
           02 MCODTRAP  PIC X.                                          00002940
           02 MCODTRAH  PIC X.                                          00002950
           02 MCODTRAV  PIC X.                                          00002960
           02 MCODTRAO  PIC X(4).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCICSA    PIC X.                                          00002990
           02 MCICSC    PIC X.                                          00003000
           02 MCICSP    PIC X.                                          00003010
           02 MCICSH    PIC X.                                          00003020
           02 MCICSV    PIC X.                                          00003030
           02 MCICSO    PIC X(5).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNETNAMA  PIC X.                                          00003060
           02 MNETNAMC  PIC X.                                          00003070
           02 MNETNAMP  PIC X.                                          00003080
           02 MNETNAMH  PIC X.                                          00003090
           02 MNETNAMV  PIC X.                                          00003100
           02 MNETNAMO  PIC X(8).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSCREENA  PIC X.                                          00003130
           02 MSCREENC  PIC X.                                          00003140
           02 MSCREENP  PIC X.                                          00003150
           02 MSCREENH  PIC X.                                          00003160
           02 MSCREENV  PIC X.                                          00003170
           02 MSCREENO  PIC X(4).                                       00003180
                                                                                
