      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG0500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG0500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG0500.                                                    00000090
           02  EG05-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG05-TYPLIGNE                                            00000120
               PIC X(0001).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-NOLIGNE                                             00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG05-NOLIGNE                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG05-CONTINUER                                           00000160
               PIC X(0001).                                             00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-SKIPBEFORE                                          00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG05-SKIPBEFORE                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-SKIPAFTER                                           00000200
      *        PIC S9(4) COMP.                                          00000210
      *--                                                                       
           02  EG05-SKIPAFTER                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-RUPTIMPR                                            00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  EG05-RUPTIMPR                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG05-NOMCHAMPC                                           00000240
               PIC X(0018).                                             00000250
           02  EG05-NOMCHAMPS                                           00000260
               PIC X(0008).                                             00000270
           02  EG05-SOUSTABLE                                           00000280
               PIC X(0005).                                             00000290
           02  EG05-DSYST                                               00000300
               PIC S9(13) COMP-3.                                       00000310
      *                                                                 00000320
      *---------------------------------------------------------        00000330
      *   LISTE DES FLAGS DE LA TABLE RVEG0500                          00000340
      *---------------------------------------------------------        00000350
      *                                                                 00000360
       01  RVEG0500-FLAGS.                                              00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-NOMETAT-F                                           00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  EG05-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-TYPLIGNE-F                                          00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  EG05-TYPLIGNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-NOLIGNE-F                                           00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  EG05-NOLIGNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-CONTINUER-F                                         00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  EG05-CONTINUER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-SKIPBEFORE-F                                        00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  EG05-SKIPBEFORE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-SKIPAFTER-F                                         00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  EG05-SKIPAFTER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-RUPTIMPR-F                                          00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  EG05-RUPTIMPR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-NOMCHAMPC-F                                         00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  EG05-NOMCHAMPC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-NOMCHAMPS-F                                         00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  EG05-NOMCHAMPS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-SOUSTABLE-F                                         00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  EG05-SOUSTABLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG05-DSYST-F                                             00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  EG05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000600
                                                                                
