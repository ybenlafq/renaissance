      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG1500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG1500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG1500.                                                    00000090
           02  EG15-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG15-TYPLIGNE                                            00000120
               PIC X(0001).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-NOLIGNE                                             00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG15-NOLIGNE                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG15-CONTINUER                                           00000160
               PIC X(0001).                                             00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-POSCHAMP                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG15-POSCHAMP                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG15-NOMCHAMPT                                           00000200
               PIC X(0018).                                             00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-RUPTRAZ                                             00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  EG15-RUPTRAZ                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG15-DSYST                                               00000240
               PIC S9(13) COMP-3.                                       00000250
      *                                                                 00000260
      *---------------------------------------------------------        00000270
      *   LISTE DES FLAGS DE LA TABLE RVEG1500                          00000280
      *---------------------------------------------------------        00000290
      *                                                                 00000300
       01  RVEG1500-FLAGS.                                              00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-NOMETAT-F                                           00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  EG15-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-TYPLIGNE-F                                          00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  EG15-TYPLIGNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-NOLIGNE-F                                           00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  EG15-NOLIGNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-CONTINUER-F                                         00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  EG15-CONTINUER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-POSCHAMP-F                                          00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  EG15-POSCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-NOMCHAMPT-F                                         00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  EG15-NOMCHAMPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-RUPTRAZ-F                                           00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  EG15-RUPTRAZ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG15-DSYST-F                                             00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  EG15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000480
                                                                                
