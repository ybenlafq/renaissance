      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ HOST --> LOCAL                                        
      * TRAITEMENTS MQ - COTE HOST                                              
      *****************************************************************         
      *                                                                         
      *                                                                         
      * CODRET : 1 = MQOPEN NON OK                                              
      *          2 = MQPUT    //                                                
      *          3 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          4 = SELECT SS-TABLE MQPUT SQLCODE < 0                          
      *          5 = MQCLOSE NON OK                                             
      *                                                                         
      *                                                                         
       01  COMM-MQ20-APPLI.                                                     
           05  COMM-MQ20-ENTREE.                                                
               10  COMM-MQ20-NSOC            PIC X(03).                         
               10  COMM-MQ20-NLIEU           PIC X(03).                         
               10  COMM-MQ20-FONCTION        PIC X(03).                         
               10  COMM-MQ20-NOMPROG         PIC X(05).                         
               10  COMM-MQ20-MSGID           PIC X(24).                         
               10  COMM-MQ20-CORRELID        PIC X(24).                         
               10  COMM-MQ20-LONGMES         PIC S9(6).                         
               10  COMM-MQ20-CODRET          PIC XX.                            
               10  COMM-MQ20-ERREUR          PIC X(52).                         
           05  COMM-MQ20-MESS.                                                  
               10  COMM-MQ20-MESSAGE         PIC X(5878).                       
                                                                                
