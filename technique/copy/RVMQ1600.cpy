      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVMQ1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMQ1600                         
      *   CALENDRIER OUVERTURES MAGASINS POUR CONTROLES MQ                      
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMQ1600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMQ1600.                                                            
      *}                                                                        
           02  MQ16-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  MQ16-NLIEU                                                       
               PIC X(0003).                                                     
           02  MQ16-LUNDI                                                       
               PIC X(0001).                                                     
           02  MQ16-MARDI                                                       
               PIC X(0001).                                                     
           02  MQ16-MERCREDI                                                    
               PIC X(0001).                                                     
           02  MQ16-JEUDI                                                       
               PIC X(0001).                                                     
           02  MQ16-VENDREDI                                                    
               PIC X(0001).                                                     
           02  MQ16-SAMEDI                                                      
               PIC X(0001).                                                     
           02  MQ16-DIMANCHE                                                    
               PIC X(0001).                                                     
           02  MQ16-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVMQ1600                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMQ1602-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMQ1602-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-LUNDI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-LUNDI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-MARDI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-MARDI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-MERCREDI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-MERCREDI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-JEUDI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-JEUDI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-VENDREDI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-VENDREDI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-SAMEDI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-SAMEDI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-DIMANCHE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-DIMANCHE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ16-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ16-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
