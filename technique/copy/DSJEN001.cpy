      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  JEN001-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  JEN001-E00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'JEN001                                     ECARTS DES MONT'.        
               10  FILLER                         PIC X(58) VALUE               
           'ANTS ENCAISSES APPLICATION DE GESTION / ENCAISSEMENT      '.        
               10  FILLER                         PIC X(55) VALUE               
           '                                              EDITE LE '.           
               10  JEN001-E00-DATEDITE            PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  JEN001-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  JEN001-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  JEN001-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E05  --------------------------        
           05  JEN001-E05.                                                      
               10  FILLER                         PIC X(07) VALUE               
           'LIEU : '.                                                           
               10  JEN001-E05-SLRAPPRO            PIC X(06).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JEN001-E05-LIBLIEU             PIC X(20).                    
               10  FILLER                         PIC X(42) VALUE               
           '                                   DATE : '.                        
               10  JEN001-E05-DATEJ               PIC X(10).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(54) VALUE               
           '                                                      '.            
      *--------------------------- LIGNE E10  --------------------------        
           05  JEN001-E10.                                                      
               10  FILLER                         PIC X(10) VALUE               
           'DOMAINE : '.                                                        
               10  JEN001-E10-DOMAINE             PIC X(05).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                        CO'.        
               10  FILLER                         PIC X(09) VALUE               
           'MPTEUR : '.                                                         
               10  JEN001-E10-COMPTEUR            PIC X(02).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(56) VALUE               
           '                                                        '.          
      *--------------------------- LIGNE E15  --------------------------        
           05  JEN001-E15.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-------------------+-------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------+-----------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(24) VALUE               
           '---------+-------------+'.                                          
      *--------------------------- LIGNE E20  --------------------------        
           05  JEN001-E20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                   !                    DONNEES CAISSE   '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                        !             DONN'.        
               10  FILLER                         PIC X(58) VALUE               
           'EES APPLICATION DE GESTION                                '.        
               10  FILLER                         PIC X(24) VALUE               
           '         !             !'.                                          
      *--------------------------- LIGNE E25  --------------------------        
           05  JEN001-E25.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                   !-------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------+-----------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(24) VALUE               
           '---------+             +'.                                          
      *--------------------------- LIGNE E30  --------------------------        
           05  JEN001-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!    REFERENCE      !   DATE      ST�/LIEU    N�        N�'.        
               10  FILLER                         PIC X(58) VALUE               
           '        N�      MONTANT      TOTAL      !    DATE         '.        
               10  FILLER                         PIC X(58) VALUE               
           ' DATE    STE/LIEU   REFERENCE 2    NSEQ    MONTANT       T'.        
               10  FILLER                         PIC X(24) VALUE               
           'OTAL     !   ECARTS    !'.                                          
      *--------------------------- LIGNE E35  --------------------------        
           05  JEN001-E35.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                   ! ENCAISSE    ENCAISSE  CAISSE   TRANS'.        
               10  FILLER                         PIC X(58) VALUE               
           'AC    LIGNE     ENCAISSE     ENCAISSE   !   D EFFET      D'.        
               10  FILLER                         PIC X(58) VALUE               
           'E SAISIE D ENCAISS                        ENCAISSE     ENC'.        
               10  FILLER                         PIC X(24) VALUE               
           'AISSE    !             !'.                                          
      *--------------------------- LIGNE E40  --------------------------        
           05  JEN001-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                   !                                     '.        
               10  FILLER                         PIC X(58) VALUE               
           '                            /REFERENCE  !                 '.        
               10  FILLER                         PIC X(58) VALUE               
           '         THEORIQUE                                    /REF'.        
               10  FILLER                         PIC X(24) VALUE               
           'ERENCE   !             !'.                                          
      *--------------------------- LIGNE E45  --------------------------        
           05  JEN001-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-------------------+-------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------+-----------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(24) VALUE               
           '---------+-------------+'.                                          
      *--------------------------- LIGNE D05  --------------------------        
           05  JEN001-D05.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  JEN001-D05-REFERENCE           PIC X(15).                    
               10  FILLER                         PIC X(05) VALUE               
           '   ! '.                                                             
               10  JEN001-D05-DTE-ENCAISS         PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-SLENC               PIC X(06).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-NCAISSE             PIC X(03).                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
               10  JEN001-D05-NTRANS              PIC X(09).                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  JEN001-D05-NSEQ                PIC X(03).                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
               10  JEN001-D05-MTT-NEM                                           
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-TOT-NEM                                           
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  JEN001-D05-DTE-EFFET           PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-DTE-SAISIE          PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-SLENCTHEO           PIC X(06).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-REFERENC2           PIC X(15).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  JEN001-D05-NSEQNASL            PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  JEN001-D05-MTT-NASL                                          
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  JEN001-D05-TOT-NASL                                          
                   PIC -ZZZZZZ9,99                    BLANK WHEN ZERO.          
               10  FILLER                         PIC X(07) VALUE               
           '   !   '.                                                           
               10  JEN001-D05-ECART                                             
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
      *--------------------------- LIGNE D10  --------------------------        
           05  JEN001-D10.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-------------------+-------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------+-----------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(24) VALUE               
           '---------+-------------+'.                                          
      *--------------------------- LIGNE T05  --------------------------        
           05  JEN001-T05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!    TOTAL          !                                     '.        
               10  FILLER                         PIC X(28) VALUE               
           '                            '.                                      
               10  JEN001-T05-TOTAL-NEM                                         
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(58) VALUE               
           '  !                                                       '.        
               10  FILLER                         PIC X(15) VALUE               
           '               '.                                                   
               10  JEN001-T05-TOTAL-NASL                                        
                   PIC -ZZZZZZ9,99                    BLANK WHEN ZERO.          
               10  FILLER                         PIC X(07) VALUE               
           '   !   '.                                                           
               10  JEN001-T05-TOTAL-ECART                                       
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
      *--------------------------- LIGNE T10  --------------------------        
           05  JEN001-T10.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'ECARTS PAR DATE D ENCAISSEMENT(COTE CAISSE) OU DE SAISIE(C'.        
               10  FILLER                         PIC X(58) VALUE               
           'OTE APPLICATION DE GESTION) :                             '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(24) VALUE               
           '                        '.                                          
      *--------------------------- LIGNE T15  --------------------------        
           05  JEN001-T15.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                       DATE              DATE             '.        
               10  FILLER                         PIC X(58) VALUE               
           ' ECART                                                    '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(24) VALUE               
           '                        '.                                          
      *--------------------------- LIGNE T20  --------------------------        
           05  JEN001-T20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                  D ENCAISSEMENT       DE SAISIE          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(24) VALUE               
           '                        '.                                          
      *--------------------------- LIGNE T25  --------------------------        
           05  JEN001-T25.                                                      
               10  FILLER                         PIC X(21) VALUE               
           '                     '.                                             
               10  JEN001-T25-DTE-ENCAISS         PIC X(10).                    
               10  FILLER                         PIC X(08) VALUE               
           '        '.                                                          
               10  JEN001-T25-DTE-SAISIE          PIC X(10).                    
               10  FILLER                         PIC X(08) VALUE               
           '        '.                                                          
               10  JEN001-T25-ECART-DATE                                        
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(15) VALUE               
           '               '.                                                   
      *--------------------------- LIGNE T30  --------------------------        
           05  JEN001-T30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                                        --'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------                                                '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(24) VALUE               
           '                        '.                                          
      *--------------------------- LIGNE T35  --------------------------        
           05  JEN001-T35.                                                      
               10  FILLER                         PIC X(57) VALUE               
           '                                                         '.         
               10  JEN001-T35-TOT-ECART-DTE                                     
                   PIC -ZZZZZ9,99                     BLANK WHEN ZERO.          
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(15) VALUE               
           '               '.                                                   
                                                                                
