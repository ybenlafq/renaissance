      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: elemant dtd                                                00000020
      ***************************************************************** 00000030
       01   EXM03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECRANL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MECRANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MECRANF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MECRANI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECRANMXL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MECRANMXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MECRANMXF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MECRANMXI      PIC X(3).                                  00000290
      * nom  dtd                                                        00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTDL     COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MDTDL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDTDF     PIC X.                                          00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MDTDI     PIC X(20).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLGDTDL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLGDTDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLGDTDF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLGDTDI   PIC X(5).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOCLEGACYL    COMP PIC S9(4).                            00000390
      *--                                                                       
           02 MDOCLEGACYL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDOCLEGACYF    PIC X.                                     00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MDOCLEGACYI    PIC X(10).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROLOGUE1L    COMP PIC S9(4).                            00000430
      *--                                                                       
           02 MPROLOGUE1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MPROLOGUE1F    PIC X.                                     00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MPROLOGUE1I    PIC X(68).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROLOGUE2L    COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MPROLOGUE2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MPROLOGUE2F    PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MPROLOGUE2I    PIC X(79).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELEMENTFL     COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MELEMENTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MELEMENTFF     PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MELEMENTFI     PIC X(10).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTETL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLENTETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTETF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLENTETI  PIC X(54).                                      00000580
      * la liste des element                                            00000590
           02 MLDTDI OCCURS   10 TIMES .                                00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MLCHOIXI     PIC X.                                     00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MNSEQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEQF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNSEQI  PIC X(6).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFOL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINFOF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MINFOI  PIC X(70).                                      00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLIBERRI  PIC X(74).                                      00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCODTRAI  PIC X(4).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MCICSI    PIC X(5).                                       00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MNETNAMI  PIC X(8).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MSCREENI  PIC X(4).                                       00000920
      ***************************************************************** 00000930
      * SDF: elemant dtd                                                00000940
      ***************************************************************** 00000950
       01   EXM03O REDEFINES EXM03I.                                    00000960
           02 FILLER    PIC X(12).                                      00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MDATJOUA  PIC X.                                          00000990
           02 MDATJOUC  PIC X.                                          00001000
           02 MDATJOUP  PIC X.                                          00001010
           02 MDATJOUH  PIC X.                                          00001020
           02 MDATJOUV  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUP  PIC X.                                          00001080
           02 MTIMJOUH  PIC X.                                          00001090
           02 MTIMJOUV  PIC X.                                          00001100
           02 MTIMJOUO  PIC X(5).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MPAGEA    PIC X.                                          00001130
           02 MPAGEC    PIC X.                                          00001140
           02 MPAGEP    PIC X.                                          00001150
           02 MPAGEH    PIC X.                                          00001160
           02 MPAGEV    PIC X.                                          00001170
           02 MPAGEO    PIC X(3).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEMAXA      PIC X.                                     00001200
           02 MPAGEMAXC PIC X.                                          00001210
           02 MPAGEMAXP PIC X.                                          00001220
           02 MPAGEMAXH PIC X.                                          00001230
           02 MPAGEMAXV PIC X.                                          00001240
           02 MPAGEMAXO      PIC X(3).                                  00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MECRANA   PIC X.                                          00001270
           02 MECRANC   PIC X.                                          00001280
           02 MECRANP   PIC X.                                          00001290
           02 MECRANH   PIC X.                                          00001300
           02 MECRANV   PIC X.                                          00001310
           02 MECRANO   PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MECRANMXA      PIC X.                                     00001340
           02 MECRANMXC PIC X.                                          00001350
           02 MECRANMXP PIC X.                                          00001360
           02 MECRANMXH PIC X.                                          00001370
           02 MECRANMXV PIC X.                                          00001380
           02 MECRANMXO      PIC X(3).                                  00001390
      * nom  dtd                                                        00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MDTDA     PIC X.                                          00001420
           02 MDTDC     PIC X.                                          00001430
           02 MDTDP     PIC X.                                          00001440
           02 MDTDH     PIC X.                                          00001450
           02 MDTDV     PIC X.                                          00001460
           02 MDTDO     PIC X(20).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLGDTDA   PIC X.                                          00001490
           02 MLGDTDC   PIC X.                                          00001500
           02 MLGDTDP   PIC X.                                          00001510
           02 MLGDTDH   PIC X.                                          00001520
           02 MLGDTDV   PIC X.                                          00001530
           02 MLGDTDO   PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDOCLEGACYA    PIC X.                                     00001560
           02 MDOCLEGACYC    PIC X.                                     00001570
           02 MDOCLEGACYP    PIC X.                                     00001580
           02 MDOCLEGACYH    PIC X.                                     00001590
           02 MDOCLEGACYV    PIC X.                                     00001600
           02 MDOCLEGACYO    PIC X(10).                                 00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MPROLOGUE1A    PIC X.                                     00001630
           02 MPROLOGUE1C    PIC X.                                     00001640
           02 MPROLOGUE1P    PIC X.                                     00001650
           02 MPROLOGUE1H    PIC X.                                     00001660
           02 MPROLOGUE1V    PIC X.                                     00001670
           02 MPROLOGUE1O    PIC X(68).                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPROLOGUE2A    PIC X.                                     00001700
           02 MPROLOGUE2C    PIC X.                                     00001710
           02 MPROLOGUE2P    PIC X.                                     00001720
           02 MPROLOGUE2H    PIC X.                                     00001730
           02 MPROLOGUE2V    PIC X.                                     00001740
           02 MPROLOGUE2O    PIC X(79).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MELEMENTFA     PIC X.                                     00001770
           02 MELEMENTFC     PIC X.                                     00001780
           02 MELEMENTFP     PIC X.                                     00001790
           02 MELEMENTFH     PIC X.                                     00001800
           02 MELEMENTFV     PIC X.                                     00001810
           02 MELEMENTFO     PIC X(10).                                 00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLENTETA  PIC X.                                          00001840
           02 MLENTETC  PIC X.                                          00001850
           02 MLENTETP  PIC X.                                          00001860
           02 MLENTETH  PIC X.                                          00001870
           02 MLENTETV  PIC X.                                          00001880
           02 MLENTETO  PIC X(54).                                      00001890
      * la liste des element                                            00001900
           02 MLDTDO OCCURS   10 TIMES .                                00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MLCHOIXA     PIC X.                                     00001930
             03 MLCHOIXC     PIC X.                                     00001940
             03 MLCHOIXP     PIC X.                                     00001950
             03 MLCHOIXH     PIC X.                                     00001960
             03 MLCHOIXV     PIC X.                                     00001970
             03 MLCHOIXO     PIC X.                                     00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MNSEQA  PIC X.                                          00002000
             03 MNSEQC  PIC X.                                          00002010
             03 MNSEQP  PIC X.                                          00002020
             03 MNSEQH  PIC X.                                          00002030
             03 MNSEQV  PIC X.                                          00002040
             03 MNSEQO  PIC X(6).                                       00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MINFOA  PIC X.                                          00002070
             03 MINFOC  PIC X.                                          00002080
             03 MINFOP  PIC X.                                          00002090
             03 MINFOH  PIC X.                                          00002100
             03 MINFOV  PIC X.                                          00002110
             03 MINFOO  PIC X(70).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIBERRA  PIC X.                                          00002140
           02 MLIBERRC  PIC X.                                          00002150
           02 MLIBERRP  PIC X.                                          00002160
           02 MLIBERRH  PIC X.                                          00002170
           02 MLIBERRV  PIC X.                                          00002180
           02 MLIBERRO  PIC X(74).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCODTRAA  PIC X.                                          00002210
           02 MCODTRAC  PIC X.                                          00002220
           02 MCODTRAP  PIC X.                                          00002230
           02 MCODTRAH  PIC X.                                          00002240
           02 MCODTRAV  PIC X.                                          00002250
           02 MCODTRAO  PIC X(4).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCICSA    PIC X.                                          00002280
           02 MCICSC    PIC X.                                          00002290
           02 MCICSP    PIC X.                                          00002300
           02 MCICSH    PIC X.                                          00002310
           02 MCICSV    PIC X.                                          00002320
           02 MCICSO    PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNETNAMA  PIC X.                                          00002350
           02 MNETNAMC  PIC X.                                          00002360
           02 MNETNAMP  PIC X.                                          00002370
           02 MNETNAMH  PIC X.                                          00002380
           02 MNETNAMV  PIC X.                                          00002390
           02 MNETNAMO  PIC X(8).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MSCREENA  PIC X.                                          00002420
           02 MSCREENC  PIC X.                                          00002430
           02 MSCREENP  PIC X.                                          00002440
           02 MSCREENH  PIC X.                                          00002450
           02 MSCREENV  PIC X.                                          00002460
           02 MSCREENO  PIC X(4).                                       00002470
                                                                                
