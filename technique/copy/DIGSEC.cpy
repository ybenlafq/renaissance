      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *==> DL1-DBD ==> SEGMENT ************************ AIDA-COBOL *            
      *    DIGDP0      DIGSE   GESTION DES DESTINATIONS            *            
      ************************************************ COPY DIGSEC *            
      *                                                                         
      * DESCRIPTION VARIABLE E/S ----------------------------------*            
      *                                                                         
       01  DIGSE PIC X(064).                                                    
      *                                                                         
       01  DIGSE-SEG0 REDEFINES DIGSE.                                          
      *                                                                         
      *    CLE ACCES DESTINATION                                                
           05 DIGFE0-DST PIC X(009).                                            
      *                                                                         
      *    LIBELLE DESTINATION                                                  
           05 DIGZE0-LIB PIC X(030).                                            
      *                                                                         
      *    ZONE LIBRE                                                           
           05 DIGZE0-FLR PIC X(025).                                            
      *------------------------------------------------------------*            
      * ==> ATTENTION <==                                          *            
      * CETTE DESCRIPTION COBOL-DIGSEC A UNE EQUIVALENCE EN        *            
      * PL1-DIGSE .                                                *            
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .               *            
      *------------------------------------------------------------*            
       EJECT                                                                    
                                                                                
