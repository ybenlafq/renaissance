      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2046                            *        
      ******************************************************************        
       01  RVXM2046.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2046-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2046-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2046-CMARQ.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2046-CMARQ-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2046-CMARQ-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2046-CMARQ-TEXT        PIC X(05).                            
           10 XM2046-LMARQ.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2046-LMARQ-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2046-LMARQ-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2046-LMARQ-TEXT        PIC X(20).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2046                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2046-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2046-CMARQ-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2046-CMARQ-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2046-LMARQ-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2046-LMARQ-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 03      *        
      ******************************************************************        
                                                                                
