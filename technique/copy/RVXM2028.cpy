      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2028                  *        
      ******************************************************************        
       01  RVXM2028.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2028-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2028-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2028-NCODIC          PIC X(07).                                 
           10 XM2028-COPCO           PIC X(03).                                 
           10 XM2028-CODEOPCO        PIC X(15).                                 
           10 XM2028-LIBOPCO         PIC X(30).                                 
           10 XM2028-FLAGREF         PIC X(01).                                 
           10 XM2028-DEFFET          PIC X(08).                                 
           10 XM2028-DFINEFFET       PIC X(08).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2028-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-COPCO-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-COPCO-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-CODEOPCO-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-CODEOPCO-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-LIBOPCO-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-LIBOPCO-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-FLAGREF-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-FLAGREF-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-DEFFET-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2028-DEFFET-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2028-DFINEFFET-F     PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2028-DFINEFFET-F     PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
