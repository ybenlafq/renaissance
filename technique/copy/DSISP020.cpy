      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * PROJET : SRP                                                            
      * DECRIPTION DES LIGNES D'ETAT                                            
      * MEME DESCRIPTION POUR TOUS LES ETATS - ZONES DE TITRE VARIABLES         
      *--> POUR ETATS ISP020 - ISP021 ET ISP023                                 
      *                                                                         
      *                                                                         
      * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
      * NB : CETTE DESCRIPTION SERT EGALEMENT A LA GENERATION DE                
      *      L'ETAT ISP050                                                      
      *      --> LIGNE E03 REMPLACEE PAR E03-BIS                                
      *          LIGNE E04 "     "     " E04-BIS                                
      *          LIGNE E05 "     "     " E05-BIS                                
      *          LIGNE E21 "     "     " E21-BIS                                
      * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
      *                                                                         
      *****************************************************************         
      *                                                                         
       01 ETAT-ISP020.                                                          
      *----------------------------------------------------------------         
      * LIGNES ENTETE                                                           
      *----------------------------------------------------------------         
      *                                                                         
      *--------------------------- LIGNE E00  --------------------------        
       02 ISP020-E00.                                                           
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(16) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E01  --------------------------        
       02 ISP020-E01.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 ISP020-NOMETAT      PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(51) VALUE SPACES.                        
          05 FILLER              PIC X(11) VALUE 'EDITE LE : '.                 
          05 ISP020-DEDITE       PIC X(10) VALUE SPACES.                        
          05  FILLER             PIC X(02) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E02  --------------------------        
       02 ISP020-E02.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(55) VALUE SPACES.                        
          05 FILLER              PIC X(07) VALUE 'PAGE : '.                     
          05 ISP020-NPAGE        PIC ZZZ9  VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E03  --------------------------        
       02 ISP020-E03.                                                           
          05 FILLER          PIC X(52) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'FACTURATION INTERNE.'.            
          05 FILLER          PIC X(42) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *-------------------- LIGNE E03-BIS (ISP051) ---------------------        
       02 ISP020-E03-BIS.                                                       
          05 FILLER          PIC X(42) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'FACTURATION INTERNE '.            
          05 FILLER          PIC X(20) VALUE '- CAPROFEM.         '.            
          05 FILLER          PIC X(32) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E04  --------------------------        
       02 ISP020-E04.                                                           
          05 FILLER          PIC X(40) VALUE SPACES.                            
          05 FILLER          PIC X(18) VALUE 'ETAT DETAILLEE DE '.              
          05 FILLER          PIC X(18) VALUE 'L''ECART SRP / PRMP'.             
          05 FILLER          PIC X(10) VALUE ' EN CUMUL.'.                      
          05 FILLER          PIC X(46) VALUE SPACES.                            
      *                                                                         
      *-------------------- LIGNE E04-BIS (ISP051) ---------------------        
       02 ISP020-E04-BIS.                                                       
          05 FILLER          PIC X(38) VALUE SPACES.                            
          05 FILLER          PIC X(21) VALUE 'EDITION MENSUELLE DE '.           
          05 FILLER          PIC X(19) VALUE 'L''ECART PRAK / PRMP'.            
          05 FILLER          PIC X(10) VALUE ' EN CUMUL.'.                      
          05 FILLER          PIC X(44) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E05  --------------------------        
       02 ISP020-E05.                                                           
          05 FILLER          PIC X(50) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'TABLEAU DE SYNTHESE '.            
          05 ISP020-TYPSYNT  PIC X(46) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *-------------------- LIGNE E05-BIS (ISP051) ---------------------        
       02 ISP020-E05-BIS.                                                       
          05 FILLER          PIC X(56) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'TABLEAU DE SYNTHESE.'.            
          05 FILLER          PIC X(40) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E06  --------------------------        
       02 ISP020-E06.                                                           
          05 FILLER          PIC X(55) VALUE SPACES.                            
          05 FILLER          PIC X(14) VALUE 'MOIS TRAITE : '.                  
          05 ISP020-MOISTRT  PIC X(05) VALUE SPACES.                            
          05 FILLER          PIC X(58) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E10  --------------------------        
       02 ISP020-E10.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE EMETTRICE  : '.         
          05 ISP020-NSOCEMET   PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP020-LSOCEMET   PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E11  --------------------------        
       02 ISP020-E11.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE RECEPTRICE : '.         
          05 ISP020-NSOCRECEP  PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP020-LSOCRECEP  PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E15  --------------------------        
       02 ISP020-E15.                                                           
          05 FILLER            PIC X(13) VALUE '    MARQUE : '.                 
          05 ISP020-CMARQ      PIC X(05) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP020-LMARQ      PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(15) VALUE '   EXERCICE DU '.               
          05 ISP020-DEBEXER.                                                    
             07 ISP020-DEBEXER-MM   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP020-DEBEXER-AA   PIC X(02) VALUE SPACES.                     
          05 FILLER            PIC X(04) VALUE ' AU '.                          
          05 ISP020-FINEXER.                                                    
             07 ISP020-FINEXER-MM   PIC X(02) VALUE SPACES.                     
             07 FILLER              PIC X(01) VALUE '/'.                        
             07 ISP020-FINEXER-AA   PIC X(02) VALUE SPACES.                     
          05 FILLER            PIC X(59) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E16  --------------------------        
       02 ISP020-E16.                                                           
          05 FILLER            PIC X(47) VALUE SPACES.                          
          05 FILLER            PIC X(21) VALUE 'ECHEANCE D''AVOIR  :  '.        
          05 ISP020-DAVOIR.                                                     
             07 ISP020-DAVOIR-MM  PIC X(02) VALUE SPACES.                       
             07 FILLER            PIC X(01) VALUE '/'.                          
             07 ISP020-DAVOIR-AA  PIC X(02) VALUE SPACES.                       
          05 FILLER            PIC X(59) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E20  --------------------------        
       02 ISP020-E20.                                                           
          05 FILLER            PIC X(20) VALUE '                    '.          
          05 FILLER            PIC X(20) VALUE '     .--------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(16) VALUE '-----------.    '.              
      *                                                                         
      *--------------------------- LIGNE E21  --------------------------        
       02 ISP020-E21.                                                           
          05 FILLER                         PIC X(10) VALUE SPACES.             
          05 FILLER                         PIC X(49) VALUE                     
                  '               !       VALO SRP         VALO PRMP'.          
          05 FILLER                         PIC X(57) VALUE                     
           '                      ECART SRP / PRMP                   '.         
          05 FILLER                         PIC X(16) VALUE                     
           '           !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E21-BIS (ISP050) --------------        
       02 ISP020-E21-BIS.                                                       
          05 FILLER                         PIC X(10) VALUE  SPACES.            
          05 FILLER                         PIC X(49) VALUE                     
                  '               !       VALO PRAK        VALO PRMP'.          
          05 FILLER                         PIC X(57) VALUE                     
           '                     ECART PRAK / PRMP                   '.         
          05 FILLER                         PIC X(16) VALUE                     
           '           !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E22  --------------------------        
        02 ISP020-E22.                                                          
           05 FILLER                         PIC X(58) VALUE                    
           '                         !                                '.        
           05 FILLER                         PIC X(58) VALUE                    
           '              H.T.       TAUX           T.V.A.            '.        
           05 FILLER                         PIC X(16) VALUE                    
           ' T.T.C.    !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E23  --------------------------        
        02  ISP020-E23.                                                         
           05 FILLER                         PIC X(58) VALUE                    
           '  .----------------------!--------------------------------'.        
           05 FILLER                         PIC X(58) VALUE                    
           '----+-----------------+--------+-----------------+--------'.        
           05 FILLER                         PIC X(16) VALUE                    
           '-----------!    '.                                                  
      *                                                                         
      *--------------------------- LIGNE D01  --------------------------        
        02 ISP020-D01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP020-TEBRB.                                                     
              10 ISP020-TEBRB-COM   PIC X(05) VALUE SPACES.                     
              10 ISP020-TEBRB-VAL   PIC X(15) VALUE SPACES.                     
           05 FILLER                PIC X(04) VALUE ' !  '.                     
           05 ISP020-VALOSRP        PIC ----------9,99.                         
           05 FILLER                PIC X(03) VALUE SPACES.                     
           05 ISP020-VALOPRMP       PIC ----------9,99.                         
           05 FILLER                PIC X(06) VALUE SPACES.                     
           05 ISP020-ECARTHT        PIC ---------9,99.                          
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP020-TXTVA          PIC Z9,99.                                  
           05 FILLER                PIC X(03) VALUE SPACES.                     
           05 ISP020-ECARTTVA       PIC ----------9,99.                         
           05 FILLER                PIC X(06) VALUE SPACES.                     
           05 ISP020-ECARTTTC       PIC ----------9,99.                         
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T01  --------------------------        
        02 ISP020-T01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP020-TYPE.                                                      
              10 ISP020-TYPE-COM    PIC X(11) VALUE SPACES.                     
              10 ISP020-TYPE-VAL    PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(03) VALUE ' ! '.                      
           05 ISP020-TOTSRP         PIC -----------9,99.                        
           05 FILLER                PIC X(02) VALUE SPACES.                     
           05 ISP020-TOTPRMP        PIC -----------9,99.                        
           05 FILLER                PIC X(05) VALUE SPACES.                     
           05 ISP020-TOTHT          PIC ----------9,99.                         
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP020-TOTTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(02) VALUE SPACES.                     
           05 ISP020-TOTTVA         PIC -----------9,99.                        
           05 FILLER                PIC X(05) VALUE SPACES.                     
           05 ISP020-TOTTTC         PIC -----------9,99.                        
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T10  --------------------------        
        02 ISP020-T10.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP020-TYPE10.                                                    
              10 ISP020-TYPE10-COM  PIC X(11) VALUE SPACES.                     
              10 ISP020-TYPE10-VAL  PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(02) VALUE ' !'.                       
           05 ISP020-GENSRP         PIC ------------9,99.                       
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP020-GENPRMP        PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP020-GENHT          PIC -----------9,99.                        
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP020-GENTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP020-GENTVA         PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP020-GENTTC         PIC ------------9,99.                       
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE B01  --------------------------        
           02  ISP020-B01.                                                      
               05  FILLER                         PIC X(48) VALUE               
           '  ''---------------------------------------------'.                 
               05  FILLER   PIC X(10) VALUE '----------'.                       
               05  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               05  FILLER                         PIC X(16) VALUE               
           '-----------''    '.                                                 
      *                                                                         
      *----------------------- LIGNE VIDE DANS TABLEAU -----------------        
           02  ISP020-TAB-VIDE.                                                 
               05  FILLER                         PIC X(58) VALUE               
           '  !                      !                                '.        
               05  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               05  FILLER                         PIC X(16) VALUE               
           '           !    '.                                                  
      *                                                                         
                                                                                
