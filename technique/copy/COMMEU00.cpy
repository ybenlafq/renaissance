      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MEU00-LONG-COMMAREA         PIC S9(4) COMP VALUE +4096. 00000010
      *--                                                                       
       01  COMM-MEU00-LONG-COMMAREA         PIC S9(4) COMP-5 VALUE              
                                                                  +4096.        
      *}                                                                        
       01  COMM-MEU00-APPLI.                                            00000011
           02 COMM-MEU00-ZONES-ENTREE.                                  00000020
              05 COMM-MEU00-NSOCIETE           PIC X(03).               00000021
              05 COMM-MEU00-CDEVORIG           PIC X(03).               00000030
              05 COMM-MEU00-CDEVDEST           PIC X(03).               00000031
              05 COMM-MEU00-DEFFET             PIC X(08).               00000040
              05 COMM-MEU00-PVALE              PIC S9(15)      COMP-3.  00000050
              05 COMM-MEU00-PVALE-1 REDEFINES  COMM-MEU00-PVALE         00000051
                                               PIC S9(14)V9    COMP-3.  00000052
              05 COMM-MEU00-PVALE-2 REDEFINES  COMM-MEU00-PVALE         00000053
                                               PIC S9(13)V9(2) COMP-3.  00000054
              05 COMM-MEU00-PVALE-3 REDEFINES  COMM-MEU00-PVALE         00000055
                                               PIC S9(12)V9(3) COMP-3.  00000056
              05 COMM-MEU00-PVALE-4 REDEFINES  COMM-MEU00-PVALE         00000057
                                               PIC S9(11)V9(4) COMP-3.  00000058
              05 COMM-MEU00-PVALE-5 REDEFINES  COMM-MEU00-PVALE         00000059
                                               PIC S9(10)V9(5) COMP-3.  00000060
              05 COMM-MEU00-PVALE-6 REDEFINES  COMM-MEU00-PVALE         00000061
                                               PIC S9(09)V9(6) COMP-3.  00000063
              05 COMM-MEU00-NBDECIM-E          PIC X.                   00000064
              05 COMM-MEU00-NBDECIM-ES         PIC X.                   00000065
           02 COMM-MEU00-ZONES-SORTIE.                                  00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MEU00-CODRET      COMP   PIC  S9(4).              00000110
      *--                                                                       
              05 COMM-MEU00-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MEU00-LMESS              PIC X(58).               00000120
              05 COMM-MEU00-NBDECIM-S          PIC X.                   00000121
              05 COMM-MEU00-PVALS              PIC S9(15)      COMP-3.  00000122
              05 COMM-MEU00-PVALS-1 REDEFINES  COMM-MEU00-PVALS         00000123
                                               PIC S9(14)V9    COMP-3.  00000124
              05 COMM-MEU00-PVALS-2 REDEFINES  COMM-MEU00-PVALS         00000125
                                               PIC S9(13)V9(2) COMP-3.  00000126
              05 COMM-MEU00-PVALS-3 REDEFINES  COMM-MEU00-PVALS         00000127
                                               PIC S9(12)V9(3) COMP-3.  00000128
              05 COMM-MEU00-PVALS-4 REDEFINES  COMM-MEU00-PVALS         00000129
                                               PIC S9(11)V9(4) COMP-3.  00000130
              05 COMM-MEU00-PVALS-5 REDEFINES  COMM-MEU00-PVALS         00000140
                                               PIC S9(10)V9(5) COMP-3.  00000150
              05 COMM-MEU00-PVALS-6 REDEFINES  COMM-MEU00-PVALS         00000160
                                               PIC S9(09)V9(6) COMP-3.  00000170
              05 COMM-MEU00-PTAUX              PIC S9(5)V9(10) COMP-3.  00000180
              05 COMM-MEU00-PEURO              PIC S9(03)V9(2) COMP-3.  00000190
           02 COMM-MEU00-FILLER                PIC X(3989).             00000100
                                                                                
