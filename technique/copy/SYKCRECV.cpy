      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **********************************************===>CICS XXX******          
      * RECEPTION DE LA MAP                                                     
      *****************************************************************         
      *                                                                         
       RECEIVE-MAP SECTION.                                                     
      *                                                                         
           MOVE EIBAID              TO WORKAID.                                 
           PERFORM INQUIRE-TRANSACTION.                                         
      *    IF NOM-PROG-TACHE = 'TGM99   '                                       
      *       PERFORM LECTURE-TS-MAP                                            
      *    ELSE                                                                 
              PERFORM LECTURE-MAP.                                              
      *    END-IF.                                                              
      *                                                                         
           IF TOUCHE-CLEAR                                                      
              MOVE 1 TO ETAT-ECRAN                                              
           ELSE                                                                 
              MOVE 0 TO ETAT-ECRAN                                              
           END-IF.                                                              
      *                                                                         
       LECTURE-TS-MAP SECTION.                                                  
      *                                                                         
           STRING 'GMMP'                                                        
                  EIBTRMID                                                      
                  DELIMITED BY SIZE INTO IDENT-TS.                              
           MOVE Z-COMMAREA-LONG-MAP TO LONG-TS.                                 
           MOVE 1                   TO RANG-TS.                                 
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READQ TS QUEUE(IDENT-TS)                                       
                                INTO(Z-MAP)                                     
                                LENGTH(LONG-TS)                                 
                                ITEM(RANG-TS)                                   
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��Yi00031   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-MAP LONG-TS DF        
      *dfhei*     RANG-TS.                                                      
      *dfhei*} decommente EXEC                                                  
      *                                                                         
      **********************************************===>CICS XXX******          
      * RECEPTION DE LA MAP                                                     
      *****************************************************************         
      *                                                                         
       LECTURE-MAP SECTION.                                                     
      *===============================================================*         
      *  CETTE PARTIE DE LA BRIQUE PERMET DE CAPTER LA TOUCHE HELP    *         
      *  SANS FAIRE DE RECEIVE, ET DE SE DEBRANCHER AU PROGRAMME      *         
      *  DE HELP IMMEDIATEMENT.                                       *         
      *===============================================================*         
           IF TOUCHE-PA2                                                        
           OR TOUCHE-PF2                                                        
              MOVE NOM-MAP             TO Z-COMMAREA-NOM-MAP                    
              MOVE NOM-MAPSET          TO Z-COMMAREA-NOM-MAPSET                 
              MOVE NOM-TACHE           TO Z-COMMAREA-NOM-TACHE                  
              MOVE LENGTH OF Z-MAP     TO Z-COMMAREA-LONG-MAP                   
              MOVE LONG-COMMAREA       TO Z-COMMAREA-LONG                       
              MOVE 'THELP'             TO NOM-PROG-XCTL                         
              MOVE NOM-PROG            TO COM-PGMPRC                            
              PERFORM XCTL-PROG-COMMAREA                                        
           END-IF.                                                              
      *===============================================================*         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS RECEIVE MAP (NOM-MAP)                                          
                         MAPSET (NOM-MAPSET)                                    
                         INTO (Z-MAP)                                           
                         NOHANDLE                                               
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�	��00059   ' TO DFHEIV0                             
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHDUMMY           
      *dfhei*     NOM-MAPSET.                                                   
      *dfhei*} decommente EXEC                                                  
      *                                                                         
           MOVE EIBRCODE TO EIB-RCODE.                                          
           IF   EIB-NORMAL                                                      
                MOVE 0 TO ETAT-ECRAN                                            
           ELSE                                                                 
                IF   EIB-MAPFAIL                                                
                     MOVE 1 TO ETAT-ECRAN                                       
                ELSE                                                            
                     GO TO ABANDON-CICS                                         
                END-IF                                                          
           END-IF.                                                              
                                                                                
                                                                                
