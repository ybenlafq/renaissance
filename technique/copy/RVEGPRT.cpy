      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EGPRT IMPRIM. AS400 POUR ETAT/TERM     *        
      *----------------------------------------------------------------*        
       01  RVEGPRT.                                                             
           05  EGPRT-CTABLEG2    PIC X(15).                                     
           05  EGPRT-CTABLEG2-REDEF REDEFINES EGPRT-CTABLEG2.                   
               10  EGPRT-CETAT           PIC X(10).                             
               10  EGPRT-CTERM           PIC X(04).                             
           05  EGPRT-WTABLEG     PIC X(80).                                     
           05  EGPRT-WTABLEG-REDEF  REDEFINES EGPRT-WTABLEG.                    
               10  EGPRT-CIMPRIM         PIC X(10).                             
               10  EGPRT-LIBELLE         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEGPRT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGPRT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EGPRT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGPRT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EGPRT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
