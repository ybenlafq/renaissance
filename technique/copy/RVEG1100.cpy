      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG1100                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG1100                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG1100.                                                    00000090
           02  EG11-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG11-TYPLIGNE                                            00000120
               PIC X(0001).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-NOLIGNE                                             00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG11-NOLIGNE                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG11-CONTINUER                                           00000160
               PIC X(0001).                                             00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-POSCHAMP                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG11-POSCHAMP                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-RUPTIMPR                                            00000200
      *        PIC S9(4) COMP.                                          00000210
      *--                                                                       
           02  EG11-RUPTIMPR                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG11-SOUSTABLE                                           00000220
               PIC X(0005).                                             00000230
           02  EG11-NOMCHAMPS                                           00000240
               PIC X(0008).                                             00000250
           02  EG11-NOMCHAMPC                                           00000260
               PIC X(0018).                                             00000270
           02  EG11-CHCOND1                                             00000280
               PIC X(0018).                                             00000290
           02  EG11-CONDITION                                           00000300
               PIC X(0002).                                             00000310
           02  EG11-CHCOND2                                             00000320
               PIC X(0018).                                             00000330
           02  EG11-DSYST                                               00000340
               PIC S9(13) COMP-3.                                       00000350
           02  EG11-LIBCHAMP.                                           00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        49  EG11-LIBCHAMP-L                                      00000370
      *            PIC S9(4) COMP.                                      00000380
      *--                                                                       
               49  EG11-LIBCHAMP-L                                              
                   PIC S9(4) COMP-5.                                            
      *}                                                                        
               49  EG11-LIBCHAMP-V                                      00000390
                   PIC X(0066).                                         00000400
      *                                                                 00000410
      *---------------------------------------------------------        00000420
      *   LISTE DES FLAGS DE LA TABLE RVEG1100                          00000430
      *---------------------------------------------------------        00000440
      *                                                                 00000450
       01  RVEG1100-FLAGS.                                              00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-NOMETAT-F                                           00000470
      *        PIC S9(4) COMP.                                          00000480
      *--                                                                       
           02  EG11-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-TYPLIGNE-F                                          00000490
      *        PIC S9(4) COMP.                                          00000500
      *--                                                                       
           02  EG11-TYPLIGNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-NOLIGNE-F                                           00000510
      *        PIC S9(4) COMP.                                          00000520
      *--                                                                       
           02  EG11-NOLIGNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-CONTINUER-F                                         00000530
      *        PIC S9(4) COMP.                                          00000540
      *--                                                                       
           02  EG11-CONTINUER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-POSCHAMP-F                                          00000550
      *        PIC S9(4) COMP.                                          00000560
      *--                                                                       
           02  EG11-POSCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-RUPTIMPR-F                                          00000570
      *        PIC S9(4) COMP.                                          00000580
      *--                                                                       
           02  EG11-RUPTIMPR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-SOUSTABLE-F                                         00000590
      *        PIC S9(4) COMP.                                          00000600
      *--                                                                       
           02  EG11-SOUSTABLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-NOMCHAMPS-F                                         00000610
      *        PIC S9(4) COMP.                                          00000620
      *--                                                                       
           02  EG11-NOMCHAMPS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-NOMCHAMPC-F                                         00000630
      *        PIC S9(4) COMP.                                          00000640
      *--                                                                       
           02  EG11-NOMCHAMPC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-CHCOND1-F                                           00000650
      *        PIC S9(4) COMP.                                          00000660
      *--                                                                       
           02  EG11-CHCOND1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-CONDITION-F                                         00000670
      *        PIC S9(4) COMP.                                          00000680
      *--                                                                       
           02  EG11-CONDITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-CHCOND2-F                                           00000690
      *        PIC S9(4) COMP.                                          00000700
      *--                                                                       
           02  EG11-CHCOND2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-DSYST-F                                             00000710
      *        PIC S9(4) COMP.                                          00000720
      *--                                                                       
           02  EG11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG11-LIBCHAMP-F                                          00000730
      *        PIC S9(4) COMP.                                          00000740
      *--                                                                       
           02  EG11-LIBCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000750
                                                                                
