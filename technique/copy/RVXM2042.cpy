      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2042                            *        
      ******************************************************************        
       01  RVXM2042.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2042-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2042-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2042-CFAM.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2042-CFAM-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2042-CFAM-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2042-CFAM-TEXT         PIC X(05).                            
           10 XM2042-CDESCRIPTIF.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2042-CDESCRIPTIF-LEN   PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2042-CDESCRIPTIF-LEN   PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2042-CDESCRIPTIF-TEXT  PIC X(05).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2042                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2042-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2042-CFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2042-CFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2042-CDESCRIPTIF-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2042-CDESCRIPTIF-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 03      *        
      ******************************************************************        
                                                                                
