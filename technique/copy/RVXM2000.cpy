      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR LA VUE RVXM2000                         *        
      ******************************************************************        
       01  RVXM2000.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM20-REFID             USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM20-REFID PIC X(40).                                             
      *}                                                                        
           10 XM20-SOURCE.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM20-SOURCE-LEN     PIC S9(4) USAGE COMP.                      
      *--                                                                       
              49 XM20-SOURCE-LEN     PIC S9(4) COMP-5.                          
      *}                                                                        
              49 XM20-SOURCE-TEXT    PIC X(32).                                 
           10 XM20-REFERENCE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM20-REFERENCE-LEN  PIC S9(4) USAGE COMP.                      
      *--                                                                       
              49 XM20-REFERENCE-LEN  PIC S9(4) COMP-5.                          
      *}                                                                        
              49 XM20-REFERENCE-TEXT PIC X(32).                                 
           10 XM20-STATUT            PIC S9(3)V USAGE COMP-3.                   
           10 XM20-DCREATION         PIC X(26).                                 
           10 XM20-DMAJ              PIC X(26).                                 
           10 XM20-XDATA             USAGE SQL TYPE IS XML AS CLOB(1M).         
      ******************************************************************        
      * FLAGS                                                          *        
      ******************************************************************        
       01  RVXM20-F.                                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM20-SOURCE-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 XM20-SOURCE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM20-REFERENCE-F     PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 XM20-REFERENCE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM20-STATUT-F        PIC S9(4) USAGE COMP.                        
      *                                                                         
      *--                                                                       
           10 XM20-STATUT-F        PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
