      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *==> DL1-DBD ==> SEGMENT ************************** AIDA-COBOL *          
      *    DIGDP0      DIGSC   LIGNE ETAT CICS                       *          
      ************************************************** COPY DIGSCC *          
      *                                                                         
      * DESCRIPTION VARIABLE E/S -------------------------------------          
      *                                                                         
       01  DIGSC PIC X(158).                                                    
      *                                                                         
       01  DIGSC-SEG0 REDEFINES DIGSC.                                          
      *                                                                         
      *    LONGUEUR DU SEGMENT                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZC0-PFX-LEN    PIC 9(004) COMP.                                
      *--                                                                       
           05 DIGZC0-PFX-LEN    PIC 9(004) COMP-5.                              
      *}                                                                        
      *                                                                         
      *    CLE ACCES NON-UNIQUE                                                 
      *       DATE  EDITION AAMMJJ                                              
      *       HEURE EDITION HHMMSS                                              
           05 DIGFC0-PFX.                                                       
              10 DIGFC1-PFX-DAT PIC 9(006).                                     
              10 DIGFC2-PFX-HEU PIC 9(006).                                     
      *                                                                         
      *    IDENTIFICATION CICS                                                  
      *       NUMERO DE TACHE CICS                                              
      *       NUMERO DE TERMINAL EIBTRMIB                                       
           05 DIGZC0-CICS.                                                      
              10 DIGFC3-PFX-TAS PIC 9(007).                                     
              10 DIGFC4-PFX-TRM PIC X(004).                                     
      *                                                                         
      *    MISE EN FORME DE LA LIGNE                                            
      *       CARACTERE MISE EN PAGE ASA                                        
      *       LIGNE ETATTERMINAL EIBTRMIB                                       
           05 DIGZC0-LIGN.                                                      
              10 DIGZC0-MEP-ASA PIC X.                                          
              10 DIGZC0-EDI-LIG PIC X(132).                                     
      *--------------------------------------------------------*                
      * ==> ATTENTION <==                                      *                
      * CETTE DESCRIPTION COBOL-DIGSCC A UNE CORRESPONDANCE EN *                
      * PL1-DIGSC .                                            *                
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .           *                
      *--------------------------------------------------------*                
       EJECT                                                                    
                                                                                
