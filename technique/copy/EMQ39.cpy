      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMQ39   EMQ39                                              00000020
      ***************************************************************** 00000030
       01   EMQ39I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUEUEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MQUEUEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQUEUEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MQUEUEI   PIC X(24).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERSISL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MPERSISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPERSISF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPERSISI  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATEI    PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHEUREL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MHEUREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MHEUREF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MHEUREI   PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMSGIDL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MMSGIDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMSGIDF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MMSGIDI   PIC X(24).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTAILLEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTAILLEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTAILLEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTAILLEI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRIORL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPRIORL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPRIORF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPRIORI   PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCOPCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCOPCOF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCOPCOI   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORRELL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCORRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCORRELF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCORRELI  PIC X(24).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLISTPGRL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLISTPGRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLISTPGRF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLISTPGRI      PIC X(28).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDSIL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MIDSIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIDSIF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MIDSII    PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLEL     COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCLEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCLEF     PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCLEI     PIC X(45).                                      00000650
           02 MLIGNED OCCURS   12 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNEL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIGNEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIGNEF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIGNEI      PIC X(78).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(78).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNBPAGESI      PIC X(3).                                  00000940
      ***************************************************************** 00000950
      * SDF: EMQ39   EMQ39                                              00000960
      ***************************************************************** 00000970
       01   EMQ39O REDEFINES EMQ39I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNPAGEA   PIC X.                                          00001150
           02 MNPAGEC   PIC X.                                          00001160
           02 MNPAGEP   PIC X.                                          00001170
           02 MNPAGEH   PIC X.                                          00001180
           02 MNPAGEV   PIC X.                                          00001190
           02 MNPAGEO   PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MQUEUEA   PIC X.                                          00001220
           02 MQUEUEC   PIC X.                                          00001230
           02 MQUEUEP   PIC X.                                          00001240
           02 MQUEUEH   PIC X.                                          00001250
           02 MQUEUEV   PIC X.                                          00001260
           02 MQUEUEO   PIC X(24).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MPERSISA  PIC X.                                          00001290
           02 MPERSISC  PIC X.                                          00001300
           02 MPERSISP  PIC X.                                          00001310
           02 MPERSISH  PIC X.                                          00001320
           02 MPERSISV  PIC X.                                          00001330
           02 MPERSISO  PIC X.                                          00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATEA    PIC X.                                          00001360
           02 MDATEC    PIC X.                                          00001370
           02 MDATEP    PIC X.                                          00001380
           02 MDATEH    PIC X.                                          00001390
           02 MDATEV    PIC X.                                          00001400
           02 MDATEO    PIC X(8).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MHEUREA   PIC X.                                          00001430
           02 MHEUREC   PIC X.                                          00001440
           02 MHEUREP   PIC X.                                          00001450
           02 MHEUREH   PIC X.                                          00001460
           02 MHEUREV   PIC X.                                          00001470
           02 MHEUREO   PIC X(4).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MMSGIDA   PIC X.                                          00001500
           02 MMSGIDC   PIC X.                                          00001510
           02 MMSGIDP   PIC X.                                          00001520
           02 MMSGIDH   PIC X.                                          00001530
           02 MMSGIDV   PIC X.                                          00001540
           02 MMSGIDO   PIC X(24).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MTAILLEA  PIC X.                                          00001570
           02 MTAILLEC  PIC X.                                          00001580
           02 MTAILLEP  PIC X.                                          00001590
           02 MTAILLEH  PIC X.                                          00001600
           02 MTAILLEV  PIC X.                                          00001610
           02 MTAILLEO  PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MPRIORA   PIC X.                                          00001640
           02 MPRIORC   PIC X.                                          00001650
           02 MPRIORP   PIC X.                                          00001660
           02 MPRIORH   PIC X.                                          00001670
           02 MPRIORV   PIC X.                                          00001680
           02 MPRIORO   PIC X(2).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCOPCOA   PIC X.                                          00001710
           02 MCOPCOC   PIC X.                                          00001720
           02 MCOPCOP   PIC X.                                          00001730
           02 MCOPCOH   PIC X.                                          00001740
           02 MCOPCOV   PIC X.                                          00001750
           02 MCOPCOO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCORRELA  PIC X.                                          00001780
           02 MCORRELC  PIC X.                                          00001790
           02 MCORRELP  PIC X.                                          00001800
           02 MCORRELH  PIC X.                                          00001810
           02 MCORRELV  PIC X.                                          00001820
           02 MCORRELO  PIC X(24).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLISTPGRA      PIC X.                                     00001850
           02 MLISTPGRC PIC X.                                          00001860
           02 MLISTPGRP PIC X.                                          00001870
           02 MLISTPGRH PIC X.                                          00001880
           02 MLISTPGRV PIC X.                                          00001890
           02 MLISTPGRO      PIC X(28).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MIDSIA    PIC X.                                          00001920
           02 MIDSIC    PIC X.                                          00001930
           02 MIDSIP    PIC X.                                          00001940
           02 MIDSIH    PIC X.                                          00001950
           02 MIDSIV    PIC X.                                          00001960
           02 MIDSIO    PIC X(10).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCLEA     PIC X.                                          00001990
           02 MCLEC     PIC X.                                          00002000
           02 MCLEP     PIC X.                                          00002010
           02 MCLEH     PIC X.                                          00002020
           02 MCLEV     PIC X.                                          00002030
           02 MCLEO     PIC X(45).                                      00002040
           02 DFHMS1 OCCURS   12 TIMES .                                00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MLIGNEA      PIC X.                                     00002070
             03 MLIGNEC PIC X.                                          00002080
             03 MLIGNEP PIC X.                                          00002090
             03 MLIGNEH PIC X.                                          00002100
             03 MLIGNEV PIC X.                                          00002110
             03 MLIGNEO      PIC X(78).                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIBERRA  PIC X.                                          00002140
           02 MLIBERRC  PIC X.                                          00002150
           02 MLIBERRP  PIC X.                                          00002160
           02 MLIBERRH  PIC X.                                          00002170
           02 MLIBERRV  PIC X.                                          00002180
           02 MLIBERRO  PIC X(78).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCODTRAA  PIC X.                                          00002210
           02 MCODTRAC  PIC X.                                          00002220
           02 MCODTRAP  PIC X.                                          00002230
           02 MCODTRAH  PIC X.                                          00002240
           02 MCODTRAV  PIC X.                                          00002250
           02 MCODTRAO  PIC X(4).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCICSA    PIC X.                                          00002280
           02 MCICSC    PIC X.                                          00002290
           02 MCICSP    PIC X.                                          00002300
           02 MCICSH    PIC X.                                          00002310
           02 MCICSV    PIC X.                                          00002320
           02 MCICSO    PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNETNAMA  PIC X.                                          00002350
           02 MNETNAMC  PIC X.                                          00002360
           02 MNETNAMP  PIC X.                                          00002370
           02 MNETNAMH  PIC X.                                          00002380
           02 MNETNAMV  PIC X.                                          00002390
           02 MNETNAMO  PIC X(8).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MSCREENA  PIC X.                                          00002420
           02 MSCREENC  PIC X.                                          00002430
           02 MSCREENP  PIC X.                                          00002440
           02 MSCREENH  PIC X.                                          00002450
           02 MSCREENV  PIC X.                                          00002460
           02 MSCREENO  PIC X(4).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNBPAGESA      PIC X.                                     00002490
           02 MNBPAGESC PIC X.                                          00002500
           02 MNBPAGESP PIC X.                                          00002510
           02 MNBPAGESH PIC X.                                          00002520
           02 MNBPAGESV PIC X.                                          00002530
           02 MNBPAGESO      PIC X(3).                                  00002540
                                                                                
