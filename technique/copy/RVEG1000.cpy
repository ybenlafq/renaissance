      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG1000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG1000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG1000.                                                    00000090
           02  EG10-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG10-TYPLIGNE                                            00000120
               PIC X(0001).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-NOLIGNE                                             00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG10-NOLIGNE                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG10-CONTINUER                                           00000160
               PIC X(0001).                                             00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-POSCHAMP                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG10-POSCHAMP                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG10-NOMCHAMP                                            00000200
               PIC X(0018).                                             00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-RUPTIMPR                                            00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  EG10-RUPTIMPR                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG10-CHCOND1                                             00000240
               PIC X(0018).                                             00000250
           02  EG10-CONDITION                                           00000260
               PIC X(0002).                                             00000270
           02  EG10-CHCOND2                                             00000280
               PIC X(0018).                                             00000290
           02  EG10-DSYST                                               00000300
               PIC S9(13) COMP-3.                                       00000310
           02  EG10-MASKCHAMP.                                          00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        49  EG10-MASKCHAMP-L                                     00000330
      *            PIC S9(4) COMP.                                      00000340
      *--                                                                       
               49  EG10-MASKCHAMP-L                                             
                   PIC S9(4) COMP-5.                                            
      *}                                                                        
               49  EG10-MASKCHAMP-V                                     00000350
                   PIC X(0030).                                         00000360
      *                                                                 00000370
      *---------------------------------------------------------        00000380
      *   LISTE DES FLAGS DE LA TABLE RVEG1000                          00000390
      *---------------------------------------------------------        00000400
      *                                                                 00000410
       01  RVEG1000-FLAGS.                                              00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-NOMETAT-F                                           00000430
      *        PIC S9(4) COMP.                                          00000440
      *--                                                                       
           02  EG10-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-TYPLIGNE-F                                          00000450
      *        PIC S9(4) COMP.                                          00000460
      *--                                                                       
           02  EG10-TYPLIGNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-NOLIGNE-F                                           00000470
      *        PIC S9(4) COMP.                                          00000480
      *--                                                                       
           02  EG10-NOLIGNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-CONTINUER-F                                         00000490
      *        PIC S9(4) COMP.                                          00000500
      *--                                                                       
           02  EG10-CONTINUER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-POSCHAMP-F                                          00000510
      *        PIC S9(4) COMP.                                          00000520
      *--                                                                       
           02  EG10-POSCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-NOMCHAMP-F                                          00000530
      *        PIC S9(4) COMP.                                          00000540
      *--                                                                       
           02  EG10-NOMCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-RUPTIMPR-F                                          00000550
      *        PIC S9(4) COMP.                                          00000560
      *--                                                                       
           02  EG10-RUPTIMPR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-CHCOND1-F                                           00000570
      *        PIC S9(4) COMP.                                          00000580
      *--                                                                       
           02  EG10-CHCOND1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-CONDITION-F                                         00000590
      *        PIC S9(4) COMP.                                          00000600
      *--                                                                       
           02  EG10-CONDITION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-CHCOND2-F                                           00000610
      *        PIC S9(4) COMP.                                          00000620
      *--                                                                       
           02  EG10-CHCOND2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-DSYST-F                                             00000630
      *        PIC S9(4) COMP.                                          00000640
      *--                                                                       
           02  EG10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG10-MASKCHAMP-F                                         00000650
      *        PIC S9(4) COMP.                                          00000660
      *--                                                                       
           02  EG10-MASKCHAMP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000670
                                                                                
