      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2027                  *        
      ******************************************************************        
       01  RVXM2027.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2027-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2027-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2027-NCODIC          PIC X(07).                                 
           10 XM2027-NDEPOT          PIC X(06).                                 
           10 XM2027-NSOC            PIC X(03).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2027-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2027-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2027-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2027-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2027-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2027-NDEPOT-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2027-NDEPOT-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2027-NSOC-F          PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2027-NSOC-F          PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
