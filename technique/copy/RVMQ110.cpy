      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MQ110 MODULES APPELES PAR BMQ110.      *        
      *----------------------------------------------------------------*        
       01  RVMQ110.                                                             
           05  MQ110-CTABLEG2    PIC X(15).                                     
           05  MQ110-CTABLEG2-REDEF REDEFINES MQ110-CTABLEG2.                   
               10  MQ110-TYPEMSG         PIC X(03).                             
           05  MQ110-WTABLEG     PIC X(80).                                     
           05  MQ110-WTABLEG-REDEF  REDEFINES MQ110-WTABLEG.                    
               10  MQ110-LIBELLE         PIC X(20).                             
               10  MQ110-CPGM            PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMQ110-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQ110-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MQ110-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MQ110-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MQ110-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
