      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2064                            *        
      ******************************************************************        
       01  RVXM2064.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2064-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2064-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2064-NLIST.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2064-NLIST-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2064-NLIST-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2064-NLIST-TEXT        PIC X(07).                            
           10 XM2064-NENTITE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2064-NENTITE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2064-NENTITE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2064-NENTITE-TEXT      PIC X(07).                            
           10 XM2064-NSEQENT              PIC S9(3)       COMP-3.               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2064                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2064-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2064-NLIST-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2064-NLIST-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2064-NENTITE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2064-NENTITE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2064-NSEQENT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2064-NSEQENT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 04      *        
      ******************************************************************        
                                                                                
