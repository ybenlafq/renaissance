      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES BMS     ATTENTION CE COPYY COBOL EST                     *         
      * ATTENTION : CE COPYY COBOL EST FARCI DE VALUES EN BINAIRE....  *         
      ****************************************************** SYKWECRA *         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  WORKAID   PIC X     VALUE LOW-VALUE.                             
               88  TOUCHE-NULL     VALUE LOW-VALUE.                             
               88  TOUCHE-ENTER    VALUE QUOTE.                                 
               88  TOUCHE-CLEAR    VALUE '_'.                                   
               88  TOUCHE-PEN      VALUE '='.                                   
               88  TOUCHE-OPID     VALUE 'W'.                                   
               88  TOUCHE-MSRE     VALUE 'X'.                                   
               88  TOUCHE-STRF     VALUE 'h'.                                   
               88  TOUCHE-TRIG     VALUE '"'.                                   
               88  TOUCHE-PA1      VALUE '%'.                                   
               88  TOUCHE-PA2      VALUE '>'.                                   
               88  TOUCHE-PA3      VALUE ','.                                   
               88  TOUCHE-PF1      VALUE '1'.                                   
               88  TOUCHE-PF01     VALUE '1'.                                   
               88  TOUCHE-PF2      VALUE '2'.                                   
               88  TOUCHE-PF02     VALUE '2'.                                   
               88  TOUCHE-PF3      VALUE '3'.                                   
               88  TOUCHE-PF03     VALUE '3'.                                   
               88  TOUCHE-PF4      VALUE '4'.                                   
               88  TOUCHE-PF04     VALUE '4'.                                   
               88  TOUCHE-PF5      VALUE '5'.                                   
               88  TOUCHE-PF05     VALUE '5'.                                   
               88  TOUCHE-PF6      VALUE '6'.                                   
               88  TOUCHE-PF06     VALUE '6'.                                   
               88  TOUCHE-PF7      VALUE '7'.                                   
               88  TOUCHE-PF07     VALUE '7'.                                   
               88  TOUCHE-PF8      VALUE '8'.                                   
               88  TOUCHE-PF08     VALUE '8'.                                   
               88  TOUCHE-PF9      VALUE '9'.                                   
               88  TOUCHE-PF09     VALUE '9'.                                   
               88  TOUCHE-PF10     VALUE ':'.                                   
      *{Post-translation Correct-Value-Touche
      *         88  TOUCHE-PF11     VALUE '�'.                                   
      *         88  TOUCHE-PF12     VALUE '�'.                                   
               88  TOUCHE-PF11     VALUE IS X'23'.
               88  TOUCHE-PF12     VALUE IS X'40'.
      *}
               88  TOUCHE-PF13     VALUE 'A'.                                   
               88  TOUCHE-PF14     VALUE 'B'.                                   
               88  TOUCHE-PF15     VALUE 'C'.                                   
               88  TOUCHE-PF16     VALUE 'D'.                                   
               88  TOUCHE-PF17     VALUE 'E'.                                   
               88  TOUCHE-PF18     VALUE 'F'.                                   
               88  TOUCHE-PF19     VALUE 'G'.                                   
               88  TOUCHE-PF20     VALUE 'H'.                                   
               88  TOUCHE-PF21     VALUE 'I'.                                   
      *{Post-translation Correct-Value-Touche
      *         88  TOUCHE-PF22     VALUE '�'.                                   
               88  TOUCHE-PF22     VALUE X'5B'.
      *}
               88  TOUCHE-PF23     VALUE '.'.                                   
               88  TOUCHE-PF24     VALUE '<'.                                   
              EJECT                                                             
      *                                                                         
      * DEFINITION DES ATTRIBUTS                                                
      *                                                                         
           05  BRT-ALP-FSET   PIC  X VALUE 'I'.                                 
           05  BRT-NUM-FSET   PIC  X VALUE 'R'.                                 
           05  BRT-PRO-FSET   PIC  X VALUE '9'.                                 
           05  DRK-ALP-FSET   PIC  X VALUE '('.                                 
           05  DRK-PRO-FSET   PIC  X VALUE QUOTE.                               
           05  DRK-PRO-RSET   PIC  X VALUE '�'.                                 
           05  NOR-ALP-FSET   PIC  X VALUE 'E'.                                 
           05  NOR-NUM-FSET   PIC  X VALUE 'N'.                                 
           05  NOR-PRO-FSET   PIC  X VALUE '5'.                                 
      *                                                                         
      * DEFINITION DES couleurs                                                 
      *                                                                         
           05  DFH-BLUE       PIC  X VALUE '1'.                                 
           05  DFH-RED        PIC  X VALUE '2'.                                 
           05  DFH-PINK       PIC  X VALUE '3'.                                 
           05  DFH-GREEN      PIC  X VALUE '4'.                                 
           05  DFH-TURQ       PIC  X VALUE '5'.                                 
           05  DFH-YELLO      PIC  X VALUE '6'.                                 
           05  DFH-NEUTR      PIC  X VALUE '7'.                                 
      *                                                                         
      * DEFINITION DES attributs etendus                                        
      *                                                                         
           05  DFH-BASE       PIC  X VALUE '�'.                                 
           05  DFH-BLINK      PIC  X VALUE '1'.                                 
           05  DFH-REVRS      PIC  X VALUE '2'.                                 
           05  DFH-UNDLN      PIC  X VALUE '4'.                                 
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CURSEUR        PIC S9(4) COMP VALUE -1.                          
      *--                                                                       
           05  CURSEUR        PIC S9(4) COMP-5 VALUE -1.                        
      *}                                                                        
      *        WCC-SCREEN     = X 'C3'                                          
           05  WCC-SCREEN     PIC X    VALUE 'C'.                               
      *        WCC-PRINTER    = X 'C8'                                          
           05  WCC-PRINTER    PIC X    VALUE 'H'.                               
      *        SAUT-PAGE-EOM  = X'0C19'                                         
      *{Post-translation Correct-Valeur-Binaire
      *     05  SAUT-PAGE-EOM  PIC X(2) VALUE ''.                              
           05  SAUT-PAGE-EOM  PIC X(2) VALUE X'0C19'. 
      *}
      *                                                                         
      * ATTENTION, LES DEUX ZONES CI-DESSOUS                                    
      * TYPE-TERMINAL   ET   MODELE-TERMINAL                                    
      * DOIVENT RESTER GROUPEES, ET DANS CET ORDRE.                             
      *                                                                         
      * CODES TERMINAL-ECRAN =        X'91  X'99                                
      * CODES TERMINAL-IMPRIMANTE =   X'93  X'94  X'9B  X'9C  X'B6              
      *                                                                         
           05  TERMCODE.                                                        
             10  TYPE-TERMINAL   PIC X VALUE SPACE.                             
               88  TERMINAL-ECRAN      VALUE 'j' 'r'.                           
               88  TERMINAL-IMPRIMANTE VALUE 'l' 'm' '�' '�' '�'.               
      *                                                                         
             10  MODELE-TERMINAL PIC X VALUE SPACE.                             
      *                                                                         
       EJECT                                                                    
                                                                                
                                                                                
