      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MEU05-LONG-COMMAREA       PIC S9(4) COMP VALUE +4096.   00000010
      *--                                                                       
       01  COMM-MEU05-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +4096.         
      *}                                                                        
       01  COMM-MEU05-APPLI.                                            00000011
           02 COMM-MEU05-ZONES-ENTREE.                                  00000020
              05 COMM-MEU05-NSOCIETE           PIC X(03).               00000021
              05 COMM-MEU05-CDEVORIG           PIC X(03).               00000030
              05 COMM-MEU05-CDEVDEST           PIC X(03).               00000031
              05 COMM-MEU05-DEFFET             PIC X(08).               00000040
              05 COMM-MEU05-PVALE              PIC S9(15)      COMP-3.  00000050
              05 COMM-MEU05-PVALE-1 REDEFINES  COMM-MEU05-PVALE         00000051
                                               PIC S9(14)V9    COMP-3.  00000052
              05 COMM-MEU05-PVALE-2 REDEFINES  COMM-MEU05-PVALE         00000053
                                               PIC S9(13)V9(2) COMP-3.  00000054
              05 COMM-MEU05-PVALE-3 REDEFINES  COMM-MEU05-PVALE         00000055
                                               PIC S9(12)V9(3) COMP-3.  00000056
              05 COMM-MEU05-PVALE-4 REDEFINES  COMM-MEU05-PVALE         00000057
                                               PIC S9(11)V9(4) COMP-3.  00000058
              05 COMM-MEU05-PVALE-5 REDEFINES  COMM-MEU05-PVALE         00000059
                                               PIC S9(10)V9(5) COMP-3.  00000060
              05 COMM-MEU05-PVALE-6 REDEFINES  COMM-MEU05-PVALE         00000061
                                               PIC S9(09)V9(6) COMP-3.  00000063
              05 COMM-MEU05-NBDECIM-E          PIC X.                   00000064
              05 COMM-MEU05-NBDECIM-ES         PIC X.                   00000065
           02 COMM-MEU05-ZONES-SORTIE.                                  00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MEU05-CODRET      COMP   PIC  S9(4).              00000110
      *--                                                                       
              05 COMM-MEU05-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MEU05-LMESS              PIC X(58).               00000120
              05 COMM-MEU05-NBDECIM-S          PIC X.                   00000121
              05 COMM-MEU05-PVALS              PIC S9(15)      COMP-3.  00000122
              05 COMM-MEU05-PVALS-1 REDEFINES  COMM-MEU05-PVALS         00000123
                                               PIC S9(14)V9    COMP-3.  00000124
              05 COMM-MEU05-PVALS-2 REDEFINES  COMM-MEU05-PVALS         00000125
                                               PIC S9(13)V9(2) COMP-3.  00000126
              05 COMM-MEU05-PVALS-3 REDEFINES  COMM-MEU05-PVALS         00000127
                                               PIC S9(12)V9(3) COMP-3.  00000128
              05 COMM-MEU05-PVALS-4 REDEFINES  COMM-MEU05-PVALS         00000129
                                               PIC S9(11)V9(4) COMP-3.  00000130
              05 COMM-MEU05-PVALS-5 REDEFINES  COMM-MEU05-PVALS         00000140
                                               PIC S9(10)V9(5) COMP-3.  00000150
              05 COMM-MEU05-PVALS-6 REDEFINES  COMM-MEU05-PVALS         00000160
                                               PIC S9(09)V9(6) COMP-3.  00000170
              05 COMM-MEU05-DEJA.                                               
               15 COMM-MEU05-PTAUX              PIC S9(3)V9(12) COMP-3.  0000018
               15 COMM-MEU05-PEURO              PIC S9(03)V9(2) COMP-3.  0000019
               15 COMM-MEU05-PEUROV5            PIC S9(6)V9(5) COMP-3.   0000018
               15 COMM-MEU05-LEURO              PIC XXX.                 0000018
               15 COMM-MEU05-LFRAN              PIC XXX.                 0000018
               15 COMM-MEU05-LDEVDEST25         PIC X(25).               0000003
               15 COMM-MEU05-LDEVDEST05         PIC X(05).               0000003
               15 COMM-MEU05-LSUBDEST25         PIC X(25).               0000003
               15 COMM-MEU05-LSUBDEST05         PIC X(05).               0000003
               15 COMM-MEU05-WEUROORIG          PIC X.                   0000003
               15 COMM-MEU05-LDEVORIG25         PIC X(25).               0000003
               15 COMM-MEU05-LDEVORIG05         PIC X(05).               0000003
               15 COMM-MEU05-LSUBORIG25         PIC X(25).               0000003
               15 COMM-MEU05-LSUBORIG05         PIC X(05).               0000003
               15 COMM-MEU05-CDEVORIGOLD        PIC X(03).               0000003
               15 COMM-MEU05-CDEVDESTOLD        PIC X(03).               0000003
               15 COMM-MEU05-WEURODEST          PIC X.                   0000003
               15 COMM-MEU05-NBDECDEST          PIC 9(01).               0000003
               15 COMM-MEU05-DEFFETOLD          PIC X(08).               0000004
               15 COMM-MEU05-NENTITE            PIC X(05).               0000004
           02 COMM-MEU05-APPEL                 PIC X(1).                00000100
               88 COMM-MEU05-APPEL-UN          VALUE '$'.                       
               88 COMM-MEU05-APPEL-SUITE       VALUE '&'.                       
           02 COMM-MEU05-FILLER                PIC X(3834).             00000100
                                                                                
