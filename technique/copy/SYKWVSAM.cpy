      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES AUX FICHIERS VSAM KSDS       *         
      *****************************************************************         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
             10  FILLER         PIC  X(16)  VALUE '..VSAM....AREA..'.           
      *                                                                         
             10  FILLER      PIC  X(12)      VALUE 'VSAM-KEY....'.              
             10  VSAM-KEY    PIC  X(256).                                       
      *                                                                         
       EJECT                                                                    
                                                                                
                                                                                
