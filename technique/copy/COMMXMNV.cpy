      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:29 >
      
      *
      *01  COMM-XMNV-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.
      *
       01  Z-COMMAREA.
      
      * ZONES RESERVEES A AIDA ----------------------------------- 100
           02 FILLER-COM-AIDA      PIC X(100).
      
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020
           02 COMM-CICS-APPLID     PIC X(08).
           02 COMM-CICS-NETNAM     PIC X(08).
           02 COMM-CICS-TRANSA     PIC X(04).
      
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100
           02 COMM-DATE-SIECLE     PIC X(02).
           02 COMM-DATE-ANNEE      PIC X(02).
           02 COMM-DATE-MOIS       PIC X(02).
           02 COMM-DATE-JOUR       PIC 99.
      *   QUANTIEMES CALENDAIRE ET STANDARD
           02 COMM-DATE-QNTA       PIC 999.
           02 COMM-DATE-QNT0       PIC 99999.
      *   ANNEE BISSEXTILE 1=OUI 0=NON
           02 COMM-DATE-BISX       PIC 9.
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE
           02 COMM-DATE-JSM        PIC 9.
      *   LIBELLES DU JOUR COURT - LONG
           02 COMM-DATE-JSM-LC     PIC X(03).
           02 COMM-DATE-JSM-LL     PIC X(08).
      *   LIBELLES DU MOIS COURT - LONG
           02 COMM-DATE-MOIS-LC    PIC X(03).
           02 COMM-DATE-MOIS-LL    PIC X(08).
      *   DIFFERENTES FORMES DE DATE
           02 COMM-DATE-SSAAMMJJ   PIC X(08).
           02 COMM-DATE-AAMMJJ     PIC X(06).
           02 COMM-DATE-JJMMSSAA   PIC X(08).
           02 COMM-DATE-JJMMAA     PIC X(06).
           02 COMM-DATE-JJ-MM-AA   PIC X(08).
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).
      *   TRAITEMENT DU NUMERO DE SEMAINE
           02 COMM-DATE-WEEK.
              05 COMM-DATE-SEMSS   PIC 99.
              05 COMM-DATE-SEMAA   PIC 99.
              05 COMM-DATE-SEMNU   PIC 99.
           02 COMM-DATE-FILLER     PIC X(08).
      *   ZONES RESERVEES TRAITEMENT DU SWAP
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.
      *
      *--
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.
      
      *}
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874
      
           02 COMM-XMNV-ENTETE.
              03 COMM-ENTETE.
                 05 COMM-CODLANG            PIC X(02).
                 05 COMM-CODPIC             PIC X(02).
                 05 COMM-CODESFONCTION  .
                    10 COMM-00-CODE-FONCTION      OCCURS 3.
                       15  COMM-00-CFONC     PIC  X(03).
                 05 COMM-01-CFONC-OPTION    PIC X(03).
                 05 COMM-00-COPTION            PIC X(02).
                 05 COMM-CACID              PIC X(08).
                 05 COMM-00-LEVEL-MAX          PIC X(06).
                 05 COMM-FILLER             PIC X(40).
           02 COMM-XMNV-FILLER              PIC X(3650).
      
      *--  ZONES RESERVEES APPLICATIVES OPTION 1 ----------------- 3800
           02 COMM-XM01-APPLI REDEFINES COMM-XMNV-FILLER.
              03 COMM-01.
      *-                              GESTION SUPPRESSION LIGNE
                 05  COMM-01-CONFIRM-SUP     PIC X(01).
                     88 01-CONFIRM-SUP           VALUE '1'.
                 05  COMM-01-DEMANDE-CONFIRM PIC X(01).
                     88 01-DEMANDE-CONFIRM       VALUE '1'.
                 05  COMM-01-ACTION-LIGNE    PIC X(01).
                     88 01-CREATION-LIGNE        VALUE '1'.
                     88 01-MAJ-LIGNE             VALUE '2'.
      *-                              NUMERO PAGE COURANTE
                 05 COMM-01-NUMPAG      PIC 9(03).
      *-                              NUMERO PAGE MAXI
                 05 COMM-01-PAGE-MAX    PIC 9(03).
      *-                              NB LIGNES
                 05 COMM-01-I-CHOIX-MAX    PIC 9(02).
      *-                              NUM CHOIX
                 05 COMM-01-I-CHOIX        PIC 9(02).
      *                               CODE TRAITEMENT (MAJ CRE)
      *          05 COMM-01-CODETR           PIC X(03).
      *
                 05 COMM-01-INDMAX          PIC S9(5) COMP-3.
      *             L"ALIMENTATION DE L"ECRAN
                 05 COMM-01-VAL-ECRAN.
                    10 COMM-01-LIGNE  OCCURS 10.
                       20 COMM-01-AFFICH       PIC X(1).
                       20 COMM-01-CAPPLI       PIC X(6).
                       20 COMM-01-CNOMTSXSD    PIC X(8).
                       20 COMM-01-CNOMTSCAR    PIC X(8).
                       20 COMM-01-LGVERSION    PIC ZZZZ9.
                       20 COMM-01-VERSION      PIC X(50).
                 05 COMM-01-VAL-LIGNE.
                    15 COMM-01-CAPPLI-NW       PIC X(6).
                    15 COMM-01-CNOMTSXSD-NW    PIC X(8).
                    15 COMM-01-CNOMTSCAR-NW    PIC X(8).
                    15 COMM-01-LGVERSION-NW    PIC ZZZZ9.
                    15 COMM-01-VERSION1-NW     PIC X(50).
                    15 COMM-01-VERSION2-NW     PIC X(50).
                    15 COMM-01-VERSION3-NW     PIC X(50).
                 05 COMM-01-FILLER PIC X(1900).
      
      *--  ZONES RESERVEES APPLICATIVES OPTION 2 ----------------- 3800
           02 COMM-XM02-APPLI REDEFINES COMM-XMNV-FILLER.
              03 COMM-02.
      *-                              GESTION SUPPRESSION LIGNE
                 05  COMM-02-CONFIRM-SUP     PIC X(01).
                     88 02-CONFIRM-SUP           VALUE '1'.
                 05  COMM-02-DEMANDE-CONFIRM PIC X(01).
                     88 02-DEMANDE-CONFIRM       VALUE '1'.
      *-                              NUMERO PAGE COURANTE
                 05 COMM-02-NUMPAG      PIC 9(03).
      *-                              NUMERO PAGE MAXI
                 05 COMM-02-PAGE-MAX    PIC 9(03).
      *-                              NB LIGNES
                 05 COMM-02-I-CHOIX-MAX    PIC 9(02).
      *-                              NUM CHOIX
                 05 COMM-02-I-CHOIX        PIC 9(02).
      *                               CODE TRAITEMENT (MAJ CRE)
                 05 COMM-02-CODETR           PIC X(03).
      *
                 05 COMM-02-INDMAX          PIC S9(5) COMP-3.
      *               FILTRE SUR LE DTD
                 05 COMM-02-CDTD-FILTRE     PIC X(20).
      *             L"ALIMENTATION DE L"ECRAN
                 05 COMM-02-VAL-ECRAN.
                    10 COMM-02-LIGNE  OCCURS 10.
                       20 COMM-02-CDTD         PIC X(20).
                       20 COMM-02-LGDATA       PIC ZZZZZ.
                       20 COMM-02-PROLOGUE     PIC X(50).
      *             LA DTD SELECTIONNéE
                 05 COMM-02-VAL-DTD.
                       20 COMM-02-LG-CDTD         PIC X(20).
                       20 COMM-02-LG-LGDATA       PIC ZZZZ9.
                       20 COMM-02-LG-PROLOGUE     PIC X(150).
                       20 COMM-02-LG-DOC-LEGACY   PIC X(10).
                 05 COMM-02-FILLER PIC X(090).
      *-        LA LISTE DES ELEMENTS
              03 COMM-03.
      *-                              GESTION SUPPRESSION LIGNE
                 05  COMM-03-CONFIRM-SUP     PIC X(01).
                     88 03-CONFIRM-SUP           VALUE '1'.
                 05  COMM-03-DEMANDE-CONFIRM PIC X(01).
                     88 03-DEMANDE-CONFIRM       VALUE '1'.
      *-            PF21         DEMANDE DE REPLIQUATION DE MEME DTD
                 05  COMM-03-DEMANDE-REPLIQ  PIC X(01).
                     88 03-DEMANDE-REPLIQUE      VALUE '1'.
      *-                              NUMERO PAGE COURANTE
                 05 COMM-03-NUMPAG      PIC 9(03).
      *-                              NUMERO PAGE MAXI
                 05 COMM-03-PAGE-MAX    PIC 9(03).
      *-                              NUMERO PAGE ECRAN
                 05 COMM-03-ECRAN       PIC 9(03).
      *-                              NUMERO PAGE ECRAN MAXI
                 05 COMM-03-ECRAN-MAX PIC 9(03).
      *-                              NB LIGNES
                 05 COMM-03-I-CHOIX-MAX    PIC 9(02).
      *-                              NUM CHOIX
                 05 COMM-03-I-CHOIX        PIC 9(02).
      *                               CODE TRAITEMENT (MAJ CRE)
                 05 COMM-03-CODETR           PIC X(03).
      *
                 05 COMM-03-INDMAX          PIC S9(5) COMP-3.
      *             LA DTD SELECTIONNéE
                 05 COMM-03-VAL-DTD.
                       20 COMM-03-LG-CDTD         PIC X(20).
                       20 COMM-03-LG-LGDATA       PIC ZZZZ9.
                       20 COMM-03-LG-PROLOGUE     PIC X(150).
                       20 COMM-03-LG-DOC-LEGACY   PIC X(10).
      *               FILTRE SUR L"ELEMENT
                 05 COMM-03-ELEM-FILTRE     PIC X(10).
      *             L"ALIMENTATION DE L"ECRAN
                 05 COMM-03-VAL-ECRAN.
                    10 COMM-03-LIGNE  OCCURS 10.
                       20 COMM-03-ELEMENT          PIC X(50).
                       20 COMM-03-NSEQ             PIC ZZZZZZ.
                       20 COMM-03-MAXOCCURS        PIC ZZ9.
                       20 COMM-03-USE              PIC X(01).
                       20 COMM-03-TELEMENT         PIC X(01).
                       20 COMM-03-TCOBOL           PIC X(02).
                       20 COMM-03-LGDONNEE         PIC ZZZ.
                       20 COMM-03-NBDEC            PIC Z.
                       20 COMM-03-ELPERE           PIC X(50).
                       20 COMM-03-ELTLEG           PIC X(10).
                       20 COMM-03-CTRLFLUX         PIC X(01).
      *          05 COMM-03-FILLER PIC X(37).
                 05 COMM-03-FILLER PIC X(16).
      *-        LA LISTE DES ELEMENTS
              03 COMM-04.
                 05 COMM-04-LIGNE.
                       20 COMM-04-ELEMENT          PIC X(50).
                       20 COMM-04-NSEQ             PIC ZZZZZZ.
                       20 COMM-04-MAXOCCURS        PIC ZZ9.
                       20 COMM-04-USE              PIC X(01).
                       20 COMM-04-TELEMENT         PIC X(01).
                       20 COMM-04-TCOBOL           PIC X(02).
                       20 COMM-04-LGDONNEE         PIC ZZZ.
                       20 COMM-04-NBDEC            PIC Z.
                       20 COMM-04-ELPERE           PIC X(50).
                       20 COMM-04-ELTLEG           PIC X(10).
                       20 COMM-04-CTRLFLUX         PIC X(01).
                 05 COMM-04-COMMENTAIRE.
                       20 COMM-04-VALEUR1          PIC X(50).
                       20 COMM-04-VALEUR2          PIC X(50).
                       20 COMM-04-VALEUR3          PIC X(50).
      *-         MODIFICATION DE LA LIGNE COMMENTAIRE
                 05 COMM-04-MAJ-COMMT PIC X(01).
                 05 COMM-04-FILLER PIC X(786).
      
      
