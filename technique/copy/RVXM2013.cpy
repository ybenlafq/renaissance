      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2013                            *        
      ******************************************************************        
       01  RVXM2013.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2013-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2013-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2013-CONTEXT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2013-CONTEXT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2013-CONTEXT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2013-CONTEXT-TEXT      PIC X(50).                            
           10 XM2013-ADMIN.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2013-ADMIN-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2013-ADMIN-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2013-ADMIN-TEXT        PIC X(50).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2013                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2013-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2013-CONTEXT-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2013-CONTEXT-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2013-ADMIN-F              PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2013-ADMIN-F              PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *        
      ******************************************************************        
                                                                                
