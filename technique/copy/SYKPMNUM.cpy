      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------                          00000930
       DECODE-NUMERIQUE                SECTION.                         00000940
      *---------------------------------------                          00000950
           PERFORM CONTROLE-PARAMETRES.                                         
           IF ZWN-OK                                                            
              PERFORM TRAITEMENT-DECODAGE                                       
           END-IF.                                                              
           IF ZWN-OK                                                    00000820
              MOVE ZWN-LGIN            TO ZWN-LGOUT                             
              MOVE 18                  TO ZWN-LGIN                              
              PERFORM EDIT-NUMERIQUE                                    00000830
           ELSE                                                                 
              PERFORM ERREUR-NUM                                        00000870
           END-IF.                                                      00000840
      *---------------------------------------                          00000930
       TRAITEMENT-DECODAGE             SECTION.                         00000940
      *---------------------------------------                          00000950
           SET ZWN-DECIM-NON     TO TRUE.                               00000970
           SET ZWN-NEUTRE        TO TRUE.                               00000980
           MOVE ZERO             TO ZWN-I1.                             00000990
           MOVE ZERO             TO ZWN-I2.                             00001000
           MOVE ZWN-NBDEC        TO ZWN-DECIM.                          00001010
                                                                        00001020
           PERFORM VARYING ZWN-I1 FROM 1 BY 1                           00001030
                   UNTIL ZWN-I1 > ZWN-LGIN                              00001040
                   OR NOT ZWN-OK                                        00001050
              IF ZWN-ZEDIT(ZWN-I1:1) NOT < '0' AND NOT > '9'            00001060
                 PERFORM DECODE-NUM-MVCHIFFRE                           00001070
              ELSE                                                      00001080
                 IF ZWN-ZEDIT(ZWN-I1:1) = '+' OR '-'                    00001090
                    IF ZWN-POSITIF OR ZWN-NEGATIF                       00001100
                       SET ZWN-DBLESIGNE TO TRUE                        00001110
                    ELSE                                                00001120
                       IF ZWN-ZEDIT(ZWN-I1:1) = '+'                     00001130
                          SET ZWN-POSITIF     TO TRUE                   00001140
                       ELSE                                             00001150
                          IF NOT ZWN-NOTMOINS                           00001160
                             SET ZWN-NEGATIF     TO TRUE                00001170
                          ELSE                                          00001180
                             SET ZWN-PASNEGATIF TO TRUE                 00001190
                          END-IF                                        00001200
                       END-IF                                           00001210
                    END-IF                                              00001220
                 ELSE                                                   00001230
                    IF ZWN-ZEDIT(ZWN-I1:1) = ZWN-VIRGULE                00001240
                       IF ZWN-DECIM = ZERO                              00001250
                          SET ZWN-TROPLONG    TO TRUE                   00001260
                       ELSE                                             00001270
                          IF ZWN-DECIM-OUI                              00001280
                             SET ZWN-DBLEDECIM   TO TRUE                00001290
                          ELSE                                          00001300
                             SET ZWN-DECIM-OUI   TO TRUE                00001310
                          END-IF                                        00001320
                       END-IF                                           00001330
                    ELSE                                                00001340
                       IF ZWN-ZEDIT(ZWN-I1:1) NOT = ZWN-POINT           00001350
                                              AND NOT = SPACE           00001360
      *{ Tr-Hexa-Map 1.5                                                        
      *                                       AND NOT = X'00'           00001370
      *--                                                                       
                                              AND NOT = X'00'                   
      *}                                                                        
                          SET ZWN-INVCAR      TO TRUE                   00001380
                       END-IF                                           00001390
                    END-IF                                              00001400
                 END-IF                                                 00001410
              END-IF                                                    00001420
           END-PERFORM.                                                 00001430
                                                                        00001440
           IF ZWN-OK                                                    00001450
              PERFORM DECODE-NUM-MVZERO                                 00001460
           END-IF.                                                      00001470
      *---------------------------------------                          00001480
       DECODE-NUM-MVCHIFFRE            SECTION.                         00001490
      *---------------------------------------                          00001480
           IF ZWN-DECIM-OUI                                             00001500
              SUBTRACT 1           FROM ZWN-DECIM                       00001510
              IF ZWN-DECIM < ZERO                                       00001520
                 SET  ZWN-TROPDECIM    TO TRUE                          00001530
              END-IF                                                    00001540
           END-IF.                                                      00001550
           ADD  1                      TO ZWN-I2.                       00001560
           MOVE ZWN-ZEDIT(ZWN-I1:1)    TO ZWN-WORK(ZWN-I2:1).           00001570
           IF (ZWN-I2 + ZWN-DECIM) > ZWN-LGOUT                          00001580
              SET  ZWN-TROPLONG        TO TRUE                          00001590
           END-IF.                                                      00001600
      *---------------------------------------                          00001610
       DECODE-NUM-MVZERO               SECTION.                         00001620
      *---------------------------------------                          00001630
           IF ZWN-DECIM > ZERO                                          00001640
              PERFORM VARYING ZWN-DECIM FROM ZWN-DECIM BY -1            00001650
                      UNTIL ZWN-DECIM = ZERO                            00001660
                 ADD  1               TO ZWN-I2                         00001670
                 MOVE '0'             TO ZWN-WORK(ZWN-I2:1)             00001680
              END-PERFORM                                               00001690
           END-IF.                                                      00001700
                                                                        00001710
           PERFORM VARYING ZWN-I1 FROM 18 BY -1                         00001720
                   UNTIL ZWN-I1 = ZERO                                  00001730
              IF ZWN-I2 > ZERO                                          00001740
                 MOVE ZWN-WORK(ZWN-I2:1) TO ZWN-ZNUM(ZWN-I1:1)          00001750
                 SUBTRACT 1         FROM ZWN-I2                         00001760
              ELSE                                                      00001770
                 MOVE '0'             TO ZWN-ZNUM(ZWN-I1:1)             00001780
              END-IF                                                    00001790
           END-PERFORM.                                                 00001800
                                                                        00001810
           IF ZWN-NEGATIF                                               00001820
              COMPUTE ZWN-ZNUMN = ZWN-ZNUMN * -1                        00001830
           END-IF.                                                      00001840
      *---------------------------------------                          00001870
       EDIT-NUMERIQUE                  SECTION.                         00001880
      *---------------------------------------                          00001890
           PERFORM CONTROLE-PARAMETRES.                                         
           IF ZWN-OK                                                    00000820
              PERFORM TRAITEMENT-EDIT                                   00000870
           END-IF.                                                      00000840
           IF NOT ZWN-OK                                                00000820
              PERFORM ERREUR-NUM                                        00000870
           END-IF.                                                      00000840
      *---------------------------------------                          00001870
       TRAITEMENT-EDIT                 SECTION.                         00001880
      *---------------------------------------                          00001890
           MOVE LOW-VALUE           TO ZWN-ZEDIT.                       00001900
           MOVE ZERO                TO ZWN-I2.                          00001910
           COMPUTE ZWN-ENTIER  = 18 - ZWN-NBDEC.                        00001920
           COMPUTE ZWN-I1      = 18 - ZWN-LGIN + 1.                     00001930
           MOVE ZWN-ZNUM            TO ZWN-WORK.                                
           MOVE ZWN-WORK(18:1)      TO ZWN-ZNEGATIVE.                   00001940
           MOVE ZWN-ZNEGATIVN       TO ZWN-NONSIGNE.                    00001940
           MOVE ZWN-NONSIGNE        TO ZWN-WORK(18:1).                  00001960
           IF ZWN-ZNUMN < ZERO                                                  
              ADD  1                TO ZWN-I2                           00001990
              MOVE '-'              TO ZWN-ZEDIT(ZWN-I2:1)              00002000
           END-IF.                                                      00002020
                                                                        00002030
           PERFORM VARYING ZWN-I1 FROM ZWN-I1 BY 1                      00002040
                   UNTIL ZWN-I1 > ZWN-ENTIER                            00002050
              IF ZWN-WORK (ZWN-I1:1) NOT = '0'                          00002060
                 PERFORM EDIT-NUM-MVCHIFFRE                             00002070
              ELSE                                                      00002080
                 IF ZWN-I2 > ZERO AND                                   00002090
                    ZWN-ZEDIT(ZWN-I2:1) NOT < '0'                       00002100
                    PERFORM EDIT-NUM-MVCHIFFRE                          00002110
                 END-IF                                                 00002120
              END-IF                                                    00002130
           END-PERFORM.                                                 00002140
                                                                        00002150
           IF ZWN-NBDEC > ZERO                                          00002160
              IF ZWN-WORK(ZWN-I1:ZWN-NBDEC) NOT = ALL '0'               00002170
                 IF ZWN-I2 = ZERO                                       00002180
                    ADD  1             TO ZWN-I2                        00002190
                    MOVE '0'           TO ZWN-ZEDIT(ZWN-I2:1)           00002200
                 END-IF                                                 00002210
              END-IF                                                    00002220
           END-IF.                                                      00002230
                                                                        00002240
           IF ZWN-NBDEC > ZERO                                          00002250
           AND ZWN-WORK(ZWN-I1:ZWN-NBDEC) NOT = ALL '0'                 00002260
              ADD  1                   TO ZWN-I2                        00002270
              MOVE ZWN-VIRGULE         TO ZWN-ZEDIT(ZWN-I2:1)           00002280
              PERFORM VARYING ZWN-I1 FROM ZWN-I1 BY 1                   00002290
                      UNTIL ZWN-I1 > 18                                 00002300
                 PERFORM EDIT-NUM-MVCHIFFRE                             00002310
              END-PERFORM                                               00002320
           END-IF.                                                      00002330
      *---------------------------------------                          00002340
       EDIT-NUM-MVCHIFFRE              SECTION.                         00002350
      *---------------------------------------                          00002340
           IF ZWN-I2 NOT < ZWN-LGOUT                                    00002360
              SET  ZWN-TROPLONG        TO TRUE                          00002370
           ELSE                                                         00002380
              ADD  1                     TO ZWN-I2                      00002390
              MOVE ZWN-WORK(ZWN-I1:1) TO ZWN-ZEDIT(ZWN-I2:1)            00002400
           END-IF.                                                      00002410
      *---------------------------------------                          00002340
       CONTROLE-PARAMETRES             SECTION.                         00002350
      *---------------------------------------                          00002340
           SET ZWN-OK               TO TRUE.                            00000610
                                                                        00000620
           IF ZWN-TYPDECIMAL NOT = ZWN-VIRGULE                          00000630
              IF ZWN-TYPDECIMAL = ZWN-POINT                             00000640
                 MOVE ZWN-VIRGULE         TO ZWN-POINT                  00000650
                 MOVE ZWN-TYPDECIMAL      TO ZWN-VIRGULE                00000660
              ELSE                                                      00000670
                 SET  ZWN-ERRIN           TO TRUE                       00000680
              END-IF                                                    00000690
           END-IF.                                                      00000700
                                                                        00000710
           IF ZWN-LGIN  > 18                                            00000720
           OR ZWN-LGOUT > 18                                            00000730
           OR ZWN-NBDEC > 18                                            00000740
              SET ZWN-ERRIN            TO TRUE                          00000750
           END-IF.                                                      00000760
      *---------------------------------------                          00002440
       ERREUR-NUM                      SECTION.                         00002450
      *---------------------------------------                          00002440
           STRING 'NUM' ZWN-CDRET DELIMITED BY SIZE                     00002490
                  INTO ERREUR-UTILISATEUR.                              00002500
                                                                        00002510
           EVALUATE ZWN-CDRET                                           00002520
              WHEN '1'                                                  00002530
                   MOVE 'CARACTERES INVALIDES DANS CETTE ZONE    '      00002540
                                          TO LIBERR-MESSAGE             00002550
              WHEN '2'                                                  00002560
                   MOVE 'CARACTERE DECIMAL EN DOUBLE             '      00002570
                                          TO LIBERR-MESSAGE             00002580
              WHEN '3'                                                  00002590
                   MOVE 'CHIFFRE TROP LONG PAR RAPPORT AU PREVU  '      00002600
                                          TO LIBERR-MESSAGE             00002610
              WHEN '4'                                                  00002620
                   MOVE 'CARACTERE SIGNE SAISI EN DOUBLE         '      00002630
                                          TO LIBERR-MESSAGE             00002640
              WHEN '5'                                                  00002650
                   MOVE 'IL Y A TROP DE DECIMALES SAISIES        '      00002660
                                          TO LIBERR-MESSAGE             00002670
              WHEN '6'                                                  00002680
                   MOVE 'CETTE ZONE NE PEUT PAS ETRE NEGATIVE    '      00002690
                                          TO LIBERR-MESSAGE             00002700
              WHEN 'P'                                                  00002710
                   MOVE 'ERREUR DE PARAMETRAGE  DU SOUS-PROGRAMME'      00002720
                                          TO LIBERR-MESSAGE             00002730
           END-EVALUATE.                                                00002740
                                                                                
