      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ZWORK-MASQUE.                                                        
           05  ZWM-TYPCHAMP                PIC X.                               
           05  ZWM-LGCHAMP                 PIC S999      COMP-3.                
           05  ZWM-DECCHAMP                PIC S999      COMP-3.                
           05  ZWM-LGMASQUE                PIC S999      COMP-3.                
           05  ZWM-MASQUE                  PIC X(30).                           
           05  ZWM-CONTENU                 PIC X(15).                           
           05  ZWM-CONTENU-N REDEFINES ZWM-CONTENU                              
                                           PIC S9(13)V99 COMP-3.                
           05  ZWM-DECEDIT                 PIC S999      COMP-3.                
                                                                                
