      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2018                  *        
      ******************************************************************        
       01  RVXM2018.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2018-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2018-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2018-CDEST           PIC X(05).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2018-FLAGS.                                                      
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2011-REFID-F         USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2011-REFID-F PIC X(40).                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2018-CDEST-F         PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2018-CDEST-F         PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
