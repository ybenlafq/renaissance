      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      * TEST-CODE-RETOUR DL/I                                           00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       TEST-CODE-RETOUR               SECTION.                          00000500
      *                                                                 00000600
           MOVE 0        TO CODE-RETOUR.                                00000700
           MOVE 0        TO CODE-LEVEL.                                 00000800
           MOVE DIBSTAT  TO PCB-STATUS-CODE.                            00000900
           MOVE DIBSEGM  TO SEG-NAME-FB.                                00001000
           MOVE DIBSEGLV TO SEG-LEVEL.                                  00001100
      *    MOVE DIBKFBL  TO LENGTH-FB-KEY.                              00001200
      *                                                                 00001300
           IF GU                                                        00001400
              IF GE                                                     00001500
                 MOVE 1 TO CODE-RETOUR                                  00001600
               ELSE                                                     00001700
                 IF NOT BB                                              00001800
                    GO TO ABANDON-DL1                                   00001900
                 END-IF                                                 00002000
               END-IF                                                   00002100
           END-IF.                                                      00002200
      *                                                                 00002300
           IF GN                                                        00002400
              IF GE                                                     00002500
                 MOVE 1 TO CODE-RETOUR                                  00002600
               ELSE                                                     00002700
                 IF GA                                                  00002800
                    MOVE 1 TO CODE-LEVEL                                00002900
                 ELSE                                                   00003000
                    IF GK                                               00003100
                       MOVE 2 TO CODE-LEVEL                             00003200
                    ELSE                                                00003300
                       IF GB                                            00003400
                          MOVE 3 TO CODE-RETOUR                         00003500
                       ELSE                                             00003600
                          IF NOT BB                                     00003700
                             GO TO ABANDON-DL1                          00003800
                          END-IF                                        00003900
                        END-IF                                          00004000
                    END-IF                                              00004100
                 END-IF                                                 00004200
              END-IF                                                    00004300
           END-IF.                                                      00004400
      *                                                                 00004500
           IF GNP                                                       00004600
              IF GE                                                     00004700
                 MOVE 1 TO CODE-RETOUR                                  00004800
               ELSE                                                     00004900
                 IF GA                                                  00005000
                    MOVE 1 TO CODE-LEVEL                                00005100
                 ELSE                                                   00005200
                    IF GK                                               00005300
                       MOVE 2 TO CODE-LEVEL                             00005400
                    ELSE                                                00005500
                       IF NOT BB                                        00005600
                          GO TO ABANDON-DL1                             00005700
                       END-IF                                           00005800
                    END-IF                                              00005900
                 END-IF                                                 00006000
              END-IF                                                    00006100
           END-IF.                                                      00006200
      *                                                                 00006300
           IF ISRT                                                      00006400
              IF II OR NI                                               00006500
                 MOVE 2 TO CODE-RETOUR                                  00006600
              ELSE                                                      00006700
                 IF NOT BB                                              00006800
                    GO TO ABANDON-DL1                                   00006900
                 END-IF                                                 00007000
              END-IF                                                    00007100
           END-IF.                                                      00007200
      *                                                                 00007300
           IF REPL                                                      00007400
              IF NOT BB                                                 00007500
                 GO TO ABANDON-DL1                                      00007600
              END-IF                                                    00007700
           END-IF.                                                      00007800
      *                                                                 00007900
           IF DLET                                                      00008000
              IF NOT BB                                                 00008100
                 GO TO ABANDON-DL1                                      00008200
              END-IF                                                    00008300
            END-IF.                                                     00008400
      *                                                                 00008500
       FIN-TEST-CODE-RETOUR.   EXIT.                                    00008600
                EJECT                                                   00009000
                                                                                
                                                                                
