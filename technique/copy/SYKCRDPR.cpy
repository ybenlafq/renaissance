      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  MODULE GENERALISE DE READ PREVIOUS                             00000200
      ****************************************************************  00000300
      *                                                                 00000400
       READ-PREVIOUS             SECTION.                               00000500
      *                                                                 00000600
      *    MOVE LONG-Z-INOUT TO FILE-LONG.                              00000700
      *                                                                 00000800
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READPREV DATASET (FILE-NAME)                                   
                          RIDFLD  (VSAM-KEY)                                    
                          INTO    (Z-INOUT)                                     
                          LENGTH  (FILE-LONG)                                   
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�4�00010   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME Z-INOUT FILE-LO        
      *dfhei*     VSAM-KEY DFHDUMMY DFHEIB0.                                    
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001700
           MOVE EIBRCODE TO EIB-RCODE.                                  00001800
           IF   EIB-NORMAL                                              00001900
                MOVE 0 TO CODE-RETOUR                                   00002000
           ELSE                                                         00002100
                IF   EIB-ENDFILE OR EIB-NOTFND                          00002200
                     MOVE 3 TO CODE-RETOUR                              00002300
                ELSE                                                    00002400
                     GO TO ABANDON-CICS                                 00002500
                IF   EIB-DUPKEY                                         00002200
                     MOVE 7 TO CODE-RETOUR                              00002300
                ELSE                                                    00002400
                     GO TO ABANDON-CICS                                 00002500
                END-IF                                                          
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002600
       FIN-READ-PREVIOUS.  EXIT.                                        00002700
                    EJECT                                               00002900
                                                                                
                                                                                
