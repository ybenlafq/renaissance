      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *          * ZONES BUFFER D'ENTREE/SORTIE                       *         
      ****************************************************** SYKWZINO *         
      *                                                                         
       01   FILLER  PIC X(16) VALUE '**** ZINOUT ****'.                         
      *                                                                         
       01   Z-INOUT     PIC X(6496)  VALUE SPACES.                              
      *                                                                         
       01   Z-SWAP      REDEFINES   Z-INOUT.                                    
            02  Z-INOUT-MAP         PIC X(2400).                                
            02  Z-INOUT-COMMAREA    PIC X(4096).                                
       EJECT                                                                    
                                                                                
                                                                                
