      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE XMLCS XML - CARACTERES SPECIAUX        *        
      *----------------------------------------------------------------*        
       01  RVXMLCS.                                                             
           05  XMLCS-CTABLEG2    PIC X(15).                                     
           05  XMLCS-CTABLEG2-REDEF REDEFINES XMLCS-CTABLEG2.                   
               10  XMLCS-CARACT          PIC X(05).                             
           05  XMLCS-WTABLEG     PIC X(80).                                     
           05  XMLCS-WTABLEG-REDEF  REDEFINES XMLCS-WTABLEG.                    
               10  XMLCS-VALNUM          PIC X(20).                             
               10  XMLCS-VALALPHA        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVXMLCS-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XMLCS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  XMLCS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XMLCS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  XMLCS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
