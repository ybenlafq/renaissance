      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG2000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG2000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG2000.                                                    00000090
           02  EG20-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG20-TYPLIGNE                                            00000120
               PIC X(0001).                                             00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-NOLIGNE                                             00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG20-NOLIGNE                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG20-CONTINUER                                           00000160
               PIC X(0001).                                             00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-POSCHAMP                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG20-POSCHAMP                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG20-DSYST                                               00000200
               PIC S9(13) COMP-3.                                       00000210
           02  EG20-LIBCHAMP.                                           00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        49  EG20-LIBCHAMP-L                                      00000230
      *            PIC S9(4) COMP.                                      00000240
      *--                                                                       
               49  EG20-LIBCHAMP-L                                              
                   PIC S9(4) COMP-5.                                            
      *}                                                                        
               49  EG20-LIBCHAMP-V                                      00000250
                   PIC X(0066).                                         00000260
      *                                                                 00000270
      *---------------------------------------------------------        00000280
      *   LISTE DES FLAGS DE LA TABLE RVEG2000                          00000290
      *---------------------------------------------------------        00000300
      *                                                                 00000310
       01  RVEG2000-FLAGS.                                              00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-NOMETAT-F                                           00000330
      *        PIC S9(4) COMP.                                          00000340
      *--                                                                       
           02  EG20-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-TYPLIGNE-F                                          00000350
      *        PIC S9(4) COMP.                                          00000360
      *--                                                                       
           02  EG20-TYPLIGNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-NOLIGNE-F                                           00000370
      *        PIC S9(4) COMP.                                          00000380
      *--                                                                       
           02  EG20-NOLIGNE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-CONTINUER-F                                         00000390
      *        PIC S9(4) COMP.                                          00000400
      *--                                                                       
           02  EG20-CONTINUER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-POSCHAMP-F                                          00000410
      *        PIC S9(4) COMP.                                          00000420
      *--                                                                       
           02  EG20-POSCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-DSYST-F                                             00000430
      *        PIC S9(4) COMP.                                          00000440
      *--                                                                       
           02  EG20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG20-LIBCHAMP-F                                          00000450
      *        PIC S9(4) COMP.                                          00000460
      *--                                                                       
           02  EG20-LIBCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000470
                                                                                
