      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ECS30   ECS30                                              00000020
      ***************************************************************** 00000030
       01   EMQ31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(5).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDATEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNDATEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNDATEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDATEI   PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MWREML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWREMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWREMI    PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFONCI   PIC X(3).                                       00000330
           02 MCLIEUD OCCURS   42 TIMES .                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEUL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCLIEUF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCLIEUI      PIC X(3).                                  00000380
           02 MCFJRD OCCURS   42 TIMES .                                00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFJRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MCFJRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFJRF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MCFJRI  PIC X.                                          00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLIBERRI  PIC X(78).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCODTRAI  PIC X(4).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCICSI    PIC X(5).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNETNAMI  PIC X(8).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MSCREENI  PIC X(4).                                       00000630
      ***************************************************************** 00000640
      * SDF: ECS30   ECS30                                              00000650
      ***************************************************************** 00000660
       01   EMQ31O REDEFINES EMQ31I.                                    00000670
           02 FILLER    PIC X(12).                                      00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MDATJOUA  PIC X.                                          00000700
           02 MDATJOUC  PIC X.                                          00000710
           02 MDATJOUP  PIC X.                                          00000720
           02 MDATJOUH  PIC X.                                          00000730
           02 MDATJOUV  PIC X.                                          00000740
           02 MDATJOUO  PIC X(10).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MTIMJOUA  PIC X.                                          00000770
           02 MTIMJOUC  PIC X.                                          00000780
           02 MTIMJOUP  PIC X.                                          00000790
           02 MTIMJOUH  PIC X.                                          00000800
           02 MTIMJOUV  PIC X.                                          00000810
           02 MTIMJOUO  PIC X(5).                                       00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MNPAGEA   PIC X.                                          00000840
           02 MNPAGEC   PIC X.                                          00000850
           02 MNPAGEP   PIC X.                                          00000860
           02 MNPAGEH   PIC X.                                          00000870
           02 MNPAGEV   PIC X.                                          00000880
           02 MNPAGEO   PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MNSOCA    PIC X.                                          00000910
           02 MNSOCC    PIC X.                                          00000920
           02 MNSOCP    PIC X.                                          00000930
           02 MNSOCH    PIC X.                                          00000940
           02 MNSOCV    PIC X.                                          00000950
           02 MNSOCO    PIC X(3).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MNDATEA   PIC X.                                          00000980
           02 MNDATEC   PIC X.                                          00000990
           02 MNDATEP   PIC X.                                          00001000
           02 MNDATEH   PIC X.                                          00001010
           02 MNDATEV   PIC X.                                          00001020
           02 MNDATEO   PIC X(8).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MWREMA    PIC X.                                          00001050
           02 MWREMC    PIC X.                                          00001060
           02 MWREMP    PIC X.                                          00001070
           02 MWREMH    PIC X.                                          00001080
           02 MWREMV    PIC X.                                          00001090
           02 MWREMO    PIC X.                                          00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MCFONCA   PIC X.                                          00001120
           02 MCFONCC   PIC X.                                          00001130
           02 MCFONCP   PIC X.                                          00001140
           02 MCFONCH   PIC X.                                          00001150
           02 MCFONCV   PIC X.                                          00001160
           02 MCFONCO   PIC X(3).                                       00001170
           02 DFHMS1 OCCURS   42 TIMES .                                00001180
             03 FILLER       PIC X(2).                                  00001190
             03 MCLIEUA      PIC X.                                     00001200
             03 MCLIEUC PIC X.                                          00001210
             03 MCLIEUP PIC X.                                          00001220
             03 MCLIEUH PIC X.                                          00001230
             03 MCLIEUV PIC X.                                          00001240
             03 MCLIEUO      PIC X(3).                                  00001250
           02 DFHMS2 OCCURS   42 TIMES .                                00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MCFJRA  PIC X.                                          00001280
             03 MCFJRC  PIC X.                                          00001290
             03 MCFJRP  PIC X.                                          00001300
             03 MCFJRH  PIC X.                                          00001310
             03 MCFJRV  PIC X.                                          00001320
             03 MCFJRO  PIC X.                                          00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLIBERRA  PIC X.                                          00001350
           02 MLIBERRC  PIC X.                                          00001360
           02 MLIBERRP  PIC X.                                          00001370
           02 MLIBERRH  PIC X.                                          00001380
           02 MLIBERRV  PIC X.                                          00001390
           02 MLIBERRO  PIC X(78).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCODTRAA  PIC X.                                          00001420
           02 MCODTRAC  PIC X.                                          00001430
           02 MCODTRAP  PIC X.                                          00001440
           02 MCODTRAH  PIC X.                                          00001450
           02 MCODTRAV  PIC X.                                          00001460
           02 MCODTRAO  PIC X(4).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCICSA    PIC X.                                          00001490
           02 MCICSC    PIC X.                                          00001500
           02 MCICSP    PIC X.                                          00001510
           02 MCICSH    PIC X.                                          00001520
           02 MCICSV    PIC X.                                          00001530
           02 MCICSO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNETNAMA  PIC X.                                          00001560
           02 MNETNAMC  PIC X.                                          00001570
           02 MNETNAMP  PIC X.                                          00001580
           02 MNETNAMH  PIC X.                                          00001590
           02 MNETNAMV  PIC X.                                          00001600
           02 MNETNAMO  PIC X(8).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MSCREENA  PIC X.                                          00001630
           02 MSCREENC  PIC X.                                          00001640
           02 MSCREENP  PIC X.                                          00001650
           02 MSCREENH  PIC X.                                          00001660
           02 MSCREENV  PIC X.                                          00001670
           02 MSCREENO  PIC X(4).                                       00001680
                                                                                
