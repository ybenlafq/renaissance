      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT :         *               
      *        "JHC011".                                        *               
      *    LONGUEUR : 250                                       *               
      *    RIDFLD   : 027                                       *               
      *---------------------------------------------------------*               
       01  RBA-D10.                                                             
           05  VSAM-CLE.                                                        
               10  VSAM-ETAT             PIC X(06).                             
               10  VSAM-DATE             PIC X(08).                             
               10  VSAM-USER             PIC X(08).                             
               10  VSAM-NSEQ             PIC 9(05).                             
           05  VSAM-CLE-R                REDEFINES  VSAM-CLE.                   
               10  VSAM-CLE06            PIC X(14).                             
               10  FILLER                PIC X(13).                             
           05  VSAM-DONNEES.                                                    
               10  RBA-D10-CTIERS    PIC X(17).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-NENVOI    PIC X(07).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-DENVOI    PIC X(08).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-ADRESSE   PIC X(08).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-NCODIC    PIC X(07).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-QTE       PIC X(01).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-CFAM      PIC X(05).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-CMARQ     PIC X(05).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-LREF      PIC X(20).                              000
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-CSERIE    PIC X(16).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-NLIEUHC   PIC X(03).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-NCHS      PIC X(07).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-NACCORD   PIC X(13).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-DACCFOUR  PIC X(08).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-INTERLOC  PIC X(10).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-PVTTC     PIC -9(7)V99.                              
               10  RBA-D10-PVTTC-R   REDEFINES  RBA-D10-PVTTC                   
                                     PIC X(10).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-PRMP      PIC -9(7)V9(6).                            
               10  RBA-D10-PRMP-R    REDEFINES  RBA-D10-PRMP                    
                                     PIC X(14).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                   000
               10  RBA-D10-ECOP      PIC -9(5)V99.                              
               10  RBA-D10-ECOP-R    REDEFINES  RBA-D10-ECOP                    
                                     PIC X(08).                                 
               10  FILLER            PIC X(01)  VALUE ';'.               000019 
           05  FILLER                PIC X(38)  VALUE ' '.               000019 
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
