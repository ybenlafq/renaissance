      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                                         
      * MODULE DE TRAITEMENT DES VALEURS NUMERIQUES (ENTIERS POSITIFS)          
      *                                                                         
      *****************************************************************         
       TRAITEMENT-VALN SECTION.                                                 
      *****************************************************************         
      *                                                                         
       DEB-TRAITEMENT-VALN.                                                     
      *                                                                         
           IF W-VALN-USER = SPACE OR LOW-VALUES                                 
                   PERFORM T-VALN-FILE                                          
           ELSE                                                                 
                   PERFORM T-VALN-USER                                          
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-VALN. EXIT.                                               
      *                                                                         
      *****************************************************************         
       T-VALN-USER     SECTION.                                                 
      *****************************************************************         
      *                                                                         
       DEB-T-VALN-USER.                                                         
      *                                                                         
           MOVE 5 TO CODE-RETOUR.                                               
      *                                                                         
           MOVE SPACE TO W-VALN-FILE-X.                                         
      *                                                                         
           MOVE   18    TO W-VALN-I.                                            
      *                                                                         
           PERFORM T-VALN-CHERCHE                                               
                   UNTIL W-VALN-USER-CAR (W-VALN-I) NOT = ' '.                  
      *                                                                         
           MOVE   18   TO W-VALN-J.                                             
      *                                                                         
           PERFORM T-VALN-TRANSFERT                                             
                   UNTIL W-VALN-I < 1.                                          
      *                                                                         
           INSPECT W-VALN-FILE-X                                                
                   REPLACING LEADING ' ' BY '0'.                                
      *                                                                         
           IF W-VALN-FILE-X NOT NUMERIC                                         
                   GO TO FIN-TRAITEMENT-VALN                                    
           END-IF.                                                              
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
       FIN-T-VALN-USER. EXIT.                                                   
      *                                                                         
      *****************************************************************         
       T-VALN-CHERCHE SECTION.                                                  
      *****************************************************************         
      *                                                                         
       DEB-T-VALN-CHERCHE.                                                      
      *                                                                         
           SUBTRACT 1 FROM W-VALN-I.                                            
      *                                                                         
       FIN-T-VALN-CHERCHE. EXIT.                                                
      *                                                                         
      *****************************************************************         
       T-VALN-TRANSFERT SECTION.                                                
      *****************************************************************         
      *                                                                         
       DEB-T-VALN-TRANSFERT.                                                    
      *                                                                         
           MOVE W-VALN-USER-CAR (W-VALN-I)                                      
             TO W-VALN-FILE-CAR (W-VALN-J).                                     
      *                                                                         
           SUBTRACT 1 FROM W-VALN-I.                                            
           SUBTRACT 1 FROM W-VALN-J.                                            
      *                                                                         
       FIN-T-VALN-TRANSFERT. EXIT.                                              
      *                                                                         
      *****************************************************************         
       T-VALN-FILE     SECTION.                                                 
      *****************************************************************         
      *                                                                         
       DEB-T-VALN-FILE.                                                         
      *                                                                         
           MOVE SPACE TO W-VALN-USER.                                           
           MOVE 1     TO W-VALN-I.                                              
           MOVE 1     TO W-VALN-J.                                              
      *                                                                         
       T-VALN-ZEROS.                                                            
      *                                                                         
           IF W-VALN-I NOT < 18                                                 
              GO TO T-VALN-ALIMENTE                                             
           END-IF.                                                              
      *                                                                         
           IF W-VALN-FILE-CAR (W-VALN-I) = '0'                                  
              ADD 1 TO W-VALN-I                                                 
              GO TO T-VALN-ZEROS                                                
           END-IF.                                                              
      *                                                                         
       T-VALN-ALIMENTE.                                                         
      *                                                                         
           MOVE W-VALN-FILE-CAR (W-VALN-I)                                      
             TO W-VALN-USER-CAR (W-VALN-J).                                     
      *                                                                         
           IF W-VALN-I NOT < 18                                                 
              GO TO FIN-T-VALN-FILE                                             
           END-IF.                                                              
      *                                                                         
           ADD 1 TO W-VALN-I.                                                   
           ADD 1 TO W-VALN-J.                                                   
           GO TO T-VALN-ALIMENTE.                                               
      *                                                                         
       FIN-T-VALN-FILE. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
