      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2047                            *        
      ******************************************************************        
       01  RVXM2047.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2047-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2047-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2047-NENTCDE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2047-NENTCDE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2047-NENTCDE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2047-NENTCDE-TEXT      PIC X(05).                            
           10 XM2047-LENTCDE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2047-LENTCDE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2047-LENTCDE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2047-LENTCDE-TEXT      PIC X(20).                            
           10 XM2047-WIMPORT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2047-WIMPORT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2047-WIMPORT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2047-WIMPORT-TEXT      PIC X(01).                            
           10 XM2047-CECO.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2047-CECO-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2047-CECO-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2047-CECO-TEXT         PIC X(10).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2047                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2047-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2047-NENTCDE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2047-NENTCDE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2047-LENTCDE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2047-LENTCDE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2047-WIMPORT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2047-WIMPORT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2047-CECO-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2047-CECO-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 05      *        
      ******************************************************************        
                                                                                
