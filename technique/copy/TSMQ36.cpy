      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMQ36 = TS DE CONSULTATION D'UN MESSAGE                   *         
      *                                                               *         
      *    CETTE TS CONTIENT LE  MESSAGE  A AFFICHER                  *         
      *    1 SEQUENCE = LIGNE DE MESSAGE                              *         
      *                                                               *         
      *    LONGUEUR TOTALE = 78                                       *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS36                      PIC S9(05) COMP-3 VALUE +0.               
       01  ITS36-MAX                  PIC S9(05) COMP-3 VALUE +0.               
      *                                                               *         
       01  TS36-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'MQ36'.             
           05 TS36-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS36-LONG                  PIC S9(04) COMP VALUE +78.                
      *--                                                                       
       01  TS36-LONG                  PIC S9(04) COMP-5 VALUE +78.              
      *}                                                                        
      *                                                               *         
       01  TS36-DONNEES.                                                        
           05 TS36-LIGNE           PIC X(78).                                   
                                                                                
