      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2033                  *        
      ******************************************************************        
       01  RVXM2033.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2033-REFID            USAGE SQL TYPE IS ROWID.                  
      *--                                                                       
           10 XM2033-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2033-CPREST.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2033-CPREST-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2033-CPREST-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2033-CPREST-TEXT   PIC X(5).                                 
           10 XM2033-CCDESC.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2033-CCDESC-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2033-CCDESC-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2033-CCDESC-TEXT   PIC X(5).                                 
           10 XM2033-CVDESC.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2033-CVDESC-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2033-CVDESC-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2033-CVDESC-TEXT   PIC X(5).                                 
      ******************************************************************        
      * FLAGS                                                          *        
      ******************************************************************        
       01  RVXM2033-F.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2033-CPREST-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2033-CPREST-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2033-CCDESC-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2033-CCDESC-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2033-CVDESC-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2033-CVDESC-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
