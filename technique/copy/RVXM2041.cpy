      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2041                            *        
      ******************************************************************        
       01  RVXM2041.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2041-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2041-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2041-CFAM.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-CFAM-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-CFAM-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-CFAM-TEXT         PIC X(05).                            
           10 XM2041-LFAM.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-LFAM-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-LFAM-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-LFAM-TEXT         PIC X(20).                            
           10 XM2041-CTYPENT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-CTYPENT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-CTYPENT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-CTYPENT-TEXT      PIC X(02).                            
           10 XM2041-CHEFPRO.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-CHEFPRO-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-CHEFPRO-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-CHEFPRO-TEXT      PIC X(20).                            
           10 XM2041-TXTVA.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-TXTVA-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-TXTVA-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-TXTVA-TEXT        PIC X(05).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2041-CLIENTREQ            PIC S9(9) USAGE COMP.                 
      *--                                                                       
           10 XM2041-CLIENTREQ            PIC S9(9) COMP-5.                     
      *}                                                                        
           10 XM2041-CGARANTIE.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2041-CGARANTIE-LEN     PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-CGARANTIE-LEN     PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2041-CGARANTIE-TEXT    PIC X(05).                            
E0713      10 XM2041-CGARANMGD.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0713 *       49 XM2041-CGARANMGD-LEN     PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2041-CGARANMGD-LEN     PIC S9(4) COMP-5.                     
      *}                                                                        
E0713         49 XM2041-CGARANMGD-TEXT    PIC X(05).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2041                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2041-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-CFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-LFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-LFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-CTYPENT-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CTYPENT-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-CHEFPRO-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CHEFPRO-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-TXTVA-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-TXTVA-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-CLIENTREQ-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CLIENTREQ-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2041-CGARANTIE-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CGARANTIE-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0713 *    02 XM2041-CGARANMGD-F         PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2041-CGARANMGD-F         PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 08      *        
      ******************************************************************        
                                                                                
