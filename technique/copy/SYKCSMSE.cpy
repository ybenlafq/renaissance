      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000600
      *==> DARTY ****************************************************   00000400
      *          * SEND MAP ERREUR *                                *   00000200
      **************************************************** SYKCSMSE *   00000400
      *---------------------------------------                          00000600
       SEND-MAP-SORTIE-ERREUR          SECTION.                         00000500
      *---------------------------------------                          00000600
           PERFORM INQUIRE-TRANSACTION.                                         
      *    IF NOM-PROG-TACHE = 'TGM99'                                          
      *       PERFORM ENVOI-TS-MAP-SORTIE-ERREUR                                
      *    ELSE                                                                 
              PERFORM ENVOI-MAP-SORTIE-ERREUR.                                  
      *    END-IF.                                                              
      *---------------------------------------                          00000600
       ENVOI-TS-MAP-SORTIE-ERREUR      SECTION.                         00000500
      *---------------------------------------                          00000600
           IF JUMP                                                              
           OR Z-COMMAREA-SELECT  NOT =  SPACES                                  
              SET  COMGM00-ERASE       TO TRUE                                  
           ELSE                                                                 
              SET  COMGM00-DATAONLY    TO TRUE                                  
           END-IF.                                                              
           PERFORM MAJ-TS-MAP.                                                  
      *---------------------------------------                          00000600
       ENVOI-MAP-SORTIE-ERREUR         SECTION.                         00000500
      *---------------------------------------                          00000600
           IF JUMP  OR  Z-COMMAREA-SELECT  NOT =  SPACES                        
      *dfhei*{ decommente EXEC                                                  
       THEN                                                                     
       EXEC CICS SEND                                                           
             MAP    (NOM-MAP)                                                   
             MAPSET (NOM-MAPSET)                                                
             FROM   (Z-MAP)                                                     
             LENGTH (LENGTH OF Z-MAP)                                           
             CURSOR                                                             
             ALARM                                                              
             ERASE                                                              
             FRSET                                                              
             FREEKB                                                             
             NOHANDLE                                                           
       END-EXEC                                                                 
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     THEN MOVE '�1GS��00029   ' TO DFHEIV0                      
      *dfhei*          MOVE LENGTH OF Z-MAP TO DFHB0020                         
      *dfhei*          MOVE -1 TO DFHB0021                                      
      *dfhei*          CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHB00        
      *dfhei*     NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY DFHB0021                
      *dfhei*} decommente EXEC                                                  
       ELSE                                                                     
       EXEC CICS SEND                                                           
            MAP    (NOM-MAP)                                                    
            MAPSET (NOM-MAPSET)                                                 
            FROM   (Z-MAP)                                                      
            LENGTH (LENGTH OF Z-MAP)                                            
            DATAONLY                                                            
            CURSOR                                                              
            ALARM                                                               
            FRSET                                                               
            FREEKB                                                              
            NOHANDLE                                                            
       END-EXEC                                                                 
      *    ELSE MOVE '�1G���00041   ' TO DFHEIV0                             
      *         MOVE LENGTH OF Z-MAP TO DFHB0020                                
      *         MOVE -1 TO DFHB0021                                             
      *         CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHB0020             
      *    NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY DFHB0021                       
           END-IF.                                                              
      *                                                                 00002100
           MOVE EIBRCODE TO EIB-RCODE.                                  00002200
      *                                                                 00002100
           IF   NOT EIB-NORMAL                                          00002300
                GO TO ABANDON-CICS                                      00002400
           END-IF.                                                              
                                                                                
                                                                                
