      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT :         *               
      *        "JEF021".                                        *               
      *    LONGUEUR : 250                                       *               
      *    RIDFLD   : 027                                       *               
      *---------------------------------------------------------*               
       01  F021-ENREG.                                                          
           05  F021-CLE.                                                        
               10  F021-ETAT             PIC X(06).                             
               10  F021-DATE             PIC X(08).                             
               10  F021-USER             PIC X(08).                             
               10  F021-NSEQ             PIC 9(05).                             
           05  F021-DONNEES.                                                    
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-NRENDU           PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-DRENDU           PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-MRENDU           PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-LTIERS           PIC X(15).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CTIERS           PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CODE             PIC X(01).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-QTRDU            PIC 9(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-NCODIC           PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CFAM             PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-CMARQ            PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-LREF             PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-NLIEUHS          PIC X(03).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-NHS              PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-NENVOI           PIC X(07).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-DENVOI           PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-QTENV            PIC 9(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-PRMP             PIC -9(6)V99.                          
               10  F021-PRMP-R           REDEFINES  F021-PRMP                   
                                         PIC X(09).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-PVTTC            PIC -9(7)V99.                          
               10  F021-PVTTC-R          REDEFINES  F021-PVTTC                  
                                         PIC X(10).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
               10  F021-ECOPA            PIC -9(5)V99.                          
               10  F021-ECOPA-R          REDEFINES  F021-ECOPA                  
                                         PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.                  
           05  FILLER                    PIC X(50)  VALUE ' '.                  
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
