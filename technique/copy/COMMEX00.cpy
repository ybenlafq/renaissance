      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMEX00.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *           COMMAREA DU MODULE DE MAJ DU FICHIER                 *        
      *           DES PARAMETRES D'EXPLOITATION (TEX00)                *        
      *                                                                *        
      *           COMM-EX00-LONG-COMMAREA A UNE VALUE DE +120          *        
      *           Z-COMMAREA-EX00 = 200 C                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-EX00-LONG-COMMAREA      PIC S9(4) COMP   VALUE +120.            
      *--                                                                       
       01  COMM-EX00-LONG-COMMAREA      PIC S9(4) COMP-5   VALUE +120.          
      *}                                                                        
      *                                                                 00730918
       01  Z-COMMAREA-EX00.                                             00731018
      *                                 ZONE CLE                        00731709
           05 COMM-EX00-CLE.                                            00731818
      *                                 DATE DE PASSAGE PREVU DU STEP   00731909
              10 COMM-EX00-DATE.                                        00732018
                 15 COMM-EX00-AA        PIC X(02).                      00732218
                 15 COMM-EX00-MM        PIC X(02).                      00732418
                 15 COMM-EX00-JJ        PIC X(02).                      00732618
      *                                 NOM DU PROGRAMME CICS APPELANT  00732709
              10 COMM-EX00-NOMPGM       PIC X(05).                      00732818
      *                                 NUMERO DE TACHE                 00732909
              10 COMM-EX00-NUMTAC       PIC X(02).                      00733018
      *                                 NUMERO DE CARTE                 00733109
              10 COMM-EX00-NUMCAR       PIC X(02).                      00733218
      *                                 CODE MISE A JOUR (G/I/D/R)      00733309
           05 COMM-EX00-CODMAJ          PIC X(01).                      00733418
      *                                 CODE RETOUR (00/99/98)          00733509
           05 COMM-EX00-CODRET          PIC X(02).                      00733618
      *                                 ZONE INUTILISEE                 00733509
           05 COMM-EX00-FILLER1         PIC X(15).                      00733618
      *                                 ZONE PARAMETRE                  00733709
           05 COMM-EX00-ZPARAM          PIC X(80).                      00733818
      *                                 LIBRE                           00733509
           05 COMM-EX00-LIBRE           PIC X(87).                              
                                                                                
