      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2024                  *        
      ******************************************************************        
       01  RVXM2024.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2024-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2024-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2024-NCODIC          PIC X(07).                                 
           10 XM2024-NSOC            PIC X(03).                                 
           10 XM2024-NENTCDE         PIC X(05).                                 
           10 XM2024-CTAXE           PIC X(05).                                 
           10 XM2024-CPAYS           PIC X(02).                                 
           10 XM2024-DEFFET          PIC X(08).                                 
           10 XM2024-DFINEFFET       PIC X(08).                                 
           10 XM2024-PMONTANT        PIC S9(5)V9(2)  COMP-3.                    
           10 XM2024-WIMPORT         PIC X(01).                                 
           10 XM2024-CPRODECO        PIC X(20).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2024-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-NSOC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-NSOC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-NENTCDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-NENTCDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-CTAXE-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-CTAXE-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-CPAYS-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-CPAYS-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-DEFFET-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-DEFFET-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-DFINEFFET-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-DFINEFFET-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-PMONTANT-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-PMONTANT-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-WIMPORT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2024-WIMPORT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2024-CPRODECO-F      PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2024-CPRODECO-F      PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
