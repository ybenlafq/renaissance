      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE XML00 CARACTERES ET ENTITES XML        *        
      *----------------------------------------------------------------*        
       01  RVXML00.                                                             
           05  XML00-CTABLEG2    PIC X(15).                                     
           05  XML00-CTABLEG2-REDEF REDEFINES XML00-CTABLEG2.                   
               10  XML00-CARACT          PIC X(01).                             
           05  XML00-WTABLEG     PIC X(80).                                     
           05  XML00-WTABLEG-REDEF  REDEFINES XML00-WTABLEG.                    
               10  XML00-ENTITE          PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVXML00-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XML00-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  XML00-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XML00-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  XML00-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
