      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2010                            *        
      ******************************************************************        
       01  RVXM2010.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2010-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2010-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2010-SOURCE.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2010-SOURCE-LEN        PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2010-SOURCE-LEN        PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2010-SOURCE-TEXT       PIC X(32).                            
           10 XM2010-REFERENCE.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2010-REFERENCE-LEN     PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2010-REFERENCE-LEN     PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2010-REFERENCE-TEXT    PIC X(32).                            
           10 XM2010-ISSUED               PIC X(26).                            
      ******************************************************************        
      * FLAGS                                                          *        
      ******************************************************************        
       01  RVXM2010-F.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2010-REFID-F              PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2010-REFID-F              PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2010-SOURCE-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2010-SOURCE-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2010-REFERENCE-F          PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2010-REFERENCE-F          PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2010-ISSUED-F             PIC S9(4) USAGE COMP.                 
      *                                                                         
      *--                                                                       
           10 XM2010-ISSUED-F             PIC S9(4) COMP-5.                     
                                                                                
      *}                                                                        
