      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * CADRAGE DE DATE                                                         
      *****************************************************************         
       01  C-D-SAISIDATE.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  C-D-I         PIC S9(4)    VALUE ZERO     COMP.                  
      *--                                                                       
           05  C-D-I         PIC S9(4)    VALUE ZERO COMP-5.                    
      *}                                                                        
           05  C-D-DATE      PIC X(10)    VALUE SPACES.                         
           05  C-D-JJ        PIC X(2)     VALUE SPACES.                         
           05  C-D-MM        PIC X(2)     VALUE SPACES.                         
           05  C-D-SA        PIC X(4)     VALUE SPACES.                         
           05  C-D-CRETOUR   PIC X        VALUE SPACES.                         
               88  C-D-OK VALUE 'O'.                                            
               88  C-D-KO VALUE 'K'.                                            
                                                                                
