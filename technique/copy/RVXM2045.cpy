      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2045                            *        
      ******************************************************************        
       01  RVXM2045.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2045-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2045-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2045-CNOMDOU.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2045-CNOMDOU-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2045-CNOMDOU-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2045-CNOMDOU-TEXT      PIC X(08).                            
           10 XM2045-LLNOMDOU.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2045-LLNOMDOU-LEN      PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2045-LLNOMDOU-LEN      PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2045-LLNOMDOU-TEXT     PIC X(500).                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2045                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2045-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2045-CNOMDOU-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2045-CNOMDOU-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2045-LLNOMDOU-F          PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2045-LLNOMDOU-F          PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 03      *        
      ******************************************************************        
                                                                                
