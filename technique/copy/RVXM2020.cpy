      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2020                  *        
      ******************************************************************        
       01  RVXM2020.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2020-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2020-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2020-NCODIC          PIC X(07).                                 
           10 XM2020-CDESCRIPTIF     PIC X(05).                                 
           10 XM2020-CVDESCRIPT      PIC X(05).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2020-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2020-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2020-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2020-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2020-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2020-CDESCRIPTIF-F   PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2020-CDESCRIPTIF-F   PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2020-CVDESCRIPT-F    PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2020-CVDESCRIPT-F    PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
