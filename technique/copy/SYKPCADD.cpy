      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *    CADRAGE D'UNE DATE                                                   
      ******************************************************************        
      *-----------------------------------------                                
       CADRE-DATE                        SECTION.                               
      *-----------------------------------------                                
           SET C-D-OK TO TRUE                                                   
      **** CARACTERES NON PERMIS                                                
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 10                   
             IF ( (C-D-DATE(C-D-I:1) < '0') OR                                  
                  (C-D-DATE(C-D-I:1) > '9'))                                    
              AND (C-D-DATE(C-D-I:1) NOT = '/' AND ' ' AND '.')                 
               SET C-D-KO TO TRUE                                               
               GO FIN-CADRE-DATE                                                
             END-IF                                                             
           END-PERFORM.                                                         
      **** RECHERCHE DU JOUR                                                    
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 10                   
             IF (C-D-DATE(C-D-I:1) >= '0') AND                                  
                (C-D-DATE(C-D-I:1) <= '9')                                      
                MOVE C-D-DATE(C-D-I:1) TO C-D-JJ                                
                ADD 1 TO C-D-I                                                  
                IF (C-D-DATE(C-D-I:1) >= '0') AND                               
                   (C-D-DATE(C-D-I:1) <= '9')                                   
                  MOVE C-D-DATE(C-D-I:1) TO C-D-JJ(2:1)                         
                  ADD 1 TO C-D-I                                                
                ELSE                                                            
                  MOVE C-D-JJ(1:1) TO C-D-JJ(2:1)                               
                  MOVE '0' TO C-D-JJ(1:1)                                       
                END-IF                                                          
             MOVE C-D-DATE(C-D-I:) TO C-D-DATE                                  
             MOVE 11 TO C-D-I                                                   
             END-IF                                                             
           END-PERFORM.                                                         
      **** RECHERCHE DU MOIS                                                    
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 10                   
             IF (C-D-DATE(C-D-I:1) >= '0') AND                                  
                (C-D-DATE(C-D-I:1) <= '9')                                      
                MOVE C-D-DATE(C-D-I:1) TO C-D-MM                                
                ADD 1 TO C-D-I                                                  
                IF (C-D-DATE(C-D-I:1) >= '0') AND                               
                   (C-D-DATE(C-D-I:1) <= '9')                                   
                  MOVE C-D-DATE(C-D-I:1) TO C-D-MM(2:1)                         
                  ADD 1 TO C-D-I                                                
                ELSE                                                            
                  MOVE C-D-MM(1:1) TO C-D-MM(2:1)                               
                  MOVE '0' TO C-D-MM(1:1)                                       
                END-IF                                                          
             MOVE C-D-DATE(C-D-I:) TO C-D-DATE                                  
             MOVE 11 TO C-D-I                                                   
             END-IF                                                             
           END-PERFORM.                                                         
      **** RECHERCHE DU SIECLE / ANNEE                                          
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 10                   
             IF (C-D-DATE(C-D-I:1) >= '0') AND                                  
                (C-D-DATE(C-D-I:1) <= '9')                                      
               MOVE C-D-DATE(C-D-I:) TO C-D-SA                                  
               MOVE 11 TO C-D-I                                                 
             END-IF                                                             
           END-PERFORM.                                                         
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 4                    
             IF (C-D-SA(C-D-I:1) < '0') OR                                      
                (C-D-SA(C-D-I:1) > '9')                                         
                MOVE SPACES TO C-D-SA(C-D-I:)                                   
                MOVE 5 TO C-D-I                                                 
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM VARYING C-D-I FROM 1 BY 1 UNTIL C-D-I > 4                    
             IF C-D-SA(4:1) = ' '                                               
                MOVE C-D-SA(1:3) TO C-D-DATE                                    
                MOVE C-D-DATE    TO C-D-SA(2:3)                                 
                MOVE '0'         TO C-D-SA(1:1)                                 
              ELSE                                                              
                MOVE 5 TO C-D-I                                                 
              END-IF                                                            
           END-PERFORM.                                                         
      **** SI LE SIECLE N'EST PAS RENSEIGNE, ON LE MET                          
           IF C-D-SA(1:2) = '00'                                                
             IF C-D-SA(3:2) >= '50'                                             
               MOVE '19' TO C-D-SA(1:2)                                         
             ELSE                                                               
               MOVE '20' TO C-D-SA(1:2)                                         
             END-IF                                                             
           END-IF                                                               
           STRING C-D-JJ '/' C-D-MM '/' C-D-SA                                  
                  DELIMITED BY SIZE INTO C-D-DATE.                              
       FIN-CADRE-DATE. EXIT.                                                    
      ******************************************************************        
      * FIN DE SECTION CADRAGE DE DATE                                          
      ******************************************************************        
                                                                                
