      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  LINKSYALLOC1.                                                        
           02 MESS-APPC    PIC X(80).                                           
           02 LG-DONNEES-APPC   PIC S9(5) COMP-3.                               
           02 SYSID-APPC        PIC X(4).                                       
           02 PROCNAME-APPC     PIC X(15).                                      
      *                                                                         
      *                     FLAG ANOMALIE : 0 ---> OK                           
      *                                     1 ---> ERREUR                       
      *                                     2 ---> REMPLACANT                   
      *                                                                         
      * LA TOUCHE ENTREE PROVOQUE L'ENVOI DE LA PAGE COURANTE                   
      * LA TOUCHE PF5 PROVOQUE L'ENVOI DES 4 PAGES ET VALIDE                    
      *                                                                         
           02 BUFFER-AS400.                                                     
              03 PROGRAMME                        PIC X(5).                     
              03 CICS                             PIC X(5).                     
              03 BUFFER-TCD21.                                                  
                 04 ZCONTROLE-21.                                               
                    05 TOUCHE-21                  PIC X(1).                     
                    05 NBRLIGNE-21                PIC 9(2).                     
                 04 ENTETE-21.                                                  
                    05 CLIENT-21                  PIC X(6).                     
                    05 FLAGCLIENT-21              PIC X(1).                     
                    05 CRITERE-21                 PIC X(5).                     
                    05 FLAGCRITERE-21             PIC X(1).                     
                    05 REFCRITERE-21              PIC X(10).                    
                    05 PTLIVR-21                  PIC X(4).                     
                    05 FLAGPTLIVR-21              PIC X(1).                     
                    05 NCDE-21                    PIC X(6).                     
                    05 DEVISE-21                  PIC X(3).                     
                 04 LIGNES-21      OCCURS 32.                                   
                    05 L1-21.                                                   
                       06 NCODIC-21               PIC X(7).                     
                       06 FLAGNCODIC-21           PIC X(1).                     
                       06 QTE-21                  PIC 9(5).                     
                       06 UNITEVTE-21             PIC X(2).                     
                       06 COMMENT-21              PIC X(20).                    
                       06 DLIVRE-21               PIC X(8).                     
                       06 LIBELLE-21              PIC X(30).                    
                    05 L2-21.                                                   
                       06 INFO-21                 PIC X(65).                    
                       06 PRIX-21                 PIC 9(9)V99.                  
      *-----                                                                    
              03 BUFFER-TCD22   REDEFINES    BUFFER-TCD21.                      
                 04 ZCONTROLE-22.                                               
                    05 TOUCHE-22                  PIC X(1).                     
                    05 NBRLIGNE-22                PIC 9(2).                     
                 04 ENTETE-22.                                                  
                    05 NCODIC-22                  PIC X(7).                     
                    05 FLAGNCODIC-22              PIC X(1).                     
                    05 CLIENT-22                  PIC X(6).                     
                    05 FLAGCLIENT-22              PIC X(1).                     
                    05 LIBNCODIC-22               PIC X(30).                    
                    05 LIBCLIENT-22               PIC X(30).                    
                    05 MESSAGE-22                 PIC X(80).                    
                 04 LIGNES-22      OCCURS 64.                                   
                    05 ANNULATION-22              PIC X(1).                     
                    05 FLAGANNULATION-22          PIC X(1).                     
                    05 CMD-22                     PIC X(6).                     
                    05 LIGNE-22                   PIC X(3).                     
                    05 DATE-22.                                                 
                       06 DSS-22                  PIC X(2).                     
                       06 DAA-22                  PIC X(2).                     
                       06 DMM-22                  PIC X(2).                     
                       06 DJJ-22                  PIC X(2).                     
                    05 QTECMD-22                  PIC 9(5).                     
                    05 STATUT-22                  PIC X(3).                     
                    05 QTES-22                    PIC 9(5).                     
                    05 COMM-22                    PIC X(17).                    
                    05 PTLIV-22                   PIC X(4).                     
                    05 CRIT-22                    PIC X(5).                     
                    05 REF-22                     PIC X(10).                    
      *-----                                                                    
              03 BUFFER-TCD23   REDEFINES    BUFFER-TCD21.                      
                 04 ZCONTROLE-23.                                               
                    05 TOUCHE-23                  PIC X(1).                     
                    05 NBRLIGNE-23                PIC 9(2).                     
                 04 ENTETE-23.                                                  
                    05 CLIENT-23                  PIC X(6).                     
                    05 FLAGCLIENT-23              PIC X(1).                     
                    05 LIBCLIENT-23               PIC X(30).                    
                    05 MESSAGE-23                 PIC X(80).                    
                 04 LIGNES-23      OCCURS 64.                                   
                    05 CMD-23                     PIC X(6).                     
                    05 REF1-23                    PIC X(5).                     
                    05 REF2-23                    PIC X(10).                    
                    05 DS-23.                                                   
                       06 DSSS-23                 PIC X(2).                     
                       06 DSAA-23                 PIC X(2).                     
                       06 DSMM-23                 PIC X(2).                     
                       06 DSJJ-23                 PIC X(2).                     
                    05 NBLIGNE-23                 PIC 9(5).                     
                    05 PTLIV-23                   PIC X(4).                     
                    05 DE-23.                                                   
                       06 DESS-23                 PIC X(2).                     
                       06 DEAA-23                 PIC X(2).                     
                       06 DEMM-23                 PIC X(2).                     
                       06 DEJJ-23                 PIC X(2).                     
                                                                                
