      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMQ34 = TS DE CONSULTATION DES ANOMALIES MQ10             *         
      *                                                               *         
      *    CETTE TS CONTIENT LES MESSAGES A AFFICHER                  *         
      *    1 SEQUENCE = DATE/HEURE/SOC/LIEU/CODE FONCTION/            *         
      *                 TAILLE/PRIORITE/MSGID                         *         
      *                                                               *         
      *    LONGUEUR TOTALE = 76                                       *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS34                      PIC S9(05) COMP-3 VALUE +0.               
       01  ITS34-MAX                  PIC S9(05) COMP-3 VALUE +0.               
      *                                                               *         
       01  TS34-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'MQ34'.             
           05 TS34-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS34-LONG                  PIC S9(04) COMP VALUE +99.                
      *--                                                                       
       01  TS34-LONG                  PIC S9(04) COMP-5 VALUE +99.              
      *}                                                                        
      *                                                               *         
       01  TS34-DONNEES.                                                        
           05 TS34-MSGID           PIC X(24).                                   
           05 TS34-CORREL          PIC X(24).                                   
           05 TS34-DATE            PIC X(08).                                   
           05 TS34-HEURE           PIC X(04).                                   
           05 TS34-NSOC            PIC X(03).                                   
           05 TS34-NLIEU           PIC X(03).                                   
           05 TS34-CFONC           PIC X(03).                                   
           05 TS34-TAILLE          PIC 9(05).                                   
           05 TS34-PRIOR           PIC 9(02).                                   
           05 TS34-CHRONO          PIC 9(07).                                   
           05 TS34-TOTAL           PIC 9(07).                                   
           05 TS34-EXPIRY          PIC 9(09).                                   
                                                                                
