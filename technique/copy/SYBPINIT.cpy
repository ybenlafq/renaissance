      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000102
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000202
      *                 I N I T  -  G E N E R A L E                   * 00000302
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000402
      *                                                                 00000500
       INIT-GENERALE                SECTION.                            00000601
      *                                                                 00000701
      *****************RECUPERATION DE LA DATE ET DE L'HEURE SYSTEME*** 00000903
      *                                                                 00001100
      * - DATE ET HEURE DE COMPILATION -                                00001200
      *                                                                 00001300
           MOVE WHEN-COMPILED TO Z-WHEN-COMPILED.                       00001400
      *                                                                 00001500
      * - DATE -                                                        00001600
      *                                                                 00001700
           ACCEPT Z-TIMER-HHMMSSCC FROM DATE.                           00002007
           MOVE Z-TIMER-TIMJOU-CAR (2) TO AA OF Z-TIMER-DATJOU.         00002206
           MOVE Z-TIMER-TIMJOU-CAR (3) TO MM OF Z-TIMER-DATJOU.         00002306
           MOVE Z-TIMER-TIMJOU-CAR (4) TO JJ OF Z-TIMER-DATJOU.         00002406
           MOVE CORRESPONDING Z-TIMER-DATJOU TO Z-TIMER-DATJOU-FILE.    00003000
           MOVE 19 TO SS OF Z-TIMER-DATJOU-FILE.                        00004000
      *                                                                 00005000
      * - HEURE -                                                       00006000
      *                                                                 00007000
           ACCEPT  Z-TIMER-HHMMSSCC FROM TIME.                          00008007
           MOVE Z-TIMER-TIMJOU-CAR (1) TO HH OF Z-TIMER-TIMJOU.         00009006
           MOVE Z-TIMER-TIMJOU-CAR (2) TO MM OF Z-TIMER-TIMJOU.         00010006
           MOVE Z-TIMER-TIMJOU-CAR (3) TO SS OF Z-TIMER-TIMJOU.         00030006
      *                                                                 00090001
       FIN-INIT-GENERALE. EXIT.                                         00100004
                EJECT                                                   00120001
                                                                                
