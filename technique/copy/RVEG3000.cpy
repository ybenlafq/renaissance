      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG3000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG3000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG3000.                                                    00000090
           02  EG30-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
           02  EG30-NOMCHAMP                                            00000120
               PIC X(0018).                                             00000130
           02  EG30-TYPCHAMP                                            00000140
               PIC X(0001).                                             00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-LGCHAMP                                             00000160
      *        PIC S9(4) COMP.                                          00000170
      *--                                                                       
           02  EG30-LGCHAMP                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-DECCHAMP                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG30-DECCHAMP                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-POSDSECT                                            00000200
      *        PIC S9(4) COMP.                                          00000210
      *--                                                                       
           02  EG30-POSDSECT                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-OCCURENCES                                          00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  EG30-OCCURENCES                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG30-DSYST                                               00000240
               PIC S9(13) COMP-3.                                       00000250
      *                                                                 00000260
      *---------------------------------------------------------        00000270
      *   LISTE DES FLAGS DE LA TABLE RVEG3000                          00000280
      *---------------------------------------------------------        00000290
      *                                                                 00000300
       01  RVEG3000-FLAGS.                                              00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-NOMETAT-F                                           00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  EG30-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-NOMCHAMP-F                                          00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  EG30-NOMCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-TYPCHAMP-F                                          00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  EG30-TYPCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-LGCHAMP-F                                           00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  EG30-LGCHAMP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-DECCHAMP-F                                          00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  EG30-DECCHAMP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-POSDSECT-F                                          00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  EG30-POSDSECT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-OCCURENCES-F                                        00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  EG30-OCCURENCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG30-DSYST-F                                             00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  EG30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000480
                                                                                
