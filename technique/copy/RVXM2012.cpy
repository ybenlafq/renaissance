      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2012                            *        
      ******************************************************************        
       01  RVXM2012.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2012-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2012-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2012-CONTEXT.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2012-CONTEXT-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2012-CONTEXT-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2012-CONTEXT-TEXT      PIC X(50).                            
           10 XM2012-NCODICK.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2012-NCODICK-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2012-NCODICK-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2012-NCODICK-TEXT      PIC X(7).                             
           10 XM2012-NCODIC.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2012-NCODIC-LEN        PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2012-NCODIC-LEN        PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2012-NCODIC-TEXT       PIC X(7).                             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2012                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2012-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2012-CONTEXT-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2012-CONTEXT-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2012-NCODICK-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2012-NCODICK-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2012-NCODIC-F             PIC S9(4) COMP.                       
      *--                                                                       
           02 XM2012-NCODIC-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *        
      ******************************************************************        
                                                                                
