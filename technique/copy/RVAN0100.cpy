      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAN0100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAN0100                         
      *   TABLE DES ANOMALIES ET DES RESOLUTIONS (ARIANE)                       
      **********************************************************                
       01  RVAN0100.                                                            
           02  AN01-CNOMPGRM                                                    
               PIC X(0006).                                                     
           02  AN01-NSEQERR                                                     
               PIC X(0004).                                                     
           02  AN01-CDEST                                                       
               PIC X(0012).                                                     
           02  AN01-NLIGNE                                                      
               PIC S9(03) COMP-3.                                               
           02  AN01-LIBANO                                                      
               PIC X(0050).                                                     
           02  AN01-LRESANO                                                     
               PIC X(0050).                                                     
           02  AN01-WDEST                                                       
               PIC X(0008).                                                     
           02  AN01-LIMPRIM                                                     
               PIC X(0006).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVAN0100                                  
      **********************************************************                
       01  RVAN0010-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-CNOMPGRM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-CNOMPGRM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-NSEQERR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-NSEQERR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-CDEST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-CDEST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-LIBANO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-LIBANO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-LRESANO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-LRESANO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-WDEST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AN01-WDEST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AN01-LIMPRIM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  AN01-LIMPRIM-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
