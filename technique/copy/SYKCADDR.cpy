      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *==> DARTY ******************************************* 19-10-93 *         
      *          * ADRESSAGE DES ZONES SYSTEME DE CICS                *         
      ****************************************************** SYKCADDR *         
       INIT-ADDRESS     SECTION.                                                
           IF  EIBTRMID NOT = SPACE AND LOW-VALUE                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *EXEC CICS ASSIGN TERMCODE (TERMCODE)                                     
      *                 NOHANDLE                                                
      *                 END-EXEC                                                
      *--                                                                       
       EXEC CICS ASSIGN TERMCODE (TERMCODE)                                     
                        NOHANDLE                                                
       END-EXEC                                                                 
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*         MOVE '��00007   ' TO DFHEIV0                           
      *dfhei*         CALL 'DFHEI1' USING DFHEIV0  TERMCODE                     
      *dfhei*} decommente EXEC                                                  
               MOVE EIBRCODE TO EIB-RCODE                                       
               IF   NOT EIB-NORMAL                                              
                    GO TO ABANDON-CICS                                          
               END-IF                                                           
           END-IF.                                                              
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS ADDRESS CWA    (ADDRESS OF LINK-CWA)                           
                         TCTUA  (ADDRESS OF LINK-TCTUA)                         
                         NOHANDLE                                               
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��00016   ' TO DFHEIV0                              
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  ADDRESS OF LINK-CWA ADDRE        
      *dfhei*     LINK-TCTUA.                                                   
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE TO EIB-RCODE.                                          
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
      * - C W A -                                                               
      *{Post-translation Correct-Val-pointeur      
      *     IF  ADDRESS OF LINK-CWA NOT EQUAL TO NULL THEN                       
           IF  ADDRESS OF LINK-CWA NOT EQUAL TO NULLP THEN
      *}
               MOVE LINK-CWA TO Z-CWA                                           
           ELSE                                                                 
               MOVE 'CWA NON ADRESSEE' TO MESS                                  
               GO TO ABANDON-AIDA                                               
           END-IF.                                                              
      * - T C T U A -                                                           
      *{Post-translation Correct-Val-pointeur      
      *     IF  ADDRESS OF LINK-TCTUA NOT EQUAL TO NULL THEN                     
           IF  ADDRESS OF LINK-TCTUA NOT EQUAL TO NULLP THEN
      *}
               MOVE LINK-TCTUA TO Z-TCTUA                                       
           ELSE                                                                 
               MOVE 'TCTUA NON ADRESSEE' TO MESS                                
               GO TO ABANDON-AIDA                                               
           END-IF.                                                              
      * - DATE DE COMPILATION -                                                 
           MOVE WHEN-COMPILED TO Z-WHEN-COMPILED.                               
      * - H E U R E -                                                           
           MOVE EIBTIME TO Z-TIMER-000HHMMSS.                                   
           MOVE Z-TIMER-TIMJOU-CAR (4) TO Z-TIMER-TIMJOU-CAR (1).               
           MOVE Z-TIMER-TIMJOU-CAR (5) TO Z-TIMER-TIMJOU-CAR (2).               
           MOVE 'H'                    TO Z-TIMER-TIMJOU-CAR (3).               
           MOVE Z-TIMER-TIMJOU-CAR (6) TO Z-TIMER-TIMJOU-CAR (4).               
           MOVE Z-TIMER-TIMJOU-CAR (7) TO Z-TIMER-TIMJOU-CAR (5).               
           MOVE 'M'                    TO Z-TIMER-TIMJOU-CAR (6).               
           MOVE Z-TIMER-TIMJOU-CAR (8) TO Z-TIMER-TIMJOU-CAR (7).               
           MOVE Z-TIMER-TIMJOU-CAR (9) TO Z-TIMER-TIMJOU-CAR (8).               
           MOVE 'S'                    TO Z-TIMER-TIMJOU-CAR (9).               
       FIN-INIT-ADDRESS.                                                        
         EXIT.                                                                  
       EJECT                                                                    
                                                                                
                                                                                
