      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TRAVAIL DE LA BRIQUE DE TRAITEMENT DES HEURES                  
      *****************************************************************         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-TIME ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-TIME-USER.                                                     
             10  HH                    PIC X(2) VALUE '00'.                     
             10  S1                    PIC X    VALUE 'H'.                      
             10  MM                    PIC X(2) VALUE '00'.                     
             10  S2                    PIC X    VALUE 'M'.                      
             10  SS                    PIC X(2) VALUE '00'.                     
             10  S3                    PIC X    VALUE 'S'.                      
           05  FILLER                  REDEFINES W-TIME-USER.                   
             10  FILLER                OCCURS 9.                                
               15  W-TIME-X            PIC X.                                   
      *                                                                         
           05  W-TIME-FILE.                                                     
             10  HH                    PIC 9(2) VALUE 0.                        
             10  MM                    PIC 9(2) VALUE 0.                        
             10  SS                    PIC 9(2) VALUE 0.                        
           05  FILLER REDEFINES W-TIME-FILE.                                    
             10  W-TIME-HH.                                                     
               15  W-TIME-H1           PIC X.                                   
               15  W-TIME-H2           PIC X.                                   
             10  W-TIME-MM.                                                     
               15  W-TIME-M1           PIC X.                                   
               15  W-TIME-M2           PIC X.                                   
             10  W-TIME-SS.                                                     
               15  W-TIME-S1           PIC X.                                   
               15  W-TIME-S2           PIC X.                                   
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-TIME-I                PIC S9(3) COMP-3 VALUE +0.               
           05  W-TIME-J                PIC S9(3) COMP-3 VALUE +0.               
       EJECT                                                                    
                                                                                
                                                                                
