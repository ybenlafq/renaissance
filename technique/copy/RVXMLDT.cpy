      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE XMLDT FORMAT DATE EN SORTIE MOD. XML   *        
      *----------------------------------------------------------------*        
       01  RVXMLDT.                                                             
           05  XMLDT-CTABLEG2    PIC X(15).                                     
           05  XMLDT-CTABLEG2-REDEF REDEFINES XMLDT-CTABLEG2.                   
               10  XMLDT-CDATE           PIC X(02).                             
           05  XMLDT-WTABLEG     PIC X(80).                                     
           05  XMLDT-WTABLEG-REDEF  REDEFINES XMLDT-WTABLEG.                    
               10  XMLDT-FDATE           PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVXMLDT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XMLDT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  XMLDT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  XMLDT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  XMLDT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
