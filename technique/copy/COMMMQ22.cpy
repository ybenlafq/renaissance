      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ HOST --> LOCAL                                        
      * TRAITEMENTS MQ - COTE HOST                                              
      *****************************************************************         
      * TAILLE 50124                                                            
      *                                                                         
      * CODRET : 1 = MQOPEN NON OK                                              
      *          2 = MQPUT    //                                                
      *          3 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          4 = SELECT SS-TABLE MQPUT SQLCODE < 0                          
      *          5 = MQCLOSE NON OK                                             
      *----------------------------------------------------------------         
       01  COMM-MQ22-APPLI.                                                     
           05  COMM-MQ22-ENTREE.                                                
      *        PARAMETRES SSTABLE MQPUT POUR L'ENVOI                            
               10  COMM-MQ22-NSOC            PIC X(03).                         
               10  COMM-MQ22-NLIEU           PIC X(03).                         
               10  COMM-MQ22-FONCTION        PIC X(03).                         
      *        PARAMETRES MQ POUR L'ENVOI                                       
               10  COMM-MQ22-SYNCPOINT       PIC X.                             
               10  COMM-MQ22-PRIORITE        PIC 9.                             
               10  COMM-MQ22-NOMPROG         PIC X(05).                         
               10  COMM-MQ22-MSGID           PIC X(24).                         
               10  COMM-MQ22-CORRELID        PIC X(24).                         
               10  COMM-MQ22-LONGMES         PIC S9(6).                         
      *        IDSI     = NOM DE L'ID SI (CLE RVEC001 UTILISEE)                 
               10  COMM-MQ22-IDSI            PIC X(10).                         
      *        OPCO     = OPCO D'ORIGINE OU CONCERNEE DANS LE MSG               
               10  COMM-MQ22-OPCO            PIC X(03).                         
      *        CLE      = CLE DU MESSAGE ENVOYE EX: '1234567' NCODIC            
               10  COMM-MQ22-CLE             PIC X(45).                         
      *        WENTETE  = FLAG ENVOI DE L'ENTETE DARTY O/N                      
               10  COMM-MQ22-WENTETE         PIC X.                             
                 88  COMM-MQ22-AVEC-ENTETE   VALUE 'O'.                         
                 88  COMM-MQ22-SANS-ENTETE   VALUE 'N'.                         
      *        STOCKAGE = FLAG STOCKAGE DU MESSAGE DANS UNE AUTRE Q             
               10  COMM-MQ22-STOCKAGE-MSG    PIC X.                             
                 88  COMM-MQ22-STOCKAGE      VALUE 'O'.                         
                 88  COMM-MQ22-ENVOI-SIMPLE  VALUE 'N'.                         
      *        PARAMETRE MQPUT POUR LA Q DE STOCKAGE                            
               10  COMM-MQ22-NSOC-STOCK      PIC X(03).                         
               10  COMM-MQ22-NLIEU-STOCK     PIC X(03).                         
               10  COMM-MQ22-FONCTION-STOCK  PIC X(03).                         
      *        ZONES RETOUR DU MMQ22                                            
               10  COMM-MQ22-CODRET          PIC XX.                            
               10  COMM-MQ22-ERREUR          PIC X(52).                         
               10  COMM-MQ22-FILLER-ENTREE   PIC X(12).                         
               10  COMM-MQ22-MESSAGE.                                           
      *                ENTETE STANTARD ENVOYE OU NON          LG=105C           
                   15  COMM-MQ22-ENTETE.                                        
                       20 COMM-MQ22-TYPE     PIC  X(03).                        
                       20 COMM-MQ22-NSOCMSG  PIC  X(03).                        
                       20 COMM-MQ22-NLIEUMSG PIC  X(03).                        
                       20 COMM-MQ22-NSOCDST  PIC  X(03).                        
                       20 COMM-MQ22-NLIEUDST PIC  X(03).                        
                       20 COMM-MQ22-NORD     PIC  9(08).                        
                       20 COMM-MQ22-LPROG    PIC  X(10).                        
                       20 COMM-MQ22-DJOUR    PIC  X(08).                        
                       20 COMM-MQ22-WSID     PIC  X(10).                        
                       20 COMM-MQ22-USER     PIC  X(10).                        
                       20 COMM-MQ22-CHRONO   PIC  9(07).                        
                       20 COMM-MQ22-NBRMSG   PIC  9(07).                        
                       20 COMM-MQ22-NBRENR   PIC  9(05).                        
                       20 COMM-MQ22-TAILLE   PIC  9(05).                        
                       20 COMM-MQ22-VERSION  PIC  X(02).                        
                       20 COMM-MQ22-DSYST    PIC S9(13).                        
                       20 COMM-MQ22-FILLER   PIC  X(05).                        
                   15  COMM-MQ22-MESS        PIC X(49800).                      
                                                                                
