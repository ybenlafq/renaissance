      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2021                  *        
      ******************************************************************        
       01  RVXM2021.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2021-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2021-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2021-NCODIC          PIC X(07).                                 
           10 XM2021-NDEPOT          PIC X(06).                                 
           10 XM2021-CAPPRO          PIC X(05).                                 
           10 XM2021-SENSAPPRO       PIC X(01).                                 
           10 XM2021-CEXPO           PIC X(05).                                 
           10 XM2021-STATCOMP        PIC X(03).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2021-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-NDEPOT-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-NDEPOT-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-CAPPRO-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-CAPPRO-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-SENSAPPRO-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-SENSAPPRO-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-CEXPO-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2021-CEXPO-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2021-STATCOMP-F      PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2021-STATCOMP-F      PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
