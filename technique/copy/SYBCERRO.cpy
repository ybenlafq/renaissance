      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * S E C T I O N - A B A N D O N - T A C H E                               
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       SECTION-ABANDON-TACHE             SECTION.                               
       ABANDON-AIDA.                                                            
           MOVE 'K' TO CODE-ABANDON                                             
           MOVE 'ABANDON-AIDA  :' TO MESS1                                      
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-BATCH.                                                           
           MOVE 'B' TO CODE-ABANDON                                             
           MOVE 'ABANDON-BATCH :' TO MESS1                                      
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-DL1.                                                             
           MOVE 'D' TO CODE-ABANDON                                             
           MOVE 'ABANDON-DL1   :' TO MESS1                                      
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-SQL.                                                             
           MOVE 'S' TO CODE-ABANDON                                             
           MOVE 'ABANDON-SQL   :' TO MESS1                                      
           GO TO SEND-MESSAGE-ABANDON.                                          
       SEND-MESSAGE-ABANDON.                                                    
           DISPLAY TRACE-MESSAGE.                                               
      ******************************************************************        
      *  ALIMENTATION DES ZONES ERREURS                                         
      ******************************************************************        
      *                                                                         
      *    MOVE TRACE-MESS    TO Z-ERREUR-TRACE-MESS.                           
      *                                                                         
      ******************************************************************        
      *  SORTIE                                                                 
      ******************************************************************        
      *{ Tr-Rename-External-Call 1.3                                            
      *    CALL 'ABEND'  USING  ABEND-PROG  ABEND-MESS.                         
      *--                                                                       
           CALL "MW-ABEND"  USING  ABEND-PROG  ABEND-MESS.                      
      *}                                                                        
           GOBACK.                                                              
       FIN-SECTION-ABANDON-TACHE.       EXIT.                                   
           EJECT                                                                
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * F I N - P H Y S I Q U E - P R O G R A M M E                             
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       FIN-PHYSIQUE-PROGRAMME  SECTION.                                         
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-PHYSIQUE-PROGRAMME.      EXIT.                                   
           EJECT                                                                
                                                                                
EMOD                                                                            
