      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   ENVOI MESSAGE MQ HOST --> LOCAL PAR BATCH                             
      *   ZONES PASSES EN PARAMETRE DU CALL BMQ021                              
      *****************************************************************         
      *                                                                         
      *                                                                         
      * CODRET : 1 = CONNECT NON OK                                             
      *          2 = MQOPEN NON OK                                              
      *          3 = MQPUT    //                                                
      *          4 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          5 = SELECT SS-TABLE NCGFC NON TROUVE                           
      *          6 = MQCLOSE NON OK                                             
      *          7 = DISCONNECT NON OK                                          
      *          8 = SQLCODE < 0                                                
      *          9 = ERREUR BETDATC                                             
      *                                                                         
       01  WORK-MQ21-APPLI.                                                     
           05  WORK-MQ21-ENTREE.                                                
               10  WORK-MQ21-NSOC            PIC X(03).                         
               10  WORK-MQ21-NSOCDST         PIC X(03).                         
               10  WORK-MQ21-NLIEU           PIC X(03).                         
               10  WORK-MQ21-NLIEUDST        PIC X(03).                         
               10  WORK-MQ21-QALIAS          PIC X(24).                         
      *  ACTION :                                                               
      *    PLUSIEURS MESSAGES (1 PAR CALL)                                      
      *           1 --> CONNECT, OPEN, ET PUT                                   
      *           P --> PUT                                                     
      *           D --> PUT, CLOSE ET DISCONNECT                                
      *           F --> CLOSE ET DISCONNECT                                     
      *    UN SEUL MESSAGE                                                      
      *           U --> CONNECT, OPEN, PUT, CLOSE ET DISCONNECT                 
               10  WORK-MQ21-ACTION          PIC X.                             
               10  WORK-MQ21-CFONC           PIC X(03).                         
               10  WORK-MQ21-NOMPROG         PIC X(06).                         
               10  WORK-MQ21-MSGID           PIC X(24).                         
               10  WORK-MQ21-CORRELID        PIC X(24).                         
               10  WORK-MQ21-PRIORITY        PIC S9(1).                         
               10  WORK-MQ21-SYNCP           PIC X.                             
               10  WORK-MQ21-NB-MESSAGES     PIC 9(5).                          
               10  WORK-MQ21-NBOCC           PIC 9(5).                          
               10  WORK-MQ21-LONG-ENREG      PIC 9(5).                          
               10  WORK-MQ21-VERSION         PIC X(2).                          
               10  WORK-MQ21-LOG             PIC X(4).                          
               10  WORK-MQ21-LOG-9 REDEFINES WORK-MQ21-LOG   PIC 9(4).          
               10  WORK-MQ21-DISPLAY         PIC X.                             
               10  WORK-MQ21-DSYST           PIC X(13).                         
               10  WORK-MQ21-FILLER          PIC X(50).                         
           05  WORK-MQ21-SORTIE.                                                
               10  WORK-MQ21-CODRET          PIC XX.                            
               10  WORK-MQ21-ERREUR          PIC X(80).                         
           05  WORK-MQ21-MESS.                                                  
               10  WORK-MQ21-MESSAGE         PIC X(50000).                      
                                                                                
