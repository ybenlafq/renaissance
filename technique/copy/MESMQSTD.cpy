      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ STANDARD                                              
      *                                                                         
      *****************************************************************         
       01  WS-MESSAGE.                                                          
         02 MESSAGE-ENTETE.                                                     
           05  MES-TYPE        PIC    X(3).                                     
           05  MES-NSOCMSG     PIC    X(3).                                     
           05  MES-NLIEUMSG    PIC    X(3).                                     
           05  MES-NSOCDST     PIC    X(3).                                     
           05  MES-NLIEUDST    PIC    X(3).                                     
           05  MES-NORD        PIC    9(8).                                     
           05  MES-LPROG       PIC    X(10).                                    
           05  MES-DJOUR       PIC    X(8).                                     
           05  MES-WSID        PIC    X(10).                                    
           05  MES-USER        PIC    X(10).                                    
           05  MES-CHRONO      PIC    9(7).                                     
           05  MES-NBRMSG      PIC    9(7).                                     
           05  MES-NBRENR      PIC    9(5).                                     
           05  MES-TAILLE      PIC    9(5).                                     
           05  MES-VERSION     PIC    X(2).                                     
           05  MES-FILLER.                                                      
             10  MES-DSYST     PIC    X(13).                                    
             10  MES-FILLER2   PIC    X(05).                                    
         02 MESSAGE-DATA       PIC    X(50000).                                 
                                                                                
