      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00320001
      * MODULE DE VERIFICATION VALIDITE CARTE SUR 9 CHIFFRES           *00330001
      * (ALGORITHME DE LUHN OU "MOD 10")      JC0806                   *00330101
      *----------------------------------------------------------------*00331001
      *                                                                 00400000
       CALCUL-NOUVEAU-CODMOD    SECTION.                                00410000
      *                                                                 00420000
           PERFORM CONTROLE-MODULO .                                    00450000
      *                                                                 00460000
       F-CALCUL-NOUVEAU-CODMOD.         EXIT.                           00470000
           EJECT                                                        00471000
      *                                                                 00480000
       CONTROLE-MODULO     SECTION.                                     00490000
      *                                                                 00500000
           SET CODMOD-KO       TO TRUE                                  00501008
           SET MODULO-KO       TO TRUE                                  00510008
           MOVE 0              TO W-CALCUL W-RESULTAT .                 00540023
      *                                                                 00541001
           PERFORM  VARYING IND FROM 1 BY 1 UNTIL IND > 9               00550009
             MOVE 0 TO W-CODE1                                          00550107
             MOVE 0 TO W-RESTE                                          00550207
             COMPUTE IND2 = 10 - IND                                    00550309
             MOVE W-NCODMOD-99 (IND2:1)   TO W-CODE1                    00550409
             DIVIDE IND2 BY 2 GIVING W-CALCUL                           00550625
                             REMAINDER W-RESTE                          00550725
             IF W-RESTE = 0                                             00551006
                COMPUTE W-CODE1 = W-CODE1 * 2                           00551107
                IF W-CODE1 > 9                                          00551206
                  COMPUTE W-CODE1 = W-CODE1B(1) + W-CODE1B(2)           00551420
                END-IF                                                  00551606
             END-IF                                                     00552005
             ADD W-CODE1 TO W-RESULTAT                                  00553013
           END-PERFORM                                                  00580001
      *                                                                 00581001
           MOVE 0 TO W-RESTE                                            00582008
           DIVIDE W-RESULTAT BY 10 GIVING W-CALCUL REMAINDER W-RESTE    00590016
      *                                                                 00600008
           IF W-RESTE = 0                                               00610008
              SET MODULO-OK TO TRUE                                     00611008
           END-IF.                                                      00640000
      *                                                                 00650000
       F-CONTROLE-MODULO.  EXIT.                                        00690000
               EJECT                                                    00691000
                                                                                
                                                                        00700000
