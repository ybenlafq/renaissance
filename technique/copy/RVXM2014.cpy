      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2014                  *        
      ******************************************************************        
       01  RVXM2014.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2014-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2014-REFID PIC X(40).                                           
      *}                                                                        
      *    10 XM2014-REFID.                                                     
      *       49  XM2014-REFID-LEN   PIC S9(4) USAGE COMP.                      
      *       49  XM2014-REFID-TEXT  PIC X(40).                                 
           10 XM2014-CONTEXT         PIC X(50).                                 
           10 XM2014-NCODICK         PIC X(07).                                 
           10 XM2014-NCODIC          PIC X(07).                                 
           10 XM2014-CMARQ           PIC X(05).                                 
           10 XM2014-CCOLOR          PIC X(05).                                 
           10 XM2014-CLIGPROD        PIC X(05).                                 
           10 XM2014-CNOMDOU         PIC X(08).                                 
           10 XM2014-CORIGPROD       PIC X(05).                                 
           10 XM2014-LEMBALLAGE      PIC X(50).                                 
           10 XM2014-LMODBASE        PIC X(20).                                 
           10 XM2014-NSOC            PIC X(03).                                 
           10 XM2014-QBOXCART        PIC S9(4)      COMP-3.                     
           10 XM2014-QCARTCOUCH      PIC S9(2)      COMP-3.                     
           10 XM2014-QCOLIRECEPT     PIC S9(5)      COMP-3.                     
           10 XM2014-QCOUCHPAL       PIC S9(2)      COMP-3.                     
           10 XM2014-QGRATUITE       PIC S9(5)      COMP-3.                     
           10 XM2014-QHAUTDE         PIC S9(4)V9(1) COMP-3.                     
           10 XM2014-QHAUTEUR        PIC S9(3)      COMP-3.                     
           10 XM2014-QLARGDE         PIC S9(4)V9(1) COMP-3.                     
           10 XM2014-QLARGEUR        PIC S9(3)      COMP-3.                     
           10 XM2014-QNBPRACK        PIC S9(5)      COMP-3.                     
           10 XM2014-QPOIDS          PIC S9(7)      COMP-3.                     
           10 XM2014-QPOIDSDE        PIC S9(7)      COMP-3.                     
           10 XM2014-QPRODBOX        PIC S9(4)      COMP-3.                     
           10 XM2014-QPRODCAM        PIC S9(5)      COMP-3.                     
E0713      10 XM2014-QNBCOL          PIC S9(5)      COMP-3.                     
           10 XM2014-QPROFDE         PIC S9(4)V9(1) COMP-3.                     
           10 XM2014-QPROFONDEUR     PIC S9(3)      COMP-3.                     
           10 XM2014-VERSION         PIC X(20).                                 
           10 XM2014-LREFFOURN       PIC X(20).                                 
           10 XM2014-CASSORT         PIC X(05).                                 
           10 XM2014-CFAM            PIC X(05).                                 
           10 XM2014-CGARANTIE       PIC X(05).                                 
           10 XM2014-CGARCONST       PIC X(05).                                 
E0713      10 XM2014-CGARANMGD       PIC X(05).                                 
           10 XM2014-CHEFPROD        PIC X(05).                                 
           10 XM2014-CTAUXTVA        PIC X(05).                                 
           10 XM2014-DEFFET          PIC X(08).                                 
           10 XM2014-LREFDARTY       PIC X(20).                                 
           10 XM2014-LSTATUT         PIC X(10).                                 
           10 XM2014-PCOMMREF        PIC S9(5)V9(2) COMP-3.                     
           10 XM2014-PREFTTC         PIC S9(5)V9(2) COMP-3.                     
           10 XM2014-WSENSVTE        PIC X(01).                                 
           10 XM2014-D1RECEPT        PIC X(08).                                 
           10 XM2014-REPLENGRP       PIC X(05).                                 
           10 XM2014-CLIRQ           PIC X(01).                                 
           10 XM2014-QTEMT           PIC X(01).                                 
           10 XM2014-REMIS           PIC X(01).                                 
           10 XM2014-IMPRI           PIC X(01).                                 
           10 XM2014-QCONTENU        PIC S9(5)      COMP-3.                     
           10 XM2014-LREFO           PIC X(20).                                 
           10 XM2014-QECO            PIC S9(5)      COMP-3.                     
           10 XM2014-DANGER          PIC X(06).                                 
           10 XM2014-PTC             PIC S9(07)V9(6) COMP-3.                    
           10 XM2014-SOURNAT         PIC X(05).                                 
           10 XM2014-COLLECTION      PIC X(05).                                 
           10 XM2014-ALIMENTAIRE     PIC X(01).                                 
           10 XM2014-LIQUIDE         PIC X(01).                                 
           10 XM2014-PEREMPT         PIC X(01).                                 
           10 XM2014-DELAIACCEPT     PIC S9(03)     COMP-3.                     
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2014-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CONTEXT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CONTEXT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-NCODICK-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-NCODICK-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CMARQ-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CMARQ-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CCOLOR-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CCOLOR-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CLIGPROD-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CLIGPROD-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CNOMDOU-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CNOMDOU-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CORIGPROD-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CORIGPROD-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LEMBALLAGE-F    PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LEMBALLAGE-F    PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LMODBASE-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LMODBASE-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-NSOC-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-NSOC-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QBOXCART-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QBOXCART-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QCARTCOUCH-F    PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QCARTCOUCH-F    PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QCOLIRECEPT-F   PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QCOLIRECEPT-F   PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QCOUCHPAL-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QCOUCHPAL-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QGRATUITE-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QGRATUITE-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QHAUTDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QHAUTDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QHAUTEUR-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QHAUTEUR-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QLARGDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QLARGDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QLARGEUR-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QLARGEUR-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QNBPRACK-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QNBPRACK-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPOIDS-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPOIDS-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPOIDSDE-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPOIDSDE-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPRODBOX-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPRODBOX-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPRODCAM-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPRODCAM-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0713 *    10 XM2014-QNBCOL-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QNBCOL-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPROFDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPROFDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QPROFONDEUR-F   PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QPROFONDEUR-F   PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-VERSION-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-VERSION-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LREFFOURN-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LREFFOURN-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CASSORT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CASSORT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CFAM-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CFAM-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CGARANTIE-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CGARANTIE-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CGARCONST-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CGARCONST-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0713 *    10 XM2014-CGARANMGD-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CGARANMGD-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CHEFPROD-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CHEFPROD-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CTAUXTVA-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CTAUXTVA-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-DEFFET-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-DEFFET-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LREFDARTY-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LREFDARTY-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LSTATUT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LSTATUT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-PCOMMREF-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-PCOMMREF-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-PREFTTC-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-PREFTTC-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-WSENSVTE-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-WSENSVTE-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-D1RECEPT-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-D1RECEPT-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-REPLENGRP-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-REPLENGRP-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-CLIRQ-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-CLIRQ-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QTEMT-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QTEMT-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-REMIS-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-REMIS-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-IMPRI-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-IMPRI-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QCONTENU-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QCONTENU-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LREFO-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LREFO-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-QECO-F          PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-QECO-F          PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-DANGER-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-DANGER-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-PTC-F           PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-PTC-F           PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-SOURNAT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-SOURNAT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-COLLECTION-F    PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-COLLECTION-F    PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-ALIMENTAIRE-F   PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-ALIMENTAIRE-F   PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-LIQUIDE-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-LIQUIDE-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-PEREMPT-F       PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2014-PEREMPT-F       PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2014-DELAIACCEPT-F   PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2014-DELAIACCEPT-F   PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
