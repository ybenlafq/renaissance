      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  MODULE GENERALISE DE READ SUPERIEUR OU EGAL A LA CLE           00000200
      ****************************************************************  00000300
      *                                                                 00000400
       READ-SUP             SECTION.                                    00000500
      *                                                                 00000600
      *    MOVE LONG-Z-INOUT TO FILE-LONG.                              00000700
      *                                                                 00000800
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READ     DATASET (FILE-NAME)                                   
                          RIDFLD  (VSAM-KEY)                                    
                          INTO    (Z-INOUT)                                     
                          LENGTH  (FILE-LONG)                                   
                          GTEQ                                                  
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�0��00010   ' TO DFHEIV0                              
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME Z-INOUT FILE-LO        
      *dfhei*     VSAM-KEY.                                                     
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001800
           MOVE EIBRCODE TO EIB-RCODE.                                  00001900
           IF   EIB-NORMAL                                              00002000
                MOVE 0 TO CODE-RETOUR                                   00002100
           ELSE                                                         00002200
                IF   EIB-NOTFND                                         00002300
                     MOVE 1 TO CODE-RETOUR                              00002400
                ELSE                                                    00002500
                     GO TO ABANDON-CICS                                 00002600
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002700
       FIN-READ-SUP.  EXIT.                                             00002800
                    EJECT                                               00003000
                                                                                
                                                                                
