      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ** AIDA *********************************************************         
      * PASSAGE DU CONTROLE AU PROGRAMME DE HELP GENERALISE AIDA      *         
      ****************************************************** SYKCHELP *         
      *                                                                         
       SORTIE-HELP                SECTION.                                      
      *                                                                         
           MOVE NOM-MAP          TO Z-COMMAREA-NOM-MAP.                         
           MOVE NOM-MAPSET       TO Z-COMMAREA-NOM-MAPSET.                      
           MOVE NOM-TACHE        TO Z-COMMAREA-NOM-TACHE.                       
           MOVE LENGTH OF Z-MAP  TO Z-COMMAREA-LONG-MAP.                        
           MOVE LONG-COMMAREA    TO Z-COMMAREA-LONG.                            
      *                                                                         
           MOVE Z-COMMAREA       TO Z-INOUT-COMMAREA.                           
           MOVE Z-MAP            TO Z-INOUT-MAP.                                
           MOVE 'HELP'           TO AIDA-IDENT-TS-A.                            
           MOVE EIBTRMID         TO AIDA-IDENT-TS-B.                            
           MOVE 1                TO AIDA-RANG-TS.                               
      *                                                                         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS DELETEQ  QUEUE    (AIDA-IDENT-TS)                              
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����00019   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS.                   
      *dfhei*} decommente EXEC                                                  
      *                                                                         
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS QUEUE    (AIDA-IDENT-TS)                             
                           FROM     (Z-INOUT)                                   
                           LENGTH   (AIDA-LONG-TS)                              
                           ITEM     (AIDA-RANG-TS)                              
                           NOHANDLE                                             
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00023   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS Z-INOUT            
      *dfhei*     AIDA-LONG-TS DFHDUMMY AIDA-RANG-TS.                           
      *dfhei*} decommente EXEC                                                  
      *                                                                         
           MOVE EIBRCODE      TO EIB-RCODE.                                     
      *                                                                         
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
      *                                                                         
           MOVE 'PGMHELP' TO NOM-PROG-XCTL.                                     
      *                                                                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-HELP. EXIT.                                                   
       EJECT                                                                    
                                                                                
                                                                                
