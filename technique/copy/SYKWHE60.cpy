      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT :         *               
      *        "JHE601".                                        *               
      *    LONGUEUR : 250                                       *               
      *    RIDFLD   : 027                                       *               
      *---------------------------------------------------------*               
       01  RBA-L10.                                                             
           05  VSAM-CLE.                                                 000008 
               10  VSAM-ETAT             PIC X(06).                      000008 
               10  VSAM-DATE             PIC X(08).                      000008 
               10  VSAM-USER             PIC X(08).                      000008 
               10  VSAM-NSEQ             PIC 9(05).                      000008 
           05  VSAM-CLE-R                REDEFINES  VSAM-CLE.                   
               10  VSAM-CLE60            PIC X(14).                             
               10  FILLER                PIC X(13).                             
           05  VSAM-DONNEES.                                             000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
      *        10  RBA-DATEDI            PIC X(08).                      000008 
               10  RBA-NUMSEQL           PIC X(08).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-CENTRE            PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-NDEM              PIC 9(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-CTIERS            PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-FAM               PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-CODIC             PIC X(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-MARQ              PIC X(05).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-REF               PIC X(19).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-NDEP              PIC X(03).                      000008 
               10  RBA-NHS               PIC X(07).                      000008 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-NTRAI             PIC X(05).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-DVENTE.                                                  
                   15  RBA-DVEN-SSAA.                                           
                       20  RBA-DVEN-SS   PIC X(02).                             
                       20  RBA-DVEN-AA   PIC X(02).                             
                   15  RBA-DVEN-MOIS     PIC X(02).                             
                   15  RBA-DVEN-JOUR     PIC X(02).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-DDEP.                                                    
                   15  RBA-DDEP-SSAA.                                           
                       20  RBA-DDEP-SS   PIC X(02).                             
                       20  RBA-DDEP-AA   PIC X(02).                             
                   15  RBA-DDEP-MOIS     PIC X(02).                             
                   15  RBA-DDEP-JOUR     PIC X(02).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-SERIE             PIC X(16).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-NACCORD           PIC X(12).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-LPAN              PIC X(20).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-PVTTC             PIC -9(7)V99.                          
               10  RBA-PVTTC-R           REDEFINES  RBA-PVTTC                   
                                         PIC X(10).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-ORI               PIC X(03).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-PRMP              PIC -9(7)V9(6).                        
               10  RBA-PRMP-R            REDEFINES  RBA-PRMP                    
                                         PIC X(14).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-ECOP              PIC -9(5)V99.                          
               10  RBA-ECOP-R            REDEFINES  RBA-ECOP                    
                                         PIC X(08).                             
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-PDOSNASC          PIC X(03).                      000019 
               10  FILLER                PIC X(01)  VALUE '/'.           000019 
               10  RBA-DOSNASC           PIC X(09).                      000019 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
               10  RBA-NEAN              PIC X(13).                      000019 
               10  FILLER                PIC X(01)  VALUE ';'.           000019 
      *    05  FILLER-BLANC              PIC X(28)  VALUE ' '.           000019 
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
