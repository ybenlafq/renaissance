      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TRAVAIL DU MODULE DE TRAITEMENT DES VALEURS NUMERIQUES         
      *****************************************************************         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-VALN ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-VALN-USER             PIC X(18) VALUE SPACE.                   
           05  FILLER                  REDEFINES W-VALN-USER.                   
             10  FILLER                OCCURS 18.                               
               15  W-VALN-USER-CAR     PIC X.                                   
      *                                                                         
           05  W-VALN-FILE             PIC 9(18) VALUE 0.                       
           05  FILLER                  REDEFINES W-VALN-FILE.                   
             10  W-VALN-FILE-X         PIC X(18).                               
           05  FILLER                  REDEFINES W-VALN-FILE.                   
             10  FILLER                OCCURS 18.                               
               15  W-VALN-FILE-CAR     PIC X.                                   
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-VALN-I                PIC S9(3) COMP-3 VALUE +0.               
           05  W-VALN-J                PIC S9(3) COMP-3 VALUE +0.               
      *                                                                         
       EJECT                                                                    
                                                                                
                                                                                
