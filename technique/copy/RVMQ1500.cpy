      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVMQ1500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMQ1500                         
      *   CONTROLES DES OPERATIONS MQ                                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVMQ1500.                                                            
           02  MQ15-CPROGRAMME                                                  
               PIC X(0006).                                                     
           02  MQ15-CFONCTION                                                   
               PIC X(0003).                                                     
           02  MQ15-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  MQ15-NLIEU                                                       
               PIC X(0003).                                                     
           02  MQ15-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  MQ15-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  MQ15-DTRAITEMENT                                                 
               PIC X(0008).                                                     
           02  MQ15-DOPERATION                                                  
               PIC X(0008).                                                     
           02  MQ15-COMMENT                                                     
               PIC X(0030).                                                     
           02  MQ15-CETAT                                                       
               PIC X(0001).                                                     
           02  MQ15-NBENREGS                                                    
               PIC S9(7) COMP-3.                                                
           02  MQ15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVMQ1500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVMQ1502-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-CPROGRAMME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-CPROGRAMME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-CFONCTION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-CFONCTION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-DTRAITEMENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-DTRAITEMENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-DOPERATION-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-DOPERATION-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-COMMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-COMMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-NBENREGS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-NBENREGS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MQ15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MQ15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
