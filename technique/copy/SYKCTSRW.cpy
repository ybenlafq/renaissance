      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      * REECRITURE   DE LA TEMPORARY STORAGE                            00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       REWRITE-TS                 SECTION.                              00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS                                                      
                          QUEUE    (IDENT-TS)                                   
                          FROM     (Z-INOUT)                                    
                          LENGTH   (LONG-TS)                                    
                          ITEM     (RANG-TS)                                    
                          REWRITE                                               
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-INOUT LONG-TS         
      *dfhei*     DFHDUMMY RANG-TS.                                             
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001700
           MOVE EIBRCODE TO EIB-RCODE.                                  00001800
           IF   NOT EIB-NORMAL                                          00001900
                GO TO ABANDON-CICS                                      00002000
           END-IF.                                                              
      *                                                                 00002100
       FIN-REWRITE-TS. EXIT.                                            00002200
                    EJECT                                               00002400
                                                                                
                                                                                
