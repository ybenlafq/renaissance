      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      * RETRIEVE DES DATA EN PROVENANCE D'UN START                      00000200
      ******************************************************************00000300
      *                                                                 00000400
       RETRIEVE-DATA             SECTION.                               00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS RETRIEVE INTO   (Z-START)                                      
                          LENGTH (LONG-START)                                   
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��b00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  Z-START LONG-START.              
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001300
           MOVE EIBRCODE TO EIB-RCODE.                                  00001400
           IF   EIB-NORMAL                                              00001500
                MOVE 0 TO CODE-RETOUR                                   00001600
           ELSE                                                         00001700
                IF   EIB-ENDDATA                                        00001800
                     MOVE 1 TO CODE-RETOUR                              00001900
                ELSE                                                    00002000
                     GO TO ABANDON-CICS                                 00002100
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002200
       FIN-RETRIEVE-DATA. EXIT.                                         00002300
           EJECT                                                        00002500
                                                                                
                                                                                
