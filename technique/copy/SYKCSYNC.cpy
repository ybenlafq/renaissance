      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      * SYNCHRO-POINT                                                   00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       SYNCHRO-POINT SECTION.                                           00000500
      *                                                                 00000600
           MOVE '*' TO ETAT-PSB.                                        00000700
      *                                                                 00000800
       EXEC CICS SYNCPOINT                                                      
       END-EXEC.                                                                
      *    MOVE '00010   ' TO DFHEIV0                                       
      *    CALL 'DFHEI1' USING DFHEIV0.                                         
      *                                                                 00001200
           MOVE EIBRCODE TO EIB-RCODE.                                  00001300
           IF   NOT EIB-NORMAL                                          00001400
                GO TO ABANDON-CICS                                      00001500
           END-IF.                                                              
      *                                                                 00001600
       FIN-SYNCHRO-POINT. EXIT.                                         00001700
           EJECT                                                        00001900
                                                                                
                                                                                
