      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2049                            *        
      ******************************************************************        
       01  RVXM2049.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2049-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2049-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2049-CDESCRIPTIF.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2049-CDESCRIPTIF-LEN   PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2049-CDESCRIPTIF-LEN   PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2049-CDESCRIPTIF-TEXT  PIC X(05).                            
           10 XM2049-CVDESCRIPT.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2049-CVDESCRIPT-LEN    PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2049-CVDESCRIPT-LEN    PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2049-CVDESCRIPT-TEXT   PIC X(05).                            
           10 XM2049-LVDESCRIPT.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2049-LVDESCRIPT-LEN    PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2049-LVDESCRIPT-LEN    PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2049-LVDESCRIPT-TEXT   PIC X(20).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2049                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2049-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2049-CDESCRIPTIF-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2049-CDESCRIPTIF-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2049-CVDESCRIPT-F        PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2049-CVDESCRIPT-F        PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2049-LVDESCRIPT-F        PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2049-LVDESCRIPT-F        PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 04      *        
      ******************************************************************        
                                                                                
