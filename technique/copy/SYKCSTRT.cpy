      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  PASSAGE DU CONTROL A UNE AUTRE TACHE                           00000200
      ****************************************************************  00000300
      *                                                                 00000400
       START-TACHE         SECTION.                                     00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS START                                                          
                          TRANSID  (NOM-TACHE-START)                            
                          TERMID   (TERM-START)                                 
                          FROM     (Z-START)                                    
                          LENGTH   (LONG-START)                                 
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��{00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DFHEID0 DFHDUMMY NOM-TACH        
      *dfhei*      Z-START LONG-START TERM-START.                               
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001700
           MOVE EIBRCODE TO EIB-RCODE.                                  00001800
           IF   NOT EIB-NORMAL                                          00001900
                GO TO ABANDON-CICS                                      00002000
           END-IF.                                                              
      *                                                                 00002100
       FIN-START-TACHE. EXIT.                                           00002200
           EJECT                                                                
                                                                                
                                                                                
