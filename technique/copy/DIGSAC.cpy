      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *==> DBD-DL1 ==> SEGMENT ************************ AIDA-COBOL *            
      *    DIGDP0      DIGSA   RACINE ETAT                         *            
      ************************************************ COPY DIGSAC *            
      *                                                                         
      * DESCRIPTION VARIABLE E/S -------------------------------------          
      *                                                                         
       01  DIGSA PIC X(128).                                                    
      *                                                                         
      *-------------------------------------------------------------            
      * SEGMENT RACINE IDENTIFICATION ETAT CICS                    -            
      *-------------------------------------------------------------            
      *                                                                         
       01  DIGSA-IDT REDEFINES DIGSA.                                           
      *                                                                         
      *----CLE ACCES ETAT - IDENTIFICATION                                      
      *          NOM ETAT      JAAXXN                                           
      *          DATE EDITION  ******                                           
      *          DESTINATION   *********                                        
      *          DOCUMENT      ***************                                  
           05 DIGFA0-IDT-ETA.                                                   
              10 DIGFA1-IDT-NOM PIC  X(006).                                    
              10 DIGFA2-IDT-DAT PIC  X(006).                                    
              10 DIGFA3-IDT-DST PIC  X(009).                                    
              10 DIGFA4-IDT-DOC PIC  X(015).                                    
      *                                                                         
      *----CARACTERISTIQUES ETAT                                                
      *       TYPE DE RACINE IDENTIFICATION = '0'                               
      *       NUMERO DE CONFIGURATION 1 A 8                                     
           05 DIGZA0-IDT-RCN    PIC  X(001).                                    
           05 DIGZA0-IDT-CNF    PIC  X(001).                                    
      *                                                                         
      *----INTITULES                                                            
      *       TITRE ETAT                                                        
      *       SORTE IMPRIME                                                     
           05 DIGZA0-IDT-TIT    PIC  X(040).                                    
           05 DIGZA0-IDT-PAP    PIC  X(020).                                    
      *                                                                         
      *----FORMAT IMPRESSION ETAT                                               
      *       TYPE IMPRESSION - U = UNIQUE - M = MULTIPLE                       
      *       NOMBRE DE LIGNES DE LA PAGE  - MODE TEXTE                         
      *       NOMBRE DE LIGNES DE L'ETAT   - MODE TEXTE                         
      *       NOMBRE CE COLONNES PAR LIGNE - MODE TEXTE                         
      *       ATTACHEMENT TERMINAL : S=STANDARD P=PARTICULIER                   
           05 DIGZA0-IDT-IMP    PIC  X(001).                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZA0-IDT-HTR    PIC  9(004) COMP.                               
      *--                                                                       
           05 DIGZA0-IDT-HTR    PIC  9(004) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZA0-IDT-LNG    PIC  9(004) COMP.                               
      *--                                                                       
           05 DIGZA0-IDT-LNG    PIC  9(004) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZA0-IDT-COL    PIC  9(004) COMP.                               
      *--                                                                       
           05 DIGZA0-IDT-COL    PIC  9(004) COMP-5.                             
      *}                                                                        
           05 DIGZA0-IDT-ATT    PIC  X(001).                                    
      *                                                                         
      *----PROTOCOLE IMPRESSION                                                 
      *       TYPE PROTOCOLE  - T OU ' ' = TEXTE - L = LASER                    
           05 DIGZA0-IDT-PTC    PIC  X(001).                                    
      *                                                                         
      *----CARACTERISTIQUES PROTOCOLE LASER                                     
      *       LONGUEUR ZONE DE COMMANDES                                        
      *       LONGUEUR ZONE DE DONNEES                                          
      *       FORMAT                                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZA0-IDT-CMD    PIC  9(004) COMP.                               
      *--                                                                       
           05 DIGZA0-IDT-CMD    PIC  9(004) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 DIGZA0-IDT-DON    PIC  9(004) COMP.                               
      *--                                                                       
           05 DIGZA0-IDT-DON    PIC  9(004) COMP-5.                             
      *}                                                                        
           05 DIGZA0-IDT-FOR    PIC  X(002).                                    
      *                                                                         
      *----ZONE NON UTILISEE                                                    
           05 DIGZA0-IDT-FIL    PIC  X(015).                                    
       EJECT                                                                    
      *-------------------------------------------------------------            
      * SEGMENT RACINE CONFIGURATION ETAT CICS                     -            
      *-------------------------------------------------------------            
      *                                                                         
       01  DIGSA-CFG REDEFINES DIGSA.                                           
      *                                                                         
      *----CLE ACCES ETAT - CONFIGURATION                                       
      *          NOM ETAT      JAAXXN                                           
      *          DATE EDITION                                                   
      *          DESTINATION                                                    
      *          DOCUMENT                                                       
           05 DIGFA0-CFG-ETA.                                                   
              10 DIGFA1-CFG-NOM PIC  X(006).                                    
              10 DIGFA2-CFG-DAT PIC  X(006).                                    
              10 DIGFA3-CFG-DST PIC  X(009).                                    
              10 DIGFA4-CFG-DOC PIC  X(015).                                    
      *                                                                         
      *----CARACTERISTIQUES ETAT                                                
      *       TYPE DE RACINE CONFIGURATION  = '1'                               
      *       NUMERO DE CONFIGURATION 1 A 8                                     
           05 DIGZA1-CFG-RCN    PIC  X(001).                                    
           05 DIGZA1-CFG-CNF    PIC  X(001).                                    
      *                                                                         
      *----STATUT ETAT PAR DATE AAMMJJ                                          
      *       DATE DERNIERE EDITION                                             
      *       DATE TOPE SUPPRESSION                                             
      *       DATE IMPRESSION                                                   
      *       DATE REPRISE IMPRESSION                                           
      *       DATE CONSULTATION                                                 
      *       DATE RECUPERATION PURGE                                           
           05 DIGZA1-CFG-EDI    PIC  9(006).                                    
           05 DIGZA1-CFG-SUP    PIC  9(006).                                    
           05 DIGZA1-CFG-IMP    PIC  9(006).                                    
           05 DIGZA1-CFG-REP    PIC  9(006).                                    
           05 DIGZA1-CFG-CNT    PIC  9(006).                                    
           05 DIGZA1-CFG-PUR    PIC  9(006).                                    
      *                                                                         
      *----PROTOCOLE IMPRESSION                                                 
      *       TYPE PROTOCOLE  - T OU ' ' = TEXTE - L = LASER                    
           05 DIGZA1-CFG-PTC    PIC  X(001).                                    
      *                                                                         
      *----ZONE NON UTILISEE                                                    
           05 DIGZA1-CFG-FIL    PIC  X(053).                                    
      *-----------------------------------------------------------*             
      * ==> ATTENTION <==                                         *             
      * CETTE DESCRIPTION COBOL-DIGSAC A UNE CORRESPONDANCE EN    *             
      * PL1-DIGSA .                                               *             
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .              *             
      *-----------------------------------------------------------*             
       EJECT                                                                    
                                                                                
