      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE XM1000                       
      ******************************************************************        
      *                                                                         
       CLEF-XM1000             SECTION.                                         
      *                                                                         
           MOVE 'RVXM1000          '       TO   TABLE-NAME.                     
           MOVE 'XM1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-XM1000. EXIT.                                                   
                EJECT                                                           
                                                                                
