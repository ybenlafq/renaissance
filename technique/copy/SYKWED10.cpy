      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DL/1                    
      *****************************************************************         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  TRACE-DL1-MESSAGE.                                               
      *                                                                         
             10  FILLER         PIC  X(4)   VALUE 'PSB='.                       
             10  ETAT-PSB       PIC  X      VALUE '*'.                          
               88  PSB-NOT-OK               VALUE '*'.                          
               88  PSB-OK                   VALUE 'K'.                          
      *                                                                         
             10  FILLER         PIC  X(6)   VALUE ' FUNC='.                     
      *                                                                         
             10  FONCTIONX      PIC  X(4)   VALUE SPACES.                       
               88  GU                       VALUE 'GU  '.                       
               88  GN                       VALUE 'GN  '.                       
               88  GNP                      VALUE 'GNP '.                       
               88  ISRT                     VALUE 'ISRT'.                       
               88  REPL                     VALUE 'REPL'.                       
               88  DLET                     VALUE 'DLET'.                       
               88  PCB                      VALUE 'PCB '.                       
      *                                                                         
             10  FILLER         PIC  X(5)   VALUE ' PCB='.                      
             10  DL1-NUM-PCB    PIC  9      VALUE 0.                            
      *                                                                         
             10  FILLER         PIC  X(6)   VALUE ' RETC='.                     
             10  TRACE-DL1-RC   PIC  X(2)   VALUE '**'.                         
      *                                                                         
             10  FILLER           PIC  X(4)   VALUE ' SSA'.                     
             10  TRACE-DL1-NOMBRE PIC  9      VALUE  0.                         
             10  FILLER           PIC  X      VALUE '='.                        
             10  TRACE-DL1-SSA    PIC  X(44)  VALUE ALL '*'.                    
             10  FILLER REDEFINES TRACE-DL1-SSA.                                
                 15 TRACE-DL1-DECOUPE PIC X OCCURS 1.                           
      *                                                                         
           05  TRACE-DL1-WORKAREA.                                              
      *                                                                         
             10  TRACE-DL1-I    PIC  S9(5) COMP-3 VALUE +0.                     
      *                                                                         
      ***************************************************************           
      *                                                                         
           05  FILLER-CADRAGE   PIC  X(2)  VALUE SPACE.                         
      *                                                                         
      ***************************************************************           
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FILLER         PIC  X(8)   VALUE 'DL1-PSB.'.                   
             10  NOM-PSB        PIC  X(8)   VALUE SPACE.                        
      *                                                                         
             10  FILLER      PIC  X(12)      VALUE 'DL1-NOMBRE..'.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  NOMBRE      PIC  S9(8) COMP VALUE +0.                          
      *--                                                                       
             10  NOMBRE      PIC  S9(8) COMP-5 VALUE +0.                        
      *}                                                                        
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FILLER      PIC  X(15)  VALUE 'DL1-FCTR.......'.               
             10  DL1-FCTR    PIC  X      VALUE LOW-VALUE.                       
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FILLER      PIC  X(15)  VALUE 'DL1-DLTR.......'.               
             10  DL1-DLTR    PIC  X      VALUE LOW-VALUE.                       
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FILLER      PIC  X(15)  VALUE 'DL1-LEVEL......'.               
             10  CODE-LEVEL  PIC  X      VALUE '0'.                             
               88 HIGH-LEVEL             VALUE '1'.                             
               88 SAME-LEVEL             VALUE '2'.                             
      *                                                                         
           05  FILLER        PIC  X(16)  VALUE 'DL1-PCB-ENCOURS.'.              
           05  SAVE-PCB.                                                        
             10 DBD-NAME             PIC X(8).                                  
             10 SEG-LEVEL            PIC X(2).                                  
             10 PCB-STATUS-CODE      PIC X(2).                                  
               88 BB                 VALUE '  '.                                
               88 GA                 VALUE 'GA'.                                
               88 GB                 VALUE 'GB'.                                
               88 GE                 VALUE 'GE'.                                
               88 GK                 VALUE 'GK'.                                
               88 II                 VALUE 'II'.                                
               88 NI                 VALUE 'NI'.                                
             10 PROC-OPTIONS         PIC X(4).                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 FILLER               PIC S9(5) COMP.                            
      *--                                                                       
             10 FILLER               PIC S9(5) COMP-5.                          
      *}                                                                        
             10 SEG-NAME-FB          PIC X(8).                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 LENGTH-FB-KEY        PIC S9(5) COMP.                            
      *--                                                                       
             10 LENGTH-FB-KEY        PIC S9(5) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 NUMB-SENS-SEGS       PIC S9(5) COMP.                            
      *--                                                                       
             10 NUMB-SENS-SEGS       PIC S9(5) COMP-5.                          
      *}                                                                        
             10 KEY-FB-AREA          PIC X(60).                                 
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FUNC-GU        PIC  X(4)   VALUE 'GU  '.                       
             10  FUNC-GN        PIC  X(4)   VALUE 'GN  '.                       
             10  FUNC-GNP       PIC  X(4)   VALUE 'GNP '.                       
             10  FUNC-DLET      PIC  X(4)   VALUE 'DLET'.                       
             10  FUNC-REPL      PIC  X(4)   VALUE 'REPL'.                       
             10  FUNC-ISRT      PIC  X(4)   VALUE 'ISRT'.                       
             10  FUNC-PCB       PIC  X(4)   VALUE 'PCB '.                       
             10  FUNC-TERM      PIC  X(4)   VALUE 'TERM'.                       
             10  FILLER         PIC  X(12)  VALUE SPACE.                        
      ***************************************************************           
       01    FILLER-EXEC.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  NUM-PCB        PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  NUM-PCB        PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-1     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-1     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-2     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-2     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-3     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-3     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-4     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-4     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-5     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-5     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-6     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-6     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-7     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-7     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-8     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-8     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  SEG-LONG-9     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  SEG-LONG-9     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-1     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-1     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-2     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-2     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-3     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-3     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-4     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-4     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-5     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-5     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-6     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-6     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-7     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-7     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-8     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-8     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  KEY-LONG-9     PIC S9(4)   COMP VALUE +0.                      
      *--                                                                       
             05  KEY-LONG-9     PIC S9(4) COMP-5 VALUE +0.                      
      *}                                                                        
             05  KEY-VAL-1      PIC X(30).                                      
             05  KEY-VAL-2      PIC X(30).                                      
             05  KEY-VAL-3      PIC X(30).                                      
             05  KEY-VAL-4      PIC X(30).                                      
             05  KEY-VAL-5      PIC X(30).                                      
             05  KEY-VAL-6      PIC X(30).                                      
             05  KEY-VAL-7      PIC X(30).                                      
             05  KEY-VAL-8      PIC X(30).                                      
             05  KEY-VAL-9      PIC X(30).                                      
             05  SEG-NAME-1     PIC X(8).                                       
             05  SEG-NAME-2     PIC X(8).                                       
             05  SEG-NAME-3     PIC X(8).                                       
             05  SEG-NAME-4     PIC X(8).                                       
             05  SEG-NAME-5     PIC X(8).                                       
             05  SEG-NAME-6     PIC X(8).                                       
             05  SEG-NAME-7     PIC X(8).                                       
             05  SEG-NAME-8     PIC X(8).                                       
             05  SEG-NAME-9     PIC X(8).                                       
             05  KEY-NAME-1     PIC X(8).                                       
             05  KEY-NAME-2     PIC X(8).                                       
             05  KEY-NAME-3     PIC X(8).                                       
             05  KEY-NAME-4     PIC X(8).                                       
             05  KEY-NAME-5     PIC X(8).                                       
             05  KEY-NAME-6     PIC X(8).                                       
             05  KEY-NAME-7     PIC X(8).                                       
             05  KEY-NAME-8     PIC X(8).                                       
             05  KEY-NAME-9     PIC X(8).                                       
       EJECT                                                                    
                                                                                
                                                                                
