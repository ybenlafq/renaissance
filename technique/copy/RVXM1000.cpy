      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVXM1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVXM1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM1000.                                                            
           02  XM10-CAPPLI                                                      
               PIC X(0006).                                                     
           02  XM10-VERSION                                                     
               PIC X(0150).                                                     
           02  XM10-LGVERSION                                                   
               PIC S9(03) COMP-3.                                               
           02  XM10-CNOMTSXSD                                                   
               PIC X(0008).                                                     
           02  XM10-CNOMTSCAR                                                   
               PIC X(008).                                                      
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM10-CAPPLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM10-CAPPLI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM10-VERSION-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM10-VERSION-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM10-LGVERSION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM10-LGVERSION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM10-CNOMTSXSD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM10-CNOMTSXSD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM10-CNOMTSCAR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM10-CNOMTSCAR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
