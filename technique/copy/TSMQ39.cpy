      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMQ39 = TS DE CONSULTATION D'UN MESSAGE                   *         
      *                                                               *         
      *    CETTE TS CONTIENT LE  MESSAGE  A AFFICHER                  *         
      *    1 SEQUENCE = LIGNE DE MESSAGE                              *         
      *                                                               *         
      *    LONGUEUR TOTALE = 78                                       *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS39                      PIC S9(05) COMP-3 VALUE +0.               
       01  ITS39-MAX                  PIC S9(05) COMP-3 VALUE +0.               
      *                                                               *         
       01  TS39-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'MQ39'.             
           05 TS39-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *01  TS39-LONG                  PIC S9(04) COMP VALUE +78.                
      *                                                               *         
       01  TS39-DONNEES.                                                        
           05 TS39-LIGNE           PIC X(78).                                   
                                                                                
