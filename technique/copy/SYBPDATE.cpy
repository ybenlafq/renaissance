      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      *                                                                 00020000
      * VERIFICATION ET EDITION DES DATES                               00030000
      *                                                                 00040000
      ***************************************************************** 00050000
       TRAITEMENT-DATE SECTION.                                         00060000
      ***************************************************************** 00070000
      *                                                                 00080000
       DEB-TRAITEMENT-DATE.                                             00090000
      *                                                                 00100000
           MOVE 4 TO CODE-RETOUR.                                       00110000
      *                                                                 00120000
           IF W-DATE-USER = SPACE                                       00130000
              PERFORM T-DATE-FILE                                       00140000
           ELSE                                                         00150000
              PERFORM T-DATE-USER                                       00160000
              IF ERREUR-DATE                                            00170000
                 MOVE ALL ZERO TO W-DATE-FILE.                          00180000
      *                                                                 00190000
       FIN-TRAITEMENT-DATE.                                             00200000
           EXIT.                                                        00210000
      *                                                                 00220000
      ***************************************************************** 00230000
       T-DATE-USER SECTION.                                             00240000
      ***************************************************************** 00250000
      *                                                                 00260000
       DEB-T-DATE-USER.                                                 00270000
      *                                                                 00280000
      * 1.    SEPARATION DES TROIS COMPOSANTES DE LA DATE               00290000
      *                                                                 00300000
           MOVE SPACE TO W-DATE-GROUPE-TOKEN.                           00310000
           MOVE 1 TO W-DATE-I.                                          00320000
           MOVE 0 TO W-DATE-J.                                          00330000
      *                                                                 00340000
      * 1.1   RECHERCHE DU DEBUT DU TOKEN                               00350000
      *                                                                 00360000
       T-DATE-CHERCHE-DEBUT.                                            00370000
      *                                                                 00380000
           IF W-DATE-USER-CAR (W-DATE-I) = SPACE OR '/' OR '-'          00390000
              ADD 1 TO W-DATE-I                                         00400000
              IF W-DATE-I GREATER 10                                    00410000
                 GO TO T-DATE-TESTE                                     00420000
              ELSE                                                      00430000
                 GO TO T-DATE-CHERCHE-DEBUT.                            00440002
      *                                                                 00450000
           ADD 1 TO W-DATE-J.                                           00460000
           MOVE 1 TO W-DATE-K.                                          00470000
      *                                                                 00480000
      * 1.2   RECHERCHE DE LA FIN DU TOKEN                              00490000
      *                                                                 00500000
       T-DATE-CHERCHE-FIN.                                              00510000
      *                                                                 00511002
           MOVE W-DATE-USER-CAR (W-DATE-I)                              00520000
                TO W-DATE-TOKEN-CAR (W-DATE-J W-DATE-K).                00530001
      *                                                                 00540001
           ADD 1 TO W-DATE-I.                                           00550001
      *                                                                 00560001
           IF W-DATE-I GREATER 10                                       00570001
              GO TO T-DATE-TESTE.                                       00580001
      *                                                                 00590001
           IF W-DATE-USER-CAR (W-DATE-I) = SPACE OR '/' OR '-'          00600001
              GO TO T-DATE-CHERCHE-DEBUT.                               00610001
      *                                                                 00620001
           ADD 1 TO W-DATE-K.                                           00630001
      *                                                                 00640001
           IF W-DATE-K GREATER 4                                        00650001
              GO TO FIN-T-DATE-USER                                     00660001
           ELSE                                                         00670001
              GO TO T-DATE-CHERCHE-FIN.                                 00680001
      *                                                                 00690001
      * 2.    PREMIERS TESTS DE SYNTAXE                                 00700001
      *                                                                 00710001
       T-DATE-TESTE.                                                    00720001
      *                                                                 00730001
      * 2.1   TESTE SI L'UNE DES COMPOSANTES A ETE OMISE                00740001
      *                                                                 00750001
           IF W-DATE-TOKEN (1) = SPACE OR                               00760001
              W-DATE-TOKEN (2) = SPACE OR                               00770001
              W-DATE-TOKEN (3) = SPACE                                  00780001
              GO TO FIN-T-DATE-USER.                                    00790001
      *                                                                 00800001
      * 2.2   TESTE SI LE JOUR EST TROP LONG                            00810001
      *                                                                 00820001
           IF W-DATE-TOKEN-CAR (1 3) NOT = SPACE                        00830001
              GO TO FIN-T-DATE-USER.                                    00840001
      *                                                                 00850001
           IF W-DATE-TOKEN-CAR (1 4) NOT = SPACE                        00860001
              GO TO FIN-T-DATE-USER.                                    00870001
      *                                                                 00880001
           IF W-DATE-TOKEN-CAR (1 2) = SPACE                            00890001
              MOVE W-DATE-TOKEN-CAR (1 1) TO W-DATE-TOKEN-CAR (1 2)     00900001
              MOVE          '0'           TO W-DATE-TOKEN-CAR (1 1).    00901001
      *                                                                 00902001
      * 2.3   TESTE SI LE MOIS EST TROP LONG                            00903001
      *                                                                 00904001
           IF W-DATE-TOKEN-CAR (2 3) NOT = SPACE                        00905001
              GO TO FIN-T-DATE-USER.                                    00906001
      *                                                                 00907001
           IF W-DATE-TOKEN-CAR (2 4) NOT = SPACE                        00908001
              GO TO FIN-T-DATE-USER.                                    00909001
      *                                                                 00909101
           IF W-DATE-TOKEN-CAR (2 2) = SPACE                            00909201
              MOVE W-DATE-TOKEN-CAR (2 1) TO W-DATE-TOKEN-CAR (2 2)     00909301
              MOVE          '0'           TO W-DATE-TOKEN-CAR (2 1).    00909401
      *                                                                 00909501
      * 2.4   ARRANGE L'ANNEE                                           00909601
      *                                                                 00909701
           IF W-DATE-TOKEN-CAR (3 3) = SPACE AND                        00909801
              W-DATE-TOKEN-CAR (3 4) = SPACE                            00910101
              MOVE W-DATE-TOKEN-CAR (3 1) TO W-DATE-TOKEN-CAR (3 3)     00910401
              MOVE W-DATE-TOKEN-CAR (3 2) TO W-DATE-TOKEN-CAR (3 4)     00910501
              MOVE          '1'           TO W-DATE-TOKEN-CAR (3 1)     00910601
              MOVE          '9'           TO W-DATE-TOKEN-CAR (3 2)     00910701
              IF W-DATE-TOKEN-CAR (3 4) = SPACE                         00910801
                 MOVE W-DATE-TOKEN-CAR (3 3) TO W-DATE-TOKEN-CAR (3 4)  00910901
                 MOVE          '0'           TO W-DATE-TOKEN-CAR (3 3). 00911001
      *                                                                 00911101
      * 2.5   REMISE SOUS FORME STANDARD DE LA DATE-USER                00911201
      *                                                                 00911301
           MOVE SPACE TO W-DATE-USER.                                   00911401
           MOVE W-DATE-TOKEN-CAR (1 1) TO W-DATE-USER-CAR(1).           00911501
           MOVE W-DATE-TOKEN-CAR (1 2) TO W-DATE-USER-CAR(2).           00911601
           MOVE W-DATE-TOKEN-CAR (2 1) TO W-DATE-USER-CAR(4).           00911701
           MOVE W-DATE-TOKEN-CAR (2 2) TO W-DATE-USER-CAR(5).           00911803
           MOVE W-DATE-TOKEN-CAR (3 1) TO W-DATE-USER-CAR(7).           00911901
           MOVE W-DATE-TOKEN-CAR (3 2) TO W-DATE-USER-CAR(8).           00912001
           MOVE W-DATE-TOKEN-CAR (3 3) TO W-DATE-USER-CAR(9).           00913001
           MOVE W-DATE-TOKEN-CAR (3 4) TO W-DATE-USER-CAR(10).          00914001
      *                                                                 00940000
      * VERIFICATION A PARTIR DES ZONES USER                            00950000
      *                                                                 00960000
           IF JJ OF W-DATE-USER NOT NUMERIC                             00970000
                                     GO TO FIN-T-DATE-USER.             00980000
           IF MM OF W-DATE-USER NOT NUMERIC                             00990000
                                     GO TO FIN-T-DATE-USER.             01000000
           IF SS OF W-DATE-USER NOT NUMERIC                             01010000
                                     GO TO FIN-T-DATE-USER.             01020000
           IF AA OF W-DATE-USER NOT NUMERIC                             01030000
                                     GO TO FIN-T-DATE-USER.             01040000
      * CONTROLE PAR RAPPORT AUX VALEURS EXTREMES                       01110000
           IF SS OF W-DATE-USER NOT = 18 AND 19                         01120000
                                     GO TO FIN-T-DATE-USER.             01130000
           IF MM OF W-DATE-USER < 1                                     01140000
                                     GO TO FIN-T-DATE-USER.             01150000
           IF MM OF W-DATE-USER > 12                                    01160000
                                     GO TO FIN-T-DATE-USER.             01170000
           IF JJ OF W-DATE-USER < 1                                     01180000
                                     GO TO FIN-T-DATE-USER.             01190000
           IF MM OF W-DATE-USER = 1                                     01200000
                 IF JJ OF W-DATE-USER > 31                              01210000
                                     GO TO FIN-T-DATE-USER.             01220000
           IF MM OF W-DATE-USER = 2                                     01230000
                 IF JJ OF W-DATE-USER > 29                              01240000
                                     GO TO FIN-T-DATE-USER.             01250000
           IF MM OF W-DATE-USER = 3                                     01260000
                 IF JJ OF W-DATE-USER > 31                              01270000
                                     GO TO FIN-T-DATE-USER.             01280000
           IF MM OF W-DATE-USER = 4                                     01290000
                 IF JJ OF W-DATE-USER > 30                              01300000
                                     GO TO FIN-T-DATE-USER.             01310000
           IF MM OF W-DATE-USER = 5                                     01320000
                 IF JJ OF W-DATE-USER > 31                              01330000
                                     GO TO FIN-T-DATE-USER.             01340000
           IF MM OF W-DATE-USER = 6                                     01350000
                 IF JJ OF W-DATE-USER > 30                              01360000
                                     GO TO FIN-T-DATE-USER.             01370000
           IF MM OF W-DATE-USER = 7                                     01380000
                 IF JJ OF W-DATE-USER > 31                              01390000
                                     GO TO FIN-T-DATE-USER.             01400000
           IF MM OF W-DATE-USER = 8                                     01410000
                 IF JJ OF W-DATE-USER > 31                              01420000
                                     GO TO FIN-T-DATE-USER.             01430000
           IF MM OF W-DATE-USER = 9                                     01440000
                 IF JJ OF W-DATE-USER > 30                              01450000
                                     GO TO FIN-T-DATE-USER.             01460000
           IF MM OF W-DATE-USER = 10                                    01470000
                 IF JJ OF W-DATE-USER > 31                              01480000
                                     GO TO FIN-T-DATE-USER.             01490000
           IF MM OF W-DATE-USER = 11                                    01500000
                 IF JJ OF W-DATE-USER > 30                              01510000
                                     GO TO FIN-T-DATE-USER.             01520000
           IF MM OF W-DATE-USER = 12                                    01530000
                 IF JJ OF W-DATE-USER > 31                              01540000
                                     GO TO FIN-T-DATE-USER.             01550000
      * CONTROLE ANNEES BISSEXTILES                                     01560000
           DIVIDE AA OF W-DATE-USER BY 4                                01570000
                                  GIVING W-DATE-Q REMAINDER W-DATE-R.   01580000
           IF  MM OF W-DATE-USER = 2                                    01590000
             IF  W-DATE-R NOT = 0                                       01600000
               IF JJ OF W-DATE-USER > 28                                01610000
                                     GO TO FIN-T-DATE-USER.             01620000
      * EXCEPTIONNELLEMENT, L'ANNEE 1900 N'ETAIT PAS BISSEXTILE         01630000
           IF  SS OF W-DATE-USER = 19                                   01640000
             IF  AA OF W-DATE-USER = 00                                 01650000
               IF  MM OF W-DATE-USER = 2                                01660000
                 IF  JJ OF W-DATE-USER > 28                             01670000
                                     GO TO FIN-T-DATE-USER.             01680000
      *                                                                 01690000
           MOVE 0 TO CODE-RETOUR.                                       01700000
           MOVE CORRESPONDING W-DATE-USER TO W-DATE-FILE.               01710000
      *                                                                 01720000
       FIN-T-DATE-USER.                                                 01730000
           EXIT.                                                        01740000
      *                                                                 01750000
      ***************************************************************** 01760000
       T-DATE-FILE SECTION.                                             01770000
      ***************************************************************** 01780000
      *                                                                 01790000
       DEB-T-DATE-FILE.                                                 01800000
      *                                                                 01810000
           MOVE 0 TO CODE-RETOUR.                                       01820000
      *                                                                 01830000
      * PETITE MISE EN FORME                                            01840000
      *                                                                 01850000
           IF JJ OF W-DATE-FILE NOT NUMERIC                             01860000
              MOVE MM OF W-DATE-FILE TO JJ OF W-DATE-FILE               01870000
              MOVE AA OF W-DATE-FILE TO MM OF W-DATE-FILE               01880000
              MOVE SS OF W-DATE-FILE TO AA OF W-DATE-FILE               01890000
              MOVE       19          TO SS OF W-DATE-FILE.              01900000
      *                                                                 01910000
      * CONVERSION (SS)AAMMJJ ---> JJ/MM/(SS)AA                         01920000
      *                                                                 01930000
           MOVE CORRESPONDING W-DATE-FILE TO W-DATE-USER.               01940000
      *                                                                 01950000
           IF SS OF W-DATE-USER = 19                                    01960000
              MOVE AA OF W-DATE-USER TO SS OF W-DATE-USER               01970000
              MOVE      SPACE        TO AA OF W-DATE-USER-REDEF.        01980000
      *                                                                 01990000
           MOVE '-' TO S1 OF W-DATE-USER.                               02000000
           MOVE '-' TO S2 OF W-DATE-USER.                               02010000
      *                                                                 02020000
      * CONVERSION SSAAMMJJ ---> SSAAQQQ                                02030000
      *                                                                 02040000
      *                                                                 02050000
           MOVE SS OF W-DATE-FILE TO SS OF W-DATE-QUANT.                02060000
           MOVE AA OF W-DATE-FILE TO AA OF W-DATE-QUANT.                02070000
      *                                                                 02080000
           MOVE 0 TO QQQ OF W-DATE-QUANT.                               02090000
           IF MM OF W-DATE-FILE > 1  ADD 31 TO QQQ OF W-DATE-QUANT.     02100000
           IF MM OF W-DATE-FILE > 2  ADD 28 TO QQQ OF W-DATE-QUANT.     02110000
           IF MM OF W-DATE-FILE > 3  ADD 31 TO QQQ OF W-DATE-QUANT.     02120000
           IF MM OF W-DATE-FILE > 4  ADD 30 TO QQQ OF W-DATE-QUANT.     02130000
           IF MM OF W-DATE-FILE > 5  ADD 31 TO QQQ OF W-DATE-QUANT.     02140000
           IF MM OF W-DATE-FILE > 6  ADD 30 TO QQQ OF W-DATE-QUANT.     02150000
           IF MM OF W-DATE-FILE > 7  ADD 31 TO QQQ OF W-DATE-QUANT.     02160000
           IF MM OF W-DATE-FILE > 8  ADD 31 TO QQQ OF W-DATE-QUANT.     02170000
           IF MM OF W-DATE-FILE > 9  ADD 30 TO QQQ OF W-DATE-QUANT.     02180000
           IF MM OF W-DATE-FILE > 10 ADD 31 TO QQQ OF W-DATE-QUANT.     02190000
           IF MM OF W-DATE-FILE > 11 ADD 30 TO QQQ OF W-DATE-QUANT.     02200000
      *                                                                 02210000
           ADD JJ OF W-DATE-FILE TO QQQ OF W-DATE-QUANT.                02220000
      *                                                                 02230000
           DIVIDE AA OF W-DATE-FILE BY 4                                02240000
                                  GIVING W-DATE-Q REMAINDER W-DATE-R.   02250000
           IF  W-DATE-R = 0                                             02260000
               IF  MM OF W-DATE-FILE > 2                                02270000
                                  ADD 1 TO QQQ OF W-DATE-QUANT.         02280000
      * EXCEPTIONNELLEMENT, L'ANNEE 1900 N'ETAIT PAS BISSEXTILE         02290000
           IF  SS OF W-DATE-FILE = 19                                   02300000
             IF  AA OF W-DATE-FILE = 00                                 02310000
               IF  MM OF W-DATE-FILE > 2                                02320000
                                  SUBTRACT 1 FROM QQQ OF W-DATE-QUANT.  02330000
      *                                                                 02331001
      * CONVERSION SSAAMMJJ ---> JOUR ET MOIS EN CLAIR                  02332001
      *                                                                 02333001
           COMPUTE W-DATE-A = (AA OF W-DATE-FILE  -  1) * 365.          02334001
           COMPUTE W-DATE-B = (AA OF W-DATE-FILE  -  1) / 4.            02335001
           COMPUTE W-DATE-C = W-DATE-A + W-DATE-B + QQQ OF W-DATE-QUANT.02336001
           DIVIDE W-DATE-C BY 7 GIVING W-DATE-Q REMAINDER W-DATE-R.     02337001
      *                                                                 02338001
           IF W-DATE-R = 0                                              02339001
              MOVE 'LUNDI'    TO W-DATE-JOUR.                           02339101
      *                                                                 02340000
           IF W-DATE-R = 1                                              02341001
              MOVE 'MARDI'    TO W-DATE-JOUR.                           02342001
      *                                                                 02343001
           IF W-DATE-R = 2                                              02344001
              MOVE 'MERCREDI' TO W-DATE-JOUR.                           02345001
      *                                                                 02346001
           IF W-DATE-R = 3                                              02347001
              MOVE 'JEUDI'    TO W-DATE-JOUR.                           02348001
      *                                                                 02349001
           IF W-DATE-R = 4                                              02349101
              MOVE 'VENDREDI' TO W-DATE-JOUR.                           02349201
      *                                                                 02349301
           IF W-DATE-R = 5                                              02349401
              MOVE 'SAMEDI'   TO W-DATE-JOUR.                           02349501
      *                                                                 02349601
           IF W-DATE-R = 6                                              02349701
              MOVE 'DIMANCHE' TO W-DATE-JOUR.                           02349801
      *                                                                 02349901
           IF MM OF W-DATE-FILE = 1                                     02350001
              MOVE 'JANVIER'   TO W-DATE-MOIS.                          02350101
      *                                                                 02350201
           IF MM OF W-DATE-FILE = 2                                     02350301
              MOVE 'FEVRIER'   TO W-DATE-MOIS.                          02350401
      *                                                                 02350501
           IF MM OF W-DATE-FILE = 3                                     02350601
              MOVE 'MARS'      TO W-DATE-MOIS.                          02350701
      *                                                                 02350801
           IF MM OF W-DATE-FILE = 4                                     02350901
              MOVE 'AVRIL'     TO W-DATE-MOIS.                          02351001
      *                                                                 02351101
           IF MM OF W-DATE-FILE = 5                                     02351201
              MOVE 'MAI'       TO W-DATE-MOIS.                          02351301
      *                                                                 02351401
           IF MM OF W-DATE-FILE = 6                                     02351501
              MOVE 'JUIN'      TO W-DATE-MOIS.                          02351601
      *                                                                 02351701
           IF MM OF W-DATE-FILE = 7                                     02351801
              MOVE 'JUILLET'   TO W-DATE-MOIS.                          02351901
      *                                                                 02352001
           IF MM OF W-DATE-FILE = 8                                     02352101
              MOVE 'AOUT'      TO W-DATE-MOIS.                          02352201
      *                                                                 02352301
           IF MM OF W-DATE-FILE = 9                                     02352401
              MOVE 'SEPTEMBRE' TO W-DATE-MOIS.                          02352501
      *                                                                 02352601
           IF MM OF W-DATE-FILE = 10                                    02352701
              MOVE 'OCTOBRE'   TO W-DATE-MOIS.                          02352801
      *                                                                 02352901
           IF MM OF W-DATE-FILE = 11                                    02353001
              MOVE 'NOVEMBRE'  TO W-DATE-MOIS.                          02353101
      *                                                                 02353201
           IF MM OF W-DATE-FILE = 12                                    02353301
              MOVE 'DECEMBRE'  TO W-DATE-MOIS.                          02353401
      *                                                                 02353501
       FIN-T-DATE-FILE.                                                 02354000
           EXIT.                                                        02360000
      *                                                                 02370000
                                                                                
                                                                                
