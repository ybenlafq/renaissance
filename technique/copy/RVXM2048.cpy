      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2048                            *        
      ******************************************************************        
       01  RVXM2048.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2048-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2048-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2048-CDESCRIPTIF.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2048-CDESCRIPTIF-LEN   PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2048-CDESCRIPTIF-LEN   PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2048-CDESCRIPTIF-TEXT  PIC X(05).                            
           10 XM2048-LDESCRIPTIF.                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2048-LDESCRIPTIF-LEN   PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2048-LDESCRIPTIF-LEN   PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2048-LDESCRIPTIF-TEXT  PIC X(20).                            
           10 XM2048-CTYPE.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2048-CTYPE-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2048-CTYPE-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2048-CTYPE-TEXT        PIC X(01).                            
           10 XM2048-CMESURE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2048-CMESURE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2048-CMESURE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2048-CMESURE-TEXT      PIC X(05).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2048                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2048-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2048-CDESCRIPTIF-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2048-CDESCRIPTIF-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2048-LDESCRIPTIF-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2048-LDESCRIPTIF-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2048-CTYPE-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2048-CTYPE-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2048-CMESURE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2048-CMESURE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 05      *        
      ******************************************************************        
                                                                                
