      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010002
      ***************************************************************** 00020002
      * ZONES DE TRAVAIL DE LA BRIQUE DE TRAITEMENT DES DATES           00030002
      ***************************************************************** 00040002
      *                                                                 00050002
       01  FILLER PIC X(16) VALUE '**** W-DATE ****'.                   00060002
      *                                                                 00070002
       01  FILLER.                                                      00080002
      *                                                                 00090002
         02  FILLER.                                                    00100002
      *                                                                 00110002
           05  W-DATE-USER.                                             00120002
             10  JJ                    PIC 9(2) VALUE 0.                00130002
             10  S1                    PIC X    VALUE '-'.              00140002
             10  MM                    PIC 9(2) VALUE 0.                00150002
             10  S2                    PIC X    VALUE '-'.              00160002
             10  SS                    PIC 9(2) VALUE 0.                00170002
             10  AA                    PIC 9(2) VALUE 0.                00180002
           05  W-DATE-USER-REDEF       REDEFINES W-DATE-USER.           00190002
             10  FILLER                PIC X(6).                        00200002
             10  SS                    PIC X(2).                        00210002
             10  AA                    PIC X(2).                        00220002
           05  FILLER REDEFINES W-DATE-USER.                            00230002
             10  FILLER OCCURS 10.                                      00231002
               15 W-DATE-USER-CAR      PIC X(1).                        00232002
      *                                                                 00240002
           05  W-DATE-FILE.                                             00250002
             10  SS                    PIC 9(2) VALUE 0.                00260002
             10  AA                    PIC 9(2) VALUE 0.                00270002
             10  MM                    PIC 9(2) VALUE 0.                00280002
             10  JJ                    PIC 9(2) VALUE 0.                00290002
      *                                                                 00300002
           05  W-DATE-QUANT.                                            00310002
             10  SS                    PIC 9(2) VALUE 0.                00320002
             10  AA                    PIC 9(2) VALUE 0.                00330002
             10  QQQ                   PIC 9(3) VALUE 0.                00340002
      *                                                                 00350002
           05  W-DATE-JOUR             PIC X(8) VALUE SPACE.            00360002
           05  W-DATE-MOIS             PIC X(9) VALUE SPACE.            00370002
      *                                                                 00380002
         02  FILLER.                                                    00390002
      *                                                                 00400002
           05  W-DATE-GROUPE-TOKEN.                                     00410002
               10  FILLER OCCURS 3.                                     00420002
                   15  W-DATE-TOKEN.                                    00430002
                       20  FILLER OCCURS 4.                             00440002
                           25  W-DATE-TOKEN-CAR   PIC X.                00450002
      *                                                                 00460002
           05  W-DATE-I               PIC S9(3) COMP-3 VALUE +0.        00470002
           05  W-DATE-J               PIC S9(3) COMP-3 VALUE +0.        00480002
           05  W-DATE-K               PIC S9(3) COMP-3 VALUE +0.        00490002
           05  W-DATE-Q               PIC S9(3) COMP-3 VALUE +0.        00500002
           05  W-DATE-R               PIC S9(3) COMP-3 VALUE +0.        00510002
           05  W-DATE-A               PIC S9(5) COMP-3 VALUE +0.        00520002
           05  W-DATE-B               PIC S9(5) COMP-3 VALUE +0.        00530002
           05  W-DATE-C               PIC S9(5) COMP-3 VALUE +0.        00540002
      *                                                                 00550002
                                                                                
                                                                                
