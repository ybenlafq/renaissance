      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVXM0101                      *        
      ******************************************************************        
       01   RVXM0101.                                                           
           10 XM01-DOCXML               PIC X(20).                              
           10 XM01-NSEQ                 PIC S9(5)V USAGE COMP-3.                
           10 XM01-ELEMENT              PIC X(50).                              
           10 XM01-USE                  PIC X(1).                               
           10 XM01-MAXOCCURS            PIC S9(3)V USAGE COMP-3.                
           10 XM01-ELEMENTP             PIC X(50).                              
           10 XM01-IMAJ                 PIC X(7).                               
           10 XM01-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 XM01-ATTCTRLF             PIC X(1).                               
           10 XM01-ELTLEG               PIC X(10).                              
       01  RVXM0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-DOCXML-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-DOCXML-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ELEMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-ELEMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-USE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-USE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-MAXOCCURS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-MAXOCCURS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ELEMENTP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-ELEMENTP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ATTCTRLF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-ATTCTRLF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ELTLEG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  XM01-ELTLEG-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
