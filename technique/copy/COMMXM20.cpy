      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * HYBRIS                                                                  
      *                                                                         
E0647 ******************************************************************        
      * DE02034 09/10/2012 CODE INITIAL                                         
      ******************************************************************        
      * COMMAREA                                                                
       01 COMM-XM20-DATA.                                                       
           05 COMM-XM20-GESTION.                                                
      *        10 COMM-XM20-IDENT             PIC X(8).                         
      *        10 COMM-XM20-STATUT            PIC S9(3) COMP-3.                 
               10 COMM-XM20-NSEQERR           PIC X(04).                        
               10 COMM-XM20-CRET              PIC X(02).                        
               10 COMM-XM20-LLIBRE            PIC X(80).                        
      *        10 COMM-XM20-LONG              PIC S9(5) COMP-3.                 
           05 COMM-XM20-ENR.                                                    
               10 COMM-XM20-CORRELID          PIC X(24).                        
      *        10 COMM-XM20-REFID             USAGE SQL TYPE IS ROWID.          
               10 COMM-XM20-REFID.                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          49 COMM-XM20-REFID-L         PIC S9(4) USAGE COMP.             
      *--                                                                       
                 49 COMM-XM20-REFID-L         PIC S9(4) COMP-5.                 
      *}                                                                        
                 49 COMM-XM20-REFID-V         PIC X(40).                        
               10 COMM-XM20-TYPE              PIC X(10).                        
               10 COMM-XM20-CONTEXT           PIC X(50).                        
               10 COMM-XM20-NCODIK            PIC X(07).                        
               10 COMM-XM20-NCODIC            PIC X(07).                        
               10 COMM-XM20-ADMIN             PIC X(50).                        
           05 FILLER                          PIC X(248).                       
      *    05 FILLER                          PIC X(50948).                     
                                                                                
