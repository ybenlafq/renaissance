      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVXM0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVXM0100                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVXM0100.                                                            
           02  XM01-DOCXML                                                      
               PIC X(0020).                                                     
           02  XM01-NSEQ                                                        
               PIC S9(05) COMP-3.                                               
           02  XM01-ELEMENT                                                     
               PIC X(0050).                                                     
           02  XM01-USE                                                         
               PIC X(0001).                                                     
           02  XM01-MAXOCCURS                                                   
               PIC S9(03) COMP-3.                                               
           02  XM01-ELEMENTP                                                    
               PIC X(0050).                                                     
           02  XM01-IMAJ                                                        
               PIC X(0007).                                                     
           02  XM01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-DOCXML-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-DOCXML-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ELEMENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-ELEMENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-USE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-USE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-MAXOCCURS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-MAXOCCURS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-ELEMENTP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-ELEMENTP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  XM01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  XM01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
