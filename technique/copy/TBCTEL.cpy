      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *    CONTR�LE DE COH{RENCE D'UNE ZONE NUM{RIQUE SAISIE                    
      *    NUM TELEPHONE DE LONGUEUR  X  (SS-TABLE)                             
      *    RENVOI : MONT-WERREUR         SI KO                                  
      *           + MONT-MSGID                                                  
      *        OU   MONT-WRESULT         SI TOUT OK                             
      *                                                                         
       VERIF-NO-TEL.                                                            
           INITIALIZE    MONT-WSORTIE.                                          
      *                                                                         
           SET MONT-WJ TO 15                                                    
           MOVE 15 TO MONT-WPREMCH                                              
      *                                                                         
      * ELIMINATION DES BLANCS EN DEBUT                                         
           PERFORM UNTIL MONT-WZONE (1:1) NOT = SPACES                          
                    MOVE MONT-WZONE (2:14) TO MONT-WZONE                        
           END-PERFORM                                                          
      * DETERMINER LA POSITION DE LA DERNIERE CHIFRE                            
           PERFORM VARYING MONT-WI FROM 15 BY -1 UNTIL MONT-WI = 0              
                                     OR MONT-WZ(MONT-WI) NOT = ' '              
                 ADD 1 TO MONT-WZP                                              
           END-PERFORM                                                          
      *                                                                         
      * V{RIFICATION CARACT}RE PAR CARACT}RE                                    
      *                                                                         
           COMPUTE MONT-WZP = 15 - MONT-WZP                                     
           PERFORM VARYING MONT-WI FROM MONT-WZP BY -1                          
                         UNTIL MONT-WI = 0  OR MONT-ERREUR-O                    
      *                                                                         
      * CONTR�LE DES CARACTERES                                                 
      *                                                                         
                    IF  MONT-WZ(MONT-WI) >= '0'                                 
                       AND MONT-WZ(MONT-WI) <= '9'                              
                         MOVE MONT-WZ(MONT-WI) TO MONT-W9(MONT-WJ)              
                         SET MONT-WJ DOWN BY 1                                  
                         SUBTRACT 1 FROM MONT-WPREMCH                           
                     ELSE                                                       
                          MOVE 'LE NUMERO N"EST PAS NUMERIQUE'                  
                                         TO MONT-MSGID                          
                          SET MONT-ERREUR-O TO TRUE                             
                    END-IF                                                      
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
           IF MONT-WPREMCH < 15                                                 
              ADD 1 TO  MONT-WPREMCH                                            
           END-IF                                                               
      *    TRAITEMENT DU R{SULTAT SI PAS D'ANOMALIE DIRECTEMENT D{CELABLE       
      *                                                                         
           IF MONT-ERREUR-N                                                     
      *                                                                         
      *       ON N'A PAS LE NOMBRE DE CHIFFRES DEMAND                           
      *       le 18.03.03 MONT-WCH-MIN= MONT-WCH-MAX                            
              IF MONT-WZP < MONT-WCH-MIN OR MONT-WZP > MONT-WCH-MAX             
      *           STRING 'NUMERO TELEPHONE INFERIEURE � '                       
      *           MONT-WCH-MIN ' OU SUPERIEURE � ' MONT-WCH-MAX                 
                  STRING 'NUMERO TELEPHONE a '                                  
                  MONT-WCH-MIN ' CHIFFRES '                                     
                  DELIMITED BY SIZE  INTO  MONT-MSGID                           
                  SET  MONT-ERREUR-O TO TRUE                                    
              END-IF                                                            
      *       CADRAGE @ GAUCHE DU R{SULTAT                                      
              MOVE MONT-WRESULT-A(MONT-WPREMCH:MONT-WZP)                        
                                          TO MONT-WRESULT-A                     
              INITIALIZE MONT-WPREMCH                                           
              INSPECT MONT-WRESULT-A TALLYING                                   
                                   MONT-WPREMCH  FOR ALL '0'                    
              IF MONT-WPREMCH = MONT-WZP                                        
                 MOVE SPACE TO MONT-WRESULT-A                                   
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-VERIF-NO-TEL. EXIT.                                                  
                                                                                
