      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:21 >
      
       01  SYD2N1I.
           02  FILLER PIC X(12).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  CDTRANL    COMP  PIC  S9(4).
      *--
           02  CDTRANL COMP-5  PIC  S9(4).
      *}
           02  CDTRANF    PICTURE X.
           02  FILLER REDEFINES CDTRANF.
             03 CDTRANA    PICTURE X.
           02  CDTRANI  PIC X(4).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  GMMSGL    COMP  PIC  S9(4).
      *--
           02  GMMSGL COMP-5  PIC  S9(4).
      *}
           02  GMMSGF    PICTURE X.
           02  FILLER REDEFINES GMMSGF.
             03 GMMSGA    PICTURE X.
           02  GMMSGI  PIC X(54).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  DATEL    COMP  PIC  S9(4).
      *--
           02  DATEL COMP-5  PIC  S9(4).
      *}
           02  DATEF    PICTURE X.
           02  FILLER REDEFINES DATEF.
             03 DATEA    PICTURE X.
           02  DATEI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  HEURL    COMP  PIC  S9(4).
      *--
           02  HEURL COMP-5  PIC  S9(4).
      *}
           02  HEURF    PICTURE X.
           02  FILLER REDEFINES HEURF.
             03 HEURA    PICTURE X.
           02  HEURI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  TRMCICL    COMP  PIC  S9(4).
      *--
           02  TRMCICL COMP-5  PIC  S9(4).
      *}
           02  TRMCICF    PICTURE X.
           02  FILLER REDEFINES TRMCICF.
             03 TRMCICA    PICTURE X.
           02  TRMCICI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  NETIDL    COMP  PIC  S9(4).
      *--
           02  NETIDL COMP-5  PIC  S9(4).
      *}
           02  NETIDF    PICTURE X.
           02  FILLER REDEFINES NETIDF.
             03 NETIDA    PICTURE X.
           02  NETIDI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  NETTRML    COMP  PIC  S9(4).
      *--
           02  NETTRML COMP-5  PIC  S9(4).
      *}
           02  NETTRMF    PICTURE X.
           02  FILLER REDEFINES NETTRMF.
             03 NETTRMA    PICTURE X.
           02  NETTRMI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG101L    COMP  PIC  S9(4).
      *--
           02  LIG101L COMP-5  PIC  S9(4).
      *}
           02  LIG101F    PICTURE X.
           02  FILLER REDEFINES LIG101F.
             03 LIG101A    PICTURE X.
           02  LIG101I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG111L    COMP  PIC  S9(4).
      *--
           02  LIG111L COMP-5  PIC  S9(4).
      *}
           02  LIG111F    PICTURE X.
           02  FILLER REDEFINES LIG111F.
             03 LIG111A    PICTURE X.
           02  LIG111I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG121L    COMP  PIC  S9(4).
      *--
           02  LIG121L COMP-5  PIC  S9(4).
      *}
           02  LIG121F    PICTURE X.
           02  FILLER REDEFINES LIG121F.
             03 LIG121A    PICTURE X.
           02  LIG121I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG131L    COMP  PIC  S9(4).
      *--
           02  LIG131L COMP-5  PIC  S9(4).
      *}
           02  LIG131F    PICTURE X.
           02  FILLER REDEFINES LIG131F.
             03 LIG131A    PICTURE X.
           02  LIG131I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG141L    COMP  PIC  S9(4).
      *--
           02  LIG141L COMP-5  PIC  S9(4).
      *}
           02  LIG141F    PICTURE X.
           02  FILLER REDEFINES LIG141F.
             03 LIG141A    PICTURE X.
           02  LIG141I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG151L    COMP  PIC  S9(4).
      *--
           02  LIG151L COMP-5  PIC  S9(4).
      *}
           02  LIG151F    PICTURE X.
           02  FILLER REDEFINES LIG151F.
             03 LIG151A    PICTURE X.
           02  LIG151I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG161L    COMP  PIC  S9(4).
      *--
           02  LIG161L COMP-5  PIC  S9(4).
      *}
           02  LIG161F    PICTURE X.
           02  FILLER REDEFINES LIG161F.
             03 LIG161A    PICTURE X.
           02  LIG161I  PIC X(44).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02  LIG171L    COMP  PIC  S9(4).
      *--
           02  LIG171L COMP-5  PIC  S9(4).
      *}
           02  LIG171F    PICTURE X.
           02  FILLER REDEFINES LIG171F.
             03 LIG171A    PICTURE X.
           02  LIG171I  PIC X(44).
       01  SYD2N1O REDEFINES SYD2N1I.
           02  FILLER PIC X(12).
           02  FILLER PICTURE X(3).
           02  CDTRANO  PIC X(4).
           02  FILLER PICTURE X(3).
           02  GMMSGO  PIC X(54).
           02  FILLER PICTURE X(3).
           02  DATEO  PIC X(8).
           02  FILLER PICTURE X(3).
           02  HEURO  PIC X(8).
           02  FILLER PICTURE X(3).
           02  TRMCICO  PIC X(8).
           02  FILLER PICTURE X(3).
           02  NETIDO  PIC X(8).
           02  FILLER PICTURE X(3).
           02  NETTRMO  PIC X(8).
           02  FILLER PICTURE X(3).
           02  LIG101O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG111O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG121O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG131O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG141O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG151O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG161O  PIC X(44).
           02  FILLER PICTURE X(3).
           02  LIG171O  PIC X(44).
      
