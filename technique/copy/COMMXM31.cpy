      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * HYBRIS                                                                  
      *                                                                         
      ******************************************************************        
      * DE02034 19/12/2012 CODE INITIAL                                         
      ******************************************************************        
      * COMMAREA                                                                
       01 COMM-XM31-DATA.                                                       
           05 COMM-XM31-TYPE                  PIC X(50).                        
           05 COMM-XM31-NCODIC                PIC X(07).                        
           05 COMM-XM31-CRET                  PIC X(02).                        
           05 COMM-XM31-ERREUR                PIC X(52).                        
           05 FILLER                          PIC X(403).                       
                                                                                
