      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TRAVAIL DE LA BRIQUE DE TRAITEMENT DES DATES         *         
      ****************************************************** SYKWDATE**         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-DATE ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-DATE-USER.                                                     
             10  JJ                    PIC 9(2) VALUE 0.                        
             10  S1                    PIC X    VALUE '-'.                      
             10  MM                    PIC 9(2) VALUE 0.                        
             10  S2                    PIC X    VALUE '-'.                      
             10  SS                    PIC 9(2) VALUE 0.                        
             10  AA                    PIC 9(2) VALUE 0.                        
           05  W-DATE-USER-REDEF       REDEFINES W-DATE-USER.                   
             10  FILLER                PIC X(6).                                
             10  SS                    PIC X(2).                                
             10  AA                    PIC X(2).                                
      *                                                                         
           05  W-DATE-FILE.                                                     
             10  SS                    PIC 9(2) VALUE 0.                        
             10  AA                    PIC 9(2) VALUE 0.                        
             10  MM                    PIC 9(2) VALUE 0.                        
             10  JJ                    PIC 9(2) VALUE 0.                        
      *                                                                         
           05  W-DATE-QUANT.                                                    
             10  SS                    PIC 9(2) VALUE 0.                        
             10  AA                    PIC 9(2) VALUE 0.                        
             10  QQQ                   PIC 9(3) VALUE 0.                        
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-DATE-Q               PIC S9(3) COMP-3 VALUE +0.                
           05  W-DATE-R               PIC S9(1) COMP-3 VALUE +0.                
       EJECT                                                                    
                                                                                
                                                                                
