      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMQ35 = TS DE CONSULTATION DES ANOMALIES MQ10             *         
      *                                                               *         
      *    CETTE TS CONTIENT LES MESSAGES A AFFICHER                  *         
      *    1 SEQUENCE = DATE/HEURE/SOC/LIEU/CODE FONCTION/MESSAGE     *         
      *                                                               *         
      *    LONGUEUR TOTALE = 123                                      *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS35                      PIC S9(05) COMP-3 VALUE +0.               
       01  ITS35-MAX                  PIC S9(05) COMP-3 VALUE +0.               
      *                                                               *         
       01  TS35-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'MQ35'.             
           05 TS35-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS35-LONG                  PIC S9(04) COMP VALUE +123.               
      *--                                                                       
       01  TS35-LONG                  PIC S9(04) COMP-5 VALUE +123.             
      *}                                                                        
      *                                                               *         
       01  TS35-DONNEES.                                                        
           05 TS35-DATE            PIC X(08).                                   
           05 TS35-HEURE           PIC X(08).                                   
           05 TS35-NSOC            PIC X(03).                                   
           05 TS35-NLIEU           PIC X(03).                                   
           05 TS35-CFONC           PIC X(03).                                   
           05 TS35-MSG             PIC X(100).                                  
                                                                                
