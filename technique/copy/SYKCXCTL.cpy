      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *          * PASSAGE DU CONTROL A UN NOUVEAU PROGRAMME          *         
      ****************************************************** SYKCXCTL *         
       XCTL-PROG-COMMAREA SECTION.                                              
       EXEC CICS SYNCPOINT                                                      
       END-EXEC.                                                                
      *     MOVE '00006   ' TO DFHEIV0                                      
      *     CALL 'DFHEI1' USING DFHEIV0.                                        
            IF NOM-PROG-XCTL  NOT =  'PGMHELP'                                  
               PERFORM SWAP-CTL-TACHE                                           
            END-IF.                                                             
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS XCTL                                                           
                 PROGRAM  (NOM-PROG-XCTL)                                       
                 COMMAREA (Z-COMMAREA)                                          
                 LENGTH   (LONG-COMMAREA)                                       
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*      MOVE '��00013   ' TO DFHEIV0                              
      *dfhei*      CALL 'DFHEI1' USING DFHEIV0  NOM-PROG-XCTL Z-COMMAREA        
      *dfhei*     LONG-COMMAREA.                                                
      *dfhei*} decommente EXEC                                                  
            MOVE EIBRCODE TO EIB-RCODE.                                         
            IF   NOT EIB-NORMAL                                                 
                 GO TO ABANDON-CICS                                             
            END-IF.                                                             
       FIN-XCTL-PROG-COMMAREA. EXIT.                                            
       EJECT                                                                    
                                                                                
                                                                                
