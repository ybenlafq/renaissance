      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY * * * * * * * * * * * * * * * * * * * * * * * * * * *          
      *          * CONTROLE DU SWAP INTERDIT SUR LA MEME TACHE       *          
      ************ * * * * * * * * * * * * * * * * * * COPYY SYKXSWAP *          
      *                                                                         
       SWAP-CTL-TACHE             SECTION.                                      
      *                                                                         
           MOVE NOM-PROG-XCTL  TO  NOM-PROG-XCTL-SWAP.                          
      *                                                                         
           PERFORM SORTIE-SWAP-READ.                                            
      *                                                                         
           IF TROUVE                                                            
              IF LINK-TS-NOM-TACHE = NOM-TACHE-SWAP                             
                 MOVE 'SWAP REFUSE : ECRAN DEJA ACTIF'  TO  MLIBERRI            
                 PERFORM REST-SWAP-ATTR                                         
                 MOVE Z-MAP            TO  Z-INOUT-MAP                          
                 MOVE LENGTH OF Z-MAP  TO  LONG-MAP                             
                 PERFORM SEND-MAP-SWAP                                          
                 MOVE NOM-TACHE        TO  NOM-TACHE-RETOUR                     
                 PERFORM RETOUR-COMMAREA                                        
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-SWAP-CTL-TACHE.        EXIT.                                         
       EJECT                                                                    
                                                                                
                                                                                
