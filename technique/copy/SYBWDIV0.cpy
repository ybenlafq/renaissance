      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
                                                                                
      *                                                                 00000000
      ***************************************************************** 00000100
      *   SAUVEGARDE  DE ZONES IMPORTANTES EN CAS DE PLANTAGE         * 00000200
      ***************************************************************** 00000300
      *                                                               * 00000400
      * DEBUGGIN EST UNE ZONE DONT LE BUT EST DE...                   * 00000507
      *                                                               * 00000601
      *                                                               * 00000700
      * KONTROL  = 0  PAS D'ERREUR LOGIQUE.                           * 00000800
      *          = 1  ERREUR AVEC INTERRUPTION DU TRAITEMENT.         * 00000900
      *                                                               * 00001000
      * CODE-RETOUR = CODE RETOUR DE LA SECTION QUE L'ON VIENT        * 00001100
      *               D'EXECUTER.                                     * 00001200
      *                                                               * 00001300
      ***************************************************************** 00001400
      *                                                                 00001500
       01  FILLER.                                                      00001600
      *                                                                 00001700
         02  FILLER PIC X(20) VALUE '* WORKING--STORAGE *'.             00001800
      *                                                                 00001900
         02  FILLER      PIC X(13)          VALUE 'DEBUGGIN.....'.      00002008
         02  DEBUGGIN    PIC X(03)          VALUE 'NON'.                00002107
      *                                                                 00004300
         02  FILLER      PIC X(15)          VALUE 'KONTROL........'.    00004400
         02  KONTROL     PIC X(1)           VALUE '0'.                  00004500
           88  ERREUR                     VALUE '1'.                    00004701
           88  OK                         VALUE '0'.                    00004800
      *                                                                 00004900
         02  FILLER      PIC X(15)          VALUE 'CODE-RETOUR....'.    00005000
         02  CODE-RETOUR PIC X(1)           VALUE '0'.                  00005100
           88  TROUVE                     VALUE '0'.                    00005200
           88  NORMAL                     VALUE '0'.                    00005300
           88  DATE-OK                    VALUE '0'.                    00005400
           88  SELECTE                    VALUE '0'.                    00005500
           88  NON-TROUVE                 VALUE '1'.                    00005600
           88  NON-SELECTE                VALUE '1'.                    00005700
           88  ANORMAL                    VALUE '1'.                    00005800
           88  EXISTE-DEJA                VALUE '2'.                    00005900
           88  FIN-FICHIER                VALUE '3'.                    00006000
           88  FIN-DONNEES                VALUE '3'.                    00006101
           88  ERREUR-DATE                VALUE '4'.                    00006200
           88  ERREUR-FORMAT              VALUE '5'.                    00006300
           88  ERREUR-HEURE               VALUE '6'.                    00006400
      *                                                                 00006600
      ***************************************************************** 00008100
      *     INDICES                                                     00008200
      ***************************************************************** 00008300
      *                                                                 00008400
         02  FILLER      PIC X(13)          VALUE 'INDICE-IA....'.      00008501
         02  IA          PIC S9(5) COMP-3   VALUE +0.                   00008601
           88  FIN-BOUCLE-IA              VALUE +999.                   00008700
      *                                                                 00008800
         02  FILLER      PIC X(13)          VALUE 'INDICE-AI....'.      00008901
         02  AI          PIC S9(5) COMP-3   VALUE +0.                   00009001
           88  FIN-BOUCLE-AI              VALUE +999.                   00009100
      *                                                                 00009200
         02  FILLER      PIC X(13)          VALUE 'INDICE-IT....'.      00009301
         02  IT          PIC S9(5) COMP-3   VALUE +0.                   00009401
           88  FIN-TABLE                  VALUE +999.                   00009500
      *                                                                 00009600
         02  FILLER      PIC X(13)          VALUE 'INDICE-LIGNE.'.      00009701
         02  IL          PIC S9(5) COMP-3   VALUE +0.                   00009801
           88  FIN-BOUCLE-IL              VALUE +999.                   00009900
      *                                                                 00010000
         02  FILLER      PIC X(13)          VALUE 'INDICE-PAGE..'.      00010101
         02  IP          PIC S9(5) COMP-3   VALUE +0.                   00010201
      *                                                                 00010300
         02  FILLER            PIC X(15)    VALUE 'ETAT-CONSULT...'.    00010400
         02  ETAT-CONSULTATION PIC X        VALUE '0'.                  00010500
           88  FIN-CONSULTATION           VALUE '1'.                    00010600
      *                                                                 00010700
         02  FILLER            PIC X(15)    VALUE 'ACTIVITE.......'.    00010803
         02  ACTIVITE          PIC X        VALUE '0'.                  00010903
           88  FIN-EXTRACTION             VALUE '1'.                    00011003
      *                                                                 00011103
         02  FILLER      PIC X(7)           VALUE 'NUM-9..'.            00011201
         02  NUM-9       PIC 9(9)           VALUE 0.                    00011301
         02  NUM-8       REDEFINES NUM-9 PIC 9(8).                      00011401
         02  NUM-7       REDEFINES NUM-9 PIC 9(7).                      00011501
         02  NUM-6       REDEFINES NUM-9 PIC 9(6).                      00011601
         02  NUM-5       REDEFINES NUM-9 PIC 9(5).                      00011701
         02  NUM-4       REDEFINES NUM-9 PIC 9(4).                      00011801
         02  NUM-3       REDEFINES NUM-9 PIC 9(3).                      00011901
         02  NUM-2       REDEFINES NUM-9 PIC 9(2).                      00012001
         02  NUM-1       REDEFINES NUM-9 PIC 9(1).                      00012101
      *                                                                 00012200
         02  FILLER      PIC X(12)          VALUE 'COMPTEUR....'.       00012300
         02  COMPTEUR    PIC S9(7) COMP-3   VALUE +0.                   00012400
      *                                                                 00012500
      ***************************************************************** 00012600
      *     CODE ABANDON                                                00012700
      ***************************************************************** 00012800
      *                                                                 00012900
         02  FILLER              PIC X(15) VALUE 'CODE-ABANDON...'.     00013000
         02  CODE-ABANDON        PIC X VALUE SPACE.                     00013100
         88  AIDA-ABANDON            VALUE 'K'.                         00013204
         88  TACHE-ABANDON           VALUE 'T'.                         00013300
         88  BATCH-ABANDON           VALUE 'B'.                         00013401
         88  DL1-ABANDON             VALUE 'D'.                         00013500
      *                                                                 00013600
      ***************************************************************** 00013700
      *     DATE ET HEURE DE COMPILATION                                00013800
      ***************************************************************** 00013900
      *                                                                 00014000
         02  FILLER      PIC X(16)          VALUE 'COMPILATION DU :'.   00014104
         02  Z-WHEN-COMPILED.                                           00014201
             03 Z-WHEN-COMPILED-DATE        PIC X(8)    VALUE SPACE.    00014301
             03 Z-WHEN-COMPILED-HEURE       PIC X(8)    VALUE SPACE.    00014401
      *                                                                 00014500
      ***************************************************************** 00015500
      *     ZONES   FICHIER                                             00015609
      ***************************************************************** 00015700
      *                                                                 00018900
           02  FILE-NAME            PIC X(8)       VALUE SPACE.         00019009
           02  PATH-NAME            PIC X(8)       VALUE SPACE.         00019109
           02  FILE-STATUS          PIC X(2)       VALUE '**'.          00019209
           02  FILE-FUNCTION        PIC X(4)       VALUE '****'.        00019309
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FILE-LONG            PIC S9(4) COMP VALUE +0.            00019509
      *--                                                                       
           02  FILE-LONG            PIC S9(4) COMP-5 VALUE +0.                  
      *}                                                                        
      *                                                                 00019609
      ***************************************************************** 00019709
      *     ZONES   DIVERSES                                            00019809
      ***************************************************************** 00019909
      *                                                                 00020009
         02  CODE-FIN-BOUCLE-IA        PIC S9(3) COMP-3 VALUE +999.     00020100
         02  CODE-FIN-BOUCLE-AI        PIC S9(3) COMP-3 VALUE +999.     00020200
         02  CODE-FIN-TABLE            PIC S9(3) COMP-3 VALUE +999.     00020300
      *                                                                 00020400
         02  CODE-CONSULTATION-EN-COURS    PIC X     VALUE '0'.         00020500
         02  CODE-FIN-CONSULTATION         PIC X     VALUE '1'.         00020600
         02  CODE-FIN-EXTRACTION           PIC X     VALUE '1'.         00020703
      *                                                                 00020800
         02  APOSTROPHE                    PIC X     VALUE QUOTE.       00020900
      *                                                                 00021000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  BINAIRE                       PIC S9(4) COMP VALUE +0.     00021100
      *--                                                                       
         02  BINAIRE                       PIC S9(4) COMP-5 VALUE +0.           
      *}                                                                        
         02  REDEF-BINAIRE REDEFINES BINAIRE PIC X(2).                  00021200
      *                                                                 00022000
                                                                                
