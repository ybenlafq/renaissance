      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEG9000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG9000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEG9000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEG9000.                                                            
      *}                                                                        
           02  EG90-IDENTTS                                                     
               PIC X(0008).                                                     
           02  EG90-NOMETAT                                                     
               PIC X(0008).                                                     
           02  EG90-CHAMPTRI                                                    
               PIC X(0254).                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG90-RANGTS                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EG90-RANGTS                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEG9000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEG9000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEG9000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG90-IDENTTS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EG90-IDENTTS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG90-NOMETAT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EG90-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG90-CHAMPTRI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EG90-CHAMPTRI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG90-RANGTS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  EG90-RANGTS-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
