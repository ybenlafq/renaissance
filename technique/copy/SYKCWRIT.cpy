      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      *  MODULE GENERALISE DE WRITE SIMPLE                              00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       WRITE-RECORD             SECTION.                                00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITE    DATASET (FILE-NAME)                                   
                          RIDFLD  (VSAM-KEY)                                    
                          FROM    (Z-INOUT)                                     
                          LENGTH  (FILE-LONG)                                   
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��0�@00008   ' TO DFHEIV0                              
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME Z-INOUT FILE-LO        
      *dfhei*     VSAM-KEY.                                                     
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001500
           MOVE EIBRCODE TO EIB-RCODE.                                  00001600
           IF   EIB-NORMAL                                              00001700
                MOVE 0 TO CODE-RETOUR                                   00001800
           ELSE                                                         00001900
                IF   EIB-DUPREC                                         00002000
                     MOVE 2 TO CODE-RETOUR                              00002100
                ELSE                                                    00002200
                     GO TO ABANDON-CICS                                 00002300
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002400
       FIN-WRITE-RECORD. EXIT.                                          00002500
                    EJECT                                               00002700
                                                                                
                                                                                
