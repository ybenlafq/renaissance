      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      * SEND TEXT ERASE                                               * 00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       SEND-TEXT-ERASE            SECTION.                              00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND TEXT                                                      
                           ERASE                                                
                           FROM (LIGNE)                                         
                           LENGTH (131)                                         
                           NOHANDLE                                             
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�-���00008   ' TO DFHEIV0                             
      *dfhei*     MOVE 131 TO DFHB0020                                          
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DFHDUMMY LIGNE DFHB0020.         
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001300
           MOVE EIBRCODE TO EIB-RCODE.                                  00001400
           IF NOT EIB-NORMAL                                            00001500
              GO TO ABANDON-CICS                                        00001600
           END-IF.                                                              
      *                                                                 00001700
       FIN-SEND-TEXT-ERASE. EXIT.                                       00001800
                    EJECT                                               00002000
                                                                                
                                                                                
