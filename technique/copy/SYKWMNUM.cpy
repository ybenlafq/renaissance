      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ZWN-TRAVAIL.                                                 00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-LGIN            PIC S9(4)   VALUE ZERO   COMP.       00000100
      *--                                                                       
           05  ZWN-LGIN            PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-LGOUT           PIC S9(4)   VALUE ZERO   COMP.       00000110
      *--                                                                       
           05  ZWN-LGOUT           PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-NBDEC           PIC S9(4)   VALUE ZERO   COMP.       00000120
      *--                                                                       
           05  ZWN-NBDEC           PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-I1              PIC S9(4)   VALUE ZERO   COMP.       00000190
      *--                                                                       
           05  ZWN-I1              PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-I2              PIC S9(4)   VALUE ZERO   COMP.       00000200
      *--                                                                       
           05  ZWN-I2              PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-ENTIER          PIC S9(4)   VALUE ZERO   COMP.       00000210
      *--                                                                       
           05  ZWN-ENTIER          PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ZWN-DECIM           PIC S9(4)   VALUE ZERO   COMP.       00000220
      *--                                                                       
           05  ZWN-DECIM           PIC S9(4)   VALUE ZERO COMP-5.               
      *}                                                                        
           05  ZWN-CDRET           PIC X.                               00000140
               88  ZWN-OK                          VALUE '0'.           00000150
               88  ZWN-INVCAR                      VALUE '1'.           00000160
               88  ZWN-DBLEDECIM                   VALUE '2'.           00000170
               88  ZWN-TROPLONG                    VALUE '3'.           00000180
               88  ZWN-DBLESIGNE                   VALUE '4'.           00000190
               88  ZWN-TROPDECIM                   VALUE '5'.           00000200
               88  ZWN-PASNEGATIF                  VALUE '6'.           00000210
               88  ZWN-ERRIN                       VALUE 'P'.           00000220
           05  ZWN-TYPDECIMAL      PIC X      VALUE ','.                00000230
           05  ZWN-SIGNE           PIC X      VALUE ' '.                00000240
               88  ZWN-MOINS                       VALUE ' '.           00000250
               88  ZWN-NOTMOINS                    VALUE 'N'.           00000250
           05  ZWN-ZEDIT           PIC X(18).                           00000260
           05  ZWN-ZNUM.                                                00000270
               10  ZWN-ZNUMN           PIC S9(18).                      00000280
           05  ZWN-WORK            PIC X(18)   VALUE SPACES.            00000230
           05  ZWN-VIRGULE         PIC X       VALUE ','.               00000240
           05  ZWN-POINT           PIC X       VALUE '.'.               00000250
           05  ZWN-DECIMAL-FOUND   PIC X       VALUE 'N'.               00000260
               88  ZWN-DECIM-NON                   VALUE 'N'.           00000270
               88  ZWN-DECIM-OUI                   VALUE 'O'.           00000280
           05  ZWN-SIGNE-FOUND     PIC X       VALUE ' '.               00000290
               88  ZWN-NEUTRE                      VALUE ' '.           00000300
               88  ZWN-NEGATIF                     VALUE '-'.           00000310
               88  ZWN-POSITIF                     VALUE '+'.           00000320
           05  ZWN-ZNEGATIVE.                                           00000330
               10  ZWN-ZNEGATIVN       PIC S9      VALUE ZERO.          00000340
           05  ZWN-NONSIGNE        PIC  9      VALUE ZERO.              00000350
                                                                                
