      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVXM2034                           *        
      ******************************************************************        
       01  RVXM2034.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2034-REFID            USAGE SQL TYPE IS ROWID.                  
      *--                                                                       
           10 XM2034-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2034-CPREST.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2034-CPREST-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2034-CPREST-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2034-CPREST-TEXT   PIC X(5).                                 
           10 XM2034-CCATTR.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2034-CCATTR-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2034-CCATTR-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2034-CCATTR-TEXT   PIC X(10).                                
           10 XM2034-CVATTR.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2034-CVATTR-LEN    PIC S9(4) USAGE COMP.                     
      *--                                                                       
              49 XM2034-CVATTR-LEN    PIC S9(4) COMP-5.                         
      *}                                                                        
              49 XM2034-CVATTR-TEXT   PIC X(20).                                
      ******************************************************************        
      * FLAGS                                                          *        
      ******************************************************************        
       01  RVXM2034-F.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2034-CPREST-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2034-CPREST-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2034-CCATTR-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2034-CCATTR-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2034-CVATTR-F             PIC S9(4) USAGE COMP.                 
      *--                                                                       
           10 XM2034-CVATTR-F             PIC S9(4) COMP-5.                     
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *        
      ******************************************************************        
                                                                                
