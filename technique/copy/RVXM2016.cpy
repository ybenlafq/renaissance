      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2016                  *        
      ******************************************************************        
       01  RVXM2016.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2016-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2016-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2016-NCODIC          PIC X(07).                                 
           10 XM2016-CARACTSPE       PIC X(28).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2016-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2016-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2016-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2016-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2016-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2016-CARACTSPE-F     PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2016-CARACTSPE-F     PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
