      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *                                                                 00180300
      ***************************************************************** 00180400
      *        SCHEDULING DL/1 EXEC DLI     (UN SEUL PAR PROGRAMME)     00180500
      * DOIT OBLIGATOIREMENT ETRE EXECUTE AVANT TOUT AUTRE ORDRE DL1    00180600
      ***************************************************************** 00180700
      *                                                                 00180800
       SCHEDULING-EXEC        SECTION.                                  00180900
      *                                                                 00181000
      *dfhei*{ decommente EXEC                                                  
      *{ Pre-Catalog Correct-DAR-170                                            
      *EXEC DLI SCHD PSB((NOM-PSB)) END-EXEC.                                   
      *{ Post-Translation Correct-EXEC-DLI-30                                   
      * EXEC DLI SCHD PSB(NOM-PSB) END-EXEC.                                    
       MOVE '  ' TO DIBSTAT.                                                    
      *} Post-Translation                                                       
      *} Pre-Catalog                                                            
      *dfhei*- commente DFHEI1                                                  
      *dfhei*       MOVE '04' TO DIBVER OF DLZDIB                               
      *dfhei*       MOVE '� DLI     00009   [' TO DFHEIV0                   
      *dfhei*       MOVE NOM-PSB TO DFHC0080                                    
      *dfhei*       CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHC0080.               
      *dfhei*} decommente EXEC                                                  
      *                                                                 00182000
           IF  DIBSTAT NOT EQUAL '  ' THEN                              00183000
               MOVE 'ERREUR SCHEDULING-CALL' TO TRACE-DL1-MESSAGE       00184000
               GO TO ABANDON-DL1                                        00185000
           END-IF.                                                              
      *                                                                 00186000
           MOVE 'K' TO ETAT-PSB.                                        00187000
      *                                                                 00188000
       FIN-SCHEDULING-EXEC.   EXIT.                                     00189000
                EJECT                                                   00200000
                                                                                
                                                                                
