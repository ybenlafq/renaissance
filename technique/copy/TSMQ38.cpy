      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMQ38 = TS DE CONSULTATION DES ANOMALIES MQ10             *         
      *                                                               *         
      *    CETTE TS CONTIENT LES MESSAGES A AFFICHER                  *         
      *    1 SEQUENCE = DATE/HEURE/SOC/LIEU/CODE FONCTION/            *         
      *                 TAILLE/PRIORITE/MSGID                         *         
      *                                                               *         
      *    LONGUEUR TOTALE = 76                                       *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS38                      PIC S9(05) COMP-3 VALUE +0.               
       01  ITS38-MAX                  PIC S9(05) COMP-3 VALUE +0.               
      *                                                               *         
       01  TS38-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'MQ38'.             
           05 TS38-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *01  TS38-LONG                  PIC S9(04) COMP VALUE +148.               
      *                                                               *         
       01  TS38-DONNEES.                                                        
           05 TS38-MSGID           PIC X(24).                                   
           05 TS38-CORREL          PIC X(24).                                   
           05 TS38-DATE            PIC X(08).                                   
           05 TS38-HEURE           PIC X(04).                                   
           05 TS38-IDSI            PIC X(10).                                   
           05 TS38-CLE             PIC X(45).                                   
           05 TS38-COPCO           PIC X(03).                                   
           05 TS38-TAILLE          PIC 9(05).                                   
           05 TS38-PRIOR           PIC 9(02).                                   
           05 TS38-CHRONO          PIC 9(07).                                   
           05 TS38-TOTAL           PIC 9(07).                                   
           05 TS38-EXPIRY          PIC 9(09).                                   
                                                                                
