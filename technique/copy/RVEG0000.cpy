      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVEG0000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEG0000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVEG0000.                                                    00000090
           02  EG00-NOMETAT                                             00000100
               PIC X(0008).                                             00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-RUPTCHTPAGE                                         00000120
      *        PIC S9(4) COMP.                                          00000130
      *--                                                                       
           02  EG00-RUPTCHTPAGE                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-RUPTRAZPAGE                                         00000140
      *        PIC S9(4) COMP.                                          00000150
      *--                                                                       
           02  EG00-RUPTRAZPAGE                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-LARGEUR                                             00000160
      *        PIC S9(4) COMP.                                          00000170
      *--                                                                       
           02  EG00-LARGEUR                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-LONGUEUR                                            00000180
      *        PIC S9(4) COMP.                                          00000190
      *--                                                                       
           02  EG00-LONGUEUR                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           02  EG00-DSECTCREE                                           00000200
               PIC X(0001).                                             00000210
           02  EG00-TRANSFERE                                           00000220
               PIC X(0001).                                             00000230
           02  EG00-DSYST                                               00000240
               PIC S9(13) COMP-3.                                       00000250
           02  EG00-LIBETAT.                                            00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        49  EG00-LIBETAT-L                                       00000270
      *            PIC S9(4) COMP.                                      00000280
      *--                                                                       
               49  EG00-LIBETAT-L                                               
                   PIC S9(4) COMP-5.                                            
      *}                                                                        
               49  EG00-LIBETAT-V                                       00000290
                   PIC X(0064).                                         00000300
      *                                                                 00000310
      *---------------------------------------------------------        00000320
      *   LISTE DES FLAGS DE LA TABLE RVEG0000                          00000330
      *---------------------------------------------------------        00000340
      *                                                                 00000350
       01  RVEG0000-FLAGS.                                              00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-NOMETAT-F                                           00000370
      *        PIC S9(4) COMP.                                          00000380
      *--                                                                       
           02  EG00-NOMETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-RUPTCHTPAGE-F                                       00000390
      *        PIC S9(4) COMP.                                          00000400
      *--                                                                       
           02  EG00-RUPTCHTPAGE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-RUPTRAZPAGE-F                                       00000410
      *        PIC S9(4) COMP.                                          00000420
      *--                                                                       
           02  EG00-RUPTRAZPAGE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-LARGEUR-F                                           00000430
      *        PIC S9(4) COMP.                                          00000440
      *--                                                                       
           02  EG00-LARGEUR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-LONGUEUR-F                                          00000450
      *        PIC S9(4) COMP.                                          00000460
      *--                                                                       
           02  EG00-LONGUEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-DSECTCREE-F                                         00000470
      *        PIC S9(4) COMP.                                          00000480
      *--                                                                       
           02  EG00-DSECTCREE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-TRANSFERE-F                                         00000490
      *        PIC S9(4) COMP.                                          00000500
      *--                                                                       
           02  EG00-TRANSFERE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-DSYST-F                                             00000510
      *        PIC S9(4) COMP.                                          00000520
      *--                                                                       
           02  EG00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EG00-LIBETAT-F                                           00000530
      *        PIC S9(4) COMP.                                          00000540
      *--                                                                       
           02  EG00-LIBETAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000550
                                                                                
