      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * SYKLEDIN **************************************** 09-06-89 *            
      **************************************************************            
      ***************** EDITION D'UN ETAT CICS *********************            
      **************************************************************            
      *************************************************** SYKLEDIN *            
      *                  ------------------                        *            
      *------------------- INTERFACE CICS -------------------------*            
      *                  ------------------                        *            
      *                                                            *            
      * CE SOUS-PROGRAMME GENERALISE REALISE L'INTERFACE D'EDITION *            
      * DES ETATS ENTRE UN PROGRAMME CICS ET L'APPLICATION DE      *            
      * TRAITEMENT DES ETATS SOUS CICS.                            *            
      *                                                            *            
      * LE PROGRAMME CICS APPELANT DOIT ETRE DEVELOPPE :           *            
      *       - SOUS AIDA                                          *            
      *       - EN COBOL 2                                         *            
      *       - AVEC DL1 SOUS LA FORME EXEC DLI                    *            
      *                                                            *            
      * LA ZONE DE WORKING ASSOCIEE A CETTE BRIQUE PROCEDURALE     *            
      * SE TROUVE SOUS COPY MIGC02                                 *            
      *                                                            *            
      * modification pour internationalisation                     *            
      *  appel TETDATI                                             *            
      *                                                            *            
      *                                                            *            
      *                                                            *            
      *------------------------------------------------------------*            
       EJECT                                                                    
      *===============================================================          
      *              TRAITEMENT DE LA PROCEDURE                      *          
      *==================================================== SYKLEDIT *          
       MIGC02  SECTION.                                                         
           MOVE SPACES    TO  CODRUP.                                           
           MOVE SPACES    TO  IG8-ERR.                                          
           IF SAU-CFG  =  ' '  OR                                               
              SAU-PTC  =  ' '  OR                                               
              SAU-IMP  =  ' '                                                   
              MOVE  '*'   TO  CODRUP                                            
           END-IF.                                                              
      *----CONTROLE NUMERO MASQUE PCB EDITION ------------------------          
           IF IG-CTL-PCB  NOT =  '*'  OR  ETAT-PSB  NOT =  'K'                  
              IF IG9-PCB  <  1              OR                                  
                 IG9-PCB  >  9                                                  
                 MOVE 'MIGC02 : IG9-PCB HORS-LIMITES'  TO  MESS                 
                 GO TO  ABANDON-AIDA                                            
              END-IF                                                            
              MOVE  '*'      TO  IG-CTL-PCB                                     
              MOVE  IG9-PCB  TO  NUM-PCB                                        
              MOVE  2        TO  NOMBRE                                         
              MOVE  '*'      TO  CODRUP                                         
           END-IF.                                                              
           IF NUM-PCB  NOT =  IG9-PCB                                           
              MOVE 'MIGC02 : IG9-PCB CHANGE'         TO  MESS                   
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
      *----SYNTAXE CARACTERE TRAITEMENT EXISTANT ---------------------          
           IF IG7-SUP  NOT =  ' '  AND  'D'  AND  'S'  AND  'A'                 
                                   AND  'L'  AND  'F'                           
              MOVE 'MIGC02 : IG7-SUP ERREUR'         TO  MESS                   
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           IF IG7-SUP  NOT =  ' '                                               
              MOVE  '*'  TO  CODRUP                                             
           END-IF.                                                              
       EJECT                                                                    
      * TRAITEMENT DES RUPTURES SUR CLE ETAT ----------------- SYKLEDIT         
      *----RUPTURE SUR NON ETAT-------------------------------                  
           IF IG1-ETA      NOT =  IDT-KEY-NOM                                   
              MOVE SPACES   TO    IDT-KEY-DLI                                   
              PERFORM             IG-NOM-ETAT-0                                 
              PERFORM             DLI-GU-DIGSA-IDT                              
              PERFORM             IG-SAUVEGARDE                                 
              MOVE '*'      TO    CODRUP                                        
              MOVE IG1-ETA  TO    IDT-KEY-NOM                                   
           END-IF.                                                              
           IF SAU-PTC = 'T'                                                     
              THEN PERFORM IG-PTC-TEXTE                                         
              ELSE PERFORM IG-PTC-LASGRA                                        
           END-IF.                                                              
      *----RUPTURE SUR DATE D'EDITION-------------------------                  
           IF IDT-KEY-DAT  NOT =  IG2-DAT                                       
              PERFORM IG-DATE-EDITION                                           
              MOVE '*'      TO    CODRUP                                        
              MOVE IG2-DAT  TO    IDT-KEY-DAT                                   
           END-IF.                                                              
      *----RUPTURE DESTINATION ETAT---------------------------                  
           IF IDT-KEY-DST NOT = IG3-DST                                         
              PERFORM IG-DESTINATION                                            
              IF TOP-CFG = '*'                                                  
                 PERFORM DLI-GU-DIGSA-IDT                                       
              END-IF                                                            
              IF IG3-DST  NOT =  '000000000'                                    
                 PERFORM DLI-GNP-DIGSE                                          
                 IF IG8-ERR  NOT =  SPACES                                      
                    MOVE  ' '  TO   IG7-SUP                                     
                    GO TO FIN-MIGC02                                            
                 END-IF                                                         
              END-IF                                                            
              MOVE '*'      TO  CODRUP                                          
              MOVE IG3-DST  TO  IDT-KEY-DST                                     
           END-IF.                                                              
      *----RUPTURE DOCUMENT ETAT------------------------------                  
           IF IDT-KEY-DOC  NOT =  IG4-DOC                                       
              PERFORM IG-DOCUMENT                                               
              MOVE '*'      TO  CODRUP                                          
              MOVE IG4-DOC  TO  IDT-KEY-DOC                                     
           END-IF.                                                              
       EJECT                                                                    
      * TRAITEMENT EN FONCTION DES RUPTURES------------------- SYKLEDIT         
           IF CODRUP = '*'                                                      
              PERFORM DLI-GU-DIGSA-CFG                                          
              IF IG8-ERR  NOT =  SPACES                                         
                 MOVE  ' '  TO  IG7-SUP                                         
                 GO TO FIN-MIGC02                                               
              END-IF                                                            
              IF CODDLI = 'DLET'                                                
                 MOVE     0      TO  IG-CTR-DLET                                
                 MOVE     'GNP'  TO  FONCTIONX                                  
                 PERFORM  CLEF-DIGFC0                                           
                 PERFORM  DLI-DLET-DIGSC                                        
                 IF IG7-SUP  =  'S'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO  FIN-MIGC02                                           
                 END-IF                                                         
              END-IF                                                            
              IF CODDLI = 'ISRT'                                                
                 IF IG7-SUP  =  'S'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO  FIN-MIGC02                                           
                 END-IF                                                         
                 PERFORM  DLI-ISRT-DIGSA-CFG                                    
              END-IF                                                            
              IF CODDLI = 'REPL'                                                
                 PERFORM  DLI-REPL-DIGSA-CFG                                    
                 IF IG7-SUP  =  'A'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO  FIN-MIGC02                                           
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *----CREATION SEGMENT LIGNE                                               
           PERFORM  DLI-ISRT-DIGSC.                                             
           MOVE  ' '  TO  IG7-SUP.                                              
       FIN-MIGC02.     EXIT.                                                    
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * CONTROLE SYNTAXE NOM ETAT                          *          
      *=========*========================================== SYKLEDIT *          
       IG-NOM-ETAT-0      SECTION.                                              
           IF IG1-ETA1  NOT =  'J'                                              
              MOVE 'MIGC02 : IG1-ETA ERREUR'  TO  MESS                          
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           IF IG1-ETA3   IS NOT NUMERIC  OR                                     
              IG1-ETA3   < '1'           OR                                     
              IG1-ETA3   > '9'                                                  
              MOVE 'MIGC02 : IG1-ETA ERREUR'  TO  MESS                          
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-IG-NOM-ETAT-0.      EXIT.                                            
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * TRAITEMENT SAUVEGARDE IDENTIFICATION               *          
      *=========*========================================== SYKLEDIT *          
       IG-SAUVEGARDE      SECTION.                                              
      *----SAUVEGARDE CONFIGURATION ET TYPE IMPRESSION                          
           MOVE DIGZA0-IDT-CNF  TO  SAU-CFG.                                    
           MOVE DIGZA0-IDT-IMP  TO  SAU-IMP.                                    
      *----PROTOCOLE TYPE TEXTE                                                 
           IF DIGZA0-IDT-PTC   =  ' '  OR  'T'                                  
              MOVE 'T'             TO  SAU-PTC                                  
              MOVE DIGZA0-IDT-COL  TO  SAU-COL                                  
              GO TO  FIN-IG-SAUVEGARDE                                          
           END-IF.                                                              
      *----PROTOCOLE TYPE LASER OU GRAPHIQUE                                    
           IF DIGZA0-IDT-PTC  =  'L'  OR  'G'                                   
              MOVE DIGZA0-IDT-PTC   TO  SAU-PTC                                 
              MOVE DIGZA0-IDT-CMD   TO  SAU-CMD                                 
              MOVE DIGZA0-IDT-DON   TO  SAU-DON                                 
              COMPUTE  SAU-COL =  SAU-CMD + SAU-DON                             
              GO TO  FIN-IG-SAUVEGARDE                                          
           END-IF.                                                              
           MOVE 'MIGC02 : DIGZA0-IDT-PTC ERREUR'  TO  MESS.                     
           GO TO ABANDON-AIDA.                                                  
       FIN-IG-SAUVEGARDE.      EXIT.                                            
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT PROTOCOLES LASER ET GRAPHIQUE           *          
      *---------*------------------------------------------ SYKLEDIT *          
       IG-PTC-TEXTE       SECTION.                                              
           IF IG5-ASA  NOT =  ' '  AND  '1'  AND  '-'  AND                      
                              '0'  AND  '+'                                     
              MOVE 'MIGC02 : IG5-ASA ERREUR TEXTE'  TO  MESS                    
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-IG-PTC-TEXTE.         EXIT.                                          
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT PROTOCOLES LASER ET GRAPHIQUE           *          
      *---------*------------------------------------------ SYKLEDIT *          
       IG-PTC-LASGRA      SECTION.                                              
           IF IG5-ASA  NOT =  '$'  AND  '�'  AND  '&'                           
              MOVE 'MIGC02 : IG5-ASA ERREUR L G'  TO  MESS                      
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           IF IG6-LNG   >  132                                                  
              MOVE 'MIGC02 : IG6-LNG GT 132'      TO  MESS                      
              GO TO ABANDON-AIDA                                                
           END-IF                                                               
      *----TRAITEMENT ZONE DE COMMANDE SEULE                                    
           IF IG5-ASA  = '$'                                                    
              IF IG6-LNG  NOT =  SAU-CMD                                        
                 MOVE 'MIGC02 : IG6-LNG COMMANDE ERREUR'   TO  MESS             
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
              IF IG6-LIG  =  SPACES  OR  LOW-VALUE                              
                 MOVE 'MIGC02 : IG6-LIG COMMANDE VIDE  '   TO  MESS             
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
           END-IF.                                                              
      *----TRAITEMENT ZONE DE DONNEES SEULE                                     
           IF IG5-ASA  = '�'                                                    
              IF IG6-LNG  NOT =  SAU-DON                                        
                 MOVE 'MIGC02 : IG6-LNG DONNEE ERREUR'     TO  MESS             
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
           END-IF.                                                              
      *----TRAITEMENT ZONES DE COMMANDES + DONNEES                              
           IF IG5-ASA  = '&'                                                    
              IF IG6-LNG   NOT =  SAU-COL                                       
                 MOVE 'MIGC02 : IG6-LNG NE SAU-COL'      TO  MESS               
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
              IF IG6-LIG  =  SPACES  OR  LOW-VALUE                              
                 MOVE 'MIGC02 : IG6-LIG CMD + DON VIDE'  TO  MESS               
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
           END-IF.                                                              
       FIN-IG-PTC-LASGRA.        EXIT.                                          
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * CONTROLE DATE EDITION                              *          
      *=========*========================================== SYKLEDIT *          
       IG-DATE-EDITION      SECTION.                                            
           IF SAU-CFG > '4'                                                     
              MOVE '000000'  TO  IG2-DAT                                        
              GO TO FIN-IG-DATE-EDITION                                         
           END-IF.                                                              
           MOVE '7'                       TO  GFDATA.                           
           MOVE IG2-DAT                   TO  GFAMJ-1.                          
           MOVE COMM-DATC-LONG-COMMAREA   TO  LONG-COMMAREA-LINK.               
           MOVE W-CODLANG                 TO GFCODE-LANGUE                      
           MOVE Z-COMMAREA-TETDATC        TO  Z-COMMAREA-LINK.                  
           MOVE 'TETDATI'                 TO  NOM-PROG-LINK.                    
           PERFORM LINK-PROG.                                                   
           MOVE W-CODLANG                 TO GFCODE-LANGUE                      
           MOVE Z-COMMAREA-LINK           TO  Z-COMMAREA-TETDATC.               
           IF GFVDAT NOT = '1'                                                  
              MOVE GF-MESS-ERR            TO  MESS                              
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           IF IG2-DAT > COMM-DATE-AAMMJJ                                        
              MOVE 'MIGC02 : IG2-DAT ERREUR'  TO  MESS                          
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-IG-DATE-EDITION.      EXIT.                                          
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * VALIDATION PARAMETRE DESTINATION                   *          
      *=========*========================================== SYKLEDIT *          
       IG-DESTINATION      SECTION.                                             
      *----DESTINATION NON SIGNIFICATIVE                                        
           IF SAU-CFG = '3'  OR  '4'  OR  '7'  OR  '8'                          
              MOVE '000000000'  TO  IG3-DST                                     
              GO TO FIN-IG-DESTINATION                                          
           END-IF.                                                              
      *----DESTINATION SIGNIFICATIVE                                            
           IF IG3-DST  =  '*********'  OR  '000000000'  OR                      
                          SPACES       OR  LOW-VALUE                            
              MOVE  'MIGC02 : IG3-DST ERREUR'  TO  MESS                         
              GO TO  ABANDON-AIDA                                               
           END-IF.                                                              
       FIN-IG-DESTINATION.      EXIT.                                           
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * VALIDATION PARAMETRE DOCUMENT                      *          
      *=========*========================================== SYKLEDIT *          
       IG-DOCUMENT      SECTION.                                                
      *----DOCUMENT NON SIGNIFICATIF                                            
           IF SAU-CFG = '2'  OR  '4'  OR  '6'  OR  '8'                          
              MOVE '000000000000000'  TO  IG4-DOC                               
              GO TO  FIN-IG-DOCUMENT                                            
           END-IF.                                                              
      *----DOCUMENT SIGNIFICATIF                                                
           IF IG4-DOC = '***************'  OR  '000000000000000'  OR            
                        SPACES             OR  LOW-VALUE                        
              MOVE 'MIGC02 : IG4-DOC ERREUR'  TO  MESS                          
              GO TO  ABANDON-AIDA                                               
           END-IF.                                                              
       FIN-IG-DOCUMENT.          EXIT.                                          
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * RECHERCHE IDENTIFICATION ETAT                      *          
      *=========*========================================== SYKLEDIT *          
       DLI-GU-DIGSA-IDT       SECTION.                                          
           MOVE  '0'  TO  TOP-RCN.                                              
           PERFORM DLI-GU-QUAL-EGAL.                                            
      *----ERREUR TYPE RACINE IDENTIFICATION                                    
           IF DIGZA0-IDT-RCN  NOT =  '0'                                        
              MOVE   'MIGC02 : DIGZA0-IDT-RCN ERREUR'  TO  MESS                 
              GO TO  ABANDON-AIDA                                               
           END-IF.                                                              
       FIN-DLI-GU-DIGSA-IDT.       EXIT.                                        
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * RECHERCHE RACINE CONFIGURATION ETAT                *          
      *=========*========================================== SYKLEDIT *          
       DLI-GU-DIGSA-CFG      SECTION.                                           
           MOVE SPACES  TO  CODDLI.                                             
           MOVE '*'     TO  TOP-CFG.                                            
           MOVE '*'     TO  TOP-RCN.                                            
           PERFORM DLI-GU-QUAL-EGAL.                                            
           IF NON-TROUVE                                                        
              MOVE 'ISRT'  TO  CODDLI                                           
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----ERREUR TYPE DE RACINE CONFIGURATION                                  
           IF DIGZA1-CFG-RCN  NOT =  '1'                                        
              MOVE 'MIGC02 : DIGZA1-CFG-RCN ERREUR' TO MESS                     
              GO TO   ABANDON-AIDA                                              
           END-IF.                                                              
      *----ERREUR COHERENCE NUMERO DE CONFIGURATION                             
           IF DIGZA1-CFG-CNF  NOT =  SAU-CFG                                    
              MOVE 'MIGC02 : DIGZA1-CFG-CNF ERREUR' TO MESS                     
              GO TO   ABANDON-AIDA                                              
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE TEXTE                                     
           IF DIGZA1-CFG-PTC  =  ' '  OR  'T'                                   
              IF SAU-PTC  NOT =  'T'                                            
                 MOVE 'MIGC02 : DIGZA1-CFG-PTC T ERREUR' TO MESS                
                 GO TO   ABANDON-AIDA                                           
              END-IF                                                            
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE LASER                                     
           IF DIGZA1-CFG-PTC  =  'L'                                            
              IF SAU-PTC  NOT =  'L'                                            
                 MOVE 'MIGC02 : DIGZA1-CFG-PTC L ERREUR' TO MESS                
                 GO TO   ABANDON-AIDA                                           
              END-IF                                                            
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE GRAPHIQUE                                 
           IF DIGZA1-CFG-PTC  =  'G'                                            
              IF SAU-PTC  NOT =  'G'                                            
                 MOVE 'MIGC02 : DIGZA1-CFG-PTC G ERREUR' TO MESS                
                 GO TO   ABANDON-AIDA                                           
              END-IF                                                            
           END-IF.                                                              
       EJECT                                                                    
      *----DEMANDE DE DLET DE LA CONFIGURATION                                  
           IF IG7-SUP  =  'D'  OR  'S'                                          
              MOVE  'DLET'  TO  CODDLI                                          
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----DEMANDE D'ANNULATION DE LA CONFIGURATION                             
           IF IG7-SUP  =  'A'                                                   
              PERFORM CONFIG-ANNU                                               
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----CONFIGURATION TOPEE ANNULEE                                          
           IF DIGZA1-CFG-SUP  NOT = '000000'                                    
              MOVE  'ANNU'  TO  IG8-ERR                                         
              GO TO  FIN-DLI-GU-DIGSA-CFG                                       
           END-IF.                                                              
      *----CONFIGURATION TOPEE EN REPRISE IMPRESSION                            
           IF DIGZA1-CFG-REP  NOT =  '000000'                                   
              PERFORM  CONFIG-REPR                                              
              GO TO    FIN-DLI-GU-DIGSA-CFG                                     
           END-IF.                                                              
      *----CONFIGURATION TOPEE IMPRIMEE                                         
           IF DIGZA1-CFG-IMP  NOT =  '000000'                                   
              PERFORM  CONFIG-IMPR                                              
              GO TO    FIN-DLI-GU-DIGSA-CFG                                     
           END-IF.                                                              
           PERFORM  CONFIG-EDIT.                                                
       FIN-DLI-GU-DIGSA-CFG.      EXIT.                                         
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE REPRISE             *          
      *---------*------------------------------------------ SYKLEDIT *          
        CONFIG-ANNU             SECTION.                                        
           MOVE  COMM-DATE-AAMMJJ  TO  DIGZA1-CFG-SUP.                          
           MOVE  'REPL'            TO  CODDLI.                                  
        FIN-CONFIG-ANNU.        EXIT.                                           
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE REPRISE             *          
      *---------*------------------------------------------ SYKLEDIT *          
        CONFIG-REPR             SECTION.                                        
           IF SAU-IMP  =  'U'                                                   
              MOVE  'UNIQ'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-REPR                                             
           END-IF.                                                              
           IF IG7-SUP  NOT =  'F'                                               
              MOVE  'REPR'  TO  IG8-ERR                                         
              GO TO  FIN-CONFIG-REPR                                            
           END-IF.                                                              
           MOVE  '000000'  TO  DIGZA1-CFG-REP.                                  
           MOVE  '000000'  TO  DIGZA1-CFG-IMP.                                  
           MOVE  'REPL'    TO  CODDLI.                                          
           IF SAU-CFG  <  '5'                                                   
              GO TO  FIN-CONFIG-REPR                                            
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  COMM-DATE-AAMMJJ                           
              MOVE  COMM-DATE-AAMMJJ  TO  DIGZA1-CFG-EDI                        
              MOVE  'REPL'            TO  CODDLI                                
           END-IF.                                                              
        FIN-CONFIG-REPR.        EXIT.                                           
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE IMPRIMEE            *          
      *---------*------------------------------------------ SYKLEDIT *          
        CONFIG-IMPR             SECTION.                                        
           IF SAU-IMP  =  'U'                                                   
              MOVE  'UNIQ'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-IMPR                                             
           END-IF.                                                              
           IF IG7-SUP  NOT =  'F'                                               
              MOVE  'IMPR'  TO  IG8-ERR                                         
              GO TO  FIN-CONFIG-IMPR                                            
           END-IF.                                                              
           MOVE  '000000'  TO  DIGZA1-CFG-IMP.                                  
           MOVE  'REPL'    TO  CODDLI.                                          
           IF SAU-CFG  <  '5'                                                   
              GO TO  FIN-CONFIG-IMPR                                            
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  COMM-DATE-AAMMJJ                           
              MOVE  COMM-DATE-AAMMJJ  TO  DIGZA1-CFG-EDI                        
              MOVE  'REPL'            TO  CODDLI                                
           END-IF.                                                              
        FIN-CONFIG-IMPR.        EXIT.                                           
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE EDITEE              *          
      *---------*------------------------------------------ SYKLEDIT *          
        CONFIG-EDIT             SECTION.                                        
           IF IG7-SUP  NOT =  'L'                                               
              MOVE  'EDIT'  TO IG8-ERR                                          
              GO TO  FIN-CONFIG-EDIT                                            
           END-IF.                                                              
           IF SAU-CFG  <  '5'                                                   
              GO TO  FIN-CONFIG-EDIT                                            
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  COMM-DATE-AAMMJJ                           
              MOVE  COMM-DATE-AAMMJJ  TO  DIGZA1-CFG-EDI                        
              MOVE  'REPL'            TO  CODDLI                                
           END-IF.                                                              
        FIN-CONFIG-EDIT.        EXIT.                                           
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * RECHERCHE EXISTENCE DESTINATION                    *          
      *=========*========================================== SYKLEDIT *          
       DLI-GNP-DIGSE        SECTION.                                            
      *----GNP QUAL EGAL* * * * ( EXEC DLI ) * * * * * * * * * * * * *          
           MOVE IG3-DST  TO  DIGSE-DIGFE0-IGDESTIN.                             
           MOVE 'GNP'    TO  FONCTIONX.                                         
           PERFORM CLEF-DIGFE0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-33      
      * EXEC DLI GET NEXT IN PARENT  USING PCB(NUM-PCB)                          
      *                              SEGMENT(DIGSE)                              
      *                              WHERE(DIGFE0 = DIGSE-DIGFE0)                
      *                              FIELDLENGTH(KEY-LONG-1)                     
      *                              SEGLENGTH(SEG-LONG-1)                       
      *                              INTO(DIGSE)                                 
      *                 END-EXEC.                                                
                MOVE DIGSE-DIGFE0 TO H-DIGFE0
           EXEC SQL
               SELECT DIGFE0, COL03001
               INTO :H-DIGFE0, :H-COL03001
               FROM RTIGSE
               WHERE DIGFA0 = :H-DIGFA0
               AND  DIGFE0 = :H-DIGFE0
           END-EXEC
           IF SQLCODE = 0
              MOVE H-DIGSE TO DIGSE
           END-IF
           PERFORM TRT-SQLCODE-DL1
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�0DLI     00569   QM' TO DFHEIV0                  
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSE' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     MOVE ' = ' TO DFHC0030                                        
      *dfhei*     MOVE 'DIGFE0  ' TO DFHC0081                                   
      *dfhei*     MOVE KEY-LONG-1 TO DFHB0022                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021 DFHDUMMY DFHDUMMY DFHDUMMY DFHC0030 DFHC0081         
      *dfhei*     DIGSE-DIGFE0 DFHB0022.                                        
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE  'DEST'  TO  IG8-ERR                                         
           END-IF.                                                              
       FIN-DLI-GNP-DIGSE.        EXIT.                                          
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * DLET SEGMENT DIGSC                                 *          
      *=========*========================================== SYKLEDIT *          
        DLI-DLET-DIGSC          SECTION.                                        
      *----GNP NON QUAL * * * * ( EXEC DLI ) * * * * * * * * * * * * *          
           MOVE  'GNP'  TO  FONCTIONX.                                          
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-34      
      * EXEC DLI GET NEXT IN PARENT  USING PCB(NUM-PCB)                          
      *                              SEGMENT(DIGSC)                              
      *                              SEGLENGTH(SEG-LONG-1)                       
      *                              INTO(DIGSC)                                 
      *                 END-EXEC.                                                
                 IF NOT CURS-DIGSC-OPEN
                     PERFORM OPEN-CURS-DIGSC
                 END-IF
                 IF CURS-DIGSC-OPEN
                   PERFORM FETCH-CURS-DIGSC
                   PERFORM TRT-SQLCODE-DL1
                   IF SQLCODE = 0
                      MOVE H-DIGSC TO DIGSC
                   ELSE
                      PERFORM CLOSE-CURS-DIGSC
                   END-IF
                 END-IF
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00594   Qd' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSC' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
             MOVE    '1'  TO  TOP-RCN                                           
              PERFORM      DLI-GU-QUAL-EGAL                                     
              PERFORM      DLI-DLET-DIGSA-CFG                                   
              PERFORM      DLI-TERM-BASE                                        
              GO TO        FIN-DLI-DLET-DIGSC                                   
           END-IF.                                                              
       EJECT                                                                    
      *----DLET  * * * * * * * * ( EXEC DLI ) * * * * * * * * * * * * *         
           MOVE  'DLET'  TO  FONCTIONX.                                         
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-35      
      * EXEC DLI DELETE  USING PCB(NUM-PCB)                                      
      *                  SEGMENT(DIGSC)                                          
      *                  SEGLENGTH(SEG-LONG-1)                                   
      *                  FROM(DIGSC)                                             
      *    END-EXEC.                                                             
             EXEC SQL
                   DELETE FROM RTIGSC
                      WHERE DIGFA0 = :H-DIGFA0
                          AND DIGFC0 = :H-DIGFC0
                          AND SM_DIGSC = :H-SM-DIGSC
             END-EXEC
             PERFORM TRT-SQLCODE-DL1
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00613   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSC' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE 'MIGC02 : ERREUR DLET DIGSC'  TO  MESS                       
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       EJECT                                                                    
           ADD  1  TO  IG-CTR-DLET.                                             
           IF  IG-CTR-DLET  <  1000                                             
               GO TO  DLI-DLET-DIGSC                                            
           END-IF.                                                              
           PERFORM  DLI-TERM-BASE.                                              
           MOVE   0   TO  IG-CTR-DLET.                                          
           MOVE  '1'  TO  TOP-RCN.                                              
           PERFORM  DLI-GU-QUAL-EGAL.                                           
           PERFORM  CLEF-DIGFC0.                                                
           GO TO    DLI-DLET-DIGSC.                                             
       FIN-DLI-DLET-DIGSC.          EXIT.                                       
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * DLET RACINE DIGSA CONFIGURATION ETAT               *          
      *---------*------------------------------------------ SYKLEDIT *          
        DLI-DLET-DIGSA-CFG      SECTION.                                        
           MOVE 'DLET'   TO   FONCTIONX.                                        
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-36      
      * EXEC DLI DELETE  USING PCB(NUM-PCB)                                      
      *                  SEGMENT(DIGSA)                                          
      *                  SEGLENGTH(SEG-LONG-1)                                   
      *                  FROM(DIGSA)                                             
      *    END-EXEC.                                                             
             EXEC SQL
                   DELETE FROM RTIGSA
                      WHERE DIGFA0 = :H-DIGFA0
             END-EXEC
             PERFORM TRT-SQLCODE-DL1
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00654   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE 'MIGC02 : ERREUR DLET DIGSA-CFG' TO MESS                     
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           MOVE 'ISRT'  TO  CODDLI.                                             
       FIN-DLI-DLET-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * ISRT RACINE DIGSA CONFIGURATION ETAT               *          
      *=========*========================================== SYKLEDIT *          
       DLI-ISRT-DIGSA-CFG      SECTION.                                         
      *----CLE ACCES ETAT                                                       
           MOVE  IDT-KEY-NOM              TO  DIGFA1-CFG-NOM.                   
           MOVE  IDT-KEY-DAT              TO  DIGFA2-CFG-DAT.                   
           MOVE  IDT-KEY-DST              TO  DIGFA3-CFG-DST.                   
           MOVE  IDT-KEY-DOC              TO  DIGFA4-CFG-DOC.                   
      *----CARACTERISTIQUES CONFIGURATION                                       
           MOVE '1'                       TO  DIGZA1-CFG-RCN.                   
           MOVE SAU-CFG                   TO  DIGZA1-CFG-CNF.                   
      *----STATUT ETAT                                                          
           IF SAU-CFG > '4'                                                     
              THEN MOVE COMM-DATE-AAMMJJ  TO  DIGZA1-CFG-EDI                    
              ELSE MOVE '000000'          TO  DIGZA1-CFG-EDI                    
           END-IF.                                                              
           MOVE '000000'                  TO  DIGZA1-CFG-SUP.                   
           MOVE '000000'                  TO  DIGZA1-CFG-IMP.                   
           MOVE '000000'                  TO  DIGZA1-CFG-REP.                   
           MOVE '000000'                  TO  DIGZA1-CFG-CNT.                   
           MOVE '000000'                  TO  DIGZA1-CFG-PUR.                   
      *----PROTOCOLE IMPRESSION                                                 
           MOVE SAU-PTC                   TO  DIGZA1-CFG-PTC.                   
      *----FILLER                                                               
           MOVE SPACES                    TO  DIGZA1-CFG-FIL.                   
        EJECT                                                                   
      *----ISRT QUAL * * * * * * ( EXEC DLI )  * * * * * * * * * * * *          
           MOVE 'ISRT'        TO   FONCTIONX.                                   
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-37      
      * EXEC DLI INSERT  USING PCB(NUM-PCB)                                      
      *                  SEGMENT(DIGSA)                                          
      *                  SEGLENGTH(SEG-LONG-1)                                   
      *                  FROM(DIGSA)                                             
      *    END-EXEC.                                                             
                MOVE DIGSA TO H-DIGSA
                EXEC SQL
                     INSERT INTO RTIGSA
                         (DIGFA0, COL01001)
                  VALUES (:H-DIGFA0, :H-COL01001)
                END-EXEC
                PERFORM TRT-SQLCODE-DL1
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00712   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF EXISTE-DEJA                                                       
              MOVE 'MIGC02 : CONFIGURATION ETAT EXISTE DEJA' TO MESS            
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-DLI-ISRT-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * REPL RACINE DIGSA CONFIGURATION ETAT               *          
      *=========*========================================== SYKLEDIT *          
       DLI-REPL-DIGSA-CFG      SECTION.                                         
           MOVE  'REPL'  TO  FONCTIONX.                                         
           PERFORM  CLEF-DIGFA0.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-38     
      * EXEC DLI REPLACE  USING PCB(NUM-PCB)                                     
      *                   SEGMENT(DIGSA)                                         
      *                   SEGLENGTH(SEG-LONG-1)                                  
      *                   FROM(DIGSA)                                            
      *     END-EXEC.                                                            
             MOVE DIGSA TO H-DIGSA
             EXEC SQL
                  UPDATE RTIGSA
                  SET COL01001 = :H-COL01001
                  WHERE DIGFA0 = :H-DIGFA0
             END-EXEC
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '��DLI     00737   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
             MOVE 'MIGC02 : ERREUR REPL DIGSA-CFG' TO MESS                      
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-DLI-REPL-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      *=========*====================================================*          
      * SECTION * ISRT SEGMENT LIGNE DIGSC                           *          
      *=========*========================================== SYKLEDIT *          
       DLI-ISRT-DIGSC                 SECTION.                                  
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS ASKTIME                                                        
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '00758   ' TO DFHEIV0                                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0.                                  
      *dfhei*} decommente EXEC                                                  
           MOVE EIBTIME   TO  EIB-TIME.                                         
           MOVE EIBTASKN  TO  EIB-TSK.                                          
       EJECT                                                                    
      * INITIALISATION DSECT SEGMENT DIGSC =============================        
      *----LONGUEUR DU SEGMENT                                                  
           IF SAU-PTC = 'T'                                                     
              COMPUTE DIGZC0-PFX-LEN     =  SAU-COL + 26                        
           END-IF.                                                              
           IF SAU-PTC = 'L'  OR  'G'                                            
              IF IG5-ASA  =  '$'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-CMD + 26                        
              END-IF                                                            
              IF IG5-ASA  =  '�'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-DON + 26                        
              END-IF                                                            
              IF IG5-ASA  =  '&'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-COL + 26                        
              END-IF                                                            
           END-IF.                                                              
           IF DIGZC0-PFX-LEN < 27                                               
              MOVE 'MIGC02 : ERREUR LNG DIGSC LT 27'  TO MESS                   
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
           IF DIGZC0-PFX-LEN > 158                                              
              MOVE 'MIGC02 : ERREUR LNG DIGSC GT 158' TO MESS                   
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       EJECT                                                                    
      *----RENSEIGNEMENT CLE NON UNIQUE - DATE ET HEURE                         
           MOVE COMM-DATE-AAMMJJ  TO  DIGFC1-PFX-DAT.                           
           MOVE EIB-TIME          TO  DIGFC2-PFX-HEU.                           
      *----IDENTIFICATION CICS - NUMERO DE TACHE ET CODE TERMINAL               
           MOVE EIB-TSK         TO  DIGFC3-PFX-TAS.                             
           MOVE EIBTRMID        TO  DIGFC4-PFX-TRM.                             
      *----MISE EN FORME DE LA LIGNE                                            
           MOVE IG5-ASA         TO  DIGZC0-MEP-ASA.                             
           MOVE IG6-LIG         TO  DIGZC0-EDI-LIG.                             
      * ISRT DLI =======================================================        
           MOVE 'ISRT'          TO  FONCTIONX.                                  
           PERFORM CLEF-DIGFC0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-39
      * EXEC DLI INSERT  USING PCB(NUM-PCB)                                      
      *                  SEGMENT(DIGSC)                                          
      *                  SEGLENGTH(SEG-LONG-1)                                   
      *                  FROM(DIGSC)                                             
      *    END-EXEC.                                                             
                MOVE DIGSC TO H-DIGSC
                EXEC SQL
                     INSERT INTO RTIGSC
                         (DIGFA0, DIGFC0, SM_DIGSC, COL06001, COL06002)
                  VALUES (:H-DIGFA0, :H-DIGFC0, CURRENT TIMESTAMP,
                          :H-COL06001, :H-COL06002)
                END-EXEC
                PERFORM TRT-SQLCODE-DL1
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00811   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSC' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF EXISTE-DEJA                                                       
              MOVE 'MIGC02 : ERREUR ISRT DIGSC'  TO  MESS                       
              GO TO ABANDON-AIDA                                                
           END-IF.                                                              
       FIN-DLI-ISRT-DIGSC.              EXIT.                                   
       EJECT                                                                    
      *==============================================================*          
      * SECTION * ACCES SEGMENT DIGSA IDT OU CFG                     *          
      *==================================================== SYKLEDIT *          
       DLI-GU-QUAL-EGAL       SECTION.                                          
      *----RENSEIGNEMENT CLE IDENTIFICATION                                     
           IF TOP-RCN  = '0'                                                    
              MOVE  IG1-ETA            TO  DIGSA-DIGFA0-IGNOMETA                
              MOVE  '******'           TO  DIGSA-DIGFA0-IGDATEDI                
              MOVE  '*********'        TO  DIGSA-DIGFA0-IGDESTIN                
              MOVE  '***************'  TO  DIGSA-DIGFA0-IGDOCUME                
           END-IF.                                                              
      *----RENSEIGNEMENT CLE CONFIGURATION                                      
           IF TOP-RCN  = '1'  OR  '*'                                           
              MOVE  IDT-KEY-NOM        TO  DIGSA-DIGFA0-IGNOMETA                
              MOVE  IDT-KEY-DAT        TO  DIGSA-DIGFA0-IGDATEDI                
              MOVE  IDT-KEY-DST        TO  DIGSA-DIGFA0-IGDESTIN                
              MOVE  IDT-KEY-DOC        TO  DIGSA-DIGFA0-IGDOCUME                
           END-IF.                                                              
      *----GU QUAL EGAL * * * * ( EXEC DLI )  * * * * * * * * * * * *           
           MOVE 'GU'     TO  FONCTIONX.                                         
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-40      
      * EXEC DLI GET UNIQUE  USING PCB(NUM-PCB)                                  
      *                      SEGMENT(DIGSA)                                      
      *                      WHERE(DIGFA0 = DIGSA-DIGFA0)                        
      *                      FIELDLENGTH(KEY-LONG-1)                             
      *                      SEGLENGTH(SEG-LONG-1)                               
      *                      INTO(DIGSA)                                         
      *          END-EXEC.                                                       
             MOVE DIGSA-DIGFA0 TO H-DIGFA0
             EXEC SQL
             SELECT DIGFA0, COL01001
             INTO :H-DIGFA0, :H-COL01001
             FROM RTIGSA
             WHERE DIGFA0 = :H-DIGFA0
             END-EXEC
             PERFORM TRT-SQLCODE-DL1
             IF SQLCODE = 0
                  MOVE H-DIGSA TO DIGSA
             END-IF
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '��0DLI     00853   �M' TO DFHEIV0                  
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     MOVE ' = ' TO DFHC0030                                        
      *dfhei*     MOVE 'DIGFA0  ' TO DFHC0081                                   
      *dfhei*     MOVE KEY-LONG-1 TO DFHB0022                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021 DFHDUMMY DFHDUMMY DFHDUMMY DFHC0030 DFHC0081         
      *dfhei*     DIGSA-DIGFA0 DFHB0022.                                        
           PERFORM TEST-CODE-RETOUR.                                            
           IF TOP-RCN  = '*'                                                    
              GO TO FIN-DLI-GU-QUAL-EGAL                                        
           END-IF.                                                              
           IF NON-TROUVE                                                        
              IF TOP-RCN  = '0'                                                 
                 MOVE 'MIGC02 : DIGSA-IDT NON TROUVE' TO MESS                   
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
              IF TOP-RCN  = '1'                                                 
                 MOVE 'MIGC02 : DIGSA-CFG NON TROUVE' TO MESS                   
                 GO TO ABANDON-AIDA                                             
              END-IF                                                            
           END-IF.                                                              
       FIN-DLI-GU-QUAL-EGAL.      EXIT.                                         
       EJECT                                                                    
      *==============================================================*          
      * SECTION * DLI TERM FERMETURE DES BASES                       *          
      *==================================================== SYKLEDIT *          
       DLI-TERM-BASE          SECTION.                                          
           MOVE  '*'  TO  ETAT-PSB.                                             
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-41      
      * EXEC DLI TERM                                                            
      * END-EXEC.                                                                
           EXEC SQL COMMIT
           END-EXEC.
              SET CURS-DIGSA-CLOSE TO TRUE.
              SET CURS-DIGSC-CLOSE TO TRUE.
              MOVE '  ' TO DIBSTAT.
      *} Post-Translation
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00888   �' TO DFHEIV0                      
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB.                          
      *dfhei*} decommente EXEC                                                  
       FIN-DLI-TERM-BASE.     EXIT.                                             
      * SYKLEDIT ***************************************************            
      **************************************************************            
      ************* FIN EDITION D'UN ETAT CICS *********************            
      **************************************************************            
      *************************************************** SYKLEDIT *            
       EJECT                                                                    
                                                                                
                                                                                
