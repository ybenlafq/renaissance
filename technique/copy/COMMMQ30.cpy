      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : EXPLOITATION MQ SERIES                           *        
      *  TRANSACTION: MQ30                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION MQ30                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                        -----   *        
      * COM-MQ30-LONG-COMMAREA A UNE VALUE DE                   4096 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      * MODIF DSA056 12/06/03 APPEL TMQ37 SI 'B' OU 'E'                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-MQ30-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-MQ30-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS           PIC 9(02).                           
              05 COMM-DATE-SEMAA           PIC 9(02).                           
              05 COMM-DATE-SEMNU           PIC 9(02).                           
           02 COMM-DATE-FILLER          PIC X(07).                              
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      ******************************************************************        
      *--- ZONES RESERVEES APPLICATIVES DE LA TRANSACTION MQ30 - 3724 C*        
      ******************************************************************        
      *                                                                         
           02 COMM-MQ30-APPLI.                                                  
      *       ZONES PAGINATION MQ31           -----------------    4 C*         
              05 COMM-MQ31-PAGE            PIC 9(2) VALUE 0.                    
              05 COMM-MQ31-NBPAGES         PIC 9(2) VALUE 0.                    
      *       ZONES APPLICATIVES MQ31         -----------------   15 C*         
              05 COMM-MQ31-NSOC            PIC X(3).                            
              05 COMM-MQ31-WREM            PIC X.                               
              05 COMM-MQ31-NDATE           PIC X(8).                            
              05 COMM-MQ31-CFONC           PIC X(3).                            
      *       PARAMETRES MQ MQ31              -----------------   48 C*         
              05 COMM-MQ31-QALIAS          PIC X(24).                           
              05 COMM-MQ31-MSGID           PIC X(24).                           
      *       ZONES PAGINATION MQ32           -----------------    4 C*         
              05 COMM-MQ32-PAGE            PIC 9(2) VALUE 0.                    
              05 COMM-MQ32-NBPAGES         PIC 9(2) VALUE 0.                    
      *       ZONES APPLICATIVES MQ32         -----------------   14 C*         
              05 COMM-MQ32-NSOC            PIC X(3).                            
              05 COMM-MQ32-NDATE           PIC X(8).                            
              05 COMM-MQ32-CFONC           PIC X(3).                            
      *       PARAMETRES MQ MQ32              -----------------   99 C*         
              05 COMM-MQ32-QALIAS          PIC X(24).                           
              05 COMM-MQ32-MSGID           PIC X(24).                           
              05 COMM-MQ32-QALIASL         PIC X(24).                           
              05 COMM-MQ32-MSGIDL          PIC X(24).                           
              05 COMM-MQ32-MOIS            PIC X(03).                           
      *       ZONES PAGINATION MQ34           -----------------    4 C*         
              05 COMM-MQ34-PAGE            PIC 9(3) VALUE 0.                    
              05 COMM-MQ34-NBPAGES         PIC 9(3) VALUE 0.                    
      *       ZONES APPLICATIVES MQ34         -----------------  334 C*         
              05 COMM-MQ34-TRANS           PIC X(4).                            
              05 COMM-MQ34-NSOC            PIC X(3).                            
              05 COMM-MQ34-NLIEU           PIC X(3).                            
              05 COMM-MQ34-QUEUE           PIC X(24).                           
              05 COMM-MQ34-DATE            PIC X(08).                           
              05 COMM-MQ34-HEURE           PIC X(04).                           
              05 COMM-MQ34-MSGID           PIC X(24) OCCURS 12.                 
              05 COMM-MQ34-CFONC           PIC X(3).                            
              05 COMM-MQ34-CORRELID        PIC X(24) OCCURS 12.                 
              05 COMM-MQ34-TRAIT-SPE       PIC X.                               
      *       ZONES PAGINATION MQ35           -----------------    4 C*         
              05 COMM-MQ35-PAGE            PIC 9(2) VALUE 0.                    
              05 COMM-MQ35-NBPAGES         PIC 9(2) VALUE 0.                    
      *       ZONES APPLICATIVES MQ35         -----------------   54 C*         
              05 COMM-MQ35-NSOC            PIC X(3).                            
              05 COMM-MQ35-QUEUE           PIC X(24).                           
              05 COMM-MQ35-MSGID           PIC X(24).                           
              05 COMM-MQ35-CFONC           PIC X(3).                            
      *       ZONES PAGINATION MQ36           -----------------    4 C*         
              05 COMM-MQ36-PAGE            PIC 9(2) VALUE 0.                    
              05 COMM-MQ36-NBPAGES         PIC 9(2) VALUE 0.                    
      *       ZONES APPLICATIVES MQ36         -----------------   96 C*         
              05 COMM-MQ36-QMGR            PIC X(24).                           
              05 COMM-MQ36-QUEUE           PIC X(24).                           
              05 COMM-MQ36-QUEUED          PIC X(24).                           
              05 COMM-MQ36-MSGID           PIC X(24).                           
              05 COMM-MQ36-CORREL          PIC X(24).                           
              05 COMM-MQ36-DATE            PIC X(08).                           
              05 COMM-MQ36-HEURE           PIC X(04).                           
              05 COMM-MQ36-TAILLE          PIC X(05).                           
              05 COMM-MQ36-PRIOR           PIC X(02).                           
              05 COMM-MQ36-LIGNE1          PIC X(78).                           
              05 COMM-MQ36-LIGNE2          PIC X(78).                           
              05 COMM-MQ36-LIGNE3          PIC X(78).                           
              05 COMM-MQ36-LIGNE4          PIC X(78).                           
              05 COMM-MQ36-LIGNE5          PIC X(78).                           
              05 COMM-MQ36-LIGNE6          PIC X(78).                           
              05 COMM-MQ36-LIGNE7          PIC X(78).                           
              05 COMM-MQ36-MODIF           PIC X(01).                           
      *       ZONES PAGINATION MQ33           -----------------    4 C*         
              05 COMM-MQ33-PAGE            PIC 9(2) VALUE 0.                    
              05 COMM-MQ33-NBPAGES         PIC 9(2) VALUE 0.                    
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 3276 C*         
      *       -58 MQ35          ------------------------------- 3218 C*         
      *      -690 MQ36          ------------------------------- 2528 C*         
      *      -366 MQ34          ------------------------------- 2162 C*         
      *      -4   MQ33          ------------------------------- 2158 C*         
      *      -1   MQ30          ------------------------------- 2157 C*         
      *      -144 MQ37          ------------------------------- 2012 C*         
      ******************************************************************        
              05 COMM-MQ30-WENTETE           PIC X.                             
                 88 COMM-MQ30-AVEC-ENTETE  VALUE 'O'.                           
                 88 COMM-MQ30-SANS-ENTETE  VALUE 'N'.                           
              05 COMM-MQ37-LECTURE VALUE '1' PIC X.                             
                 88 COMM-MQ37-EDIT         VALUE '0'.                           
                 88 COMM-MQ37-BROWSE       VALUE '1'.                           
      *       ZONES PAGINATION MQ36           -----------------    4 C*         
              05 COMM-MQ37-PAGE            PIC 9(3) VALUE 0.                    
              05 COMM-MQ37-NBPAGES         PIC 9(3) VALUE 0.                    
      *       ZONES APPLICATIVES MQ36         -----------------   96 C*         
              05 COMM-MQ37-QMGR            PIC X(24).                           
              05 COMM-MQ37-QUEUE           PIC X(24).                           
              05 COMM-MQ37-QUEUED          PIC X(24).                           
              05 COMM-MQ37-MSGID           PIC X(24).                           
              05 COMM-MQ37-CORREL          PIC X(24).                           
              05 COMM-MQ37-DATE            PIC X(08).                           
              05 COMM-MQ37-HEURE           PIC X(04).                           
              05 COMM-MQ37-TAILLE          PIC X(05).                           
              05 COMM-MQ37-PRIOR           PIC X(02).                           
              05 COMM-MQ37-MODIF           PIC X(01).                           
      *       ZONES INTERNATIONAL             -----------------    4 C*         
              05 COMM-MQ30-CODLANG         PIC X(2).                            
              05 COMM-MQ30-CODPIC          PIC X(2).                            
      *       ZONES PAGINATION MQ38           -----------------    6 C*         
              05 COMM-MQ38-PAGE            PIC 9(4) VALUE 0.                    
              05 COMM-MQ38-NBPAGES         PIC 9(4) VALUE 0.                    
      *       ZONES APPLICATIVES MQ38         -----------------  679 C*         
              05 COMM-MQ38-TRANS           PIC X(4).                            
              05 COMM-MQ38-QUEUE           PIC X(24).                           
              05 COMM-MQ38-DATE            PIC X(08).                           
              05 COMM-MQ38-HEURE           PIC X(04).                           
              05 COMM-MQ38-COPCO           PIC X(03).                           
              05 COMM-MQ38-IDSI            PIC X(10).                           
              05 COMM-MQ38-CLE             PIC X(45).                           
              05 COMM-MQ38-MSGIDH          PIC X(24).                           
              05 COMM-MQ38-CORRELIDH       PIC X(24).                           
              05 COMM-MQ38-MSGID           PIC X(24) OCCURS 12.                 
              05 COMM-MQ38-CORRELID        PIC X(24) OCCURS 12.                 
              05 COMM-MQ38-TRAIT-SPE       PIC X.                               
      *       ZONES PAGINATION MQ39           -----------------    6 C*         
              05 COMM-MQ39-PAGE            PIC 9(3) VALUE 0.                    
              05 COMM-MQ39-NBPAGES         PIC 9(3) VALUE 0.                    
      *       ZONES APPLICATIVES MQ39         -----------------  230 C*         
              05 COMM-MQ39-LECTURE VALUE '1' PIC X.                             
                 88 COMM-MQ39-EDIT         VALUE '0'.                           
                 88 COMM-MQ39-BROWSE       VALUE '1'.                           
              05 COMM-MQ39-QMGR            PIC X(24).                           
              05 COMM-MQ39-QUEUED          PIC X(24).                           
              05 COMM-MQ39-QUEUE           PIC X(24).                           
              05 COMM-MQ39-MSGID           PIC X(24).                           
              05 COMM-MQ39-CORREL          PIC X(24).                           
              05 COMM-MQ39-DATE            PIC X(08).                           
              05 COMM-MQ39-HEURE           PIC X(04).                           
              05 COMM-MQ39-TAILLE          PIC X(05).                           
              05 COMM-MQ39-PRIOR           PIC X(02).                           
              05 COMM-MQ39-COPCO           PIC X(03).                           
              05 COMM-MQ39-IDSI            PIC X(10).                           
              05 COMM-MQ39-CLE             PIC X(45).                           
              05 COMM-MQ39-LISTPGR         PIC X(28).                           
              05 COMM-MQ39-MODIF           PIC X(01).                           
              05 COMM-MQ30-FILLER          PIC X(1040).                         
                                                                                
