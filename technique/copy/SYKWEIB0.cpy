      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TEST DU CODE-RETOUR CICS                                       
      * ATTENTION : CE COPYY COBOL EST FARCI DE VALUES EN BINAIRE....            
      *****************************************************************         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  EIB-RCODE.                                                       
      *                                                                         
             10  FILLER                PIC X    VALUE LOW-VALUE.                
      *                                      X'00'                              
               88  EIB-NORMAL          VALUE LOW-VALUE.                         
      *                                      X'81'                              
               88  EIB-NOTFND          VALUE 'a'.                               
      *                                      X'0F'                              
               88  EIB-ENDFILE         VALUE ''.                               
      *                                      X'04'                              
               88  EIB-MAPFAIL         VALUE '�'.                               
      *                                      X'01'                              
               88  EIB-ENDDATA         VALUE ''.                               
      *                                      X'02'                              
               88  EIB-QIDERR          VALUE ''.                               
      *                                      X'01'                              
               88  EIB-QZERO           VALUE ''.                               
      *                                      X'01'                              
               88  EIB-ITEMERR         VALUE ''.                               
      *                                      X'82'                              
               88  EIB-DUPREC          VALUE 'b'.                               
      *                                      X'84'                              
               88  EIB-DUPKEY          VALUE 'd'.                               
      *                                                                         
             10  FILLER                PIC X(5) VALUE LOW-VALUE.                
       EJECT                                                                    
                                                                                
                                                                                
