      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ HOST --> LOCAL                              *         
      * TRAITEMENTS MQ - COTE HOST                                    *         
      *****************************************************************         
           10 WS-MQ20-MESSAGE REDEFINES COMM-MQ20-MESSAGE.                      
              15 MESMQ20-ENTETE.                                                
                 20 MESMQ20-TYPE         PIC X(03).                             
                 20 MESMQ20-NSOCMSG      PIC X(03).                             
                 20 MESMQ20-NLIEUMSG     PIC X(03).                             
                 20 MESMQ20-NSOCDST      PIC X(03).                             
                 20 MESMQ20-NLIEUDST     PIC X(03).                             
                 20 MESMQ20-NORD         PIC 9(08).                             
                 20 MESMQ20-LPROG        PIC X(10).                             
                 20 MESMQ20-DJOUR        PIC X(08).                             
                 20 MESMQ20-WSID         PIC X(10).                             
                 20 MESMQ20-USER         PIC X(10).                             
                 20 MESMQ20-CHRONO       PIC 9(07).                             
                 20 MESMQ20-NBRMSG       PIC 9(07).                             
                 20 MESMQ20-FILLER       PIC X(30).                             
              15 MESMQ20-DATA.                                                  
                 20 MESMQ20-DATA-X       PIC X(100).                            
                                                                                
