      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * PROJET : SRP                                                            
      * DECRIPTION DES LIGNES D'ETAT                                            
      * MEME DESCRIPTION POUR TOUS LES ETATS - ZONES DE TITRE VARIABLES         
      *--> POUR ETATS ISP010/A - B ET ISP012/A - B                              
      *****************************************************************         
      *                                                                         
       01 ETAT-ISP010.                                                          
      *----------------------------------------------------------------         
      * LIGNES ENTETE                                                           
      *----------------------------------------------------------------         
      *                                                                         
      *--------------------------- LIGNE E00  --------------------------        
       02 ISP010-E00.                                                           
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(58) VALUE SPACES.                        
          05 FILLER              PIC X(16) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E01  --------------------------        
       02 ISP010-E01.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 ISP010-NOMETAT      PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(51) VALUE SPACES.                        
          05 FILLER              PIC X(11) VALUE 'EDITE LE : '.                 
          05 ISP010-DEDITE       PIC X(10) VALUE SPACES.                        
          05  FILLER             PIC X(02) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E02  --------------------------        
       02 ISP010-E02.                                                           
          05 FILLER              PIC X(03) VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
          05 FILLER              PIC X(47) VALUE SPACES.                        
          05 FILLER              PIC X(55) VALUE SPACES.                        
          05 FILLER              PIC X(07) VALUE 'PAGE : '.                     
          05 ISP010-NPAGE        PIC ZZZ9  VALUE SPACES.                        
          05 FILLER              PIC X(08) VALUE SPACES.                        
      *                                                                         
      *--------------------------- LIGNE E03  --------------------------        
       02 ISP010-E03.                                                           
          05 FILLER          PIC X(40) VALUE SPACES.                            
          05 FILLER          PIC X(22) VALUE 'FACTURATION INTERNE - '.          
          05 FILLER          PIC X(09) VALUE 'SOCIETES '.                       
          05 ISP010-RETRO    PIC X(15) VALUE SPACES.                            
          05 FILLER          PIC X(30) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E04  --------------------------        
       02 ISP010-E04.                                                           
          05 FILLER          PIC X(37) VALUE SPACES.                            
          05 FILLER          PIC X(21) VALUE 'EDITION MENSUELLE DE '.           
          05 FILLER          PIC X(23) VALUE 'VALORISATION DE L''ECART'.        
          05 FILLER          PIC X(35) VALUE ' SRP / PRMP '.                    
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E05  --------------------------        
       02 ISP010-E05.                                                           
          05 FILLER          PIC X(50) VALUE SPACES.                            
          05 FILLER          PIC X(20) VALUE 'TABLEAU DE SYNTHESE '.            
          05 ISP010-TYPSYNT  PIC X(46) VALUE SPACES.                            
          05 FILLER          PIC X(16) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E06  --------------------------        
       02 ISP010-E06.                                                           
          05 FILLER          PIC X(55) VALUE SPACES.                            
          05 FILLER          PIC X(14) VALUE 'MOIS TRAITE : '.                  
          05 ISP010-MOISTRT  PIC X(05) VALUE SPACES.                            
          05 FILLER          PIC X(58) VALUE SPACES.                            
      *                                                                         
      *--------------------------- LIGNE E10  --------------------------        
       02 ISP010-E10.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE EMETTRICE  : '.         
          05 ISP010-NSOCEMET   PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP010-LSOCEMET   PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E11  --------------------------        
       02 ISP010-E11.                                                           
          05 FILLER       PIC X(25) VALUE  '    SOCIETE RECEPTRICE : '.         
          05 ISP010-NSOCRECEP  PIC X(03) VALUE SPACES.                          
          05 FILLER            PIC X(01) VALUE SPACES.                          
          05 ISP010-LSOCRECEP  PIC X(25) VALUE SPACES.                          
          05 FILLER            PIC X(78) VALUE SPACES.                          
      *                                                                         
      *--------------------------- LIGNE E20  --------------------------        
       02 ISP010-E20.                                                           
          05 FILLER            PIC X(20) VALUE '                    '.          
          05 FILLER            PIC X(20) VALUE '     .--------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(20) VALUE '--------------------'.          
          05 FILLER            PIC X(18) VALUE '------------------'.            
          05 FILLER            PIC X(16) VALUE '-----------.    '.              
      *                                                                         
      *--------------------------- LIGNE E21  --------------------------        
       02 ISP010-E21.                                                           
          05 FILLER                         PIC X(10) VALUE SPACES.             
          05 FILLER                         PIC X(49) VALUE                     
                  '               !       VALO SRP         VALO PRMP'.          
          05 FILLER                         PIC X(57) VALUE                     
           '                      ECART SRP / PRMP                   '.         
          05 FILLER                         PIC X(16) VALUE                     
           '           !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E22  --------------------------        
        02 ISP010-E22.                                                          
           05 FILLER                         PIC X(58) VALUE                    
           '                         !                                '.        
           05 FILLER                         PIC X(58) VALUE                    
           '              H.T.       TAUX           T.V.A.            '.        
           05 FILLER                         PIC X(16) VALUE                    
           ' T.T.C.    !    '.                                                  
      *                                                                         
      *--------------------------- LIGNE E23  --------------------------        
        02  ISP010-E23.                                                         
           05 FILLER                         PIC X(58) VALUE                    
           '  .----------------------!--------------------------------'.        
           05 FILLER                         PIC X(58) VALUE                    
           '----+-----------------+--------+-----------------+--------'.        
           05 FILLER                         PIC X(16) VALUE                    
           '-----------!    '.                                                  
      *                                                                         
      *--------------------------- LIGNE D01  --------------------------        
        02 ISP010-D01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP010-TEBRB.                                                     
              10 ISP010-TEBRB-COM   PIC X(05) VALUE SPACES.                     
              10 ISP010-TEBRB-VAL   PIC X(15) VALUE SPACES.                     
           05 FILLER                PIC X(04) VALUE ' !  '.                     
           05 ISP010-VALOSRP        PIC ----------9,99.                         
           05 FILLER                PIC X(03) VALUE SPACES.                     
           05 ISP010-VALOPRMP       PIC ----------9,99.                         
           05 FILLER                PIC X(06) VALUE SPACES.                     
           05 ISP010-ECARTHT        PIC ---------9,99.                          
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP010-TXTVA          PIC Z9,99.                                  
           05 FILLER                PIC X(03) VALUE SPACES.                     
           05 ISP010-ECARTTVA       PIC ----------9,99.                         
           05 FILLER                PIC X(06) VALUE SPACES.                     
           05 ISP010-ECARTTTC       PIC ----------9,99.                         
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T01  --------------------------        
        02 ISP010-T01.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP010-TYPE.                                                      
              10 ISP010-TYPE-COM    PIC X(11) VALUE SPACES.                     
              10 ISP010-TYPE-VAL    PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(03) VALUE ' ! '.                      
           05 ISP010-TOTSRP         PIC -----------9,99.                        
           05 FILLER                PIC X(02) VALUE SPACES.                     
           05 ISP010-TOTPRMP        PIC -----------9,99.                        
           05 FILLER                PIC X(05) VALUE SPACES.                     
           05 ISP010-TOTHT          PIC ----------9,99.                         
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP010-TOTTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(02) VALUE SPACES.                     
           05 ISP010-TOTTVA         PIC -----------9,99.                        
           05 FILLER                PIC X(05) VALUE SPACES.                     
           05 ISP010-TOTTTC         PIC -----------9,99.                        
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE T10  --------------------------        
        02 ISP010-T10.                                                          
           05 FILLER                PIC X(04) VALUE '  ! '.                     
           05 ISP010-TYPE10.                                                    
              10 ISP010-TYPE10-COM  PIC X(11) VALUE SPACES.                     
              10 ISP010-TYPE10-VAL  PIC X(09) VALUE SPACES.                     
           05 FILLER                PIC X(02) VALUE ' !'.                       
           05 ISP010-GENSRP         PIC ------------9,99.                       
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP010-GENPRMP        PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP010-GENHT          PIC -----------9,99.                        
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP010-GENTXTVA       PIC Z9,99.                                  
           05 FILLER                PIC X(01) VALUE SPACES.                     
           05 ISP010-GENTVA         PIC ------------9,99.                       
           05 FILLER                PIC X(04) VALUE SPACES.                     
           05 ISP010-GENTTC         PIC ------------9,99.                       
           05 FILLER                PIC X(07) VALUE '   !   '.                  
      *                                                                         
      *--------------------------- LIGNE B01  --------------------------        
           02  ISP010-B01.                                                      
               05  FILLER                         PIC X(48) VALUE               
           '  ''---------------------------------------------'.                 
               05  FILLER   PIC X(10) VALUE '----------'.                       
               05  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               05  FILLER                         PIC X(16) VALUE               
           '-----------''    '.                                                 
      *                                                                         
      *----------------------- LIGNE VIDE DANS TABLEAU -----------------        
           02  ISP010-TAB-VIDE.                                                 
               05  FILLER                         PIC X(58) VALUE               
           '  !                      !                                '.        
               05  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               05  FILLER                         PIC X(16) VALUE               
           '           !    '.                                                  
      *                                                                         
                                                                                
