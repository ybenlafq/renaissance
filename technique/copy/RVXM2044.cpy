      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2044                            *        
      ******************************************************************        
       01  RVXM2044.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2044-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2044-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2044-CFAM.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2044-CFAM-LEN          PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2044-CFAM-LEN          PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2044-CFAM-TEXT         PIC X(05).                            
           10 XM2044-CARSP.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2044-CARSP-LEN         PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2044-CARSP-LEN         PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2044-CARSP-TEXT        PIC X(05).                            
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2044                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2044-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2044-CFAM-F              PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2044-CFAM-F              PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2044-CARSP-F             PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2044-CARSP-F             PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 03      *        
      ******************************************************************        
                                                                                
