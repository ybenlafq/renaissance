      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      * Working pour utilisation copie tbctel                                   
      *                                                                         
       01       MONT-WPARAM.                                                    
      *                                                                         
      *         Zones en entr{es du module                                      
      *                                                                         
           3    MONT-WENTREE.                                                   
            05  MONT-WZONE.                                                     
             08 MONT-WZ   PIC X OCCURS 15 INDEXED MONT-WI .                     
      *                                                                         
      *    On peut autoriser un nombre minimum et maximum de chiffre            
      *       SI on veut un nombre exact le minimum est egal au maximum         
      *                                                                         
            05  MONT-WCH-MAX      PIC 99.                                       
            05  MONT-WCH-MIN      PIC 99.                                       
      *                                                                         
      *         Zones sortie du module                                          
      *                                                                         
           3    MONT-WSORTIE.                                                   
            05  MONT-WERREUR     PIC 9 VALUE 0.                                 
             88 MONT-ERREUR-N          VALUE 0.                                 
             88 MONT-ERREUR-O          VALUE 1.                                 
      *                                                                         
            05  MONT-MSGID       PIC X(80).                                     
      *                                                                         
            05  MONT-WCH          PIC 99.                                       
            05  MONT-WZP          PIC 99.                                       
            05  MONT-WPREMCH      PIC 99.                                       
      *                                                                         
            05  MONT-WRESULT      PIC 9(15).                                    
            05  MONT-WRESULT-A    REDEFINES MONT-WRESULT.                       
             08 MONT-W9           PIC X OCCURS 15 INDEXED MONT-WJ.              
      *                                                                         
      *                                                                         
                                                                                
