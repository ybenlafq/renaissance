      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *@ @(#) MetaWare Technologies IBM-MF Cobol-Translater 5.0.9 05/01/        
      *                                                                         
      *                                                                         
      * �BANNER_START                                                           
      * Licensed Materials - Property of IBM                                    
      *                                                                         
      * "Restricted Materials of IBM"                                           
      *                                                                         
      * 5697-E93                                                                
      *                                                                         
      * (C) Copyright IBM Corp. 2001                                            
      *                                                                         
      * CICS                                                                    
      * (Element of CICS Transaction Server                                     
      *   for z/OS, Version 2 Release 2)                                        
      * �BANNER_END                                                             
      *                                                                         
      *                                                                         
      *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*         
      *                                                               *         
      *               METAWARE SPECIAL HEXADECIMAL VERSION            *         
      *                                                               *         
      *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*         
       01      DFHBMSCA.                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPEM  PICTURE X   VALUE  IS X"19".                           
      *--                                                                       
         02    DFHBMPEM  PICTURE X   VALUE  IS X"19".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPNL  PICTURE X   VALUE  IS X"15".                           
      *--                                                                       
         02    DFHBMPNL  PICTURE X   VALUE  IS X"0A".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPFF  PICTURE X   VALUE  IS X"0C".                           
      *--                                                                       
         02    DFHBMPFF  PICTURE X   VALUE  IS X"0C".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPCR  PICTURE X   VALUE  IS X"0D".                           
      *--                                                                       
         02    DFHBMPCR  PICTURE X   VALUE  IS X"0D".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMASK  PICTURE X   VALUE  IS X"F0".                           
      *--                                                                       
         02    DFHBMASK  PICTURE X   VALUE  IS X"30".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMUNP  PICTURE X   VALUE  IS X"40".                           
      *--                                                                       
         02    DFHBMUNP  PICTURE X   VALUE  IS X"20".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMUNN  PICTURE X   VALUE  IS X"50".                           
      *--                                                                       
         02    DFHBMUNN  PICTURE X   VALUE  IS X"26".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPRO  PICTURE X   VALUE  IS X"60".                           
      *--                                                                       
         02    DFHBMPRO  PICTURE X   VALUE  IS X"2D".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMBRY  PICTURE X   VALUE  IS X"C8".                           
      *--                                                                       
         02    DFHBMBRY  PICTURE X   VALUE  IS X"48".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMDAR  PICTURE X   VALUE  IS X"4C".                           
      *--                                                                       
         02    DFHBMDAR  PICTURE X   VALUE  IS X"3C".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMFSE  PICTURE X   VALUE  IS X"C1".                           
      *--                                                                       
         02    DFHBMFSE  PICTURE X   VALUE  IS X"41".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMPRF  PICTURE X   VALUE  IS X"61".                           
      *--                                                                       
         02    DFHBMPRF  PICTURE X   VALUE  IS X"2F".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMASF  PICTURE X   VALUE  IS X"F1".                           
      *--                                                                       
         02    DFHBMASF  PICTURE X   VALUE  IS X"31".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMASB  PICTURE X   VALUE  IS X"F8".                           
      *--                                                                       
         02    DFHBMASB  PICTURE X   VALUE  IS X"38".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMEOF  PICTURE X   VALUE  IS X"80".                           
      *--                                                                       
         02    DFHBMEOF  PICTURE X   VALUE  IS X"D8".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMCUR  PICTURE X   VALUE  IS X"02".                           
      *--                                                                       
         02    DFHBMCUR  PICTURE X   VALUE  IS X"02".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMEC   PICTURE X   VALUE  IS X"82".                           
      *--                                                                       
         02    DFHBMEC   PICTURE X   VALUE  IS X"62".                           
      *}                                                                        
         02    DFHBMFLG  PICTURE X.                                             
      *{ remove-comma-in-dde 1.5                                                
      *      88    DFHERASE VALUES ARE X"80", X"82".                            
      *--                                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *      88    DFHERASE VALUES ARE X"80"  X"82".                            
      *--                                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *      88    DFHERASE VALUES ARE X"D8"  X"82".                            
      *--                                                                       
             88    DFHERASE VALUES ARE X"D8"  X"62".                            
      *}                                                                        
      *}                                                                        
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *      88    DFHCURSR VALUES ARE X"02", X"82".                            
      *--                                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *      88    DFHCURSR VALUES ARE X"02"  X"82".                            
      *--                                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *      88    DFHCURSR VALUES ARE X"02"  X"82".                            
      *--                                                                       
             88    DFHCURSR VALUES ARE X"02"  X"62".                            
      *}                                                                        
      *}                                                                        
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBMDET  PICTURE X   VALUE  IS X"FF".                           
      *--                                                                       
         02    DFHBMDET  PICTURE X   VALUE  IS X"FF".                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02    DFHBMPSO-BIN PIC 9(4) COMP VALUE 3599.                           
      *--                                                                       
         02    DFHBMPSO-BIN PIC 9(4) COMP-5 VALUE 3599.                         
      *}                                                                        
      * ABOVE VALUE 3599 = X'0E0F' ADDED BY PTM 81385 (APAR PN23267)            
         02    FILLER REDEFINES DFHBMPSO-BIN.                                   
           03  DFHBMPSO  PICTURE X.                                             
           03  DFHBMPSI  PICTURE X.                                             
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHSA     PICTURE X   VALUE  IS X"28".                           
      *--                                                                       
         02    DFHSA     PICTURE X   VALUE  IS X"88".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHCOLOR  PICTURE X   VALUE  IS X"42".                           
      *--                                                                       
         02    DFHCOLOR  PICTURE X   VALUE  IS X"E2".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHPS     PICTURE X   VALUE  IS X"43".                           
      *--                                                                       
         02    DFHPS     PICTURE X   VALUE  IS X"E4".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHHLT    PICTURE X   VALUE  IS X"41".                           
      *--                                                                       
         02    DFHHLT    PICTURE X   VALUE  IS X"A0".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFH3270   PICTURE X   VALUE  IS X"C0".                           
      *--                                                                       
         02    DFH3270   PICTURE X   VALUE  IS X"E9".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHVAL    PICTURE X   VALUE  IS X"C1".                           
      *--                                                                       
         02    DFHVAL    PICTURE X   VALUE  IS X"41".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHOUTLN  PICTURE X   VALUE  IS X"C2".                           
      *--                                                                       
         02    DFHOUTLN  PICTURE X   VALUE  IS X"42".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBKTRN  PICTURE X   VALUE  IS X"46".                           
      *--                                                                       
         02    DFHBKTRN  PICTURE X   VALUE  IS X"E3".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHALL    PICTURE X   VALUE  IS X"00".                           
      *--                                                                       
         02    DFHALL    PICTURE X   VALUE  IS X"00".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHERROR  PICTURE X   VALUE  IS X"3F".                           
      *--                                                                       
         02    DFHERROR  PICTURE X   VALUE  IS X"1A".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHDFT    PICTURE X   VALUE  IS X"7D".                           
      *--                                                                       
         02    DFHDFT    PICTURE X   VALUE  IS X"27".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHDFCOL  PICTURE X   VALUE  IS X"00".                           
      *--                                                                       
         02    DFHDFCOL  PICTURE X   VALUE  IS X"00".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBLUE   PICTURE X   VALUE  IS X"F1".                           
      *--                                                                       
         02    DFHBLUE   PICTURE X   VALUE  IS X"31".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHRED    PICTURE X   VALUE  IS X"F2".                           
      *--                                                                       
         02    DFHRED    PICTURE X   VALUE  IS X"32".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHPINK   PICTURE X   VALUE  IS X"F3".                           
      *--                                                                       
         02    DFHPINK   PICTURE X   VALUE  IS X"33".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHGREEN  PICTURE X   VALUE  IS X"F4".                           
      *--                                                                       
         02    DFHGREEN  PICTURE X   VALUE  IS X"34".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHTURQ   PICTURE X   VALUE  IS X"F5".                           
      *--                                                                       
         02    DFHTURQ   PICTURE X   VALUE  IS X"35".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHYELLO  PICTURE X   VALUE  IS X"F6".                           
      *--                                                                       
         02    DFHYELLO  PICTURE X   VALUE  IS X"36".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHNEUTR  PICTURE X   VALUE  IS X"F7".                           
      *--                                                                       
         02    DFHNEUTR  PICTURE X   VALUE  IS X"37".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBASE   PICTURE X   VALUE  IS X"00".                           
      *--                                                                       
         02    DFHBASE   PICTURE X   VALUE  IS X"00".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHDFHI   PICTURE X   VALUE  IS X"00".                           
      *--                                                                       
         02    DFHDFHI   PICTURE X   VALUE  IS X"00".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHBLINK  PICTURE X   VALUE  IS X"F1".                           
      *--                                                                       
         02    DFHBLINK  PICTURE X   VALUE  IS X"31".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHREVRS  PICTURE X   VALUE  IS X"F2".                           
      *--                                                                       
         02    DFHREVRS  PICTURE X   VALUE  IS X"32".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNDLN  PICTURE X   VALUE  IS X"F4".                           
      *--                                                                       
         02    DFHUNDLN  PICTURE X   VALUE  IS X"34".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMFIL   PICTURE X   VALUE  IS X"04".                           
      *--                                                                       
         02    DFHMFIL   PICTURE X   VALUE  IS X"9C".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMENT   PICTURE X   VALUE  IS X"02".                           
      *--                                                                       
         02    DFHMENT   PICTURE X   VALUE  IS X"02".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMFE    PICTURE X   VALUE  IS X"06".                           
      *--                                                                       
         02    DFHMFE    PICTURE X   VALUE  IS X"86".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNNOD  PICTURE X   VALUE  IS X"4D".                           
      *--                                                                       
         02    DFHUNNOD  PICTURE X   VALUE  IS X"28".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNIMD  PICTURE X   VALUE  IS X"C9".                           
      *--                                                                       
         02    DFHUNIMD  PICTURE X   VALUE  IS X"49".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNNUM  PICTURE X   VALUE  IS X"D1".                           
      *--                                                                       
         02    DFHUNNUM  PICTURE X   VALUE  IS X"4A".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNNUB  PICTURE X   VALUE  IS X"D8".                           
      *--                                                                       
         02    DFHUNNUB  PICTURE X   VALUE  IS X"51".                           
      *}                                                                        
      * ABOVE VALUE DFHUNNUB ADDED BY APAR PN67669                              
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNINT  PICTURE X   VALUE  IS X"D9".                           
      *--                                                                       
         02    DFHUNINT  PICTURE X   VALUE  IS X"52".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNNON  PICTURE X   VALUE  IS X"5D".                           
      *--                                                                       
         02    DFHUNNON  PICTURE X   VALUE  IS X"29".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHPROTI  PICTURE X   VALUE  IS X"E8".                           
      *--                                                                       
         02    DFHPROTI  PICTURE X   VALUE  IS X"59".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHPROTN  PICTURE X   VALUE  IS X"6C".                           
      *--                                                                       
         02    DFHPROTN  PICTURE X   VALUE  IS X"25".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMT     PICTURE X   VALUE  IS X"01".                           
      *--                                                                       
         02    DFHMT     PICTURE X   VALUE  IS X"01".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMFT    PICTURE X   VALUE  IS X"05".                           
      *--                                                                       
         02    DFHMFT    PICTURE X   VALUE  IS X"09".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMET    PICTURE X   VALUE  IS X"03".                           
      *--                                                                       
         02    DFHMET    PICTURE X   VALUE  IS X"03".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHMFET   PICTURE X   VALUE  IS X"07".                           
      *--                                                                       
         02    DFHMFET   PICTURE X   VALUE  IS X"7F".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHDFFR   PICTURE X   VALUE  IS X"00".                           
      *--                                                                       
         02    DFHDFFR   PICTURE X   VALUE  IS X"00".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHLEFT   PICTURE X   VALUE  IS X"08".                           
      *--                                                                       
         02    DFHLEFT   PICTURE X   VALUE  IS X"97".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHOVER   PICTURE X   VALUE  IS X"04".                           
      *--                                                                       
         02    DFHOVER   PICTURE X   VALUE  IS X"9C".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHRIGHT  PICTURE X   VALUE  IS X"02".                           
      *--                                                                       
         02    DFHRIGHT  PICTURE X   VALUE  IS X"02".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHUNDER  PICTURE X   VALUE  IS X"01".                           
      *--                                                                       
         02    DFHUNDER  PICTURE X   VALUE  IS X"01".                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02    DFHBOX-BIN  PIC 9(4) COMP VALUE 15.                              
      *--                                                                       
         02    DFHBOX-BIN  PIC 9(4) COMP-5 VALUE 15.                            
      *}                                                                        
      * ABOVE VALUE 15 = X'000F' ADDED BY PTM 81385 (APAR PN23267)              
         02    FILLER REDEFINES DFHBOX-BIN.                                     
           03  FILLER    PICTURE X.                                             
           03  DFHBOX    PICTURE X.                                             
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHSOSI   PICTURE X   VALUE  IS X"01".                           
      *--                                                                       
         02    DFHSOSI   PICTURE X   VALUE  IS X"01".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHTRANS  PICTURE X   VALUE  IS X"F0".                           
      *--                                                                       
         02    DFHTRANS  PICTURE X   VALUE  IS X"30".                           
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *  02    DFHOPAQ   PICTURE X   VALUE  IS X"FF".                           
      *--                                                                       
         02    DFHOPAQ   PICTURE X   VALUE  IS X"FF".                           
      *}                                                                        
                                                                                
                                                                                
