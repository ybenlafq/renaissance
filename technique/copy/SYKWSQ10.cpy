      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL       *         
      ****************************************************** SYKWSQ10 *         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  TRACE-SQL-MESSAGE.                                               
      *                                                                         
             10  FILLER         PIC  X(4)  VALUE 'TAB='.                        
             10  TABLE-NAME     PIC  X(18)   VALUE SPACES.                      
      *                                                                         
      *      10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(4)  VALUE 'MOD='.                        
             10  MODEL-NAME      PIC  X(6)   VALUE SPACES.                      
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(5)   VALUE 'FUNC='.                      
             10  TRACE-SQL-FUNCTION  PIC  X(7)  VALUE  SPACES.                  
                 88  CODE-CONNECT               VALUE  'CONNECT'.               
                 88  CODE-SELECT                VALUE  'SELECT'.                
                 88  CODE-INSERT                VALUE  'INSERT'.                
                 88  CODE-UPDATE                VALUE  'UPDATE'.                
                 88  CODE-DELETE                VALUE  'DELETE'.                
                 88  CODE-PREPARE               VALUE  'PREPARE'.               
                 88  CODE-DECLARE               VALUE  'DECLARE'.               
                 88  CODE-OPEN                  VALUE  'OPEN'.                  
                 88  CODE-FETCH                 VALUE  'FETCH'.                 
                 88  CODE-CLOSE                 VALUE  'CLOSE'.                 
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(8)   VALUE 'SQLCODE='.                   
             10  TRACE-SQL-CODE      PIC  -Z(7)9 VALUE  ZEROS.                  
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FUNC-SQL       PIC  X(8)   VALUE  SPACES.                      
             10  FUNC-CONNECT   PIC  X(8)   VALUE 'CONNECT'.                    
             10  FUNC-SELECT    PIC  X(8)   VALUE 'SELECT'.                     
             10  FUNC-INSERT    PIC  X(8)   VALUE 'INSERT'.                     
             10  FUNC-UPDATE    PIC  X(8)   VALUE 'UPDATE'.                     
             10  FUNC-DELETE    PIC  X(8)   VALUE 'DELETE'.                     
             10  FUNC-PREPARE   PIC  X(8)   VALUE 'PREPARE'.                    
             10  FUNC-DECLARE   PIC  X(8)   VALUE 'DECLARE'.                    
             10  FUNC-OPEN      PIC  X(8)   VALUE 'OPEN'.                       
             10  FUNC-FETCH     PIC  X(8)   VALUE 'FETCH'.                      
             10  FUNC-CLOSE     PIC  X(8)   VALUE 'CLOSE'.                      
             10  FILLER-SQL     PIC  X(12)  VALUE SPACE.                        
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  SQL-CODE       PIC  S9(9) COMP-4.                              
      *--                                                                       
             10  SQL-CODE       PIC  S9(9) COMP-5.                              
      *}                                                                        
       EJECT                                                                    
                                                                                
