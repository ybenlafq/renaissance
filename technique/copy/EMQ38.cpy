      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMQ38   EMQ38                                              00000020
      ***************************************************************** 00000030
       01   EMQ38I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEMAXL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNPAGEMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNPAGEMAXF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEMAXI     PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUEUECL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MQUEUECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQUEUECF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MQUEUECI  PIC X(24).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATECL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATECF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATECI   PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHEURECL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MHEURECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MHEURECF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MHEURECI  PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPCOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCOPCOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOPCOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCOPCOCI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIDSIL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MIDSIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MIDSIF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MIDSII    PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLEL     COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCLEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCLEF     PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCLEI     PIC X(45).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMSGIDHL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MMSGIDHL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMSGIDHF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MMSGIDHI  PIC X(24).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORRELIDHL    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MCORRELIDHL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCORRELIDHF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCORRELIDHI    PIC X(24).                                 00000530
           02 MLIGNEI OCCURS   12 TIMES .                               00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTIONL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MACTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MACTIONF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MACTIONI     PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEML      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDATEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEMF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDATEMI      PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHEUREML     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MHEUREML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHEUREMF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MHEUREMI     PIC X(4).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MIDSIML      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MIDSIML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MIDSIMF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MIDSIMI      PIC X(10).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLEML  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCLEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCLEMF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCLEMI  PIC X(18).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOPCOML     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCOPCOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOPCOMF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCOPCOMI     PIC X(4).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTAILLEL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MTAILLEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTAILLEF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MTAILLEI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIORL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MPRIORL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIORF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MPRIORI      PIC X(2).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEXPIRYL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MEXPIRYL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MEXPIRYF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MEXPIRYI     PIC X(9).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(78).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EMQ38   EMQ38                                              00001120
      ***************************************************************** 00001130
       01   EMQ38O REDEFINES EMQ38I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNPAGEA   PIC X.                                          00001310
           02 MNPAGEC   PIC X.                                          00001320
           02 MNPAGEP   PIC X.                                          00001330
           02 MNPAGEH   PIC X.                                          00001340
           02 MNPAGEV   PIC X.                                          00001350
           02 MNPAGEO   PIC X(4).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNPAGEMAXA     PIC X.                                     00001380
           02 MNPAGEMAXC     PIC X.                                     00001390
           02 MNPAGEMAXP     PIC X.                                     00001400
           02 MNPAGEMAXH     PIC X.                                     00001410
           02 MNPAGEMAXV     PIC X.                                     00001420
           02 MNPAGEMAXO     PIC X(4).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MQUEUECA  PIC X.                                          00001450
           02 MQUEUECC  PIC X.                                          00001460
           02 MQUEUECP  PIC X.                                          00001470
           02 MQUEUECH  PIC X.                                          00001480
           02 MQUEUECV  PIC X.                                          00001490
           02 MQUEUECO  PIC X(24).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MDATECA   PIC X.                                          00001520
           02 MDATECC   PIC X.                                          00001530
           02 MDATECP   PIC X.                                          00001540
           02 MDATECH   PIC X.                                          00001550
           02 MDATECV   PIC X.                                          00001560
           02 MDATECO   PIC X(8).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MHEURECA  PIC X.                                          00001590
           02 MHEURECC  PIC X.                                          00001600
           02 MHEURECP  PIC X.                                          00001610
           02 MHEURECH  PIC X.                                          00001620
           02 MHEURECV  PIC X.                                          00001630
           02 MHEURECO  PIC X(4).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCOPCOCA  PIC X.                                          00001660
           02 MCOPCOCC  PIC X.                                          00001670
           02 MCOPCOCP  PIC X.                                          00001680
           02 MCOPCOCH  PIC X.                                          00001690
           02 MCOPCOCV  PIC X.                                          00001700
           02 MCOPCOCO  PIC X(3).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MIDSIA    PIC X.                                          00001730
           02 MIDSIC    PIC X.                                          00001740
           02 MIDSIP    PIC X.                                          00001750
           02 MIDSIH    PIC X.                                          00001760
           02 MIDSIV    PIC X.                                          00001770
           02 MIDSIO    PIC X(10).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCLEA     PIC X.                                          00001800
           02 MCLEC     PIC X.                                          00001810
           02 MCLEP     PIC X.                                          00001820
           02 MCLEH     PIC X.                                          00001830
           02 MCLEV     PIC X.                                          00001840
           02 MCLEO     PIC X(45).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MMSGIDHA  PIC X.                                          00001870
           02 MMSGIDHC  PIC X.                                          00001880
           02 MMSGIDHP  PIC X.                                          00001890
           02 MMSGIDHH  PIC X.                                          00001900
           02 MMSGIDHV  PIC X.                                          00001910
           02 MMSGIDHO  PIC X(24).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCORRELIDHA    PIC X.                                     00001940
           02 MCORRELIDHC    PIC X.                                     00001950
           02 MCORRELIDHP    PIC X.                                     00001960
           02 MCORRELIDHH    PIC X.                                     00001970
           02 MCORRELIDHV    PIC X.                                     00001980
           02 MCORRELIDHO    PIC X(24).                                 00001990
           02 MLIGNEO OCCURS   12 TIMES .                               00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MACTIONA     PIC X.                                     00002020
             03 MACTIONC     PIC X.                                     00002030
             03 MACTIONP     PIC X.                                     00002040
             03 MACTIONH     PIC X.                                     00002050
             03 MACTIONV     PIC X.                                     00002060
             03 MACTIONO     PIC X.                                     00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MDATEMA      PIC X.                                     00002090
             03 MDATEMC PIC X.                                          00002100
             03 MDATEMP PIC X.                                          00002110
             03 MDATEMH PIC X.                                          00002120
             03 MDATEMV PIC X.                                          00002130
             03 MDATEMO      PIC X(8).                                  00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MHEUREMA     PIC X.                                     00002160
             03 MHEUREMC     PIC X.                                     00002170
             03 MHEUREMP     PIC X.                                     00002180
             03 MHEUREMH     PIC X.                                     00002190
             03 MHEUREMV     PIC X.                                     00002200
             03 MHEUREMO     PIC X(4).                                  00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MIDSIMA      PIC X.                                     00002230
             03 MIDSIMC PIC X.                                          00002240
             03 MIDSIMP PIC X.                                          00002250
             03 MIDSIMH PIC X.                                          00002260
             03 MIDSIMV PIC X.                                          00002270
             03 MIDSIMO      PIC X(10).                                 00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MCLEMA  PIC X.                                          00002300
             03 MCLEMC  PIC X.                                          00002310
             03 MCLEMP  PIC X.                                          00002320
             03 MCLEMH  PIC X.                                          00002330
             03 MCLEMV  PIC X.                                          00002340
             03 MCLEMO  PIC X(18).                                      00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MCOPCOMA     PIC X.                                     00002370
             03 MCOPCOMC     PIC X.                                     00002380
             03 MCOPCOMP     PIC X.                                     00002390
             03 MCOPCOMH     PIC X.                                     00002400
             03 MCOPCOMV     PIC X.                                     00002410
             03 MCOPCOMO     PIC X(4).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MTAILLEA     PIC X.                                     00002440
             03 MTAILLEC     PIC X.                                     00002450
             03 MTAILLEP     PIC X.                                     00002460
             03 MTAILLEH     PIC X.                                     00002470
             03 MTAILLEV     PIC X.                                     00002480
             03 MTAILLEO     PIC X(5).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MPRIORA      PIC X.                                     00002510
             03 MPRIORC PIC X.                                          00002520
             03 MPRIORP PIC X.                                          00002530
             03 MPRIORH PIC X.                                          00002540
             03 MPRIORV PIC X.                                          00002550
             03 MPRIORO      PIC X(2).                                  00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MEXPIRYA     PIC X.                                     00002580
             03 MEXPIRYC     PIC X.                                     00002590
             03 MEXPIRYP     PIC X.                                     00002600
             03 MEXPIRYH     PIC X.                                     00002610
             03 MEXPIRYV     PIC X.                                     00002620
             03 MEXPIRYO     PIC X(9).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(78).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
