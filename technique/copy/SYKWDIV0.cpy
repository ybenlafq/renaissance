      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      *          * SAUVEGARDE DE ZONES IMPORTANTES EN CAS DE PLANTAGE *         
      ****************************************************** SYKWDIV0 *         
      *                                                               *         
      * FONCTION EST UNE ZONE DONT LE BUT EST DE REFLETER LA DEMANDE  *         
      *          QUE L'OPERATEUR A FAITE DE MANIERE EXPLICITE OU NON. *         
      *                                                               *         
      * KONTROL  = 0  PAS D'ERREUR LOGIQUE.                           *         
      *          = 1  ERREUR AVEC INTERRUPTION DU TRAITEMENT.         *         
      *                                                               *         
      * CODE-RETOUR = CODE RETOUR DE LA SECTION QUE L'ON VIENT        *         
      *               D'EXECUTER.                                     *         
      *                                                               *         
      ****************************************************** SYKWDIV0 *         
      *    AJOUT DE - TOUCHE-CURS        COMME VALEUR DE FONCTION               
      *             - CODE-TOUCHE-CURS                                          
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER PIC X(20) VALUE '* WORKING--STORAGE *'.                     
      *                                                                         
         02  FILLER      PIC X(13)          VALUE 'DEBUG........'.              
         02  DEBUGGIN    PIC X(03)          VALUE 'OUI'.                        
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'FONCTION......'.             
         02  FONCTION    PIC X(02)          VALUE 'FF'.                         
      *                                                                         
           88  TRAITEMENT                 VALUE '01' THRU '99'.                 
           88  TRAITEMENT-NORMAL          VALUE '01'.                           
           88  TRAITEMENT-CRITERES        VALUE '11'.                           
           88  TRAITEMENT-DATAS           VALUE '22'.                           
           88  TRAITEMENT-AUTOMATIQUE     VALUE '05' THRU '10'.                 
           88  PAGINATION                 VALUE '05' THRU '08'.                 
           88  PREMIERE                   VALUE '05'.                           
           88  DERNIERE                   VALUE '06'.                           
           88  PRECEDENTE                 VALUE '07'.                           
           88  SUIVANTE                   VALUE '08'.                           
           88  LEVEL-MAX                  VALUE 'KA'.                           
           88  LEVEL-SUP                  VALUE 'KC'.                           
           88  HELP                       VALUE 'KB'.                           
           88  SWAP                       VALUE 'KS'.                           
           88  JUMP                       VALUE 'KJ'.                           
           88  ABANDON                    VALUE 'KD'.                           
           88  TOUCHE-CURS                VALUE 'KX'.                           
           88  ERREUR-MANIPULATION        VALUE 'FF'.                           
           88  SUSPENSION                 VALUE '03'.                           
           88  FIN                        VALUE '04'.                           
      *                                                                         
         02  FILLER      PIC X(15)          VALUE 'KONTROL........'.            
         02  KONTROL     PIC X(1)           VALUE '0'.                          
           88  ERREUR-AIDA                VALUE '1'.                            
           88  ERREUR                     VALUE '2'.                            
           88  OK                         VALUE '0'.                            
      *                                                                         
         02  FILLER      PIC X(15)          VALUE 'CODE-RETOUR....'.            
         02  CODE-RETOUR PIC X(1)           VALUE '0'.                          
           88  TROUVE                     VALUE '0'.                            
           88  NORMAL                     VALUE '0'.                            
           88  DATE-OK                    VALUE '0'.                            
           88  SELECTE                    VALUE '0'.                            
           88  NON-TROUVE                 VALUE '1'.                            
           88  NON-SELECTE                VALUE '1'.                            
           88  ANORMAL                    VALUE '1'.                            
           88  EXISTE-DEJA                VALUE '2'.                            
           88  FIN-FICHIER                VALUE '3'.                            
           88  ERREUR-DATE                VALUE '4'.                            
           88  ERREUR-FORMAT              VALUE '5'.                            
           88  ERREUR-HEURE               VALUE '6'.                            
           88  DOUBLE                     VALUE '7'.                            
      *                                                                         
         02  FILLER      PIC X(15)          VALUE 'TYPE-CIRCUIT...'.            
         02  TYPE-CIRCUIT PIC X(1)          VALUE '1'.                          
           88  CIRCUIT-COURT              VALUE '7'.                            
      *                                                                         
         02  FILLER      PIC X(15)          VALUE 'ETAT-ECRAN.....'.            
         02  ETAT-ECRAN  PIC X(1)           VALUE '0'.                          
           88 ECRAN-MAPFAIL               VALUE '1'.                            
      *                                                                         
         02  FILLER      PIC X(15)          VALUE 'ACTION.........'.            
         02  ACTION      PIC X(1)           VALUE ' '.                          
           88 CREATION                    VALUE 'C'.                            
           88 MODIFICATION                VALUE 'M'.                            
           88 SUPPRESSION                 VALUE 'S'.                            
           88 INTERROGATION               VALUE 'I'.                            
      *                                                                         
      *==> DARTY ******************************************************         
      *          * INDICES                                            *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'INDICE-IA.....'.             
         02  IA          PIC S9(3) COMP-3   VALUE +0.                           
           88  FIN-BOUCLE-IA              VALUE +999.                           
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'INDICE-AI.....'.             
         02  AI          PIC S9(3) COMP-3   VALUE +0.                           
           88  FIN-BOUCLE-AI              VALUE +999.                           
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'INDICE-IT.....'.             
         02  IT          PIC S9(3) COMP-3   VALUE +0.                           
           88  FIN-TABLE                  VALUE +999.                           
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'INDICE-LIGNE..'.             
         02  IL          PIC S9(3) COMP-3   VALUE +0.                           
           88  FIN-BOUCLE-IL              VALUE +999.                           
      *                                                                         
         02  FILLER      PIC X(14)          VALUE 'INDICE-PAGE...'.             
         02  IP          PIC S9(3) COMP-3   VALUE +0.                           
      *                                                                         
         02  FILLER            PIC X(15)    VALUE 'ETAT-CONSULT...'.            
         02  ETAT-CONSULTATION PIC X        VALUE '0'.                          
           88  FIN-CONSULTATION           VALUE '1'.                            
      *                                                                         
         02  FILLER      PIC X(11)          VALUE 'NUM-5......'.                
         02  NUM-5       PIC 9(5)           VALUE 0.                            
         02  FILL1       REDEFINES NUM-5.                                       
           03  NUM-4                       PIC 9(4).                            
           03  FILLER                      PIC X(1).                            
         02  FILL2       REDEFINES NUM-5.                                       
           03  NUM-3                       PIC 9(3).                            
           03  FILLER                      PIC X(2).                            
         02  FILL3       REDEFINES NUM-5.                                       
           03  NUM-2                       PIC 9(2).                            
           03  FILLER                      PIC X(3).                            
         02  FILL4       REDEFINES NUM-5.                                       
           03  NUM-1                       PIC 9(1).                            
           03  FILLER                      PIC X(4).                            
      *                                                                         
         02  FILLER      PIC X(12)          VALUE 'COMPTEUR....'.               
         02  COMPTEUR    PIC S9(7) COMP-3   VALUE +0.                           
         02  B1          PIC X VALUE '#'.                                       
      *                                                                         
      *==> DARTY ******************************************************         
      *          * CODE ABANDON                                       *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
         02  FILLER              PIC X(15) VALUE 'CODE-ABANDON...'.             
         02  CODE-ABANDON        PIC X VALUE SPACE.                             
             88  ABEND-ABANDON           VALUE 'A'.                             
             88  AIDA-ABANDON            VALUE 'K'.                             
             88  TACHE-ABANDON           VALUE 'T'.                             
             88  CICS-ABANDON            VALUE 'C'.                             
             88  DL1-ABANDON             VALUE 'D'.                             
             88  VSAM-ABANDON            VALUE 'V'.                             
             88  SQL-ABANDON             VALUE 'S'.                             
      *                                                                         
      *==> DARTY ******************************************************         
      *          * DATE ET HEURE DE COMPILATION                       *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
         02  Z-WHEN-COMPILED     PIC X(16) VALUE SPACE.                         
      *                                                                         
      *==> DARTY ******************************************************         
      *          * DIFFERENTES ZONES POUR LES ORDRES CICS             *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-COMMAREA        PIC S9(4) COMP VALUE +0100.                   
      *--                                                                       
         02  LONG-COMMAREA        PIC S9(4) COMP-5 VALUE +0100.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-COMMAREA-LINK   PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
         02  LONG-COMMAREA-LINK   PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-START           PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
         02  LONG-START           PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-MAP             PIC S9(4) COMP VALUE +0.                      
      *--                                                                       
         02  LONG-MAP             PIC S9(4) COMP-5 VALUE +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-TD              PIC S9(4) COMP VALUE +0.                      
      *--                                                                       
         02  LONG-TD              PIC S9(4) COMP-5 VALUE +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  LONG-TS              PIC S9(4) COMP VALUE +0.                      
      *--                                                                       
         02  LONG-TS              PIC S9(4) COMP-5 VALUE +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  RANG-TS              PIC S9(4) COMP VALUE +0.                      
      *--                                                                       
         02  RANG-TS              PIC S9(4) COMP-5 VALUE +0.                    
      *}                                                                        
         02  FILE-NAME            PIC X(8)       VALUE SPACE.                   
         02  PATH-NAME            PIC X(8)       VALUE SPACE.                   
         02  FILE-FUNCTION        PIC X(4)       VALUE '****'.                  
         02  FILE-STATUS          PIC X(2)       VALUE '**'.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  FILE-LONG            PIC S9(4) COMP VALUE +0.                      
      *--                                                                       
         02  FILE-LONG            PIC S9(4) COMP-5 VALUE +0.                    
      *}                                                                        
      *                                                                         
      *==> DARTY ******************************************************         
      *          * ZONES   DIVERSES                                   *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
         02  NOM-MAP             PIC X(8)  VALUE SPACE.                         
         02  NOM-MAPSET          PIC X(8)  VALUE SPACE.                         
         02  NOM-PROG            PIC X(8)  VALUE SPACE.                         
         02  NOM-PROG-SUI        PIC X(8)  VALUE SPACE.                         
         02  NOM-TACHE           PIC X(4)  VALUE SPACE.                         
         02  NOM-PROG-XCTL       PIC X(8)  VALUE SPACE.                         
         02  NOM-PROG-LINK       PIC X(8)  VALUE SPACE.                         
         02  NOM-PROG-TACHE  REDEFINES NOM-PROG-LINK                            
                                 PIC X(8).                                      
         02  NOM-TACHE-RETOUR    PIC X(4)  VALUE SPACE.                         
         02  NOM-TACHE-START     PIC X(4)  VALUE SPACE.                         
         02  TERM-START          PIC X(4)  VALUE SPACE.                         
         02  NOM-TD              PIC X(4)  VALUE SPACE.                         
         02  IDENT-TS            PIC X(8)  VALUE SPACE.                         
         02  NOM-LEVEL-MAX       PIC X(4)  VALUE SPACE.                         
      *                                                                         
         02  CODE-TRAITEMENT-NORMAL        PIC X(2)  VALUE '01'.                
         02  CODE-TRAITEMENT-CRITERES      PIC X(2)  VALUE '11'.                
         02  CODE-TRAITEMENT-DATAS         PIC X(2)  VALUE '22'.                
         02  CODE-TRAITEMENT-AUTOMATIQUE   PIC X(2)  VALUE '10'.                
         02  CODE-PREMIERE                 PIC X(2)  VALUE '05'.                
         02  CODE-DERNIERE                 PIC X(2)  VALUE '06'.                
         02  CODE-PRECEDENTE               PIC X(2)  VALUE '07'.                
         02  CODE-SUIVANTE                 PIC X(2)  VALUE '08'.                
         02  CODE-LEVEL-MAX                PIC X(2)  VALUE 'KA'.                
         02  CODE-LEVEL-SUP                PIC X(2)  VALUE 'KC'.                
         02  CODE-HELP                     PIC X(2)  VALUE 'KB'.                
         02  CODE-SWAP                     PIC X(2)  VALUE 'KS'.                
         02  CODE-JUMP                     PIC X(2)  VALUE 'KJ'.                
         02  CODE-TOUCHE-CURS              PIC X(2)  VALUE 'KX'.                
         02  CODE-ERREUR-MANIPULATION      PIC X(2)  VALUE 'FF'.                
         02  CODE-SUSPENSION               PIC X(2)  VALUE '03'.                
         02  CODE-FIN                      PIC X(2)  VALUE '04'.                
      *                                                                         
         02  CODE-CREATION                 PIC X     VALUE 'C'.                 
         02  CODE-MODIFICATION             PIC X     VALUE 'M'.                 
         02  CODE-INTERROGATION            PIC X     VALUE 'I'.                 
         02  CODE-SUPPRESSION              PIC X     VALUE 'S'.                 
      *                                                                         
         02  CODE-FIN-BOUCLE-IA        PIC S9(3) COMP-3 VALUE +999.             
         02  CODE-FIN-BOUCLE-AI        PIC S9(3) COMP-3 VALUE +999.             
         02  CODE-FIN-TABLE            PIC S9(3) COMP-3 VALUE +999.             
      *                                                                         
         02  CODE-CONSULTATION-EN-COURS    PIC X     VALUE '0'.                 
         02  CODE-FIN-CONSULTATION         PIC X     VALUE '1'.                 
      *                                                                         
         02  APOSTROPHE                    PIC X     VALUE QUOTE.               
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  BINAIRE                       PIC S9(4) COMP VALUE +0.             
      *--                                                                       
         02  BINAIRE                       PIC S9(4) COMP-5 VALUE +0.           
      *}                                                                        
         02  REDEF-BINAIRE REDEFINES BINAIRE PIC X(2).                          
      *                                                                         
         02  W-TRACE-CICS                  PIC X(50) VALUE SPACE.               
         02  LIGNE                        PIC X(131) VALUE SPACE.               
      *                                                                         
      *==> DARTY ******************************************************         
      *          * ZONES   SWAP + HELP                                *         
      ****************************************************** SYKWDIV0 *         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  AIDA-LONG-TS        PIC S9(4) COMP VALUE +6496.                    
      *--                                                                       
         02  AIDA-LONG-TS        PIC S9(4) COMP-5 VALUE +6496.                  
      *}                                                                        
         02  AIDA-IDENT-TS.                                                     
             04  AIDA-IDENT-TS-A PIC X(4).                                      
             04  AIDA-IDENT-TS-B PIC X(4).                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  AIDA-RANG-TS        PIC S9(4) COMP VALUE +0.                       
      *--                                                                       
         02  AIDA-RANG-TS        PIC S9(4) COMP-5 VALUE +0.                     
      *}                                                                        
      *                                                                         
         02  NOM-PROG-XCTL-SWAP.                                                
             04  NOM-PCX0        PIC X.                                         
             04  NOM-TACHE-SWAP  PIC X(4).                                      
             04  NOM-PCX1        PIC X(3).                                      
       EJECT                                                                    
                                                                                
