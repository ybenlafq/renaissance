      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ****************************************************           
      *          * ZONES ADRESSABLES EXTERNES A LA TACHE            *           
      **************************************************** SYKLINKB *           
      *                                                                         
      *                      CWA                                                
       01  LINK-CWA          PIC X(512).                                        
      *                      TWA                                                
       01  LINK-TWA          PIC X(500).                                        
      *                      TCTUA                                              
       01  LINK-TCTUA        PIC X(255).                                        
      *                      USER                                               
      *01  LINK-USER         PIC X(4000).                                       
      *                            ZONE POUR AIDA                               
       01   LINK-TS.                                                            
            02  LINK-TS-MAP        PIC X(2400).                                 
            02  LINK-TS-COMMAREA   PIC X(4096).                                 
            02  LINK-TS-SWAP-HELP  REDEFINES LINK-TS-COMMAREA.                  
                05 LINK-TS-CODERR        PIC X(04).                             
                05 LINK-TS-PGMPRC        PIC X(08).                             
                05 LINK-TS-NOM-MAP       PIC X(08).                             
                05 LINK-TS-NOM-MAPSET    PIC X(08).                             
                05 LINK-TS-NOM-TACHE     PIC X(04).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-COM-LONG      PIC S9(4) COMP.                        
      *--                                                                       
                05 LINK-TS-COM-LONG      PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-MAP-LONG      PIC S9(4) COMP.                        
      *--                                                                       
                05 LINK-TS-MAP-LONG      PIC S9(4) COMP-5.                      
      *}                                                                        
                05 LINK-TS-SELECT.                                              
                   08 LINK-TS-SELECTION  PIC X(04) OCCURS 3.                    
                05 LINK-TS-TACHE-JUMP    PIC X(04).                             
                05 FILLER                PIC X(48).                             
                05 LINK-TS-CICS          PIC X(20).                             
                05 LINK-TS-DATE          PIC X(100).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 LINK-TS-POS-CURS      PIC S9(4) COMP.                        
      *--                                                                       
                05 LINK-TS-POS-CURS      PIC S9(4) COMP-5.                      
      *}                                                                        
                05 LINK-FILLER           PIC X(3874).                           
           EJECT                                                                
                                                                                
                                                                                
