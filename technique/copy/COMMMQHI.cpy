      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      * TRAITEMENTS MQ - COTE HOST                                              
      * UTILISEE PAR TMQHI/MD/LO ET APPLICATIFS                                 
      *****************************************************************         
      * ENTREE : 24 OCTETS                                                      
      * SORTIE : 24 + 2 + MESSAGE                                               
E0032 ******************************************************************        
      * DSA057 11/11/03 SUPPORT EVOLUTION                                       
      *                 REFONTE 1 SEUL PROG TMQHI/MI/LO/RC                      
      *                 SELON TRANSID APPELLANTE                                
       01  COMM-MQHI-APPLI REDEFINES Z-COMMAREA-LINK.                           
            05  COMM-MQHI-ENTREE.                                               
               10  COMM-MQHI-MSGID           PIC X(24).                         
               10  COMM-MQHI-CORRELID        PIC X(24).                         
            05  COMM-MQHI-SORTIE.                                               
               10  COMM-MQHI-CODRET          PIC X(2).                          
               10  COMM-MQHI-MESSAGE.                                           
                 12 COMM-MQHI-ENTETE.                                           
                  15 COMM-MQHI-TYPE PIC X(3).                                   
                  15 COMM-MQHI-NSOCMSG PIC X(3).                                
                  15 COMM-MQHI-NLIEUMSG PIC X(3).                               
                  15 COMM-MQHI-NSOCDST PIC X(3).                                
                  15 COMM-MQHI-NLIEUDST PIC X(3).                               
                  15 COMM-MQHI-NORD PIC 9(8).                                   
                  15 COMM-MQHI-LPROG PIC X(10).                                 
                  15 COMM-MQHI-DJOUR PIC X(8).                                  
                  15 COMM-MQHI-WSID PIC X(10).                                  
                  15 COMM-MQHI-USER PIC X(10).                                  
                  15 COMM-MQHI-CHRONO PIC 9(7).                                 
                  15 COMM-MQHI-NBRMSG PIC 9(7).                                 
                  15 COMM-MQHI-NBRENR PIC 9(5).                                 
                  15 COMM-MQHI-TAILLE PIC 9(5).                                 
E0032 *           15 COMM-MQHI-FILLER PIC X(20).                                
E0032             15 COMM-MQHI-FILLER PIC X(15).                                
E0032             15 COMM-MQHI-CPROG  PIC X(05).                                
                 12 COMM-MQHI-MESS PIC X(49676).                                
                                                                                
