      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: entete dtd                                                 00000020
      ***************************************************************** 00000030
       01   EXM02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      * filtre sur la dtd                                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTDFL    COMP PIC S9(4).                                 00000230
      *--                                                                       
           02 MDTDFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDTDFF    PIC X.                                          00000240
           02 FILLER    PIC X(4).                                       00000250
           02 MDTDFI    PIC X(20).                                      00000260
      * la liste des dtd                                                00000270
           02 MLDTDI OCCURS   10 TIMES .                                00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCHOIXL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLCHOIXL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCHOIXF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLCHOIXI     PIC X.                                     00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTDL   COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 MDTDL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDTDF   PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MDTDI   PIC X(20).                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLGDATAL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MLGDATAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLGDATAF     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLGDATAI     PIC X(5).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPROLOGUEL   COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MPROLOGUEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPROLOGUEF   PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MPROLOGUEI   PIC X(50).                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLIBERRI  PIC X(74).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCODTRAI  PIC X(4).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MCICSI    PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNETNAMI  PIC X(8).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MSCREENI  PIC X(4).                                       00000640
      ***************************************************************** 00000650
      * SDF: entete dtd                                                 00000660
      ***************************************************************** 00000670
       01   EXM02O REDEFINES EXM02I.                                    00000680
           02 FILLER    PIC X(12).                                      00000690
           02 FILLER    PIC X(2).                                       00000700
           02 MDATJOUA  PIC X.                                          00000710
           02 MDATJOUC  PIC X.                                          00000720
           02 MDATJOUP  PIC X.                                          00000730
           02 MDATJOUH  PIC X.                                          00000740
           02 MDATJOUV  PIC X.                                          00000750
           02 MDATJOUO  PIC X(10).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MTIMJOUA  PIC X.                                          00000780
           02 MTIMJOUC  PIC X.                                          00000790
           02 MTIMJOUP  PIC X.                                          00000800
           02 MTIMJOUH  PIC X.                                          00000810
           02 MTIMJOUV  PIC X.                                          00000820
           02 MTIMJOUO  PIC X(5).                                       00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MPAGEA    PIC X.                                          00000850
           02 MPAGEC    PIC X.                                          00000860
           02 MPAGEP    PIC X.                                          00000870
           02 MPAGEH    PIC X.                                          00000880
           02 MPAGEV    PIC X.                                          00000890
           02 MPAGEO    PIC X(3).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MPAGEMAXA      PIC X.                                     00000920
           02 MPAGEMAXC PIC X.                                          00000930
           02 MPAGEMAXP PIC X.                                          00000940
           02 MPAGEMAXH PIC X.                                          00000950
           02 MPAGEMAXV PIC X.                                          00000960
           02 MPAGEMAXO      PIC X(3).                                  00000970
      * filtre sur la dtd                                               00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDTDFA    PIC X.                                          00001000
           02 MDTDFC    PIC X.                                          00001010
           02 MDTDFP    PIC X.                                          00001020
           02 MDTDFH    PIC X.                                          00001030
           02 MDTDFV    PIC X.                                          00001040
           02 MDTDFO    PIC X(20).                                      00001050
      * la liste des dtd                                                00001060
           02 MLDTDO OCCURS   10 TIMES .                                00001070
             03 FILLER       PIC X(2).                                  00001080
             03 MLCHOIXA     PIC X.                                     00001090
             03 MLCHOIXC     PIC X.                                     00001100
             03 MLCHOIXP     PIC X.                                     00001110
             03 MLCHOIXH     PIC X.                                     00001120
             03 MLCHOIXV     PIC X.                                     00001130
             03 MLCHOIXO     PIC X.                                     00001140
             03 FILLER       PIC X(2).                                  00001150
             03 MDTDA   PIC X.                                          00001160
             03 MDTDC   PIC X.                                          00001170
             03 MDTDP   PIC X.                                          00001180
             03 MDTDH   PIC X.                                          00001190
             03 MDTDV   PIC X.                                          00001200
             03 MDTDO   PIC X(20).                                      00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MLGDATAA     PIC X.                                     00001230
             03 MLGDATAC     PIC X.                                     00001240
             03 MLGDATAP     PIC X.                                     00001250
             03 MLGDATAH     PIC X.                                     00001260
             03 MLGDATAV     PIC X.                                     00001270
             03 MLGDATAO     PIC X(5).                                  00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MPROLOGUEA   PIC X.                                     00001300
             03 MPROLOGUEC   PIC X.                                     00001310
             03 MPROLOGUEP   PIC X.                                     00001320
             03 MPROLOGUEH   PIC X.                                     00001330
             03 MPROLOGUEV   PIC X.                                     00001340
             03 MPROLOGUEO   PIC X(50).                                 00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MLIBERRA  PIC X.                                          00001370
           02 MLIBERRC  PIC X.                                          00001380
           02 MLIBERRP  PIC X.                                          00001390
           02 MLIBERRH  PIC X.                                          00001400
           02 MLIBERRV  PIC X.                                          00001410
           02 MLIBERRO  PIC X(74).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCODTRAA  PIC X.                                          00001440
           02 MCODTRAC  PIC X.                                          00001450
           02 MCODTRAP  PIC X.                                          00001460
           02 MCODTRAH  PIC X.                                          00001470
           02 MCODTRAV  PIC X.                                          00001480
           02 MCODTRAO  PIC X(4).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCICSA    PIC X.                                          00001510
           02 MCICSC    PIC X.                                          00001520
           02 MCICSP    PIC X.                                          00001530
           02 MCICSH    PIC X.                                          00001540
           02 MCICSV    PIC X.                                          00001550
           02 MCICSO    PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNETNAMA  PIC X.                                          00001580
           02 MNETNAMC  PIC X.                                          00001590
           02 MNETNAMP  PIC X.                                          00001600
           02 MNETNAMH  PIC X.                                          00001610
           02 MNETNAMV  PIC X.                                          00001620
           02 MNETNAMO  PIC X(8).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MSCREENA  PIC X.                                          00001650
           02 MSCREENC  PIC X.                                          00001660
           02 MSCREENP  PIC X.                                          00001670
           02 MSCREENH  PIC X.                                          00001680
           02 MSCREENV  PIC X.                                          00001690
           02 MSCREENO  PIC X(4).                                       00001700
                                                                                
