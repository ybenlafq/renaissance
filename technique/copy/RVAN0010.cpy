      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      * COPY DE LA TABLE RVRVAN0010                                             
      **********************************************************                
      * LISTE DES HOST VARIABLES DE LA TABLE RVRVAN0010                         
            EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01 RVAN0010.                                                             
           02 AN00-CNOMPGRM PIC X(0006).                                        
           02 AN00-NSEQERR  PIC X(0004).                                        
           02 AN00-LIBERR   PIC X(0100).                                        
           02 AN00-DSYST    PIC S9(13) COMP-3.                                  
           02 AN00-CODRET   PIC X(0004).                                        
           02 AN00-ENRID    PIC X(0024).                                        
      **********************************************************                
      * LISTE DES FLAGS DE LA TABLE RVRVAN0010                                  
       01 RVAN0010-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-CNOMPGRM-F PIC S9(4) COMP.                                   
      *--                                                                       
           02 AN00-CNOMPGRM-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-NSEQERR-F  PIC S9(4) COMP.                                   
      *--                                                                       
           02 AN00-NSEQERR-F  PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-LIBERR-F   PIC S9(4) COMP.                                   
      *--                                                                       
           02 AN00-LIBERR-F   PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-DSYST-F    PIC S9(4) COMP.                                   
      *--                                                                       
           02 AN00-DSYST-F    PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-CODRET-F   PIC S9(4) COMP.                                   
      *--                                                                       
           02 AN00-CODRET-F   PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 AN00-ENRID-F    PIC S9(4) COMP.                                   
      *                                                                         
      *--                                                                       
           02 AN00-ENRID-F    PIC S9(4) COMP-5.                                 
            EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
