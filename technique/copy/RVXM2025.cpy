      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DEVNCG00.RVXM2025                  *        
      ******************************************************************        
       01  RVXM2025.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2025-REFID           USAGE SQL TYPE IS ROWID.                   
      *--                                                                       
           10 XM2025-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2025-NCODIC          PIC X(07).                                 
           10 XM2025-NCODICLIE       PIC X(07).                                 
           10 XM2025-CTYPLIEN        PIC X(05).                                 
           10 XM2025-QTYPLIEN        PIC S9(3)       COMP-3.                    
           10 XM2025-WDEGRELIB       PIC X(05).                                 
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVXM2025-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-REFID-F         PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2025-REFID-F         PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-NCODIC-F        PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2025-NCODIC-F        PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-NCODICLIE-F     PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2025-NCODICLIE-F     PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-CTYPLIEN-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2025-CTYPLIEN-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-QTYPLIEN-F      PIC S9(4) COMP.                            
      *--                                                                       
           10 XM2025-QTYPLIEN-F      PIC S9(4) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 XM2025-WDEGRELIB-F     PIC S9(4) COMP.                            
      *                                                                         
      *--                                                                       
           10 XM2025-WDEGRELIB-F     PIC S9(4) COMP-5.                          
                                                                                
      *}                                                                        
