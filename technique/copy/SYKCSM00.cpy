      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000000
      *==> DARTY ****************************************************** 00000105
      *          * ENVOI MAP SIMPLE (MODIFIE AVEC CURSEUR)            * 00000205
      ****************************************************** SYKCSM00 * 00000305
      *---------------------------------------                          00000400
       SEND-MAP                        SECTION.                         00000500
      *---------------------------------------                          00000600
           MOVE SPACES TO COM-CODERR.                                   00000700
      *                                                                 00000800
           PERFORM INQUIRE-TRANSACTION.                                         
      *                                                                 00000800
      *    IF NOM-PROG-TACHE = 'TGM99'                                          
      *       PERFORM ENVOI-TS-MAP                                              
      *    ELSE                                                                 
              PERFORM ENVOI-MAP.                                                
      *     END-IF.                                                             
      *---------------------------------------                                  
       ENVOI-TS-MAP                    SECTION.                                 
      *---------------------------------------                                  
           SET  COMGM00-ERASE       TO TRUE.                                    
           PERFORM MAJ-TS-MAP.                                                  
      *---------------------------------------                          00000400
       ENVOI-MAP                       SECTION.                         00000500
      *---------------------------------------                          00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND                                                           
                 MAP    (NOM-MAP)                                               
                 MAPSET (NOM-MAPSET)                                            
                 FROM   (Z-MAP)                                                 
                 LENGTH (LENGTH OF Z-MAP)                                       
                 ERASE                                                          
                 CURSOR                                                         
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�1                                                    
      *dfhei*0025   ' TO DFHEIV0                                                
      *dfhei*     MOVE LENGTH OF Z-MAP TO DFHB0020                              
      *dfhei*     MOVE -1 TO DFHB0021                                           
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-MAP DFHB0020           
      *dfhei*     NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY DFHB0021.               
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001800
           MOVE EIBRCODE TO EIB-RCODE.                                  00001900
      *                                                                 00002005
           IF   NOT EIB-NORMAL                                          00002100
                GO TO ABANDON-CICS                                      00002200
           END-IF.                                                      00002300
      *---------------------------------------                          00002400
       INQUIRE-TRANSACTION             SECTION.                         00002500
      *---------------------------------------                          00002600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS INQUIRE TRANSACTION(NOM-TACHE)                                 
                         PROGRAM(NOM-PROG-TACHE)                                
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '&dd00043   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-TACHE DFHDUMMY DFHDUM        
      *dfhei*     DFHDUMMY DFHDUMMY NOM-PROG-TACHE.                             
      *dfhei*} decommente EXEC                                                  
      *---------------------------------------                                  
       MAJ-TS-MAP                      SECTION.                                 
      *---------------------------------------                                  
           MOVE LENGTH OF Z-MAP  TO LONG-MAP.                                   
           MOVE NOM-TACHE        TO Z-COMMAREA-NOM-TACHE.                       
           MOVE NOM-MAP          TO Z-COMMAREA-NOM-MAP.                         
           MOVE NOM-MAPSET       TO Z-COMMAREA-NOM-MAPSET.                      
           MOVE LONG-MAP         TO Z-COMMAREA-LONG-MAP.                        
           MOVE NOM-PROG         TO COM-PGMPRC.                                 
           MOVE NOM-PROG         TO NOM-PROG-TACHE.                             
           MOVE NOM-PROG         TO NOM-PROG-XCTL.                              
           STRING 'GMMP'                                                        
                  EIBTRMID                                                      
                  DELIMITED BY SIZE INTO IDENT-TS.                              
           MOVE LONG-MAP            TO LONG-TS.                                 
           MOVE 1                   TO RANG-TS.                                 
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS QUEUE(IDENT-TS)                                      
                           FROM(Z-MAP)                                          
                           LENGTH(LONG-TS)                                      
                           ITEM(RANG-TS)                                        
                           REWRITE                                              
                           NOHANDLE                                             
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00064   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-MAP LONG-TS DF        
      *dfhei*     RANG-TS.                                                      
           IF EIBRESP NOT = ZERO                                                
              PERFORM ECRITURE-TS-MAP                                           
           END-IF.                                                              
           MOVE KONTROL             TO COMGM00-KONTROL.                         
           PERFORM XCTL-PROG-COMMAREA.                                          
      *---------------------------------------                                  
       ECRITURE-TS-MAP                 SECTION.                                 
      *---------------------------------------                                  
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS QUEUE(IDENT-TS)                                      
                           FROM(Z-MAP)                                          
                           LENGTH(LONG-TS)                                      
                           MAIN                                                 
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '���00084   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-MAP LONG-TS.          
      *dfhei*} decommente EXEC                                                  
                                                                                
                                                                                
