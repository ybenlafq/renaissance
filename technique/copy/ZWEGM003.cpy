      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *================================================================*        
      *    ZONE DE TRANSMISSION D'INFORMATIONS ENTRE LE PROGRAMME      *        
      *    APPELANT ET LE SOUS PROGRAMME DE CALCULS                    *        
      *    TOUTES LES ZONES DOIVENT ETRE RENSEIGNEES A L'EXECPTION     *        
      *    DE LA ZONE ZW-CONTENUR DANS LAQUELLE LE SOUS PROGRAMME      *        
      *    REND LE RESULTAT.                                           *        
      *----------------------------------------------------------------*        
      * APPEL AU SOUS PROGRAMME : CALL 'BEGM003' USING ZWORK-CALCULS   *        
      *================================================================*        
       01  ZWORK-CALCULS.                                                       
           05  ZW-TYPCALCUL                PIC XX.                              
           05  ZW-TYPCHAMP1                PIC X.                               
           05  ZW-LGCHAMP1                 PIC S999   COMP-3.                   
           05  ZW-DECCHAMP1                PIC S999   COMP-3.                   
           05  ZW-CONTENU1                 PIC X(15).                           
           05  ZW-TYPCHAMP2                PIC X.                               
           05  ZW-LGCHAMP2                 PIC S999   COMP-3.                   
           05  ZW-DECCHAMP2                PIC S999   COMP-3.                   
           05  ZW-CONTENU2                 PIC X(15).                           
           05  ZW-TYPCHAMPR                PIC X.                               
           05  ZW-LGCHAMPR                 PIC S999   COMP-3.                   
           05  ZW-DECCHAMPR                PIC S999   COMP-3.                   
           05  ZW-CONTENUR                 PIC X(15).                           
                                                                                
