      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DATE/HEURE DU DEBUT DE TRANSACTION                                
      *****************************************************************         
      *                                                                         
       01  FILLER PIC X(16) VALUE '* ZONE-TIMER *'.                             
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  Z-TIMER.                                                           
      *                                                                         
           05  Z-TIMER-DATJOU-FILE.                                             
             10  SS                           PIC 9(02) VALUE 0.                
             10  AA                           PIC 9(02) VALUE 0.                
             10  MM                           PIC 9(02) VALUE 0.                
             10  JJ                           PIC 9(02) VALUE 0.                
      *                                                                         
           05  Z-TIMER-DATJOU.                                                  
             10  JJ                           PIC 9(02) VALUE 0.                
             10  FILLER-DATJOU-1              PIC X     VALUE '/'.              
             10  MM                           PIC 9(02) VALUE 0.                
             10  FILLER-DATJOU-2             PIC X     VALUE '/'.               
             10  AA                           PIC 9(02) VALUE 0.                
      *                                                                         
           05  Z-TIMER-TIMJOU.                                                  
             10  HH                           PIC 9(02) VALUE  0.               
             10  FILLER-TIMJOU-1              PIC X     VALUE 'H'.              
             10  MM                           PIC 9(02) VALUE  0.               
             10  FILLER-TIMJOU-2              PIC X     VALUE 'M'.              
             10  SS                           PIC 9(02) VALUE  0.               
             10  FILLER-TIMJOU-3              PIC X     VALUE 'S'.              
           05  FILLER REDEFINES Z-TIMER-TIMJOU.                                 
             10  Z-TIMER-000HHMMSS            PIC 9(9).                         
           05  FILLER REDEFINES Z-TIMER-TIMJOU.                                 
             10  FILLER                       OCCURS 9.                         
               15  Z-TIMER-TIMJOU-CAR         PIC X.                            
       EJECT                                                                    
                                                                                
                                                                                
