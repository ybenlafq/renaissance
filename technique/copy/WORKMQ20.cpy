      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   ENVOI MESSAGE MQ HOST --> LOCAL PAR BATCH                             
      *   ZONES PASSES EN PARAMETRE DU CALL BMQ020                              
      *****************************************************************         
      *                                                                         
      *                                                                         
      * CODRET : 1 = CONNECT NON OK                                             
      *          2 = MQOPEN NON OK                                              
      *          3 = MQPUT    //                                                
      *          4 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          5 = SELECT SS-TABLE NCGFC NON TROUVE                           
      *          6 = MQCLOSE NON OK                                             
      *          7 = DISCONNECT NON OK                                          
      *          8 = SQLCODE < 0                                                
      *          9 = ERREUR BETDATC                                             
      *                                                                         
       01  WORK-MQ20-APPLI.                                                     
           05  WORK-MQ20-ENTREE.                                                
               10  WORK-MQ20-NSOC            PIC X(03).                         
               10  WORK-MQ20-NSOCDST         PIC X(03).                         
               10  WORK-MQ20-NLIEU           PIC X(03).                         
               10  WORK-MQ20-NLIEUDST        PIC X(03).                         
               10  WORK-MQ20-QALIAS          PIC X(24).                         
      *  ACTION :                                                               
      *    PLUSIEURS MESSAGES (1 PAR CALL)                                      
      *           1 --> CONNECT, OPEN, ET PUT                                   
      *           P --> PUT                                                     
      *           D --> PUT, CLOSE ET DISCONNECT                                
      *           F --> CLOSE ET DISCONNECT                                     
      *    UN SEUL MESSAGE                                                      
      *           U --> CONNECT, OPEN, PUT, CLOSE ET DISCONNECT                 
               10  WORK-MQ20-ACTION          PIC X.                             
               10  WORK-MQ20-CFONC           PIC X(03).                         
               10  WORK-MQ20-NOMPROG         PIC X(06).                         
               10  WORK-MQ20-MSGID           PIC X(24).                         
               10  WORK-MQ20-CORRELID        PIC X(24).                         
               10  WORK-MQ20-PRIORITY        PIC S9(1).                         
               10  WORK-MQ20-SYNCP           PIC X.                             
               10  WORK-MQ20-NB-MESSAGES     PIC 9(5).                          
               10  WORK-MQ20-NBOCC           PIC 9(5).                          
               10  WORK-MQ20-LONG-ENREG      PIC 9(5).                          
           05  WORK-MQ20-SORTIE.                                                
               10  WORK-MQ20-CODRET          PIC XX.                            
               10  WORK-MQ20-ERREUR          PIC X(80).                         
           05  WORK-MQ20-MESS.                                                  
               10  WORK-MQ20-MESSAGE         PIC X(30000).                      
                                                                                
