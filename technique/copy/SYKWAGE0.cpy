      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES DE TRAVAIL DU MODULE DE CALCUL DE L'AGE                 *         
      ****************************************************** SYKWAGE0 *         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W--AGE ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-AGE-DATNAI.                                                    
             10  SS                  PIC 9(2) VALUE 0.                          
             10  AA                  PIC 9(2) VALUE 0.                          
             10  MM                  PIC 9(2) VALUE 0.                          
             10  JJ                  PIC 9(2) VALUE 0.                          
      *                                                                         
           05  W-AGE-DATJOU.                                                    
             10  SS                  PIC 9(2) VALUE 0.                          
             10  AA                  PIC 9(2) VALUE 0.                          
             10  MM                  PIC 9(2) VALUE 0.                          
             10  JJ                  PIC 9(2) VALUE 0.                          
      *                                                                         
           05  W-AGE                 PIC 9(3) VALUE 0.                          
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-AGE-A               PIC S9(3) COMP-3 VALUE +0.                 
           05  W-AGE-M               PIC S9(3) COMP-3 VALUE +0.                 
           05  W-AGE-J               PIC S9(3) COMP-3 VALUE +0.                 
      *                                                                         
       EJECT                                                                    
                                                                                
                                                                                
