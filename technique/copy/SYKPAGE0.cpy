      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      *****************************************************************         
      *                                                                         
      * CALCUL DE L'AGE                                                         
      *                                                                         
      *****************************************************************         
       TRAITEMENT-AGE SECTION.                                                  
      *****************************************************************         
      *                                                                         
       DEB-TRAITEMENT-AGE.                                                      
      * DIFFERENCE DES ANNEES                                                   
           COMPUTE W-AGE-A =                                                    
              100 * (SS OF W-AGE-DATJOU - SS OF W-AGE-DATNAI)                   
                   + AA OF W-AGE-DATJOU - AA OF W-AGE-DATNAI.                   
      * DIFFERENCE DES MOIS                                                     
           COMPUTE W-AGE-M =                                                    
                     MM OF W-AGE-DATJOU - MM OF W-AGE-DATNAI.                   
      * DIFFERENCE DES JOURS                                                    
           COMPUTE W-AGE-J =                                                    
                     JJ OF W-AGE-DATJOU - JJ OF W-AGE-DATNAI.                   
      * CORRECTIONS                                                             
           IF W-AGE-J < 0                                                       
                   ADD      30 TO   W-AGE-J                                     
                   SUBTRACT 1  FROM W-AGE-M                                     
           END-IF.                                                              
           IF W-AGE-M < 0                                                       
                   ADD      12 TO   W-AGE-M                                     
                   SUBTRACT 1  FROM W-AGE-A                                     
           END-IF.                                                              
           IF W-AGE-M > 5                                                       
                   ADD      1  TO   W-AGE-A                                     
           END-IF.                                                              
           MOVE W-AGE-A TO W-AGE.                                               
      *                                                                         
       FIN-TRAITEMENT-AGE. EXIT.                                                
       EJECT                                                                    
                                                                                
                                                                                
