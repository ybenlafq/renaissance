      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION POUR VUE RVXM2031                            *        
      ******************************************************************        
       01  RVXM2031.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 XM2031-REFID                USAGE SQL TYPE IS ROWID.              
      *--                                                                       
           10 XM2031-REFID PIC X(40).                                           
      *}                                                                        
           10 XM2031-CPREST.                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2031-CPREST-LEN        PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2031-CPREST-LEN        PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2031-CPREST-TEXT       PIC X(5).                             
           10 XM2031-NENTCDE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2031-NENTCDE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2031-NENTCDE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2031-NENTCDE-TEXT      PIC X(5).                             
           10 XM2031-WENTCDE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 XM2031-WENTCDE-LEN       PIC S9(4) USAGE COMP.                 
      *--                                                                       
              49 XM2031-WENTCDE-LEN       PIC S9(4) COMP-5.                     
      *}                                                                        
              49 XM2031-WENTCDE-TEXT      PIC X(1).                             
           10 XM2031-QDELAIAPPRO          PIC S9(3) COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVXM2031                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVXM2031-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2031-CPREST-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2031-CPREST-F            PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2031-NENTCDE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2031-NENTCDE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2031-WENTCDE-F           PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2031-WENTCDE-F           PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 XM2031-QDELAIAPPRO-F       PIC S9(4) COMP.                        
      *--                                                                       
           02 XM2031-QDELAIAPPRO-F       PIC S9(4) COMP-5.                      
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
                                                                                
