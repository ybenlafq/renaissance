      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES PASSES EN PARAMETRE POUR LE MAN00                                 
MAINT *01 COMAN00-LONGUEUR PIC S9(4) COMP VALUE +105.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *01 COMAN00-LONGUEUR PIC S9(4) COMP VALUE +138.                           
      *--                                                                       
       01 COMAN00-LONGUEUR PIC S9(4) COMP-5 VALUE +138.                         
      *}                                                                        
       01 Z-COMAN00.                                                            
            15 COMAN00-ENTREE.                                                  
               20 COMAN00-CNOMPGRM PIC X(6).                                    
               20 COMAN00-NSEQERR PIC X(4).                                     
               20 COMAN00-LLIBRE PIC X(50).                                     
            15 COMAN00-SORTIE.                                                  
               20 COMAN00-CRET PIC X(4).                                        
MAINT-*        20 COMAN00-LRET PIC X(41).                                       
               20 COMAN00-LRET PIC X(50).                                       
            15 COMAN00-REF.                                                     
-MAINT         20 COMAN00-ENRID PIC X(24).                                      
                                                                                
