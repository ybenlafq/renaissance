      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       EJECT                                                                    
      * SYBEDITI **************************************** 29-05-89 *            
      **************************************************************            
      ***************** EDITION D'UN ETAT BATCH ********************            
      **************************************************************            
      *************************************************** SYBEDITI *            
      *                  -------------------                       *            
      *------------------- INTERFACE BATCH ------------------------*            
      *                  -------------------                       *            
      *                                                            *            
      * CE SOUS-PROGRAMME GENERALISE REALISE L'INTERFACE D'EDITION *            
      * DES ETATS ENTRE UN PROGRAMME BATCH ET L'APPLICATION DE     *            
      * TRAITEMENT DES ETATS SOUS CICS.                            *            
      *                                                            *            
      * LE PROGRAMME BATCH APPELANT DOIT ETRE DEVELOPPE :          *            
      *       - EN COBOL 2                                         *            
      *       - AVEC DL1 SOUS LA FORME EXEC DLI                    *            
      *                                                            *            
      * LA ZONE DE WORKING ASSOCIEE A CETTE BRIQUE PROCEDURALE     *            
      * SE TROUVE SOUS COPY MIGWCOB2                               *            
      *                                                            *            
      *------------------------------------------------------------*            
       EJECT                                                                    
      *===============================================================          
      *              TRAITEMENT DE LA PROCEDURE                      *          
      *==================================================== SYBEDITI *          
       MIGC02-BATCH  SECTION.                                                   
           MOVE SPACES    TO  CODRUP.                                           
           MOVE SPACES    TO  IG8-ERR.                                          
      *----TRAITEMENT DE LA DATE DU JOUR------------------------------          
           IF IG-DAT-JOU = '000000'                                             
              MOVE '4' TO  GFDATA                                               
              CALL 'BETDATC'  USING  WORK-BETDATC                               
              IF GFVDAT NOT = '1'                                               
                 MOVE 'MIGC02 : ERREUR DATE DU JOUR'  TO  ABEND-MESS            
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
              MOVE GFAMJ-1  TO  IG-DAT-JOU                                      
           END-IF.                                                              
      *----CONTROLE NUMERO MASQUE PCB EDITION ------------------------          
           IF IG-CTL-PCB  NOT =  '*'  OR  ETAT-PSB  NOT =  'K'                  
              IF IG9-PCB  <  1  OR  > 9                                         
                 MOVE 'MIGC02 : ERREUR VALEUR IG9-PCB'  TO  ABEND-MESS          
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
              MOVE  '*'      TO  IG-CTL-PCB                                     
              MOVE  IG9-PCB  TO  NUM-PCB                                        
              MOVE  2        TO  NOMBRE                                         
           END-IF.                                                              
           IF NUM-PCB  NOT =  IG9-PCB                                           
              MOVE 'MIGC02 : ERREUR VALEUR IG9-PCB'  TO  ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
      *----SYNTAXE CARACTERE TRAITEMENT EXISTANT ---------------------          
           IF IG7-SUP  NOT =  ' '  AND  'D'  AND  'S'  AND  'A'                 
                                   AND  'L'  AND  'F'                           
              MOVE 'MIGC02 : IG7-SUP ERREUR'         TO  ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           IF IG7-SUP  NOT =  ' '                                               
              MOVE '*'  TO  CODRUP                                              
           END-IF.                                                              
       EJECT                                                                    
      * TRAITEMENT DES RUPTURES SUR CLE ETAT ----------------- SYBEDITI         
      *----RUPTURE SUR NON ETAT-------------------------------                  
           IF IG1-ETA      NOT =  IDT-KEY-NOM                                   
              MOVE SPACES   TO    IDT-KEY-DLI                                   
              PERFORM             IG-NOM-ETAT-0                                 
              PERFORM             DLI-GU-DIGSA-IDT                              
              PERFORM             IG-SAUVEGARDE                                 
              MOVE '*'      TO    CODRUP                                        
              MOVE IG1-ETA  TO    IDT-KEY-NOM                                   
           END-IF.                                                              
           IF SAU-PTC = 'T'                                                     
              THEN PERFORM IG-PTC-TEXTE                                         
              ELSE PERFORM IG-PTC-LASGRA                                        
           END-IF.                                                              
      *----RUPTURE SUR DATE D'EDITION-------------------------                  
           IF IDT-KEY-DAT  NOT =  IG2-DAT                                       
              PERFORM             IG-DATE-EDITION                               
              MOVE '*'      TO    CODRUP                                        
              MOVE IG2-DAT  TO    IDT-KEY-DAT                                   
           END-IF.                                                              
      *----RUPTURE DESTINATION ETAT---------------------------                  
           IF IDT-KEY-DST  NOT =  IG3-DST                                       
              PERFORM             IG-DESTINATION                                
              IF TOP-CFG = '*'                                                  
                 PERFORM          DLI-GU-DIGSA-IDT                              
              END-IF                                                            
              IF IG3-DST  NOT =  '000000000'                                    
                 PERFORM             DLI-GNP-DIGSE                              
                 IF IG8-ERR   NOT =  SPACES                                     
                    MOVE ' '   TO    IG7-SUP                                    
                    GO TO            FIN-MIGC02-BATCH                           
                 END-IF                                                         
              END-IF                                                            
              MOVE '*'      TO    CODRUP                                        
              MOVE IG3-DST  TO    IDT-KEY-DST                                   
           END-IF.                                                              
      *----RUPTURE DOCUMENT ETAT------------------------------                  
           IF IDT-KEY-DOC  NOT =  IG4-DOC                                       
              PERFORM             IG-DOCUMENT                                   
              MOVE '*'      TO    CODRUP                                        
              MOVE IG4-DOC  TO    IDT-KEY-DOC                                   
           END-IF.                                                              
       EJECT                                                                    
      * TRAITEMENT EN FONCTION DES RUPTURES------------------- SYBEDITI         
           IF CODRUP = '*'                                                      
              PERFORM DLI-GU-DIGSA-CFG                                          
              IF IG8-ERR  NOT =  SPACES                                         
                 MOVE ' '  TO  IG7-SUP                                          
                 GO TO FIN-MIGC02-BATCH                                         
              END-IF                                                            
              IF CODDLI = 'DLET'                                                
                 PERFORM DLI-DLET-DIGSA-CFG                                     
                 IF IG7-SUP  =  'S'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO FIN-MIGC02-BATCH                                      
                 END-IF                                                         
              END-IF                                                            
              IF CODDLI = 'ISRT'                                                
                 IF IG7-SUP  =  'S'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO FIN-MIGC02-BATCH                                      
                 END-IF                                                         
                 PERFORM DLI-ISRT-DIGSA-CFG                                     
              END-IF                                                            
              IF CODDLI = 'REPL'                                                
                 PERFORM  DLI-REPL-DIGSA-CFG                                    
                 IF IG7-SUP  =  'A'                                             
                    MOVE  ' '  TO  IG7-SUP                                      
                    GO TO FIN-MIGC02-BATCH                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *----CREATION SEGMENT LIGNE                                               
           PERFORM DLI-ISRT-DIGSC.                                              
           MOVE ' '  TO  IG7-SUP.                                               
       FIN-MIGC02-BATCH.     EXIT.                                              
       EJECT                                                                    
      *==============================================================*          
      * SECTION * CONTROLE SYNTAXE NOM ETAT                          *          
      *==================================================== SYBEDITI *          
       IG-NOM-ETAT-0      SECTION.                                              
           IF IG1-ETA1  NOT =  'J'                                              
              MOVE 'MIGC02 : IG1-ETA ERREUR'  TO  ABEND-MESS                    
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           IF IG1-ETA3   IS NOT NUMERIC  OR                                     
              IG1-ETA3   < '1'           OR                                     
              IG1-ETA3   > '9'                                                  
              MOVE 'MIGC02 : IG1-ETA ERREUR'  TO  ABEND-MESS                    
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-IG-NOM-ETAT-0.      EXIT.                                            
       EJECT                                                                    
      *==============================================================*          
      * SECTION * TRAITEMENT SAUVEGARDE IDENTIFICATION               *          
      *==================================================== SYBEDITI *          
       IG-SAUVEGARDE      SECTION.                                              
      *----SAUVEGARDE NUMERO CONFIGURATION                                      
           MOVE DIGZA0-IDT-CNF  TO  SAU-CFG.                                    
           MOVE DIGZA0-IDT-IMP  TO  SAU-IMP.                                    
      *----PROTOCOLE TYPE TEXTE                                                 
           IF DIGZA0-IDT-PTC  = ' '  OR  'T'                                    
              MOVE 'T'             TO  SAU-PTC                                  
              MOVE DIGZA0-IDT-COL  TO  SAU-COL                                  
              GO TO  FIN-IG-SAUVEGARDE                                          
           END-IF.                                                              
      *----PROTOCOLE TYPE LASER OU GRAPHIQUE                                    
           IF DIGZA0-IDT-PTC  = 'L'  OR 'G'                                     
              MOVE DIGZA0-IDT-PTC   TO  SAU-PTC                                 
              MOVE DIGZA0-IDT-CMD   TO  SAU-CMD                                 
              MOVE DIGZA0-IDT-DON   TO  SAU-DON                                 
              COMPUTE SAU-COL = SAU-CMD + SAU-DON                               
              GO TO  FIN-IG-SAUVEGARDE                                          
           END-IF.                                                              
           MOVE 'MIGC02 : DIGZA0-IDT-PTC ERREUR'  TO ABEND-MESS.                
           GO TO ABANDON-BATCH.                                                 
       FIN-IG-SAUVEGARDE.      EXIT.                                            
       EJECT                                                                    
      *--------------------------------------------------------------*          
      * SECTION * TRAITEMENT PROTOCOLE TEXTE                         *          
      *---------------------------------------------------- SYBEDITI *          
       IG-PTC-TEXTE       SECTION.                                              
           IF IG7-SUP  =  'S'  OR  'A'                                          
              GO TO  FIN-IG-PTC-TEXTE                                           
           END-IF.                                                              
           IF IG5-ASA  NOT =  ' '  AND  '1'  AND  '-'  AND                      
                              '0'  AND  '+'                                     
              MOVE 'MIGC02 : IG5-ASA ERREUR TEXTE' TO ABEND-MESS                
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-IG-PTC-TEXTE.         EXIT.                                          
       EJECT                                                                    
      *--------------------------------------------------------------*          
      * SECTION * TRAITEMENT PROTOCOLES LASER ET GRAPHIQUE           *          
      *---------------------------------------------------- SYBEDITI *          
       IG-PTC-LASGRA      SECTION.                                              
           IF IG7-SUP  =  'S'  OR  'A'                                          
              GO TO  FIN-IG-PTC-LASGRA                                          
           END-IF.                                                              
           IF IG5-ASA  NOT =  '$'  AND  '�'  AND  '&'                           
              MOVE 'MIGC02 : IG5-ASA ERREUR L G'     TO ABEND-MESS              
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           IF IG6-LNG  >  132                                                   
              MOVE 'MIGC02 : IG6-LNG GT 132    '     TO ABEND-MESS              
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
      *----TRAITEMENT ZONE DE COMMANDE SEULE                                    
           IF IG5-ASA  = '$'                                                    
              IF IG6-LNG  NOT =  SAU-CMD                                        
                 MOVE 'MIGC02 : IG6-LNG COMMANDE ERREUR' TO  ABEND-MESS         
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
              IF IG6-LIG  =  SPACES  OR  LOW-VALUE                              
                 MOVE 'MIGC02 : IG6-LIG COMMANDE VIDE'   TO  ABEND-MESS         
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
      *----TRAITEMENT ZONE DE DONNEES SEULE                                     
           IF IG5-ASA  = '�'                                                    
              IF IG6-LNG  NOT =  SAU-DON                                        
                 MOVE 'MIGC02 : IG6-LNG DONNEE ERREUR'   TO  ABEND-MESS         
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
      *----TRAITEMENT ZONE DE COMMANDES + DONNEES                               
           IF IG5-ASA  = '&'                                                    
              IF IG6-LNG  NOT =  SAU-COL                                        
                 MOVE 'MIGC02 : IG6-LNG NE SAU-COL'      TO  ABEND-MESS         
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
              IF IG6-LIG  =  SPACES  OR  LOW-VALUE                              
                 MOVE 'MIGC02 : IG6-LIG CMD + DON VIDE'  TO  ABEND-MESS         
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
       FIN-IG-PTC-LASGRA.      EXIT.                                            
       EJECT                                                                    
      *==============================================================*          
      * SECTION * CONTROLE DATE EDITION                              *          
      *==================================================== SYBEDITI *          
       IG-DATE-EDITION      SECTION.                                            
           IF SAU-CFG > '4'                                                     
              MOVE '000000'  TO  IG2-DAT                                        
              GO TO FIN-IG-DATE-EDITION                                         
           END-IF.                                                              
           MOVE '7'                       TO  GFDATA.                           
           MOVE IG2-DAT                   TO  GFAMJ-1.                          
           CALL 'BETDATC'  USING  WORK-BETDATC.                                 
           IF GFVDAT NOT = '1'                                                  
              MOVE 'MIGC02 : ERREUR VALEUR IG2-DAT'  TO  ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           IF IG2-DAT > IG-DAT-JOU                                              
              MOVE 'MIGC02 : ERREUR VALEUR IG2-DAT'  TO  ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-IG-DATE-EDITION.      EXIT.                                          
       EJECT                                                                    
      *==============================================================*          
      * SECTION * VALIDATION PARAMETRE DESTINATION                   *          
      *==================================================== SYBEDITI *          
       IG-DESTINATION      SECTION.                                             
      *----DESTINATION NON SIGNIFICATIVE                                        
           IF SAU-CFG = '3'  OR  '4'  OR  '7'  OR  '8'                          
              MOVE '000000000'  TO  IG3-DST                                     
              GO TO FIN-IG-DESTINATION                                          
           END-IF.                                                              
      *----DESTINATION SIGNIFICATIVE                                            
           IF IG3-DST = '*********'  OR  '000000000'  OR  SPACES                
              MOVE  'MIGC02 : IG3-DST ERREUR'         TO  ABEND-MESS            
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-IG-DESTINATION.      EXIT.                                           
       EJECT                                                                    
      *==============================================================*          
      * SECTION * VALIDATION PARAMETRE DOCUMENT                      *          
      *==================================================== SYBEDITI *          
       IG-DOCUMENT      SECTION.                                                
      *----DOCUMENT NON SIGNIFICATIF                                            
           IF SAU-CFG = '2'  OR  '4'  OR  '6'  OR  '8'                          
              MOVE '000000000000000'  TO  IG4-DOC                               
              GO TO  FIN-IG-DOCUMENT                                            
           END-IF.                                                              
      *----DOCUMENT SIGNIFICATIF                                                
           IF IG4-DOC = '***************' OR                                    
                        '000000000000000' OR                                    
                        SPACES                                                  
              MOVE  'MIGC02 : IG4-DOC ERREUR'         TO  ABEND-MESS            
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-IG-DOCUMENT.          EXIT.                                          
       EJECT                                                                    
      *==============================================================*          
      * SECTION * RECHERCHE IDENTIFICATION ETAT                      *          
      *==================================================== SYBEDITI *          
       DLI-GU-DIGSA-IDT       SECTION.                                          
           MOVE  '0'  TO  TOP-RCN.                                              
           PERFORM DLI-GU-QUAL-EGAL.                                            
           IF DIGZA0-IDT-RCN  NOT =  '0'                                        
              MOVE 'MIGC02 : DIGSA-IDT-RCN ERREUR' TO ABEND-MESS                
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-DLI-GU-DIGSA-IDT.       EXIT.                                        
       EJECT                                                                    
      *==============================================================*          
      * SECTION * RECHERCHE RACINE CONFIGURATION ETAT                *          
      *==================================================== SYBEDITI *          
       DLI-GU-DIGSA-CFG      SECTION.                                           
           MOVE SPACES       TO   CODDLI.                                       
           MOVE '*'          TO   TOP-CFG.                                      
           MOVE '*'          TO   TOP-RCN.                                      
           PERFORM DLI-GU-QUAL-EGAL.                                            
           IF NON-TROUVE                                                        
              MOVE 'ISRT'  TO  CODDLI                                           
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----ERREUR TYPE DE RACINE CONFIGURATION                                  
           IF DIGZA1-CFG-RCN  NOT =  '1'                                        
              MOVE 'MIGC02 : DIGSA CFG TYPE RACINE' TO ABEND-MESS               
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
      *----ERREUR COHERENCE NUMERO DE CONFIGURATION                             
           IF DIGZA1-CFG-CNF  NOT =  SAU-CFG                                    
              MOVE 'MIGC02 : DIGSA CFG CONFIGURATION' TO ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE TEXTE                                     
           IF DIGZA1-CFG-PTC  =  ' '  OR  'T'                                   
              IF SAU-PTC  NOT =  'T'                                            
                 MOVE 'MIGC02 : DIGSA CFG PROTOCOLE' TO ABEND-MESS              
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE LASER                                     
           IF DIGZA1-CFG-PTC  =  'L'                                            
              IF SAU-PTC  NOT =  'L'                                            
                 MOVE 'MIGC02 : DIGSA CFG PROTOCOLE' TO ABEND-MESS              
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
      *----ERREUR COHERENCE PROTOCOLE GRAPHIQUE                                 
           IF DIGZA1-CFG-PTC  =  'G'                                            
              IF SAU-PTC  NOT =  'G'                                            
                 MOVE 'MIGC02 : DIGSA CFG PROTOCOLE' TO ABEND-MESS              
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
           END-IF.                                                              
       EJECT                                                                    
      *----DEMANDE DE DLET DE LA CONFIGURATION                                  
           IF IG7-SUP = 'D'  OR  'S'                                            
              MOVE  'DLET'  TO  CODDLI                                          
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----DEMANDE D'ANNULATION DE LA CONFIGURATION                             
           IF IG7-SUP = 'A'                                                     
              PERFORM CONFIG-ANNU                                               
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----CONFIGURATION  TOPEE ANNULEE                                         
           IF DIGZA1-CFG-SUP  NOT = '000000'                                    
              MOVE  'ANNU'  TO  IG8-ERR                                         
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----CONFIGURATION TOPEE EN REPRISE IMPRESSION                            
           IF DIGZA1-CFG-REP  NOT =  '000000'                                   
              PERFORM CONFIG-REPR                                               
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
      *----CONFIGURATION TOPEE IMPRIMEE                                         
           IF DIGZA1-CFG-IMP  NOT =  '000000'                                   
              PERFORM CONFIG-IMPR                                               
              GO TO FIN-DLI-GU-DIGSA-CFG                                        
           END-IF.                                                              
           PERFORM CONFIG-EDIT.                                                 
       FIN-DLI-GU-DIGSA-CFG.      EXIT.                                         
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT DEMANDE ANNULATION CONFIGURATION        *          
      *---------*------------------------------------------ SYBEDITI *          
       CONFIG-ANNU          SECTION.                                            
           MOVE  IG-DAT-JOU  TO  DIGZA1-CFG-SUP.                                
           MOVE  'REPL'      TO  CODDLI.                                        
       FIN-CONFIG-ANNU.     EXIT.                                               
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE REPRISE             *          
      *---------*------------------------------------------ SYBEDITI *          
       CONFIG-REPR          SECTION.                                            
           IF SAU-IMP  =  'U'                                                   
              MOVE  'UNIQ'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-REPR                                             
           END-IF.                                                              
           IF IG7-SUP  NOT =  'F'                                               
              MOVE  'REPR'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-REPR                                             
           END-IF.                                                              
           MOVE  '000000'  TO  DIGZA1-CFG-REP.                                  
           MOVE  '000000'  TO  DIGZA1-CFG-IMP.                                  
           MOVE  'REPL'    TO  CODDLI.                                          
           IF  SAU-CFG  <  '5'                                                  
               GO TO  FIN-CONFIG-REPR                                           
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  IG-DAT-JOU                                 
              MOVE IG-DAT-JOU  TO  DIGZA1-CFG-EDI                               
              MOVE 'REPL'      TO  CODDLI                                       
           END-IF.                                                              
       FIN-CONFIG-REPR.     EXIT.                                               
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE IMPRIMEE            *          
      *---------*------------------------------------------ SYBEDITI *          
       CONFIG-IMPR          SECTION.                                            
           IF SAU-IMP  =  'U'                                                   
              MOVE  'UNIQ'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-IMPR                                             
           END-IF.                                                              
           IF IG7-SUP  NOT =  'F'                                               
              MOVE  'IMPR'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-IMPR                                             
           END-IF.                                                              
           MOVE  '000000'  TO  DIGZA1-CFG-IMP.                                  
           MOVE  'REPL'    TO  CODDLI.                                          
           IF  SAU-CFG  <  '5'                                                  
               GO TO  FIN-CONFIG-IMPR                                           
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  IG-DAT-JOU                                 
              MOVE IG-DAT-JOU  TO  DIGZA1-CFG-EDI                               
              MOVE 'REPL'      TO  CODDLI                                       
           END-IF.                                                              
       FIN-CONFIG-IMPR.     EXIT.                                               
       EJECT                                                                    
      *---------*----------------------------------------------------*          
      * SECTION * TRAITEMENT CONFIGURATION TOPEE EDITEE              *          
      *---------*------------------------------------------ SYBEDITI *          
       CONFIG-EDIT          SECTION.                                            
           IF IG7-SUP  NOT =  'L'                                               
              MOVE  'EDIT'  TO  IG8-ERR                                         
              GO TO FIN-CONFIG-EDIT                                             
           END-IF.                                                              
           IF  SAU-CFG  <  '5'                                                  
               GO TO  FIN-CONFIG-EDIT                                           
           END-IF.                                                              
           IF DIGZA1-CFG-EDI  NOT =  IG-DAT-JOU                                 
              MOVE IG-DAT-JOU  TO  DIGZA1-CFG-EDI                               
              MOVE 'REPL'      TO  CODDLI                                       
           END-IF.                                                              
       FIN-CONFIG-EDIT.     EXIT.                                               
       EJECT                                                                    
      *==============================================================*          
      * SECTION * RECHERCHE EXISTENCE DESTINATION                    *          
      *==================================================== SYBEDITI *          
       DLI-GNP-DIGSE        SECTION.                                            
      *----GNP QUAL EGAL* * * * ( EXEC DLI )* * * * * * * * * * * * *           
           MOVE IG3-DST  TO  DIGSE-DIGFE0-IGDESTIN.                             
           MOVE 'GNP'    TO  FUNCTON.                                           
           PERFORM CLEF-DIGFE0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-33                                   
      * EXEC DLI GET NEXT IN PARENT  USING PCB(NUM-PCB)                         
      *                              SEGMENT(DIGSE)                             
      *                              WHERE(DIGFE0 = DIGSE-DIGFE0)               
      *                              FIELDLENGTH(KEY-LONG-1)                    
      *                              SEGLENGTH(SEG-LONG-1)                      
      *                              INTO(DIGSE)                                
      *              END-EXEC.                                                  
                MOVE DIGSE-DIGFE0 TO H-DIGFE0                                   
                EXEC SQL                                                        
                      SELECT DIGFE0, COL03001                                   
                      INTO :H-DIGFE0, :H-COL03001                               
                 FROM RTIGSE                                                    
                 WHERE DIGFA0 = :H-DIGFA0                                       
                  AND  DIGFE0 = :H-DIGFE0                                       
                END-EXEC                                                        
                IF SQLCODE = 0                                                  
                   MOVE H-DIGSE TO DIGSE                                        
                END-IF                                                          
                PERFORM TRT-SQLCODE-DL1                                         
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�0DLI     00574   QM' TO DFHEIV0                  
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSE' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     MOVE ' = ' TO DFHC0030                                        
      *dfhei*     MOVE 'DIGFE0  ' TO DFHC0081                                   
      *dfhei*     MOVE KEY-LONG-1 TO DFHB0022                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021 DFHDUMMY DFHDUMMY DFHDUMMY DFHC0030 DFHC0081         
      *dfhei*     DIGSE-DIGFE0 DFHB0022.                                        
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE  'DEST'  TO  IG8-ERR                                         
           END-IF.                                                              
       FIN-DLI-GNP-DIGSE.        EXIT.                                          
       EJECT                                                                    
      *==============================================================*          
      * SECTION * DLET RACINE CONFIGURATION ETAT                     *          
      *==================================================== SYBEDITI *          
        DLI-DLET-DIGSA-CFG      SECTION.                                        
           MOVE 'DLET'   TO   FUNCTON.                                          
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-42                                   
      * EXEC DLI DELETE  USING PCB(NUM-PCB)                                     
      *              SEGMENT(DIGSA)                                             
      *              SEGLENGTH(SEG-LONG-1)                                      
      *              FROM(DIGSA)                                                
      * END-EXEC.                                                               
             EXEC SQL                                                           
                   DELETE FROM RTIGSA                                           
                      WHERE DIGFA0 = :H-DIGFA0                                  
             END-EXEC                                                           
             PERFORM TRT-SQLCODE-DL1                                            
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00600   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE 'MIGC02 : ERREUR DIGSA CFG DLET ' TO ABEND-MESS              
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
           MOVE 'ISRT'  TO  CODDLI.                                             
       FIN-DLI-DLET-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      *==============================================================*          
      * SECTION * ISRT RACINE CONFIGURATION ETAT                     *          
      *==================================================== SYBEDITI *          
       DLI-ISRT-DIGSA-CFG      SECTION.                                         
      *----CLE ACCES ETAT                                                       
           MOVE IDT-KEY-NOM         TO  DIGFA1-CFG-NOM.                         
           MOVE IDT-KEY-DAT         TO  DIGFA2-CFG-DAT.                         
           MOVE IDT-KEY-DST         TO  DIGFA3-CFG-DST.                         
           MOVE IDT-KEY-DOC         TO  DIGFA4-CFG-DOC.                         
      *----CARACTERISTIQUES CONFIGURATION                                       
           MOVE '1'                 TO  DIGZA1-CFG-RCN.                         
           MOVE SAU-CFG             TO  DIGZA1-CFG-CNF.                         
      *----STATUT ETAT                                                          
           IF SAU-CFG > '4'                                                     
              THEN MOVE IG-DAT-JOU  TO  DIGZA1-CFG-EDI                          
              ELSE MOVE '000000'    TO  DIGZA1-CFG-EDI                          
           END-IF.                                                              
           MOVE '000000'            TO  DIGZA1-CFG-SUP.                         
           MOVE '000000'            TO  DIGZA1-CFG-IMP.                         
           MOVE '000000'            TO  DIGZA1-CFG-REP.                         
           MOVE '000000'            TO  DIGZA1-CFG-CNT.                         
           MOVE '000000'            TO  DIGZA1-CFG-PUR.                         
      *----PROTOCOLE IMPRESSION                                                 
           MOVE SAU-PTC             TO  DIGZA1-CFG-PTC.                         
      *----FILLER                                                               
           MOVE SPACES              TO  DIGZA1-CFG-FIL.                         
        EJECT                                                                   
      * ISRT QUAL * * * * * * ( EXEC DLI )  * * * * * * * * * * * * * *         
           MOVE 'ISRT'              TO  FUNCTON.                                
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-43                                   
      * EXEC DLI INSERT  USING PCB(NUM-PCB)                                     
      *                        SEGMENT(DIGSA)                                   
      *                        SEGLENGTH(SEG-LONG-1)                            
      *                        FROM(DIGSA)                                      
      * END-EXEC.                                                               
                MOVE DIGSA TO H-DIGSA                                           
                EXEC SQL                                                        
                     INSERT INTO RTIGSA                                         
                         (DIGFA0, COL01001)                                     
                  VALUES (:H-DIGFA0, :H-COL01001)                               
                END-EXEC                                                        
                PERFORM TRT-SQLCODE-DL1                                         
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00657   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
          PERFORM TEST-CODE-RETOUR.                                             
           IF EXISTE-DEJA                                                       
              MOVE 'MIGC02 : ERREUR DIGSA CFG ISRT '  TO  ABEND-MESS            
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-DLI-ISRT-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      *==============================================================*          
      * SECTION * REPL RACINE CONFIGURATION ETAT                     *          
      *==================================================== SYBEDITI *          
       DLI-REPL-DIGSA-CFG      SECTION.                                         
           MOVE 'REPL'            TO  FUNCTON.                                  
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-44                                   
      * EXEC DLI REPLACE  USING PCB(NUM-PCB)                                    
      *                         SEGMENT(DIGSA)                                  
      *                         SEGLENGTH(SEG-LONG-1)                           
      *                         FROM(DIGSA)                                     
      *  END-EXEC.                                                              
             MOVE DIGSA TO H-DIGSA                                              
             EXEC SQL                                                           
                  UPDATE RTIGSA                                                 
                  SET COL01001 = :H-COL01001                                    
                  WHERE DIGFA0 = :H-DIGFA0                                      
             END-EXEC                                                           
             PERFORM TRT-SQLCODE-DL1
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '��DLI     00682   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
           IF NON-TROUVE                                                        
              MOVE 'MIGC02 : ERREUR DIGSA CFG REPL'  TO  ABEND-MESS             
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
       FIN-DLI-REPL-DIGSA-CFG.      EXIT.                                       
       EJECT                                                                    
      ****************************************************************          
      * SECTION * ISRT SEGMENT LIGNE EDITION PROTOCOLE TEXTE         *          
      ***************************************************** SYBEDITI *          
       DLI-ISRT-DIGSC                 SECTION.                                  
           ACCEPT  IG-TIM-JOU  FROM  TIME.                                      
      * INITIALISATION DSECT SEGMENT DIGSC =============================        
      *----LONGUEUR DU SEGMENT                                                  
           IF SAU-PTC = 'T'                                                     
              COMPUTE DIGZC0-PFX-LEN  =  SAU-COL + 26                           
           END-IF.                                                              
           IF SAU-PTC = 'L'  OR  'G'                                            
              IF IG5-ASA  =  '$'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-CMD + 26                        
              END-IF                                                            
              IF IG5-ASA  =  '�'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-DON + 26                        
              END-IF                                                            
              IF IG5-ASA  =  '&'                                                
                 COMPUTE DIGZC0-PFX-LEN  =  SAU-COL + 26                        
              END-IF                                                            
           END-IF.                                                              
           IF DIGZC0-PFX-LEN  <  27  OR  >  158                                 
              MOVE  'MIGC02 :  DIGZC0-PFX-LEN LT 27 OU GT 158'                  
                               TO  ABEND-MESS                                   
              GO TO ABANDON-BATCH                                               
           END-IF.                                                              
      *----RENSEIGNEMENT CLE NON UNIQUE - DATE ET HEURE                         
           MOVE IG-DAT-JOU      TO  DIGFC1-PFX-DAT.                             
           MOVE IG-TIM-HMS      TO  DIGFC2-PFX-HEU.                             
      *----IDENTIFICATION BATCH -                                               
           MOVE IG1-ETA         TO  DIGFC3-PFX-TAS.                             
           MOVE 'BATC'          TO  DIGFC4-PFX-TRM.                             
      *----MISE EN FORME DE LA LIGNE                                            
           MOVE IG5-ASA         TO  DIGZC0-MEP-ASA.                             
           MOVE IG6-LIG         TO  DIGZC0-EDI-LIG.                             
       EJECT                                                                    
      * RENSEIGNEMENT DU CHEMIN D'ACCES AIDA ===========================        
           MOVE 'ISRT'          TO  FUNCTON.                                    
           PERFORM CLEF-DIGFC0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-45                                   
      * EXEC DLI INSERT  USING PCB(NUM-PCB)                                     
      *              SEGMENT(DIGSC)                                             
      *              SEGLENGTH(SEG-LONG-1)                                      
      *              FROM(DIGSC)                                                
      * END-EXEC.                                                               
                MOVE DIGSA TO H-DIGSA                                           
                EXEC SQL                                                        
                     INSERT INTO RTIGSA                                         
                         (DIGFA0, COL01001)                                     
                  VALUES (:H-DIGFA0, :H-COL01001)                               
                END-EXEC                                                        
                PERFORM TRT-SQLCODE-DL1                                         
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '�DLI     00747   �d' TO DFHEIV0                   
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSC' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021.                                                     
           PERFORM TEST-CODE-RETOUR.                                            
       FIN-DLI-ISRT-DIGSC.              EXIT.                                   
       EJECT                                                                    
      *==============================================================*          
      * SECTION * ACCES SEGMENT DIGSA                                *          
      *==================================================== SYBEDITI *          
       DLI-GU-QUAL-EGAL       SECTION.                                          
      *----RENSEIGNEMENT CLE IDENTIFICATION                                     
           IF TOP-RCN = '0'                                                     
              MOVE IG1-ETA             TO  DIGSA-DIGFA0-IGNOMETA                
              MOVE '******'            TO  DIGSA-DIGFA0-IGDATEDI                
              MOVE '*********'         TO  DIGSA-DIGFA0-IGDESTIN                
              MOVE '***************'   TO  DIGSA-DIGFA0-IGDOCUME                
           END-IF.                                                              
      *----RENSEIGNEMENT CLE CONFIGURATION                                      
      *    IF TOP-RCN = '1'  OR  '*'                                            
           IF TOP-RCN = '*'                                                     
              MOVE IDT-KEY-NOM         TO  DIGSA-DIGFA0-IGNOMETA                
              MOVE IDT-KEY-DAT         TO  DIGSA-DIGFA0-IGDATEDI                
              MOVE IDT-KEY-DST         TO  DIGSA-DIGFA0-IGDESTIN                
              MOVE IDT-KEY-DOC         TO  DIGSA-DIGFA0-IGDOCUME                
           END-IF.                                                              
      *----GU QUAL EGAL * * * * ( EXEC DLI )* * * * * * * * * * * * *           
           MOVE 'GU'     TO  FUNCTON.                                           
           PERFORM CLEF-DIGFA0.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ Post-Translation Correct-EXEC-DLI-46                                   
      * EXEC DLI GET UNIQUE  USING PCB(NUM-PCB)                                 
      *                            SEGMENT(DIGSA)                               
      *                            WHERE(DIGFA0 = DIGSA-DIGFA0)                 
      *                            FIELDLENGTH(KEY-LONG-1)                      
      *                            SEGLENGTH(SEG-LONG-1)                        
      *                            INTO(DIGSA)                                  
      *      END-EXEC.                                                          
             MOVE DIGSA-DIGFA0 TO H-DIGFA0                                      
             EXEC SQL                                                           
             SELECT DIGFA0, COL01001                                            
             INTO :H-DIGFA0, :H-COL01001                                        
             FROM RTIGSA                                                        
             WHERE DIGFA0 = :H-DIGFA0                                           
             END-EXEC                                                           
             PERFORM TRT-SQLCODE-DL1                                           
             IF SQLCODE = 0                                                     
                  MOVE H-DIGSA TO DIGSA                                         
             END-IF                                                             
      *} Post-Translation                                                       
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '04' TO DIBVER OF DLZDIB                                 
      *dfhei*     MOVE '��0DLI     00785   �M' TO DFHEIV0                  
      *dfhei*     MOVE NUM-PCB TO DFHB0020                                      
      *dfhei*     MOVE 'DIGSA' TO DFHC0080                                      
      *dfhei*     MOVE SEG-LONG-1 TO DFHB0021                                   
      *dfhei*     MOVE ' = ' TO DFHC0030                                        
      *dfhei*     MOVE 'DIGFA0  ' TO DFHC0081                                   
      *dfhei*     MOVE KEY-LONG-1 TO DFHB0022                                   
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  DLZDIB DFHB0020 DFHC0080         
      *dfhei*     DFHB0021 DFHDUMMY DFHDUMMY DFHDUMMY DFHC0030 DFHC0081         
      *dfhei*     DIGSA-DIGFA0 DFHB0022.                                        
           PERFORM TEST-CODE-RETOUR.                                            
           IF TOP-RCN = '*'                                                     
              GO TO FIN-DLI-GU-QUAL-EGAL                                        
           END-IF.                                                              
           IF NON-TROUVE                                                        
              IF TOP-RCN  =  '0'                                                
                 MOVE 'MIGC02 : DIGSA-IDT NON TROUVE' TO ABEND-MESS             
                 GO TO ABANDON-BATCH                                            
              END-IF                                                            
      *       IF TOP-RCN  =  '1'                                                
      *          MOVE 'MIGC02 : DIGSA-CFG NON TROUVE' TO ABEND-MESS             
      *          GO TO ABANDON-BATCH                                            
      *       END-IF                                                            
           END-IF.                                                              
       FIN-DLI-GU-QUAL-EGAL.      EXIT.                                         
      * SYBEDITI ***************************************************            
      **************************************************************            
      ************* FIN EDITION D'UN ETAT BATCH ********************            
      **************************************************************            
      *************************************************** SYBEDITI *            
       EJECT                                                                    
                                                                                
                                                                                
