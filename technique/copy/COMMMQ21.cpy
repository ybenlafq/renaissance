      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ HOST --> LOCAL                                        
      * TRAITEMENTS MQ - COTE HOST                                              
      *****************************************************************         
      * TAILLE 500124                                                           
      *                                                                         
      * CODRET : 1 = MQOPEN NON OK                                              
      *          2 = MQPUT    //                                                
      *          3 = SELECT SS-TABLE MQPUT NON TROUVE                           
      *          4 = SELECT SS-TABLE MQPUT SQLCODE < 0                          
      *          5 = MQCLOSE NON OK                                             
      *----------------------------------------------------------------         
      * 31/01/02 ! JB01 ! AJOUT DE LA DATE SYSTEME.                             
      *----------------------------------------------------------------         
       01  COMM-MQ21-APPLI.                                                     
           05  COMM-MQ21-ENTREE.                                                
               10  COMM-MQ21-NSOC            PIC X(03).                         
               10  COMM-MQ21-NLIEU           PIC X(03).                         
               10  COMM-MQ21-FONCTION        PIC X(03).                         
               10  COMM-MQ21-SYNCPOINT       PIC X.                             
               10  COMM-MQ21-PRIORITE        PIC 9.                             
               10  COMM-MQ21-NOMPROG         PIC X(05).                         
               10  COMM-MQ21-MSGID           PIC X(24).                         
               10  COMM-MQ21-CORRELID        PIC X(24).                         
               10  COMM-MQ21-LONGMES         PIC S9(6).                         
               10  COMM-MQ21-CODRET          PIC XX.                            
               10  COMM-MQ21-ERREUR          PIC X(52).                         
JB01  * AJOUT DU DESCRIPTIF DE L'ENTETE POUR LA DSYST         LG=105C           
 "             10  COMM-MQ21-MESSAGE.                                           
 "                 15  COMM-MQ21-ENTETE.                                        
 "                     20 COMM-MQ21-TYPE     PIC  X(03).                        
 "                     20 COMM-MQ21-NSOCMSG  PIC  X(03).                        
 "                     20 COMM-MQ21-NLIEUMSG PIC  X(03).                        
 "                     20 COMM-MQ21-NSOCDST  PIC  X(03).                        
 "                     20 COMM-MQ21-NLIEUDST PIC  X(03).                        
 "                     20 COMM-MQ21-NORD     PIC  9(08).                        
 "                     20 COMM-MQ21-LPROG    PIC  X(10).                        
 "                     20 COMM-MQ21-DJOUR    PIC  X(08).                        
 "                     20 COMM-MQ21-WSID     PIC  X(10).                        
 "                     20 COMM-MQ21-USER     PIC  X(10).                        
 "                     20 COMM-MQ21-CHRONO   PIC  9(07).                        
 "                     20 COMM-MQ21-NBRMSG   PIC  9(07).                        
 "                     20 COMM-MQ21-NBRENR   PIC  9(05).                        
 "                     20 COMM-MQ21-TAILLE   PIC  9(05).                        
 "                     20 COMM-MQ21-VERSION  PIC  X(02).                        
 "                     20 COMM-MQ21-DSYST    PIC S9(13).                        
 "                     20 COMM-MQ21-FILLER   PIC  X(05).                        
                   15  COMM-MQ21-MESS        PIC X(49895).                      
                                                                                
