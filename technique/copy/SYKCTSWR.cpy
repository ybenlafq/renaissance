      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******* 00000100
      * ECRITURE     DE LA TEMPORARY STORAGE                            00000200
      ***************************************************************** 00000300
      *                                                                 00000400
       WRITE-TS                   SECTION.                              00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS                                                      
                          QUEUE    (IDENT-TS)                                   
                          FROM     (Z-INOUT)                                    
                          LENGTH   (LONG-TS)                                    
                          ITEM     (RANG-TS)                                    
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  IDENT-TS Z-INOUT LONG-TS         
      *dfhei*     DFHDUMMY RANG-TS.                                             
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001600
           MOVE EIBRCODE TO EIB-RCODE.                                  00001700
           IF   NOT EIB-NORMAL                                          00001800
                GO TO ABANDON-CICS                                      00001900
           END-IF.                                                              
      *                                                                 00002000
       FIN-WRITE-TS. EXIT.                                              00002100
                    EJECT                                               00002300
                                                                                
                                                                                
