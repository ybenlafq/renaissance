      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *==> DARTY *******************************************************        
      *          * S O R T I E   S W A P                               *        
      ******************************************************* SYKCSWAP *        
       SORTIE-SWAP                  SECTION.                                    
           MOVE NOM-MAP          TO Z-COMMAREA-NOM-MAP.                         
           MOVE LENGTH OF Z-MAP  TO Z-COMMAREA-LONG-MAP.                        
           MOVE NOM-MAPSET       TO Z-COMMAREA-NOM-MAPSET.                      
           MOVE NOM-TACHE        TO Z-COMMAREA-NOM-TACHE.                       
           MOVE LONG-COMMAREA    TO Z-COMMAREA-LONG.                            
           MOVE Z-COMMAREA       TO Z-INOUT-COMMAREA.                           
           MOVE ' ECRAN SAUVEGARDE PAR SWAP ' TO MLIBERRI.                      
           MOVE Z-MAP            TO Z-INOUT-MAP.                                
           PERFORM     SORTIE-SWAP-READ.                                        
           IF NON-TROUVE                                                        
              PERFORM SORTIE-SWAP-WRITE                                         
           ELSE                                                                 
              PERFORM SORTIE-SWAP-REWRITE                                       
           END-IF.                                                              
       FIN-SORTIE-SWAP. EXIT.                                                   
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * S O R T I E   S W A P     R E A D                  *         
      ****************************************************** SYKCSWAP *         
       SORTIE-SWAP-READ             SECTION.                                    
           MOVE 'SWAP'        TO AIDA-IDENT-TS-A.                               
           MOVE EIBTRMID      TO AIDA-IDENT-TS-B.                               
           MOVE 1             TO AIDA-RANG-TS.                                  
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READQ TS                                                       
                 QUEUE    (AIDA-IDENT-TS)                                       
                 SET      (ADDRESS OF LINK-TS)                                  
                 LENGTH   (AIDA-LONG-TS)                                        
                 ITEM     (AIDA-RANG-TS)                                        
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��Yi00036   ' TO DFHEIV0                              
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS ADDRESS OF         
      *dfhei*      AIDA-LONG-TS DFHDUMMY AIDA-RANG-TS.                          
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   EIB-NORMAL                                                      
                MOVE 0        TO CODE-RETOUR                                    
           ELSE                                                                 
                IF   EIB-QIDERR OR EIB-ITEMERR                                  
                     MOVE 1   TO CODE-RETOUR                                    
                ELSE                                                            
                     GO TO ABANDON-CICS                                         
           END-IF                                                               
                END-IF.                                                         
       FIN-SORTIE-SWAP-READ. EXIT.                                              
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * S O R T I E   S W A P     W R I T E                *         
      ****************************************************** SYKCSWAP *         
       SORTIE-SWAP-WRITE           SECTION.                                     
           MOVE 'SWAP'        TO AIDA-IDENT-TS-A.                               
           MOVE EIBTRMID      TO AIDA-IDENT-TS-B.                               
           MOVE 1             TO AIDA-RANG-TS.                                  
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS                                                      
                 QUEUE    (AIDA-IDENT-TS)                                       
                 FROM     (Z-INOUT)                                             
                 LENGTH   (AIDA-LONG-TS)                                        
                 ITEM     (AIDA-RANG-TS)                                        
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00068   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS Z-INOUT            
      *dfhei*     AIDA-LONG-TS DFHDUMMY AIDA-RANG-TS.                           
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
           IF EIBTRNID NOT = NOM-LEVEL-MAX                                      
              PERFORM SORTIE-LEVEL-MAX                                          
           ELSE                                                                 
              PERFORM SORTIE-AFFICHAGE-FORMAT                                   
           END-IF.                                                              
       FIN-SORTIE-SWAP-WRITE. EXIT.                                             
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * S O R T I E   S W A P     R E W R I T E            *         
      ****************************************************** SYKCSWAP *         
       SORTIE-SWAP-REWRITE           SECTION.                                   
           MOVE 'SWAP'            TO AIDA-IDENT-TS-A.                           
           MOVE EIBTRMID          TO AIDA-IDENT-TS-B.                           
           MOVE 1                 TO AIDA-RANG-TS.                              
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS WRITEQ TS                                                      
                 QUEUE    (AIDA-IDENT-TS)                                       
                 FROM     (Z-INOUT)                                             
                 LENGTH   (AIDA-LONG-TS)                                        
                 ITEM     (AIDA-RANG-TS)                                        
                 REWRITE                                                        
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�00100   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS Z-INOUT            
      *dfhei*     AIDA-LONG-TS DFHDUMMY AIDA-RANG-TS.                           
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
           MOVE LINK-TS-MAP           TO Z-INOUT-MAP.                           
           MOVE LINK-TS-COMMAREA      TO Z-INOUT-COMMAREA.                      
           MOVE LINK-TS-COMMAREA      TO Z-COMMAREA.                            
           MOVE Z-COMMAREA-NOM-MAP    TO NOM-MAP.                               
           MOVE Z-COMMAREA-LONG       TO LONG-COMMAREA.                         
           MOVE Z-COMMAREA-LONG-MAP   TO LONG-MAP.                              
           MOVE Z-COMMAREA-NOM-MAPSET TO NOM-MAPSET.                            
           PERFORM SEND-MAP-SWAP.                                               
           MOVE Z-COMMAREA-NOM-TACHE  TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA-SWAP.                                        
       FIN-SORTIE-SWAP-REWRITE. EXIT.                                           
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * S O R T I E   S W A P     D E L E T E              *         
      ****************************************************** SYKCSWAP *         
       SORTIE-SWAP-DELETE           SECTION.                                    
           PERFORM SORTIE-SWAP-READ.                                            
           IF NON-TROUVE                                                        
              GO TO FIN-SORTIE-SWAP-DELETE                                      
           END-IF.                                                              
           MOVE 'SWAP'            TO AIDA-IDENT-TS-A.                           
           MOVE EIBTRMID          TO AIDA-IDENT-TS-B.                           
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS DELETEQ                                                        
                 QUEUE    (AIDA-IDENT-TS)                                       
                 NOHANDLE                                                       
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����00143   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  AIDA-IDENT-TS.                   
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   EIB-NORMAL                                                      
                    MOVE 0  TO CODE-RETOUR                                      
           ELSE                                                                 
                IF EIB-QIDERR                                                   
                    MOVE 1  TO CODE-RETOUR                                      
                ELSE                                                            
                    GO TO ABANDON-CICS                                          
                END-IF                                                          
           END-IF.                                                              
           MOVE LINK-TS-MAP           TO Z-INOUT-MAP.                           
           MOVE LINK-TS-COMMAREA      TO Z-INOUT-COMMAREA.                      
           MOVE LINK-TS-COMMAREA      TO Z-COMMAREA.                            
           MOVE Z-COMMAREA-NOM-MAP    TO NOM-MAP.                               
           MOVE Z-COMMAREA-LONG       TO LONG-COMMAREA.                         
           MOVE Z-COMMAREA-LONG-MAP   TO LONG-MAP.                              
           MOVE Z-COMMAREA-NOM-MAPSET TO NOM-MAPSET.                            
           PERFORM SEND-MAP-SWAP.                                               
           MOVE Z-COMMAREA-NOM-TACHE  TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA-SWAP.                                        
       FIN-SORTIE-SWAP-DELETE. EXIT.                                            
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * SEND MAP SWAP ECRAN                                *         
      ****************************************************** SYKCSWAP *         
       SEND-MAP-SWAP                 SECTION.                                   
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS SEND MAP    (NOM-MAP)                                          
                      MAPSET (NOM-MAPSET)                                       
                      FROM   (Z-INOUT-MAP)                                      
                      LENGTH (LONG-MAP)                                         
                      CURSOR (COMM-SWAP-CURS)                                   
                      ERASE                                                     
                      NOHANDLE                                                  
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�1                                                    
      *dfhei*0179   ' TO DFHEIV0                                                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-MAP Z-INOUT-MAP LONG-        
      *dfhei*     NOM-MAPSET DFHDUMMY DFHDUMMY DFHDUMMY COMM-SWAP-CURS.         
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
       FIN-SEND-MAP-SWAP. EXIT.                                                 
       EJECT                                                                    
      *==> DARTY ******************************************************         
      *          * RETOUR COMMAREA SWAP ECRAN                         *         
      ****************************************************** SYKCSWAP *         
       RETOUR-COMMAREA-SWAP          SECTION.                                   
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *EXEC CICS RETURN   TRANSID  (NOM-TACHE-RETOUR)                           
      *                   COMMAREA (Z-INOUT-COMMAREA)                           
      *                   LENGTH   (LONG-COMMAREA)                              
      *                   NOHANDLE                                              
      *                   END-EXEC.                                             
      *--                                                                       
       EXEC CICS RETURN   TRANSID  (NOM-TACHE-RETOUR)                           
                          COMMAREA (Z-INOUT-COMMAREA)                           
                          LENGTH   (LONG-COMMAREA)                              
                          NOHANDLE                                              
       END-EXEC.                                                                
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��00202   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  NOM-TACHE-RETOUR                 
      *dfhei*     Z-INOUT-COMMAREA LONG-COMMAREA.                               
      *dfhei*} decommente EXEC                                                  
           MOVE EIBRCODE      TO EIB-RCODE.                                     
           IF   NOT EIB-NORMAL                                                  
                GO TO ABANDON-CICS                                              
           END-IF.                                                              
       FIN-RETOUR-COMMAREA-SWAP. EXIT.                                          
       EJECT                                                                    
                                                                                
                                                                                
