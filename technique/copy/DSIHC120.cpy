      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  IHC120-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IHC120-E00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'IHC120                                                    '.        
               10  FILLER                         PIC X(53) VALUE               
           '                                          DEMANDE LE '.             
               10  IHC120-E00-DATEDITE            PIC X(10).                    
               10  FILLER                         PIC X(08) VALUE               
           '   PAGE '.                                                          
               10  IHC120-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E10  --------------------------        
           05  IHC120-E10.                                                      
               10  FILLER                         PIC X(35) VALUE               
           '                SUIVI DES PRODUITS '.                               
               10  IHC120-E10-TEBRB               PIC X(09).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-E10-NLIEUHC             PIC X(03).                    
               10  FILLER                         PIC X(12) VALUE               
           ' , TRI SUR: '.                                                      
               10  IHC120-E10-TITRE1              PIC X(25).                    
               10  IHC120-E10-TITRE2              PIC X(25).                    
               10  FILLER                         PIC X(22) VALUE               
           '                      '.                                            
      *--------------------------- LIGNE E15  --------------------------        
           05  IHC120-E15.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'SELECTION : ENCOURS TOTAL                                 '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E20  --------------------------        
           05  IHC120-E20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------+'.                                                  
      *--------------------------- LIGNE E30  --------------------------        
           05  IHC120-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                                                         '.        
               10  FILLER                         PIC X(58) VALUE               
           '        DATE           DOSSIER  DATE DEM   DATE    DATE   '.        
               10  FILLER                         PIC X(16) VALUE               
           '           CHEZ!'.                                                  
      *--------------------------- LIGNE E40  --------------------------        
           05  IHC120-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '! N� IDEN ADRESSE  CHOIX CODIC   FAM   MARQ  REFERENCE    '.        
               10  FILLER                         PIC X(58) VALUE               
           '        ENTREE   DELAI   SAV    ACCORD   PRE-ACC  ACCORD  '.        
               10  FILLER                         PIC X(16) VALUE               
           ' N� ACCORD FOUR!'.                                                  
      *--------------------------- LIGNE D05  --------------------------        
           05  IHC120-D05.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IHC120-D05-NCHS                PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-ADRESSE             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-CSTATUT             PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-CCHOIX              PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DENTREE             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DELAI               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DOSSAV              PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DDEMACC             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DPREACC             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-DACCFOUR            PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-NACCORD             PIC X(12).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IHC120-D05-FOURN               PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
      *--------------------------- LIGNE B20  --------------------------        
           05  IHC120-B20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '  ================>    P A G E   DE   S Y N T H E S E     '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B50  --------------------------        
           05  IHC120-B50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                         +--------+---------+------+------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----+                                                     '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B52  --------------------------        
           05  IHC120-B52.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                         ! MARQUE ! FAMILLE ! SAV  ! QUANT'.        
               10  FILLER                         PIC X(58) VALUE               
           'ITE !                                                     '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B55  --------------------------        
           05  IHC120-B55.                                                      
               10  FILLER                         PIC X(27) VALUE               
           '                         ! '.                                       
               10  IHC120-B55-CMARQT1             PIC X(05).                    
               10  FILLER                         PIC X(05) VALUE               
           '  !  '.                                                             
               10  IHC120-B55-CFAMT1              PIC X(05).                    
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  IHC120-B55-NSAVT1              PIC X(03).                    
               10  FILLER                         PIC X(06) VALUE               
           '  !   '.                                                            
               10  IHC120-B55-QT1                 PIC ZZZ.                      
               10  FILLER                         PIC X(58) VALUE               
           '    !                                                     '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B60  --------------------------        
           05  IHC120-B60.                                                      
               10  FILLER                         PIC X(18) VALUE               
           'SYNTHESE PRODUITS '.                                                
               10  IHC120-B60-LIBELLE             PIC X(10).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(46) VALUE               
           '                                              '.                    
      *--------------------------- LIGNE B70  --------------------------        
           05  IHC120-B70.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '  +---------+--------+--------+--------+--------+--------+'.        
               10  FILLER                         PIC X(58) VALUE               
           '--------+                                                 '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B71  --------------------------        
           05  IHC120-B71.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '  !         ! STATUT ! STATUT ! STATUT ! STATUT ! STATUT !'.        
               10  FILLER                         PIC X(58) VALUE               
           ' STATUT !                                                 '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE B72  --------------------------        
           05  IHC120-B72.                                                      
               10  FILLER                         PIC X(17) VALUE               
           '  ! MARQUE  !    '.                                                 
               10  IHC120-B72-CSTATUT1T2          PIC X(01).                    
               10  FILLER                         PIC X(08) VALUE               
           '   !    '.                                                          
               10  IHC120-B72-CSTATUT2T2          PIC X(01).                    
               10  FILLER                         PIC X(08) VALUE               
           '   !    '.                                                          
               10  IHC120-B72-CSTATUT3T2          PIC X(01).                    
               10  FILLER                         PIC X(08) VALUE               
           '   !    '.                                                          
               10  IHC120-B72-CSTATUT4T2          PIC X(01).                    
               10  FILLER                         PIC X(08) VALUE               
           '   !    '.                                                          
               10  IHC120-B72-CSTATUT5T2          PIC X(01).                    
               10  FILLER                         PIC X(08) VALUE               
           '   !    '.                                                          
               10  IHC120-B72-CSTATUT6T2          PIC X(01).                    
               10  FILLER                         PIC X(58) VALUE               
           '   !                                                      '.        
               10  FILLER                         PIC X(11) VALUE               
           '           '.                                                       
      *--------------------------- LIGNE B75  --------------------------        
           05  IHC120-B75.                                                      
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  IHC120-B75-CMARQT2             PIC X(05).                    
               10  FILLER                         PIC X(06) VALUE               
           '   !  '.                                                            
               10  IHC120-B75-Q1T2                PIC ZZZ.                      
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  IHC120-B75-Q2T2                PIC ZZZ.                      
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  IHC120-B75-Q3T2                PIC ZZZ.                      
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  IHC120-B75-Q4T2                PIC ZZZ.                      
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  IHC120-B75-Q5T2                PIC ZZZ.                      
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  IHC120-B75-Q6T2                PIC ZZZ.                      
               10  FILLER                         PIC X(58) VALUE               
           '   !                                                      '.        
               10  FILLER                         PIC X(11) VALUE               
           '           '.                                                       
      *--------------------------- LIGNE B80  --------------------------        
           05  IHC120-B80.                                                      
               10  FILLER                         PIC X(13) VALUE               
           '  ! TOTAUX  !'.                                                     
               10  IHC120-B80-Q1TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '   !'.                                                              
               10  IHC120-B80-Q2TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '   !'.                                                              
               10  IHC120-B80-Q3TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '   !'.                                                              
               10  IHC120-B80-Q4TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '   !'.                                                              
               10  IHC120-B80-Q5TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '   !'.                                                              
               10  IHC120-B80-Q6TOTT2             PIC ZZZZZ.                    
               10  FILLER                         PIC X(58) VALUE               
           '   !                                                      '.        
               10  FILLER                         PIC X(11) VALUE               
           '           '.                                                       
                                                                                
