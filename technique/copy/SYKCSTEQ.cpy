      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  MODULE GENERALISE DE START BROWSING EGAL A LA CLE              00000200
      ****************************************************************  00000300
      *                                                                 00000400
       START-BROWSING-EQUAL           SECTION.                          00000500
      *                                                                 00000600
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS STARTBR  DATASET (FILE-NAME)                                   
                          RIDFLD  (VSAM-KEY)                                    
                          EQUAL                                                 
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��`00008   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME DFHDUMMY DFHEIB        
      *dfhei*     VSAM-KEY.                                                     
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001400
           MOVE EIBRCODE TO EIB-RCODE.                                  00001500
           IF   EIB-NORMAL                                              00001600
                MOVE 0 TO CODE-RETOUR                                   00001700
           ELSE                                                         00001800
                IF   EIB-NOTFND                                         00001900
                     MOVE 1 TO CODE-RETOUR                              00002000
                ELSE                                                    00002100
                     GO TO ABANDON-CICS                                 00002200
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002300
       FIN-START-BR-EQU. EXIT.                                          00002400
                    EJECT                                               00002600
                                                                                
                                                                                
