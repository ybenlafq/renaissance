      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00510014
      **********************************************===>CICS XXX******  00000100
      *  MODULE GENERALISE DE READ  EGAL A LA CLE EN UPDATE             00000200
      ****************************************************************  00000300
      *                                                                 00000400
       READ-UPDATE             SECTION.                                 00000500
      *                                                                 00000600
      *    MOVE LONG-Z-INOUT TO FILE-LONG.                              00000700
      *                                                                 00000800
      *dfhei*{ decommente EXEC                                                  
       EXEC CICS READ     DATASET (FILE-NAME)                                   
                          RIDFLD  (VSAM-KEY)                                    
                          INTO    (Z-INOUT)                                     
                          LENGTH  (FILE-LONG)                                   
                          UPDATE                                                
                          EQUAL                                                 
                          NOHANDLE                                              
       END-EXEC.                                                                
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�0d00010   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  FILE-NAME Z-INOUT FILE-LO        
      *dfhei*     VSAM-KEY.                                                     
      *dfhei*} decommente EXEC                                                  
      *                                                                 00001900
           MOVE EIBRCODE TO EIB-RCODE.                                  00002000
           IF   EIB-NORMAL                                              00002100
                MOVE 0 TO CODE-RETOUR                                   00002200
           ELSE                                                         00002300
                IF   EIB-NOTFND                                         00002400
                     MOVE 1 TO CODE-RETOUR                              00002500
                ELSE                                                    00002600
                     GO TO ABANDON-CICS                                 00002700
                END-IF                                                          
           END-IF.                                                              
      *                                                                 00002800
       FIN-READ-UPDATE. EXIT.                                           00002900
                    EJECT                                               00003100
                                                                                
                                                                                
