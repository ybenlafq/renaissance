      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     TEG6I.                                   
       AUTHOR. M-DUPREY-CEAFI.                                                  
      *----------------------------------------------------------------*        
      *     MODULE D'IMPRESSION CICS DU GENERATEUR D'ETAT              *        
      *----------------------------------------------------------------*        
      *     CE MODULE LIT UNE TS ET CONSTRUIT LE MESSAGE A             *        
      *     ENVOYER SUR L'IMPRIMANTE.                                  *        
      *----------------------------------------------------------------*        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
           COPY SYKWDIV0.                                                       
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
       01  WS.                                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  I                PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-P              PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  I-P              PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-L              PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  I-L              PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LG             PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  I-LG             PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ITEM-TS          PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  ITEM-TS          PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PAGEHT           PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  PAGEHT           PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PAGEWD           PIC S9(4)      COMP   VALUE ZERO.               
      *--                                                                       
           05  PAGEWD           PIC S9(4) COMP-5   VALUE ZERO.                  
      *}                                                                        
           05  W-CTLCHAR        PIC X                 VALUE 'H'.                
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-NEWPAGE        PIC X                 VALUE X'0C'.              
      *--                                                                       
           05  W-NEWPAGE        PIC X                 VALUE X'0C'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-NEWLINE        PIC X                 VALUE X'15'.              
      *--                                                                       
           05  W-NEWLINE        PIC X                 VALUE X'0A'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-CRETURN        PIC X                 VALUE X'0D'.              
      *--                                                                       
           05  W-CRETURN        PIC X                 VALUE X'0D'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-ENDMESS        PIC X                 VALUE X'19'.              
      *--                                                                       
           05  W-ENDMESS        PIC X                 VALUE X'19'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-RA             PIC X                 VALUE X'3C'.              
      *--                                                                       
           05  W-RA             PIC X                 VALUE X'14'.              
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    05  W-SBA            PIC X                 VALUE X'11'.              
      *--                                                                       
           05  W-SBA            PIC X                 VALUE X'11'.              
      *}                                                                        
           05  TS-RES.                                                          
               10  TSR-SAUT         PIC X             VALUE SPACES.             
               10  TSR-LIGNE        PIC X(255)        VALUE SPACES.             
           05  Z-COMMAREA.                                                      
               10  NOM-TS           PIC X(8)              VALUE SPACES. 00150103
           05  TS-RECORD.                                                       
               10  TS-SAUT          PIC X             VALUE SPACES.             
               10  TS-LIGNE         PIC X(255)        VALUE SPACES.             
           05  Z-SEND           PIC X(1920)           VALUE LOW-VALUE.          
           COPY SYKWZCMD.                                                       
      * -AIDA *********************************************************         
      *   ZONES GENERALES OBLIGATOIRES                                *         
      *****************************************************************         
           COPY SYKWEIB0.                                                       
      *    COPY SYKWDATH.                                                       
           COPY SYKWECRA.                                                       
      * -AIDA *********************************************************         
      *  ZONE DE COMMAREA POUR PROGRAMME APPELE PAR LINK                        
      *****************************************************************         
      *                                                                         
       01  FILLER             PIC X(16)  VALUE 'Z-COMMAREA-LINK'.               
       01  Z-COMMAREA-LINK    PIC X(200).                                       
      * -AIDA *********************************************************         
      *       * ZONE DE GESTION DES ERREURS                                     
      *****************************************************************         
           COPY SYKWERRO.                                                       
      * -AIDA *********************************************************         
      *                                                               *         
      *  LLL      IIIIIIII NN    NNN KKK    KKK AAAAAAAAA GGGGGGGGG   *         
      *  LLL      IIIIIIII NNNN  NNN KKK   KKK  AAAAAAAAA GGGGGGGGG   *         
      *  LLL         II    NNNNN NNN KKK  KKK   AA     AA GG          *         
      *  LLL         II    NNN NNNNN KKK KKK    AA     AA GG          *         
      *  LLL         II    NNN  NNNN KKKKKK     AAAAAAAAA GG          *         
      *  LLL         II    NNN   NNN KKKKKK     AAAAAAAAA GG  GGGG    *         
      *  LLL         II    NNN   NNN KKK KKK    AA     AA GG    GG    *         
      *  LLL         II    NNN   NNN KKK  KKK   AA     AA GG    GG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK   KKK  AA     AA GGGGGGGG    *         
      *  LLLLLLLL IIIIIIII NNN   NNN KKK    KK  AA     AA GGGGGGGG    *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       LINKAGE SECTION.                                                         
      *                    DFHCOMMAREA                                          
       01  DFHCOMMAREA     PIC X.                                               
           COPY SYKLINKA.                                                       
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *---------------------------------------                                  
       MODULE-MEG6I                    SECTION.                                 
      *---------------------------------------                                  
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT                                            
                   UNTIL EIBRESP NOT = ZERO.                                    
           PERFORM MODULE-SORTIE.                                               
      *---------------------------------------                                  
       MODULE-ENTREE                   SECTION.                                 
      *---------------------------------------                                  
           MOVE 8                   TO LONG-TS.                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETRIEVE INTO(NOM-TS)                                      
      *                       LENGTH(LONG-TS)                                   
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS RETRIEVE INTO(NOM-TS)                                      
                              LENGTH(LONG-TS)                                   
           END-EXEC.                                                            
      *}                                                                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASSIGN TERMCODE(TERMCODE)                                  
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS ASSIGN TERMCODE(TERMCODE)                                  
           END-EXEC.                                                            
      *}                                                                        
           IF NOT TERMINAL-IMPRIMANTE                                           
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS XCTL PROGRAM('TEG61')                                   
      *                      COMMAREA(NOM-TS)                                   
      *                      LENGTH(LONG-TS)                                    
      *                 END-EXEC                                                
      *--                                                                       
              EXEC CICS XCTL PROGRAM('TEG61')                                   
                             COMMAREA(NOM-TS)                                   
                             LENGTH(LONG-TS)                                    
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASSIGN SCRNHT(PAGEHT)                                      
      *                     SCRNWD(PAGEWD)                                      
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS ASSIGN SCRNHT(PAGEHT)                                      
                            SCRNWD(PAGEWD)                                      
           END-EXEC.                                                            
      *}                                                                        
           MOVE ZERO                TO ITEM-TS.                                 
           PERFORM READ-TS.                                                     
           MOVE ZERO                TO I-L.                                     
           MOVE 1                   TO I-P.                                     
      *---------------------------------------                                  
       MODULE-TRAITEMENT               SECTION.                                 
      *---------------------------------------                                  
           EVALUATE TS-SAUT                                                     
              WHEN '1'                                                          
                 IF I-L > ZERO                                                  
                    PERFORM SEND-MESSAGE                                        
                 END-IF                                                         
                 MOVE W-NEWPAGE         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
                 MOVE W-CRETURN         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
              WHEN ' '                                                          
                 IF I-P > 1                                                     
                    IF I-LG < 132                                               
                       MOVE W-NEWLINE         TO Z-SEND (I-P:1)                 
                       COMPUTE I-P = I-P + 1                                    
                       COMPUTE I-L = I-L + 1                                    
                    END-IF                                                      
                 ELSE                                                           
                    IF I-LG = ZERO                                              
                       MOVE W-NEWLINE         TO Z-SEND (I-P:1)                 
                       COMPUTE I-P = I-P + 1                                    
                       COMPUTE I-L = I-L + 1                                    
                    END-IF                                                      
                 END-IF                                                         
              WHEN '0'                                                          
                 IF I-P > 1 AND I-LG < 132                                      
                    MOVE W-NEWLINE         TO Z-SEND (I-P:1)                    
                    COMPUTE I-P = I-P + 1                                       
                    COMPUTE I-L = I-L + 1                                       
                 END-IF                                                         
                 MOVE W-NEWLINE         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
              WHEN '-'                                                          
                 IF I-P > 1 AND I-LG < 132                                      
                    MOVE W-NEWLINE         TO Z-SEND (I-P:1)                    
                    COMPUTE I-P = I-P + 1                                       
                    COMPUTE I-L = I-L + 1                                       
                 END-IF                                                         
                 MOVE W-NEWLINE         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
                 MOVE W-NEWLINE         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
              WHEN '+'                                                          
                 MOVE W-CRETURN         TO Z-SEND (I-P:1)                       
                 COMPUTE I-P = I-P + 1                                          
                 COMPUTE I-L = I-L + 1                                          
           END-EVALUATE.                                                        
           PERFORM VARYING I-LG FROM LONG-TS BY -1                              
                   UNTIL   I-LG < 1                                             
                   OR      TS-LIGNE (I-LG:1) > ' '                              
           END-PERFORM.                                                         
           IF I-LG > ZERO                                                       
              MOVE TS-LIGNE (1:I-LG)   TO Z-SEND (I-P:I-LG)                     
              COMPUTE I-P = I-P + I-LG                                          
              COMPUTE I-L = I-L + I-LG                                          
           END-IF.                                                              
           COMPUTE I = 1920 - I-L - 1.                                          
           PERFORM READ-TS.                                                     
           IF EIBRESP = ZERO                                                    
              IF TS-SAUT NOT = '+'                                              
                 IF I NOT > LONG-TS                                             
                    PERFORM SEND-MESSAGE                                        
                 END-IF                                                         
              ELSE                                                              
                 IF I-L < 1918                                                  
                    MOVE W-CRETURN         TO Z-SEND (I-P:1)                    
                    COMPUTE I-P = I-P + 1                                       
                    COMPUTE I-L = I-L + 1                                       
                 ELSE                                                           
                    PERFORM SEND-MESSAGE                                        
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *---------------------------------------                                  
       MODULE-SORTIE                   SECTION.                                 
      *---------------------------------------                                  
           IF I-L > ZERO                                                        
              PERFORM SEND-MESSAGE                                              
           END-IF.                                                              
           PERFORM DELETE-TS.                                                   
           MOVE W-NEWPAGE            TO Z-SEND (1:1).                           
           COMPUTE I-P = I-P + 1.                                               
           COMPUTE I-L = I-L + 1.                                               
           PERFORM SEND-MESSAGE.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN           END-EXEC.                                 
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *---------------------------------------                                  
       SEND-MESSAGE                    SECTION.                                 
      *---------------------------------------                                  
           IF Z-SEND (I-L:1) = W-NEWLINE                                        
              MOVE ' '               TO Z-SEND (I-L:1)                          
           END-IF.                                                              
           MOVE W-ENDMESS            TO Z-SEND (I-P:1)                          
           COMPUTE I-L = I-L + 1.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS SEND FROM(Z-SEND)                                          
      *                   LENGTH(I-L)                                           
      *                   CTLCHAR(W-CTLCHAR)                                    
      *                   ERASE                                                 
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS SEND FROM(Z-SEND)                                          
                          LENGTH(I-L)                                           
                          CTLCHAR(W-CTLCHAR)                                    
                          ERASE                                                 
           END-EXEC.                                                            
      *}                                                                        
           MOVE LOW-VALUE            TO Z-SEND.                                 
           MOVE 1                    TO I-P.                                    
           MOVE ZERO                 TO I-L.                                    
      *----------------------------------------                                 
       READ-TS                          SECTION.                                
      *----------------------------------------                                 
           MOVE 256                TO LONG-TS.                                  
           COMPUTE ITEM-TS = ITEM-TS + 1.                                       
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS READQ TS QUEUE(NOM-TS)                                     
      *                       INTO(TS-RECORD)                                   
      *                       LENGTH(LONG-TS)                                   
      *                       ITEM(ITEM-TS)                                     
      *                       NOHANDLE                                          
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS READQ TS QUEUE(NOM-TS)                                     
                              INTO(TS-RECORD)                                   
                              LENGTH(LONG-TS)                                   
                              ITEM(ITEM-TS)                                     
                              NOHANDLE                                          
           END-EXEC.                                                            
      *}                                                                        
           IF EIBRESP = ZERO                                                    
              COMPUTE LONG-TS = LONG-TS - 1                                     
           END-IF.                                                              
      *----------------------------------------                                 
       DELETE-TS                        SECTION.                                
      *----------------------------------------                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELETEQ TS QUEUE(NOM-TS)                                   
      *                         NOHANDLE                                        
      *              END-EXEC.                                                  
      *--                                                                       
           EXEC CICS DELETEQ TS QUEUE(NOM-TS)                                   
                                NOHANDLE                                        
           END-EXEC.                                                            
      *}                                                                        
      *================================================================*        
       S01-COPY SECTION. CONTINUE.   COPY SYKCLINK.                             
       S99-COPY SECTION. CONTINUE.   COPY SYKCERRO.                             
