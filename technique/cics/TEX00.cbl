      * @(#) MetaWare Technologies Cataloguer 0.9.18
      * Translated: 19/10/2016 18:44
      * Portfolio: IMAGE01
       IDENTIFICATION DIVISION.
       PROGRAM-ID. TEX00.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01  S--PROGRAM                PIC X(8) VALUE 'TEX00'.
       01  ENDVAL--4                 PIC S9(5).
      * PRINTER EOM            */
      * *
       01  DFHBMPEM                  PIC X(1) VALUE X'19'.
      * PRINTER NL             */
      * *
       01  DFHBMPNL                  PIC X(1) VALUE X'15'.
      * PRINTER FF             */
      * *
       01  DFHBMPFF                  PIC X(1) VALUE X'0C'.
      * PRINTER CR             */
      * *
       01  DFHBMPCR                  PIC X(1) VALUE X'0D'.
      * AUTO SKIP              */
       01  DFHBMASK                  PIC X(1) VALUE '0'.
      * UNPROTECTED            */
      * *
       01  DFHBMUNP                  PIC X(1) VALUE X'40'.
      * UNPROT + NUM           */
      * *
       01  DFHBMUNN                  PIC X(1) VALUE X'50'.
      * PROTECTED              */
       01  DFHBMPRO                  PIC X(1) VALUE '-'.
      * BRIGHT                 */
       01  DFHBMBRY                  PIC X(1) VALUE 'H'.
      * DARK                   */
       01  DFHBMDAR                  PIC X(1) VALUE '<'.
      * MDT SET                */
       01  DFHBMFSE                  PIC X(1) VALUE 'A'.
      * PROT + MDT SET         */
       01  DFHBMPRF                  PIC X(1) VALUE '/'.
      * ASKP+MDT               */
       01  DFHBMASF                  PIC X(1) VALUE '1'.
      * AUTO + BRIGHT          */
       01  DFHBMASB                  PIC X(1) VALUE '8'.
      * SHIFT OUT              */
      * *
       01  DFHBMPSO                  PIC X(1) VALUE X'0E'.
      * SHIFT IN               */
      * *
       01  DFHBMPSI                  PIC X(1) VALUE X'0F'.
      *  FIELD FLAG VALUE SET BY INPUT MAPPING
      *                                                               */
      * FIELD ERASED           */
      * *
       01  DFHBMEOF                  PIC X(1) VALUE X'80'.
      * CURSOR IN FIELD        */
      * *
       01  DFHBMCUR                  PIC X(1) VALUE X'02'.
      *  FIELD DATA VALUE SET BY INPUT MAPPING
      *                                                               */
      * FIELD DETECTED         */
       01  DFHBMDET                  PIC X(1) VALUE '�'.
      *  CODE FOR SA ORDER
      *                                                               */
      * SA ORDER (X'28')       */
      * *
       01  DFHSA                     PIC X(1) VALUE X'28'.
      *  CODE FOR ERROR CODE
      *                                                               */
      * ERROR CODE-X'3F'       */
      * *
       01  DFHERROR                  PIC X(1) VALUE X'3F'.
      *  EXTENDED ATTRIBUTE TYPE CODES
      *                                                               */
      * COLOR    (X'42')       */
       01  DFHCOLOR                  PIC X(1) VALUE '�'.
      * PS       (X'43')       */
       01  DFHPS                     PIC X(1) VALUE '�'.
      * HIGHLIGHT(X'41')       */
       01  DFHHLT                    PIC X(1) VALUE '�'.
      * 3270     (X'C0')       */
       01  DFH3270                   PIC X(1) VALUE '�'.
      * VALIDT'N (X'C1')       */
       01  DFHVAL                    PIC X(1) VALUE 'A'.
      * OUTLINE                */
       01  DFHOUTLN                  PIC X(1) VALUE 'B'.
      * BACKGROUND             */
       01  DFHBKTRN                  PIC X(1) VALUE '�'.
      * TRANSP   (X'46')       */
      * ALL , RESET TO         */
      * *
       01  DFHALL                    PIC X(1) VALUE X'00'.
      * DEFAULT  (X'00')       */
      *  DEFAULT ATTRIBUTE CODE  - TO SET ATTRIBUTES IN MAPS
      *                                                               */
      * DEFAULT  (X'FF')       */
       01  DFHDFT                    PIC X(1) VALUE '�'.
      * �P2C*/
      *  COLOR ATTRIBUTE VALUES
      *                                                               */
      * DEFAULT                */
      * *
      * MANDATORY ENTER+       */
      * TRIGGER                */
       01  DFHDFCOL                  PIC X(1) VALUE X'00'.
      * BLUE                   */
       01  DFHBLUE                   PIC X(1) VALUE '1'.
      * RED                    */
       01  DFHRED                    PIC X(1) VALUE '2'.
      * PINK                   */
       01  DFHPINK                   PIC X(1) VALUE '3'.
      * GREEN                  */
       01  DFHGREEN                  PIC X(1) VALUE '4'.
      * TURQUOISE              */
       01  DFHTURQ                   PIC X(1) VALUE '5'.
      * YELLOW                 */
       01  DFHYELLO                  PIC X(1) VALUE '6'.
      * NEUTRAL                */
       01  DFHNEUTR                  PIC X(1) VALUE '7'.
      *  BASE PS  ATTRIBUTE VALUE
      *                                                               */
      * BASE PS                */
      * *
       01  DFHBASE                   PIC X(1) VALUE X'00'.
      *  HIGHLIGHT ATTRIBUTE VALUES
      *                                                               */
      * NORMAL                 */
      * *
       01  DFHDFHI                   PIC X(1) VALUE X'00'.
      * BLINK                  */
       01  DFHBLINK                  PIC X(1) VALUE '1'.
      * REVERSE VIDEO          */
       01  DFHREVRS                  PIC X(1) VALUE '2'.
      * UNDERSCORE             */
       01  DFHUNDLN                  PIC X(1) VALUE '4'.
      *  VALIDATION ATTRIBUTE VALUES
      *                                                               */
      * MANDATORY FILL         */
      * *
       01  DFHMFIL                   PIC X(1) VALUE X'04'.
      * MANDATORY ENTER        */
      * *
       01  DFHMENT                   PIC X(1) VALUE X'02'.
      * MANDATORY FILL+        */
      * *
       01  DFHMFE                    PIC X(1) VALUE X'06'.
      *  MANDATORY ENTER       */
      *  ADDITIONAL ATTRIBUTES
      *                                                               */
      * UNPROTECTED            */
       01  DFHUNNOD                  PIC X(1) VALUE '('.
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNIMD                  PIC X(1) VALUE 'I'.
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNNUM                  PIC X(1) VALUE 'J'.
      * NUMERIC                */
      * MDT                    */
      * UNPROTECTED        �01A*/
       01  DFHUNNUB                  PIC X(1) VALUE 'Q'.
      * NUMERIC            �01A*/
      * INTENSIFY          �01A*/
      * LIGHT PEN DET.     �01A*/
      *                    �01A*/
      * UNPROTECTED            */
       01  DFHUNINT                  PIC X(1) VALUE 'R'.
      * NUMERIC                */
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * MDT                    */
      * UNPROTECTED            */
       01  DFHUNNON                  PIC X(1) VALUE ')'.
      * NUMERIC                */
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * MDT                    */
      * PROTECTED              */
       01  DFHPROTI                  PIC X(1) VALUE 'Y'.
      * INTENSIFY              */
      * LIGHT PEN DET.         */
      * PROTECTED              */
       01  DFHPROTN                  PIC X(1) VALUE '%'.
      * NON-DISPLAY            */
      * NON-PRINT              */
      * NON-DETECTABLE         */
      * TRIGGER                */
      * *
       01  DFHMT                     PIC X(1) VALUE X'01'.
      * MANDATORY FILL+        */
      * *
       01  DFHMFT                    PIC X(1) VALUE X'05'.
      * TRIGGER                */
      * MANDATORY ENTER+       */
      * *
       01  DFHMET                    PIC X(1) VALUE X'03'.
      * TRIGGER                */
      * MANDATORY FILL+        */
      * *
       01  DFHMFET                   PIC X(1) VALUE X'07'.
      *  FIELD OUTLINING ATTRIBUTE CODES       KJ0001
      *                                                               */
      * DEFAULT OUTLINE        */
      * *
       01  DFHDFFR                   PIC X(1) VALUE X'00'.
      * LEFT                   */
      * *
       01  DFHLEFT                   PIC X(1) VALUE X'08'.
      * OVERLINE               */
      * *
       01  DFHOVER                   PIC X(1) VALUE X'04'.
      * RIGHT                  */
      * *
       01  DFHRIGHT                  PIC X(1) VALUE X'02'.
      * UNDER                  */
      * *
       01  DFHUNDER                  PIC X(1) VALUE X'01'.
      * LEFT+OVER+RIGHT+       */
      * *
       01  DFHBOX                    PIC X(1) VALUE X'0F'.
      * UNDER LINES            */
      *  SOSI ATTRIBUTE CODES
      *                                                               */
      * SOSI = YES             */
      * *
       01  DFHSOSI                   PIC X(1) VALUE X'01'.
      *  BACKGROUND TRANSPARENCY ATTRIBUTE CODES
      *                                                               */
      * TRANSP = YES           */
       01  DFHTRANS                  PIC X(1) VALUE '0'.
      * TRANSP = NO            */
       01  DFHOPAQ                   PIC X(1) VALUE '�'.
       01  DFHENTER                  PIC X(1) VALUE ''''.
       01  DFHCLEAR                  PIC X(1) VALUE '_'.
       01  DFHCLRP                   PIC X(1) VALUE '�'.
       01  DFHPEN                    PIC X(1) VALUE '='.
       01  DFHOPID                   PIC X(1) VALUE 'W'.
       01  DFHMSRE                   PIC X(1) VALUE 'X'.
       01  DFHSTRF                   PIC X(1) VALUE 'h'.
       01  DFHTRIG                   PIC X(1) VALUE '"'.
       01  DFHPA1                    PIC X(1) VALUE '%'.
       01  DFHPA2                    PIC X(1) VALUE '>'.
       01  DFHPA3                    PIC X(1) VALUE ','.
       01  DFHPF1                    PIC X(1) VALUE '1'.
       01  DFHPF2                    PIC X(1) VALUE '2'.
       01  DFHPF3                    PIC X(1) VALUE '3'.
       01  DFHPF4                    PIC X(1) VALUE '4'.
       01  DFHPF5                    PIC X(1) VALUE '5'.
       01  DFHPF6                    PIC X(1) VALUE '6'.
       01  DFHPF7                    PIC X(1) VALUE '7'.
       01  DFHPF8                    PIC X(1) VALUE '8'.
       01  DFHPF9                    PIC X(1) VALUE '9'.
       01  DFHPF10                   PIC X(1) VALUE ':'.
       01  DFHPF11                   PIC X(1) VALUE ''.
       01  DFHPF12                   PIC X(1) VALUE '�'.
       01  DFHPF13                   PIC X(1) VALUE 'A'.
       01  DFHPF14                   PIC X(1) VALUE 'B'.
       01  DFHPF15                   PIC X(1) VALUE 'C'.
       01  DFHPF16                   PIC X(1) VALUE 'D'.
       01  DFHPF17                   PIC X(1) VALUE 'E'.
       01  DFHPF18                   PIC X(1) VALUE 'F'.
       01  DFHPF19                   PIC X(1) VALUE 'G'.
       01  DFHPF20                   PIC X(1) VALUE 'H'.
       01  DFHPF21                   PIC X(1) VALUE 'I'.
       01  DFHPF22                   PIC X(1) VALUE '�'.
       01  DFHPF23                   PIC X(1) VALUE '.'.
       01  DFHPF24                   PIC X(1) VALUE '<'.
       COPY SP-VERIFY-COPY.
       LOCAL-STORAGE SECTION.
       01  ENRE.
         05 Z_CLE.
           10 ANNEE                  PIC X(4).
           10 QUANTIEME              PIC X(3).
           10 NOMPRO                 PIC X(5).
           10 NOTACHE                PIC X(2).
           10 NOCARTE                PIC X(2).
         05 PARAM                    PIC X(80).
       01  CLE                       PIC X(16) VALUE ' '.
       01  KW--MESSAGE               PIC X(80) VALUE ' '.
      * TABLE DE COMMUNIC. */
       01  GFDX.
      * CODE D'APPEL       */
         05 GFDATA                   PIC X(1).
      * JOUR               */
         05 GFJOUR                   PIC X(2).
         05 GFJJ REDEFINES GFJOUR    PIC 99.
      * MOIS               */
         05 GFMOIS                   PIC X(2).
         05 GFMM REDEFINES GFMOIS    PIC 99.
      * ANNEE              */
         05 GFANNEE                  PIC X(2).
         05 GFAA REDEFINES GFANNEE   PIC 99.
      * QUANTIEME CALEND.  */
         05 GFQNTA                   PIC 999.
      * QUANT. 'STANDARD'  */
         05 GFQNT0                   PIC 9(5).
      * JOUR DE LA SEMAINE */
         05 GFSMN                    PIC 9.
      * DATE CONCATENEE   */
         05 GFAMJ                    PIC X(6).
         05 GFAAMMJJ REDEFINES GFAMJ PIC 9(6).
      * CODE REPONSE       */
         05 GFVDAT                   PIC 1(1) BIT.
         05 GFBISS                   PIC 1(1) BIT.
       01  GRP--GFTABJ.
         05 GFTABJ OCCURS 12         PIC S9(4) COMP-5.
       01  FLAG1                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG2                     PIC 1(1) BIT SYNCHRONIZED.
       01  FLAG3                     PIC 1(1) BIT SYNCHRONIZED.
       01  GFX1                      PIC S9(9) COMP-5.
       01  GFX2                      PIC S9(9) COMP-5.
       01  GFBJ                      PIC S9(9) COMP-5.
       01  GFBM                      PIC S9(9) COMP-5.
       01  GFBA                      PIC S9(9) COMP-5.
       01  GFBQTA                    PIC S9(9) COMP-5.
       01  GFBQT0                    PIC S9(9) COMP-5.
       01  I                         PIC S9(4) COMP-5.
       LINKAGE SECTION.
       01  KW--COMMA                 PIC X(120).
       01  NOCARTE2                  PIC 99.
       01  POINTEUR                  POINTER.
       01  COM.
         05 Z_CLE.
           10 Z_DATE.
             15 Z_AA                 PIC X(2).
             15 Z_MM                 PIC X(2).
             15 Z_JJ                 PIC X(2).
           10 Z_PROG                 PIC X(5).
           10 Z_TACHE                PIC X(2).
           10 Z_CARTE                PIC X(2).
         05 Z_CODE                   PIC X(1).
         05 CODERETOUR               PIC X(2).
         05 KW--FILLER               PIC X(15).
         05 Z_PARAM                  PIC X(80).
         05 FILL                     PIC X(7).
       PROCEDURE DIVISION USING POINTEUR.
       BEGIN--MAIN SECTION.
           SET ADDRESS OF NOCARTE2 TO ADDRESS OF NOCARTE
           SET ADDRESS OF KW--COMMA TO POINTEUR
           SET ADDRESS OF COM TO ADDRESS OF KW--COMMA
           MOVE 31 TO GFTABJ(1)
           MOVE 28 TO GFTABJ(2)
           MOVE 31 TO GFTABJ(3)
           MOVE 30 TO GFTABJ(4)
           MOVE 31 TO GFTABJ(5)
           MOVE 30 TO GFTABJ(6)
           MOVE 31 TO GFTABJ(7)
           MOVE 31 TO GFTABJ(8)
           MOVE 30 TO GFTABJ(9)
           MOVE 31 TO GFTABJ(10)
           MOVE 30 TO GFTABJ(11)
           MOVE 31 TO GFTABJ(12)
           CONTINUE.
       BEGIN--PROGRAM SECTION.
           EXEC CICS             HANDLE CONDITION NOTOPEN (
                                                        FERMEE) END-EXEC
           IF EIBCALEN = 0 THEN
             EXEC CICS                                GETMAIN LENGTH(
                                     120) SET (       POINTEUR) END-EXEC
             SET ADDRESS OF KW--COMMA TO POINTEUR
             SET ADDRESS OF COM TO ADDRESS OF KW--COMMA
           END-IF
           MOVE Z_JJ TO GFJOUR
           MOVE Z_MM TO GFMOIS
           MOVE Z_AA TO GFANNEE
           MOVE '1' TO GFDATA
           PERFORM GFDATX THRU E--GFDATX
           IF NOT GFVDAT THEN
             MOVE 'DATE INVALIDE' TO KW--MESSAGE
             EXEC CICS                             SEND FROM(
                      KW--MESSAGE) LENGTH(       80) ERASE WAIT END-EXEC
             EXEC CICS                             DELAY INTERVAL(
                                                            15) END-EXEC
             EXEC CICS                             ABEND ABCODE(
                                                        'TEX0') END-EXEC
           END-IF
           MOVE GFQNTA TO QUANTIEME
           STRING '20' GFANNEE DELIMITED BY SIZE INTO ANNEE
           IF Z_CODE = 'G' THEN
             STRING ANNEE QUANTIEME Z_PROG Z_TACHE Z_CARTE DELIMITED BY
                                                           SIZE INTO CLE
             EXEC CICS                  HANDLE CONDITION NOTFND(
                                                      INTROUV1) END-EXEC
             EXEC CICS                  READ INTO(       ENRE) DATASET(
                                   'FRP000') RIDFLD(       CLE) END-EXEC
             MOVE PARAM TO Z_PARAM
             MOVE '00' TO CODERETOUR
             GO TO FIN
             GO TO INTROUV1
           END-IF
           GO TO FWD--0
           CONTINUE.
       INTROUV1.
           MOVE 'GE' TO CODERETOUR
           GO TO FIN
           CONTINUE.
       FWD--0.
           IF Z_CODE = 'D' THEN
             MOVE 0 TO NOCARTE2
             MOVE Z_CARTE TO NOCARTE
             EXEC CICS                  HANDLE CONDITION NOTFND(
                                                      INTROUV2) END-EXEC
             GO TO INCRE
           END-IF
           GO TO FWD--1
           CONTINUE.
       INCRE.
           STRING ANNEE QUANTIEME Z_PROG Z_TACHE NOCARTE DELIMITED BY
                                                           SIZE INTO CLE
           EXEC CICS                  READ INTO(       ENRE) DATASET(
                                   'FRP000') RIDFLD(       CLE) END-EXEC
           IF PARAM = Z_PARAM THEN
             EXEC CICS                     DELETE DATASET(
              'FRP000') RIDFLD(       CLE) KEYLENGTH(       16) END-EXEC
             MOVE '00' TO CODERETOUR
             GO TO FIN
           END-IF
           CONTINUE.
       INTROUV2.
           IF NOCARTE2 = 99 THEN
             MOVE 'GE' TO CODERETOUR
             GO TO FIN
           END-IF
           COMPUTE NOCARTE2 = NOCARTE2 + 1
           GO TO INCRE
           CONTINUE.
       FWD--1.
           IF Z_CODE = 'R' THEN
             MOVE Z_CARTE TO NOCARTE2
             STRING ANNEE QUANTIEME Z_PROG Z_TACHE NOCARTE2 DELIMITED BY
                                                           SIZE INTO CLE
             EXEC CICS                  HANDLE CONDITION NOTFND(
                                                      INTROUV3) END-EXEC
             EXEC CICS                  READ INTO(       ENRE) DATASET(
                            'FRP000') RIDFLD(       CLE) UPDATE END-EXEC
             MOVE Z_PARAM TO PARAM
             EXEC CICS                  REWRITE DATASET(       'FRP000')
                                              FROM(       ENRE) END-EXEC
             MOVE '00' TO CODERETOUR
             GO TO FIN
             GO TO INTROUV3
           END-IF
           GO TO FWD--2
           CONTINUE.
       INTROUV3.
           MOVE 'GE' TO CODERETOUR
           GO TO FIN
           CONTINUE.
       FWD--2.
           IF Z_CODE = 'I' THEN
             MOVE 0 TO NOCARTE2
             MOVE Z_PROG TO NOMPRO
             MOVE Z_TACHE TO NOTACHE
             MOVE Z_CARTE TO NOCARTE
             EXEC CICS                  HANDLE CONDITION NOTFND(
                                                      INTROUV4) END-EXEC
             GO TO PARCLE
           END-IF
           GO TO FWD--3
           CONTINUE.
       PARCLE.
           STRING ANNEE QUANTIEME Z_PROG Z_TACHE NOCARTE DELIMITED BY
                                                           SIZE INTO CLE
           EXEC CICS                  READ INTO(       ENRE) DATASET(
                                   'FRP000') RIDFLD(       CLE) END-EXEC
           IF PARAM = Z_PARAM THEN
             MOVE 'II' TO CODERETOUR
             GO TO FIN
           END-IF
           IF NOCARTE2 = 99 THEN
             MOVE 'XX' TO CODERETOUR
             GO TO FIN
           END-IF
           COMPUTE NOCARTE2 = NOCARTE2 + 1
           MOVE Z_PROG TO NOMPRO
           MOVE Z_TACHE TO NOTACHE
           GO TO PARCLE
           CONTINUE.
       INTROUV4.
           MOVE Z_PARAM TO PARAM
           EXEC CICS                  WRITE DATASET(       'FRP000')
                           FROM(       ENRE) RIDFLD(       CLE) END-EXEC
           MOVE '00' TO CODERETOUR
           CONTINUE.
       FWD--3.
           GO TO FIN
           CONTINUE.
       FERMEE.
           MOVE 'FICHIERS FERMES APPELER BONDY POSTE 493' TO KW--MESSAGE
           EXEC CICS             SEND FROM(       KW--MESSAGE) LENGTH(
                                                      80) ERASE END-EXEC
           EXEC CICS             ABEND ABCODE(       'TEX0') END-EXEC
           CONTINUE.
       FIN.
           EXEC CICS             RETURN END-EXEC
           CONTINUE.
           COPY CBGOBACK.
       GFDATX.
           MOVE '123' TO VERIFY-MASK
           CALL SP-VERIFY USING LENGTH OF GFDATA GFDATA LENGTH OF '123'
                                         '123' VERIFY-MASK RETURN-VERIFY
           IF RETURN-VERIFY NOT = 0 THEN
             GO TO GFERDAT
           END-IF
           IF GFDATA = '1' THEN
             MOVE B'1' TO FLAG1
           END-IF
           IF GFDATA = '2' THEN
             MOVE B'1' TO FLAG2
           END-IF
           IF GFDATA = '3' THEN
             MOVE B'1' TO FLAG3
           END-IF
           IF FLAG1 OR FLAG2 THEN
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFANNEE GFANNEE LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               GO TO GFERDAT
             END-IF
             COMPUTE GFBA = GFAA
             IF FUNCTION MOD (GFBA 4) = 0 THEN
               MOVE B'1' TO GFBISS
             ELSE
               MOVE B'0' TO GFBISS
             END-IF
             IF GFBISS THEN
               MOVE 29 TO GFTABJ(2)
             ELSE
               MOVE 28 TO GFTABJ(2)
             END-IF
           END-IF
           IF FLAG1 THEN
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFJOUR GFJOUR LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               GO TO GFERDAT
             END-IF
             MOVE '9876543210' TO VERIFY-MASK
             CALL SP-VERIFY USING LENGTH OF GFMOIS GFMOIS LENGTH OF
                     '9876543210' '9876543210' VERIFY-MASK RETURN-VERIFY
             IF RETURN-VERIFY NOT = 0 THEN
               GO TO GFERDAT
             END-IF
             COMPUTE GFBM = GFMM
             COMPUTE GFBJ = GFJJ
             IF GFBM < 1 THEN
               GO TO GFERDAT
             END-IF
             IF GFBM > 12 THEN
               GO TO GFERDAT
             END-IF
             IF GFBJ < 1 THEN
               GO TO GFERDAT
             END-IF
             IF GFBJ > GFTABJ(GFBM) THEN
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG2 THEN
             COMPUTE GFBQTA = GFQNTA
             IF GFBQTA < 1 THEN
               GO TO GFERDAT
             END-IF
             IF GFBISS THEN
               MOVE 366 TO GFX1
             ELSE
               MOVE 365 TO GFX1
             END-IF
             IF GFBQTA > GFX1 THEN
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG3 THEN
             COMPUTE GFBQT0 = GFQNT0
             IF GFBQT0 < 1 THEN
               GO TO GFERDAT
             END-IF
             IF GFBQT0 > 36525 THEN
               GO TO GFERDAT
             END-IF
           END-IF
           IF FLAG1 THEN
             COMPUTE GFBQTA = GFBJ
             COMPUTE ENDVAL--4 = GFBM - 1
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > ENDVAL--4
               COMPUTE GFBQTA = GFBQTA + GFTABJ(I)
             END-PERFORM
           END-IF
           IF FLAG1 OR FLAG2 THEN
             COMPUTE GFX1 = GFBA * 365
             COMPUTE GFX2 = (GFBA + 3) / 4
             COMPUTE GFBQT0 = GFX1 + GFX2 + GFBQTA
           END-IF
           IF FLAG3 THEN
             COMPUTE GFBA = ((GFBQT0 - 1) * 4) / 1461
             COMPUTE GFBQTA = GFBQT0 - ((GFBA * 1461) + 3) / 4
             IF FUNCTION MOD (GFBA 4) = 0 THEN
               MOVE B'1' TO GFBISS
             ELSE
               MOVE B'0' TO GFBISS
             END-IF
             IF GFBISS THEN
               MOVE 29 TO GFTABJ(2)
             ELSE
               MOVE 28 TO GFTABJ(2)
             END-IF
           END-IF
           IF FLAG2 OR FLAG3 THEN
             COMPUTE GFX1 = GFBQTA
             PERFORM VARYING GFBM FROM 1 BY 1 UNTIL NOT (GFX1 > 0)
               COMPUTE GFBJ = GFX1
               COMPUTE GFX1 = GFX1 - GFTABJ(GFBM)
             END-PERFORM
             COMPUTE GFBM = GFBM - 1
           END-IF
           COMPUTE GFSMN = FUNCTION MOD (GFBQT0 + 5 7) + 1
           MOVE B'1' TO GFVDAT
           COMPUTE GFJJ = GFBJ
           COMPUTE GFMM = GFBM
           COMPUTE GFAA = GFBA
           COMPUTE GFQNTA = GFBQTA
           COMPUTE GFQNT0 = GFBQT0
           STRING GFANNEE GFMOIS GFJOUR DELIMITED BY SIZE INTO GFAMJ
           GO TO E--GFDATX
           CONTINUE.
       GFERDAT.
           MOVE B'0' TO GFVDAT
           GO TO E--GFDATX
           CONTINUE.
       E--GFDATX.
           EXIT.
       END--MAIN.
           EXIT.
       END PROGRAM TEX00.
      *
      *
      *
      *
      *
      *
