      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
      ****************************************************************          
       IDENTIFICATION DIVISION.                                                 
      ***************************************************** 02-12-94 *          
      * 02-12-94 -----------------------------------------------------          
      * SUITE AU WORKSHOP STROBE (02-12-94)                                     
      *       TRANSFORMATION DES INDICES EN S9(4) COMP                          
      *       CONVERSION DES ELEMENTS DE CALCUL DE PIC ETENDU                   
      *                                         EN PIC COMP-3                   
      *---------------------------------------------------------------          
      *{ MW DAR-168                                                             
      * PROGRAM-ID    BETDATC1                                                  
       PROGRAM-ID.    BETDATI.                                                    
      *}                                                                        
       AUTHOR        ODINET                                                     
       EJECT                                                                    
      ****************************************************************          
       ENVIRONMENT DIVISION.                                                    
      ****************************************************************          
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       EJECT                                                                    
      ****************************************************************          
       DATA DIVISION.                                                           
      ****************************************************************          
       WORKING-STORAGE SECTION.                                                 
       01  GFTAB.                                                               
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 28.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 30.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 30.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 30.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 30.                                 
           02 FILLER PIC S9(3) COMP-3 VALUE 31.                                 
       01  GFTABB REDEFINES GFTAB.                                              
           02 GFTABJ OCCURS 12 PIC S9(3) COMP-3.                                
       01  SYS-DAT0         PIC  9(6).                                          
       01  SYS-DATE-AMJ   REDEFINES SYS-DAT0.                                   
           02 SYS-DATE-AA   PIC  XX.                                            
           02 SYS-DATE-MM   PIC  XX.                                            
           02 SYS-DATE-JJ   PIC  XX.                                            
       EJECT                                                                    
      * EXPRESSION DATE SOUS FORME JJ/MM/SSAA            - CODE APPEL 0         
       01  GWJMSA-5.                                                            
           05 GFJ-5         PIC  XX.                                            
           05 GF1-5         PIC  X.                                             
           05 GFM-5         PIC  XX.                                            
           05 GF2-5         PIC  X.                                             
           05 GFS-5         PIC  XX.                                            
           05 GFA-5         PIC  XX.                                            
      * EXPRESSION DATE SOUS FORME JJMMSSAA              - CODE APPEL 1         
       01  GWJMSA-2.                                                            
           05 GFJ-2         PIC  XX.                                            
           05 GFM-2         PIC  XX.                                            
           05 GFS-2         PIC  XX.                                            
           05 GFA-2         PIC  XX.                                            
      * EXPRESSION DATE SOUS FORME SSAAMMJJ              - CODE APPEL 5         
       01  GWSAMJ-0.                                                            
           05 GFS-0         PIC  XX.                                            
           05 GFA-0         PIC  XX.                                            
           05 GFM-0         PIC  XX.                                            
           05 GFJ-0         PIC  XX.                                            
      * EXPRESSION DATE SOUS FORME JJMMAA                - CODE APPEL 6         
       01  GWJMA-3.                                                             
           05 GFJ-3         PIC  XX.                                            
           05 GFM-3         PIC  XX.                                            
           05 GFA-3         PIC  XX.                                            
      * EXPRESSION DATE SOUS FORME AAMMJJ                - CODE APPEL 7         
       01  GWAMJ-1.                                                             
           05 GFA-1         PIC  XX.                                            
           05 GFM-1         PIC  XX.                                            
           05 GFJ-1         PIC  XX.                                            
      * EXPRESSION DATE SOUS FORME JJ/MM/AA              - CODE APPEL 9         
       01  GWJMA-4.                                                             
           05 GFJ-4         PIC  XX.                                            
           05 GF1-4         PIC  X.                                             
           05 GFM-4         PIC  XX.                                            
           05 GF2-4         PIC  X.                                             
           05 GFA-4         PIC  XX.                                            
       EJECT                                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  I                PIC  S9(4) COMP.                                    
      *--                                                                       
       77  I                PIC  S9(4) COMP-5.                                  
      *}                                                                        
       77  ANN              PIC  S9(3) COMP-3  VALUE 0.                         
       77  GFX1             PIC  S9(5) COMP-3  VALUE 0.                         
       77  GFX2             PIC  S9(5) COMP-3  VALUE 0.                         
       77  GFXJ             PIC  S9(5) COMP-3  VALUE 0.                         
       77  W-GFBJ           PIC  S9(3) COMP-3  VALUE 0.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-GFBM           PIC  S9(4) COMP    VALUE 0.                         
      *--                                                                       
       77  W-GFBM           PIC  S9(4) COMP-5    VALUE 0.                       
      *}                                                                        
       77  W-GFBA           PIC  S9(3) COMP-3  VALUE 0.                         
       77  W-GFBS           PIC  99            VALUE 0.                         
       77  W-GFBQTA         PIC  S9(3) COMP-3  VALUE 0.                         
       77  W-GFBQT0         PIC  S9(5) COMP-3  VALUE 0.                         
       77  W-GFSMN          PIC  S9(3) COMP-3  VALUE 0.                         
       77  W-GFNUSEM        PIC  S9(3) COMP-3  VALUE 0.                         
       77  W-GFBISS         PIC  9             VALUE 0.                         
       77  RESTE            PIC  S9(3) COMP-3  VALUE 0.                         
       77  MAXSTAND         PIC  S9(5) COMP-3  VALUE 0.                         
       77  NBR-JOUR-SEM     PIC  S9(5) COMP-3  VALUE 0.                         
       77  W8-GFBQT0        PIC  S9(5) COMP-3  VALUE 0.                         
       77  W8-GFSMN         PIC  S9(3) COMP-3  VALUE 0.                         
       77  MD-DISPLAC       PIC  S9(5) COMP-3  VALUE 0.                         
       EJECT                                                                    
      * WORKING POUR VARIATION DE 1 A 60 MOIS SUR UNE DATE                      
       01  WA-SAMJ.                                                             
           02 WA-SS         PIC  99    VALUE 0.                                 
           02 WA-AA         PIC  99    VALUE 0.                                 
           02 WA-MM         PIC  99    VALUE 0.                                 
           02 WA-JJ         PIC  99    VALUE 0.                                 
       01  WA1-SAMJ         REDEFINES  WA-SAMJ.                                 
           02 WA1-SA        PIC 9999.                                           
           02 WA1-MJ        PIC 9999.                                           
       77  WA-MMX           PIC S9(3) COMP-3  VALUE 0.                          
       77  WA-SAX           PIC S9(3) COMP-3  VALUE 0.                          
       77  W-GFAJOUP        PIC S9(3) COMP-3  VALUE 0.                          
       EJECT                                                                    
      ****************************************************************          
       LINKAGE SECTION.                                                         
      ****************************************************************          
ptsup *COPY WORKDATC.                                                           
ptadd  COPY WORKDATI.                                                           
PTADD  01  GFCode-Langue   PIC X(02).                                           
       EJECT                                                                    
      ****************************************************************          
PTSUP *PROCEDURE DIVISION USING WORK-BETDATC.                                   
PTADD  PROCEDURE DIVISION USING WORK-BETDATC GFCode-Langue.                     
      ****************************************************************          
           MOVE '0'        TO GFVDAT.                                           
           MOVE SPACES     TO GF-MESS-ERR.                                      
       EJECT                                                                    
      *-----------------------------*                                           
      * 0  FORMAT ENTREE JJ/MM/SSAA *                                           
      *-----------------------------*                                           
PTADD      EVALUATE GFcode-langue                                               
PTADD         WHEN 'FR'                                                         
PTADD            CONTINUE                                                       
PTADD         WHEN 'GB'                                                         
PTADD            CONTINUE                                                       
PTADD         WHEN OTHER                                                        
PTADD            MOVE     'TETDATC : COUNTRY CODE ERROR '                       
PTADD                                TO  GF-MESS-ERR                            
PTADD            PERFORM  RETOUR-PGM                                            
PTADD      END-EVALUATE                                                         
      *                                                                         
           IF GFDATA = '0'                                                      
              MOVE GFJMSA-5         TO GWJMSA-5                                 
              IF GF1-5 NOT = '/'  OR                                            
                 GF2-5 NOT = '/'                                                
                 MOVE 'BETDATC : FORMAT DATE INVALIDE'  TO  GF-MESS-ERR         
                 PERFORM RETOUR-PGM                                             
              END-IF                                                            
              MOVE GFJ-5            TO GFJOUR                                   
              MOVE GFM-5            TO GFMOIS                                   
              MOVE GFS-5            TO GFSIECLE                                 
              MOVE GFA-5            TO GFANNEE                                  
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *---------------------------*                                             
      * 1  FORMAT ENTREE JJMMSSAA *                                             
      *---------------------------*                                             
           IF GFDATA = '1'                                                      
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *--------------------------*                                              
      * 2  FORMAT ENTREE SSAADDD *                QUANTIEME CALENDAIRE          
      *--------------------------*                                              
           IF GFDATA = '2'                                                      
              PERFORM CTL-NUM-SSAAMMJJ                                          
              MOVE GFQNTA           TO W-GFBQTA                                 
              PERFORM DAT-SSAADDD                                               
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *------------------------*                                                
      * 3  FORMAT ENTREE QQQQQ *                    QUANTIEME STANDARD          
      *------------------------*                                                
           IF GFDATA = '3'                                                      
              MOVE GFQNT0           TO W-GFBQT0                                 
              PERFORM DAT-QQQQQ                                                 
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *-----------------*                                                       
      * 4  DATE SYSTEME *                                                       
      *-----------------*                                                       
           IF GFDATA = '4'                                                      
              ACCEPT SYS-DAT0       FROM DATE                                   
              MOVE SYS-DATE-AA      TO GFANNEE                                  
              MOVE SYS-DATE-MM      TO GFMOIS                                   
              MOVE SYS-DATE-JJ      TO GFJOUR                                   
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *---------------------------*                                             
      * 5  FORMAT ENTREE SSAAMMJJ *                                             
      *---------------------------*                                             
           IF GFDATA = '5'                                                      
              MOVE GFSAMJ-0         TO GWSAMJ-0                                 
              MOVE GFS-0            TO GFSIECLE                                 
              MOVE GFA-0            TO GFANNEE                                  
              MOVE GFM-0            TO GFMOIS                                   
              MOVE GFJ-0            TO GFJOUR                                   
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *-------------------------*                                               
      * 6  FORMAT ENTREE JJMMAA *                                               
      *-------------------------*                                               
           IF GFDATA = '6'                                                      
              MOVE GFJMA-3          TO GWJMA-3                                  
              MOVE GFJ-3            TO GFJOUR                                   
              MOVE GFM-3            TO GFMOIS                                   
              MOVE GFA-3            TO GFANNEE                                  
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *-------------------------*                                               
      * 7  FORMAT ENTREE AAMMJJ *                                               
      *-------------------------*                                               
           IF GFDATA = '7'                                                      
              MOVE GFAMJ-1          TO GWAMJ-1                                  
              MOVE GFA-1            TO GFANNEE                                  
              MOVE GFM-1            TO GFMOIS                                   
              MOVE GFJ-1            TO GFJOUR                                   
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *-------------------------*                                               
      * 8  FORMAT ENTREE SSAASE *                     NUMERO DE SEMAINE         
      *-------------------------*                                               
           IF GFDATA = '8'                                                      
              MOVE GFSEMSS  TO  GFSS                                            
              MOVE GFSEMAA  TO  GFAA                                            
              PERFORM       CTL-NUM-SSAAMMJJ                                    
              PERFORM       DAT-SSAASEM                                         
              GO TO         TRAITE-COMMUN                                       
           END-IF.                                                              
       EJECT                                                                    
      *---------------------------*                                             
      * 9  FORMAT ENTREE JJ/MM/AA *                                             
      *---------------------------*                                             
           IF GFDATA = '9'                                                      
              MOVE GFJMA-4          TO GWJMA-4                                  
              IF GF1-4 NOT = '/'  OR                                            
                 GF2-4 NOT = '/'                                                
                 MOVE 'BETDATC : FORMAT DATE INVALIDE'  TO  GF-MESS-ERR         
                 PERFORM RETOUR-PGM                                             
              END-IF                                                            
              MOVE GFJ-4            TO GFJOUR                                   
              MOVE GFM-4            TO GFMOIS                                   
              MOVE GFA-4            TO GFANNEE                                  
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *------------------------------------*                                    
      * A  FORMAT ENTREE SS AA MM JJ - MMX *          AJOUT 1 A 60 MOIS         
      *------------------------------------*                                    
           IF GFDATA = 'A'                                                      
              IF GFAJOUX NOT NUMERIC                                            
                 MOVE     'BETDATC : AJOUT MOIS NON NUMERIQUE'                  
                                     TO GF-MESS-ERR                             
                 PERFORM  RETOUR-PGM                                            
              END-IF                                                            
              MOVE  GFAJOUP  TO  W-GFAJOUP                                      
              IF GFAJOUP < 01  OR  > 60                                         
                 MOVE     'BETDATC : VALEUR AJOUT MOIS HORS-LIMITES'            
                                     TO GF-MESS-ERR                             
                 PERFORM  RETOUR-PGM                                            
              END-IF                                                            
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              MOVE    GFSS    TO        WA-SS                                   
              MOVE    GFAA    TO        WA-AA                                   
              MOVE    GFJJ    TO        WA-JJ                                   
              COMPUTE WA-MMX  =         GFMM + GFAJOUP                          
              DIVIDE  WA-MMX  BY        12                                      
                              GIVING    WA-SAX                                  
                              REMAINDER WA-MM                                   
              IF WA-MM  =  00                                                   
                 MOVE 12  TO  WA-MM                                             
                 SUBTRACT  1  FROM  WA-SAX                                      
              END-IF                                                            
              COMPUTE WA1-SA = WA1-SA + WA-SAX                                  
              IF WA-MM = 02                                                     
                 IF WA-JJ  >  28                                                
                    MOVE  28  TO  WA-JJ                                         
                 END-IF                                                         
              END-IF                                                            
              IF WA-MM  =  04  OR  06  OR  09  OR  11                           
                 IF WA-JJ  >  30                                                
                    MOVE  30  TO  WA-JJ                                         
                 END-IF                                                         
              END-IF                                                            
              IF WA-SAMJ  >  '20991231'                                         
                 MOVE  '20991231'  TO  WA-SAMJ                                  
              END-IF                                                            
              MOVE WA-SS  TO  GFSS                                              
              MOVE WA-AA  TO  GFAA                                              
              MOVE WA-MM  TO  GFMM                                              
              MOVE WA-JJ  TO  GFJJ                                              
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
      *------------------------------------*                                    
      * B  FORMAT ENTREE SS AA MM JJ - MMX *      - RETRAIT 1 A 60 MOIS         
      *------------------------------------*                                    
           IF GFDATA = 'B'                                                      
              IF GFAJOUX NOT NUMERIC                                            
                 MOVE     'BETDATC : RETRAIT MOIS NON NUMERIQUE'                
                                     TO GF-MESS-ERR                             
                 PERFORM  RETOUR-PGM                                            
              END-IF                                                            
              MOVE GFAJOUP  TO  W-GFAJOUP                                       
              IF GFAJOUP < 01  OR  > 60                                         
                 MOVE 'BETDATC : VALEUR RETRAIT MOIS HORS-LIMITES'              
                       TO GF-MESS-ERR                                           
                 PERFORM RETOUR-PGM                                             
              END-IF                                                            
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              MOVE    GFSS    TO        WA-SS                                   
              MOVE    GFAA    TO        WA-AA                                   
              MOVE    GFJJ    TO        WA-JJ                                   
              COMPUTE WA-MMX  =         GFMM - GFAJOUP                          
              IF WA-MMX > -60  AND  < -47                                       
                 COMPUTE WA-MM  = WA-MMX  + 60                                  
                 COMPUTE WA1-SA = WA1-SA  - 05                                  
              END-IF                                                            
              IF WA-MMX > -48  AND  < -35                                       
                 COMPUTE WA-MM  = WA-MMX  + 48                                  
                 COMPUTE WA1-SA = WA1-SA  - 04                                  
              END-IF                                                            
              IF WA-MMX > -36  AND  < -23                                       
                 COMPUTE WA-MM  = WA-MMX  + 36                                  
                 COMPUTE WA1-SA = WA1-SA  - 03                                  
              END-IF                                                            
              IF WA-MMX > -24  AND  < -11                                       
                 COMPUTE WA-MM  = WA-MMX  + 24                                  
                 COMPUTE WA1-SA = WA1-SA  - 02                                  
              END-IF                                                            
              IF WA-MMX > -12  AND  < 01                                        
                 COMPUTE WA-MM  = WA-MMX  + 12                                  
                 COMPUTE WA1-SA = WA1-SA  - 01                                  
              END-IF                                                            
              IF WA-MMX > 00                                                    
                 MOVE WA-MMX  TO  WA-MM                                         
              END-IF                                                            
              IF WA-MM = 02                                                     
                 IF WA-JJ  >  28                                                
                    MOVE  28  TO  WA-JJ                                         
                 END-IF                                                         
              END-IF                                                            
              IF WA-MM  =  04  OR  06  OR  09  OR  11                           
                 IF WA-JJ  >  30                                                
                    MOVE  30  TO  WA-JJ                                         
                 END-IF                                                         
              END-IF                                                            
              IF WA-SAMJ  <  '19000101'                                         
                 MOVE  '19000101'  TO  WA-SAMJ                                  
              END-IF                                                            
              MOVE WA-SS  TO  GFSS                                              
              MOVE WA-AA  TO  GFAA                                              
              MOVE WA-MM  TO  GFMM                                              
              MOVE WA-JJ  TO  GFJJ                                              
              PERFORM CTL-NUM-SSAAMMJJ                                          
              PERFORM DAT-SSAAMMJJ                                              
              GO TO TRAITE-COMMUN                                               
           END-IF.                                                              
       EJECT                                                                    
           MOVE 'BETDATC : CODE APPEL INVALIDE'  TO  GF-MESS-ERR.               
           PERFORM RETOUR-PGM.                                                  
       EJECT                                                                    
      ****************************************************************          
      * TRAITEMENT COMMUN A TOUS LES CODES D'APPEL                              
      ****************************************************************          
       TRAITE-COMMUN.                                                           
      *----CALCUL DU JOUR DE LA SEMAINE                                         
           PERFORM CAL-JOUR-SEMAINE.                                            
      *----MOUVEMENT DANS LA COMMAREA DES RESULTATS DES CALCULS                 
           MOVE W-GFBJ       TO    GFJJ.                                        
           MOVE W-GFBM       TO    GFMM.                                        
           MOVE W-GFBA       TO    GFAA.                                        
           MOVE W-GFBS       TO    GFSS.                                        
           MOVE W-GFBQTA     TO    GFQNTA.                                      
           MOVE W-GFBQT0     TO    GFQNT0.                                      
           MOVE W-GFBISS     TO    GFBISS.                                      
           MOVE W-GFSMN      TO    GFSMN.                                       
      *----DETERMINATION DES LIBELLES DU JOUR ET DU MOIS                        
           PERFORM DET-JOUR-MOIS.                                               
      *----FORMATAGE DES DATES                                                  
           PERFORM DET-FORME-DATES.                                             
       EJECT                                                                    
      *----CALCUL DU NUMERO DE SEMAINE DE LA DATE TRAITEE                       
           MOVE 0 TO  W-GFBQT0.                                                 
           MOVE 0 TO  W-GFBQTA.                                                 
           MOVE 0 TO  W-GFBS.                                                   
           MOVE 0 TO  W-GFBA.                                                   
           COMPUTE W-GFBQT0 = GFQNT0 + 7 - GFSMN.                               
           PERFORM DAT-QQQQQ.                                                   
           PERFORM CAL-NUMERO-SEMAINE.                                          
           MOVE W-GFNUSEM  TO  GFSEMNU.                                         
           MOVE W-GFBA     TO  GFSEMAA.                                         
           MOVE W-GFBS     TO  GFSEMSS.                                         
           MOVE '1' TO GFVDAT.                                                  
           PERFORM RETOUR-PGM.                                                  
       EJECT                                                                    
      ****************************************************************          
      *                                                                         
      ****************************************************************          
      * TRAITEMENT DATE SOUS FORME SSAAMMJJ TRANSPOSEE EN ENTREE     *          
      * CODE APPEL = '0'  '1'  '5'  '6'  '7'  '9'  'A'  'B'          *          
      ****************************************************************          
       DAT-SSAAMMJJ SECTION.                                                    
           PERFORM CTL-VAL-SIECLE.                                              
           PERFORM CTL-VAL-ANNEE.                                               
           IF W-GFBM < 01  OR  > 12                                             
              MOVE 'BETDATC : VALEUR DU MOIS HORS LIMITES'                      
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           PERFORM CAL-ANNEE-BISSX.                                             
           IF W-GFBJ < 1 OR W-GFBJ > GFTABJ (W-GFBM)                            
              MOVE 'BETDATC : VALEUR DU JOUR HORS LIMITES'                      
                    TO  GF-MESS-ERR                                             
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           MOVE W-GFBJ  TO W-GFBQTA.                                            
           IF W-GFBM = 01                                                       
              GO TO S4                                                          
           END-IF.                                                              
           MOVE 1 TO I.                                                         
       RETOUR.                                                                  
           COMPUTE W-GFBQTA = W-GFBQTA + GFTABJ(I).                             
           ADD 1 TO I.                                                          
           IF I NOT = W-GFBM                                                    
              GO TO RETOUR                                                      
           END-IF.                                                              
       S4.                                                                      
           IF W-GFBQTA < 1 OR W-GFBQTA > GFX1                                   
              MOVE 'BETDATC : QUANTIEME CALENDAIRE HORS LIMITES'                
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           PERFORM CAL-QTIEM-STANDARD.                                          
       FIN-DAT-SSAAMMJJ.       EXIT.                                            
       EJECT                                                                    
      ****************************************************************          
      *                                                                         
      ****************************************************************          
      * TRAITEMENT DATE SOUS FORME SSAADDD                           *          
      * CODE APPEL = '2'                                             *          
      ****************************************************************          
       DAT-SSAADDD  SECTION.                                                    
           PERFORM CTL-VAL-SIECLE.                                              
           PERFORM CTL-VAL-ANNEE.                                               
           PERFORM CAL-ANNEE-BISSX.                                             
           IF W-GFBQTA < 1 OR W-GFBQTA > GFX1                                   
              MOVE 'BETDATC : QUANTIEME CALENDAIRE HORS LIMITES'                
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           PERFORM CAL-QTIEM-STANDARD.                                          
           PERFORM CAL-MOIS-JOUR.                                               
       FIN-DAT-SSAADDD. EXIT.                                                   
       EJECT                                                                    
      ****************************************************************          
      *                                                                         
      ****************************************************************          
      * TRAITEMENT DATE SOUS FORME QQQQQ                             *          
      * CODE APPEL = '3'                                             *          
      ****************************************************************          
       DAT-QQQQQ  SECTION.                                                      
           IF W-GFBQT0 < 1  OR  > 73049                                         
              MOVE 'BETDATC : QUANTIEME STANDARD HORS LIMITES'                  
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF W-GFBQT0 > 36524                                                  
              THEN MOVE  20  TO W-GFBS                                          
              ELSE MOVE  19  TO W-GFBS                                          
           END-IF.                                                              
           PERFORM CTL-VAL-SIECLE.                                              
           COMPUTE MD-DISPLAC = W-GFBQT0 - MAXSTAND.                            
           IF W-GFBS = 20 AND MD-DISPLAC > 365                                  
              COMPUTE W-GFBA = ((W-GFBQT0 - MAXSTAND - 1) * 4) / 1461           
           ELSE                                                                 
              COMPUTE W-GFBA = ((W-GFBQT0 - MAXSTAND) * 4) / 1461               
           END-IF.                                                              
           PERFORM CTL-VAL-ANNEE.                                               
           PERFORM CAL-ANNEE-BISSX.                                             
           IF W-GFBS =  19                                                      
              THEN COMPUTE W-GFBQTA = (W-GFBQT0 - MAXSTAND) -                   
                                      (W-GFBA  * 365)       -                   
                                       W-GFBA  / 4                              
                   IF W-GFBISS  =  1                                            
                      ADD  1  TO  W-GFBQTA                                      
                   END-IF                                                       
              ELSE COMPUTE W-GFBQTA = (W-GFBQT0 - MAXSTAND) -                   
                                      (W-GFBA  * 365)       -                   
                                      (W-GFBA  + 3  ) / 4                       
           END-IF.                                                              
           IF W-GFBQTA < 1 OR W-GFBQTA > GFX1                                   
              MOVE 'BETDATC : ERREUR CALCUL QUANTIEME STANDARD'                 
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           PERFORM CAL-MOIS-JOUR.                                               
       FIN-DAT-QQQQQ.       EXIT.                                               
       EJECT                                                                    
      ****************************************************************          
      *                                                                         
      ****************************************************************          
      * TRAITEMENT DATE SOUS FORME SSAASEM                           *          
      * CODE APPEL = '8'                                             *          
      ****************************************************************          
       DAT-SSAASEM      SECTION.                                                
           IF GFSEMNU < 01 OR > 53                                              
              MOVE 'BETDATC : NUMERO DE SEMAINE HORS LIMITES'                   
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
      *----ON CALCULE LA DATE SSAA0101                                          
           MOVE GFSEMSS   TO  W-GFBS.                                           
           MOVE GFSEMAA   TO  W-GFBA.                                           
           MOVE 01        TO  W-GFBM.                                           
           MOVE 01        TO  W-GFBJ.                                           
           PERFORM DAT-SSAAMMJJ.                                                
           MOVE W-GFBQT0  TO  W8-GFBQT0.                                        
      *----ON CALCULE LE JOUR DE LA SEMAINE DE SSAA0101                         
           PERFORM CAL-JOUR-SEMAINE.                                            
           MOVE W-GFSMN   TO  W8-GFSMN.                                         
      *----ON CALCULE LE NUMERO DE SEMAINE DE SSAA0101                          
           MOVE 0         TO  W-GFBQT0.                                         
           MOVE 0         TO  W-GFBQTA.                                         
           MOVE 0         TO  W-GFBS.                                           
           MOVE 0         TO  W-GFBA.                                           
           COMPUTE W-GFBQT0 = W8-GFBQT0 + 7 - W8-GFSMN.                         
           MOVE W-GFBQT0  TO  GFQNT0.                                           
           PERFORM DAT-QQQQQ.                                                   
           PERFORM CAL-NUMERO-SEMAINE.                                          
           MOVE 0         TO  GFQNT0.                                           
           MOVE 0         TO  W-GFBQT0.                                         
      *----ON SE POSITIONNE AU LUNDI PRECEDENT                                  
           IF W-GFNUSEM = 01                                                    
              COMPUTE W-GFBQT0 = W8-GFBQT0 - W8-GFSMN + 1                       
           END-IF.                                                              
      *----ON SE POSITIONNE AU LUNDI SUIVANT                                    
           IF W-GFNUSEM = 53  OR  52                                            
              COMPUTE W-GFBQT0 = W8-GFBQT0 + 8 - W8-GFSMN                       
           END-IF.                                                              
           MOVE 0         TO  W-GFBS.                                           
           MOVE 0         TO  W-GFBA.                                           
           MOVE 0         TO  W-GFBM.                                           
           MOVE 0         TO  W-GFBJ.                                           
           MOVE 0         TO  W-GFBQTA.                                         
           MOVE 0         TO  W-GFSMN.                                          
           MOVE 0         TO  W-GFBISS.                                         
           COMPUTE NBR-JOUR-SEM = (GFSEMNU * 7).                                
           COMPUTE W-GFBQT0     = W-GFBQT0 + NBR-JOUR-SEM - 7.                  
           PERFORM DAT-QQQQQ.                                                   
       FIN-DAT-SSAASEM.     EXIT.                                               
       EJECT                                                                    
      *===============================================================          
      *                                                              *          
      *        PROCEDURES DE CALCUL ==> CAL                          *          
      *        ====================                                  *          
      *                                                              *          
      *   CAL-ANNEE-BISSX          ANNEE BISSEXTILE                  *          
      *                                                              *          
      *   CAL-QTIEM-STANDARD       QUANTIEME STANDARD                *          
      *                                                              *          
      *   CAL-MOIS-JOUR            VALEUR DU MOIS ET DU JOUR         *          
      *                                                              *          
      *   CAL-JOUR-SEMAINE         JOUR DE LA SEMAINE                *          
      *                                                              *          
      *   CAL-NUMERO-SEMAINE       NUMERO DE SEMAINE                 *          
      *                                                              *          
      *===============================================================          
       EJECT                                                                    
      *===============================================================          
      * CALCUL ANNEE BISSEXTILE                                                 
      *===============================================================          
       CAL-ANNEE-BISSX    SECTION.                                              
           DIVIDE W-GFBA BY 4 GIVING ANN.                                       
           COMPUTE ANN = ANN * 4.                                               
           IF ANN = W-GFBA                                                      
              THEN MOVE 1   TO W-GFBISS                                         
                   MOVE 29  TO GFTABJ(2)                                        
                   MOVE 366 TO GFX1                                             
              ELSE MOVE 0   TO W-GFBISS                                         
                   MOVE 28  TO GFTABJ(2)                                        
                   MOVE 365 TO GFX1                                             
           END-IF.                                                              
           IF W-GFBS =  19  AND  W-GFBA = 00                                    
              MOVE 0   TO W-GFBISS                                              
              MOVE 28  TO GFTABJ(2)                                             
              MOVE 365 TO GFX1                                                  
           END-IF.                                                              
       FIN-CAL-ANNEE-BISSX.     EXIT.                                           
       EJECT                                                                    
      *===============================================================          
      * CALCUL DU QUANTIEME STANDARD                                            
      *===============================================================          
       CAL-QTIEM-STANDARD SECTION.                                              
           COMPUTE GFX1     =  W-GFBA * 365.                                    
           COMPUTE GFX2     = (W-GFBA + 3) / 4.                                 
           IF W-GFBS =  19     AND  W-GFBA > 0                                  
              THEN COMPUTE GFX2     = GFX2 - 1                                  
           END-IF.                                                              
           COMPUTE W-GFBQT0 = GFX1     +                                        
                              GFX2     +                                        
                              W-GFBQTA +                                        
                              MAXSTAND.                                         
           IF W-GFBQT0 < 1  OR  > 73049                                         
              MOVE 'BETDATC : ERREUR CALCUL QUANTIEME STANDARD'                 
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
       FIN-CAL-QTIEM-STANDARD.   EXIT.                                          
       EJECT                                                                    
      *===============================================================          
      * CALCUL DU MOIS ET DU JOUR                                               
      *===============================================================          
       CAL-MOIS-JOUR      SECTION.                                              
           MOVE W-GFBQTA TO GFXJ.                                               
           MOVE 1        TO W-GFBM.                                             
       RET.                                                                     
           IF GFXJ > 0                                                          
              THEN GO TO S8                                                     
              ELSE GO TO S9                                                     
           END-IF.                                                              
       S8.                                                                      
           MOVE GFXJ TO W-GFBJ.                                                 
           COMPUTE GFXJ = GFXJ - GFTABJ(W-GFBM).                                
           ADD 1 TO W-GFBM.                                                     
           GO TO RET.                                                           
       S9.                                                                      
           COMPUTE W-GFBM = W-GFBM - 1.                                         
           IF W-GFBM < 01  OR  > 12                                             
              MOVE 'BETDATC : ERREUR CALCUL MOIS'  TO  GF-MESS-ERR              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF W-GFBJ < 1 OR W-GFBJ > GFTABJ (W-GFBM)                            
              MOVE 'BETDATC : ERREUR CALCUL JOUR'  TO  GF-MESS-ERR              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
       FIN-CAL-MOIS-JOUR.        EXIT.                                          
       EJECT                                                                    
      *===============================================================          
      * CALCUL DU JOUR DE LA SEMAINE                                            
      *===============================================================          
       CAL-JOUR-SEMAINE        SECTION.                                         
           DIVIDE  W-GFBQT0 BY 7 GIVING ANN REMAINDER W-GFSMN.                  
           IF W-GFSMN < 0 OR > 6                                                
              MOVE 'BETDATC : ERREUR CALCUL JOUR SEMAINE'                       
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
PTSUP *    IF W-GFSMN = 0                                                       
PTSUP *       MOVE 7  TO  W-GFSMN                                               
PTSUP *    END-IF.                                                              
PTADD      EVALUATE GFcode-langue                                               
PTADD      WHEN 'FR'                                                            
PTADD         IF W-GFSMN = 0                                                    
PTADD         MOVE 7  TO  W-GFSMN                                               
PTADD         END-IF                                                            
PTADD      WHEN 'GB'                                                            
PTADD ********** jour compris entre 1 et 7 ******                               
PTADD         ADD 1 TO W-GFSMN                                                  
PTADD      END-EVALUATE.                                                        
       FIN-CAL-JOUR-SEMAINE.       EXIT.                                        
       EJECT                                                                    
      *===============================================================          
      * CALCUL DU NUMERO DE SEMAINE                                             
      *===============================================================          
       CAL-NUMERO-SEMAINE      SECTION.                                         
           DIVIDE W-GFBQTA BY 7 GIVING W-GFNUSEM REMAINDER RESTE.               
           IF RESTE > 4                                                         
              ADD 1 TO W-GFNUSEM                                                
              GO TO FIN-CAL-NUMERO-SEMAINE                                      
           END-IF.                                                              
           IF RESTE = 4                                                         
              ADD 1 TO W-GFNUSEM                                                
              GO TO FIN-CAL-NUMERO-SEMAINE                                      
           END-IF.                                                              
           IF W-GFNUSEM NOT = 00                                                
              GO TO FIN-CAL-NUMERO-SEMAINE                                      
           END-IF.                                                              
       EJECT                                                                    
      *----RECHERCHER QTIEME STANDARD DE SSAA0101 ET                            
      *    OBTENIR LE JOUR DE SEMAINE DE CETTE DATE                             
           MOVE GFSS  TO  W-GFBS.                                               
           MOVE GFAA  TO  W-GFBA.                                               
           MOVE 01    TO  W-GFBM.                                               
           MOVE 01    TO  W-GFBJ.                                               
       ANNEE-PRECED.                                                            
           PERFORM  DAT-SSAAMMJJ.                                               
           PERFORM  CAL-JOUR-SEMAINE.                                           
      *----CALCUL DU QANTIEME STANDARD DU LUNDI PRECEDENT SSAA0101              
           COMPUTE  W-GFBQT0  =  W-GFBQT0 - W-GFSMN + 1.                        
      *----CALCUL DU N0 SEMAINE A PARTIR DE LA DIFFERENCE DES QTIEMES           
      *    DE LA DATE A TRAITER ET DU LUNDI PRECEDENT LE 01 JANVIER             
           COMPUTE  W-GFNUSEM  =  (GFQNT0 - W-GFBQT0) / 7.                      
           IF W-GFNUSEM = 0  AND  W-GFSMN  >  4                                 
              IF W-GFBS = 20 AND W-GFBA = 00                                    
                 THEN MOVE 19 TO W-GFBS                                         
                      MOVE 99 TO W-GFBA                                         
                 ELSE SUBTRACT  1  FROM  W-GFBA                                 
              END-IF                                                            
              GO TO  ANNEE-PRECED                                               
           END-IF.                                                              
      *----AJUSTEMENT DU NUMERO DE SEMAINE EN FONCTION DU NUMERO                
      *    DE JOUR DE LA DATE SSAA0101                                          
           IF W-GFSMN  <  4   OR  =  4                                          
              ADD  1  TO  W-GFNUSEM                                             
           END-IF.                                                              
       FIN-CAL-NUMERO-SEMAINE.     EXIT.                                        
       EJECT                                                                    
      *---------------------------------------------------------------          
      *                                                              *          
      *        PROCEDURES DE CONTROLE ==> CTL                        *          
      *        ----------------------                                *          
      *                                                              *          
      *   CTL-VAL-SIECLE           VALEUR DU SIECLE                  *          
      *                                                              *          
      *   CTL-VAL-ANNEE            VALEUR DE L'ANNEE                 *          
      *                                                              *          
      *   CTL-NUM-SSAAMMJJ         NUMERICITE AA SS MM JJ            *          
      *                                                              *          
      *---------------------------------------------------------------          
       EJECT                                                                    
      *---------------------------------------------------------------          
      * CONTROLE DU SIECLE                                                      
      *---------------------------------------------------------------          
       CTL-VAL-SIECLE SECTION.                                                  
           IF W-GFBS   <  19  OR  > 20                                          
              MOVE 'BETDATC : VALEUR DU SIECLE HORS LIMITES'                    
                    TO GF-MESS-ERR                                              
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF W-GFBS   =   19                                                   
              THEN MOVE  0    TO MAXSTAND                                       
              ELSE MOVE 36524 TO MAXSTAND                                       
           END-IF.                                                              
       FIN-CTL-VAL-SIECLE. EXIT.                                                
       EJECT                                                                    
      *---------------------------------------------------------------          
      * CONTROLE VALEUR ANNEE                                                   
      *---------------------------------------------------------------          
       CTL-VAL-ANNEE    SECTION.                                                
           IF W-GFBA < 00  OR  > 99                                             
              MOVE 'BETDATC : VALEUR ANNEE NON ADMISSIBLE'                      
                    TO  GF-MESS-ERR                                             
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
       FIN-CTL-VAL-ANNEE.   EXIT.                                               
       EJECT                                                                    
      *---------------------------------------------------------------          
      * CONTROLE DE NUMERICITE DE SS AA MM JJ                                   
      *---------------------------------------------------------------          
       CTL-NUM-SSAAMMJJ SECTION.                                                
           IF GFANNEE  NOT NUMERIC                                              
              MOVE 'BETDATC : ANNEE NON NUMERIQUE'    TO  GF-MESS-ERR           
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF GFDATA = '6'  OR  '7'  OR  '9'  OR  '4'                           
              IF GFANNEE < '50'                                                 
                 THEN MOVE '20'  TO  GFSIECLE                                   
                 ELSE MOVE '19'  TO  GFSIECLE                                   
              END-IF                                                            
           END-IF.                                                              
           IF GFSIECLE NOT NUMERIC                                              
              MOVE 'BETDATC : SIECLE NON NUMERIQUE'   TO  GF-MESS-ERR           
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF GFDATA = '8'                                                      
              GO TO FIN-CTL-NUM-SSAAMMJJ                                        
           END-IF.                                                              
           MOVE GFSS  TO  W-GFBS.                                               
           MOVE GFAA  TO  W-GFBA.                                               
           IF GFDATA = '2'                                                      
              GO TO FIN-CTL-NUM-SSAAMMJJ                                        
           END-IF.                                                              
           IF GFMOIS   NOT NUMERIC                                              
              MOVE 'BETDATC : MOIS NON NUMERIQUE'     TO  GF-MESS-ERR           
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           IF GFJOUR   NOT NUMERIC                                              
              MOVE 'BETDATC : JOUR NON NUMERIQUE'     TO  GF-MESS-ERR           
              PERFORM RETOUR-PGM                                                
           END-IF.                                                              
           MOVE GFMM  TO  W-GFBM.                                               
           MOVE GFJJ  TO  W-GFBJ.                                               
       FIN-CTL-NUM-SSAAMMJJ. EXIT.                                              
       EJECT                                                                    
      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
      *                                                              *          
      *        PROCEDURES DE DETERMINATION ==> DET                   *          
      *        +++++++++++++++++++++++++++                           *          
      *                                                              *          
      *   DET-JOUR-MOIS            LIBELLES DU JOUR ET DU MOIS       *          
      *                                                              *          
      *   DET-FORME-DATES          FORMATAGE DES DATES               *          
      *                                                              *          
      *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*          
       EJECT                                                                    
      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
      * DETERMINATION DES LIBELLES DU JOUR ET DU MOIS                           
      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
       DET-JOUR-MOIS           SECTION.                                         
      *----DETERMINETION DES LIBELLES DU JOUR                                   
PTSUP *    EVALUATE  GFSMN                                                      
PTSUP *        WHEN  1  MOVE 'LUN'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'LUNDI'     TO  GFSMN-LIB-L                        
PTSUP *        WHEN  2  MOVE 'MAR'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'MARDI'     TO  GFSMN-LIB-L                        
PTSUP *        WHEN  3  MOVE 'MER'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'MERCREDI'  TO  GFSMN-LIB-L                        
PTSUP *        WHEN  4  MOVE 'JEU'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'JEUDI'     TO  GFSMN-LIB-L                        
PTSUP *        WHEN  5  MOVE 'VEN'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'VENDREDI'  TO  GFSMN-LIB-L                        
PTSUP *        WHEN  6  MOVE 'SAM'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'SAMEDI'    TO  GFSMN-LIB-L                        
PTSUP *        WHEN  7  MOVE 'DIM'       TO  GFSMN-LIB-C                        
PTSUP *                 MOVE 'DIMANCHE'  TO  GFSMN-LIB-L                        
PTSUP *    END-EVALUATE.                                                        
PTADD      EVALUATE GFCODE-LANGUE ALSO GFSMN                                    
PTADD          WHEN  'FR' ALSO 1                                                
PTADD                   MOVE 'LUN'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'LUNDI'     TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 2                                                
PTADD                   MOVE 'MAR'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'MARDI'     TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 3                                                
PTADD                   MOVE 'MER'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'MERCREDI'  TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 4                                                
PTADD                   MOVE 'JEU'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'JEUDI'     TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 5                                                
PTADD                   MOVE 'VEN'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'VENDREDI'  TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 6                                                
PTADD                   MOVE 'SAM'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'SAMEDI'    TO  GFSMN-LIB-L                        
PTADD          WHEN  'FR' ALSO 7                                                
PTADD                   MOVE 'DIM'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'DIMANCHE'  TO  GFSMN-LIB-L                        
PTADD          WHEN  'GB' ALSO 1                                                
PTADD                   MOVE 'SUN'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'SUNDAY'  TO  GFSMN-LIB-L                          
PTADD          WHEN  'GB' ALSO 2                                                
PTADD                   MOVE 'MON'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'MONDAY'     TO  GFSMN-LIB-L                       
PTADD          WHEN  'GB' ALSO 3                                                
PTADD                   MOVE 'TUE'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'TUESDAY'     TO  GFSMN-LIB-L                      
PTADD          WHEN  'GB' ALSO 4                                                
PTADD                   MOVE 'WED'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'WEDNESDAY'  TO  GFSMN-LIB-L                       
PTADD          WHEN  'GB' ALSO 5                                                
PTADD                   MOVE 'THU'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'THURSDAY'     TO  GFSMN-LIB-L                     
PTADD          WHEN  'GB' ALSO 6                                                
PTADD                   MOVE 'FRI'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'FRIDAY'  TO  GFSMN-LIB-L                          
PTADD          WHEN  'GB' ALSO 7                                                
PTADD                   MOVE 'SAT'       TO  GFSMN-LIB-C                        
PTADD                   MOVE 'SATURDAY'    TO  GFSMN-LIB-L                      
PTADD      END-EVALUATE.                                                        
      *                                                                         
       EJECT                                                                    
      *                                                                         
      * DETERMINATION DES LIBELLES DU MOIS                                      
PTSUP *    EVALUATE  GFMM                                                       
PTSUP *        WHEN  '01'  MOVE 'JAN'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'JANVIER'    TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '02'  MOVE 'FEV'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'FEVRIER'    TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '03'  MOVE 'MAR'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'MARS'       TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '04'  MOVE 'AVR'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'AVRIL'      TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '05'  MOVE 'MAI'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'MAI'        TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '06'  MOVE 'JUN'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'JUIN'       TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '07'  MOVE 'JUI'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'JUILLET'    TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '08'  MOVE 'AOU'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'AOUT'       TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '09'  MOVE 'SEP'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'SEPTEMBRE'  TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '10'  MOVE 'OCT'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'OCTOBRE'    TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '11'  MOVE 'NOV'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'NOVEMBRE'   TO  GFMOI-LIB-L                    
PTSUP *        WHEN  '12'  MOVE 'DEC'        TO  GFMOI-LIB-C                    
PTSUP *                    MOVE 'DECEMBRE'   TO  GFMOI-LIB-L                    
PTSUP *    END-EVALUATE.                                                        
PTADD      EVALUATE GFCode-Langue ALSO GFMM                                     
PTADD          WHEN  'FR' ALSO '01'                                             
PTADD                      MOVE 'JAN'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JANVIER'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '02'                                             
PTADD                      MOVE 'FEV'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'FEVRIER'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '03'                                             
PTADD                      MOVE 'MAR'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'MARS'       TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '04'                                             
PTADD                      MOVE 'AVR'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'AVRIL'      TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '05'                                             
PTADD                      MOVE 'MAI'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'MAI'        TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '06'                                             
PTADD                      MOVE 'JUN'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JUIN'       TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '07'                                             
PTADD                      MOVE 'JUI'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JUILLET'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '08'                                             
PTADD                      MOVE 'AOU'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'AOUT'       TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '09'                                             
PTADD                      MOVE 'SEP'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'SEPTEMBRE'  TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '10'                                             
PTADD                      MOVE 'OCT'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'OCTOBRE'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '11'                                             
PTADD                      MOVE 'NOV'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'NOVEMBRE'   TO  GFMOI-LIB-L                    
PTADD          WHEN  'FR' ALSO '12'                                             
PTADD                      MOVE 'DEC'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'DECEMBRE'   TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '01'                                             
PTADD                      MOVE 'JAN'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JANUARY'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '02'                                             
PTADD                      MOVE 'FEB'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'FEBRUARY'   TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '03'                                             
PTADD                      MOVE 'MAR'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'MARCH'      TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '04'                                             
PTADD                      MOVE 'APR'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'APRIL'      TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '05'                                             
PTADD                      MOVE 'MAY'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'MAY'        TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '06'                                             
PTADD                      MOVE 'JUN'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JUNE'       TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '07'                                             
PTADD                      MOVE 'JUL'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'JULY'       TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '08'                                             
PTADD                      MOVE 'AUG'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'AUGUST'     TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '09'                                             
PTADD                      MOVE 'SEP'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'SEPTEMBER'  TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '10'                                             
PTADD                      MOVE 'OCT'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'OCTOBER'    TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '11'                                             
PTADD                      MOVE 'NOV'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'NOVEMBER'   TO  GFMOI-LIB-L                    
PTADD          WHEN  'GB' ALSO '12'                                             
PTADD                      MOVE 'DEC'        TO  GFMOI-LIB-C                    
PTADD                      MOVE 'DECEMBER'   TO  GFMOI-LIB-L                    
PTADD      END-EVALUATE.                                                        
       FIN-DET-JOUR-MOIS.       EXIT.                                           
       EJECT                                                                    
      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
      * MISE EN PLACE DES DIFFERENTES DE DATES                                  
      *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
       DET-FORME-DATES SECTION.                                                 
           MOVE W-GFBS    TO  GFS-0                                             
                              GFS-2                                             
                              GFS-5.                                            
           MOVE GFAA      TO  GFA-0                                             
                              GFA-1                                             
                              GFA-2                                             
                              GFA-3                                             
                              GFA-4                                             
                              GFA-5.                                            
           MOVE GFMM      TO  GFM-0                                             
                              GFM-1                                             
                              GFM-2                                             
                              GFM-3                                             
                              GFM-4                                             
                              GFM-5.                                            
           MOVE GFJJ      TO  GFJ-0                                             
                              GFJ-1                                             
                              GFJ-2                                             
                              GFJ-3                                             
                              GFJ-4                                             
                              GFJ-5.                                            
           MOVE '/'       TO  GF1-4                                             
                              GF2-4                                             
                              GF1-5                                             
                              GF2-5.                                            
           MOVE GWSAMJ-0  TO  GFSAMJ-0.                                         
           MOVE GWAMJ-1   TO  GFAMJ-1.                                          
           MOVE GWJMSA-2  TO  GFJMSA-2.                                         
           MOVE GWJMA-3   TO  GFJMA-3.                                          
           MOVE GWJMA-4   TO  GFJMA-4.                                          
           MOVE GWJMSA-5  TO  GFJMSA-5.                                         
       FIN-DET-FORME-DATES. EXIT.                                               
       EJECT                                                                    
      ****************************************************************          
      * RETOUR AU PROGRAMME APPALANT                                 *          
      ****************************************************************          
       RETOUR-PGM     SECTION.                                                  
           GOBACK.                                                              
       FIN-RETOUR-PGM.     EXIT.                                                
