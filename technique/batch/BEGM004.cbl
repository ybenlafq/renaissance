      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BEGM004                                                      
      *================================================================*        
      *           CE MODULE PERMET DE FAIRE DES COMPARAISONS DE        *        
      *           DEUX ZONES.                                          *        
      *   EN ENTREE : DESCRIPTION DES ZONES SUR LESQUELLES EFFECTUER   *        
      *               LA COMPARAISON.                                  *        
      *   EN SORTIE : UN BOOLEEN DONNANT LA VALIDITE DE LA COMPARAISON *        
      *               '0' = FAUX                                       *        
      *               '1' = VRAI                                       *        
      *================================================================*        
       AUTHOR. P-ROLLAND-APSIDE.                                                
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
           COPY CEGM000W.                                                       
       LINKAGE SECTION.                                                         
           COPY ZWEGM004.                                                       
      *-------------------------------------------------------------            
       PROCEDURE DIVISION USING ZWORK-COMPARAISON.                              
      *-------------------------------------------------------------            
           PERFORM TRAITEMENT-EGM004.                                           
           GOBACK.                                                              
      *----------------------------------------                                 
           COPY CEGM004P.                                                       
